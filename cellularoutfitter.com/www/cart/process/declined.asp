<%
useHttps = 1
response.buffer = true
SEtitle = "Processor Declined - CellularOutfitter.com"
SEdescription = ""
SEkeywords = ""
pageTitle = "declined"
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/cart/includes/inc_getStates.asp"-->
<!--#include virtual="/includes/asp/inc_getMySession.asp"-->
<%
declinedOrderID = prepInt(request.Form("declinedOrderID"))
sql	=	"exec getDeclinedOrderDetail " & declinedOrderID
'response.write "<pre>" & sql & "</pre>"
'response.end
set rs = oConn.execute(sql)
if rs.eof then
	response.redirect("/sessionexpired.asp")
	response.end
end if

orderSubTotal = rs("ordersubtotal")
orderShippingFee = rs("ordershippingfee")
orderTax = rs("orderTax")
orderGrandTotal = rs("ordergrandtotal")
strShiptype = rs("shiptype")

fname = rs("fname")
lname = rs("lname")
email = rs("email")
phone = rs("phone")
sAddress1 = rs("sAddress1")
sAddress2 = rs("sAddress2")
scity = rs("scity")
sstate = rs("sstate")
szip = rs("szip")
sCountry = rs("sCountry")
bAddress1 = rs("bAddress1")
bAddress2 = rs("bAddress2")
bcity = rs("bcity")
bstate = rs("bstate")
bzip = rs("bzip")
couponid = rs("couponid")

if couponid > 0 then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	SQL = "SELECT * FROM CO_coupons WHERE couponid = '" & couponid & "'"
	set couponRS = oConn.execute(sql)
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = couponRS("couponid")
		promoMin = couponRS("promoMin")
		promoPercent = couponRS("promoPercent")
		typeID = couponRS("typeID")
		excludeBluetooth = couponRS("excludeBluetooth")
		BOGO = couponRS("BOGO")
		couponDesc = couponRS("couponDesc")
		'FreeItemPartNumber = RS("FreeItemPartNumber")
	end if
end if

nShipType = 0
if instr(strShiptype, "First Class") > 0 then
	nShipType = 0
elseif instr(strShiptype, "USPS Priority") > 0 then
	nShipType = 2
elseif instr(strShiptype, "USPS Express") > 0 then
	nShipType = 3
elseif instr(strShiptype, "UPS Ground") > 0 then
	nShipType = 7
elseif instr(strShiptype, "UPS 3 Day Select") > 0 then
	nShipType = 8
elseif instr(strShiptype, "UPS 2nd Day Air") > 0 then
	nShipType = 9
end if

nProdIdCount = 0
nTotalQuantity = 0
shippingQty = 0
strItemCheck = ""
strQty = ""
FreeProductInCart = 0
specialAmt = 0

sql	= "select promoCode from co_coupons where couponid = '" & couponid & "'"
set rsCoupon = oConn.execute(sql)
if not rsCoupon.eof then sPromoCode = rsCoupon("promoCode")

do until RS.eof
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("quantity")
	sItemPrice = rs("price")
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	sItemName = rs("itemdesc")
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		sWeight = cdbl(sWeight) + cdbl(rs("itemWeight"))
		nSubTotal = nSubTotal + (sItemPrice * nProdQuantity)
		nTotalQuantity = nTotalQuantity + RS("quantity")
										
		WEhtml = WEhtml & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_number_" &  nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""ssl_item_qty_" &  nProdIdCount & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""ssl_item_price_" &  nProdIdCount & """ value=""" & sItemPrice & """>" & vbcrlf
	end if
	RS.movenext
loop
rs = null

dim finalSubTotal : finalSubTotal = nSubTotal
'response.write "finalSubTotal:" & finalSubTotal & "<br>"
%>
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/includes/template/top_cart.asp"-->
<%
dim nDiscountedSubTotal : nDiscountedSubTotal = nSubTotal
'response.write "nDiscountedSubTotal:" & nDiscountedSubTotal & "<br>"
%>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
	function getCardType(cardNum) {
		var f = document.frmDeclined;
		
		document.getElementById('cc_master_img').src = '/images/checkout/cc-master-off.png';
		document.getElementById('cc_master').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_master').style.borderColor = '#f4f4f4';
		document.getElementById('cc_visa_img').src = '/images/checkout/cc-visa-off.png';
		document.getElementById('cc_visa').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_visa').style.borderColor = '#f4f4f4';
		document.getElementById('cc_amex_img').src = '/images/checkout/cc-amex-off.png';
		document.getElementById('cc_amex').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_amex').style.borderColor = '#f4f4f4';
		document.getElementById('cc_disc_img').src = '/images/checkout/cc-discover-off.png';
		document.getElementById('cc_disc').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_disc').style.borderColor = '#f4f4f4';

		var first1 = parseInt(cardNum.substring(0,1));
		var first2 = parseInt(cardNum.substring(0,2));
		if (first1 == 4) {
			f.cc_cardType.value = "VISA";
			document.getElementById('cc_visa_img').src = '/images/checkout/cc-visa-on.png';
			document.getElementById('cc_visa').style.backgroundColor = '#fff';
			document.getElementById('cc_visa').style.borderColor = '#ccc';
		}
		else if (first2 == 34 || first2 == 37) {
			f.cc_cardType.value = "AMEX";
			document.getElementById('cc_amex_img').src = '/images/checkout/cc-amex-on.png';
			document.getElementById('cc_amex').style.backgroundColor = '#fff';
			document.getElementById('cc_amex').style.borderColor = '#ccc';
		}
		else if (first2 > 50 && first2 < 56) {
			f.cc_cardType.value = "MC";
			document.getElementById('cc_master_img').src = '/images/checkout/cc-master-on.png';
			document.getElementById('cc_master').style.backgroundColor = '#fff';
			document.getElementById('cc_master').style.borderColor = '#ccc';
		}
		else if (first1 == 6) {
			f.cc_cardType.value = "DISC";
			document.getElementById('cc_disc_img').src = '/images/checkout/cc-discover-on.png';
			document.getElementById('cc_disc').style.backgroundColor = '#fff';
			document.getElementById('cc_disc').style.borderColor = '#ccc';
		}
	}
	
	function onEdit(rowName) {
		$('#row_'+rowName).find('.txtLabel').css('display', 'none');
		$('#row_'+rowName).find('.btnEdit').css('display', 'none');
		$('#row_'+rowName).find('.txtEdit').css('display', 'block');
	}
	
	function openLowPricePopup() {
		document.getElementById("popBox").innerHTML = document.getElementById('lowPricePopup').innerHTML;
		
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function closeLowPricePopup() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}	
	
	function openReturnPopup() {
		document.getElementById("popBox").innerHTML = document.getElementById('returnPopup').innerHTML;
		
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function closeReturnPopup() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}	
	
	function CheckSubmit() {
		var f = document.frmDeclined;
		bValid = true;

		CheckValidNEW(f.email.value, "Email is a required field!"); if (!bValid) { f.email.focus(); return false; }
		CheckEMailNEW(f.email.value); if (!bValid) f.email.focus();
		
		CheckValidNEW(f.fname.value, "Your First Name is a required field!"); if (!bValid) { f.fname.focus(); return false; }
		CheckValidNEW(f.lname.value, "Your Last Name is a required field!"); if (!bValid) { f.lname.focus(); return false; }
		CheckValidNEW(f.phone.value, "Your Phone Number is a required field!"); if (!bValid) { f.phone.focus(); return false; }
		
		CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!"); if (!bValid) { f.bAddress1.focus(); return false; }
		CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!"); if (!bValid) { f.bCity.focus(); return false; }
		CheckValidNEW(f.bState.options[f.bState.selectedIndex].value, "State (for Billing Address) is a required field!"); if (!bValid) { f.bState.focus(); return false; }
		CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!"); if (!bValid) { f.bZip.focus(); return false; }
	
		CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!"); if (!bValid) { f.cc_cardOwner.focus(); return false; }
		CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!"); if (!bValid) { f.cardNum.focus(); return false; }
		CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!"); if (!bValid) { f.cc_month.focus(); return false; }
		CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!"); if (!bValid) { f.cc_year.focus(); return false; }
		CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!"); if (!bValid) { f.secCode.focus(); return false; }

		if (bValid) {
			<% if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then %>
				f.action = '/framework/processing.asp';
			<% else %>
				f.action = 'https://www.cellularoutfitter.com/framework/processing.asp';
			<% end if %>
			document.getElementById("submitBttn").innerHTML = "Please wait while<br />we process your order"
			f.submit();
		}
		return false;
	}	
</script>
<link rel="stylesheet" type="text/css" href="/includes/css/checkoutBase.css" />
<div id="account_nav" style="display:none;"></div>
<td>
    <form id="id_frmDeclined" method="post" name="frmDeclined" >
    <input type="hidden" name="sWeight" value="<%=sWeight%>">
    <input type="hidden" name="nTotalQuantity" value="<%=nTotalQuantity%>">
	<input type="hidden" name="shippingTotal" value="<%=orderShippingFee%>">
	<input type="hidden" name="nPromo" value="<%=couponid%>">
    <input type="hidden" name="nCATax" value="<%=orderTax%>">
    <input type="hidden" name="subTotal" value="<%=finalSubTotal%>">
	<input type="hidden" name="discountedSubTotal" value="<%=nDiscountedSubTotal%>">
    <input type="hidden" name="grandTotal" value="<%=orderGrandTotal%>">
    <input type="hidden" name="ssl_ProdIdCount" value="<%=nProdIdCount%>">
    <input type="hidden" name="PaymentType" value="CC">
    <input type="hidden" name="shiptype" value="<%=nShipType%>">
    <input type="hidden" name="sAddress1" value="<%=sAddress1%>">
	<input type="hidden" name="sAddress2" value="<%=sAddress2%>">
	<input type="hidden" name="sCity" value="<%=sCity%>">
	<input type="hidden" name="sState" value="<%=sState%>">
	<input type="hidden" name="sZip" value="<%=sZip%>">
    <input type="hidden" name="chkOptMail" value="Y">
	<%=WEhtml%>
    <%=EBtemp%>
    
    <div style="width:980px; text-align:left; padding-top:10px;">
        <div id="mvt_topBanner" style="float:left; width:100%; text-align:center; padding:5px 0px 5px 0px;">
        	<img src="/images/cart/110-banner.jpg" border="0" width="980" height="78" usemap="#secureBanner" />
            <map name="secureBanner">
                <area shape="rect" coords="0,0,562,78" href="javascript:openLowPricePopup();" alt="110% LOW PRICE GUARANTEE">
                <area shape="rect" coords="563,0,980,78" href="javascript:openReturnPopup();" alt="90-DAY RETURN POLICY">
            </map>
        </div>
        <div style="float:left; width:100%; text-align:center; border-top:1px dotted #000; border-bottom:1px dotted #000; padding:15px 0px 15px 0px;">
        	<div style="float:left; margin-left:250px; padding:0px 10px 0px 0px;"><img src="/images/checkout/lock.png" border="0" /></div>
        	<a href="/cart/basket.asp" style="font-size:26px; color:#999; font-weight:normal;"><div style="float:left; font-size:28px; color:#999; padding-top:6px;">Shopping Cart</div></a>
        	<div style="float:left; padding:0px 10px 0px 10px;"><img src="/images/checkout/dotted-div.png" border="0" /></div>
        	<div style="float:left; font-size:26px; color:#02659E; padding-top:6px;">Order</div>
        	<div style="float:left; padding:0px 10px 0px 10px;"><img src="/images/checkout/dotted-div.png" border="0" /></div>
        	<div style="float:left; font-size:26px; color:#999; padding-top:6px;">Receipt</div>
		</div>
        
        <!-- processor declined box -->
        <div style="float:left; width:700px; border:1px solid #fe0000; margin:20px 0px 20px 130px; background-color:#ffeae9; border-radius:5px; padding:10px;">
	        <div style="float:left; margin:10px 0px 0px 10px;"><img src="/images/cart/icon-warning.png" border="0" /></div>
	        <div style="float:left; margin-left:20px; color:#fe0000; font-size:13px;">
            	<div><strong>Payment Validation failed: Processor Declined</strong></div>
                <ul style="color:#fe0000;">
                	<li>Please re-enter credit card information.</li>
                	<li>Check to see if billing address is correct.</li>
                	<li><i>Tip: You may try another credit card or <span style="text-decoration:underline; cursor:pointer;" onclick="javascript:document.PaypalForm.submit()">pay with Paypal</span></i></li>
                </ul>
            </div>
        </div>
        <!--// processor declined box -->

		<div style="float:left; width:100%;">
        	<div style="float:left; width:100%; padding-bottom:5px; border-bottom:1px solid #ccc;">
	            <div style="float:left; width:70px; padding-top:5px;"><img src="/images/checkout/icon-lock.png" width="50" height="49" border="0" /></div>
	            <div style="float:left; width:910px;">
                    <div style="float:left; width:100%; text-align:left; font-size:28px; font-weight:bold; color:#222;">
                        Secure Credit Card Payment
                    </div>
                    <div style="float:left; width:100%; text-align:left; font-weight:bold; color:#222; font-size:14px;">
                    	<div style="float:left;">This is a secure 128-bit SSL encrypted payment.</div>
                    	<div style="float:left; padding-left:200px;"><span style="font-style:italic; font-weight:normal; font-size:14px; color:#999;">*Required Field</span></div>
                    </div>
                </div>
            </div>
            
        	<!-- left -->
			<div style="float:left; width:630px;">
            	<!-- payment -->
		        <div style="float:left; width:628px; border:1px solid #ccc; background-color:#f4f4f4; border-radius:4px; margin-top:20px; padding:10px 0px 10px 0px;">
				    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Cardholder Name:</div>
                        <div style="float:left; width:190px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                            <input name="cc_cardOwner" type="text" style="width:180px; margin:5px 0px 5px 0px; border:0px;" >
                        </div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Credit Card Number:</div>
                        <div style="float:left; width:190px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                            <input name="cardNum" type="text" maxlength="16" onblur="getCardType(this.value)"  style="width:180px; margin:5px 0px 5px 0px; border:0px;">
                        </div>
                        <div style="float:left; width:140px; text-align:right; border-radius:4px; padding-left:5px;">
                            <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_master"><img id="cc_master_img" src="/images/checkout/cc-master-off.png" border="0" /></div>
                            <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_visa"><img id="cc_visa_img" src="/images/checkout/cc-visa-off.png" border="0" /></div>
                            <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_amex"><img id="cc_amex_img" src="/images/checkout/cc-amex-off.png" border="0" /></div>
                            <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_disc"><img id="cc_disc_img" src="/images/checkout/cc-discover-off.png" border="0" /></div>
                            <input type="hidden" name="cc_cardType" value="">
                        </div>
                    </div>
                    <div style="float:left; width:100%;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                        <div style="float:left; width:190px; text-align:right; font-style:italic; color:#999; font-size:12px; text-align:left;">No spaces/dashes<br />Example: 1234123412341234</div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Expiration Date:</div>
                        <div style="float:left; width:80px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                            <select name="cc_month" style="width:70px; margin:5px 0px 5px 0px; border:0px;">
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                        <div style="float:left; width:30px; text-align:center; padding-top:5px;">/</div>
                        <div style="float:left; width:80px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                            <select name="cc_year" style="width:70px; margin:5px 0px 5px 0px; border:0px;">
                            <%
                            for countYear = 0 to 14
                                response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & right(cStr(year(date) + countYear),2) & "</option>" & vbcrlf
                            next
                            %>
                            </select>
                        </div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:10px;">*Verification Code:</div>
                        <div style="float:left; width:80px; text-align:center;  margin:7px 15px 0px 0px; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                            <input name="secCode" type="text" id="secCode" maxlength="4"  style="width:70px; margin:5px 0px 5px 0px; border:0px;">
                        </div>
                        <div style="float:left; width:85px; text-align:left;"><img src="/images/checkout/cc-verify.png" border="0" width="62" height="42" /></div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                        <div style="float:left; width:300px; text-align:right; font-style:italic; color:#333; font-size:11px; text-align:left;">
                            <span style="font-weight:bold; color:#000;">Note:</span> American Express cards show the verification code printed above and to the right of the imprinted card number on the front of the card.
                        </div>
                    </div>
                </div>
                <!--// payment-->
                
                <!-- billing information -->
			    <div style="float:left; width:100%; padding-top:30px;">
                    <div style="float:left; width:100%; ">
                        <div style="float:left; width:50px;"><img src="/images/checkout/icon-indicator.png" border="0" /></div>
                        <div style="float:left; font-size:28px; font-weight:bold; color:#333;">Billing Information</div>
                    </div>
                    <div style="float:left; width:100%; border-bottom:1px solid #ccc; padding-bottom:3px;">
                        <div style="float:left; width:50px;">&nbsp;</div>        
                        <div style="float:left; width:400px; font-size:14px; font-weight:bold; color:#333;">Please enter your billing information below.</div>
                    </div>
   					 <div style="float:left; width:100%;">
                        <div id="row_email" style="float:left; width:100%; padding-top:10px;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">Email:</div>
                            <div class="txtLabel" style="float:left; width:300px; text-align:left; padding-top:5px; color:#333; font-size:13px;"><%=email%></div>
                            <div class="txtEdit" style="float:left; width:300px; text-align:center; border:1px solid #ccc; border-radius:4px; display:none;">
                            	<input type="text" name="email" style="width:290px; margin:5px 0px 5px 0px; border:0px;" value="<%=email%>">
							</div>
                            <div style="float:left; margin:5px 0px 0px 5px; font-size:12px;">
	                            <div class="btnEdit" style="float:left; cursor:pointer; color:#305083;" onclick="onEdit('email')">[Edit]</div>
							</div>
                        </div>
                        <div id="row_fname" style="float:left; width:100%; padding-top:10px;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">First Name:</div>
                            <div class="txtLabel" style="float:left; width:300px; text-align:left; padding-top:5px; color:#333; font-size:13px;"><%=fname%></div>
                            <div class="txtEdit" style="float:left; width:300px; text-align:center; border:1px solid #ccc; border-radius:4px; display:none;">
                            	<input type="text" name="fname" style="width:290px; margin:5px 0px 5px 0px; border:0px;" value="<%=fname%>" >
							</div>
                            <div style="float:left; margin:5px 0px 0px 5px; font-size:12px;">
	                            <div class="btnEdit" style="float:left; cursor:pointer; color:#305083;" onclick="onEdit('fname')">[Edit]</div>
							</div>
                        </div> 
                        <div id="row_lname" style="float:left; width:100%; padding-top:10px;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">Last Name:</div>
                            <div class="txtLabel" style="float:left; width:300px; text-align:left; padding-top:5px; color:#333; font-size:13px;"><%=lname%></div>
                            <div class="txtEdit" style="float:left; width:300px; text-align:center; border:1px solid #ccc; border-radius:4px; display:none;">
                            	<input type="text" name="lname" style="width:290px; margin:5px 0px 5px 0px; border:0px;" value="<%=lname%>" >
							</div>
                            <div style="float:left; margin:5px 0px 0px 5px; font-size:12px;">
	                            <div class="btnEdit" style="float:left; cursor:pointer; color:#305083;" onclick="onEdit('lname')">[Edit]</div>
							</div>
                        </div>
                        <div id="row_addr" style="float:left; width:100%; padding-top:10px;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">Address:</div>
							<div class="txtLabel" style="float:left; width:300px; color:#333; font-size:13px; padding-top:5px;">
                            	<div><%=bAddress1 & " " & bAddress2%></div>
                            	<div><%=bCity%>, <%=bState & " " & bZip%></div>
                            </div>
                            <div class="txtEdit" style="float:left; width:300px; display:none;">
                                <div style="float:left; width:100%;">
                                    <div style="float:left; width:145px; text-align:center; border:1px solid #ccc; border-radius:4px;">
                                        <input type="text" name="bAddress1" style="width:140px; margin:5px 0px 5px 0px; border:0px;" value="<%=bAddress1%>" >
                                    </div>
                                    <div style="float:left; width:145px; text-align:center; border:1px solid #ccc; border-radius:4px; margin-left:5px;">
                                        <input type="text" name="bAddress2" style="width:140px; margin:5px 0px 5px 0px; border:0px;" value="<%=bAddress2%>" >
                                    </div>
                                </div>
                                <div style="float:left; width:100%; padding-top:5px;">
                                    <div style="float:left; width:100px; text-align:center; border:1px solid #ccc; border-radius:4px;">
                                        <input type="text" style="width:90px; margin:5px 0px 5px 0px; border:0px;" name="bCity" value="<%=bCity%>" >
                                    </div>
                                    <div style="float:left; width:100px; text-align:center; border:1px solid #ccc; border-radius:4px; margin-left:5px;">
                                        <select name="bState" style="width:90px; margin:5px 0px 5px 0px; border:0px;"><%getStates(bState)%></select>
                                    </div>
                                    <div style="float:left; width:80px; text-align:center; border:1px solid #ccc; border-radius:4px; margin-left:5px;">
                                        <input type="text" name="bZip" style="width:70px; margin:5px 0px 5px 0px; border:0px;" value="<%=bZip%>">
                                    </div>
                                </div>
                            </div>
                            <div style="float:left; margin:5px 0px 0px 5px; font-size:12px;">
	                            <div class="btnEdit" style="float:left; cursor:pointer; color:#305083;" onclick="onEdit('addr')">[Edit]</div>
							</div>
                        </div>
                        <div id="row_phone" style="float:left; width:100%; padding-top:10px;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">Phone:</div>
                            <div class="txtLabel" style="float:left; width:300px; text-align:left; color:#333; font-size:13px; padding-top:5px;">
                            	<%=phone%>
							</div>
                            <div class="txtEdit" style="float:left; width:300px; text-align:center; border:1px solid #ccc; border-radius:4px; display:none;">
                            	<input type="text" name="phone" style="width:290px; margin:5px 0px 5px 0px; border:0px;" value="<%=phone%>" >
							</div>
                            <div style="float:left; margin:5px 0px 0px 5px; font-size:12px;">
	                            <div class="btnEdit" style="float:left; cursor:pointer; color:#305083;" onclick="onEdit('phone')">[Edit]</div>
							</div>
                        </div>
                    </div>                    
                </div>
                <!--// billing information -->
            </div>
            <!--// left -->
            
            <!-- right -->
			<div style="float:left; width:330px; padding:20px 0px 0px 20px;">
				<div style="width:308px; border:1px solid #ccc; padding:10px; border-radius:5px;">
					<div style="float:left; width:30px; color:#305083; text-align:center;">&bull;</div>
					<div style="float:left; width:278px; color:#305083;">Rest Assured</div>
					<div style="float:left; padding:10px 0px 0px 30px; width:278px; line-height:23px; font-size:12px; color:#555;">Even though your payment was declined, you may still see a pending charge on your bank statement. Don't worry, these pending transactions will be removed from your account in the next 3-5 days and you will not be charged.</div>
                    <div style="clear:both;"></div>
                </div>
				<div style="width:308px; border:1px solid #ccc; padding:10px; border-radius:5px; margin-top:20px;">
					<div style="float:left; width:30px; color:#305083; text-align:center;">&bull;</div>
					<div style="float:left; width:278px; color:#305083;">Using A Gift Card?</div>
					<div style="float:left; padding:10px 0px 0px 30px; width:278px; line-height:23px; font-size:12px; color:#555;">Sometimes issuers such as American Express require that you register the card in order to give it a billing address.</div>
                    <div style="clear:both;"></div>
                </div>
				<div style="width:308px; border:1px solid #ccc; padding:10px; border-radius:5px; margin-top:20px;">
					<div style="float:left; width:30px; color:#305083; text-align:center;">&bull;</div>
					<div style="float:left; width:278px; color:#305083;">Happy to Help</div>
					<div style="float:left; padding:10px 0px 0px 30px; width:278px; line-height:23px; font-size:12px; color:#555;">Still having issues with your payment? Our Customer Service team is happy to help! Feel free to call us at (800) 871-6926 or message us via Live Chat for assistance!</div>
                    <div style="clear:both;"></div>
                </div>
            </div>
            <!--// right -->
        </div>

        <div style="float:left; width:100%; text-align:center; padding:30px 0px 20px 0px;">
            <div id="submitBttn" style="font-weight:bold; font-size:18px; color:#000;"><img onClick="return CheckSubmit();" src="/images/checkout/btn-order-now.png" alt="Submit Order" style="cursor: pointer;"></div>
            <input type="hidden" name="submitted" value="submitted">        
        </div>
    </div>
    </form>
	<div style="clear:both;"></div>
    <div id="mvt_trust2" style="width:100%; padding:40px 0px 20px 0px;">
        <div style="float:left; width:20%; padding:10px 20px 0px 100px;"><a href="/privacypolicy.html"><img src="/images/truste-seal-web.gif" border="0" /></a></div>
        <div style="float:left; width:19%; padding-top: 7px;"><a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=100097908&source=ctc" target="_blank"><img src="/images/BBB_clickratingsm.gif" border="0" /></a></div>
        <div style="float:left; width:13%;">
            <script type="text/javascript" src="/includes/js/j.js"></script>
            <script type="text/javascript">showMark(3);</script>
            <noscript><img id="googleChkOutImg" src="/images/sc.gif" border="0" width="72px" height="73px" /></noscript>
        </div>
        <div style="float:left; width:15%;" onclick="copyAB()"><img src="/images/Inc5000_color3.jpg" border="0" /></div>
        <div style="float:left; width:15%;"><a href="http://www.shopwiki.com/store-252661" target="_blank" title="CellularOutfitter.com is a ShopWiki Approved Store"><img src="/images/Shopwiki_Certified_EN.gif" border="0" /></a></div>
    </div>
</td>

<div id="lowPricePopup" style="display:none;">
    <div style="width:600px; background-color:#fff; text-align:center; margin-left:auto; margin-right:auto; margin-top:150px; position:relative;">
        <div style="position:absolute; top:5px; right:5px; color:#666; font-weight:bold; cursor:pointer;" onclick="closeLowPricePopup()">X</div>
        <div style="border-bottom:1px solid #ccc; background-color:#f2f2f2; height:100px;">
		    <div style="float:left; padding:15px;">
            	<img src="/images/basket/shield.png" border="0" />
            </div>
		    <div style="padding:35px 0px 0px 10px; float:left; font-size:30px; color:#333; font-weight:bold;">110% LOW PRICE GUARANTEE</div>
        </div>
        <div style="clear:both;"></div>
        <div style="width:100%; font-size:12px; color:#333;">
        	<div style="padding:25px; text-align:left; line-height:17px;">
	            CellularOutfitter.com GUARANTEES the lowest price of any major online retailer. We're so confident in our ability to provide the lowest possible price of any online retailer, that we will credit you the difference if you can find a cheaper price online.
				<br /><br />
                <span style="font-weight:bold; color:#000;">HERE's HOW IT WORKS:</span>
                <br /><br />
                If you have found a price (product + shipping + tax) lower than ours on a competing site, please email a screen shot of the checkout page to: priceguarantee@cellularoutfitter.com, or, you can fax a printout to:
				<br /><br />
                <span style="font-style:italic;">(714) 278-1932</span><br />
                <span style="font-style:italic;">attn: "CellularOutfitter.com Price Guarantee"</span>
				<br /><br />
				We will credit you the difference! This offer is effective a full 14 days after your purchase date so you can be assured you are receiving the BEST value anywhere.
				<br /><br />
				It's as simple as that! No gimmicks, no tricks. It's just another way that the team here at CellularOutfitter.com is dedicated to providing the best possible shopping experience anywhere!
                <br /><br />
                <span style="font-weight:bold; color:#000;">PLEASE NOTE: </span>Due to the volatility of the online auction marketplace, we DO NOT match pricing on auction sites such as eBay, Overstock, Half.com, Yahoo! Auctions, Amazon.com auctions, etc. The competing site and offer must be recognized by what CellularOutfitter.com deems a valid, established online U.S. merchant in good standing and shall not include micro-affiliate, gateway, non-domestic (USA) sites or any other variation thereof and of which shall be determined at the sole discretion of CellularOutfitter.com.
				<br /><Br />
                <span style="font-weight:bold; color:#000;">ALSO NOTE: </span>Our price matching guarantee applies to cellular accessories only. This guarantee does not apply to cellular phones.
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
<div id="returnPopup" style="display:none;">
    <div style="width:600px; background-color:#fff; text-align:center; margin-left:auto; margin-right:auto; margin-top:150px; position:relative;">
        <div style="position:absolute; top:5px; right:5px; color:#666; font-weight:bold; cursor:pointer;" onclick="closeReturnPopup()">X</div>
        <div style="border-bottom:1px solid #ccc; background-color:#f2f2f2; height:100px;">
		    <div style="float:left; padding:15px;">
            	<img src="/images/basket/shield.png" border="0" />
            </div>
		    <div style="padding:35px 0px 0px 10px; float:left; font-size:30px; color:#333; font-weight:bold;">90-DAY RETURNS</div>
        </div>
        <div style="clear:both;"></div>
        <div style="width:100%; font-size:12px; color:#333;">
        	<div style="padding:25px; text-align:left; line-height:17px; height:450px; overflow:auto;">
				<span style="font-weight:bold; color:#000;">Return/Warranty Policy:</span>
                <br />
				If you are not happy with the any product, you may return the item for an exchange or refund of the purchase price (minus shipping) within ninety (90) days of receipt. We also offer a ONE YEAR MANUFACTURER WARRANTY on all items. If your item is defective, for up to a full year, simply return the item to CellularOutfitter.com for a prompt exchange. 
				<br /><br />
				All returns must be accompanied by a Cellular Outfitter RMA number. To obtain an RMA number, please send an e-mail to: <a href="mailto:returns@CellularOutfitter.com">returns@CellularOutfitter.com </a>
				<br /><br />
				The information below must be included for any return:
                <br />
				&nbsp; - Order Number<br />
                &nbsp; - Date of original purchase<br />
                &nbsp; - Reason for request for RMA<br />
                &nbsp; - Your e-mail address<br />
				<br /><br />	
				No returns, refunds or exchanges will be processed without an RMA number. 
				<br /><br />
				<span style="font-weight:bold; color:#000;">CELL PHONE RETURN POLICY:</span><br />
				All return requests must be made within 7 days of receiving the item. All returned merchandise must be unregistered, in complete original manufacturer's packaging, same condition as sold, with all literature, accessories, instructions, blank warranty cards and documentation. All Phones must be returned in "Like-New Condition", show no signs of use and must have less than 25 minutes in total cumulative talk time. 
				<br /><br />
				If an item is defective it may only be exchanged with an equivalent product if we are unable to supply the same item or if the item cannot be repaired. Any return shipping cost is the responsibility of the customer. All shipping and handling fees are non-refundable. This includes all refused and unaccepted packages. 
				<br /><br />
				All NON-DEFECTIVE cell phone returns will be subject to a 20% restocking fee. 
				<br /><br />
				For refunds allow 3-4 business days for the refund to post to your account from the time the refund is issued. 
				<br /><br />
				<span style="font-weight:bold; color:#000;">Non-Defective Returns:</span><br />
				NON-DEFECTIVE RETURNS OF ACCESSORIES ARE SUBJECT TO A 15% RE-STOCKING FEE. NON-DEFECTIVE RETURNS OF PHONES ARE SUBJECT TO A 20% RE-STOCKING FEE. Such returns will be for store credit or refund at the customer's request. Refunds are only available for order within 30 days of original order date for accessories and 7 days for phones. Refunds will be issued for the current published price of the product(s) minus a 15% or 20% re-stocking fee. 
				<br /><br />
				<span style="font-weight:bold; color:#000;">NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS:</span><br /> 
				NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS MAY BE SUBJECT TO A 15% RE-STOCKING FEE and such returns will be for store credit or refund (at the customer's request). Refunds are only available within 90 days of original order date. Please contact us at <a href="mailto:returns@cellularoutfitter.com">returns@cellularoutfitter.com</a> if you have questions about which products are returnable and which products may be subject to a restocking fee. 
				<br /><br />
				Customers must contact the headset manufacturer for all defective/warranty issues on headsets that have had the packaging opened. For your convenience, you can contact us at <a href="mailto:returns@CellularOutfitter.com">returns@CellularOutfitter.com</a> for the manufacturers' contact information.
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
<div id="timesUp" style="display:none;">
	<div class="tb finalChanceBox">
    	<div class="finalChance">Here is your final chance to get your purchase at stunningly low prices!</div>
        <div class="stillInterested">Are you still interested in these items?</div>
        <div class="stillInterestedCta" onclick="resetTimer()"><img src="/images/checkout/cta.png" border="0" width="325" height="55" /></div>
        <div class="noThanks" onclick="closeTimesUp()">No thanks</div>
    </div>
</div>
<div id="welcomeBack" style="display:none;">
	<div class="tb finalChanceBox" onclick="closeTimesUp()">
    	<div class="welcomeBackLogo"><img src="/images/coLogo2.gif" border="0" width="314" height="74" /></div>
        <div class="welcomeBackTitle">Welcome Back!</div>
        <div class="welcomeBackMsg">
        	We have saved your address information from a previous order. 
            Please review the information and make any required updates.
        </div>
        <div class="noThanks" onclick="closeTimesUp()">Click to Continue</div>
    </div>
</div>
<div id="customerInfo" style="display:none;"></div>
<form name="PaypalForm" action="/cart/process/PaypalSOAP/ReviewOrder.asp" method="post">
	<% if session("stmFreeItem") = 1 then nProdIdCount = nProdIdCount + 1 %>
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
    <input type="hidden" name="paymentAmount" value="<%=nSubTotal%>">
    <input type="hidden" name="shipZip" value="<%=sZip%>">
    <% if session("stmFreeItem") = 1 then PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & stm_freeItem & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & stm_freeItemDesc & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""0"">" & vbcrlf %>
    <%=PPhtml%>
</form>
<!--#include virtual="/includes/template/bottom_cart.asp"-->