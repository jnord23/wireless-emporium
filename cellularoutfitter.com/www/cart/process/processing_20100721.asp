<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
dim HTTP_REFERER
HTTP_REFERER = request.servervariables("HTTP_REFERER")
if HTTP_REFERER <> "https://www.cellularoutfitter.com/cart/checkout.asp" and HTTP_REFERER <> "http://staging.cellularoutfitter.com/cart/checkout.asp" then
	response.write "<h3>This page will only accept forms submitted from the Cellular Outfitter secure website.</h3>" & vbcrlf
	response.end
end if

dim PaymentType
dim sPromoCode, nPromo, nAvsStreet, nAvsZip
dim nOrderTotal, nCATax, nSubTotal, nShipType, nShipRate
dim strShiptype, nShippingID
dim sEmail, sPwd, sFname, sLname, sPhone
dim sSAddy1, sSAddy2, sSCity, sSState, sSZip, sSCountry
dim sBAddy1, sBAddy2, sBCity, sBState, sBZip, sBCountry
dim sHearFrom, sEmailOpt

'nAccountId = request.form("accountid")
PaymentType = request.form("PaymentType")
sPromoCode = request.form("sPromoCode")
nPromo = request.form("nPromo")
nAvsStreet = request.form("baddress1")
nAvsStreet = replace(nAvsStreet, "&", "and")
nAvsZip = request.form("bzip")
nShippingID = request.form("sID" & request.form("radioAddress"))

nSubTotal = request.form("subTotal")
nShipRate = request.form("shippingTotal")
nShipRate = cDbl(nShipRate)
if request.form("sstate") = "CA" then
	nCATax = round(cDbl(nSubTotal) * .0875, 2)
else
	nCATax = 0
end if
nTotalQuantity = cDbl(request.form("nTotalQuantity"))

sEmail = request.form("email")
sPwd = SQLquote(request.form("pword2"))
sFname = request.form("fname")
sLname = request.form("lname")
sPhone = request.form("phone")

sSAddy1 = request.form("saddress1")
sSAddy2 = request.form("saddress2")
sSCity = request.form("scity")
sSState = request.form("sstate")
sSZip = request.form("szip")

if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
	sSCountry = "CANADA"
else
	sSCountry = ""
end if
nShipType = request.form("shiptype")

select case nShipType
	case "2"
		if sSCountry = "CANADA" then
			strShiptype = "USPS Priority Int'l"
			'nShipRate = 11.96 + (2.99 * nTotalQuantity)
		else
			strShiptype = "USPS Priority"
			'nShipRate = 7.96 + (1.99 * nTotalQuantity)
		end if
	case "3"
		if sSCountry = "CANADA" then
			strShiptype = "USPS Priority Int'l"
			'nShipRate = 11.96 + (2.99 * nTotalQuantity)
		else
			strShiptype = "USPS Express"
			'nShipRate = 20.96 + (1.99 * nTotalQuantity)
		end if
	case "4"
		if sSCountry = "CANADA" then
			strShiptype = "First Class Int'l"
			'nShipRate = 4.96 + (2.99 * nTotalQuantity)
		else
			strShiptype = "First Class"
			'nShipRate = 3.96 + (1.99 * nTotalQuantity)
		end if
	case "5"
		if sSCountry = "CANADA" then
			strShiptype = "USPS Priority Int'l"
			'nShipRate = 11.96 + (2.99 * nTotalQuantity)
		else
			strShiptype = "USPS Priority"
			'nShipRate = 7.96 + (1.99 * nTotalQuantity)
		end if
	case "0"
		if sSCountry = "CANADA" then
			strShiptype = "First Class Int'l"
			'nShipRate = 4.96 + (2.99 * nTotalQuantity)
		else
			strShiptype = "First Class"
			'nShipRate = 3.96 + (1.99 * nTotalQuantity)
		end if
	case "7" : strShiptype = "UPS Ground"
	case "8" : strShiptype = "UPS 3 Day Select"
	case "9" : strShiptype = "UPS 2nd Day Air"
end select

'BuySafe values
dim ShoppingCartId, WantsBondValue, buysafeamount
ShoppingCartId = request.form("buysafecartID")
WantsBondValue = request.form("WantsBondField")
if request.form("buysafeamount") = "" then
	buysafeamount = 0
else
	buysafeamount = cDbl(request.form("buysafeamount"))
end if

nSubTotal = formatNumber(nSubTotal,2)
nOrderTotal = nSubTotal + nCATax + nShipRate + buysafeamount
nOrderTotal = formatNumber(nOrderTotal,2)

sBAddy1 = request.form("baddress1")
sBAddy2 = request.form("baddress2")
sBCity = request.form("bcity")
sBState = request.form("bstate")
sBZip = request.form("bzip")
sBCountry = request.form("bcountry")

sEmailOpt = request.form("chkOptMail")

'this is an insert new account
call fOpenConn()
SQL = "SET NOCOUNT ON; "
SQL = SQL & "INSERT INTO CO_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered) VALUES ("
SQL = SQL & "'" & SQLquote(sFname) & "', "
SQL = SQL & "'" & SQLquote(sLname) & "', "
SQL = SQL & "'" & SQLquote(sBAddy1) & "', "
SQL = SQL & "'" & SQLquote(sBAddy2) & "', "
SQL = SQL & "'" & SQLquote(sBCity) & "', "
SQL = SQL & "'" & SQLquote(sBState) & "', "
SQL = SQL & "'" & SQLquote(sBZip) & "', "
SQL = SQL & "'" & SQLquote(sBCountry) & "', "
SQL = SQL & "'" & SQLquote(sEmail) & "', "
SQL = SQL & "'" & SQLquote(sPhone) & "', "
SQL = SQL & "'" & SQLquote(sSAddy1) & "', "
SQL = SQL & "'" & SQLquote(sSAddy2) & "', "
SQL = SQL & "'" & SQLquote(sSCity) & "', "
SQL = SQL & "'" & SQLquote(sSState) & "', "
SQL = SQL & "'" & SQLquote(sSZip) & "', "
SQL = SQL & "'" & SQLquote(sSCountry) & "', "
SQL = SQL & "'" & now & "'); "
SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
SQL = SQL & "SET NOCOUNT OFF;"
session("errorSQL") = SQL
'response.write "<p>" & SQL & "</p>" & vbcrlf
dim nAccountId
set RS = oConn.execute(SQL)
if not RS.eof then nAccountId = RS("newAccountID")

oConn.execute ("sp_ModifyEmailOpt_CO '" & SQLquote(sFname) & "','" & sEmail & "','" & sEmailOpt & "'")
call fCloseConn()

dim ssl_ProdIdCount
ssl_ProdIdCount = request.form("ssl_ProdIdCount")
if ssl_ProdIdCount = "0" or ssl_ProdIdCount = "" or not isNumeric(ssl_ProdIdCount) then
	'basket currently empty
	response.redirect("http://www.cellularoutfitter.com/sessionexpired.asp")
	response.end
else
	ssl_ProdIdCount = cDbl(ssl_ProdIdCount)
end if

if PaymentType = "eBillMe" then
	%>
	<html><body>
	<form name="eBillmeForm" action="https://www.cellularoutfitter.com/cart/process/eBillMe/eBillMe.asp" method="post">
	<!--<form name="eBillmeForm" action="/cart/process/eBillMe/eBillMe.asp" method="post">-->
		<input type="hidden" name="amount" value="<%=nsubTotal%>">
		<input type="hidden" name="nAmount" value="<%=nOrderTotal%>">
		<input type="hidden" name="nAccountId" value="<%=nAccountId%>">
		<input type="hidden" name="shipping" value="<%=nShipRate%>">
		<input type="hidden" name="shiptype" value="<%=strShiptype%>">
		<input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
		<input type="hidden" name="nCouponid" value="<%=nPromo%>">
		<input type="hidden" name="tax" value="<%=nCATax%>">
		<input type="hidden" name="buysafeamount" value="<%=buysafeamount%>">
		<input type="hidden" name="ShippingAddressID" value="<%=nShippingID%>">
		<%
		for a = 1 to ssl_ProdIdCount
			%>
			<input type="hidden" name="ssl_item_number_<%=a%>" value="<%=request.form("ssl_item_number_" & a)%>">
			<input type="hidden" name="ssl_item_qty_<%=a%>" value="<%=request.form("ssl_item_qty_" & a)%>">
			<%
		next
		%>
		<input type="hidden" name="myBasketXML" value="<%=request.form("myBasketXML")%>">
		<input type="hidden" name="ssl_ProdIdCount" value="<%=ssl_ProdIdCount%>">
	</form>
	<script language="javascript">
		document.eBillmeForm.submit();
	</script>
	</body></html>
	<%
	response.end
end if

'WRITE THE ORDER IN THE DATABASE
dim nOrderID
nOrderId = fWriteBasketItemIntoDB(nAccountId,nSubTotal,nShipRate,nOrderTotal,strShiptype,nCATax,nShippingID,nPromo)

' Add new shoppingcart
dim strItems, CellPhoneInOrder, oRsCheckType
strItems = ""
CellPhoneInOrder = 0
call fOpenConn()
for a = 1 to ssl_ProdIdCount
	set oRsCheckType = oConn.execute("SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "'")
	if not oRsCheckType.eof then
		if oRsCheckType("typeID") = 16 then CellPhoneInOrder = 1
		strItems = strItems & request.form("ssl_item_number_" & a) & ","
	end if
	oRsCheckType.close
	set oRsCheckType = nothing
next
call fCloseConn()
strItems = left(strItems,len(strItems)-1)
' end add new shoppingcart

dim result_message, cvv2_response, avs_response, txn_id, txn_time, approval_code, transaction_type, sCVV2

dim sCardOwner, sCardType, sCardNum
sCardOwner = request.form("cc_cardOwner")
sCardOwner = replace(sCardOwner, Chr(34), " ")
sCardOwner = replace(sCardOwner, Chr(39), " ")
sCardType = request.form("cc_cardType")
sCardNum = request.form("cardNum")
sCardNum = replace(sCardNum, Chr(34), "")
sCardNum = replace(sCardNum, Chr(39), "")
sCVV2 = request.form("secCode")

dim sCC_Month, sCC_Year, sExp
sCC_Month = request.form("cc_month")
sCC_Year = request.form("cc_year")
sExp = sCC_Month & sCC_Year
sCC_Year1 = "20" & sCC_Year

'Start PGP encryption
strDetails = "fname" & chr(255) & sFname & chr(255) & "lname" & chr(255) & sLname & chr(255) & "addr1" & chr(255) & sBAddy1 & chr(255) & "addr2" & chr(255) & sBAddy2 & chr(255) & "city" & chr(255) & sBCity & chr(255) & "state" & chr(255) & sBState & chr(255) & "pcode" & chr(255) & sBZip & chr(255) & "cctype" & chr(255) & sCardType & chr(255) & "ccnumber" & chr(255) & sCardNum & chr(255) & "ccexpmonth" & chr(255) & sCC_Month & chr(255) & "ccexpyear" & chr(255) & sCC_Year1 & chr(255) & "cref" & chr(255) & nOrderID
myReadFile = server.mappath("webloyalty.txt")
set filesys = CreateObject("Scripting.FileSystemObject")
set readfile = filesys.OpenTextFile(myReadFile, 1)
do while readfile.AtEndOfStream <> true
	thePassword = thePassword & readfile.readline & vbCrLf
loop
readfile.Close()
set objCryptocx = Server.CreateObject("EasyByte_Software.Cryptocxv6")
myWorkingFolder = server.mappath("CryptocxTemp")
objCryptocx.WorkingFolder = myWorkingFolder
objCryptocx.LicenseKey = "IzkARIJ8LabUEpFZXZlYwnro1gWDQCQVLTQwzPBgwghpHzHlWUuH5ndrytnqn54nU8ac8QH0PhFBCnSEjvUShg=="
objCryptocx.InputText = strDetails
objCryptocx.PublicKey = thePassword
strEnctdata = objCryptocx.PGPEncryptString
set objCryptocx = nothing
strEnctdata = SQLquote(left(strEnctdata,inStr(strEnctdata,"-----END PGP MESSAGE-----")+24))
'End PGP encryption

call fOpenConn()
strSql = "INSERT INTO we_ccinfo (accountid,orderid,CCtype,webloy) VALUES ('" & nAccountID & "','" & nOrderID & "','" & sCardType & "','" & strEnctdata & "')"
'session("errorSQL") = strSql
oConn.execute strSql
call fCloseConn()

'Staring XCharge
set XCharge1 = Server.CreateObject("USAePayXChargeCom2.XChargeCom2")
'XCharge1.Command = authonly
XCharge1.Command = 3  '8/19/2009 change to 3
XCharge1.Sourcekey = "g0bJz2mnr55FBe9Iv1z5I7R7hT418jab"
XCharge1.Pin = "bears1986"
Xcharge1.ignoreDuplicate = true
XCharge1.IP = Request.ServerVariables("REMOTE_ADDR")

if StrComp(sCardNum,"4000100011112224") = 0 then
	XCharge1.Testmode = true			'yyy Test mode
elseif StrComp(sCardNum,"4000100011112222") = 0 then
	XCharge1.Testmode = true			'NYZ Test mode
elseif StrComp(sCardNum,"5424180279791740") = 0 then
	XCharge1.Testmode = true			'negative AVS Test mode
elseif StrComp(sCardNum,"371122223332241") = 0 then
	XCharge1.Testmode = true			'Test mode
elseif StrComp(sCardNum,"4000200011112226") = 0 then
	XCharge1.Testmode = true			'N/A Test mode
elseif StrComp(sCardNum,"") = 0 then
	XCharge1.Testmode = true			'Test mode
elseif StrComp(sCardNum,"6011222233332273") = 0 then
	XCharge1.Testmode = true			'Test mode
elseif StrComp(sCardNum,"4000600011112223") = 0 then
	XCharge1.Testmode = true			'Test mode
else
	XCharge1.Testmode = false			'production mode
end if

if XCharge1.Testmode = true then
	if SQLquote(sBAddy1) <> "4040 N. Palm St." then
		response.redirect("http://www.cellularoutfitter.com/")
		response.end
	end if
end if

'=================XCharge Prepare
XCharge1.Card = sCardNum										'Card number, no dashes, no spaces
XCharge1.Exp = sExp												'Expiration date 4 digits
XCharge1.Shipping = nShipRate									'Shipping charges.
' XCharge1.Discount =											'The amount of any discounts that were applied.
XCharge1.Tax = nCATax
XCharge1.Subtotal = nSubTotal + buysafeamount
XCharge1.Amount = nOrderTotal									'Charge amount in dollars
XCharge1.Invoice = nOrderId										'Invoice number. Must be unique.
XCharge1.OrderID = nOrderId
XCharge1.TransHolderName = sCardOwner							'Name of card holder
XCharge1.Street = sBAddy1 & " " & sBAddy2						'Street address
XCharge1.Zip = sBZip											'Zip code
XCharge1.Description = "CellularOutfitter.com Online Order"		'Description of charge
XCharge1.CVV2 = sCVV2											'CVV2 code
XCharge1.Email = SQLquote(sEmail)

' card Auth  by UCAF, not sure we have purchase this service
XCharge1.CardAuth = true

'Billing Address
XCharge1.BillFname = SQLquote(sFname)
XCharge1.BillLNAme = SQLquote(sLname)
XCharge1.BillStreet = SQLquote(sBAddy1)
XCharge1.Billstreet2 = SQLquote(sBAddy2)
XCharge1.BillCity = SQLquote(sBCity)
XCharge1.BillState = SQLquote(sBState)
XCharge1.BillZip = SQLquote(sBZip)
XCharge1.BillCountry = SQLquote(sBCountry)
XCharge1.BillPhone = SQLquote(sPhone)

'Shipping Address
XCharge1.ShipFNAme = SQLquote(sFname)
XCharge1.ShipLName = SQLquote(sLname)
XCharge1.ShipStreet = sSAddy1
XCharge1.ShipStreet2 = SQLquote(sSAddy2)
XCharge1.ShipCity = SQLquote(sSCity)
XCharge1.ShipState = SQLquote(sSState) 
XCharge1.ShipZip = SQLquote(sSZip)
XCharge1.ShipCountry = SQLquote(sSCountry)
XCharge1.ShipPhone = SQLquote(sPhone)

' Response.Flush
XCharge1.Process

Dim sResults
sResults = XCharge1.ResponseStatus '==== production
'sResults= 0 '========================= testing mode
txn_id = XCharge1.ResponseReferenceNum
nAvscode = XCharge1.ResponseAVSCODE
nCvv2code = XCharge1.ResponseCVV2CODE
sRESPMSG = XCharge1.ResponseError
'================ End of XCharge Prepare

dim blnAVSSuccess
MaxAmtCheck = 100
if sResults = 0 then
	if (nAvscode="YNA" or nAvscode="NYW" or nAvscode="XXW" or nAvscode="XXU" or nAvscode="XXR" or nAvscode="XXS" or nAvscode="XXE" or nAvscode="XXG" or nAvscode="YYG" or nAvscode="YGG" or nAvsCode = "") and nSubTotal > MaxAmtCheck  then
		blnDoVoid = true
	elseif (nAvscode="A" or nAvscode="YNY" or nAvscode="W" or nAvscode="R" or nAvscode="U" or nAvscode="E" or nAvscode="S" or nAvsCode = "") and nSubTotal > MaxAmtCheck then
		blnDoVoid = true	
	elseif nAvscode="NNN" or nAvscode="N" or nAvscode="NN" or nAvscode="GCI" or nAvscode="B" or nAvscode="M" or nAvscode="P" or nAvscode="" then  'zip code no match, address no match do void
		blnDoVoid = true
	end if
	if blnDoVoid = true then
		strNotes = "AVS Verification failed For Order Id: " & nOrderID & ", Transaction Id : " & txn_id & " on " & Date() & " at around " & Time() & ". "
		Set XCharge1 = Server.CreateObject("USAePayXChargeCom2.XChargeCom2")
			XCharge1.Command = 2  'cvoid
			XCharge1.Sourcekey ="g0bJz2mnr55FBe9Iv1z5I7R7hT418jab"
			XCharge1.Pin="bears1986"
			requestID = generateRequestID()
			XCharge1.RefNum=txn_id
			XCharge1.Process
			
			Dim voidResults
			voidResults= XCharge1.ResponseStatus '==== production
			'Response.Write("<br>voidResults:"&voidResults&"<br>")
				if Cstr(Trim("" & voidResults)) = "0" then
					'SUCCESSFULLY CREATED DELAYED TRANSACTION CAPTURING
					VoidTransaction = "<BR>Successfully created delayed capture transaction entry with ID : " & TransactId & "."
					blnVoidSuccess = true	'SUCCESSFULLY VOIDED TRANSACTION
					CDOSend "sales@cellularoutfitter.com","webmaster@cellularoutfitter.com","Successfully Created Void Transaction For Order Id  SENT EMAIL1: " & nOrderID, strNotes & VoidTransaction
				else
					'VOID TRANSACTION CREATION FAILED.
					VoidTransaction = "<BR>Failed to create Void Transactions entry With response->" & transResponse & ". <BR>Please make sure administrator voids transaction entry having PNREF (i.e. TRANSID) as "  &  TransactId & "   manually as soon as possible."
					CDOSend "sales@cellularoutfitter.com","webmaster@cellularoutfitter.com","Successfully Created Void Transaction For Order Id  SENT EMAIL1: " & nOrderID, strNotes & VoidTransaction
				end if
		call fOpenConn()
		strNotes = Ucase(strNotes)
		strNotes = replace(strNotes,"<B>","")
		strNotes = replace(strNotes,"</B>","")
		strNotes = replace(strNotes,"<BR>","")
		strNotes = replace(strNotes,"<U>","")
		strNotes = replace(strNotes,"</U>","")
		strNotes = replace(strNotes,"<B>","")
		strNotes = replace(strNotes,"<B>","")
		strNotes = replace(strNotes,"<B>","")
		oConn.execute "UPDATE we_ordernotes SET ordernotes='" & replace(strNotes,"'","''") & "' WHERE OrderId='" & nOrderID & "'"
		if nAvscode="NNN" then
			oConn.execute "DELETE FROM we_orders WHERE orderid=" & nOrderID
			oConn.execute "DELETE FROM we_orderdetails WHERE orderid=" & nOrderID
		end if
		response.write("<html><head><link rel=stylesheet href='https://www.cellularoutfitter.com/includes/css/styleCO.css' type=text/css></head><body bgcolor=#ffffff text=#000000 leftmargin=0 topmargin=0 marginwidth='0' marginheight='0' link=#666666 vlink=#666666 alink=#ff6600><br><br><TABLE cellSpacing=1 cellPadding=1 width='75%' border=1  ALIGN=CENTER class=smlText><TR><TD align=middle><IMG src='https://www.cellularoutfitter.com/images/CellularOutfitter.jpg'></TD></TR>")
		response.write("<TR><TD><STRONG><FONT COLOR=red>ORDER PROCESSING FAILED!!!<BR></FONT></STRONG><BR>Your Purchase order is cancelled because <STRONG>AVS verification failed</STRONG>. <BR>Either Zip code or Street Address must match with your details filed with the credit card issuing bank and&nbsp;it must &nbsp;be issued by any bank&nbsp;in United States.<BR>The Street And Zip Code provided by you for the transaction are as follows:&nbsp; <BR></TD></TR>")
		response.write("<TR><TD><STRONG>STREET :" & nAvsStreet & "<BR>ZIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " & nAvsZip & "&nbsp;&nbsp;</STRONG></TD></TR>")
		response.write("<TR><TD><STRONG><BR>Please make the required modifications in the Billing Address Details - Billing Address line 1 and&nbsp;Billing Zip Code by <A href='/register/default.asp?pf=modify&basket=true'><STRONG>Clicking Here</STRONG></A>&nbsp;and then resubmit your order for processing.<BR>If you continue to experience difficulties, please contact us at (888) 725-7575 to complete your order.<br></STRONG></TD></TR>")
		response.write("</TABLE></P></B></body></HTML>")
		call fCloseConn()
	else ' no do void ' avsSucess
		'response.Write("<br>blnAVSSuccess:"&blnAVSSuccess)
		sQueryString = "&ssl_result_message=" & Server.URLencode(sRESPMSG) &_
		"&cc_cardOwner=" & Server.URLencode(sCardOwner) &_
		"&cc_cardType=" & Server.URLencode(sCardType) &_
		"&ssl_cvv2_response=" & Server.URLencode(cvv2_response) &_
		"&ssl_avs_response=" & Server.URLencode(sResults) &_
		"&ssl_avs_response_Verisign=" & Server.URLencode(IAVS) &_
		"&ssl_avs_ADDR=" & Server.URLencode(AVSADDR) &_
		"&ssl_avs_ZIP=" & Server.URLencode(AVSZIP) &_
		"&ssl_txn_id=" & Server.URLencode(txn_id) &_
		"&ssl_txn_time=" & Server.URLencode(sResults) &_
		"&ssl_approval_code=" & Server.URLencode(approval_code) &_
		"&ssl_transaction_type=" & Server.URLencode(sResults) &_
		"&ssl_customer_code=" & Server.URLencode(nAccountID) &_
		"&ssl_invoice_number=" & Server.URLencode(nOrderID) &_
		"&ssl_promo_code=" & Server.URLencode(sPromoCode) &_
		"&ssl_amount=" & Server.URLencode(nSubTotal) &_
		"&ssl_ordamount=" & Server.URLencode(nOrderTotal) &_
		"&ssl_shippingid=" & Server.URLencode(nShippingID) &_
		"&ctxl="& sQueryTestString
		call fOpenConn()
		dim thisItemID, thisQuantity
		SQL = "SELECT A.itemID, A.quantity, B.ItemKit_NEW FROM we_orderdetails A INNER JOIN we_items B ON A.itemID=B.itemID WHERE A.orderID = '" & nOrderID & "'"
		session("errorSQL") = SQL
		set orderdetailRS = oConn.execute(SQL)
		while not orderdetailRS.EOF
			thisItemID = orderdetailRS("itemID")
			thisQuantity = orderdetailRS("quantity")
			
			if isNull(orderdetailRS("ItemKit_NEW")) then
				SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & thisQuantity & " WHERE itemID = '" & thisItemID & "'"
			else
				SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & thisQuantity & " WHERE itemID IN (" & orderdetailRS("ItemKit_NEW") & ")"
			end if
			session("errorSQL") = request.servervariables("PATH_INFO") & ":<br>" & SQL
			oConn.execute SQL
			orderdetailRS.movenext
		wend
		
		' update ShoppingCart table
		SQL = "UPDATE ShoppingCart SET purchasedOrderID = '" & nOrderID & "' WHERE store = 2 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
		SQL = SQL & " AND itemID IN (" & strItems & ")"
		session("errorSQL") = SQL
		oConn.execute SQL
		' update GiftCertificates table
		'if sGiftCert <> "" then
		'	SQL = "UPDATE GiftCertificates SET UsedOnOrderNumber = '" & nOrderID & "', dateUsed = '" & now & "' WHERE GiftCertificateCode = '" & sGiftCert & "'"
		'	oConn.execute SQL
		'end if
		
		'save BuySafe data to DB
		'if buysafeamount > 0 then
			SQL = "UPDATE we_orders SET buysafeamount = '" & buysafeamount & "', BuySafeCartID = '" & ShoppingCartId & "' WHERE orderid = '" & nOrderID & "'"
			session("errorSQL") = SQL
			oConn.execute SQL
			%>
			<!--#include virtual="/cart/buysafe/setshoppingcartcheckout.asp"-->
			<%	
		'end if
		
		call fCloseConn()
		response.redirect("https://www.cellularoutfitter.com/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal)
		'response.redirect("/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal)
		response.end
	end if
else  '  declined or Error
	response.write "<html><head><link rel=stylesheet href=""https://www.cellularoutfitter.com/includes/css/styleCO.css"" type=""text/css""></head><body bgcolor=#ffffff text=#000000 leftmargin=0 topmargin=0 marginwidth='0' marginheight='0' link=#666666 vlink=#666666 alink=#ff6600>" & vbcrlf
	%>
	<br><br>
	<table cellpadding="1" cellspacing="1" width="75%" border="1" align="center" class="mc-text">
		<tr>
			<td align=middle><img src="https://www.cellularoutfitter.com/images/CellularOutfitter.jpg" border="0"></td>
		</tr>
		<tr>
			<td>
				<strong><font color="#FF0000">ORDER PROCESSING FAILED!!!</font></strong>
				<br><br>Your Purchase order is cancelled because<br><strong>AVS verification failed</strong>.
				<br>AVS verification failed due to <%=sRESPMSG%>
				<br><br>
			</td>
		</tr>
		<tr>
			<td>
				<br>Please resubmit your order for processing.
				<br>If you continue to experience difficulties, please contact us at (888) 725-7575 to complete your order.
				<br><br>
			</td>
		</tr>
		<tr><td height="5" bgcolor="#CCCCCC"><img src="https://www.cellularoutfitter.com/images/spacer.gif" width="1" height="5" border="0"></td></tr>
		<tr>
			<td>
				<p>We apologize for the issues that you are having with this transaction. Please verify that the following information is entered correctly:</p>
				<ul>
					<li>Billing Address (your billing address must be entered exactly as it is on your credit card statement)</li>
					<li>Credit Card Number</li>
					<li>Expiration Date</li>
					<li>CVV Code (you can find this 3 digit code on the back of the card, for Amex the code can also be 4 digits and be located on the front)</li>
				</ul>
				<p>If all of this information is entered correctly please fill out the form below and we will get back to you shortly.</p>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="mc-text">
					<form action="http://www.cellularoutfitter.com/DeclineProcess.asp" method="post">
					<tr>
						<td valign="top">Customer Name on Order:</p></td>
						<td valign="top"><p><input type="text" name="CustomerName" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Email address associated with order:</p></td>
						<td valign="top"><p><input type="text" name="EmailAddress" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Contact phone number:</p></td>
						<td valign="top"><p><input type="text" name="ContactPhone" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Error code given:</p></td>
						<td valign="top"><p><input type="text" name="ErrorCode" size="80" value="<%=sRESPMSG%>"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Additional comments/concerns:</p></td>
						<td valign="top"><p><textarea name="AdditionalComments" cols="65" rows="3"></textarea></p></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><p><input type="submit" name="submitted" value="Send Form"></p></td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	<%
	response.write "</body></html>" & vbcrlf
	dim strSql,rsFraud,InValidCount,DiffLastInvalid
	InValidCount = 0
	call fOpenConn()
	set rsFraud = Server.CreateObject("Adodb.Recordset")
	strSql = "SELECT isnull(InValidCount,0),isnull(LastInvalid,getDate()) FROM CO_accounts WHERE accountId='" & nAccountId & "'"
	session("errorSQL") = request.servervariables("PATH_INFO") & ":<br>" & strSql
	rsFraud.Open strSql, oConn
	if not rsFraud.EOF then
		InvalidCount = rsFraud(0) + 1
		DiffLastInvalid = DateDiff("n",Now(),rsFraud(1))
	end if
	' "or rsFraud.EOF" clause added by MC 12/15/2006 to avoid error message and also block bots from accessing this page directly
	if (DiffLastInvalid <= 10 and InvalidCount >= 3) or rsFraud.eof then
		strSql = "UPDATE CO_accounts SET IsBlocked='yes',InValidCount=0,LastInvalid='" & now() & "',BlockedIp='" & request.ServerVariables("remote_addr") & "' WHERE accountid='" & nAccountId & "'"
		'oConn.execute strSql
		session.abandon
		rsFraud.close
		set rsFraud = nothing
		call fCloseConn()
		response.redirect("Blocked.asp")
	else
		strSql = "UPDATE CO_accounts SET InValidCount=" & InvalidCount & ",LastInvalid='" & now() & "',BlockedIp='" & request.ServerVariables("remote_addr") & "' WHERE accountid='" & nAccountId & "'"
		oConn.execute strSql
		rsFraud.close
		set rsFraud = nothing
		call fCloseConn()
	end if
	
End if

'Function from inc_orderfunctions.asp
function fWriteBasketItemIntoDB(nAccountId,nSubTotal,sShipRate,nOrderTotal,sShipType,sTax,shippingid,nPromo)
	'this function will write the basket items into the database
	if ssl_ProdIdCount = 0 then
		'basket currently empty
		response.redirect("http://www.cellularoutfitter.com/sessionexpired.asp")
		response.end
	else
		call fOpenConn()
		SQL = "sp_InsertOrder3 2,'" & nAccountId & "','" & FormatNumber(nSubTotal,2) & "','" & FormatNumber(sShipRate,2) & "'," & sTax & ",'" & FormatNumber(nOrderTotal,2) & "','" & SQLquote(sShipType) & "','" & shippingid & "','" & nPromo & "','" & Now & "'"
		session("errorSQL") = SQL
		'response.write "<p>" & SQL & "</p>" & vbcrlf
		nOrderId = oConn.execute(SQL).fields(0).value
		session("invoice") = nOrderId
		'initialize basket variables
		dim sSprocString
		sSprocString = ""
		for a = 1 to ssl_ProdIdCount
			nIdProd = request.form("ssl_item_number_" & a)
			nQtyProd = request.form("ssl_item_qty_" & a)
			'get string for the insertorderdetail stored proc
			sSprocString = "sp_InsertOrderDetail '" & nOrderId & "','" & nIdProd & "','" & nQtyProd & "'"
			'stored procedure that will write each orderitem detail within the orderdetail db
			'response.write "<p>" & sSprocString & "</p>" & vbcrlf
			oConn.execute(sSprocString)
		next
		call fCloseConn()
	end if
	fWriteBasketItemIntoDB = nOrderId
end function

function generateRequestID()
	randomize
	dim strResult, n, strChars
	strChars = ""
	for a = 1 to 18
		n = Int(Rnd * 26) + 75
		if n >= 91 then
			strChars = strChars & chr(n - 43)
		else
			strChars = strChars & chr(n)
		end if
	next
	strResult = Mid(CCFormatDateTime(Now), 2, 14) & strChars
	generateRequestID = strResult
end function

function CCFormatDateTime(ByVal dtmDate)
	dim strDate, strTmp
	
	If (IsNull(dtmDate)) Then
		CCFormatDateTime = Null
		Exit Function
	End If
	
	strTmp = Trim(CStr(Month(dtmDate)))
	If (Len(strTmp) < 2) Then strTmp = "0" + strTmp
	strDate = Trim(CStr(Year(dtmDate))) & strTmp
	
	strTmp = Trim(CStr(Day(dtmDate)))
	If (Len(strTmp) < 2) Then strTmp = "0" + strTmp
	strDate = strDate & strTmp
	
	strTmp = Trim(CStr(Hour(dtmDate)))
	If (Len(strTmp) < 2) Then strTmp = "0" + strTmp
	strDate = strDate & strTmp
	
	strTmp = Trim(CStr(Minute(dtmDate)))
	If (Len(strTmp) < 2) Then strTmp = "0" + strTmp
	strDate = strDate & strTmp
	
	strTmp = Trim(CStr(Second(dtmDate)))
	If (Len(strTmp) < 2) Then strTmp = "0" + strTmp
	strDate = strDate & strTmp
	
	CCFormatDateTime = "A" & strDate & ".000"
end function

function XMLencode(str)
	XMLencode = replace(replace(replace(replace(replace(str,"&","&amp;"),"<","&lt;"),">","&gt;"),"'","&apos;"),chr(34),"&quot;")
end function
%>
