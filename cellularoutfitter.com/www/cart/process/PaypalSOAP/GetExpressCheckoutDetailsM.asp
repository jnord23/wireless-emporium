<%
securePage = 1
response.buffer = true
SEtitle = "Cheap Cell Phone Accessories ?Cellular Phone Accessories ?CellularOutfitter.com"
SEdescription = "Buy cheap Cell Phone Accessories at Wholesale Prices to the Public.  Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more.  Cellular Accessories at the Lowest Prices Online Guaranteed."
SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"

strBody = "<body leftmargin=""0"" topmargin=""0"" onload=""changeTax(document.frmProcessOrder.sState.value);"">"

pageName = "Checkout"	' to use checkout.css on mobile
%>

<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/template/topCO.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/process/PaypalSOAP/Constants.asp"-->
<!--#include virtual="/cart/includes/inc_getStates.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->

<%
dim token, PayerID, SoapStr
token = request.querystring("token")
PayerID = request.querystring("PayerID")
if token = "" or payerID = "" then
	response.redirect "PaypalError.asp"
	response.end
end if

'BUILD CALL DATA
SoapStr = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & vbcrlf
SoapStr = SoapStr & "<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbcrlf
SoapStr = SoapStr & "	<soap:Header>" & vbcrlf
SoapStr = SoapStr & "		<RequesterCredentials xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<Credentials xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
SoapStr = SoapStr & "				<Username>" & API_USERNAME & "</Username>" & vbcrlf
SoapStr = SoapStr & "				<Password>" & API_PASSWORD & "</Password>" & vbcrlf
SoapStr = SoapStr & "				<Signature>" & API_SIGNATURE & "</Signature>" & vbcrlf
SoapStr = SoapStr & "			</Credentials>" & vbcrlf
SoapStr = SoapStr & "		</RequesterCredentials>" & vbcrlf
SoapStr = SoapStr & "	</soap:Header>" & vbcrlf
SoapStr = SoapStr & "	<soap:Body>" & vbcrlf
SoapStr = SoapStr & "		<GetExpressCheckoutDetailsReq xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<GetExpressCheckoutDetailsRequest>" & vbcrlf
SoapStr = SoapStr & "				<Version xmlns=""urn:ebay:apis:eBLBaseComponents"">53.0</Version>" & vbcrlf
SoapStr = SoapStr & "				<Token>" & token & "</Token>" & vbcrlf
SoapStr = SoapStr & "			</GetExpressCheckoutDetailsRequest>" & vbcrlf
SoapStr = SoapStr & "		</GetExpressCheckoutDetailsReq>" & vbcrlf
SoapStr = SoapStr & "	</soap:Body>" & vbcrlf
SoapStr = SoapStr & "</soap:Envelope>" & vbcrlf

'CREATE OBJECTS
dim objXMLDOC, objXMLDOM, oNode, strError
set objXMLDOC = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
set objXMLDOM = Server.CreateObject("Msxml2.DomDocument")
set oNode = Server.CreateObject("Microsoft.XMLDOM")

'SET CALL TIMEOUTS
dim lResolve, lConnect, lSend, lReceive
lResolve = 30000 'Timeout values are in milli-seconds
lConnect = 30000
lSend = 30000
lReceive = 30000
objXMLDOC.setTimeouts lResolve, lConnect, lSend, lReceive

'MAKE THE CALL
objXMLDOC.open "POST", API_ENDPOINT, False
objXMLDOC.setRequestHeader "Content-Type", "text/xml"
objXMLDOC.send(SoapStr)
objXMLDOM.async = false
objXMLDOM.LoadXML objXMLDOC.responseText
'response.write server.HTMLEncode(objXMLDOC.responseText)
'response.end
'PROCESS THE CALL
if objXMLDOM.parseError.errorCode <> 0 then
	strError = strError & "<h3>Parser error found.</h3>"
else
	'BEGIN CHECK FOR ERROR FROM PAYPAL
	set oNode = objXMLDOM.getElementsByTagName("Ack")
	if (not oNode is nothing) then
		session("errorSQL") = objXMLDOC.responseText
		if oNode.item(0).text = "Failure" then
			set oNode = objXMLDOM.getElementsByTagName("ShortMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("LongMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("ErrorCode")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
		end if
	else
		strError = strError & "<h3>No data received.</h3>"
	end if
end if

if strError <> "" then
	response.write strError
	response.end
else
	'PROCESS SUCCESSFUL CALL
	dim ShipToAddressName, ShipToAddressStreet1, ShipToAddressStreet2, ShipToAddressCityName, ShipToAddressStateOrProvince, ShipToAddressPostalCode
	dim ShipToAddressCountry, ShipToAddressPhone
	set oNode = objXMLDOM.getElementsByTagName("Token")
	if (not oNode is nothing) then
		Token = oNode.item(0).text
	end if
	
	dim nCATax, nItemTotal
	nCATax = 0
	nItemTotal = cdbl(0)
	nDiscountTotal = cdbl(0)
	
	dim ack
	ack = Ucase(objXMLDOM.getElementsByTagName("Ack").item(0).text)
	if ack = "SUCCESS" then
		paymentAmount = objXMLDOM.getElementsByTagName("PaymentDetails/ItemTotal").item(0).text
		if paymentAmount = "" then
			call responseRedirect("PaypalError.asp")
		end if
		
		dim nTotalQuantity, sWeight, sZip
		customdata = objXMLDOM.getElementsByTagName("GetExpressCheckoutDetailsResponseDetails/Custom").item(0).text
		myarray = split(customdata,",")
		ItemsAndWeight = myarray(0)
		BuySafeAmount = myarray(1)
		BuySafeCartID = myarray(2)
		WantsBondField = myarray(3)
		promoIncluded = myarray(4)

		itemsArray = split(ItemsAndWeight,"|")
		for a = 0 to uBound(itemsArray)
			select case a
				case 0 : nTotalQuantity = cDbl(itemsArray(a))
				case 1 : sWeight = cDbl(itemsArray(a))
				case 2 : sZip = cStr(itemsArray(a))
			end select
		next

		set ItemDetails = objXMLDOM.documentElement.getElementsByTagName("PaymentDetailsItem")
		for basketitem = 0 to ItemDetails.Length - 1
			set ItemDetailsNodes = ItemDetails.item(basketitem).childNodes
			itemPrice = cdbl(ItemDetailsNodes(4).text)
			itemQty = cdbl(ItemDetailsNodes(2).text)
			if itemPrice >= 0 then
				nItemTotal = nItemTotal + (itemPrice*itemQty)
			else
				nDiscountTotal = nDiscountTotal + (itemPrice*itemQty)
			end if
		next
				
		dim TAXAMT, strShiptype, shiptype, nShipRate, doNotProcess, newOrderID
		
		dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
		if promoIncluded = 1 then shipQty = nTotalQuantity - 1 else shipQty = nTotalQuantity

		shipcost0 = prepInt(4.00 + (1.99 * shipQty))
		shipcost2 = prepInt(8.00 + (1.99 * shipQty))
		shipcost3 = prepInt(21.00 + (1.99 * shipQty))
		shipcost4 = prepInt(5.00 + (2.99 * shipQty))
		shipcost5 = prepInt(12.00 + (2.99 * shipQty))
		
		dim fullName, nameArray, FIRSTNAME, LASTNAME
		fullName = objXMLDOM.getElementsByTagName("ShipToAddress/Name").item(0).text
		nameArray = split(fullName," ")
		FIRSTNAME = nameArray(0)
		LASTNAME = replace(fullName,FIRSTNAME & " ","")
		
		dim SHIPTOSTREET, SHIPTOSTREET2, SHIPTOCITY, sState, SHIPTOZIP, SHIPTOCOUNTRYCODE, PHONENUM, EMAIL
		SHIPTOSTREET = objXMLDOM.getElementsByTagName("ShipToAddress/Street1").item(0).text
		SHIPTOSTREET2 = objXMLDOM.getElementsByTagName("ShipToAddress/Street2").item(0).text
		SHIPTOCITY = objXMLDOM.getElementsByTagName("ShipToAddress/CityName").item(0).text
		sState = objXMLDOM.getElementsByTagName("ShipToAddress/StateOrProvince").item(0).text
		SHIPTOZIP = objXMLDOM.getElementsByTagName("ShipToAddress/PostalCode").item(0).text
		SHIPTOCOUNTRYCODE = objXMLDOM.getElementsByTagName("ShipToAddress/Country").item(0).text
		PHONENUM = objXMLDOM.getElementsByTagName("ShipToAddress/Phone").item(0).text
		EMAIL = objXMLDOM.getElementsByTagName("Payer").item(0).text
		
		dim CorrelationID
		CorrelationID = objXMLDOM.getElementsByTagName("CorrelationID").item(0).text
				
		if len(SHIPTOCOUNTRYCODE) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sState) > 0 then SHIPTOCOUNTRYCODE = "CA"
		if inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sState) > 0 then
			strShiptype = "1"
			doNotProcess = 0
			nShipRate = shipcost4
		else
			strShiptype = ""
			doNotProcess = 0
			nShipRate = shipcost0
		end if

		if sState = "CA" then
			TAXAMT = round((nItemTotal - buysafeamount) * Application("taxMath"), 2)
		else
			TAXAMT = 0
		end if
		
		if doNotProcess = 0 then
			SQL = "SELECT * FROM we_Orders WHERE extOrderType=1 AND extOrderNumber='" & CorrelationID & "'"
			session("errorSQL") = SQL
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if RS.eof then
				' Don't do anything if an order already exists in the WE database with this Paypal Order Number
				SQL = "SET NOCOUNT ON; "
				SQL = SQL & "INSERT INTO CO_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,hearFrom,dateEntered) VALUES ("
				SQL = SQL & "'" & SQLquote(FIRSTNAME) & "', "
				SQL = SQL & "'" & SQLquote(LASTNAME) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET2) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCITY) & "', "
				SQL = SQL & "'" & sState & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOZIP) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCOUNTRYCODE) & "', "
				SQL = SQL & "'" & SQLquote(EMAIL) & "', "
				SQL = SQL & "'" & SQLquote(PHONENUM) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET2) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCITY) & "', "
				SQL = SQL & "'" & sState & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOZIP) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCOUNTRYCODE) & "', "
				SQL = SQL & "'Paypal API', "
				SQL = SQL & "'" & now & "'); "
				SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
				SQL = SQL & "SET NOCOUNT OFF;"
				session("errorSQL") = SQL
				set RS = oConn.execute(SQL)
				
				dim newAccountID
				if not RS.eof then newAccountID = RS("newAccountID")
				
				SQL = "SET NOCOUNT ON; "
				SQL = SQL & "INSERT INTO we_Orders (store,accountid,extOrderType,extOrderNumber,ordersubtotal,ordershippingfee,orderTax,ordergrandtotal,shiptype,BuySafeCartID,BuySafeAmount,orderdatetime,mobileSite) VALUES ("
				SQL = SQL & "'2', "
				SQL = SQL & "'" & newAccountID & "', "
				SQL = SQL & "'1', "
				SQL = SQL & "'" & CorrelationID & "', "
				SQL = SQL & "'" & formatNumber(nItemTotal,2) & "', "
				SQL = SQL & "'" & nShipRate & "', "
				SQL = SQL & "'" & TAXAMT & "', "
				SQL = SQL & "'" & paymentAmount & "', "
				if strShiptype = "1" then
					SQL = SQL & "'First Class Int''l', "
				else
					SQL = SQL & "'First Class', "
				end if
				SQL = SQL & "'"& BuySafeCartID &"', "
				SQL = SQL & "'"& BuySafeAmount &"', "
				SQL = SQL & "'" & now & "',1)"
				SQL = SQL & "SELECT @@IDENTITY AS newOrderID; "
				SQL = SQL & "SET NOCOUNT OFF;"
				session("errorSQL") = SQL
				set RS = oConn.execute(SQL)
				if not RS.eof then newOrderID = RS("newOrderID")
				
				set ItemDetails = objXMLDOM.documentElement.getElementsByTagName("PaymentDetailsItem")
				for basketitem = 0 to ItemDetails.Length - 1
					set ItemDetailsNodes = ItemDetails.item(basketitem).childNodes
					itemID = ItemDetailsNodes(1).text
					itemPrice = ItemDetailsNodes(4).text
					if itemID = "DISCOUNT" then
						couponcode = ItemDetailsNodes(0).text
					else
						if itemID <> "" and itemID <> "1" and itemID <> "DISCOUNT" and itemPrice >= 0 then
							SQL = "INSERT INTO we_Orderdetails (orderid,itemid,quantity,price) VALUES ("
							SQL = SQL & "'" & newOrderID & "', "
							SQL = SQL & "'" & itemID & "', "
							SQL = SQL & "'" & ItemDetailsNodes(2).text & "','" & itemPrice & "')"
							
							oConn.execute(SQL)
						end if					
					end if
				next
				
				if couponcode <> "" then
					SQL = "SELECT couponid FROM CO_coupons WHERE promoCode='" & couponcode & "'"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 0, 1
					if not RS.eof then
						dim couponid
						couponid = RS("couponid")
					end if
					SQL = "UPDATE we_Orders SET couponid='" & couponid & "' WHERE orderid='" & newOrderID & "'"
					oConn.execute(SQL)
				end if
			end if
			
			grandtotal = paymentAmount + TAXAMT + nShipRate
			%>
            <form action="/cart/process/PaypalSOAP/DoExpressCheckoutPayment.asp" method="post" name="frmProcessOrder">
                <div class="checkoutSteps">
                    <div class="centerContain">
                        <div class="fl lockImg"></div>
                        <div class="fl step1 inactiveCheckoutStep">1. Your Info</div>
                        <div class="fl step2">2. Payment</div>
                        <div class="fl step2 inactiveCheckoutStep">3. Review</div>
                    </div>
                </div>
                <div class="checkoutTitleBar" onclick="revealShippingOptions()">
                    <div class="fl">Shipping Options</div>
                    <div class="fr expandDiv">+</div>
                </div>
                <div id="shippingOptionDiv" class="shippingOptions hidden">
                    <div class="centerContain">
                        <div class="shippingOptionRow">
                            <div class="fl shippingRadio">
                                <input type="radio" name="shiptype" value="0" checked="checked" />
                                <input type="hidden" name="shipcost0" value="<%=shipcost0%>" />
                            </div>
                            <div class="fl shippingTxt">USPS First Class (4-10 business days)</div>
                        </div>
                        <div class="shippingOptionRow">
                            <div class="fl shippingRadio">
                                <input type="radio" name="shiptype" value="2" />
                                <input type="hidden" name="shipcost2" value="<%=shipcost2%>" />
                            </div>
                            <div class="fl shippingTxt">USPS Priority Mail (2-4 business days)</div>
                        </div>
                        <div class="shippingOptionRow">
                            <div class="fl shippingRadio">
                                <input type="radio" name="shiptype" value="3" />
                                <input type="hidden" name="shipcost3" value="<%=shipcost3%>" />
                            </div>
                            <div class="fl shippingTxt">USPS Express Mail (1-2 business days)</div>
                        </div>
                        <div class="applyShipping" onclick="updateShippingTotal()">Apply</div>
                        <div class="mustApply">(MUST press Apply to update total)</div>
                    </div>
                </div>
                <div class="checkoutTitleBar">Shipping Address</div>
                <div class="lightBgBordered">
                    <div class="centerContain">
                        <div class="centerContain">
                            <div class="centerContain">
                                <div class="billingAddressBox">
                                    <div class="billingAddress">
                                        <%=FIRSTNAME & " " & LASTNAME%><br />
                                        <%=SHIPTOSTREET%><br />
                                        <% if SHIPTOSTREET2 <> "" then %>
                                        <%=SHIPTOSTREET2%><br />
                                        <% end if %>
                                        <%=SHIPTOCITY & ", " & sState & " " & SHIPTOZIP%>
                                    </div>
                                    <!--
                                    <div class="changeBilling">
                                        <div class="fl"><img src="/images/mobile/icons/deleteItem.gif" border="0" /></div>
                                        <div class="fl chgBillingTxt">Change Shipping Address</div>
                                    </div>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cartItem">
                    <div class="cartSummaryTitle">CART SUMMARY</div>
                    <div class="summaryRow">
                        <div class="fl rowTitle">Cart Total:</div>
                        <div class="fr rowValue"><%=formatCurrency(nItemTotal,2)%></div>
                    </div>
                    <div class="summaryRow">
                        <div class="fl rowTitle">Tax:</div>
                        <div id="taxDiv" class="fr rowValue"><%=formatCurrency(TAXAMT,2)%></div>
                    </div>
                    <%
                    if nDiscountTotal < 0 then
                        session("promocode") = couponcode
                    %>
                    <div class="summaryRow">
                        <div class="fl rowTitle">Discount 
                        <%if couponcode <> "" then%>
                        	<%="(" & ucase(couponcode) & ")"%>
                        <%end if%>
						:
                        </div>
                        <div class="fr rowValue discountPrice"><%=formatcurrency(0-nDiscountTotal,2)%></div>
                    </div>
                    <%end if%>
                    <div id="shipSummary" class="summaryRow hidden">
                        <div class="fl rowTitle">Shipping:</div>
                        <div id="shippingCostDiv" class="fr rowValue"><%=formatCurrency(shipcost0,2)%></div>
                    </div>
                    <div id="estSummary" class="summaryRowDark">
                        <div class="fl rowTitle">Estimated Total:</div>
                        <div id="estTotalDiv" class="fr rowValueGreen"><%=formatCurrency(paymentAmount+TAXAMT,2)%></div>
                    </div>
                    <div id="grandSummary" class="summaryRowDark hidden">
                        <div class="fl rowTitle">Grand Total:</div>
                        <div id="grandTotalDiv" class="fr rowValueGreen"><%=formatCurrency(grandtotal,2)%></div>
                    </div>
                </div>
                <div class="proceedButton">
					<div id="submitBttn" style="font-weight:bold; font-size:18px; color:#000;">
                    	<input type="image" name="Proceed to Checkout" src="/images/mobile/buttons/submitPayment.gif" border="0" onclick="return(checkout1FormReview())" />
					</div>
                    
                    <input type="hidden" name="shippingTotal" value="<%=nShipRate%>">
                    <input type="hidden" name="nCATax" value="<%=TAXAMT%>">
                    <input type="hidden" name="sWeight" value="<%=sWeight%>">
					<input type="hidden" name="nTotalQuantity" value="<%=nTotalQuantity%>">
                    <input type="hidden" name="Token" value="<%=Token%>">
                    <input type="hidden" name="PayerID" value="<%=PayerID%>">
                    <input type="hidden" name="CorrelationID" value="<%=CorrelationID%>">
                    <input type="hidden" name="grandTotal" value="<%=grandtotal%>">
                    <input type="hidden" name="SHIPPINGAMT" value="<%=nShipRate%>">
                    <input type="hidden" name="TAXAMT" value="<%=TAXAMT%>">
                    <input type="hidden" name="intlShipping" value="<%=strShiptype%>">
                    <input type="hidden" name="INVNUM" value="<%=newOrderID%>">
                    <input type="hidden" name="sPromoCode" value="<%=couponcode%>">
                    <input type="hidden" name="itemTotal" value="<%=nItemTotal%>">
                    <input type="hidden" name="paymentAmount" value="<%=paymentAmount%>">
                    <input type="hidden" name="BuySafeAmount" value="<%=buysafeamount%>">
                    <input type="hidden" name="BuySafeCartID" value="<%=BuySafeCartID%>">
					<input type="hidden" name="fname" value="<%=FIRSTNAME%>">	
                    <input type="hidden" name="lname" value="<%=LASTNAME%>">
					<input type="hidden" name="sAddress1" value="<%=SHIPTOSTREET%>">
                    <input type="hidden" name="sAddress2" value="<%=SHIPTOSTREET2%>">
					<input type="hidden" name="sCity" value="<%=SHIPTOCITY%>">
					<input type="hidden" name="sZip" value="<%=SHIPTOZIP%>">
					<input type="hidden" name="sState" value="<%=sState%>">
					<input type="hidden" name="SHIPTOCOUNTRYCODE" value="<%=SHIPTOCOUNTRYCODE%>">
					<input type="hidden" name="EMAIL" value="<%=EMAIL%>">
					<input type="hidden" name="chkOptMail" value="Y">
                    <input type="hidden" name="showShipping" value="0" />
                    <input type="hidden" name="mobile" value="1" />
                </div>            
			</form>
			<%
		else
			%>
            <div class="centerContain">
	            <h3 align="center"><br><br><br><br>We're sorry, but CellularOutfitter does not ship outside of the U.S., its territories, and Canada.</h3>
            </div>
			<%
		end if
	else
		%>
		<div class="centerContain">
			<h3 align="center"><br><br><br><br>We're sorry!<br>Your Paypal payment was declined.</h3>
			<p align="center"><a href="<%=cancelURL%>">PLEASE CLICK HERE TO START AGAIN.</a><br><br><br><br></p>
        </div>
		<%
	end if
end if

if err.number <> 0 then
	call responseRedirect("PaypalError.asp")
end if

'CLEAN UP EVERYTHING
set objXMLDOC = nothing
set objXMLDOM = nothing
set oNode = nothing
%>
<script>
window.WEDATA.pageType = 'checkout';
window.WEDATA.pageData = {
	discountTotal: '<%=ndiscountTotal%>',
	subTotal: '<%=formatNumber(nItemTotal+TAXAMT-nDiscountTotal,2) %>',
	shipping: '<%= nShipRate %>',
	amount: '<%=grandtotal%>',
	tax: '<%=TAXAMT%>',
	cartItems: []
};
</script>
<!--#include virtual="/template/bottom.asp"-->

<script language="javascript">
<!-- Begin
function updateShippingTotal() {
	if (document.frmProcessOrder.shiptype[0].checked) {
		shipCost = document.frmProcessOrder.shipcost0.value;
	}
	else if (document.frmProcessOrder.shiptype[1].checked) {
		shipCost = document.frmProcessOrder.shipcost2.value;
	}
	else if (document.frmProcessOrder.shiptype[2].checked) {
		shipCost = document.frmProcessOrder.shipcost3.value;
	}
	var newTotal = parseFloat(document.frmProcessOrder.paymentAmount.value) + parseFloat(shipCost) + parseFloat(<%=TAXAMT%>);
	newTotal = newTotal.toFixed(2);
	shipCost = parseFloat(shipCost).toFixed(2);
	document.getElementById("shippingCostDiv").innerHTML = '$' + shipCost;
	document.getElementById("grandTotalDiv").innerHTML = '$' + newTotal;
	document.frmProcessOrder.grandTotal.value = newTotal;
	document.frmProcessOrder.shippingTotal.value = shipCost;
	document.getElementById("shipSummary").className = 'summaryRow';
	document.getElementById("estSummary").className = 'summaryRowDark hidden';
	document.getElementById("grandSummary").className = 'summaryRowDark';
	document.frmProcessOrder.showShipping.value = 1;
}

function changeValue(span,val) {
	if (document.all) {
		eval(span).innerHTML = val;
	} else if (document.getElementById) {
		document.getElementById(span).innerHTML = val;
	}
}

function changeTax(state) {
	var nCATax = CurrencyFormatted((document.frmProcessOrder.itemTotal.value - document.frmProcessOrder.BuySafeAmount.value) * <%=Application("taxMath")%>);
	if (state == "CA") {
		changeValue("CAtaxMsg","Additional <%=Application("taxDisplay")%>% Tax for Shipments Within CA");
		changeValue("CAtax","$" + nCATax);
		document.frmProcessOrder.nCATax.value = nCATax;
	} else {
		changeValue("CAtaxMsg","");
		changeValue("CAtax","$0.00");
		document.frmProcessOrder.nCATax.value = 0;
	}
	if (state == "AB" || state == "BC" || state == "MB" || state == "NB" || state == "NL" || state == "NS" || state == "NT" || state == "NU" || state == "ON" || state == "PE" || state == "QC" || state == "SK" || state == "YT") {
		changeValue("Shipping0","USPS First Class Int'l (8-12 business days)");
		document.frmProcessOrder.shipcost0.value = "<%=shipcost4%>";
		document.frmProcessOrder.shipcost2.value = "<%=shipcost5%>";
		changeValue("Shipping2","USPS Priority Int'l (3-8 business days)");
		if (document.frmProcessOrder.shiptype[2].checked == true) {
			document.frmProcessOrder.shiptype[2].checked = false;
			document.frmProcessOrder.shiptype[1].checked = true;
		}
		for (var i = 2; i < shippingOptions; i++) {
			document.frmProcessOrder.shiptype[i].disabled = true;
			changeValue("Shipping" + (i+1),"");
		}
		document.frmProcessOrder.intlShipping.value = "1";
	} else {
		changeValue("Shipping0","USPS First Class (4-10 business days)");
		document.frmProcessOrder.shipcost2.value = "<%=shipcost2%>";
		changeValue("Shipping2","USPS Priority Mail (2-4 business days)");
		document.frmProcessOrder.shiptype[2].disabled = false;
		changeValue("Shipping3","USPS Express Mail (1-2 business days)");
		document.frmProcessOrder.intlShipping.value = "0";
	}
	updateGrandTotal();
}

function updateGrandTotal() {
	var a = document.frmProcessOrder.shiptype.length;
	for (var i = 0; i < a; i++) {
		if (document.frmProcessOrder.shiptype[i].checked) {
			var useVal = document.frmProcessOrder.shiptype[i].value
			var b = eval("document.frmProcessOrder.shipcost" + useVal + ".value")
			b = b.replace("$","")
			var shippingCost = b;
			document.frmProcessOrder.shippingTotal.value = shippingCost;
			document.frmProcessOrder.SHIPPINGAMT.value = shippingCost;
			document.getElementById("shipcost").innerHTML = "$" + shippingCost
			//changeValue("shipcost","$" + shippingCost);
		}
	}
	var GrandTotal = Number(document.frmProcessOrder.paymentAmount.value) + Number(document.frmProcessOrder.nCATax.value) + Number(b);
	changeValue("GrandTotal","$" + CurrencyFormatted(GrandTotal));
	document.frmProcessOrder.grandTotal.value = CurrencyFormatted(GrandTotal);
}

function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

function CheckSubmit() {
	var f = document.frmProcessOrder;
	bValid = true;
	
	CheckValidNEW(f.fname.value, "Your First Name is a required field!");
	CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
	CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
	CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
	sString = f.sState.options[f.sState.selectedIndex].value;
	CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
	CheckValidNEW(f.sZip.value, "Zip/Postal is a requried field!");
	
	if (bValid) {
		var a = f.shiptype.length;
		for (var i = 0; i < a; i++) {
			if (f.shiptype[i].checked) {
				var shiptype = f.shiptype[i].value;
			}
		}
		var shiprate = eval('document.frmProcessOrder.shipcost' + shiptype);
		f.SHIPPINGAMT.value = shiprate.value;
		if (f.sState.options[f.sState.selectedIndex].value == "CA") {
			f.TAXAMT.value = <%=round((paymentAmount - buysafeamount) * Application("taxMath"), 2)%>;
		} else {
			f.TAXAMT.value = 0;
		}
		document.getElementById("submitBttn").innerHTML = "Please wait while<br />we process your order"
		f.submit();
	}
	return false;
}

function showShip() {
	document.getElementById("shipcost").style.display = '';
	document.getElementById("grandTotal").style.display = '';
	document.getElementById("subTotal").style.display = 'none';
}
//  End -->
</script>
