<%
if request.ServerVariables("REMOTE_ADDR") = "173.200.82.81" then isMe = true else isMe = false
dim promoItem : promoItem = 259959
dim promoItem2 : promoItem2 = 321981
dim useHttps : useHttps = 1
response.buffer = true
dim HTTP_REFERER
HTTP_REFERER = request.servervariables("HTTP_REFERER")
'if HTTP_REFERER <> "https://www.cellularoutfitter.com/cart/basket.asp" and HTTP_REFERER <> "http://www.cellularoutfitter.com/cart/basket.asp" and HTTP_REFERER <> "http://staging.cellularoutfitter.com/cart/basket.asp" then
if instr(HTTP_REFERER,"cellularoutfitter.com") < 1 then
	response.write "<h2>This page will only accept forms submitted from the Cellular Outfitter secure website.</h2>" & vbcrlf
	response.end
end if

if instr(request.ServerVariables("SERVER_NAME"),"m.cellularoutfitter.com") > 0 or instr(request.ServerVariables("SERVER_NAME"),"mdev.cellularoutfitter.com") > 0 then
	mobileOrder = 1
	if instr(Request.ServerVariables("SERVER_NAME"), "mdev.") > 0 then
		useSite = "http://mdev.cellularoutfitter.com"	
	else
		useSite = "https://m.cellularoutfitter.com"
	end if
	returnURL = useSite & "/cart/process/PaypalSOAP/GetExpressCheckoutDetailsM.asp"
	cancelURL = useSite & "/basket.html"
else
	if instr(Request.ServerVariables("SERVER_NAME"), "staging.") > 0 then
		useSite = "http://staging.cellularoutfitter.com"
	else
		useSite = "https://www.cellularoutfitter.com"
	end if
	returnURL = useSite & "/cart/process/PaypalSOAP/GetExpressCheckoutDetails.asp"
	cancelURL = useSite & "/cart/basket.asp"
end if
%>
<!--#include virtual="/cart/process/PaypalSOAP/Constants.asp"-->
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/cart/includes/inc_makeNum.asp"-->
<%
dim paymentAmount, ItemsAndWeight, itemsArray, a, numItems, sWeight, basketitem, ItemStr
paymentAmount = 0
ItemsAndWeight = request.form("ItemsAndWeight")
itemsArray = split(ItemsAndWeight,"|")
for a = 0 to uBound(itemsArray)
	if a = 0 then
		numItems = cDbl(itemsArray(a))
	else
		sWeight = cDbl(itemsArray(a))
	end if
next
ItemsAndWeight = ItemsAndWeight & "|" & request.form("shipZip")
dim promoIncluded : promoIncluded = 0

for basketitem = 0 to numItems - 1
	if prepInt(XMLencode(request.form("L_NUMBER" & basketitem))) = promoItem and prepInt(XMLencode(request.form("L_AMT" & basketitem))) = 0 then
		promoIncluded = 1
	end if
	if prepInt(XMLencode(request.form("L_NUMBER" & basketitem))) = promoItem2 and prepInt(XMLencode(request.form("L_AMT" & basketitem))) = 0 then
		promoIncluded = 1
	end if
	
	ItemStr = ItemStr & "						<PaymentDetailsItem>" & vbcrlf
	ItemStr = ItemStr & "							<Name>" & XMLencode(request.form("L_NAME" & basketitem)) & "</Name>" & vbcrlf
	ItemStr = ItemStr & "							<Number>" & XMLencode(request.form("L_NUMBER" & basketitem)) & "</Number>" & vbcrlf
	ItemStr = ItemStr & "							<Quantity>" & XMLencode(request.form("L_QTY" & basketitem)) & "</Quantity>" & vbcrlf
	ItemStr = ItemStr & "							<Amount currencyID=""USD"">" & XMLencode(request.form("L_AMT" & basketitem)) & "</Amount>" & vbcrlf
	ItemStr = ItemStr & "						</PaymentDetailsItem>" & vbcrlf
next

call fOpenConn()
redim itemType(numItems), itemQty(numItems), itemPrice(numItems)
dim nProdIdCheck, nProdQuantity, RS, SQL
for basketitem = 0 to numItems - 1
	nProdIdCheck = makeNum(request.form("L_NUMBER" & basketitem))
	nProdQuantity = makeNum(request.form("L_QTY" & basketitem))
	checkValue = makeNum(request.form("L_AMT" & basketitem))
	if nProdIdCheck = 0 and checkValue = -5 then
		paymentAmount = paymentAmount - 5
	else
		set RS = server.createobject("ADODB.recordset")
		SQL = "SELECT typeID, price_CO FROM we_items WHERE itemID='" & nProdIdCheck & "'"
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			itemType(basketitem) = RS("typeID")
			itemQty(basketitem) = nProdQuantity
			if checkValue = 0 then
				itemPrice(basketitem) = 0
			else
				itemPrice(basketitem) = RS("price_CO")
				paymentAmount = paymentAmount + (itemPrice(basketitem) * itemQty(basketitem))
			end if
		end if
		RS.close
	end if
next
call fCloseConn()

dim sPromoCode, discountTotal
if request.form("promo") <> "" then
	sPromoCode = request.form("promo")
else
	sPromoCode = session("promocode")
end if

'if sPromoCode <> "" then
'	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
'	call fOpenConn()
'	SQL = "SELECT * FROM CO_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
'	set RS = server.CreateObject("ADODB.Recordset")
'	RS.open SQL, oConn, 3, 3
'	if not RS.eof then
'		dim couponid, promoMin, promoPercent, typeID, BOGO, couponDesc
'		couponid = RS("couponid")
'		promoMin = RS("promoMin")
'		promoPercent = RS("promoPercent")
'		typeID = RS("typeID")
'		BOGO = RS("BOGO")
'		
'		if paymentAmount >= promoMin then
'			if typeID <> "0" or BOGO = true then
'				if typeID <> "0" then
'					typeIDarray = split(typeID,",")
'					for a = 0 to uBound(typeIDarray)
'						for basketitem = 0 to numItems - 1
'							if cStr(itemType(basketitem)) = cStr(typeIDarray(a)) then
'								if BOGO = true then
'									discountTotal = discountTotal + (itemPrice(basketitem) * int(itemQty(basketitem)/2) * promoPercent)
'								else
'									discountTotal = discountTotal + (itemPrice(basketitem) * itemQty(basketitem) * promoPercent)
'								end if
'							end if
'						next
'					next
'				end if
'			else
'				discountTotal = paymentAmount * promoPercent
'			end if
'			
'			discountTotal = round(discountTotal,2)
'			
'			ItemStr = ItemStr & "						<PaymentDetailsItem>" & vbcrlf
'			ItemStr = ItemStr & "							<Name>" & XMLencode(sPromoCode) & "</Name>" & vbcrlf
'			ItemStr = ItemStr & "							<Number>DISCOUNT</Number>" & vbcrlf
'			ItemStr = ItemStr & "							<Quantity>1</Quantity>" & vbcrlf
'			ItemStr = ItemStr & "							<Amount currencyID=""USD"">" & XMLencode(discountTotal * -1) & "</Amount>" & vbcrlf
'			ItemStr = ItemStr & "						</PaymentDetailsItem>" & vbcrlf
'		end if
'		call fCloseConn()
'	else
'		sPromoCode = ""
'	end if
'end if

discountTotal = paymentAmount - request.form("paymentAmount")
if isnumeric(discountTotal) then
	if discountTotal > 0 then
		discountTotal = round(cdbl(discountTotal), 2)
	
		ItemStr = ItemStr & "						<PaymentDetailsItem>" & vbcrlf
		ItemStr = ItemStr & "							<Name>" & XMLencode(sPromoCode) & "</Name>" & vbcrlf
		ItemStr = ItemStr & "							<Number>DISCOUNT</Number>" & vbcrlf
		ItemStr = ItemStr & "							<Quantity>1</Quantity>" & vbcrlf
		ItemStr = ItemStr & "							<Amount currencyID=""USD"">" & XMLencode(discountTotal * -1) & "</Amount>" & vbcrlf
		ItemStr = ItemStr & "						</PaymentDetailsItem>" & vbcrlf
	end if
else
	discountTotal = 0.0
end if

dim buysafeamount, WantsBondField, buysafecartID
if request.form("buysafeamount") = "" then
	buysafeamount = 0
else
	buysafeamount = cDbl(request.form("buysafeamount"))
end if
if buysafeamount > 0 then
	ItemStr = ItemStr & "						<PaymentDetailsItem>" & vbcrlf
	ItemStr = ItemStr & "							<Name>  BuySafe </Name>" & vbcrlf
	ItemStr = ItemStr & "							<Number>" & 1 & "</Number>" & vbcrlf
	ItemStr = ItemStr & "							<Quantity>" & 1 & "</Quantity>" & vbcrlf
	ItemStr = ItemStr & "							<Amount currencyID=""USD"">" & buysafeamount & "</Amount>" & vbcrlf
	ItemStr = ItemStr & "							<Tax currencyID=""USD"">" & 0 & "</Tax>" & vbcrlf
	ItemStr = ItemStr & "						</PaymentDetailsItem>" & vbcrlf
end if
WantsBondField = request.form("WantsBondField")
buysafecartID = request.form("buysafecartID")

'paymentAmount = paymentAmount - discountTotal
paymentAmount = paymentAmount - discountTotal + buysafeamount

'response.write "<pre>" & itemStr & "</pre>"
'response.end

'BUILD CALL DATA
dim SoapStr
SoapStr = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & vbcrlf
SoapStr = SoapStr & "<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbcrlf
SoapStr = SoapStr & "	<soap:Header>" & vbcrlf
SoapStr = SoapStr & "		<RequesterCredentials xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<Credentials xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
SoapStr = SoapStr & "				<Username>" & API_USERNAME & "</Username>" & vbcrlf
SoapStr = SoapStr & "				<Password>" & API_PASSWORD & "</Password>" & vbcrlf
SoapStr = SoapStr & "				<Signature>" & API_SIGNATURE & "</Signature>" & vbcrlf
SoapStr = SoapStr & "			</Credentials>" & vbcrlf
SoapStr = SoapStr & "		</RequesterCredentials>" & vbcrlf
SoapStr = SoapStr & "	</soap:Header>" & vbcrlf
SoapStr = SoapStr & "	<soap:Body>" & vbcrlf
SoapStr = SoapStr & "		<SetExpressCheckoutReq xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<SetExpressCheckoutRequest>" & vbcrlf
SoapStr = SoapStr & "				<Version xmlns=""urn:ebay:apis:eBLBaseComponents"">" & API_VERSION & "</Version>" & vbcrlf
SoapStr = SoapStr & "				<SetExpressCheckoutRequestDetails xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
SoapStr = SoapStr & "					<ReturnURL>" & returnURL & "</ReturnURL>" & vbcrlf
SoapStr = SoapStr & "					<CancelURL>" & cancelURL & "</CancelURL>" & vbcrlf
SoapStr = SoapStr & "					<BillingAgreementDetails>" & vbcrlf
SoapStr = SoapStr & "						<BillingType>MerchantInitiatedBilling</BillingType>" & vbcrlf
SoapStr = SoapStr & "						<BillingAgreementDescription>CellularOutfitter.com Online Paypal Order</BillingAgreementDescription>" & vbcrlf
SoapStr = SoapStr & "					</BillingAgreementDetails>" & vbcrlf
SoapStr = SoapStr & "					<ReqConfirmShipping>0</ReqConfirmShipping>" & vbcrlf
SoapStr = SoapStr & "					<ReqBillingAddress>0</ReqBillingAddress>" & vbcrlf
SoapStr = SoapStr & "					<NoShipping>0</NoShipping>" & vbcrlf
SoapStr = SoapStr & "					<LocaleCode>US</LocaleCode>" & vbcrlf
SoapStr = SoapStr & "					<cpp-header-border-color>FFFFFF</cpp-header-border-color>" & vbcrlf
SoapStr = SoapStr & "					<cpp-header-back-color>FFFFFF</cpp-header-back-color>" & vbcrlf
SoapStr = SoapStr & "					<cpp-payflow-color>FFFFFF</cpp-payflow-color>" & vbcrlf
SoapStr = SoapStr & "					<PaymentAction>Authorization</PaymentAction>" & vbcrlf
SoapStr = SoapStr & "					<AllowNote>0</AllowNote>" & vbcrlf
SoapStr = SoapStr & "					<PaymentDetails>" & vbcrlf
SoapStr = SoapStr & "						<OrderTotal currencyID=""USD"">" & paymentAmount & "</OrderTotal>" & vbcrlf
SoapStr = SoapStr & "						<ItemTotal currencyID=""USD"">" & paymentAmount & "</ItemTotal>" & vbcrlf
SoapStr = SoapStr & "						<Custom>" & ItemsAndWeight & ","& buysafeamount& "," & buysafecartID & "," & WantsBondField & "," & promoIncluded & "</Custom>" & vbcrlf
SoapStr = SoapStr & ItemStr
SoapStr = SoapStr & "					</PaymentDetails>" & vbcrlf
SoapStr = SoapStr & "				</SetExpressCheckoutRequestDetails>" & vbcrlf
SoapStr = SoapStr & "			</SetExpressCheckoutRequest>" & vbcrlf
SoapStr = SoapStr & "		</SetExpressCheckoutReq>" & vbcrlf
SoapStr = SoapStr & "	</soap:Body>" & vbcrlf
SoapStr = SoapStr & "</soap:Envelope>" & vbcrlf
'response.write SoapStr
'response.end
'CREATE OBJECTS
dim objXMLDOC, objXMLDOM, oNode, strError
set objXMLDOC = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
set objXMLDOM = Server.CreateObject("Msxml2.DomDocument")
set oNode = Server.CreateObject("Microsoft.XMLDOM")

'MAKE THE CALL
objXMLDOC.open "POST", API_ENDPOINT, False
objXMLDOC.setRequestHeader "Content-Type", "text/xml"
objXMLDOC.send(SoapStr)
objXMLDOM.async = false
objXMLDOM.LoadXML objXMLDOC.responseText

'response.write objXMLDOC.responseText
'response.end

'PROCESS THE CALL
if objXMLDOM.parseError.errorCode <> 0 then
	strError = strError & "<h3>Parser error found.</h3>"
else
	'BEGIN CHECK FOR ERROR FROM PAYPAL
	set oNode = objXMLDOM.getElementsByTagName("Ack")
	if (not oNode is nothing) then
		if oNode.item(0).text = "Failure" then
			set oNode = objXMLDOM.getElementsByTagName("ShortMessage")
			if (not oNode is nothing) then strError = strError & "<h3>ERROR: " & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("LongMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("ErrorCode")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
		else
			'PROCESS SUCCESSFUL CALL
			dim token
			set oNode = objXMLDOM.getElementsByTagName("Token")
			if (not oNode is nothing) then token = oNode.item(0).text
		end if
	else
		strError = strError & "<h3>No data received.</h3>"
	end if
end if

'CLEAN UP EVERYTHING
set objXMLDOC = nothing
set objXMLDOM = nothing
set oNode = nothing

if strError <> "" then
	response.write strError
else
	response.redirect(PAYPAL_EC_URL & "?cmd=_express-checkout&token=" & token)
end if
response.end

function XMLencode(str)
	XMLencode = replace(replace(replace(replace(replace(str,"&","&amp;"),"<","&lt;"),">","&gt;"),"'","&apos;"),chr(34),"&quot;")
end function
%>
