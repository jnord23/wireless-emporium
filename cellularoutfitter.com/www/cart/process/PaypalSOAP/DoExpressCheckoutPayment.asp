<%
dim useHttps : useHttps = 1
response.buffer = true
dim HTTP_REFERER
HTTP_REFERER = request.servervariables("HTTP_REFERER")
if instr(HTTP_REFERER,"cellularoutfitter.com") < 1 then
	response.write "<h2>This page will only accept forms submitted from the Cellular Outfitter secure website.</h2><p>" & vbcrlf
	response.end
end if
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_Webloyalty.asp"-->
<!--#include virtual="/cart/process/PaypalSOAP/Constants.asp"-->
<!--#include virtual="/includes/asp/inc_getMySession.asp"-->
<%
'for each aaa in request.form
'	response.write aaa & ": " & request.form(aaa) & "<br><br>"
'next
'response.end
dim token, payerID, INVNUM
INVNUM = request.form("INVNUM")
token = request.form("token")
payerID = request.form("PayerID")
mobileOrder = prepInt(request.Form("mobile"))

if INVNUM = "" or token = "" or payerID = "" then
	call responseRedirect("PaypalError.asp")
end if

dim nShipType, intlShipping, strShiptype
nShipType = request.form("shiptype")
intlShipping = request.form("intlShipping")
select case nShipType
	case "2"
		if intlShipping = "1" then
			strShiptype = "USPS Priority Int''l"
		else
			strShiptype = "USPS Priority"
		end if
	case "3"
		if intlShipping = "1" then
			strShiptype = "USPS Priority Int''l"
		else
			strShiptype = "USPS Express"
		end if
	case "4"
		if intlShipping = "1" then
			strShiptype = "First Class Int''l"
		else
			strShiptype = "First Class"
		end if
	case "5"
		if intlShipping = "1" then
			strShiptype = "USPS Priority Int''l"
		else
			strShiptype = "USPS Priority"
		end if
	case "0"
		if intlShipping = "1" then
			strShiptype = "First Class Int''l"
		else
			strShiptype = "First Class"
		end if
	case "7" : strShiptype = "UPS Ground"
	case "8" : strShiptype = "UPS 3 Day Select"
	case "9" : strShiptype = "UPS 2nd Day Air"
end select

dim TAXAMT, SHIPPINGAMT, grandTotal, paymentAmount, nTotalQuantity
TAXAMT = request.form("nCATax")
SHIPPINGAMT = request.form("shipcost" & nShipType)
paymentAmount = request.form("paymentAmount")
nTotalQuantity = request.form("nTotalQuantity")
grandTotal = cDbl(paymentAmount) + cDbl(TAXAMT) + cDbl(SHIPPINGAMT)

'dim fname, lname, sAddress1, sAddress2, sCity, sState, sZip, sCountry
fname = request.form("fname")
lname = request.form("lname")
sAddress1 = request.form("sAddress1")
sAddress2 = request.form("sAddress2")
sCity = request.form("sCity")
sState = request.form("sState")
sZip = request.form("sZip")
sCountry = request.form("SHIPTOCOUNTRYCODE")
sEmail = request.form("EMAIL")

dim BuySafeAmount, ShoppingCartId
BuySafeAmount = request.form("BuySafeAmount")
ShoppingCartId = request.form("BuySafeCartID")

dim SoapStr
SoapStr = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & vbcrlf
SoapStr = SoapStr & "<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbcrlf
SoapStr = SoapStr & "	<soap:Header>" & vbcrlf
SoapStr = SoapStr & "		<RequesterCredentials xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<Credentials xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
SoapStr = SoapStr & "				<Username>" & API_USERNAME & "</Username>" & vbcrlf
SoapStr = SoapStr & "				<Password>" & API_PASSWORD & "</Password>" & vbcrlf
SoapStr = SoapStr & "				<Signature>" & API_SIGNATURE & "</Signature>" & vbcrlf
SoapStr = SoapStr & "			</Credentials>" & vbcrlf
SoapStr = SoapStr & "		</RequesterCredentials>" & vbcrlf
SoapStr = SoapStr & "	</soap:Header>" & vbcrlf
SoapStr = SoapStr & "	<soap:Body>" & vbcrlf
SoapStr = SoapStr & "		<DoExpressCheckoutPaymentReq xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<DoExpressCheckoutPaymentRequest>" & vbcrlf
SoapStr = SoapStr & "				<Version xmlns=""urn:ebay:apis:eBLBaseComponents"">" & API_VERSION & "</Version>" & vbcrlf
SoapStr = SoapStr & "				<DoExpressCheckoutPaymentRequestDetails xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
SoapStr = SoapStr & "					<PaymentAction>Authorization</PaymentAction>" & vbcrlf
SoapStr = SoapStr & "					<Token>" & token & "</Token>" & vbcrlf
SoapStr = SoapStr & "					<PayerID>" & payerID & "</PayerID>" & vbcrlf
SoapStr = SoapStr & "					<PaymentDetails>" & vbcrlf
SoapStr = SoapStr & "						<OrderTotal currencyID=""USD"">" & grandTotal & "</OrderTotal>" & vbcrlf
SoapStr = SoapStr & "						<ItemTotal currencyID=""USD"">" & paymentAmount & "</ItemTotal>" & vbcrlf
SoapStr = SoapStr & "						<TaxTotal currencyID=""USD"">" & TAXAMT & "</TaxTotal>" & vbcrlf
SoapStr = SoapStr & "						<ShippingTotal currencyID=""USD"">" & SHIPPINGAMT & "</ShippingTotal>" & vbcrlf
SoapStr = SoapStr & "						<ShipToAddress>" & vbcrlf
SoapStr = SoapStr & "							<Name>" & fname & " " & lname & "</Name>" & vbcrlf
SoapStr = SoapStr & "							<Street1>" & sAddress1 & "</Street1>" & vbcrlf
SoapStr = SoapStr & "							<Street2>" & sAddress2 & "</Street2>" & vbcrlf
SoapStr = SoapStr & "							<CityName>" & sCity & "</CityName>" & vbcrlf
SoapStr = SoapStr & "							<StateOrProvince>" & sState & "</StateOrProvince>" & vbcrlf
SoapStr = SoapStr & "							<Country>" & sCountry & "</Country>" & vbcrlf
SoapStr = SoapStr & "							<PostalCode>" & sZip & "</PostalCode>" & vbcrlf
SoapStr = SoapStr & "						</ShipToAddress>" & vbcrlf
SoapStr = SoapStr & "					</PaymentDetails>" & vbcrlf
SoapStr = SoapStr & "				</DoExpressCheckoutPaymentRequestDetails>" & vbcrlf
SoapStr = SoapStr & "			</DoExpressCheckoutPaymentRequest>" & vbcrlf
SoapStr = SoapStr & "		</DoExpressCheckoutPaymentReq>" & vbcrlf
SoapStr = SoapStr & "	</soap:Body>" & vbcrlf
SoapStr = SoapStr & "</soap:Envelope>" & vbcrlf

'CREATE OBJECTS
dim objXMLDOC, objXMLDOM, oNode, strError
set objXMLDOC = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
set objXMLDOM = Server.CreateObject("Msxml2.DomDocument")
set oNode = Server.CreateObject("Microsoft.XMLDOM")

'MAKE THE CALL
objXMLDOC.open "POST", API_ENDPOINT, False
objXMLDOC.setRequestHeader "Content-Type", "text/xml"
objXMLDOC.send(SoapStr)
objXMLDOM.async = false
objXMLDOM.LoadXML objXMLDOC.responseText

'PROCESS THE CALL
if objXMLDOM.parseError.errorCode <> 0 then
	strError = strError & "<h3>Parser error found.</h3>"
else
	'BEGIN CHECK FOR ERROR FROM PAYPAL
	set oNode = objXMLDOM.getElementsByTagName("Ack")
	if (not oNode is nothing) then
		if oNode.item(0).text = "Failure" then
			set oNode = objXMLDOM.getElementsByTagName("ShortMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("LongMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("ErrorCode")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
		end if
	else
		strError = strError & "<h3>No data received.</h3>"
	end if
end if

if strError <> "" then
	set objXMLDOC = nothing
	set objXMLDOM = nothing
	set oNode = nothing

	call responseRedirect("PaypalError.asp")
else
	set oNode = objXMLDOM.getElementsByTagName("BillingAgreementID")
	if (not oNode is nothing) then
		BillingAgreementID = oNode.item(0).text
	end if
	
	' UPDATE DB WITH APPROVAL, SHIPPING, ETC.
	dim SQL, RS
	SQL = "UPDATE we_orders SET"
	SQL = SQL & " approved = -1,"
	SQL = SQL & " emailSent = null,"
	SQL = SQL & " extOrderNumber = '" & request.form("CorrelationID") & "',"
	SQL = SQL & " ordershippingfee = '" & formatNumber(SHIPPINGAMT,2) & "',"
	SQL = SQL & " orderTax = '" & TAXAMT & "',"
	SQL = SQL & " ordergrandtotal = '" & grandTotal & "',"
	SQL = SQL & " shiptype = '" & strShiptype & "'"
	SQL = SQL & " WHERE orderid='" & INVNUM & "'"
	oConn.execute(SQL)
	
	strItems = ""
	SQL = "SELECT a.itemID, a.quantity, b.ItemKit_NEW FROM we_orderdetails a left join we_items b on a.itemID = b.itemID WHERE a.orderid = '" & INVNUM & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	dim nProdIdCheck, Quantity, KIT, decreaseSql
	do until RS.eof
		nProdIdCheck = RS("itemid")
		KIT = RS("ItemKit_NEW")
		nProdQuantity = RS("quantity")
		orderNumber = INVNUM
		if strItems = "" then
			strItems = nProdIdCheck
		else
			strItems = strItems & "," & nProdIdCheck
		end if
		
		if isNull(KIT) then
			'grab the single items master id and master qty
			SQL = "SELECT a.PartNumber, b.itemID as masterID, b.inv_qty FROM we_items a left join we_items b on a.partNumber = b.partNumber and b.master = 1 WHERE a.itemID = '" & nProdIdCheck & "'"
		else
			'grab the master id and master qty for each item in the kit
			SQL = "SELECT a.PartNumber, b.itemID as masterID, b.inv_qty FROM we_items a left join we_items b on a.partNumber = b.partNumber and b.master = 1 WHERE a.itemID IN (" & KIT & ")"
		end if
		set itemRS = oConn.execute(SQL)
		
		'loop through all items in kit, or single item if not in kit
		do while not itemRS.EOF
			nPartNumber = itemRS("PartNumber")
			masterID = itemRS("masterID")
			'adjust inventory
			decreaseSql = "UPDATE we_items SET inv_qty = CASE WHEN (inv_qty - " & nProdQuantity & " > 0) THEN inv_qty - " & nProdQuantity & " ELSE 0 END WHERE PartNumber = '" & nPartNumber & "' AND master = 1"
			oConn.execute(decreaseSql)
			'save inventory adjustment
			On Error Resume Next
			if prepInt(nProdQuantity) >= prepInt(itemRS("inv_qty")) then
				sql = "if not (select count(*) from we_invRecord where itemID = '" & masterID & "' and orderID = '" & OrderNumber & "') > 0 insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & masterID & "," & itemRS("inv_qty") & "," & nProdQuantity & "," & OrderNumber & ",0,'CO PayPal Customer Order *Out of Stock*','" & now & "')"
			else
				sql = "if not (select count(*) from we_invRecord where itemID = '" & masterID & "' and orderID = '" & OrderNumber & "') > 0 insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & masterID & "," & itemRS("inv_qty") & "," & nProdQuantity & "," & OrderNumber & ",0,'CO PayPal Customer Order','" & now & "')"
			end if
			session("errorSQL") = sql
			oConn.execute(sql)			
			On Error Goto 0
			'set number of sales for select item
			sqlQuery = "UPDATE we_items SET numberOfSales = numberOfSales + " & nProdQuantity & " WHERE itemID = '" & nProdIdCheck & "'"
			oConn.Execute(SQL)
			
			itemRS.movenext
		loop
		RS.movenext
	loop
	
	SQL = "SELECT A.orderid, A.accountid, A.ordersubtotal, A.ordergrandtotal, B.fname, B.email FROM we_orders A INNER JOIN CO_accounts B ON A.accountid=B.accountid WHERE A.orderid = '" & INVNUM & "'"
	'response.write "<p>" & SQL & "</p>" & vbcrlf
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	
	dim strRedirect
	if not RS.eof then
		'dim nAccountId, sPromoCode
		nAccountId = RS("accountID")
		sPromoCode = request.form("sPromoCode")
		
		SQL = "UPDATE CO_accounts SET"
		SQL = SQL & " fname = '" & SQLquote(fname) & "',"
		SQL = SQL & " lname = '" & SQLquote(lname) & "',"
		SQL = SQL & " sAddress1 = '" & SQLquote(sAddress1) & "',"
		SQL = SQL & " sAddress2 = '" & SQLquote(sAddress2) & "',"
		SQL = SQL & " sCity = '" & SQLquote(sCity) & "',"
		SQL = SQL & " sState = '" & SQLquote(sState) & "',"
		SQL = SQL & " sZip = '" & SQLquote(sZip) & "',"
		SQL = SQL & " sCountry = '" & SQLquote(sCountry) & "',"
		SQL = SQL & " BillingAgreementID = '" & BillingAgreementID & "'"
		SQL = SQL & " WHERE accountid = '" & nAccountId & "'"
'		response.write "<p>" & SQL & "</p>" & vbcrlf
'		response.end
		oConn.execute SQL
		
		SQL = "UPDATE ShoppingCart SET purchasedOrderID = '" & RS("orderID") & "' WHERE store = 2 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
		SQL = SQL & " AND itemID IN (" & strItems & ")"
		session("errorSQL") = SQL
		oConn.execute SQL
		
		if request.form("chkOptMail") = "Y" then
			oConn.execute("sp_ModifyEmailOpt_CO '" & SQLquote(RS("fname")) & "','" & SQLquote(RS("email")) & "','Y'")
		end if
		
		if mobileOrder = 1 then
			if instr(Request.ServerVariables("SERVER_NAME"), "mdev.") > 0 then
				strRedirect = "http://mdev.cellularoutfitter.com/cart/complete.asp?a=" & RS("accountID") & "&o=" & RS("orderID") & "&d=" & RS("ordergrandtotal") & "&c=" & RS("ordersubtotal") & "&pp=Y"
			else
				strRedirect = "https://m.cellularoutfitter.com/cart/complete.asp?a=" & RS("accountID") & "&o=" & RS("orderID") & "&d=" & RS("ordergrandtotal") & "&c=" & RS("ordersubtotal") & "&pp=Y"
			end if		
		else
			if instr(Request.ServerVariables("SERVER_NAME"), "staging.") > 0 then
				strRedirect = "http://staging.cellularoutfitter.com/cart/complete.asp?a=" & RS("accountID") & "&o=" & RS("orderID") & "&d=" & RS("ordergrandtotal") & "&c=" & RS("ordersubtotal") & "&pp=Y"			
			else
				strRedirect = "https://www.cellularoutfitter.com/cart/complete.asp?a=" & RS("accountID") & "&o=" & RS("orderID") & "&d=" & RS("ordergrandtotal") & "&c=" & RS("ordersubtotal") & "&pp=Y"
			end if
		end if
		
		nOrderId = INVNUM
		%>
		<%'#include virtual="/includes/asp/inc_receipt.asp"-->%>
		<%
	else
		' RECORD NOT FOUND error message
		call responseRedirect("PaypalError.asp")
	end if
	
	RS.close
	set RS = nothing
	call CloseConn(oConn)
	
	if strRedirect <> "" then
		set objXMLDOC = nothing
		set objXMLDOM = nothing
		set oNode = nothing
		call responseRedirect(strRedirect)
	end if
end if

function XMLencode(str)
	XMLencode = replace(replace(replace(replace(replace(str,"&","&amp;"),"<","&lt;"),">","&gt;"),"'","&apos;"),chr(34),"&quot;")
end function
%>
