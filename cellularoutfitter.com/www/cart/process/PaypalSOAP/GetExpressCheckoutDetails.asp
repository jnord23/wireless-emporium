<%
dim useHttps : useHttps = 1
response.buffer = true
SEtitle = "Cheap Cell Phone Accessories ?Cellular Phone Accessories ?CellularOutfitter.com"
SEdescription = "Buy cheap Cell Phone Accessories at Wholesale Prices to the Public.  Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more.  Cellular Accessories at the Lowest Prices Online Guaranteed."
SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"

strBody = "<body leftmargin=""0"" topmargin=""0"" onload=""changeTax(document.frmProcessOrder.sState.value);"">"
%>

<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/process/PaypalSOAP/Constants.asp"-->
<!--#include virtual="/cart/includes/inc_getStates.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->

<%
dim token, PayerID, SoapStr
token = request.querystring("token")
PayerID = request.querystring("PayerID")
if token = "" or payerID = "" then
	response.redirect "PaypalError.asp"
	response.end
end if

'BUILD CALL DATA
SoapStr = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & vbcrlf
SoapStr = SoapStr & "<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbcrlf
SoapStr = SoapStr & "	<soap:Header>" & vbcrlf
SoapStr = SoapStr & "		<RequesterCredentials xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<Credentials xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
SoapStr = SoapStr & "				<Username>" & API_USERNAME & "</Username>" & vbcrlf
SoapStr = SoapStr & "				<Password>" & API_PASSWORD & "</Password>" & vbcrlf
SoapStr = SoapStr & "				<Signature>" & API_SIGNATURE & "</Signature>" & vbcrlf
SoapStr = SoapStr & "			</Credentials>" & vbcrlf
SoapStr = SoapStr & "		</RequesterCredentials>" & vbcrlf
SoapStr = SoapStr & "	</soap:Header>" & vbcrlf
SoapStr = SoapStr & "	<soap:Body>" & vbcrlf
SoapStr = SoapStr & "		<GetExpressCheckoutDetailsReq xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<GetExpressCheckoutDetailsRequest>" & vbcrlf
SoapStr = SoapStr & "				<Version xmlns=""urn:ebay:apis:eBLBaseComponents"">" & API_VERSION & "</Version>" & vbcrlf
SoapStr = SoapStr & "				<Token>" & token & "</Token>" & vbcrlf
SoapStr = SoapStr & "			</GetExpressCheckoutDetailsRequest>" & vbcrlf
SoapStr = SoapStr & "		</GetExpressCheckoutDetailsReq>" & vbcrlf
SoapStr = SoapStr & "	</soap:Body>" & vbcrlf
SoapStr = SoapStr & "</soap:Envelope>" & vbcrlf

'CREATE OBJECTS
dim objXMLDOC, objXMLDOM, oNode, strError
set objXMLDOC = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
set objXMLDOM = Server.CreateObject("Msxml2.DomDocument")
set oNode = Server.CreateObject("Microsoft.XMLDOM")

'SET CALL TIMEOUTS
dim lResolve, lConnect, lSend, lReceive
lResolve = 30000 'Timeout values are in milli-seconds
lConnect = 30000
lSend = 30000
lReceive = 30000
objXMLDOC.setTimeouts lResolve, lConnect, lSend, lReceive

'MAKE THE CALL
objXMLDOC.open "POST", API_ENDPOINT, False
objXMLDOC.setRequestHeader "Content-Type", "text/xml"
objXMLDOC.send(SoapStr)
objXMLDOM.async = false
objXMLDOM.LoadXML objXMLDOC.responseText
'response.write objXMLDOC.responseText
'response.end
'PROCESS THE CALL
if objXMLDOM.parseError.errorCode <> 0 then
	strError = strError & "<h3>Parser error found.</h3>"
else
	'BEGIN CHECK FOR ERROR FROM PAYPAL
	set oNode = objXMLDOM.getElementsByTagName("Ack")
	if (not oNode is nothing) then
		session("errorSQL") = objXMLDOC.responseText
		if oNode.item(0).text = "Failure" then
			set oNode = objXMLDOM.getElementsByTagName("ShortMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("LongMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("ErrorCode")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
		end if
	else
		strError = strError & "<h3>No data received.</h3>"
	end if
end if

if strError <> "" then
	response.write strError
	response.end
else
	'PROCESS SUCCESSFUL CALL
	dim ShipToAddressName, ShipToAddressStreet1, ShipToAddressStreet2, ShipToAddressCityName, ShipToAddressStateOrProvince, ShipToAddressPostalCode
	dim ShipToAddressCountry, ShipToAddressPhone
	set oNode = objXMLDOM.getElementsByTagName("Token")
	if (not oNode is nothing) then
		Token = oNode.item(0).text
	end if
	
	dim nCATax, nItemTotal
	nCATax = 0
	nItemTotal = cdbl(0)
	nDiscountTotal = cdbl(0)
	
	dim ack
	ack = Ucase(objXMLDOM.getElementsByTagName("Ack").item(0).text)
	if ack = "SUCCESS" then
		paymentAmount = objXMLDOM.getElementsByTagName("PaymentDetails/ItemTotal").item(0).text
		'if paymentAmount = "" or token = "" then
		'	response.redirect("/sessionexpired.asp")
		'	response.end
		'end if
		if paymentAmount = "" then
			call responseRedirect("PaypalError.asp")
		end if
		
		dim nTotalQuantity, sWeight, sZip
		customdata = objXMLDOM.getElementsByTagName("GetExpressCheckoutDetailsResponseDetails/Custom").item(0).text
		myarray = split(customdata,",")
		ItemsAndWeight = myarray(0)
		BuySafeAmount = myarray(1)
		BuySafeCartID = myarray(2)
		WantsBondField = myarray(3)
		promoIncluded = myarray(4)

		itemsArray = split(ItemsAndWeight,"|")
		for a = 0 to uBound(itemsArray)
			select case a
				case 0 : nTotalQuantity = cDbl(itemsArray(a))
				case 1 : sWeight = cDbl(itemsArray(a))
				case 2 : sZip = cStr(itemsArray(a))
			end select
		next

		set ItemDetails = objXMLDOM.documentElement.getElementsByTagName("PaymentDetailsItem")
		for basketitem = 0 to ItemDetails.Length - 1
			set ItemDetailsNodes = ItemDetails.item(basketitem).childNodes
			itemPrice = prepInt(ItemDetailsNodes(4).text)
			itemQty = prepInt(ItemDetailsNodes(2).text)
			if itemPrice >= 0 then
				nItemTotal = nItemTotal + (itemPrice*itemQty)
			else
				nDiscountTotal = nDiscountTotal + (itemPrice*itemQty)
			end if
		next
				
		dim TAXAMT, strShiptype, shiptype, nShipRate, doNotProcess, newOrderID
		
		dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
'		shipcost0 = 5.99
'		shipcost2 = 9.99
'		shipcost3 = 22.99
'		shipcost4 = 7.99
'		shipcost5 = 14.99
'		
'		shipcost0 = formatCurrency(3.96 + (1.99 * nTotalQuantity))
'		shipcost2 = formatCurrency(7.96 + (1.99 * nTotalQuantity))
'		shipcost3 = formatCurrency(20.96 + (1.99 * nTotalQuantity))
'		shipcost4 = formatCurrency(4.96 + (2.99 * nTotalQuantity))
'		shipcost5 = formatCurrency(11.96 + (2.99 * nTotalQuantity))

		if promoIncluded = 1 then shipQty = nTotalQuantity - 1 else shipQty = nTotalQuantity

		shipcost0 = prepInt(4.00 + (1.99 * shipQty))
		shipcost2 = prepInt(8.00 + (1.99 * shipQty))
		shipcost3 = prepInt(21.00 + (1.99 * shipQty))
		shipcost4 = prepInt(5.00 + (2.99 * shipQty))
		shipcost5 = prepInt(12.00 + (2.99 * shipQty))
		
		dim fullName, nameArray, FIRSTNAME, LASTNAME
		fullName = objXMLDOM.getElementsByTagName("ShipToAddress/Name").item(0).text
		nameArray = split(fullName," ")
		FIRSTNAME = nameArray(0)
		LASTNAME = replace(fullName,FIRSTNAME & " ","")
		
		dim SHIPTOSTREET, SHIPTOSTREET2, SHIPTOCITY, sState, SHIPTOZIP, SHIPTOCOUNTRYCODE, PHONENUM, EMAIL
		SHIPTOSTREET = objXMLDOM.getElementsByTagName("ShipToAddress/Street1").item(0).text
		SHIPTOSTREET2 = objXMLDOM.getElementsByTagName("ShipToAddress/Street2").item(0).text
		SHIPTOCITY = objXMLDOM.getElementsByTagName("ShipToAddress/CityName").item(0).text
		sState = objXMLDOM.getElementsByTagName("ShipToAddress/StateOrProvince").item(0).text
		SHIPTOZIP = objXMLDOM.getElementsByTagName("ShipToAddress/PostalCode").item(0).text
		SHIPTOCOUNTRYCODE = objXMLDOM.getElementsByTagName("ShipToAddress/Country").item(0).text
		PHONENUM = objXMLDOM.getElementsByTagName("ShipToAddress/Phone").item(0).text
		EMAIL = objXMLDOM.getElementsByTagName("Payer").item(0).text

		if sZip = "" then
			sZip = SHIPTOZIP
			if instr(SHIPTOZIP, "-") > 0 then sZip = split(SHIPTOZIP, "-")(0)
		end if

		dim CorrelationID
		CorrelationID = objXMLDOM.getElementsByTagName("CorrelationID").item(0).text
				
		if len(SHIPTOCOUNTRYCODE) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sState) > 0 then SHIPTOCOUNTRYCODE = "CA"
		if inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sState) > 0 or SHIPTOCOUNTRYCODE = "CA" then
			strShiptype = "1"
			doNotProcess = 0
			nShipRate = shipcost4
		else
			strShiptype = ""
			doNotProcess = 0
			nShipRate = shipcost0
		end if

		if sState = "CA" then
			TAXAMT = round((nItemTotal - buysafeamount) * Application("taxMath"), 2)
		else
			TAXAMT = 0
		end if
		
		if doNotProcess = 0 then
			SQL = "SELECT * FROM we_Orders WHERE extOrderType=1 AND extOrderNumber='" & CorrelationID & "'"
			session("errorSQL") = SQL
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if RS.eof then
				' Don't do anything if an order already exists in the WE database with this Paypal Order Number
				SQL = "SET NOCOUNT ON; "
				SQL = SQL & "INSERT INTO CO_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,hearFrom,dateEntered) VALUES ("
				SQL = SQL & "'" & SQLquote(FIRSTNAME) & "', "
				SQL = SQL & "'" & SQLquote(LASTNAME) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET2) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCITY) & "', "
				SQL = SQL & "'" & sState & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOZIP) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCOUNTRYCODE) & "', "
				SQL = SQL & "'" & SQLquote(EMAIL) & "', "
				SQL = SQL & "'" & SQLquote(PHONENUM) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET2) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCITY) & "', "
				SQL = SQL & "'" & sState & "', "
				SQL = SQL & "'" & SQLquote(sZip) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCOUNTRYCODE) & "', "
				SQL = SQL & "'Paypal API', "
				SQL = SQL & "'" & now & "'); "
				SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
				SQL = SQL & "SET NOCOUNT OFF;"
				session("errorSQL") = SQL
				set RS = oConn.execute(SQL)
				
				dim newAccountID
				if not RS.eof then newAccountID = RS("newAccountID")
				
				SQL = "SET NOCOUNT ON; "
				SQL = SQL & "INSERT INTO we_Orders (store,accountid,extOrderType,extOrderNumber,ordersubtotal,ordershippingfee,orderTax,ordergrandtotal,shiptype,BuySafeCartID,BuySafeAmount,orderdatetime) VALUES ("
				SQL = SQL & "'2', "
				SQL = SQL & "'" & newAccountID & "', "
				SQL = SQL & "'1', "
				SQL = SQL & "'" & CorrelationID & "', "
				SQL = SQL & "'" & formatNumber(nItemTotal,2) & "', "
				SQL = SQL & "'" & nShipRate & "', "
				SQL = SQL & "'" & TAXAMT & "', "
				SQL = SQL & "'" & paymentAmount & "', "
				if strShiptype = "1" then
					SQL = SQL & "'First Class Int''l', "
				else
					SQL = SQL & "'First Class', "
				end if
				SQL = SQL & "'"& BuySafeCartID &"', "
				SQL = SQL & "'"& BuySafeAmount &"', "
				SQL = SQL & "'" & now & "')"
				SQL = SQL & "SELECT @@IDENTITY AS newOrderID; "
				SQL = SQL & "SET NOCOUNT OFF;"
				session("errorSQL") = SQL
				set RS = oConn.execute(SQL)
				if not RS.eof then newOrderID = RS("newOrderID")
				
				set ItemDetails = objXMLDOM.documentElement.getElementsByTagName("PaymentDetailsItem")
				for basketitem = 0 to ItemDetails.Length - 1
					set ItemDetailsNodes = ItemDetails.item(basketitem).childNodes
					itemID = ItemDetailsNodes(1).text
					itemPrice = ItemDetailsNodes(4).text
					if itemID = "DISCOUNT" then
						couponcode = ItemDetailsNodes(0).text
					else
						if itemID <> "" and itemID <> "1" and itemID <> "DISCOUNT" and prepInt(itemPrice) >= 0 then
							SQL = "INSERT INTO we_Orderdetails (orderid,itemid,quantity,price) VALUES ("
							SQL = SQL & "'" & newOrderID & "', "
							SQL = SQL & "'" & itemID & "', "
							SQL = SQL & "'" & ItemDetailsNodes(2).text & "','" & itemPrice & "')"
							
							oConn.execute(SQL)
						end if					
					end if
				next
				
				if couponcode <> "" then
					SQL = "SELECT couponid FROM CO_coupons WHERE promoCode='" & couponcode & "'"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 3, 3
					if not RS.eof then
						dim couponid
						couponid = RS("couponid")
					end if
					SQL = "UPDATE we_Orders SET couponid='" & couponid & "' WHERE orderid='" & newOrderID & "'"
					oConn.execute(SQL)
				end if
			end if
			
			grandtotal = paymentAmount + TAXAMT + nShipRate
			%>
			<form action="/cart/process/PaypalSOAP/DoExpressCheckoutPayment.asp" method="post" name="frmProcessOrder">
            <tr>
				<td valign="top" bgcolor="#FFFFFF" align="center" style="padding-top:5px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="api">
						<tr>
							<td bgcolor="#5B6775"><span style="color:#FFFFFF; font-family:Arial,Helvetica,sans-serif; font-size:14px; font-weight:bold;">&nbsp;Your&nbsp;Order</span></td>
						</tr>
						<tr>
							<td align="center" width="100%">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="1" cellpadding="3" class="mc-text">
												<tr>
													<td colspan="3" bgcolor="#E3E3E3"><b>Item&nbsp;Total:</b></td>
													<td align="right" bgcolor="#E3E3E3"><b><%=formatCurrency(nItemTotal)%></b></td>
												</tr>
												<tr>
													<td bgcolor="#E3E3E3"><b>Tax</b></td>
													<td colspan="2" bgcolor="#E3E3E3"><span id="CAtaxMsg"></span></td>
													<td align="right" bgcolor="#E3E3E3">
														<b><span id="CAtax"><%=formatCurrency(TAXAMT)%></span></b>
														<input type="hidden" name="nCATax" value="<%=TAXAMT%>">
													</td>
												</tr>
												<%if nDiscountTotal < 0 then%>
                                                <tr>
                                                    <td width="27%" bgcolor="#E3E3E3"><b>Discount</b></td>
                                                    <td width="65%" align="right" bgcolor="#E3E3E3" colspan="2"></td>
                                                    <td width="56" align="right" bgcolor="#E3E3E3">
                                                        <b><%=formatCurrency(nDiscountTotal)%></b>
                                                    </td>
                                                </tr>
                                                <%end if%>
												<tr>
													<td bgcolor="#E3E3E3"><b>Shipping &amp; Handling</b><br><span style="font-size:9px;">(Must press APPLY to update total.)</span></td>
													<td bgcolor="#E3E3E3">
														<%
														if strShiptype = "1" then
															%>
															<input type="hidden" name="shipcost4" value="<%=shipcost4%>"><input type="radio" name="shiptype" id="rad_ship1" value="4"<%if shiptype = "4" or shiptype = "3" then response.write " checked"%>><span id="Shipping4">USPS First Class Int'l (8-12 business days)</span><br>
															<input type="hidden" name="shipcost5" value="<%=shipcost5%>"><input type="radio" name="shiptype" id="rad_ship2" value="5"<%if shiptype = "5" then response.write " checked"%>><span id="Shipping5">USPS Priority Int'l (3-8 business days)</span>
															<%
														else
															%>
															<input type="hidden" name="shipcost0" value="<%=shipcost0%>"><input type="radio" name="shiptype" id="rad_ship1" value="0"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>><span id="Shipping0">USPS First Class (4-10 business days)</span><br>
															<input type="hidden" name="shipcost2" value="<%=shipcost2%>"><input type="radio" name="shiptype" id="rad_ship2" value="2"<%if shiptype = "2" then response.write " checked"%>><span id="Shipping2">USPS Priority Mail (2-4 business days)</span><br>
															<input type="hidden" name="shipcost3" value="<%=shipcost3%>"><input type="radio" name="shiptype" id="rad_ship3" value="3"<%if shiptype = "3" then response.write " checked"%>><span id="Shipping3">USPS Express Mail (1-2 business days)</span>
															<%
														end if
														on error resume next
														if sZip <> "" then response.write UPScode(sZip,sWeight,shiptype,nTotalQuantity,1,0)
														on error goto 0
														%>
													</td>
													<td bgcolor="#E3E3E3" align="center">
														<input type="hidden" name="sWeight" value="<%=sWeight%>">
														<input type="hidden" name="nTotalQuantity" value="<%=nTotalQuantity%>">
														<input type="button" onClick="javascript:updateGrandTotal();showShip();" name="recalculate" value="Apply" class="btnCart">
													</td>
													<td align="right" valign="top" bgcolor="#E3E3E3" style="color:#db3300;font-weight:bold;">
														<input type="hidden" name="shippingTotal" value="<%=nShipRate%>"><span id="shipcost" style="display:none;"></span>
													</td>
												</tr>
                                                <tr id="subTotal">
													<td colspan="3" bgcolor="#E3E3E3"><b>SubTotal</b></td>
													<td align="right" bgcolor="#E3E3E3">
														<b><%=formatNumber(nItemTotal+TAXAMT-nDiscountTotal,2)%></b>
													</td>
												</tr>
												<tr id="grandTotal" style="display:none;">
													<td colspan="3" bgcolor="#E3E3E3"><b>Grand Total</b></td>
													<td align="right" bgcolor="#E3E3E3">
														<input type="hidden" name="Token" value="<%=Token%>">
														<input type="hidden" name="PayerID" value="<%=PayerID%>">
														<input type="hidden" name="CorrelationID" value="<%=CorrelationID%>">
														<input type="hidden" name="grandTotal" value="<%=grandtotal%>">
														<input type="hidden" name="SHIPPINGAMT" value="<%=nShipRate%>">
														<input type="hidden" name="TAXAMT" value="<%=TAXAMT%>">
														<input type="hidden" name="intlShipping" value="<%=strShiptype%>">
														<input type="hidden" name="INVNUM" value="<%=newOrderID%>">
														<input type="hidden" name="sPromoCode" value="<%=couponcode%>">
                                                        <input type="hidden" name="itemTotal" value="<%=nItemTotal%>">
														<input type="hidden" name="paymentAmount" value="<%=paymentAmount%>">
														<input type="hidden" name="BuySafeAmount" value="<%=buysafeamount%>">
														<input type="hidden" name="BuySafeCartID" value="<%=BuySafeCartID%>">
														<b><span id="GrandTotal"><%=formatCurrency(grandtotal)%></span></b>
													</td>
												</tr>
												<tr>
													<td colspan="4" width="100%" align="center">
														<table id="tbl_step2" width="398" border="0" cellpadding="4" cellspacing="0" bgcolor="#F7F7F7" class="mc-text">
															<tr>
																<td style="padding-top:5px;" valign="top" colspan="2"><font style="color:#FF0000; font-weight:bold;">&nbsp;&nbsp;Shipping&nbsp;Address:</font></td>
															</tr>
															<tr>
																<td align="right"><strong><font color="red" size="+1">*</font>&nbsp;First&nbsp;Name:</strong></td>
																<td><input type="text" name="fname" size="27" value="<%=FIRSTNAME%>"></td>
															</tr>
															<tr>
																<td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Last&nbsp;Name:</strong></td>
																<td><input type="text" name="lname" size="20" value="<%=LASTNAME%>"></td>
															</tr>
															<tr>
																<td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Address:</strong></td>
																<td><input type="text" name="sAddress1" size="40" value="<%=SHIPTOSTREET%>"></td>
															</tr>
															<tr>
																<td align="right">&nbsp;</td>
																<td><input type="text" name="sAddress2" size="40" value="<%=SHIPTOSTREET2%>"></td>
															</tr>
															<tr>
																<td align="right"><strong><font color="red" size="+1">*</font>&nbsp;City:</strong></td>
																<td><input type="text" name="sCity" size="40" value="<%=SHIPTOCITY%>"></td>
															</tr>
															<tr>
																<td align="right"><strong><font color="red" size="+1">*</font>&nbsp;State:</strong></td>
																<td>
																	<select name="sState" onChange="changeTax(this.value);">
																		<%getStates(sState)%>
																	</select>
																</td>
															</tr>
															<tr>
																<td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Zip/Postal:</strong></td>
																<td>
                                                                	<input type="hidden" name="SHIPTOCOUNTRYCODE" value="<%=SHIPTOCOUNTRYCODE%>">
                                                                	<input type="hidden" name="EMAIL" value="<%=EMAIL%>">
                                                                    <!--<input type="hidden" name="sZip" value="<%=sZip%>">-->
                                                                    <input type="text" name="sZip" value="<%=sZip%>" />
                                                                    <!--<strong><font color="red"><%=sZip%></font></strong>&nbsp;&nbsp;<a href="http://www.cellularoutfitter.com/cart/basket.asp">change</a></td>-->
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td colspan="4" align="center" bgcolor="#E3E3E3">
														<p><input type="checkbox" name="chkOptMail" value="Y" checked>Please include me in the Cellular Outfitter VIP mailing list for periodic coupon code offers from CellularOutfitter.com via email.</p>
													</td>
												</tr>
												<tr>
													<td colspan="4" align="center" bgcolor="#E3E3E3">
														<div id="submitBttn" style="font-weight:bold; font-size:18px; color:#000;"><img onClick="return CheckSubmit();" src="https://www.cellularoutfitter.com/images/cart/button_submit_order.jpg" width="212" height="40" alt="Submit Order" style="cursor: hand;"></div>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</form>            
			<%
		else
			%>
			<tr>
				<td colspan="2">
					<h3 align="center"><br><br><br><br>We're sorry, but CellularOutfitter does not ship outside of the U.S., its territories, and Canada.</h3>
				</td>
			</tr>
			<%
		end if
	else
		%>
		<tr>
			<td colspan="2">
				<h3 align="center"><br><br><br><br>We're sorry!<br>Your Paypal payment was declined.</h3>
				<p align="center"><a href="http://www.cellularoutfitter.com/cart/basket.asp">PLEASE CLICK HERE TO START AGAIN.</a><br><br><br><br></p>
			</td>
		</tr>
		<%
	end if
end if

if err.number <> 0 then
	response.redirect "PaypalError.asp"
	response.end
end if

'CLEAN UP EVERYTHING
set objXMLDOC = nothing
set objXMLDOM = nothing
set oNode = nothing
%>
<script>
window.WEDATA.pageType = 'checkout';
window.WEDATA.pageData = {
	discountTotal: '<%=ndiscountTotal%>',
	subTotal: '<%=formatNumber(nItemTotal+TAXAMT-nDiscountTotal,2) %>',
	shipping: '<%= ShipRate %>',
	amount: '<%=nAmount%>',
	tax: '<%=TAXAMT%>',
	cartItems: []
};
</script>
<!--#include virtual="/includes/template/bottom_cart.asp"-->

<script language="javascript">
<!-- Begin
function changeValue(span,val) {
	if (document.all) {
		eval(span).innerHTML = val;
	} else if (document.getElementById) {
		document.getElementById(span).innerHTML = val;
	}
}

function changeTax(state) {
	var nCATax = CurrencyFormatted((document.frmProcessOrder.itemTotal.value - document.frmProcessOrder.BuySafeAmount.value) * <%=Application("taxMath")%>);
	if (state == "CA") {
		changeValue("CAtaxMsg","Additional <%=Application("taxDisplay")%>% Tax for Shipments Within CA");
		changeValue("CAtax","$" + nCATax);
		document.frmProcessOrder.nCATax.value = nCATax;
	} else {
		changeValue("CAtaxMsg","");
		changeValue("CAtax","$0.00");
		document.frmProcessOrder.nCATax.value = 0;
	}
	if (state == "AB" || state == "BC" || state == "MB" || state == "NB" || state == "NL" || state == "NS" || state == "NT" || state == "NU" || state == "ON" || state == "PE" || state == "QC" || state == "SK" || state == "YT") {
		changeValue("Shipping4","USPS First Class Int'l (8-12 business days)");
		document.frmProcessOrder.shipcost4.value = "<%=shipcost4%>";
		document.frmProcessOrder.shipcost5.value = "<%=shipcost5%>";
		changeValue("Shipping5","USPS Priority Int'l (3-8 business days)");
		/*
		if (document.frmProcessOrder.shiptype[2].checked == true) {
			document.frmProcessOrder.shiptype[2].checked = false;
			document.frmProcessOrder.shiptype[1].checked = true;
		}
		*/
		document.frmProcessOrder.shiptype[0].checked = true;
		for (var i = 2; i < shippingOptions; i++) {
			document.frmProcessOrder.shiptype[i].disabled = true;
			changeValue("Shipping" + (i+1),"");
		}
		document.frmProcessOrder.intlShipping.value = "1";
	} else {
		changeValue("Shipping0","USPS First Class (4-10 business days)");
		document.frmProcessOrder.shipcost2.value = "<%=shipcost2%>";
		changeValue("Shipping2","USPS Priority Mail (2-4 business days)");
		document.frmProcessOrder.shiptype[2].disabled = false;
		changeValue("Shipping3","USPS Express Mail (1-2 business days)");
		document.frmProcessOrder.intlShipping.value = "0";
	}
	updateGrandTotal();
}

function updateGrandTotal() {
	var a = document.frmProcessOrder.shiptype.length;
	for (var i = 0; i < a; i++) {
		if (document.frmProcessOrder.shiptype[i].checked) {
			var useVal = document.frmProcessOrder.shiptype[i].value
			var b = eval("document.frmProcessOrder.shipcost" + useVal + ".value")
			b = b.replace("$","")
			var shippingCost = b;
			document.frmProcessOrder.shippingTotal.value = shippingCost;
			document.frmProcessOrder.SHIPPINGAMT.value = shippingCost;
			document.getElementById("shipcost").innerHTML = "$" + shippingCost
			//changeValue("shipcost","$" + shippingCost);
		}
	}
	var GrandTotal = Number(document.frmProcessOrder.paymentAmount.value) + Number(document.frmProcessOrder.nCATax.value) + Number(b);
	changeValue("GrandTotal","$" + CurrencyFormatted(GrandTotal));
	document.frmProcessOrder.grandTotal.value = CurrencyFormatted(GrandTotal);
}

function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

function CheckSubmit() {
	var f = document.frmProcessOrder;
	bValid = true;
	
	CheckValidNEW(f.fname.value, "Your First Name is a required field!");
	CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
	CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
	CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
	sString = f.sState.options[f.sState.selectedIndex].value;
	CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
	CheckValidNEW(f.sZip.value, "Zip/Postal is a requried field!");
	
	if (bValid) {
		var a = f.shiptype.length;
		for (var i = 0; i < a; i++) {
			if (f.shiptype[i].checked) {
				var shiptype = f.shiptype[i].value;
			}
		}
		var shiprate = eval('document.frmProcessOrder.shipcost' + shiptype);
		f.SHIPPINGAMT.value = shiprate.value;
		if (f.sState.options[f.sState.selectedIndex].value == "CA") {
			f.TAXAMT.value = <%=round((paymentAmount - buysafeamount) * Application("taxMath"), 2)%>;
		} else {
			f.TAXAMT.value = 0;
		}
		document.getElementById("submitBttn").innerHTML = "Please wait while<br />we process your order"
		f.submit();
	}
	return false;
}

function showShip() {
	document.getElementById("shipcost").style.display = '';
	document.getElementById("grandTotal").style.display = '';
	document.getElementById("subTotal").style.display = 'none';
}
//  End -->
</script>
<!-- <%=request.ServerVariables("REMOTE_ADDR")%> -->