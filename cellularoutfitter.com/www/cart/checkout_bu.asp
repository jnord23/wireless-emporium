<%
dim useHttps : useHttps = 1
response.buffer = true
response.CacheControl = "no-cache"
SEtitle = "Cheap Cell Phone Accessories - Cellular Phone Accessories - CellularOutfitter.com"
SEdescription = "Buy cheap Cell Phone Accessories at Wholesale Prices to the Public.  Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more.  Cellular Accessories at the Lowest Prices Online Guaranteed."
SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"
pageTitle = "checkout"
mvtName = "checkout"

dim szip, sPromoCode, sWeight
szip = request.form("shipZip")
'sWeight = request.form("sWeight")

session("szip") = szip
'session("sWeight") = sWeight
%>

<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/cart/includes/inc_makeNum.asp"-->
<!--#include virtual="/cart/includes/inc_getStates.asp"-->
<!--#include virtual="/includes/asp/inc_getMySession.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%
HTTP_REFERER = lcase(request.servervariables("HTTP_REFERER"))



dim buysafeamount, WantsBondField, ShoppingCartId
if request.form("buysafeamount") = "" then
	buysafeamount = 0
	WantsBondField = "false"
else
	buysafeamount = cDbl(request.form("buysafeamount"))
	WantsBondField = request.form("WantsBondField")
end if
ShoppingCartId = request.form("buysafecartID")

nCATax = 0
if request.form("submitted") = "submitted" then
	newEmail = request.form("newEmail")
	pword1 = request.form("pword1")
	pword2 = request.form("pword2")
	fname = request.form("fname")
	lname = request.form("lname")
	email = request.form("email")
	phone = request.form("phone")
	saddress1 = request.form("saddress1")
	saddress2 = request.form("saddress2")
	scity = request.form("scity")
	sstate = request.form("sstate")
	szip = request.form("szip")
	bAddress1 = request.form("bAddress1")
	bAddress2 = request.form("bAddress2")
	bCity = request.form("bCity")
	bState = request.form("bState")
	bZip = request.form("bZip")
	shiptype = request.form("shiptype")
	PaymentType = request.form("PaymentType")
	cc_cardType = request.form("cc_cardType")
	cc_cardOwner = request.form("cc_cardOwner")
	cardNum = request.form("cardNum")
	cc_month = request.form("cc_month")
	cc_year = request.form("cc_year")
        cc_secCode = request.form("secCode")
	
	'B version
	b_fname = request.form("b_fname")
	b_lname = request.form("b_lname")
	b_email = request.form("b_email")
	b_phone = request.form("b_phone")
	b_saddress1 = request.form("b_saddress1")
	b_saddress2 = request.form("b_saddress2")
	b_scity = request.form("b_scity")
	b_sstate = request.form("b_sstate")
	b_szip = request.form("b_szip")
	b_bAddress1 = request.form("b_bAddress1")
	b_bAddress2 = request.form("b_bAddress2")
	b_bCity = request.form("b_bCity")
	b_bState = request.form("b_bState")
	b_bZip = request.form("b_bZip")
	b_cc_cardType = request.form("b_cc_cardType")
	b_cc_cardOwner = request.form("b_cc_cardOwner")
	b_cardNum = request.form("b_cardNum")
	b_cc_month = request.form("b_cc_month")
	b_cc_year = request.form("b_cc_year")	
	b_cc_secCode = request.form("b_secCode")
end if

if request.form("promo") <> "" then
	sPromoCode = request.form("promo")
else
	sPromoCode = session("promocode")
end if

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	SQL = "SELECT * FROM CO_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc
		'FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		'FreeItemPartNumber = RS("FreeItemPartNumber")
	else
		sPromoCode = ""
	end if
	call fCloseConn()
end if

'Start basket grab
dim htmBasketRows, WEhtml, EBtemp, EBxml
htmBasketRows = ""
WEhtml = ""
EBtemp = ""
EBxml = ""

dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, nTotalQuantity
dim strItemCheck, strQty, FreeProductInCart
dim SquaretradeItemName, SquaretradeItemPrice, SquaretradeItemCondition

nProdIdCount = 0
nTotalQuantity = 0
shippingQty = 0
strItemCheck = ""
strQty = ""
FreeProductInCart = 0
specialAmt = 0

call fOpenConn()
' New cart abandonment tracking added 9/20/2010 by MC
SQL = "UPDATE ShoppingCart SET DateCheckout = '" & now & "' WHERE store = 2 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
oConn.execute SQL

sql = 	"SELECT c.brandName, d.modelName, (select top 1 inv_qty from we_items where partNumber = b.partNumber order by master desc, inv_qty desc) as inv_qty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, " & vbcrlf & _
		"B.itemDesc_CO, B.itemPic_CO, B.price_CO, B.price_retail, B.Condition, isnull(B.itemWeight, 0) itemWeight, B.NoDiscount, a.customcost FROM ShoppingCart A INNER JOIN we_items B ON A.itemID = " & vbcrlf & _
		"B.itemID left join we_brands c on b.brandID = c.brandID left join we_models d on b.modelID = d.modelID WHERE A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)" & vbcrlf & _
		" union " & vbcrlf & _
		"SELECT '' as brandName, '' as modelName, 100 as inv_qty, A.itemID, A.qty, B.modelID, 3 as typeID, B.musicSkinsID, B.artist + ' ' + B.designName, B.image as itemPic_co, B.price_CO, " & vbcrlf & _
		"B.msrp as price_retail, 1 as Condition, 1 as itemWeight, 1 as NoDiscount, a.customcost FROM ShoppingCart A INNER JOIN we_items_musicskins B ON A.itemID=B.id WHERE " & vbcrlf & _
		"A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
'response.write "<pre>" & sql & "</pre>"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

do until RS.eof
	brandName = RS("brandName")
	modelName = RS("modelName")
	partNumber = RS("partNumber")
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		sWeight = cdbl(sWeight) + cdbl(rs("itemWeight"))
		sItemName = insertDetails(RS("itemDesc_CO"))
		sItemPic = RS("itemPic_CO")
		sItemPrice = RS("price_CO")
		customCost = rs("customCost")
		if not isnull(customCost) then sItemPrice = customCost
		sRetailPrice = RS("price_retail")
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		'if RS("typeID") = 16 then
		'	PhonePurchased = 1
		'	SquaretradeItemName = sItemName
		'	SquaretradeItemPrice = sItemPrice
		'	SquaretradeItemCondition = RS("Condition")
		'end if
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_CO > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		
		'Free Product Offer code added 2/1/2010 by MC
		if inStr(RS("PartNumber"),"scr-") > 0 then FreeProductInCart = 1
		'session("errorSQL") = "nItemTotal = " & nItemTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		
		if instr(RS("PartNumber"),"PHN-") < 1 and RS("nodiscount") = 0 and ucase(left(RS("PartNumber"), 2)) <> "BT" then 
			specialAmt = specialAmt + (sItemPrice * nProdQuantity)
		end if
		
		nSubTotal = nSubTotal + (sItemPrice * nProdQuantity)
		ntotalQuantity = ntotalQuantity + RS("qty")
		if left(RS("PartNumber"),4) <> "WCD-" and isnull(customCost) then
			shippingQty = shippingQty + RS("qty")
		end if
		
		if nProdIdCheck => 1000000 then
			sql = "select preferredImg, image, defaultImg from we_items_musicSkins where id = " & nProdIdCheck
			session("errorSQL") = sql
			set msRS = oConn.execute(sql)
			if not msRSEOF then
				preferredImg = msRS("preferredImg")
				itempic_CO = msRS("image")
				defaultImg = msRS("defaultImg")
				if isnull(preferredImg) then																					
					if hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsLarge", itempic_CO) then
						sql = "update we_items_musicSkins set preferredImg = 1 where id = " & itemID
						session("errorSQL") = sql
						oConn.execute(sql)
						useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
					elseif hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault", defaultImg) then
						sql = "update we_items_musicSkins set preferredImg = 2 where id = " & itemID
						session("errorSQL") = sql
						oConn.execute(sql)
						useImg = "http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/" & defaultImg
					else
						useImg = "/productPics/thumb/imagena.jpg"
					end if
				elseif preferredImg = 1 then
					useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
				elseif preferredImg = 2 then
					useImg = "/productpics/musicSkins/musicSkinsDefault/" & defaultImg
				else
					useImg = "/productPics/thumb/imagena.jpg"
				end if
			end if
			msRS = null
		else
			useImg = "/productpics/homepage65/" & sItemPic
		end if
		htmBasketRows = htmBasketRows & fWriteBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),useImg)
										
		WEhtml = WEhtml & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		'PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf
		
		'eBillme variables
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_number_" &  nProdIdCount & """ value=""" & nProdIdCheck & """>" & vbcrlf

		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_qty_" &  nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_price_" &  nProdIdCount & """ value=""" & sItemPrice & """>" & vbcrlf
		EBxml = EBxml & "			<itemdetail>" & vbcrlf
		EBxml = EBxml & "				<id>" & nProdIdCheck & "</id>" & vbcrlf
		EBxml = EBxml & "				<name>" & sItemName & "</name>" & vbcrlf
		EBxml = EBxml & "				<quantity>" & nProdQuantity & "</quantity>" & vbcrlf
		EBxml = EBxml & "				<itemcost>" & formatnumber(RS("price_CO"), 2) & "</itemcost>" & vbcrlf
		EBxml = EBxml & "			</itemdetail>"
	end if
	RS.movenext
loop
RS.close
set RS = nothing
call fCloseConn()
' END basket grab

if nProdIdCount < 1 then
'basket currently empty
	response.redirect("/sessionexpired.asp")
	response.end
end if
' END basket grab

session("sWeight") = sWeight

'#### Upfront Shipping Costs ####
dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
'shipcost0 = formatCurrency(Application("shippingCost"))
'shipcost2 = formatCurrency(9.99)
'shipcost3 = formatCurrency(22.99)
'shipcost4 = formatCurrency(7.99)
'shipcost5 = formatCurrency(14.99)

'#### Shady Shipping Costs ####
'shipcost0 = formatCurrency(3.96 + (1.99 * nTotalQuantity))
'shipcost2 = formatCurrency(7.96 + (1.99 * nTotalQuantity))
'shipcost3 = formatCurrency(20.96 + (1.99 * nTotalQuantity))
'shipcost4 = formatCurrency(4.96 + (2.99 * nTotalQuantity))
'shipcost5 = formatCurrency(11.96 + (2.99 * nTotalQuantity))

'#### changed on 4/19/2012 5:00PM by Terry
shipcost0 = formatCurrency(4.00 + (1.99 * shippingQty))
shipcost2 = formatCurrency(8.00 + (1.99 * shippingQty))
shipcost3 = formatCurrency(21.00 + (1.99 * shippingQty))

'Canadian shipping Cost
shipcost4 = formatCurrency(6.00 + (2.99 * shippingQty))
shipcost5 = formatCurrency(19.00 + (2.99 * shippingQty))

'response.write "shiptype:" & shiptype & "<br>"
if request.form("refreshed") = "1" then
	if (len(sstate) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sstate) > 0) or (len(b_sstate) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",b_sstate) > 0) then
		sSCountry = "CANADA"
	else
		sSCountry = ""
	end if
	select case shiptype
		case "2"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				shiptype = "5"
				nShipRate = shipcost5
			else
				strShiptype = "0"
				nShipRate = shipcost2
			end if
		case "3"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				shiptype = "5"
				nShipRate = shipcost5
			else
				strShiptype = "0"
				nShipRate = shipcost3
			end if
		case "4"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				nShipRate = shipcost4
			else
				strShiptype = "0"
				shiptype = "0"
				nShipRate = shipcost0
			end if
		case "5"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				nShipRate = shipcost5
			else
				strShiptype = "0"
				shiptype = "2"
				nShipRate = shipcost2
			end if
		case "0"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				shiptype = "4"
				nShipRate = shipcost4
			else
				strShiptype = "0"
				nShipRate = shipcost0
			end if
		case else
			nShipRate = formatCurrency(request.form("shipcost" & shiptype))
	end select
	if sstate = "CA" or b_sstate = "CA" then
		nCATax = round(nSubTotal * Application("taxMath"), 2)
	else
		nCATax = 0
	end if
end if

if spendThisMuch = 1 then
	if nSubTotal => stm_total then
		sql = "select b.brandName, c.modelName, a.* from we_items a left join we_brands b on a.brandID = b.brandID left join we_models c on a.modelID = c.modelID where itemID = " & stm_freeItem
		set freeItemRS = Server.CreateObject("ADODB.Recordset")
		freeItemRS.open sql, oConn, 0, 1
		
		free_brandName = prepStr(freeItemRS("brandName"))
		free_modelName = prepStr(freeItemRS("modelName"))
		free_partNumber = prepStr(freeItemRS("partNumber"))
		free_itemDesc = prepStr(freeItemRS("itemDesc"))
		free_itemID = prepInt(freeItemRS("itemID"))
		free_itemPic = freeItemRS("itemPic_CO")
		free_price = freeItemRS("price_co")
	
		htmBasketRows = htmBasketRows & fWriteBasketRow(free_itemID,free_itemDesc,0,1,free_itemPic)
	end if
end if
%>
<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<link rel="stylesheet" type="text/css" href="/includes/css/checkoutBase.css" />
<div id="account_nav" style="display:none;"></div>
<td>
    <form id="id_frmProcessOrder" action="/cart/checkout.asp" method="post" name="frmProcessOrder">
    <div style="width:980px; text-align:left; padding-top:10px;">
    	<div id="floatingTimer" class="flTimer">
        	<div class="tb">Time left to complete purchase:</div>
            <div class="tb activeTimer_b">
                <div id="saleMinutes_fl" class="fl checkoutTimer_b">10</div>
                <div class="fl checkoutTimer_b">:</div>
                <div id="saleSeconds_fl" class="fl checkoutTimer_b">00</div>
            </div>
        </div>
        <div id="mvt_topBanner" style="float:left; width:100%; text-align:center; padding:5px 0px 5px 0px;">
        	<img src="/images/cart/110-banner.jpg" border="0" width="980" height="78" usemap="#secureBanner" />
            <map name="secureBanner">
                <area shape="rect" coords="0,0,562,78" href="javascript:openLowPricePopup();" alt="110% LOW PRICE GUARANTEE">
                <area shape="rect" coords="563,0,980,78" href="javascript:openReturnPopup();" alt="90-DAY RETURN POLICY">
            </map>
        </div>
        <div style="float:left; width:100%; text-align:center; border-top:1px dotted #000; border-bottom:1px dotted #000; padding:15px 0px 15px 0px;">
        	<div style="float:left; margin-left:250px; padding:0px 10px 0px 0px;"><img src="/images/checkout/lock.png" border="0" /></div>
        	<a href="/cart/basket.asp" style="font-size:26px; color:#999; font-weight:normal;"><div style="float:left; font-size:28px; color:#999; padding-top:6px;">Shopping Cart</div></a>
        	<div style="float:left; padding:0px 10px 0px 10px;"><img src="/images/checkout/dotted-div.png" border="0" /></div>
        	<div style="float:left; font-size:26px; color:#02659E; padding-top:6px;">Order</div>
        	<div style="float:left; padding:0px 10px 0px 10px;"><img src="/images/checkout/dotted-div.png" border="0" /></div>
        	<div style="float:left; font-size:26px; color:#999; padding-top:6px;">Receipt</div>
		</div>
        <div style="float:left; width:100%; text-align:center;">
        	<div class="tb" style="margin:10px auto 3px auto;">
            	<div class="fl"><img src="/images/checkout/clock.png" border="0" width="20" height="20" /></div>
                <div class="fl holdingTxt">Note: Due to current demand at these prices and limited quantities, we are holding the items in your cart for </div>
        	    <div class="fl activeTimer">
                    <div id="saleMinutes" class="fl checkoutTimer">10</div>
                    <div class="fl checkoutTimer">:</div>
		    		<div id="saleSeconds" class="fl checkoutTimer">00</div>
                </div>
                <div class="fl holdingTxt2"> minutes.</div>
            </div>
        </div>
        <div style="float:left; width:100%; padding-top:10px;">
            <!-- shipping & handling fees -->
            <div style="float:left; width:300px; border:1px solid #ccc; border-bottom-left-radius:4px; border-bottom-right-radius:4px; margin-right:18px;">
                <div style="float:left; width:100%; border-bottom:1px solid #ccc; background-color:#f4f4f4; text-align:center; font-size:14px; font-weight:bold; color:#222; padding:5px 0px 5px 0px;">Shipping & Handling Fees</div>
                <div style="float:left; width:290px; font-size:12px; color:#333; padding:5px;">
                    <div style="clear:both;" id="id_shippingDetails">
                    <%
                    if strShiptype = "1" then
                        %>
                        <input type="hidden" name="shipcost4" value="<%=shipcost4%>"><input type="radio" name="shiptype" autocomplete="off" value="4"<%if shiptype = "4" or shiptype = "3" then response.write " checked"%>>USPS First Class Int'l (8-12 business days)<br>
                        <input type="hidden" name="shipcost5" value="<%=shipcost5%>"><input type="radio" name="shiptype" autocomplete="off" value="5"<%if shiptype = "5" then response.write " checked"%>>USPS Priority Int'l (3-8 business days)
                        <%
                    else
                        %>
                        <input type="hidden" name="shipcost0" value="<%=shipcost0%>"><input type="radio" name="shiptype" autocomplete="off" value="0"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>>USPS First Class (4-10 business days)<br />
                        <input type="hidden" name="shipcost2" value="<%=shipcost2%>"><input type="radio" name="shiptype" autocomplete="off" value="2"<%if shiptype = "2" then response.write " checked"%>>USPS Priority Mail (2-4 business days)<br />
                        <input type="hidden" name="shipcost3" value="<%=shipcost3%>"><input type="radio" name="shiptype" autocomplete="off" value="3"<%if shiptype = "3" then response.write " checked"%>>Express Mail (1-2 business days) - <%=shipcost3%>
                        <div style="clear:both;" id="id_addr_shippingDetails">
                        <%
                        on error resume next
                        if sZip <> "" then response.write UPScode(sZip,sWeight,shiptype,nTotalQuantity,0,0)
                        if sZip = "" and b_sZip <> "" then response.write UPScode(b_sZip,sWeight,shiptype,nTotalQuantity,0,0)
                        on error goto 0
                        shipcost = request.form("shipcost" & request.form("shiptype"))
                        %>
                        </div>
                        <%
                    end if
                    %>
                    </div>
                    <input type="hidden" name="sWeight" value="<%=sWeight%>">
                    <input type="hidden" name="refreshed" value="1">
                    <input type="hidden" name="nTotalQuantity" value="<%=nTotalQuantity%>">
                    <div id="mvt_apply1" style="clear:both; text-align:center; padding:5px 0px 5px 0px;">
                        <input type="image" name="recalculate" src="/images/checkout/btn-apply.png" width="139" height="19" border="0" alt="Apply" onClick="javascript:fnRecalculate();">
                    </div>
                    <div id="mvt_apply2" style="clear:both; text-align:center; padding:5px 0px 5px 0px;">
                        <input type="image" name="recalculate" src="/images/cart/button_apply.jpg" width="116" height="20" border="0" alt="Apply" onClick="javascript:fnRecalculate();">
                    </div>
                    <div style="clear:both; text-align:center; font-size:9px; color:#444;">
                        (* MUST press APPLY to update total.)<br />
                        (Please enter zip code below for additional shipping options.)
                    </div>
    
                    <input type="hidden" name="shippingTotal" value="<%=nShipRate%>">
                </div>
            </div>
            <!--// shipping & handling fees -->        
            <!-- item review -->
            <div style="float:left; width:660px;">
                <div style="float:left; width:100%; background:url(/images/cart/headerbar-blue.jpg) repeat-x; height:23px; font-size:12px; color:#fff; padding-top:5px; border-top-left-radius:5px; border-top-right-radius:5px;">
                    <div style="float:left; width:330px; text-align:left; font-size:14px; font-weight:bold;">&nbsp; Item</div>
                    <div style="float:left; width:110px; text-align:center; font-size:14px; font-weight:bold;">Qty</div>
                    <div style="float:left; width:110px; text-align:center; font-size:14px; font-weight:bold;">Price</div>
                    <div style="float:left; width:110px; text-align:center; font-size:14px; font-weight:bold;">Total</div>
                </div>
                <%=htmBasketRows%>
                <div style="clear:both; height:5px;"></div>
                <%
                if sstate = "CA" or b_sstate = "CA" then
                    nCATax = round(nSubTotal * Application("taxMath"), 2)
                else
                    nCATax = 0
                end if
                
                nAmount = nShipRate + nCATax + nSubTotal + buysafeamount
                if date > cdate("9/30/2011") then specialAmt = 0 end if
                spend2Get = 0
                
                if spend2Get = 1 then																						
                    if sPromoCode = "" and specialAmt >= 30 then
                        sPromoCode = "specialDeal"
                        nSubTotal = nSubTotal - 5
                        nAmount = nAmount - 5
                    end if
                end if	
                
                if sPromoCode <> "" then
                    session("promocode") = sPromoCode
                    if sPromoCode <> "specialDeal" then
                    %>
                    <div style="float:left; width:100%; padding-top:5px; text-align:right; font-size:16px; color:#333;">
                        <b>Promotions:</b>&nbsp;&nbsp;&nbsp;<%=couponDesc%> &nbsp;  &nbsp;  &nbsp; 
                        <input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
                        <input type="hidden" name="nPromo" value="<%=couponid%>">
                    </div>
                    <%
                    end if
                end if
                %>
                <div style="float:left; width:100%; padding-top:5px;">
                    <div style="float:right; width:90px; font-size:16px; font-weight:bold; color:#222; text-align:right; padding-right:30px;"><%=formatCurrency(nSubTotal)%></div>
                    <div style="float:right; width:200px; font-size:16px; color:#333; text-align:right;">Item Total:</div>
                </div>
                <%
                if nCATax > 0 then
                    %>
                    <div style="float:left; width:100%; padding-top:5px;">
                        <div style="float:right; width:90px; font-size:16px; font-weight:bold; color:#222; text-align:right; padding-right:30px;"><%=formatCurrency(nCATax)%></div>
                        <div style="float:right; width:400px; font-size:16px; color:#333; text-align:right;">Additional <%=Application("taxDisplay")%>% Tax for Shipments Within CA:</div>
                    </div>
                    <%
                end if
                
                if WantsBondField = "true" and buysafeamount > 0 then
                    %>
                    <div style="float:left; width:100%; padding-top:5px;">
                        <div style="float:right; width:90px; font-size:16px; font-weight:bold; color:#222; text-align:right; padding-right:30px;">
                            <%=formatCurrency(buysafeamount)%>
                            <input name="WantsBondField" value="<%=WantsBondField%>" type="hidden">
                        </div>
                        <div style="float:right; width:400px; font-size:16px; color:#333; text-align:right;">buySAFE Bond:</div>
                    </div>
                    <%
                end if
    
                if discountTotal > 0 then
                %>
                    <div style="float:left; width:100%; padding-top:5px;">
                        <div style="float:right; width:90px; font-size:16px; font-weight:bold; color:#900; text-align:right; padding-right:30px;"><%=formatCurrency(discountTotal)%></div>
                        <div style="float:right; width:200px; font-size:16px; color:#333; text-align:right;">Discount Total:</div>
                    </div>            
                <%
                end if
    
                if nShipRate <> "" then
                %>
                <div style="float:left; width:100%; padding-top:5px;">
                    <div style="float:right; width:90px; font-size:16px; font-weight:bold; color:#222; text-align:right; padding-right:30px;"><%=formatCurrency(nShipRate)%></div>
                    <div style="float:right; width:200px; font-size:16px; color:#333; text-align:right;">Shipping & Handling:</div>
                </div>
                <div style="float:left; width:100%; padding-top:5px;">
                    <div style="float:right; width:90px; text-align:right; padding-right:30px;"><span style="font-size:16px; font-weight:bold; color:#222;" id="GrandTotal"><%=formatCurrency(nAmount)%></span></div>
                    <div style="float:right; width:200px; font-size:16px; color:#333; text-align:right;">Grand Total:</div>
                </div>
                <%end if%>
                <input type="hidden" name="nCATax" value="<%=nCATax%>">
                <input type="hidden" name="subTotal" value="<%=nSubTotal%>">
                <input type="hidden" name="grandTotal" value="<%=nAmount%>">
                <input type="hidden" name="buysafeamount" value="<%=round(buysafeamount,2)%>">
                <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
            </div>
            <!--// item review -->
        </div>
        <!-- billing information -->
        <div style="float:left; width:100%; padding-top:20px;">
            <div style="float:left; width:100%; ">
                <div style="float:left; width:50px;"><img src="/images/checkout/icon-indicator.png" border="0" /></div>
                <div style="float:left; font-size:28px; font-weight:bold; color:#333;">Billing Information</div>
            </div>
            <div style="float:left; width:100%; border-bottom:1px solid #ccc; padding-bottom:3px;">
                <div style="float:left; width:50px;">&nbsp;</div>        
                <div style="float:left; width:400px; font-size:14px; font-weight:bold; color:#333;">Please enter your billing information below.</div>
                <div style="float:left; width:200px; font-style:italic; font-size:14px; color:#999;">*Required Field</div>
            </div>
            <!-- A version billing -->
            <div id="mvt_billing1" style="float:left; width:100%; padding-top:15px;">
                <table width="970" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td width="50%" valign="top">
                            <table id="tbl_step2" width="100%" border="0" cellpadding="2" cellspacing="0" align="center" class="mc-text">
                                <tr>
                                    <td width="15" align="center" valign="top" class="blue-star">&nbsp;</td>
                                    <td valign="top" colspan="2" align="left"><p style="font-weight:bold;">Shipping&nbsp;Address:</p></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><img src="/images/spacer.gif" width="1" height="20" border="0"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td width="30" valign="top" class="checkout-text" align="left">First&nbsp;Name:</td>
                                    <td align="left"><input type="text" name="fname" size="40" value="<%=fname%>"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td valign="top" class="checkout-text" align="left">Last&nbsp;Name:</td>
                                    <td align="left"><input type="text" name="lname" size="40" value="<%=lname%>"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td valign="top" class="checkout-text" align="left">Address&nbsp;1:</td>
                                    <td align="left"><input type="text" name="sAddress1" size="40" value="<%=saddress1%>"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">&nbsp;</td>
                                    <td valign="top" class="checkout-text" align="left">Address&nbsp;2:</td>
                                    <td align="left"><input type="text" name="sAddress2" size="40" value="<%=saddress2%>"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td valign="top" class="checkout-text" align="left">City:</td>
                                    <td align="left"><input type="text" name="sCity" size="40" value="<%=scity%>"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td valign="top" class="checkout-text" align="left">State:</td>
                                    <td align="left">
                                        <select name="sState" onchange="getNewShippingByState(this.value)">
                                            <%getStates(sstate)%>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td valign="top" class="checkout-text" align="left">Postal&nbsp;Code:</td>
                                    <td align="left"><input type="text" name="sZip" value="<%=szip%>" onkeyup="getNewShipping()"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td valign="top" class="checkout-text" align="left">Phone:</td>
                                    <td align="left"><input type="text" name="phone" size="40" value="<%=phone%>"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td valign="top" class="checkout-text" align="left">Email:</td>
                                    <td align="left"><input type="text" name="email" size="40" value="<%=email%>"></td>
                                </tr>
                            </table>
                        </td>
                        <td width="50%" valign="top">
                            <table id="billing_info" width="100%" border="0" cellpadding="2" cellspacing="0" align="center" class="mc-text">
                                <tr>
                                    <td width="15" valign="top" class="blue-star">&nbsp;</td>
                                    <td valign="top" colspan="2" align="left"><p style="font-weight:bold;">Billing&nbsp;Address:</p></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="blue-star"><input id="sameaddress" type="checkbox" name="sameaddress" value="yes" onClick="javascript:ShipToBillPerson();"<%if request.form("sameaddress")="yes" then response.write " checked"%>></td>
                                    <td colspan="2" class="checkout-blue" align="left">Check if Billing Address is the same as Shipping Address.</td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td width="30" valign="top" class="checkout-text" align="left">Address&nbsp;1:</strong></td>
                                    <td align="left"><input name="bAddress1" size="40" value="<%=bAddress1%>" type="text"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">&nbsp;</td>
                                    <td valign="top" class="checkout-text" align="left">Address&nbsp;2</td>
                                    <td align="left"><input name="bAddress2" size="40" value="<%=bAddress2%>" type="text"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td valign="top" class="checkout-text" align="left">City:</strong></td>
                                    <td align="left"><input name="bCity" size="40" value="<%=bCity%>" type="text"></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td valign="top" class="checkout-text" align="left">State:</strong></td>
                                    <td valign="top" align="left">
                                        <select name="bState">
                                            <%getStates(bState)%>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="blue-star">*</td>
                                    <td valign="top" class="checkout-text" align="left">Postal&nbsp;Code:</strong></td>
                                    <td style="padding-bottom:5px;" align="left"><input name="bZip" size="20" value="<%=bZip%>" type="text"></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><input type="checkbox" name="chkOptMail" value="Y" checked>&nbsp;</td>
                                    <td valign="top" colspan="2" class="checkout-text" align="left">
                                        <div align="justify">
                                            Be alerted via email on occasional time-sensitive "below wholesale" private sales offered by CellularOutfitter.com.
                                            CellularOutfitter.com abides by the strictest Privacy Policy standards.
                                            <a href="/privacypolicy.html" target="_blank">Click Here</a> for Privacy Policy details.
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <!--// A version billing -->
            <!-- B version billing -->
            <div id="mvt_billing2" style="float:left; width:100%; padding-top:15px;">
                <div style="float:left; width:500px;">
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Email:</div>
                        <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;"><input type="text" name="b_email" style="width:190px; margin:5px 0px 5px 0px; border:0px;" value="<%=b_email%>" onchange="getCustomerData(this.value)" onblur="remarket(this.value,'<%=mySession%>')" ></div>
                        <div style="float:left; width:14px; height:14px; margin:7px 0px 0px 5px; background:url(/images/checkout/icon-question.png) no-repeat; position:relative;" onmouseover="document.getElementById('email_pop').style.display=''" onmouseout="document.getElementById('email_pop').style.display='none'">
                            <div id="email_pop" style="display:none; position:absolute; border:2px solid #336799; background-color:#f4f4f4; font-size:14px; color:#333; padding:10px; width:200px; height:125px; top:-70px; right:-230px; z-index:5;">
                                We ask for your email just in case we need to reach you for any reason regarding your order with CellularOutfitter.com. We do not rent, share or sell your personal information. Ever.
                            </div>
                        </div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*First Name:</div>
                        <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;"><input type="text" name="b_fname" style="width:190px; margin:5px 0px 5px 0px; border:0px;" value="<%=b_fname%>" ></div>
                    </div> 
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Last Name:</div>
                        <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;"><input type="text" name="b_lname" style="width:190px; margin:5px 0px 5px 0px; border:0px;" value="<%=b_lname%>" ></div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Address 1:</div>
                        <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;"><input type="text" name="b_bAddress1" style="width:190px; margin:5px 0px 5px 0px; border:0px;" value="<%=b_bAddress1%>" ></div>
                    </div>
                    <div style="float:left; width:100%;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                        <div style="float:left; width:200px; text-align:right; font-style:italic; color:#999; font-size:12px; text-align:left;">Street address, PO box, company name, c/o</div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">Address 2:</div>
                        <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;"><input type="text" name="b_bAddress2" style="width:190px; margin:5px 0px 5px 0px; border:0px;" value="<%=b_bAddress2%>" ></div>
                    </div>
                    <div style="float:left; width:100%;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                        <div style="float:left; width:200px; text-align:right; font-style:italic; color:#999; font-size:12px; text-align:left;">Apartment, suite, unit, building, floor, etc.</div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Zip/Postal Code:</div>
                        <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;"><input type="text" name="b_bZip" style="width:190px; margin:5px 0px 5px 0px; border:0px;" value="<%=b_bZip%>" onkeyup="getNewShipping2('B')"></div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*City:</div>
                        <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px; position:relative;">
                            <input type="text" style="width:190px; margin:5px 0px 5px 0px; border:0px;" name="b_bCity" value="<%=b_bCity%>" >
                            <div id="id_bCity" style="position:absolute; display:none; z-index:100; top:0px; right:-265px; width:250px; padding:5px; border-radius:5px; border:2px solid #336799; background-color:#fff;">
                                <div style="float:left; width:100%; padding-bottom:3px; border-bottom:1px solid #ccc;">
                                    <div style="float:left; font-size:12px; color:#555;">City Name Suggestion</div>
                                    <div style="float:right;"><a style="font-size:12px; color:#555;" href="#" onclick="closeCityPop('B');">Close</a></div>
                                </div>
                                <div id="id_return_bCity" style="float:left; text-align:center; width:100%; height:150px; overflow:auto;"></div>
                            </div>
                        </div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*State/Province:</div>
                        <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;">
                            <select name="b_bState" style="width:190px; margin:5px 0px 5px 0px; border:0px;" onchange="getNewShippingByState(this.value)"><%getStates(b_bState)%></select>
                        </div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Phone:</div>
                        <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;"><input type="text" name="b_phone" style="width:190px; margin:5px 0px 5px 0px; border:0px;" value="<%=b_phone%>" ></div>
                        <div style="float:left; width:14px; height:14px; margin:7px 0px 0px 5px; background:url(/images/checkout/icon-question.png) no-repeat; position:relative;" onmouseover="document.getElementById('phone_pop').style.display=''" onmouseout="document.getElementById('phone_pop').style.display='none'">
                            <div id="phone_pop" style="display:none; position:absolute; border:2px solid #336799; background-color:#f4f4f4; font-size:14px; color:#333; padding:10px; width:200px; height:125px; top:-70px; right:-230px; z-index:5;">
                                We ask for your phone number just in case we need to reach you for any reason regarding your order with CellularOutfitter.com. We do not rent, share or sell your personal information. Ever.                        
                            </div>
                        </div>
                    </div>
                    <div style="float:left; width:100%;">
                        <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                        <div style="float:left; width:200px; text-align:right; font-style:italic; color:#999; font-size:12px; text-align:left;">Example: 1234567890</div>
                    </div>
                    <div style="float:left; width:100%; padding-top:20px;">
                        <div style="float:left; width:80px; text-align:right;">
                            <input type="checkbox" name="b_sameaddress" value="no" <%if request.form("b_sameaddress")="no" then response.write " checked"%> onclick="DiffShipping()">
                        </div>
                        <div style="float:left; width:420px; color:#333; font-size:12px;"> &nbsp; Check if Shipping Address is different than Billing Address.</div>
                    </div>
                    <div id="id_ShippingInformation" style="float:left; width:100%; <%if request.form("b_sameaddress") <> "no" then%>display:none;<%end if%>">
                        <div style="float:left; width:100%; padding-top:10px;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Address 1:</div>
                            <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;"><input type="text" name="b_sAddress1" style="width:190px; margin:5px 0px 5px 0px; border:0px;" value="<%=b_sAddress1%>" ></div>
                        </div>
                        <div style="float:left; width:100%;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                            <div style="float:left; width:200px; text-align:right; font-style:italic; color:#999; font-size:12px; text-align:left;">Street address, PO box, company name, c/o</div>
                        </div>
                        <div style="float:left; width:100%; padding-top:10px;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">Address 2:</div>
                            <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;"><input type="text" name="b_sAddress2" style="width:190px; margin:5px 0px 5px 0px; border:0px;" value="<%=b_sAddress2%>" ></div>
                        </div>
                        <div style="float:left; width:100%;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                            <div style="float:left; width:200px; text-align:right; font-style:italic; color:#999; font-size:12px; text-align:left;">Apartment, suite, unit, building, floor, etc.</div>
                        </div>
                        <div style="float:left; width:100%; padding-top:10px;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Zip/Postal Code:</div>
                            <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;"><input type="text" name="b_sZip" value="<%=b_sZip%>" style="width:190px; margin:5px 0px 5px 0px; border:0px;" onkeyup="getNewShipping2('S')"></div>
                        </div>
                        <div style="float:left; width:100%; padding-top:10px;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*City:</div>
                            <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px; position:relative;">
                                <input type="text" name="b_sCity" style="width:190px; margin:5px 0px 5px 0px; border:0px;" value="<%=b_sCity%>" >
                                <div id="id_sCity" style="position:absolute; display:none; z-index:100; top:0px; right:-265px; width:250px; padding:5px; border-radius:5px; border:2px solid #336799; background-color:#fff;">
                                    <div style="float:left; width:100%; padding-bottom:3px; border-bottom:1px solid #000;">
                                        <div style="float:left; font-size:12px; color:#555;">City Name Suggestion</div>
                                        <div style="float:right;"><a style="font-size:12px; color:#555;" href="#" onclick="closeCityPop('S');">Close</a></div>
                                    </div>
                                    <div id="id_return_sCity" style="float:left; text-align:center; width:100%; height:150px; overflow:auto;"></div>
                                </div>
                            </div>
                        </div>
                        <div style="float:left; width:100%; padding-top:10px;">
                            <div style="float:left; width:230px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*State/Province:</div>
                            <div style="float:left; width:200px; text-align:center; border:1px solid #ccc; border-radius:4px;">
                                <select name="b_sState" style="width:190px; margin:5px 0px 5px 0px; border:0px;" onchange="getNewShippingByState(this.value)"><%getStates(b_sState)%></select>
                            </div>
                        </div>                
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:80px; text-align:right;"><input type="checkbox" name="b_chkOptMail" value="Y" checked></div>
                        <div style="float:left; width:420px; color:#333; font-size:12px;"> &nbsp; Please notify me via email of exclusive CellularOutfitter.com "below wholesale" private sales.</div>
                    </div>
                </div>
                <div style="float:left; width:480px; text-align:right;">
                    <div id="mvt_testi" style="float:right; width:300px; border:1px solid #ccc; text-align:left;">
                        <div style="float:left; width:100%; border-bottom:1px solid #ccc; background-color:#f4f4f4; font-size:14px; font-weight:bold; color:#222; padding:7px 0px 7px 0px;"> &nbsp; What Our Customers Say</div>
                        <div style="float:left; width:100%; border-bottom:1px solid #ccc;">
                            <div style="float:left; width:25px; text-align:center; padding-top:5px;"><img src="/images/checkout/testimonial-quote.png" border="0" width="19" /></div>
                            <div style="float:left; width:265px; padding:5px; font-size:13px; color:#444;">
                                <div class="fl" style="width:265px;">Great price and fast delivery. Received my case four days after I ordered which is very good for coast to coast delivery. I would purchase from them again!</div>
                                <div class="fl" style="width:150px;">
	                                <div style="padding-top:10px;">
    	                                <img src="/images/checkout/testimonial-star-on.png" border="0" width="17" />
        	                            <img src="/images/checkout/testimonial-star-on.png" border="0" width="17" />
            	                        <img src="/images/checkout/testimonial-star-on.png" border="0" width="17" />
                                        <img src="/images/checkout/testimonial-star-on.png" border="0" width="17" />
                                        <img src="/images/checkout/testimonial-star-on.png" border="0" width="17" />
                                    </div>
                                    <div><b>Kelly S.</b></div>
                                    <div>Palm Beach, FL</div>
                                </div>
                            </div>
                        </div>
                        <div style="float:left; width:100%;">
                            <div style="float:left; width:25px; text-align:center; padding-top:5px;"><img src="/images/checkout/testimonial-quote.png" border="0" width="19" /></div>
                            <div style="float:left; width:265px; padding:5px; font-size:13px; color:#444;">
                                <div class="fl" style="width:265px;">This was my 3rd purchase from cellularoutfitter.com. Good selection at very good prices and they are shipped in a timely manner.</div>
                                <div class="fl" style="width:150px;">
                                    <div style="padding-top:10px;">
                                        <img src="/images/checkout/testimonial-star-on.png" border="0" width="17" />
                                        <img src="/images/checkout/testimonial-star-on.png" border="0" width="17" />
                                        <img src="/images/checkout/testimonial-star-on.png" border="0" width="17" />
                                        <img src="/images/checkout/testimonial-star-on.png" border="0" width="17" />
                                        <img src="/images/checkout/testimonial-star-on.png" border="0" width="17" />
                                    </div>
                                    <div><b>Robert W</b></div>
                                    <div>Fullerton, CA</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="float:left; width:800px; padding:10px 0px 0px 50px; color:#333; font-size:13px; font-weight:bold;">
                    CellularOutfitter.com abides by the strictest Privacy Policy standards. Please review our privacy policy <a href="/privacypolicy.html" target="_blank">here</a>.
                </div>
            </div>
            <!--// B version billing -->
        </div>
        <!--// billing information -->

        <!-- payment A -->
		<div id="mvt_payment1" style="float:left; width:100%; margin-top:20px; padding:10px 0px 10px 0px;">
        	<div style="float:left; width:100%;"><img src="/images/cart/payment_methods.jpg" border="0" alt="Select a Payment Method" /></div>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td width="467" valign="top" align="left" class="mc-text">
                        <table width="100%" border="0" cellpadding="0" cellspacing="5" align="center">
                            <tr>
                                <td><input type="radio" name="PaymentType" value="CC"></td>
                                <td width="245" colspan="2">
                                    <div style="float:left; padding-right:5px;"><img src="/images/cart/visa_off.gif" border="0" id="visaPic" /></div>
                                    <div style="float:left; padding-right:5px;"><img src="/images/cart/masterCard_off.gif" border="0" id="mcPic" /></div>
                                    <div style="float:left; padding-right:5px;"><img src="/images/cart/americanExpress_off.gif" border="0" id="amexPic" /></div>
                                    <div style="float:left;"><img src="/images/cart/discover_off.gif" border="0" id="discoverPic" /></div>
                                    <input type="hidden" name="cc_cardType" value="">
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><input name="cc_cardOwner" type="text" size="23" value="Card Holder Name" onFocus="testValue(this,this.value)" onblur="replaceValue(this,'Card Holder Name')" class="checkout-text"></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2">
                                    <div style="float:left; padding-right:10px;"><input name="cardNum" type="text" id="cardNum" size="23" maxlength="16" value="Credit Card Number" onFocus="testValue(this,this.value)" onblur="getCardType(this.value)" class="checkout-text"></div>
                                    <div style="float:left;"><input name="secCode" type="text" id="secCode" size="14" maxlength="4" value="Security Code" onFocus="testValue(this,this.value)" onblur="replaceValue(this,'Security Code')" class="checkout-text"></div>
                                    <div style="position:relative; float:left; padding-left:10px;" onmouseover="cvvHelp('on')">
                                        <img src="/images/cart/question.jpg" width="15" height="16" border="0">
                                        <div id="cvvWindow" style="position:absolute; top:-200px; left:30px; background-color:#FFF; display:none; width:320px; height:200px; z-index:999; border:2px solid #000; padding:3px;">
                                            <div style="background-color:#333; height:15px; color:#FFF; font-weight:bold; font-size:11px; width:100%;">
                                                <div style="float:left; padding-left:10px;">Credit Card CVV</div>
                                                <div style="float:right; padding-right:10px; cursor:pointer;" onclick="cvvHelp('off')">Close X</div>
                                            </div>
                                            <div id="cvvData" style="width:320px; overflow:auto; height:180px;"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2">
                                    <font size="2">Expires&nbsp;</font>
                                    <select name="cc_month" class="checkout-text">
                                        <option value="01">01 (January)</option>
                                        <option value="02">02 (February)</option>
                                        <option value="03">03 (March)</option>
                                        <option value="04">04 (April)</option>
                                        <option value="05">05 (May)</option>
                                        <option value="06">06 (June)</option>
                                        <option value="07">07 (July)</option>
                                        <option value="08">08 (August)</option>
                                        <option value="09">09 (September)</option>
                                        <option value="10">10 (October)</option>
                                        <option value="11">11 (November)</option>
                                        <option value="12">12 (December)</option>
                                    </select>
                                    <select name="cc_year" class="checkout-text">
                                        <%
                                        for countYear = 0 to 14
                                            response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & cStr(year(date) + countYear) & "</option>" & vbcrlf
                                        next
                                        %>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="30" valign="top" height="100%" align="left">
                        <div style="margin-top:10px;">
                            <img src="/images/cart/or.jpg" border="0" />
                        </div>                                                                                
                        <div style="height:110px; width:30px; border-right:1px dotted #000; margin-top:15px;">
                        </div>
                    </td>
                    <td width="473" valign="top" align="center" style="padding-left:10px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td width="10px" align="center" valign="middle"><input type="radio" name="PaymentType" value="eBillMe"></td>
                                <td width="*" align="left"><img src="/images/WU/WU-Pay-wutag-small.jpg" border="0" width="227" height="44" /></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td valign="top" class="mc-text" width="100%" style="padding-top:10px;" align="left">
                                    WU&reg; Pay allows you to pay cash online. No financial information required. Simply pay using your bank's online bill pay service, or at a Western Union&reg; Agent location near you. 
                                    Just like paying a utility bill! <a href="http://www.westernunion.com/wupay/learn/how-to-use-wupay-video" target="_blank">Watch our demo</a>.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <!--// payment A -->
        <!-- payment B -->
        <div id="mvt_payment2" style="float:left; width:978px; border:1px solid #ccc; background-color:#f4f4f4; border-radius:4px; margin-top:20px; padding:10px 0px 10px 0px;">
            <div style="float:left; width:78px; text-align:center; padding-top:5px;"><img src="/images/checkout/icon-lock.png" width="50" height="49" border="0" /></div>
            <div style="float:left; width:550px;">
                <div style="float:left; width:100%; text-align:left; font-size:28px; font-weight:bold; color:#222;">
                    Secure Credit Card Payment
                </div>
                <div style="float:left; width:100%; text-align:left; font-weight:bold; color:#222; font-size:14px;">
                    This is a secure 128-bit SSL encrypted payment. &nbsp; &nbsp; &nbsp; <span style="font-style:italic; font-weight:normal; font-size:14px; color:#999;">*Required Field</span>
                </div>
                <div style="float:left; width:100%; padding-top:10px;">
                    <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Cardholder Name:</div>
                    <div style="float:left; width:190px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                        <input name="b_cc_cardOwner" type="text" style="width:180px; margin:5px 0px 5px 0px; border:0px;" >
                    </div>
                </div>
                <div style="float:left; width:100%; padding-top:10px;">
                    <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Credit Card Number:</div>
                    <div style="float:left; width:190px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                        <input name="b_cardNum" type="text" maxlength="16" onblur="getCardType2(this.value)"  style="width:180px; margin:5px 0px 5px 0px; border:0px;">
                    </div>
                    <div style="float:left; width:140px; text-align:right; border-radius:4px; padding-left:5px;">
                        <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_master"><img id="cc_master_img" src="/images/checkout/cc-master-off.png" border="0" /></div>
                        <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_visa"><img id="cc_visa_img" src="/images/checkout/cc-visa-off.png" border="0" /></div>
                        <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_amex"><img id="cc_amex_img" src="/images/checkout/cc-amex-off.png" border="0" /></div>
                        <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_disc"><img id="cc_disc_img" src="/images/checkout/cc-discover-off.png" border="0" /></div>
                        <input type="hidden" name="b_cc_cardType" value="">
                    </div>
                </div>
                <div style="float:left; width:100%;">
                    <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                    <div style="float:left; width:190px; text-align:right; font-style:italic; color:#999; font-size:12px; text-align:left;">No spaces/dashes<br />Example: 1234123412341234</div>
                </div>
                <div style="float:left; width:100%; padding-top:10px;">
                    <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Expiration Date:</div>
                    <div style="float:left; width:80px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                        <select name="b_cc_month" style="width:70px; margin:5px 0px 5px 0px; border:0px;">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                    </div>
                    <div style="float:left; width:30px; text-align:center; padding-top:5px;">/</div>
                    <div style="float:left; width:80px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                        <select name="b_cc_year" style="width:70px; margin:5px 0px 5px 0px; border:0px;">
                        <%
                        for countYear = 0 to 14
                            response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & right(cStr(year(date) + countYear),2) & "</option>" & vbcrlf
                        next
                        %>
                        </select>
                    </div>
                </div>
                <div style="float:left; width:100%; padding-top:10px;">
                    <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:10px;">*Verification Code:</div>
                    <div style="float:left; width:80px; text-align:center;  margin:7px 15px 0px 0px; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                        <input name="b_secCode" type="text" id="b_secCode" maxlength="4"  style="width:70px; margin:5px 0px 5px 0px; border:0px;">
                    </div>
                    <div style="float:left; width:85px; text-align:left;"><img src="/images/checkout/cc-verify.png" border="0" width="62" height="42" /></div>
                </div>
                <div style="float:left; width:100%; padding-top:10px;">
                    <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                    <div style="float:left; width:300px; text-align:right; font-style:italic; color:#333; font-size:11px; text-align:left;">
                        <span style="font-weight:bold; color:#000;">Note:</span> American Express cards show the verification code printed above and to the right of the imprinted card number on the front of the card.
                    </div>
                </div>
            </div>
            <div id="mvt_trust1" style="float:left; width:350px; text-align:center;">
                <div style="float:left; margin:20px 0px 0px 10px; width:320px; border:1px solid #ccc; background-color:#fff; border-radius:4px;">
                    <div style="float:left; width:100%; border-bottom:1px solid #ccc;">
                        <div style="float:left; padding:10px;">
                            <!-- START SCANALERT CODE -->
                            <a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.cellularoutfitter.com"><img width="65" height="37" border="0" src="//images.scanalert.com/meter/www.cellularoutfitter.com/63.gif" alt="McAfee Secure sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee Secure is a Trademark of McAfee, Inc.'); return false;"></a>
                            <!-- END SCANALERT CODE -->                                    
                        </div>
                        <div style="float:left; padding:10px 10px 10px 5px;">
                            <img src="/images/checkout/trust-norton.png" border="0" />
                        </div>
                        <div style="float:left; padding:10px 0px 10px 5px;">
                            <a href="/privacypolicy.html" target="_blank"><img src="/images/checkout/trust-truste.png" border="0" /></a>
                        </div>
                    </div>
                    <div style="float:left; width:100%; border-bottom:1px solid #ccc; padding:10px 0px 10px 0px;">
                        <div style="float:left; width:60px; text-align:center; padding-top:5px;"><img src="/images/checkout/trust-low-price.png" border="0" /></div>
                        <div style="float:left; width:240px; text-align:left; color:#333; font-size:12px;">
                            <div style="font-weight:bold;">110% Low Price Assurance Guarantee</div>
                            <div style="padding-top:5px;">We offer the lowest prices on cell phone accessories anywhere, guaranteed!</div>
                        </div>
                    </div>
                    <div style="float:left; width:300px; padding:10px;">
                        <div style="float:left; width:126px; text-align:left; margin-right:10px;"><img src="/images/checkout/trust-happy.png" border="0" /></div>
                        <div style="float:left; width:57px; text-align:left; margin-right:10px;"><img src="/images/checkout/trust-inc.png" border="0" /></div>
                        <div style="float:left; width:93px; text-align:left;"><a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=100097908&source=ctc" target="_blank"><img src="/images/checkout/trust-bbb.png" border="0" /></a></div>
                    </div>
                </div>
            </div>
        </div>
        <!--// payment B -->
        
        <div style="float:left; width:100%; text-align:center; padding:30px 0px 20px 0px;">
            <div id="submitBttn" style="font-weight:bold; font-size:18px; color:#000;"><img onClick="return CheckSubmit();" src="/images/checkout/btn-order-now.png" alt="Submit Order" style="cursor: pointer;"></div>
            <input type="hidden" name="submitted" value="submitted">        
        </div>
        
         <!--WE variables-->
        <%=WEhtml%>
        <!--eBillme variables-->
        <%=EBtemp%>
        <input type="hidden" name="myBasketXML" value="<%=EBxml%>">
        <input type="hidden" name="ssl_ProdIdCount" value="<%=nProdIdCount%>">
    </div>
    </form>
	<div style="clear:both;"></div>
    <div id="mvt_trust2" style="width:100%; padding:40px 0px 20px 0px;">
        <div style="float:left; width:20%; padding:10px 20px 0px 100px;"><a href="/privacypolicy.html"><img src="/images/truste-seal-web.gif" border="0" /></a></div>
        <div style="float:left; width:19%; padding-top: 7px;"><a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=100097908&source=ctc" target="_blank"><img src="/images/BBB_clickratingsm.gif" border="0" /></a></div>
        <div style="float:left; width:13%;">
            <script type="text/javascript" src="/includes/js/j.js"></script>
            <script type="text/javascript">showMark(3);</script>
            <noscript><img id="googleChkOutImg" src="/images/sc.gif" border="0" width="72px" height="73px" /></noscript>
        </div>
        <div style="float:left; width:15%;" onclick="copyAB()"><img src="/images/Inc5000_color3.jpg" border="0" /></div>
        <div style="float:left; width:15%;"><a href="http://www.shopwiki.com/store-252661" target="_blank" title="CellularOutfitter.com is a ShopWiki Approved Store"><img src="/images/Shopwiki_Certified_EN.gif" border="0" /></a></div>
    </div>
</td>

<div id="lowPricePopup" style="display:none;">
    <div style="width:600px; background-color:#fff; text-align:center; margin-left:auto; margin-right:auto; margin-top:150px; position:relative;">
        <div style="position:absolute; top:5px; right:5px; color:#666; font-weight:bold; cursor:pointer;" onclick="closeLowPricePopup()">X</div>
        <div style="border-bottom:1px solid #ccc; background-color:#f2f2f2; height:100px;">
		    <div style="float:left; padding:15px;">
            	<img src="/images/basket/shield.png" border="0" />
            </div>
		    <div style="padding:35px 0px 0px 10px; float:left; font-size:30px; color:#333; font-weight:bold;">110% LOW PRICE GUARANTEE</div>
        </div>
        <div style="clear:both;"></div>
        <div style="width:100%; font-size:12px; color:#333;">
        	<div style="padding:25px; text-align:left; line-height:17px;">
	            CellularOutfitter.com GUARANTEES the lowest price of any major online retailer. We're so confident in our ability to provide the lowest possible price of any online retailer, that we will credit you the difference if you can find a cheaper price online.
				<br /><br />
                <span style="font-weight:bold; color:#000;">HERE's HOW IT WORKS:</span>
                <br /><br />
                If you have found a price (product + shipping + tax) lower than ours on a competing site, please email a screen shot of the checkout page to: priceguarantee@cellularoutfitter.com, or, you can fax a printout to:
				<br /><br />
                <span style="font-style:italic;">(714) 278-1932</span><br />
                <span style="font-style:italic;">attn: "CellularOutfitter.com Price Guarantee"</span>
				<br /><br />
				We will credit you the difference! This offer is effective a full 14 days after your purchase date so you can be assured you are receiving the BEST value anywhere.
				<br /><br />
				It's as simple as that! No gimmicks, no tricks. It's just another way that the team here at CellularOutfitter.com is dedicated to providing the best possible shopping experience anywhere!
                <br /><br />
                <span style="font-weight:bold; color:#000;">PLEASE NOTE: </span>Due to the volatility of the online auction marketplace, we DO NOT match pricing on auction sites such as eBay, Overstock, Half.com, Yahoo! Auctions, Amazon.com auctions, etc. The competing site and offer must be recognized by what CellularOutfitter.com deems a valid, established online U.S. merchant in good standing and shall not include micro-affiliate, gateway, non-domestic (USA) sites or any other variation thereof and of which shall be determined at the sole discretion of CellularOutfitter.com.
				<br /><Br />
                <span style="font-weight:bold; color:#000;">ALSO NOTE: </span>Our price matching guarantee applies to cellular accessories only. This guarantee does not apply to cellular phones.
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
<div id="returnPopup" style="display:none;">
    <div style="width:600px; background-color:#fff; text-align:center; margin-left:auto; margin-right:auto; margin-top:150px; position:relative;">
        <div style="position:absolute; top:5px; right:5px; color:#666; font-weight:bold; cursor:pointer;" onclick="closeReturnPopup()">X</div>
        <div style="border-bottom:1px solid #ccc; background-color:#f2f2f2; height:100px;">
		    <div style="float:left; padding:15px;">
            	<img src="/images/basket/shield.png" border="0" />
            </div>
		    <div style="padding:35px 0px 0px 10px; float:left; font-size:30px; color:#333; font-weight:bold;">90-DAY RETURNS</div>
        </div>
        <div style="clear:both;"></div>
        <div style="width:100%; font-size:12px; color:#333;">
        	<div style="padding:25px; text-align:left; line-height:17px; height:450px; overflow:auto;">
				<span style="font-weight:bold; color:#000;">Return/Warranty Policy:</span>
                <br />
				If you are not happy with the any product, you may return the item for an exchange or refund of the purchase price (minus shipping) within ninety (90) days of receipt. We also offer a ONE YEAR MANUFACTURER WARRANTY on all items. If your item is defective, for up to a full year, simply return the item to CellularOutfitter.com for a prompt exchange. 
				<br /><br />
				All returns must be accompanied by a Cellular Outfitter RMA number. To obtain an RMA number, please send an e-mail to: <a href="mailto:returns@CellularOutfitter.com">returns@CellularOutfitter.com </a>
				<br /><br />
				The information below must be included for any return:
                <br />
				&nbsp; - Order Number<br />
                &nbsp; - Date of original purchase<br />
                &nbsp; - Reason for request for RMA<br />
                &nbsp; - Your e-mail address<br />
				<br /><br />	
				No returns, refunds or exchanges will be processed without an RMA number. 
				<br /><br />
				<span style="font-weight:bold; color:#000;">CELL PHONE RETURN POLICY:</span><br />
				All return requests must be made within 7 days of receiving the item. All returned merchandise must be unregistered, in complete original manufacturer's packaging, same condition as sold, with all literature, accessories, instructions, blank warranty cards and documentation. All Phones must be returned in "Like-New Condition", show no signs of use and must have less than 25 minutes in total cumulative talk time. 
				<br /><br />
				If an item is defective it may only be exchanged with an equivalent product if we are unable to supply the same item or if the item cannot be repaired. Any return shipping cost is the responsibility of the customer. All shipping and handling fees are non-refundable. This includes all refused and unaccepted packages. 
				<br /><br />
				All NON-DEFECTIVE cell phone returns will be subject to a 20% restocking fee. 
				<br /><br />
				For refunds allow 3-4 business days for the refund to post to your account from the time the refund is issued. 
				<br /><br />
				<span style="font-weight:bold; color:#000;">Non-Defective Returns:</span><br />
				NON-DEFECTIVE RETURNS OF ACCESSORIES ARE SUBJECT TO A 15% RE-STOCKING FEE. NON-DEFECTIVE RETURNS OF PHONES ARE SUBJECT TO A 20% RE-STOCKING FEE. Such returns will be for store credit or refund at the customer's request. Refunds are only available for order within 30 days of original order date for accessories and 7 days for phones. Refunds will be issued for the current published price of the product(s) minus a 15% or 20% re-stocking fee. 
				<br /><br />
				<span style="font-weight:bold; color:#000;">NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS:</span><br /> 
				NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS MAY BE SUBJECT TO A 15% RE-STOCKING FEE and such returns will be for store credit or refund (at the customer's request). Refunds are only available within 90 days of original order date. Please contact us at <a href="mailto:returns@cellularoutfitter.com">returns@cellularoutfitter.com</a> if you have questions about which products are returnable and which products may be subject to a restocking fee. 
				<br /><br />
				Customers must contact the headset manufacturer for all defective/warranty issues on headsets that have had the packaging opened. For your convenience, you can contact us at <a href="mailto:returns@CellularOutfitter.com">returns@CellularOutfitter.com</a> for the manufacturers' contact information.
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
<div id="timesUp" style="display:none;">
	<div class="tb finalChanceBox">
    	<div class="finalChance">Here is your final chance to get your purchase at stunningly low prices!</div>
        <div class="stillInterested">Are you still interested in these items?</div>
        <div class="stillInterestedCta" onclick="resetTimer()"><img src="/images/checkout/cta.png" border="0" width="325" height="55" /></div>
        <div class="noThanks" onclick="closeTimesUp()">No thanks</div>
    </div>
</div>
<div id="welcomeBack" style="display:none;">
	<div class="tb finalChanceBox" onclick="closeTimesUp()">
    	<div class="welcomeBackLogo"><img src="/images/coLogo2.gif" border="0" width="314" height="74" /></div>
        <div class="welcomeBackTitle">Welcome Back!</div>
        <div class="welcomeBackMsg">
        	We have saved your address information from a previous order. 
            Please review the information and make any required updates.
        </div>
        <div class="noThanks" onclick="closeTimesUp()">Click to Continue</div>
    </div>
</div>
<div id="customerInfo" style="display:none;"></div>
<!--#include virtual="/includes/template/bottom_cart.asp"-->
<%
function fWriteBasketRow(nProdId,sItemName,sItemPrice,nQty,sitempic_CO)
	prodLink = "http://www.cellularoutfitter.com/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"
	sTemp	=	"<div style=""float:left; width:658px; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc; color:#333; padding:10px 0px 10px 0px;"">" & vbcrlf & _
				"	<div style=""float:left; width:328px;"">" & vbcrlf & _
				"		<div style=""float:left; width:65px; margin:0px 20px 0px 5px;""><a href=""" & prodLink & """><img src=""" & useImg & """ width=""65"" height=""65"" border=""0"" /></a></div>" & vbcrlf & _
				"		<div style=""float:left; width:233px; position:relative;"">" & vbcrlf & _
				"			<a href=""" & prodLink & """ style=""font-size:13px; font-weight:normal; color:#333;"">" & sItemName & "</a>" & vbcrlf & _
				"			<div id=""longInStock"" style=""color:#359800; position:absolute; top:60px; left:0px; font-weight:bold; float:left; width:400px; font-size:13px;"">In Stock! Leaves Our U.S. Warehouse Within 24 Hours.</div>"  & vbcrlf & _
				"		</div>" & vbcrlf & _
				"	</div>" & vbcrlf & _
				"	<div style=""float:left; width:76px; padding-left:34px; text-align:center;"">" & vbcrlf & _
				"		<div style=""padding:5px; text-align:center; width:40px; overflow:hidden; color:#555; font-size:12px;"">" & nQty & vbcrlf & _
				"		</div>" & vbcrlf & _
				"	</div>" & vbcrlf

	if cDbl(sItemPrice) = 0 then
		sTemp = sTemp & "	<div style=""float:left; width:110px; text-align:center; color:#333; font-size:13px; padding-top:5px;"">FREE</div>" & vbcrlf & _
						"	<div style=""float:left; width:110px; text-align:center; color:#333; font-size:13px; padding-top:5px;"">FREE</div>" & vbcrlf
	else
		sTemp = sTemp & "	<div style=""float:left; width:110px; text-align:center; color:#333; font-size:13px; padding-top:5px;"">" & formatCurrency(cDbl(sItemPrice)) & vbcrlf & _
						"	</div>" & vbcrlf & _
						"	<div style=""float:left; width:110px; text-align:center; color:#333; font-size:13px; padding-top:5px;"">" & formatCurrency((cDbl(sItemPrice) * cDbl(nQty))) & vbcrlf & _
						"	</div>" & vbcrlf
	end if
	
	sTemp = sTemp & "</div>"

	fWriteBasketRow = sTemp
end function
%>
<script language="javascript">
	function remarket(email,mySession) {
		ajax('/ajax/remarketing.asp?email=' + email + '&mySession=' + mySession + '&siteID=2','dumpZone')
	}
	
	function getStyle(oElm, strCssRule) {
		var strValue = "";
		if(document.defaultView && document.defaultView.getComputedStyle){
			strValue = document.defaultView.getComputedStyle(oElm, "").getPropertyValue(strCssRule);
		}
		else if(oElm.currentStyle){
			strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1){
				return p1.toUpperCase();
			});
			strValue = oElm.currentStyle[strCssRule];
		}
		return strValue;
	}

	function getNewShippingByState(sState) {
		strStates = "AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT";
		if (strStates.search(sState) >= 0) {
			document.getElementById('id_shippingDetails').innerHTML = '<input type="hidden" name="shipcost4" value="<%=shipcost4%>"><input type="radio" name="shiptype" autocomplete="off" value="4" checked>USPS First Class Int\'l (8-12 business days)<br>';
			document.getElementById('id_shippingDetails').innerHTML = document.getElementById('id_shippingDetails').innerHTML + '<input type="hidden" name="shipcost5" value="<%=shipcost5%>"><input type="radio" name="shiptype" autocomplete="off" value="5">USPS Priority Int\'l (3-8 business days)';
		}
	}

	function getNewShipping()
	{
		document.getElementById('dumpZone').innerHTML = "";
		zipcode = document.frmProcessOrder.sZip.value;

		updateURL = '/ajax/shippingDetails.asp?zip=' + zipcode + '&weight=<%=sWeight%>&shiptype=<%=shiptype%>&nTotalQuantity=<%=nTotalQuantity%>&nSubTotal=<%=nSubTotal%>';
		if (zipcode.length >= 5) {
			ajax(updateURL,'id_addr_shippingDetails');
		}
	}
	
	function getNewShipping2(zipType)
	{
		document.getElementById('dumpZone').innerHTML = "";
		if (zipType == 'B')
			zipcode = document.frmProcessOrder.b_bZip.value;
		else
			zipcode = document.frmProcessOrder.b_sZip.value;

		updateURL = '/ajax/shippingDetails.asp?zip=' + zipcode + '&weight=<%=sWeight%>&shiptype=<%=shiptype%>&nTotalQuantity=<%=nTotalQuantity%>&nSubTotal=<%=nSubTotal%>';
		if (zipcode.length >= 5) {
			ajax(updateURL,'id_addr_shippingDetails');
			getCityByZip(zipType);
		}
	}

	function getCityByZip(zipType) {
		useID = ""
		if (zipType == 'B') {
			zipcode = document.frmProcessOrder.b_bZip.value;
			useID = "bCity"
		} else {
			zipcode = document.frmProcessOrder.b_sZip.value;
			useID = "sCity"
		}
	
		if (zipcode.length >= 5) {
			document.getElementById('id_return_'+useID).innerHTML = 'Loading..';
			ajax('/ajax/cityByZip.asp?zipcode=' + zipcode + '&ziptype=' + zipType,'id_return_'+useID);
			document.getElementById('id_'+useID).style.display = '';
		}
	}
	
	function populateCityState(zipType, cityname, state) {
		if (zipType == 'B') {
			document.frmProcessOrder.b_bCity.value = cityname;
			document.frmProcessOrder.b_bState.value = state;
			document.getElementById('id_bCity').style.display = 'none';
		} else {
			document.frmProcessOrder.b_sCity.value = cityname;
			document.frmProcessOrder.b_sState.value = state;
			document.getElementById('id_sCity').style.display = 'none';
		}
	}

	function closeCityPop(zipType) {
		if (zipType == 'B') {
			useID = "bCity"
		} else {
			useID = "sCity"
		}
				
		document.getElementById('id_'+useID).style.display='none';
		return false;	
	}
	

	function testValue(obj,objVal) {
		if (objVal == "Card Holder Name") {
			obj.value = ""
		}
		if (objVal == "Credit Card Number") {
			obj.value = ""
		}
		if (objVal == "Security Code") {
			obj.value = ""
		}
	}
	
	function popUp(url, winName, h, w, l, t) {
		var args = 'height=' + h + ',width=' + w + ',left=' + l + ',top=' + t + ',toolbar=1,location=1,directories=0,status=1,menubar=1,scrollbars=1,resizable=1';
		window.open(url, winName, args);
	}
	
	function getCardType(cardNum) {
		replaceValue(document.frmProcessOrder.cardNum,'Credit Card Number')
		document.getElementById("visaPic").src = "/images/cart/visa_off.gif"
		document.getElementById("amexPic").src = "/images/cart/americanExpress_off.gif"
		document.getElementById("mcPic").src = "/images/cart/masterCard_off.gif"
		document.getElementById("discoverPic").src = "/images/cart/discover_off.gif"
		var first1 = parseInt(cardNum.substring(0,1))
		var first2 = parseInt(cardNum.substring(0,2))
		if (first1 == 4) {
			document.frmProcessOrder.cc_cardType.value = "VISA"
			document.getElementById("visaPic").src = "/images/cart/visa_on.gif"
		}
		else if (first2 == 34 || first2 == 37) {
			document.frmProcessOrder.cc_cardType.value = "AMEX"
			document.getElementById("amexPic").src = "/images/cart/americanExpress_on.gif"
		}
		else if (first2 > 50 && first2 < 56) {
			document.frmProcessOrder.cc_cardType.value = "MC"
			document.getElementById("mcPic").src = "/images/cart/masterCard_on.gif"
		}
		else if (first1 == 6) {
			document.frmProcessOrder.cc_cardType.value = "DISC"
			document.getElementById("discoverPic").src = "/images/cart/discover_on.gif"
		}
	}
	
	function getCardType2(cardNum) {
		document.getElementById('cc_master_img').src = '/images/checkout/cc-master-off.png';
		document.getElementById('cc_master').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_master').style.borderColor = '#f4f4f4';
		document.getElementById('cc_visa_img').src = '/images/checkout/cc-visa-off.png';
		document.getElementById('cc_visa').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_visa').style.borderColor = '#f4f4f4';		
		document.getElementById('cc_amex_img').src = '/images/checkout/cc-amex-off.png';
		document.getElementById('cc_amex').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_amex').style.borderColor = '#f4f4f4';
		document.getElementById('cc_disc_img').src = '/images/checkout/cc-discover-off.png';
		document.getElementById('cc_disc').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_disc').style.borderColor = '#f4f4f4';
		
		var first1 = parseInt(cardNum.substring(0,1))
		var first2 = parseInt(cardNum.substring(0,2))
		if (first1 == 4) {
			document.frmProcessOrder.b_cc_cardType.value = "VISA"
			document.getElementById('cc_visa_img').src = '/images/checkout/cc-visa-on.png';
			document.getElementById('cc_visa').style.backgroundColor = '#fff';
			document.getElementById('cc_visa').style.borderColor = '#ccc';
		}
		else if (first2 == 34 || first2 == 37) {
			document.frmProcessOrder.b_cc_cardType.value = "AMEX"
			document.getElementById('cc_amex_img').src = '/images/checkout/cc-amex-on.png';
			document.getElementById('cc_amex').style.backgroundColor = '#fff';
			document.getElementById('cc_amex').style.borderColor = '#ccc';
		}
		else if (first2 > 50 && first2 < 56) {
			document.frmProcessOrder.b_cc_cardType.value = "MC"
			document.getElementById('cc_master_img').src = '/images/checkout/cc-master-on.png';
			document.getElementById('cc_master').style.backgroundColor = '#fff';
			document.getElementById('cc_master').style.borderColor = '#ccc';
		}
		else if (first1 == 6) {
			document.frmProcessOrder.b_cc_cardType.value = "DISC"
			document.getElementById('cc_disc_img').src = '/images/checkout/cc-discover-on.png';
			document.getElementById('cc_disc').style.backgroundColor = '#fff';
			document.getElementById('cc_disc').style.borderColor = '#ccc';
		}
	}
	
	function cvvHelp(dir) {
		if (document.getElementById("cvvData").innerHTML == "") {
			ajax('/ajax/cvv.asp','cvvData')
		}
		if (dir == "off") {
			document.getElementById("cvvWindow").style.display = "none"
		}
		else {
			document.getElementById("cvvWindow").style.display = ""
		}
	}
	
	function replaceValue(input,value) {
		if (input.value == "") {
			input.value = value
		}
	}
	
	function copyAB() {
		billingA = getStyle(document.getElementById('mvt_billing1'), 'display');
		billingB = getStyle(document.getElementById('mvt_billing2'), 'display');
		paymentA = getStyle(document.getElementById('mvt_payment1'), 'display');
		paymentB = getStyle(document.getElementById('mvt_payment2'), 'display');

		var f = document.frmProcessOrder;
		var elem = document.getElementById('id_frmProcessOrder').elements;

		if ((billingA == 'none')&&(billingB != 'none')) {
			if (f.b_sameaddress.checked == false) {
				f.b_sAddress1.value = f.b_bAddress1.value;
				f.b_sAddress2.value = f.b_bAddress2.value;
				f.b_sCity.value = f.b_bCity.value;
				f.b_sZip.value = f.b_bZip.value;
				f.b_sState.selectedIndex = f.b_bState.selectedIndex;
			}
			
			f.chkOptMail.checked = f.b_chkOptMail.checked;
			f.bState.selectedIndex = f.b_bState.selectedIndex;
			f.sState.selectedIndex = f.b_sState.selectedIndex;
			
			f.fname.value = f.b_fname.value;
			f.lname.value = f.b_lname.value;
			f.email.value = f.b_email.value;
			f.phone.value = f.b_phone.value;
			f.sAddress1.value = f.b_sAddress1.value;
			f.sAddress2.value = f.b_sAddress2.value;
			f.sCity.value = f.b_sCity.value;
			f.sZip.value = f.b_sZip.value;
			f.bAddress1.value = f.b_bAddress1.value;
			f.bAddress2.value = f.b_bAddress2.value;
			f.bCity.value = f.b_bCity.value;
			f.bZip.value = f.b_bZip.value;
		}
		
		if ((paymentA == 'none')&&(paymentB != 'none')) {
			f.cc_month.selectedIndex = f.b_cc_month.selectedIndex;
			f.cc_year.selectedIndex = f.b_cc_year.selectedIndex;	
			f.cc_cardType.value = f.b_cc_cardType.value;
			f.cc_cardOwner.value = f.b_cc_cardOwner.value;
			f.cardNum.value = f.b_cardNum.value;
			f.cc_month.value = f.b_cc_month.value;
			f.cc_year.value = f.b_cc_year.value;
			f.secCode.value = f.b_secCode.value;
		}
		
		/*
        for(var i = 0; i < elem.length; i++) {
			if (elem[i].name.indexOf("b_") >= 0) {
				elem_a = eval('document.frmProcessOrder.' + elem[i].name.replace("b_", ""));
				document.getElementById('terrytest').innerHTML = document.getElementById('terrytest').innerHTML + elem_a.name + ':' + elem_a.value + ' // ' + elem[i].name + ':' + elem[i].value + '<br>'
			}
		}
		*/		
	}
		
	function CheckSubmit() {
		var f = document.frmProcessOrder;
		bValid = true;

		copyAB();

		CheckValidNEW(f.email.value, "Email is a required field!");
		CheckEMailNEW(f.email.value);
		
		CheckValidNEW(f.fname.value, "Your First Name is a required field!");
		CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
		CheckValidNEW(f.phone.value, "Your Phone Number is a required field!");
		
		CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
		CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
		sString = f.sState.options[f.sState.selectedIndex].value;
		CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
		CheckValidNEW(f.sZip.value, "Your Zip Code (for Shipping Address) is a required field!");
		
		CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!");
		CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!");
		sString = f.bState.options[f.bState.selectedIndex].value;
		CheckValidNEW(sString, "State (for Billing Address) is a required field!");
		CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!");
	
		var b = "cc";
		var a = f.PaymentType.length;
		if (a != 'undefined') {
			for (var i = 0; i < a; i++) {
				if (f.PaymentType[i].checked) {
					var b = f.PaymentType[i].value;
				}
			}			
		}

		if (b != 'eBillMe' && b != 'PAY') {
			sString = f.cc_cardType.value;
			CheckValidNEW(sString, "Credit Card Type is a required field!");
			CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!");
			CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!");
			CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!");
			CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!");
			CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!");
		}

		if (bValid) {
			var aa = f.shiptype.length;
			for (var i = 0; i < aa; i++) {
				if (f.shiptype[i].checked) {
					var shiptype = f.shiptype[i].value;
				}
			}
			if (shiptype == undefined) {
				alert("Please select shipping Method!");
				return false;
			}

			var shiprate = eval('document.frmProcessOrder.shipcost' + shiptype);
			f.shippingTotal.value = shiprate.value;
	
			<% if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then %>
				document.frmProcessOrder.action = '/framework/processing.asp';
			<% else %>
				document.frmProcessOrder.action = 'https://www.cellularoutfitter.com/framework/processing.asp';
			<% end if %>
			document.getElementById("submitBttn").innerHTML = "Please wait while<br />we process your order"
			document.frmProcessOrder.submit();
		}
		return false;
	}
	
	function DiffShipping() {
		var f = document.frmProcessOrder;
		
		if (f.b_sameaddress.checked == true)
			document.getElementById('id_ShippingInformation').style.display = '';
		else
			document.getElementById('id_ShippingInformation').style.display = 'none';
	}

	function ShipToBillPerson() {
		var f = document.frmProcessOrder;
		if (f.sameaddress.checked == false) {
			f.bAddress1.value = "";
			f.bAddress2.value = "";
			f.bCity.value = "";
			f.bZip.value = "";
			f.bState.selectedIndex = f.sState.options[0];
		}
		else {
			f.bAddress1.value = f.sAddress1.value;
			f.bAddress2.value = f.sAddress2.value;
			f.bCity.value = f.sCity.value;
			f.bZip.value = f.sZip.value;
			f.bState.selectedIndex = f.sState.selectedIndex;
		}
	}
	
	function fnRecalculate() {
		if ((document.frmProcessOrder.b_bZip.value != '')&&(document.frmProcessOrder.b_sZip.value == '')) {
			document.frmProcessOrder.b_sZip.value = document.frmProcessOrder.b_bZip.value;
		}
		
		if ((document.frmProcessOrder.b_bState.value != '')&&(document.frmProcessOrder.b_sState.value == '')) {
			document.frmProcessOrder.b_sState.value = document.frmProcessOrder.b_bState.value;
		}
				
		document.frmProcessOrder.action = '/cart/checkout.asp';
		document.frmProcessOrder.submit();
	}
	
	function openLowPricePopup() {
		document.getElementById("popBox").innerHTML = document.getElementById('lowPricePopup').innerHTML;
		
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function closeLowPricePopup() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}	
	
	function openReturnPopup() {
		document.getElementById("popBox").innerHTML = document.getElementById('returnPopup').innerHTML;
		
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function closeReturnPopup() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}
	
	var runSalesTimer = 1
	
	if (runSalesTimer == 1) {
		setTimeout("changeSalesTime()",1000)
	}
	
	function changeSalesTime() {
		var saleMinutes = parseInt(document.getElementById("saleMinutes").innerHTML);
		var saleSeconds = parseInt(document.getElementById("saleSeconds").innerHTML);
		
		if (saleSeconds > 0) {
			saleSeconds--
			if (saleSeconds < 10) {
				document.getElementById("saleSeconds").innerHTML = "0" + saleSeconds
				document.getElementById("saleSeconds_fl").innerHTML = "0" + saleSeconds
			}
			else {
				document.getElementById("saleSeconds").innerHTML = saleSeconds
				document.getElementById("saleSeconds_fl").innerHTML = saleSeconds
			}
			setTimeout("changeSalesTime()",1000)
		}
		else if (saleMinutes > 0) {
			saleMinutes--
			document.getElementById("saleMinutes").innerHTML = saleMinutes
			document.getElementById("saleMinutes_fl").innerHTML = saleMinutes
			document.getElementById("saleSeconds").innerHTML = 59
			document.getElementById("saleSeconds_fl").innerHTML = 59
			setTimeout("changeSalesTime()",1000)
		}
		else {
			openTimesUp()
		}
	}
	
	function openTimesUp() {
		document.getElementById("popBox").innerHTML = document.getElementById('timesUp').innerHTML;
		
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function closeTimesUp() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}
	
	function resetTimer() {
		document.getElementById("saleMinutes").innerHTML = "10"
		document.getElementById("saleSeconds").innerHTML = "00"
		setTimeout("changeSalesTime()",1000)
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}
	
	function getCustomerData(email) {
		ajax('/ajax/customerData.asp?email=' + email,'customerInfo')
		setTimeout("displayData(document.getElementById('customerInfo').innerHTML)",2000)
	}
	
	function displayData(dataReturn) {
		if (dataReturn != "no data" && dataReturn != "") {
			document.getElementById("popCover").style.display = ''
			document.getElementById("popBox").innerHTML = document.getElementById("welcomeBack").innerHTML
			document.getElementById("popBox").style.display = ''
			var dataArray = dataReturn.split("@@")
			if (document.frmProcessOrder.b_fname.value == "") { document.frmProcessOrder.b_fname.value = dataArray[0] }
			if (document.frmProcessOrder.b_lname.value == "") { document.frmProcessOrder.b_lname.value = dataArray[1] }
			if (document.frmProcessOrder.b_bAddress1.value == "") { document.frmProcessOrder.b_bAddress1.value = dataArray[2] }
			if (document.frmProcessOrder.b_bAddress2.value == "") { document.frmProcessOrder.b_bAddress2.value = dataArray[3] }
			if (document.frmProcessOrder.b_bCity.value == "") { document.frmProcessOrder.b_bCity.value = dataArray[4] }
			if (document.frmProcessOrder.b_bState.selectedIndex == 0) {
				for (i=0;i<50;i++) {
					if (document.frmProcessOrder.b_bState.options[i].value == dataArray[5]) {
						document.frmProcessOrder.b_bState.selectedIndex = i
					}
				}
			}
			if (document.frmProcessOrder.b_bZip.value == "") {
				document.frmProcessOrder.b_bZip.value = dataArray[6]
				getNewShipping2('B');
			}
			if (document.frmProcessOrder.b_phone.value == "") { document.frmProcessOrder.b_phone.value = dataArray[7] }
			
			if (dataArray[2] != dataArray[8] || dataArray[6] != dataArray[12]) {
				document.frmProcessOrder.b_sameaddress.checked = true
				DiffShipping()
				document.frmProcessOrder.b_sAddress1.value = dataArray[8]
				document.frmProcessOrder.b_sAddress2.value = dataArray[9]
				document.frmProcessOrder.b_sCity.value = dataArray[10]
				for (i=0;i<50;i++) {
					if (document.frmProcessOrder.b_sState.options[i].value == dataArray[11]) {
						document.frmProcessOrder.b_sState.selectedIndex = i
					}
				}
				document.frmProcessOrder.b_sZip.value = dataArray[12]
			}
		}
	}
</script>