<!--
function CheckValidNEW(strFieldName, strMsg) {
	if (strFieldName == "" || strFieldName == null || strFieldName == "NULL" || strFieldName == " ") {
		if (bValid) {
			alert(strMsg);
			bValid = false;
		}
	}
}

function CheckEMailNEW(addr) {
	var invalidChars = '\/\'\\ ";:?!()[]\{\}^|,';
	for (i=0; i<invalidChars.length; i++) {
		if (addr.indexOf(invalidChars.charAt(i),0) > -1) {
			if (bValid) {
				alert('E-mail address contains invalid characters');
				bValid = false;
			}
		}
	}
	for (i=0; i<addr.length; i++) {
		if (addr.charCodeAt(i)>127) {
			if (bValid) {
				alert("E-mail address contains non ascii characters.");
				bValid = false;
			}
		}
	}
	var atPos = addr.indexOf('@',0);
	if (atPos == -1) {
		if (bValid) {
			alert('E-mail address must contain an @');
			bValid = false;
		}
	}
	if (atPos == 0) {
		if (bValid) {
			alert('E-mail address must not start with @');
			bValid = false;
		}
	}
	if (addr.indexOf('@', atPos + 1) > - 1) {
		if (bValid) {
			alert('E-mail address must contain only one @');
			bValid = false;
		}
	}
	if (addr.indexOf('.', atPos) == -1) {
		if (bValid) {
			alert('E-mail address must contain a period in the domain name');
			bValid = false;
		}
	}
	if (addr.indexOf('@.',0) != -1) {
		if (bValid) {
			alert('Period must not immediately follow @ in e-mail address');
			bValid = false;
		}
	}
	if (addr.indexOf('.@',0) != -1) {
		if (bValid) {
			alert('Period must not immediately precede @ in e-mail address');
			bValid = false;
		}
	}
	if (addr.indexOf('..',0) != -1) {
		if (bValid) {
			alert('Two periods must not be adjacent in e-mail address');
			bValid = false;
		}
	}
	var suffix = addr.substring(addr.lastIndexOf('.')+1);
	if (suffix.length != 2 && suffix != 'com' && suffix != 'net' && suffix != 'org' && suffix != 'edu' && suffix != 'int' && suffix != 'mil' && suffix != 'gov' & suffix != 'arpa' && suffix != 'biz' && suffix != 'aero' && suffix != 'name' && suffix != 'coop' && suffix != 'info' && suffix != 'pro' && suffix != 'museum') {
		if (bValid) {
			alert('Invalid primary domain in e-mail address');
			bValid = false;
		}
	}
	return true;
}
//-->
