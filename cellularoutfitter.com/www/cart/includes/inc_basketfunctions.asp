<%
function fWriteBasketRow(sWriteDetails,nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sItemPic)
	'this will write out the row for the basket
	dim sTemp
	sTemp = "<tr>" & vbcrlf
	sTemp = sTemp & "<td width=""74"" align=""center"" bgcolor=""#FFFFFF"" class=""cart-text""><a href=""http://www.cellularoutfitter.com/p-" & nProdId & "-" & formatSEO(sItemName) & ".html""><img src=""https://www.cellularoutfitter.com/productpics/icon/" & sItemPic & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf
	sTemp = sTemp & "<td width=""419"" align=""left"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text""><a href=""http://www.cellularoutfitter.com/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"">" & sItemName & "</a></td>" & vbcrlf
	sTemp = sTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text""><s>" & formatCurrency(sRetailPrice) & "</s></td>" & vbcrlf
	sTemp = sTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text"">" & formatCurrency(sItemPrice) & "</td>" & vbcrlf
	sTemp = sTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text""><font color=""#FF0000"">" & formatCurrency(sRetailPrice - sItemPrice) & "</font></td>" & vbcrlf
	sTemp = sTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text"">" & vbcrlf
	sTemp = sTemp & "<form name=""frmEditDelete" & nProdId & """ action=""http://www.cellularoutfitter.com/cart/item_edit.asp"" method=""post"">" & vbcrlf
	sTemp = sTemp & "<input type=""hidden"" name=""prodid"" value=""" & nProdId & """>" & vbcrlf
	sTemp = sTemp & "<input type=""hidden"" name=""relatedid"" value=""" & nRelatedId & """>" & vbcrlf
	sTemp = sTemp & "<input type=""hidden"" name=""pagetype"" value=""edit"">" & vbcrlf
	sTemp = sTemp & "<input type=""text"" name=""qty"" size=""1"" maxlength=""4"" value=""" & nQty & """><br>" & vbcrlf
	sTemp = sTemp & "<input type=""image"" src=""https://www.cellularoutfitter.com/images/cart/btn_edit.gif"" height=""16"" width=""39"" onClick=""EditDeleteItem(this.form.name,'edit');"">" & vbcrlf
	sTemp = sTemp & "<input type=""image"" src=""https://www.cellularoutfitter.com/images/cart/btn_delete.gif"" height=""15"" width=""50"" onClick=""EditDeleteItem(this.form.name,'delete');"">" & vbcrlf
	sTemp = sTemp & "</form>" & vbcrlf
	sTemp = sTemp & "</td>" & vbcrlf
	sTemp = sTemp & "<td align=""right"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text"">" & formatCurrency((cDbl(sItemPrice) * cDbl(nQty))) & "</td>" & vbcrlf
	sTemp = sTemp & "</tr>" & vbcrlf
	fWriteBasketRow = sTemp
end function
%>
