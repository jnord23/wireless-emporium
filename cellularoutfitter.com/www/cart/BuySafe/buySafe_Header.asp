<%
' Basic SOAP header values
dim WSSERVER, USERNAME, PASSWORD, STORE_TOKEN, VERSION, URL, XMLNS
'WSSERVER 		= "sbws.buysafe.com"						' sandbox
WSSERVER		= "api.buysafe.com"							' production
'USERNAME		= "WirelessEmporium_Sandbox"				' sandbox
USERNAME		= "WirelessEmporium"						' production
'PASSWORD		= "094084A1-CF56-43EA-AFD0-B0A664A4AD0A"	' sandbox
PASSWORD		= "CB778FEA-D3EA-4416-8AD3-F3546D148CD4"	' production
'STORE_TOKEN	= "3b8582f3-6887-47d5-a152-5d76210c69dc"	' sandbox
STORE_TOKEN		= "aefcc1b3-49be-42cd-936b-b3e8aa781bc1"	' production

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' These should never need to be changed
VERSION      = "610"
URL          = "https://" & WSSERVER & "/BuysafeWS/CheckoutAPI.dll"
XMLNS        = "http://ws.buysafe.com"

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Build the SOAP Header - which is common to ALL requests for a particular store
dim SOAPHEADER
SOAPHEADER = SOAPHEADER &  "  <BuySafeWSHeader xmlns=""" & XMLNS & """>"
SOAPHEADER = SOAPHEADER &  "    <Version>" & VERSION & "</Version>"
SOAPHEADER = SOAPHEADER &  "  </BuySafeWSHeader>"
SOAPHEADER = SOAPHEADER &  "  <MerchantServiceProviderCredentials xmlns=""" & XMLNS & """>"
SOAPHEADER = SOAPHEADER &  "    <UserName>" & USERNAME & "</UserName>"
SOAPHEADER = SOAPHEADER &  "    <Password>" & PASSWORD & "</Password>"
SOAPHEADER = SOAPHEADER &  "  </MerchantServiceProviderCredentials>"
SOAPHEADER = SOAPHEADER &  "  <BuySafeUserCredentials xmlns=""" & XMLNS & """>"
SOAPHEADER = SOAPHEADER &  "    <AuthenticationTokens>"
SOAPHEADER = SOAPHEADER &  "      <string>" & STORE_TOKEN & "</string>"
SOAPHEADER = SOAPHEADER &  "    </AuthenticationTokens>"
SOAPHEADER = SOAPHEADER &  "  </BuySafeUserCredentials>"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%>
