<script type="text/javascript" language="javascript">
	function buySAFEOnClick(NewWantsBondValue) {
		document.cart.WantsBondField.value = NewWantsBondValue;
		document.cart.submit();
	}
</script>
<!--#include virtual="/cart/buysafe/buySafe_header.asp"-->
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Cart-level information
dim ShoppingCartId, ClientIP, WantsBondValue, WantsBondHasValue, buySAFEUID
dim Title, MarketplaceItemCode, QuantityPurchased, Price, ItemURL, PictureURL, ThumbnailURL
ClientIP = Request.ServerVariables("REMOTE_ADDR")	' The IP address of the buyer

on error resume next
	buySAFEUID = Request.Cookies("buySAFEUID")
	if isNull(Request.Cookies("ShoppingCartId")) or Request.Cookies("ShoppingCartId") = "" then
		' Your unique shopping cart ID, prefixed by your username (for ease of debugging).  Increment this to produce a new cart.
		ShoppingCartId = "CO-buySAFE-" & generateRequestID()
		Response.Cookies("ShoppingCartId") = ShoppingCartId
		Response.Cookies.Expires = DateAdd("d", 3, now)
	else
		call fOpenConn()
		SQL = "SELECT orderID FROM we_orders WHERE BuySafeCartID = '" & Request.Cookies("ShoppingCartId") & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if RS.eof then
			ShoppingCartId = Request.Cookies("ShoppingCartId")
		else
			ShoppingCartId = "CO-buySAFE-" & generateRequestID()
			Response.Cookies("ShoppingCartId") = ShoppingCartId
			Response.Cookies.Expires = DateAdd("d", 3, now)
		end if
		call fCloseConn()
	end if
	
	if Request.Cookies("WantsBond") = "" then			' This cart is new.  Make sure to update ShoppingCartId to get a new cart
		WantsBondValue = "false"
		WantsBondHasValue = "false"
	else
		if request.form("WantsBondField") = "" then					' Restore the cart from the prior session or replace to restore from database
			WantsBondValue = Request.Cookies("WantsBond")
			WantsBondHasValue = "true"
		else
			WantsBondValue = request.form("WantsBondField")			' Get the value from the previous button click
			WantsBondHasValue = "true"
		end if                         
	end if
	
	if request.form("WantsBondField") = "true" or Request.Cookies("WantsBond") = "true" then
		NewWantsBondValue = "false"
	else
		NewWantsBondValue = "true"
	end if
on error goto 0

if isNull(ShoppingCartId) or ShoppingCartId = "" then ShoppingCartId = "CO-buySAFE-" & generateRequestID()

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Now, for the Items in your cart.  Presumably, you have them in some kind of Array of objects
' So, you'll want to do the following FOR EACH item in your cart
dim ShoppingCartItem
if right(strItemCheck,1) = "," then strItemCheck = left(strItemCheck,len(strItemCheck)-1)
itemArray = split(strItemCheck,",")
QtyArray = split(strQty,",")
call fOpenConn()

for i = 0 to ubound(itemarray)
	set RSbuysafe = server.createobject("ADODB.recordset")
	if itemarray(i) => 1000000 then
		SQL = "SELECT artist + ' ' + designName as itemDesc_CO, image as itemPic_CO, price_we as price_CO, msrp as price_retail, 1 as itemWeight FROM we_items_musicSkins WHERE id='" & itemarray(i) & "'"
	else
		SQL = "SELECT itemDesc_CO, itemPic_CO, price_CO, price_retail, itemWeight FROM we_items WHERE itemID='" & itemarray(i) & "'"
	end if
	RSbuysafe.open SQL, oConn, 3, 3
	
	' This next chunk represents an item in your shopping cart
	' As you'll see below, you'll create a <ShoppingCartItem> node for each item in the cart
	Title                = rsBuySafe("itemDesc_CO")
	MarketplaceItemCode  = itemarray(i)				' The unique ID that your system uses to track this item
	QuantityPurchased    = QtyArray(i)				' The number of this item being purchased.  Use this instead of adding the same item the a single cart multiple times.
	Price                = rsBuySafe("price_CO")	' The item's price, in USD
	ItemURL              = "http://www.cellularoutfitter.com/p-" & itemarray(i) &  "-" & formatSEO(Title) & ".html"		' The URL to view the item
	PictureURL           = "https://www.cellularoutfitter.com/productpics/big/" & rsbuysafe("itemPic_CO")					' The URL for a main image for the item (optional)
	ThumbnailURL         = "https://www.cellularoutfitter.com/productpics/icon/" & rsbuysafe("itemPic_CO")					' The URL for a thubmnail picture (optional)
	
	ShoppingCartItem = ShoppingCartItem & "<ShoppingCartItem>"
	ShoppingCartItem = ShoppingCartItem & "  <UserToken>" & STORE_TOKEN & "</UserToken>"    ' Any store token that exists in a ShoppingCartItem must also be included in AuthenticationTokens in the header.
	ShoppingCartItem = ShoppingCartItem & "  <Title>" & XMLencode(Title) & "</Title>"
	ShoppingCartItem = ShoppingCartItem & "  <MarketplaceItemCode>" & MarketplaceItemCode & "</MarketplaceItemCode>"
	ShoppingCartItem = ShoppingCartItem & "  <StockKeepingUnit>" & StockKeepingUnit & "</StockKeepingUnit>"
	ShoppingCartItem = ShoppingCartItem & "  <QuantityPurchased>" & QuantityPurchased & "</QuantityPurchased>"
	ShoppingCartItem = ShoppingCartItem & "  <PriceInfo>"
	ShoppingCartItem = ShoppingCartItem & "    <FinalPrice>"
	ShoppingCartItem = ShoppingCartItem & "      <Value>" & Price & "</Value>"
	ShoppingCartItem = ShoppingCartItem & "      <CurrencyCode>USD</CurrencyCode>"
	ShoppingCartItem = ShoppingCartItem & "    </FinalPrice>"
	ShoppingCartItem = ShoppingCartItem & "  </PriceInfo>"
	ShoppingCartItem = ShoppingCartItem & "  <URLInfo>"
	ShoppingCartItem = ShoppingCartItem & "    <ViewItem>" & ItemURL & "</ViewItem>"
	ShoppingCartItem = ShoppingCartItem & "    <Picture>" & PictureURL & "</Picture>"
	ShoppingCartItem = ShoppingCartItem & "    <Thumbnail>" & ThumbnailURL & "</Thumbnail>"
	ShoppingCartItem = ShoppingCartItem & "  </URLInfo>"
	ShoppingCartItem = ShoppingCartItem & "</ShoppingCartItem>"
	
	RSbuysafe.close
	set RSbuysafe = nothing
next 
i = i + 1
call fCloseConn()

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Prepare the XML request by assembling the parts above with a few additional fields
dim AddUpdateShoppingCart
AddUpdateShoppingCart = AddUpdateShoppingCart & "<AddUpdateShoppingCart xmlns=""" & XMLNS & """>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "  <ShoppingCartAddUpdateRQ xmlns=""" & XMLNS & """>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    <ShoppingCartId>" & ShoppingCartId & "</ShoppingCartId>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    <SessionId>" & buySAFEUID & "</SessionId>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    <ClientIP>" & ClientIP & "</ClientIP>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    <WantsBond>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "      <Value>" & WantsBondValue & "</Value>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "      <HasBoolean>" & WantsBondHasValue & "</HasBoolean>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    </WantsBond>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    <Items>"
AddUpdateShoppingCart = AddUpdateShoppingCart &        ShoppingCartItem		' This contains a ShoppingCartItem for each item in your cart at this point
AddUpdateShoppingCart = AddUpdateShoppingCart & "    </Items>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "  </ShoppingCartAddUpdateRQ>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "</AddUpdateShoppingCart>"

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Finally, wrap the whole thing in the soap envelop and include header and body tags
dim SOAPREQUEST
SOAPREQUEST = SOAPREQUEST & "<?xml version=""1.0"" encoding=""utf-8""?>"
SOAPREQUEST = SOAPREQUEST & "<soap:Envelope "
SOAPREQUEST = SOAPREQUEST & "		xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""" 
SOAPREQUEST = SOAPREQUEST & "		xmlns:xsd=""http://www.w3.org/2001/XMLSchema"""
SOAPREQUEST = SOAPREQUEST & "		xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
SOAPREQUEST = SOAPREQUEST & "  <soap:Header>"
SOAPREQUEST = SOAPREQUEST & SOAPHEADER
SOAPREQUEST = SOAPREQUEST & "  </soap:Header>"
SOAPREQUEST = SOAPREQUEST & "  <soap:Body>"
SOAPREQUEST = SOAPREQUEST & AddUpdateShoppingCart
SOAPREQUEST = SOAPREQUEST & "<GetbuySAFEDateTime xmlns=""http://ws.buysafe.com"" />"
SOAPREQUEST = SOAPREQUEST & "  </soap:Body>"
SOAPREQUEST = SOAPREQUEST & "</soap:Envelope>"

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Send the call
set XMLRecv = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
XMLRecv.setTimeouts 3000, 3000, 3000, 3000
XMLRecv.open "POST", URL, false
XMLRecv.setRequestHeader "Content-Type", "text/xml"
XMLRecv.setRequestHeader "SOAPAction", "http://ws.buysafe.com/AddUpdateShoppingCart"
on error resume next
	XMLRecv.send (SOAPREQUEST)
	'iStatus = XMLRecv.status
on error goto 0
if XMLRecv.readyState <> 4 then
	XMLRecv.abort
else
	XMLResult = XMLRecv.responseText
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Parsing the response
	set xmlDoc = Server.CreateObject("Msxml2.DOMDocument.3.0")
	xmlDoc.async = "false"
	xmlDoc.loadxml(xmlresult)
	if xmlDoc.parseError.errorCode <> 0 then
		'XML error handling
	end if
	
	dim IsBuySafeEnabled
	if instr(1,xmlresult,"<faultcode>",vbtextcompare) then
		'response.Write "<BR>Error detected."												' Error handling here.
	else    
		isSuccessful = xmlDoc.getElementsByTagName("isSuccessful").item(0).text
		if ucase(isSuccessful) = "TRUE" then
			IsBuySafeEnabled = xmlDoc.getElementsByTagName("IsBuySafeEnabled").item(0).text
			if Ucase(ISBuySafeEnabled) = "TRUE" then
				buysafeamount = xmlDoc.getElementsByTagName("BondCostDisplayText").item(0).text
				if isNull(buysafeamount) or not isNumeric(buysafeamount) then buysafeamount = 0
				session("buysafeamount") = buysafeamount
				buySafeBondCost = xmlDoc.getElementsByTagName("TotalBondCost").item(0).text
				%>
                    <tr height="70px">
                        <td align="center" colspan="5" style="padding-left:75px;">
                        	<table width="100%" align="right" border="0" cellpadding="0" cellspacing="0">
                            	<tr>
                                    <td align="right" valign="middle" width="100%" style="padding-right:10px;">
                                        <span id="buySAFE_Kicker" name="buySAFE_Kicker" type="Kicker Recommended Green Arrow 335x55"></span><br><a class="link-cart" target="_blank" href="<%=xmlDoc.getElementsByTagName("CartDetailsUrl").item(0).text%>"><%=xmlDoc.getElementsByTagName("CartDetailsDisplayText").item(0).text%></a>
                                    </td>
                                    <td align="right" valign="middle" style="padding-right:10px;">
                                        <%=xmlDoc.getElementsByTagName("ShoppingCartAddUpdateRS/BondingSignal").item(0).text%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" valign="middle" style="font-size:13px;"><b><%=formatCurrency(buysafeamount)%></b></td>
                    </tr>
				<%
				on error resume next
					if (xmlDoc.getElementsByTagName("BondCostDisplayText").item(0).text <> "") then		' BondCostDisplayText indicates the buySAFE status of WantsBond.
						Response.Cookies("WantsBond") = "true"
					else
						Response.Cookies("WantsBond") = "false"
					end if
				on error goto 0
			else
				'response.write "buySAFE is not enabled.  buySAFE elements will not be displayed.<BR>"
			end if 
		else
			'response.write "Response returns unsuccessful.<BR>"		' Error handling here.
		end if 
	end if
end if
set xmlDoc = nothing
set XMLRecv = nothing

function generateRequestID()
	randomize
	dim strResult, n, strChars
	strChars = ""
	for a = 1 to 20
		n = Int(Rnd * 26) + 75
		if n >= 91 then
			strChars = strChars & chr(n - 43)
		else
			strChars = strChars & chr(n)
		end if
	next
	strResult = Mid(CCFormatDateTime(Now), 2, 14) & strChars
	generateRequestID = strResult
end function

function CCFormatDateTime(ByVal dtmDate)
	dim strDate, strTmp
	
	If (IsNull(dtmDate)) Then
		CCFormatDateTime = Null
		Exit Function
	End If
	
	strTmp = Trim(CStr(Month(dtmDate)))
	If (Len(strTmp) < 2) Then strTmp = "0" + strTmp
	strDate = Trim(CStr(Year(dtmDate))) & strTmp
	
	strTmp = Trim(CStr(Day(dtmDate)))
	If (Len(strTmp) < 2) Then strTmp = "0" + strTmp
	strDate = strDate & strTmp
	
	strTmp = Trim(CStr(Hour(dtmDate)))
	If (Len(strTmp) < 2) Then strTmp = "0" + strTmp
	strDate = strDate & strTmp
	
	strTmp = Trim(CStr(Minute(dtmDate)))
	If (Len(strTmp) < 2) Then strTmp = "0" + strTmp
	strDate = strDate & strTmp
	
	strTmp = Trim(CStr(Second(dtmDate)))
	If (Len(strTmp) < 2) Then strTmp = "0" + strTmp
	strDate = strDate & strTmp
	
	CCFormatDateTime = "A" & strDate & ".000"
end function

function XMLencode(str)
	XMLencode = replace(replace(replace(replace(replace(str,"&","&amp;"),"<","&lt;"),">","&gt;"),"'","&apos;"),chr(34),"&quot;")
end function
%>

<form name="cart" method="post">
	<input name="WantsBondField" value="<%=NewWantsBondValue%>" type="hidden">
</form>
