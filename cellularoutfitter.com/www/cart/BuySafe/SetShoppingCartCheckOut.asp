<!--#include virtual="/cart/buysafe/buySafe_header.asp"-->
<%
' Cart-level information
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
dim SessionId, ClientIP, WantsBondHasValue
dim Title, MarketplaceItemCode, StockKeepingUnit, QuantityPurchased, Price, PriceCurrency, ItemURL, PictureURL, ThumbnailURL
ClientIP = Request.ServerVariables("REMOTE_ADDR")   ' The IP address of the buyer

if BuySafeAmount > 0 then
	WantsBondValue = "true"
	WantsBondHasValue = "true"
else
	WantsBondValue = "false"
	WantsBondHasValue = "true"
end if

if BuySafeExtOrderType = 2 then
	dim RSbuysafe
	strSQL = "SELECT A.*, B.BuySafeAmount FROM CO_Accounts A INNER JOIN we_orders B ON A.accountid = B.accountid WHERE B.orderid='" & nOrderId & "'"
	set RSbuysafe = Server.CreateObject("ADODB.Recordset")
	RSbuysafe.open strSql, oConn, 3, 3
	if not RS.eof then
		sFname = RSbuysafe("fname")
		sLname = RSbuysafe("lname")
		sEmail = RSbuysafe("email")
		sSCountry = RSbuysafe("sCountry")
		sSZip = RSbuysafe("szip")
		sBCountry = RSbuysafe("bCountry")
		sBZip = RSbuysafe("bzip")   
	end if
	RSbuysafe.close
	set RSbuysafe = nothing
end if

if isNull(sSCountry) or sSCountry = "" then
	sSCountry = "US"
elseif Ucase(sSCountry) = "CANADA" then
	sSCountry = "CA"
end if
if isNull(sBCountry) or sBCountry = "" then
	sBCountry = "US"
elseif Ucase(sBCountry) = "CANADA" then
	sBCountry = "CA"
end if

session("errorSQL") = sFname & " | " & sLname & " | " & sEmail & " | " & sSCountry & " | " & sSZip & " | " & sBCountry & " | " & sBZip

' Buyer information
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Next we'll build some of the AddUpdate inner objects, and asseble everything later
dim BuyerInfo
BuyerInfo = BuyerInfo & "<BuyerInfo>"
BuyerInfo = BuyerInfo & "  <FirstName>" & XMLencode(sFname) & "</FirstName>"
BuyerInfo = BuyerInfo & "  <LastName>" & XMLencode(sLname) & "</LastName>"
BuyerInfo = BuyerInfo & "  <Email>" & XMLencode(sEmail) & "</Email>"
BuyerInfo = BuyerInfo & "  <ShippingAddress>"
BuyerInfo = BuyerInfo & "    <PostalCode>" & XMLencode(sSZip) & "</PostalCode>"
BuyerInfo = BuyerInfo & "    <CountryCode>" & XMLencode(sSCountry) & "</CountryCode>"
BuyerInfo = BuyerInfo & "  </ShippingAddress>"
BuyerInfo = BuyerInfo & "  <BillingAddress>"
BuyerInfo = BuyerInfo & "    <PostalCode>" & XMLencode(sBZip) & "</PostalCode>"
BuyerInfo = BuyerInfo & "    <CountryCode>" & XMLencode(sBCountry) & "</CountryCode>"
BuyerInfo = BuyerInfo & "  </BillingAddress>"
BuyerInfo = BuyerInfo & "</BuyerInfo>"

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Now, for the Items in your cart.  Presumably, you have them in some kind of Array of objects
' So, you'll want to do the following FOR EACH item in your cart
dim ShoppingCartItem
SQL = "SELECT c.brandName, d.modelName, a.partNumber, A.itemid, A.price_CO, A.itemDesc_CO, A.itemPic_CO, b.orderid, B.quantity"
SQL = SQL & " FROM (we_items A INNER JOIN we_orderdetails B ON A.itemid = B.itemid) left join we_brands c on a.brandID = c.brandID left join we_models d on a.modelID = d.modelID"
SQL = SQL & " WHERE B.orderid = '" & nOrderId & "'"
set RSbuysafe = server.createobject("ADODB.recordset")
RSbuysafe.open SQL, oConn, 3, 3
do until RSbuysafe.eof
	brandName = RSbuysafe("brandName")
	modelName = RSbuysafe("modelName")
	partNumber = RSbuysafe("partNumber")
	ItemPrice = RSbuysafe("price_CO")
	Title                = insertDetails(rsBuySafe("itemDesc_CO"))
	MarketplaceItemCode  = rsBuySafe("itemid")           
	QuantityPurchased    = rsBuySafe("quantity")        
	Price                = ItemPrice    ' The item's price, in USD
	PriceCurrency        = "USD"                     ' Must be USD
	ItemURL              = "http://www.cellularoutfitter.com/p-" & rsBuySafe("itemid") & "-" & formatSEO(title) & ".html" ' The URL to view the item
	PictureURL           = "https://www.cellularoutfitter.com/productpics/big/" & rsbuysafe("itemPic_CO")   ' The URL for a main image for the item (optional)
	ThumbnailURL         = "https://www.cellularoutfitter.com/productpics/icon/" & rsbuysafe("itemPic_CO")   ' The URL for a thubmnail picture (optional)
	
	ShoppingCartItem = ShoppingCartItem & "<ShoppingCartItem>"
	ShoppingCartItem = ShoppingCartItem & "  <UserToken>" & STORE_TOKEN & "</UserToken>"    ' Any store token that exists in a ShoppingCartItem must also be included in AuthenticationTokens in the header.
	ShoppingCartItem = ShoppingCartItem & "  <Title>" & XMLencode(Title) & "</Title>"
	ShoppingCartItem = ShoppingCartItem & "  <MarketplaceItemCode>" & MarketplaceItemCode & "</MarketplaceItemCode>"
	ShoppingCartItem = ShoppingCartItem & "  <StockKeepingUnit>" & StockKeepingUnit & "</StockKeepingUnit>"
	ShoppingCartItem = ShoppingCartItem & "  <QuantityPurchased>" & QuantityPurchased & "</QuantityPurchased>"
	ShoppingCartItem = ShoppingCartItem & "  <PriceInfo>"
	ShoppingCartItem = ShoppingCartItem & "    <FinalPrice>"
	ShoppingCartItem = ShoppingCartItem & "      <Value>" & Price & "</Value>"
	ShoppingCartItem = ShoppingCartItem & "      <CurrencyCode>" & PriceCurrency & "</CurrencyCode>"
	ShoppingCartItem = ShoppingCartItem & "    </FinalPrice>"
	ShoppingCartItem = ShoppingCartItem & "  </PriceInfo>"
	ShoppingCartItem = ShoppingCartItem & "  <URLInfo>"
	ShoppingCartItem = ShoppingCartItem & "    <ViewItem>" & ItemURL & "</ViewItem>"
	ShoppingCartItem = ShoppingCartItem & "    <Picture>" & PictureURL & "</Picture>"
	ShoppingCartItem = ShoppingCartItem & "    <Thumbnail>" & ThumbnailURL & "</Thumbnail>"
	ShoppingCartItem = ShoppingCartItem & "  </URLInfo>"
	ShoppingCartItem = ShoppingCartItem & "</ShoppingCartItem>"
	RSbuySafe.movenext
loop

RSbuysafe.close
set RSbuysafe = nothing

on error resume next
	buySAFEUID = Request.Cookies("buySAFEUID")
on error goto 0

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Prepare the XML request by assembling the parts above with a few additional fields
dim AddUpdateShoppingCart
AddUpdateShoppingCart = AddUpdateShoppingCart & "<SetShoppingCartCheckout xmlns=""" & XMLNS & """>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "  <ShoppingCartCheckoutRQ xmlns=""" & XMLNS & """>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    <ShoppingCartId>" & ShoppingCartId & "</ShoppingCartId>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "	 <OrderNumber>" & nOrderId & "</OrderNumber>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    <SessionId>" & buySAFEUID & "</SessionId>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    <ClientIP>" & ClientIP & "</ClientIP>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    <WantsBond>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "      <Value>" & WantsBondValue & "</Value>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "      <HasBoolean>" & WantsBondHasValue & "</HasBoolean>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "    </WantsBond>"
AddUpdateShoppingCart = AddUpdateShoppingCart &        BuyerInfo
AddUpdateShoppingCart = AddUpdateShoppingCart & "    <Items>" & ShoppingCartItem & "</Items>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "  </ShoppingCartCheckoutRQ>"
AddUpdateShoppingCart = AddUpdateShoppingCart & "</SetShoppingCartCheckout>"

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Finally, wrap the whole thing in the soap envelop and include header and body tags
dim SOAPREQUEST
SOAPREQUEST = SOAPREQUEST & "<?xml version=""1.0"" encoding=""utf-8""?>"
SOAPREQUEST = SOAPREQUEST & "<soap:Envelope "
SOAPREQUEST = SOAPREQUEST & "		xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""" 
SOAPREQUEST = SOAPREQUEST & "		xmlns:xsd=""http://www.w3.org/2001/XMLSchema"""
SOAPREQUEST = SOAPREQUEST & "		xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
SOAPREQUEST = SOAPREQUEST & "  <soap:Header>"
SOAPREQUEST = SOAPREQUEST & SOAPHEADER
SOAPREQUEST = SOAPREQUEST & "  </soap:Header>"
SOAPREQUEST = SOAPREQUEST & "  <soap:Body>"
SOAPREQUEST = SOAPREQUEST & AddUpdateShoppingCart
SOAPREQUEST = SOAPREQUEST & "<GetbuySAFEDateTime xmlns=""http://ws.buysafe.com"" />"
SOAPREQUEST = SOAPREQUEST & "  </soap:Body>"
SOAPREQUEST = SOAPREQUEST & "</soap:Envelope>"

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

strBody = SOAPREQUEST & vbcrlf & vbcrlf & XMLResult

' Send the call
set XMLRecv = Server.CreateObject("MSXML2.ServerXMLHTTP")
XMLRecv.setTimeouts 3000, 3000, 3000, 3000
XMLRecv.Open "POST", URL, False  
XMLRecv.setRequestHeader "Content-Type", "text/xml"
XMLRecv.setRequestHeader "SOAPAction", "http://ws.buysafe.com/SetShoppingCartCheckout"
on error resume next
	XMLRecv.send (SOAPREQUEST)
	'iStatus = XMLRecv.status
on error goto 0
if XMLRecv.readyState <> 4 then
	Response.Cookies("CObuySAFE")("WantsBond") = "false"
	Response.Cookies("CObuySAFE")("ShoppingCartId") = ""
	XMLRecv.abort
else
	Response.Cookies("CObuySAFE")("ShoppingCartId") = ""
	XMLResult = XMLRecv.responseText
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Parsing the response
	set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")
	xmlDoc.async = false
	xmlDoc.loadxml(xmlresult)
	if xmlDoc.parseError.errorCode <> 0 then
		strTo = "webmaster@cellularoutfitter.com,apisupport@buysafe.com"
		strFrom = "support@cellularoutfitter.com"
		strSubject = "buySAFE XML Tranmission ERROR"
		strBody = strBody & xmlresult
		CDOSend strTo,strFrom,strSubject,strBody
	end if
	
	dim IsBuySafeEnabled
	if instr(1,xmlresult,"<faultcode>",vbtextcompare) then
		strTo = "webmaster@cellularoutfitter.com,apisupport@buysafe.com"
		strFrom = "support@cellularoutfitter.com"
		strSubject = "buySAFE Transaction ERROR"
		strBody = strBody & xmlresult
		CDOSend strTo,strFrom,strSubject,strBody
	else
		isSuccessful = xmlDoc.getElementsByTagName("isSuccessful").item(0).text
		if ucase(isSuccessful) = "TRUE" then
			' Save response status to DB
		else
			strTo = "webmaster@cellularoutfitter.com,apisupport@buysafe.com"
			strFrom = "support@cellularoutfitter.com"
			strSubject = "buySAFE Unsuccessful Transaction"
			strBody = strBody & xmlresult
			CDOSend strTo,strFrom,strSubject,strBody
		end if
	end if
end if
set xmlDoc = nothing
set XMLRecv = nothing
%>
