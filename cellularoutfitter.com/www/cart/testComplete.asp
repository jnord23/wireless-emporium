<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_Webloyalty.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/framework/utility/outOfStock.asp"-->
<!--#include virtual="/framework/utility/readTextFile.asp"-->
<!--#include virtual="/includes/asp/inc_getMySession.asp"-->
<%
pageTitle = "complete.asp"
dim useHttps : useHttps = 1
dim nAccountID, nOrderID, nOrderGrandTotal, nOrderSubTotal
dim curPageName : curPageName = "Order Complete"
if not isNumeric(request.querystring("a")) or not isNumeric(request.querystring("o")) or not isNumeric(request.querystring("d")) or not isNumeric(request.querystring("c")) then
	response.redirect("/")
	response.end
end if
nAccountID = request.querystring("a")
nOrderID = request.querystring("o")
nOrderGrandTotal = request.querystring("d")
nOrderSubTotal = request.querystring("c")
usePostPurchase = request.querystring("pp")
if usePostPurchase = "Y" then
	sql	=	"select	accountid, isnull(CIM_CustomerProfileID, 0) profileID, isnull(CIM_CustomerPaymentProfileID, 0) paymentProfileID " & vbcrlf & _
			"from	co_accounts" & vbcrlf & _
			"where	accountid = '" & nAccountID & "'"
	set rsCIM = oConn.execute(sql)
	if not rsCIM.eof then
		if clng(rsCIM("profileID")) = 0 or clng(rsCIM("paymentProfileID")) = 0 then usePostPurchase = "N"
	else
		usePostPurchase = "N"
	end if
end if

dim mobileOrder : mobileOrder = 0
if instr(lcase(request.ServerVariables("HTTP_HOST")),"m.cellularoutfitter.com") > 0 then mobileOrder = 1

sql = "delete from we_remarketing where sessionID = '" & mySession & "' and siteID = 2"
session("errorSQL") = sql
oConn.execute(sql)

call fOpenConn()
SQL = "SELECT b.email FROM we_orders A"
SQL = SQL & " INNER JOIN CO_accounts B ON A.accountid = B.accountid"
SQL = SQL & " LEFT JOIN CO_coupons C ON A.couponid = C.couponid"
SQL = SQL & " WHERE A.orderid = '" & nOrderID & "' AND A.accountid = '" & nAccountID & "' AND (A.ordergrandtotal = '" & nOrderGrandTotal & "' OR A.ordergrandtotal = '" & formatNumber(nOrderGrandTotal,2) & "')"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	RS.close
	cdo_from = "Automatic E-Mail from CellularOutfitter.com<sales@cellularoutfitter.com>"
	cdo_subject = "SQL eof in CO"
	cdo_body = "<p>" & SQL & "</p>"
	cdo_to = "webmaster@cellularoutfitter.com"
	CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
	response.redirect("/")
	response.end
else
	customerEmail = RS("Email")
end if
RS.close
set RS = nothing

' Adjust Inventory
'dim nProdQuantity, nPartNumber
dim decreaseSQL, RS2, allItems, allQty, allPrice, allItems_br, allQty_br, allPrice_br
SQL = "SELECT A.quantity, B.PartNumber, b.itemID, b.price_co, B.ItemKit_NEW, (select count(*) from we_invRecord where orderID = " & nOrderID & ") as invAdjust FROM we_orderdetails A INNER JOIN we_items B ON A.itemID=B.itemID WHERE A.orderID = '" & nOrderID & "'"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

allItems = ""
allQty = ""
allPrice = ""

dim ajustInv
ajustInv = 1
if RS.EOF then
	ajustInv = 0
else
	if cdbl(RS("invAdjust")) > 0 then ajustInv = 0
end if

dim cureBitProducts : cureBitProducts = ""
if ajustInv = 1 then
	do until RS.eof
		nProdQuantity = RS("quantity")
		nPartNumber = RS("PartNumber")
		nItemID = RS("itemID")
		nPrice = RS("price_co")
		cureBitProducts = cureBitProducts & "&p[i][0][product_id]=" & nItemID & "&p[i][0][price]=" & nPrice & "&p[i][0][quantity]=" & nProdQuantity
		if allItems = "" then allItems = (cdbl(nItemID) + 300001) else allItems = allItems & "," & (cdbl(nItemID) + 300001)
		if allQty = "" then allQty = nProdQuantity else allQty = allQty & "," & nProdQuantity
		if allPrice = "" then allPrice = nPrice else allPrice = allPrice & "," & nPrice
		' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
		' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
		if isNull(RS("ItemKit_NEW")) then
			SQL = "SELECT top 1 itemID,typeID,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND master = 1"
		else
			SQL = "SELECT (select top 1 itemID from we_items where partNumber = a.partNumber and master = 1) as itemID,a.typeID,a.partNumber,(select top 1 inv_qty from we_items where partNumber = a.partNumber and master = 1) as inv_qty FROM we_items a WHERE a.itemID IN (" & RS("ItemKit_NEW") & ")"
		end if
		set RS2 = Server.CreateObject("ADODB.Recordset")
		RS2.open SQL, oConn, 0, 1
		do until RS2.eof
			curQty = RS2("inv_qty")
			if curQty - nProdQuantity > 0 then
				On Error Resume Next
				sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS2("itemID") & "," & curQty & "," & nProdQuantity & "," & nOrderID & ",0,'CO Customer Order','" & now & "')"
				session("errorSQL") = sql
				oConn.execute(sql)
				On Error Goto 0
				
				decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty - " & nProdQuantity & " WHERE itemID = '" & RS2("itemID") & "' and master = 1"
				session("errorSQL") = sql
				oConn.execute(decreaseSQL)
			else
				On Error Resume Next
				sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS2("itemID") & "," & curQty & "," & nProdQuantity & "," & nOrderID & ",0,'CO Customer Order *Out of Stock*','" & now & "')"
				session("errorSQL") = sql
				oConn.execute(sql)
				On Error Goto 0
				
				decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE master = 1 and PartNumber = '" & nPartNumber & "'"
				if RS2("typeID") <> 3 then
					' Send zero-inventory e-mail
					outOfStockEmail nPartNumber,curQty,nProdQuantity
				end if
				session("errorSQL") = sql
				oConn.execute(decreaseSQL)
			end if
			RS2.movenext
		loop
		RS2.close
		set RS2 = nothing
		if not RS.EOF then RS.movenext
	loop
end if

dim thisEmail, incEmail
incEmail = true

%>
<!--#include virtual="/includes/asp/inc_receipt.asp"-->
<%

for thisEmail = 1 to 2
	if extOrderType = 1 then
		if thisEmail = 1 then
			sFromName = "Automatic E-Mail from CellularOutfitter.com"
			sFromAddress = "sales@CellularOutfitter.com"
			sAddRecipient1 = sEmail
			sSubject = "Cellular Outfitter PAYPAL Order Confirmation"
		else
			sFromName = "Automatic E-Mail from CellularOutfitter.com"
			sFromAddress = "sales@CellularOutfitter.com"
			sSubject = "A New PAYPAL Order from Cellular Outfitter"
		end if
	else
		if thisEmail = 1 then
			sFromName = "Automatic E-Mail from CellularOutfitter.com"
			sFromAddress = "sales@CellularOutfitter.com"
			sAddRecipient1 = sEmail
			sSubject = "Cellular Outfitter Order Confirmation"
		else
			sFromName = "Automatic E-Mail from CellularOutfitter.com"
			sFromAddress = "sales@CellularOutfitter.com"
			sSubject = "A New Order from Cellular Outfitter"
		end if
	end if
	cdo_from = sFromName & "<" & sFromAddress & ">"
	cdo_subject = sSubject
	cdo_body = ReceiptText
	on error resume next
		if thisEmail = 1 then
			' to Customer
			cdo_to = "" & sAddRecipient1 & ""
			'cdo_to = "webmaster@cellularoutfitter.com"
			CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			if Err.Number <> 0 then
				session("mailError") = "<p><b>Your order is being fulfilled and shipped to the shipping address you provided.</b></p>"
				session("mailError") = session("mailError") & "<p>However, when attempting to e-mail your Order Confirmation to <b>" & cdo_to & "</b>, "
				session("mailError") = session("mailError") & "we received the following error from your e-mail provider:<br>" & Err.Description & "</p>"
				session("mailError") = session("mailError") & "<p>It is possible that you may not receive any e-mails from us (such as Shipping Confirmation, etc.) "
				session("mailError") = session("mailError") & "if we continue to encounter problems with this e-mail address.</p>"
				session("mailError") = session("mailError") & "<p>Please be assured, however, that your order will be fulfilled and shipped.</p>"
			else
				strSQL = "UPDATE we_orders SET emailSent = 'yes' WHERE orderid = '" & nOrderID & "'"
				oConn.execute strSQL
			end if
		else
			' to Cellular Outfitter recipients
			cdo_to = "shipping@wirelessemporium.com"
			'cdo_to = "webmaster@cellularoutfitter.com"
			CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		end if
	on error goto 0
next

if strOrderType <> "eBillme" then
	dim updateRS
	SQL = "UPDATE we_orders SET"
	SQL = SQL & " approved = -1"
	SQL = SQL & " WHERE orderid = '" & nOrderID & "'"
	'response.write "<p>SQL = " & SQL & "</p>" & vbcrlf
	set updateRS = oConn.execute(SQL)
end if

sql =	"select a.itemid, b.partNumber, b.itemDesc, b.itemDesc_co, c.typeName, b.price_CO, a.quantity, isnull(d.brandName, 'Universal') brandName, isnull(b.UPCCode, '') upc " &_
		"from we_orderDetails a " &_
			"left join we_Items b on a.itemID = b.itemID " &_
			"left join we_types c on b.typeID = c.typeID " &_
			"left join we_brands d on b.brandid = d.brandid " &_
		"where orderID = " & nOrderID
session("errorSQL") = sql
set jsRS = oConn.execute(sql)

jsProductLoop = 0
do while not jsRS.EOF
	jsProductLoop = jsProductLoop + 1
	transItem = "_gaq.push(['_addItem','" & nOrderID & "','" & jsRS("partNumber") & "','" & jsRS("itemDesc") & "','" & jsRS("typeName") & "','" & jsRS("price_CO") & "','" & jsRS("quantity") & "']);_gaq.push(['_trackTrans']);"
	
	countPriceGrabber = countPriceGrabber + 1
	strPriceGrabber = strPriceGrabber & "&item" & countPriceGrabber & "="
	strPriceGrabber = strPriceGrabber & server.URLencode(jsRS("brandName")) & "||"
	strPriceGrabber = strPriceGrabber & jsRS("price_CO") & "|"
	strPriceGrabber = strPriceGrabber & jsRS("itemid") & "|"
	strPriceGrabber = strPriceGrabber & jsRS("upc") & "|"
	strPriceGrabber = strPriceGrabber & jsRS("quantity")
	
	if br_conv_items = "" then
		br_conv_items = "{" & vbcrlf & _
						"	'prod_id' : """ & jsRS("itemid") & """," & vbcrlf & _
						"	'sku': """ & jsRS("itemid") & """," & vbcrlf & _
						"	'name': """ & jsRS("itemDesc_co") & """," & vbcrlf & _
						"	'quantity': """ & jsRS("quantity") & """," & vbcrlf & _
						"	'price': """ & jsRS("price_CO") & """," & vbcrlf & _
						"	'mod': """"" & vbcrlf & _
						"}" & vbcrlf
	else
		br_conv_items = br_conv_items & ",{" & vbcrlf & _
										"	'prod_id' : """ & jsRS("itemid") & """," & vbcrlf & _
										"	'sku': """ & jsRS("itemid") & """," & vbcrlf & _
										"	'name': """ & jsRS("itemDesc_co") & """," & vbcrlf & _
										"	'quantity': """ & jsRS("quantity") & """," & vbcrlf & _
										"	'price': """ & jsRS("price_CO") & """," & vbcrlf & _
										"	'mod': """"" & vbcrlf & _
										"}" & vbcrlf
	end if
	
	jsRS.movenext
loop

if mobileOrder = 1 then
	uaCode = "UA-1097464-3"
	storeName = "Cellular Outfitter Mobile"
else
	uaCode = "UA-1097464-1"
	storeName = "Cellular Outfitter"
end if
%>
<html>
<head>
	<title>Your Transaction is Being Processed...</title>
	<% call printPixel(1) %>
    <script type="text/javascript">
		//TestEffects MVT Code
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-36271972-1']);
		_gaq.push(['_trackPageview']);
		  _gaq.push(['_addTrans',
		  '<%=nOrderID%>',           // order ID - required
		  '<%=storeName%>',  // affiliation or store name
		  '<%=nOrderGrandTotal%>',          // total - required
		  '<%=nOrderTax%>',           // tax
		  '<%=nShipFee%>',              // shipping
		  '<%=sCity%>',       // city
		  '<%=sstate%>',     // state or province
		  '<%=sCountry%>'             // country
		]);
	  	<%=transItem%>
	  
	   (function() {
		  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
		
		//######################## New Google Analytics Code ########################' Added 2012-09-19
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<%=uaCode%>']);
		_gaq.push(['_trackPageview']);
		  _gaq.push(['_addTrans',
		  '<%=nOrderID%>',           // order ID - required
		  '<%=storeName%>',  // affiliation or store name
		  '<%=nOrderGrandTotal%>',          // total - required
		  '<%=nOrderTax%>',           // tax
		  '<%=nShipFee%>',              // shipping
		  '<%=sCity%>',       // city
		  '<%=sstate%>',     // state or province
		  '<%=sCountry%>'             // country
		]);
	  	<%=transItem%>
	  
	   (function() {
		  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
</head>
<% if mobileOrder = 1 then %>
<meta http-equiv="refresh" content="5;URL=http://<%=request.ServerVariables("HTTP_HOST")%>/confirm.html?o=<%=nOrderID%>&a=<%=nAccountID%>&d=<%=nOrderGrandTotal%>&pp=<%=usePostPurchase%>">
<% 
else 
	if instr(request.ServerVariables("HTTP_HOST"), "staging.") > 0 then
	%>
	<meta http-equiv="refresh" content="5;URL=http://<%=request.ServerVariables("HTTP_HOST")%>/confirm.asp?o=<%=nOrderID%>&a=<%=nAccountID%>&d=<%=nOrderGrandTotal%>&pp=<%=usePostPurchase%>">    
    <%
	else
	%>
	<!--<meta http-equiv="refresh" content="5;URL=https://<%=request.ServerVariables("HTTP_HOST")%>/confirm.asp?o=<%=nOrderID%>&a=<%=nAccountID%>&d=<%=nOrderGrandTotal%>&pp=<%=usePostPurchase%>">    -->
    <%
	end if
end if 
%>
<body>
<% call printPixel(2) %>
<table width="100%" border="0" align="center" cellpadding="20" cellspacing="0">
	<tr>
		<td align="center">
			<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
			<p>
				<strong><font color="#FF6600" size="4" face="Verdana, Arial, Helvetica, sans-serif">Your Transaction is Being Processed...</font></strong>
			</p>
			<p>
				<font face="verdana,tahoma,helvetica" size="2">VeriSign has routed, processed, and secured your payment information.
				<a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en" target="_blank"><br>More information about VeriSign.</a></font>
			</p>
			<p>
				<a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en" target="_blank"><img src="https://www.cellularoutfitter.com/images/seal_m_en.gif" width="115" height="82" border="0" alt="VeriSign Secured Site"></a>
			</p>
			<p>
				Please wait while your transaction is being processed<br>
				(DO NOT HIT BACK ON YOUR BROWSER).<br>
				You will soon be taken to an order receipt page confirming your order.
			</p>
			<%if session("mailError") <> "" then response.write "<p>" & session("mailError") & "</p>" & vbcrlf%>
			<p>
				<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>.... Exiting Secure Server ....</strong></font>
			</p>
		</td>
	</tr>
</table>
<%
if mobileOrder = 1 then
%>
<!-- Google Code for Sale Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1061947743;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "UdL3CJOVeBDfkrD6Aw";
var google_conversion_value = <%=nOrderGrandTotal%>;
/* ]]> */
</script>
<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1061947743/?value=<%=nOrderGrandTotal%>&amp;label=UdL3CJOVeBDfkrD6Aw&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<%
end if
%>
<% ' If this is an Affiliate Referral, add the appropriate tracking code %>
<!--#include virtual="/cart/includes/inc_trackingCodes.asp"-->
</body>
</html>