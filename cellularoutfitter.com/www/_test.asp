<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
dim modelPage
modelPage = 1

dim modelid
modelid = 1447

response.Cookies("myBrand") = 0
response.Cookies("myModel") = modelID

dim pageTitle
pageTitle = "Brand-Model"

call fOpenConn()

'sql	=	"select	distinct a.modelID, a.modelName, a.modelImg, a.brandID, b.brandName, c.typeID, d.typeName_co typename " & vbcrlf & _
'		"from	we_Models a join we_Brands b " & vbcrlf & _
'		"	on	a.brandID = b.brandID join we_items c " & vbcrlf & _
'		"	on	a.modelID = c.modelID and c.hideLive = 0 and c.ghost = 0 and c.price_CO > 0 and c.typeID <= 18 and c.typeid not in (17) join we_Types d " & vbcrlf & _
'		"	on	c.typeID = d.typeID " & vbcrlf & _
'		"where	a.hideLive = 0 and a.modelID = '" & modelid & "' " & vbcrlf & _
'		"union " & vbcrlf & _
'		"select	distinct a.modelID, a.modelName, a.modelImg, a.brandID, b.brandName, c.typeID, d.typeName_co typename " & vbcrlf & _
'		"from	we_Models a join we_Brands b " & vbcrlf & _
'		"	on	a.brandID = b.brandID join we_relatedItems c " & vbcrlf & _
'		"	on	a.modelID = c.modelID join we_Types d " & vbcrlf & _
'		"	on	c.typeID = d.typeID join we_Items e " & vbcrlf & _
'		"	on	c.ITEMID = e.itemID and e.hideLive = 0 and e.ghost = 0 and e.price_CO > 0 and e.typeid <= 18 and c.typeid not in (17)" & vbcrlf & _
'		"where	a.hideLive = 0 and a.modelID = '" & modelid & "' " & vbcrlf & _
'		"order by 6" & vbcrlf
		
sql	=	"select	distinct a.modelID, a.modelName, a.modelImg, a.brandID, b.brandName, d.typeid, d.typename" & vbcrlf & _
		"from	we_Models a join we_Brands b " & vbcrlf & _
		"	on	a.brandID = b.brandID join we_items c " & vbcrlf & _
		"	on	a.modelID = c.modelID join v_subtypeMatrix_co d " & vbcrlf & _
		"	on	c.subtypeid = d.subtypeid" & vbcrlf & _
		"where	(select top 1 inv_qty from we_Items where PartNumber = c.PartNumber and master = 1 order by inv_qty desc) > 0 and a.hideLive = 0 and a.modelID = '" & modelid & "' " & vbcrlf & _
		"	and	c.hideLive = 0 and c.ghost = 0 and c.price_co > 0" & vbcrlf & _
		"union " & vbcrlf & _
		"select	distinct a.modelID, a.modelName, a.modelImg, a.brandID, b.brandName, d.typeid, d.typename" & vbcrlf & _
		"from	we_Models a join we_Brands b " & vbcrlf & _
		"	on	a.brandID = b.brandID join we_subRelatedItems c " & vbcrlf & _
		"	on	a.modelID = c.modelID join v_subtypeMatrix_co d " & vbcrlf & _
		"	on	c.subtypeid = d.subtypeid join we_Items e " & vbcrlf & _
		"	on	c.ITEMID = e.itemID" & vbcrlf & _
		"where	a.hideLive = 0 and a.modelID = '" & modelid & "' " & vbcrlf & _
		"	and	e.hideLive = 0 and e.ghost = 0 and e.price_CO > 0" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct a.modelID, a.modelName, a.modelImg, a.brandID, b.brandName, 19 typeid, 'Vinyl Skins' as typename" & vbcrlf & _
		"from	we_Models a join we_Brands b " & vbcrlf & _
		"	on	a.brandID = b.brandID join we_items_musicskins c " & vbcrlf & _
		"	on	a.modelID = c.modelID " & vbcrlf & _
		"where	a.hideLive = 0 and a.modelID = '" & modelid & "' " & vbcrlf & _
		"	and	c.skip = 0 and c.deleteItem = 0 and c.artist <> '' and c.artist is not null and c.designname <> '' and c.designname is not null	" & vbcrlf & _
		"	and	c.price_co > 0" & vbcrlf & _
		"order by 6"

session("errorSQL") = SQL
set RS = oConn.execute( SQL)

'response.write "<pre>" & sql & "</pre>"

if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if

sql = "select count(*) as itemCnt from we_Items a left join we_ItemsExtendedData b on a.partNumber = b.partNumber where b.customize = 1 and a.modelID = " & modelid
session("errorSQL") = sql
set customRS = oConn.execute(sql)

dim showCustom : showCustom = 0
if customRS("itemCnt") > 0 then showCustom = 1

dim brandName, brandID, modelName, modelImg

brandName = RS("brandName")
brandID = RS("brandID")
modelName = RS("modelName")
modelImg = RS("modelImg")

'=========================================================================================
Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
	oParam.CompareMode = vbTextCompare
	oParam.Add "x_brandName", brandName
	oParam.Add "x_modelName", modelName

'call redirectURL("m", modelid, request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
'=========================================================================================		

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, h1, h2, altText

saveTopText = topText
if altText = "" then altText = brandName & " " & modelName

dim strBreadcrumb
strBreadcrumb = brandName & " " & modelName & " Cell Phone Accessories"

h1= brandName &" "& modelName &" Cell Phone Accessories"

dim strH1: strH1 = h1
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = altText

'set input values to extract page meta-values
dim dicReplaceAttribute: set dicReplaceAttribute = CreateObject("Scripting.Dictionary")
dicReplaceAttribute( "BrandName") = brandName
dicReplaceAttribute( "ModelName") = modelName

'set known product attributes to replace out template placeholders
dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dicSeoAttributeInput( "BrandId") = brandID
dicSeoAttributeInput( "ModelId") = modelID

'get content event text (initially installed to load pixels) [knguyen/20110520]
'todo: deploy to other wrappers as needed
dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)
'call LookupSeoAttributes()
%>
<!--#include virtual="/includes/template/top_index.asp"-->
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td class="top-sublink-gray">
												<a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
												<a class="top-sublink-gray" href="/b-<%=brandID & "-" & formatSEO(brandName)%>-cell-phone-accessories.html"><%=brandName%> Cell Phone Accessories</a>&nbsp;>&nbsp;
												<span class="top-sublink-blue"><%=brandName & " " & modelName%> Cell Phone Accessories</span>
											</td>
										</tr>
										<tr>
											<td class="top-sublink-gray"><img src="/images/spacer.gif" width="1" height="10"></td>
										</tr>
										<tr>
											<td align="center" width="100%">
												<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td align="center" width="200" valign="top"><div style="width:200px; text-align:center; height:120px;"><img src="/modelpics/<%=modelImg%>" align="center" border="0" height="106" alt="<%=strBreadcrumb%>"></div></td>
														<td align="center" class="static-content-font">
															<h1 class="nevi-font-bold"><%=seH1%></h1>
                                                            <div style="padding-top:5px;"><%=SeTopText%></div>
                                                            <%' XXX=brandName : YYY = modelName %>
                                                            <%'#include virtual="/Framework/seoContent.asp"-->%>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td valign="middle" width="100%" style="padding:5px 0px 5px 10px; background-color:#CCC; border-top:1px solid #000; border-bottom:1px solid #000;">
                                            	<div style="text-shadow: 1px 1px #fff; font-family: Arial; font-size: 18px; color: #000;"><%=strH1%></div>
                                                <!--
												<div style="width:100%; height:20px; position:relative;">
	                                                <div style="position:absolute; top:0px; left:0px; z-index:100; font-family: Arial; font-size: 18px; color: #000;"><%=strH1%></div>
                                                    <div style="position:absolute; top:1px; left:2px; z-index:50; font-family: Arial; font-size: 18px; color: #fff;"><%=strH1%></div>
                                                </div>
                                                -->
											</td>
										</tr>
										<tr>
											<td><img src="/images/spacer.gif" width="1" height="10" border="0"></td>
										</tr>
										<tr>
											<td align="left" valign="top" style="padding-bottom:30px;">
												<%
                                                if brandID <> 11 then
                                                    %>
                                                    <div style="float:left; width:160px; margin:20px 0px 0px 80px;">
                                                        <div style="height:100px; width:160px; overflow:hidden;"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-5-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-hands-free-kits-bluetooth-headsets.html"><img src="/images/categories/CO_cat_buttons_<%=formatSEO(replace("bluetooth & hands-free", "&", "and"))%>.jpg" border="0" alt="<%=altText%> Hands-Free Kits &amp; Bluetooth Headsets"></a></div>
                                                        <div style="width:160px; height:20px; padding-top:5px; color:#000; font-weight:bold; text-align:center; font-size:11px; background-color:#CCC;"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-5-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-hands-free-kits-bluetooth-headsets.html" style="color:#000; font-size:11px;">BLUETOOTH &amp; HANDS-FREE</a></div>                                                    
                                                    </div>
                                                    <%
                                                end if
                                                do until RS.eof
                                                    categoryID = RS("typeid")
                                                    if categoryID <> 5 then
                                                        textType = RS("typename")
                                                        picType = replace(textType, "&", "and")
                                                        textType = replace(textType,"Cell Phone ","")
                                                        linkType = replace(replace(textType,"&amp;","and"), "&", "and")
                                                        if categoryID = 19 then
                                                            strLink = "/sb-" & brandID & "-sm-" & modelID & "-sc-" & RS("typeid") & "-" & formatSEO(brandname) & "-" & formatSEO(modelName) & "-" & formatSEO(linkType) & "-select.html"
                                                        else
                                                            strLink = "/sb-" & brandID & "-sm-" & modelID & "-sc-" & RS("typeid") & "-" & formatSEO(brandname) & "-" & formatSEO(modelName) & "-" & formatSEO(linkType) & ".html"
                                                        end if
                                                        %>
                                                        <div style="float:left; width:160px; margin:20px 0px 0px 80px;" title="<%=altText & " " & textType%>">
                                                            <div style="height:100px; width:160px; overflow:hidden;"><a href="<%=strLink%>"><img src="/images/categories/CO_cat_buttons_<%=formatSEO(picType)%>.jpg" border="0" /></a></div>
                                                            <div style="width:160px; height:20px; padding-top:5px; color:#000; font-weight:bold; text-align:center; font-size:11px; background-color:#CCC;"><a href="<%=strLink%>" style="color:#000; font-size:11px;"><%=ucase(textType)%></a></div>
														</div>
                                                        <%
                                                    end if
                                                    RS.movenext
                                                loop
                                                %>
												<div style="float:left; width:160px; margin:20px 0px 0px 80px;">
                                                    <div style="height:100px; width:160px; overflow:hidden;"><a href="/sb-0-sm-0-sc-8-cell-phone-other-accessories.html"><img src="/images/categories/CO_cat_buttons_other-accessories.jpg" border="0" alt="<%=altText%> Other Accessories"></a></div>
                                                    <div style="width:160px; height:20px; padding-top:5px; color:#000; font-weight:bold; text-align:center; font-size:11px; background-color:#CCC;"><a href="/sb-0-sm-0-sc-8-cell-phone-other-accessories.html" style="color:#000; font-size:11px;">OTHER ACCESSORIES</a></div>
                                                </div>
                                                <%
                                                if showCustom = 1 then
                                                %>
												<div style="float:left; width:160px; margin:20px 0px 0px 80px;">
                                                    <div style="height:100px; width:160px; overflow:hidden;"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-24-cell-phone-custom-cases.html"><img src="/images/categories/CO_cat_buttons_customCases.jpg" border="0" alt="<%=altText%> Custom Cases"></a></div>
                                                    <div style="width:160px; height:20px; padding-top:5px; color:#000; font-weight:bold; text-align:center; font-size:11px; background-color:#CCC;"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-24-cell-phone-custom-cases.html" style="color:#000; font-size:11px;">CUSTOM CASES</a></div>
                                                </div>
                                                <%
                                                end if
                                                %>
											</td>
										</tr>
										<%if prepStr(SeBottomText) <> "" then%>
                                        <tr>
                                            <td align="center">
                                                <div style="padding:10px; font-size:11px; text-align:left;"><%=SeBottomText%></div>
                                            </td>
                                        </tr>
										<%end if%>
										<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
									</table>
<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->