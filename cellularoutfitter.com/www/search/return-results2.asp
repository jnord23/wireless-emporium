<?xml version="1.0" encoding="ISO-8859-1"?>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
xmlParsingMode = false
Response.Buffer = True
if xmlParsingMode then response.ContentType="text/xml"

dim curPageNum : curPageNum = prepInt(request.QueryString("page"))
dim keywords : keywords = prepStr(request.QueryString("keywords"))
dim from : from = request.QueryString("from")
dim FrmSearchWords : FrmSearchWords = request.QueryString("FrmSearchWords")
dim res_per_page : res_per_page = prepInt(request.QueryString("res_per_page"))
dim ipaddress : ipaddress = request.QueryString("ip")
dim user_agent : user_agent = request.QueryString("user_agent")
dim sqlWhereKeywords : sqlWhereKeywords = ""
dim sqlWhere : sqlWhere = ""
dim sort_by_field : sort_by_field = request.querystring("sort_by_field")
dim refine : refine = prepStr(request.QueryString("refine"))
dim brandName : brandName = prepStr(request.QueryString("Brand"))
dim modelName : modelName = prepStr(request.QueryString("Model"))
dim priceRange : priceRange = prepStr(request.QueryString("Price"))
dim color : color = prepStr(request.QueryString("Colors"))

if keywords = "" then keywords = prepStr(request.QueryString("search"))

if len(keywords) < 2 then response.end

set rsBlacklist = oConn.execute("select keywords from xsearchblacklist")
do until rsBlacklist.eof
	if instr(lcase(keywords), lcase(rsBlacklist("keywords"))) > 0 then response.end
	rsBlacklist.movenext
loop

sql = 	"insert into XSearchHistory(site_id, searchKeywords,userAgent,ipaddress)" & vbcrlf & _
		"values(2,'" & replace(request.QueryString, "'", "''") & "', '" & replace(user_agent, "'", "''") & "', '" & replace(ipaddress, "'", "''") & "')"
session("errorSQL") = sql
oConn.execute(sql)

sql = 	"select isnull(searchedKeyword, '') searchedKeyword, isnull(useKeyword, '') useKeyword, isnull(useTypeID, '') useTypeID, isnull(keywordExtend, '') keywordExtend " & vbcrlf & _
		"from 	XSearchKeywordMapping order by 1"
dim arrKeywordMapping : arrKeywordMapping = getDbRows(sql)

'======== need to be done by table later
'dim arrSoftMatch(12,1)
'arrSoftMatch(0,0) = "batteries" : arrSoftMatch(0,1) = "batter"
'arrSoftMatch(1,0) = "cases" : arrSoftMatch(1,1) = "case"
'arrSoftMatch(2,0) = "faceplates" : arrSoftMatch(2,1) = "faceplate"
'arrSoftMatch(3,0) = "memories" : arrSoftMatch(3,1) = "memor"
'arrSoftMatch(4,0) = "antennas" : arrSoftMatch(4,1) = "antenna"
'arrSoftMatch(5,0) = "phones" : arrSoftMatch(5,1) = "phone"
'arrSoftMatch(6,0) = "skins" : arrSoftMatch(6,1) = "skin"
'arrSoftMatch(7,0) = "cards" : arrSoftMatch(7,1) = "card"
'arrSoftMatch(8,0) = "tablets" : arrSoftMatch(8,1) = "tablet"
'arrSoftMatch(9,0) = "charms" : arrSoftMatch(9,1) = "charm"
'arrSoftMatch(10,0) = "protectors" : arrSoftMatch(10,1) = "protector"
'arrSoftMatch(11,0) = "chargers" : arrSoftMatch(11,1) = "charger"
'arrSoftMatch(12,0) = "covers" : arrSoftMatch(12,1) = "cover"

keywords = trim(replace(replace(lcase(keywords), "accessories", ""), "accessory", ""))
arrkeywords = split(keywords, " ")
sqlWhereKeywords = getSqlCondition(keywords)

totalPages = 0
totalProducts = 0
if sqlWhereKeywords <> "" then
	if refine <> "" then
		if brandName <> "" then sqlWhere = sqlWhere & "	and	c.brandName = '" & brandName & "'" & vbcrlf
		if modelName <> "" then sqlWhere = sqlWhere & "	and	d.modelName = '" & modelName & "'" & vbcrlf
		if priceRange <> "" then
			select case lcase(priceRange)
				case "less than $10"		:	sqlWhere = sqlWhere & "	and	a.price_co < 10.0" & vbcrlf
				case "$10 and $20"			:	sqlWhere = sqlWhere & "	and	a.price_co >= 10.0 and a.price_co < 20.0" & vbcrlf
				case "$20 and $30"			:	sqlWhere = sqlWhere & "	and	a.price_co >= 20.0 and a.price_co < 30.0" & vbcrlf
				case "$30 and $40"			:	sqlWhere = sqlWhere & "	and	a.price_co >= 30.0 and a.price_co < 40.0" & vbcrlf
				case "greater than $40"		:	sqlWhere = sqlWhere & "	and	a.price_co >= 40.0 " & vbcrlf
			end select
		end if
		if color <> "" then sqlWhere = sqlWhere & "	and	e.color = '" & color & "'" & vbcrlf		
	end if
	
	sqlSelect	= 	"select	a.itemid, a.itemdesc_co, a.itempic_co, a.itempic, a.price_retail, a.price_co, a.partnumber " & vbcrlf & _
					"	,	b.typeid, b.typeName, a.flag1, cast (( case when iv.OriginalPrice > a.price_CO then 1 else 0 end) as bit) as onSale " & vbcrlf & _
					"	,	isnull(c.brandid, 0) brandid, isnull(c.brandname, 'Universal') brandName" & vbcrlf & _
					"	,	isnull(d.modelid, 0) modelid, isnull(d.modelName, 'Universal') modelName" & vbcrlf & _
					"	,	convert(varchar(8000), isnull(dbo.fn_stripHTML(convert(varchar(8000), a.itemlongdetail_co)), '')) itemlongdetail, isnull(e.color, '') color" & vbcrlf
	sqlSelectCount	= 	"select	isnull(c.brandName, '') brandName, isnull(d.modelName, '') modelName" & vbcrlf & _
						"	, 	case when a.price_co < 10.0 then ' Less than $10'" & vbcrlf & _
						"			when a.price_co >= 10.0 and a.price_co < 20.0 then '$10 and $20'" & vbcrlf & _
						"			when a.price_co >= 20.0 and a.price_co < 30.0 then '$20 and $30'" & vbcrlf & _
						"			when a.price_co >= 30.0 and a.price_co < 40.0 then '$30 and $40'" & vbcrlf & _
						"			else 'Greater than $40'" & vbcrlf & _
						"		end pricing" & vbcrlf & _
						"	, 	isnull(e.color, '') color" & vbcrlf & _
						"	,	isnull(count(*), 0) totalProducts" & vbcrlf

	sqlFrom = 	"from	we_items a join we_types b" & vbcrlf & _
				"	on	a.typeid = b.typeid left outer join we_brands c" & vbcrlf & _
				"	on	a.brandid = c.brandid left outer join we_models d" & vbcrlf & _
				"	on	a.modelid = d.modelid left outer join xproductcolors e" & vbcrlf & _
				"	on	a.colorid = e.id left join ItemValue iv  " & vbcrlf &_
				"	on	iv.itemId = a.ItemId and iv.SiteId = 2  " & vbcrlf &_
				"where	(d.modelid is null or d.hidelive = 0) and a.hidelive = 0 and a.inv_qty <> 0 and a.price_co > 0" & vbcrlf & _
				"	and	(select top 1 itemID from we_items x where x.partnumber = a.partnumber and x.master = 1 and x.inv_qty > 0 ) is not null" & vbcrlf
				
	sql = 	sqlSelectCount & sqlFrom & sqlWhere & sqlWhereKeywords & _
			"group by isnull(c.brandName, ''), isnull(d.modelName, '')" & vbcrlf & _
			"	, 	case when a.price_co < 10.0 then ' Less than $10'" & vbcrlf & _
			"			when a.price_co >= 10.0 and a.price_co < 20.0 then '$10 and $20'" & vbcrlf & _
			"			when a.price_co >= 20.0 and a.price_co < 30.0 then '$20 and $30'" & vbcrlf & _
			"			when a.price_co >= 30.0 and a.price_co < 40.0 then '$30 and $40'" & vbcrlf & _
			"			else 'Greater than $40'" & vbcrlf & _
			"		end " & vbcrlf & _
			"	, 	isnull(e.color, '')" & vbcrlf & _
			"order by 1, 2, 3, 4"
			
	session("errorSQL") = sql
	
	sql = "exec sp_getGroupCount '" & replace(sql, "'", "''") & "'"
	
	if not xmlParsingMode then response.write "<pre>" & sql & "</pre>"	
	set rsCount = oConn.execute(sql)	
	
	if rsCount.state = 1 then totalProducts = rsCount("totalProducts")

	if not xmlParsingMode then response.write "totalProducts:" & totalProducts & "<br>"
	if totalProducts = 0 then response.end
	
	totalPages = cint(totalProducts / res_per_page)
	if ((totalProducts - (totalPages*res_per_page)) mod res_per_page) >= 1 then totalPages = totalPages + 1

	if curPageNum < 1 then curPageNum = 1
	if curPageNum > totalPages then curPageNum = 1
	
	startRowPos = ((curPageNum-1) * res_per_page) + 1
	endRowPos = (curPageNum * res_per_page + 1)
		
	select case sort_by_field
		case "Price:ASC" 	: sqlSelect = sqlSelect & "	,	row_number() over (order by a.price_co) rowindex" & vbcrlf
		case "Price:DESC"	: sqlSelect = sqlSelect & "	,	row_number() over (order by a.price_co desc) rowindex" & vbcrlf
		case else			: sqlSelect = sqlSelect & "	,	row_number() over (order by a.flag1 desc, a.itemid desc) rowindex" & vbcrlf
	end select
	sql = 	"select	*" & vbcrlf & _
			"from	(" & sqlSelect & sqlFrom & sqlWhere & sqlWhereKeywords & ") sub" & vbcrlf & _
			"where	sub.rowindex >= '" & startRowPos & "' and sub.rowindex < '" & endRowPos & "'"

	if not xmlParsingMode then response.write "<pre>" & sql & "</pre>"

	session("errorSQL") = sql
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sql, oConn, 0, 1

end if

if not xmlParsingMode then response.end

function getSqlCondition(pKeywords)
	ret = "(1=1)"
	if len(pKeywords) > 1 then
		arrayKeywords = split(pKeywords, " ")
		maxLength = ubound(arrayKeywords)
		for i=0 to maxLength
			ret = ret & getMatchQuery(arrayKeywords(i))
		next
	end if

	if ret = "(1=1)" then
		ret = "" 
	elseif ret <> "(1=1)" then
		ret = "	and	((" & ret & ") or ( a.itemdesc_co like '%" & pKeywords & "%'))" & vbcrlf
	end if	
	getSqlCondition = ret
end function

function getMatchQuery(pKeyword)
	ret = ""
	for i=0 to ubound(arrKeywordMapping,2)
		if lcase(arrKeywordMapping(0,i)) = lcase(pKeyword) then
			if arrKeywordMapping(2,i) <> "" then	'use typeid
				ret = " and (b.typeid in (" & arrKeywordMapping(2,i) & "))" & vbcrlf
			elseif arrKeywordMapping(3,i) <> "" then		'use extended keywords
				ret = " and (a.itemdesc_co like '%" & replace(pKeyword, "'", "''") & "%' or a.itemdesc_co like '%" & replace(arrKeywordMapping(3,i), "'", "''") & "%')" & vbcrlf
			end if
			exit for
		end if
	next
	if ret = "" then ret = " and (a.itemdesc_co like '%" & replace(pKeyword, "'", "''") & "%')" & vbcrlf
	getMatchQuery = ret
end function
%>
<xml>
    <suggested_spelling><![CDATA[]]></suggested_spelling>
    <custom_synonyms>
   		<synonym><![CDATA[<%=keywords%>]]></synonym>
		<%
        if isarray(arrkeywords) then
            for i=0 to ubound(arrkeywords)
                response.write "		<synonym><![CDATA[" & arrkeywords(i) & "]]></synonym>" & vbcrlf	
            next
        end if
        %>
    </custom_synonyms>
    <pagination>
        <total_products><%=totalProducts%></total_products>
        <product_min>1</product_min>
        <product_max><%=res_per_page%></product_max>
        <current_page><%=curPageNum%></current_page>
        <total_pages><%=totalPages%></total_pages>
        <prev_page><%=(curPageNum-1)%></prev_page>
        <next_page><%=(curPageNum+1)%></next_page>
    </pagination>
    <searched_in_field><![CDATA[]]></searched_in_field>
    <user_search_depth>
	<%
	dim refineRank : refineRank = -1
	if brandName <> "" then
		refineRank = refineRank + 1
	%>
		<item>
        	<rank><%=refineRank%></rank>
	        <key><![CDATA[Brand]]></key>
	        <value><![CDATA[<%=brandName%>]]></value>
        </item>
	<%
	end if
	if modelName <> "" then	
		refineRank = refineRank + 1
	%>
		<item>
        	<rank><%=refineRank%></rank>
	        <key><![CDATA[Model]]></key>
	        <value><![CDATA[<%=modelName%>]]></value>
        </item>
	<%
	end if
	if priceRange <> "" then	
		refineRank = refineRank + 1
	%>
		<item>
        	<rank><%=refineRank%></rank>
	        <key><![CDATA[Price]]></key>
	        <value><![CDATA[<%=priceRange%>]]></value>
        </item>
	<%
	end if
	if color <> "" then
		refineRank = refineRank + 1	
	%>
		<item>
        	<rank><%=refineRank%></rank>
	        <key><![CDATA[Colors]]></key>
	        <value><![CDATA[<%=color%>]]></value>
        </item>
	<%
	end if
	%>
    </user_search_depth>
    <currently_sorted_by><![CDATA[]]></currently_sorted_by>
    <sort_bys></sort_bys>
    <notices>
        <related_added><![CDATA[0]]></related_added>
        <sku_match><![CDATA[0]]></sku_match>
        <or_switch><![CDATA[0]]></or_switch>
    </notices>
    <merchandizing></merchandizing>
    <refinables>
	<%
	set rsCount = rsCount.nextrecordset
	if brandName = "" then
	%>
        <refinable>            
            <name><![CDATA[Brand]]></name>
            <values>
            	<%
				do until rsCount.eof
					if rsCount("brandName") <> "" then
					%>
					<value><name><![CDATA[<%=rsCount("brandName")%>]]></name><num><%=rsCount("totalProducts")%></num></value>
                    <%
					end if
					rsCount.movenext
				loop
				%>
            </values>
        </refinable>
	<%
	end if
	
	set rsCount = rsCount.nextrecordset	
	if modelName = "" then
	%>
        <refinable>            
            <name><![CDATA[Model]]></name>
            <values>
            	<%
				do until rsCount.eof
					if rsCount("modelName") <> "" then
					%>
					<value><name><![CDATA[<%=rsCount("modelName")%>]]></name><num><%=rsCount("totalProducts")%></num></value>
                    <%
					end if
					rsCount.movenext
				loop
				%>
            </values>
        </refinable>
	<%
	end if
	
	set rsCount = rsCount.nextrecordset	
	if priceRange = "" then
	%>
        <refinable>            
            <name><![CDATA[Price]]></name>
            <values>
            	<%
				do until rsCount.eof
					if rsCount("priceName") <> "" then
					%>
					<value><name><![CDATA[<%=rsCount("priceName")%>]]></name><num><%=rsCount("totalProducts")%></num></value>
                    <%
					end if
					rsCount.movenext
				loop
				%>
            </values>
        </refinable>
	<%
	end if
	
	set rsCount = rsCount.nextrecordset	
	if color = "" then
	%>
        <refinable>            
            <name><![CDATA[Colors]]></name>
            <values>
            	<%
				do until rsCount.eof
					if rsCount("colorName") <> "" then
					%>
					<value><name><![CDATA[<%=rsCount("colorName")%>]]></name><num><%=rsCount("totalProducts")%></num></value>
                    <%
					end if
					rsCount.movenext
				loop
				%>
            </values>
        </refinable>    
    <%
	end if
	%>    
	</refinables>
    <results>
	<%
	if totalProducts > 0 then
		set fsThumb = CreateObject("Scripting.FileSystemObject")
		do until rs.eof
			itemid = rs("itemid")
			itemdesc = rs("itemdesc_co")
			itempic_co = rs("itempic_co")
			itempic = rs("itempic")
			price_retail = prepInt(rs("price_retail"))
			price_co = prepInt(rs("price_co"))
			partnumber = rs("partnumber")
			skipProd = 0
			if instr(partnumber,"DEC-SKN") > 0 then
				useImg = "/weImages/big/" & itempic
			elseif not fsThumb.FileExists(Server.MapPath("/productPics/thumb/"&itempic_co)) then 
				useImg = "/productpics/thumb/imagena.jpg"
				skipProd = 1
			else
				useImg = "/productpics/thumb/" & itempic_co
			end if
			typeid = rs("typeid")
			categoryName = rs("typeName")
			brandid = rs("brandid")
			brandName = rs("brandName")
			modelid = rs("modelid")
			modelName = rs("modelName")
			itemlongDetail = rs("itemlongdetail")
			color = rs("color")
			onSale = rs("onSale")
			
			if skipProd = 0 then
			%>
			<result>
				<rank><%=lap%></rank>
				<id><![CDATA[<%=itemid%>]]></id>
				<Groupname><![CDATA[<%=itemdesc%>]]></Groupname>
				<Description><![CDATA[<%=itemlongDetail%>>]]></Description>
				<Sku><![CDATA[<%=itemid%>]]></Sku>
				<Name><![CDATA[<%=itemdesc%>]]></Name>
				<Lineitemprice><![CDATA[<%=price_retail%>]]></Lineitemprice>
				<Image><![CDATA[<%=useImg%>]]></Image>
				<Firstleveldepartment><![CDATA[<%=categoryName%>]]></Firstleveldepartment>
				<Secondleveldepartment><![CDATA[<%=brandName%>]]></Secondleveldepartment>
				<Thirdleveldepartment><![CDATA[<%=brandName & " > " & modelName%>]]></Thirdleveldepartment>
				<Keywords><![CDATA[<%=categoryName%>,<%=brandName%>,<%=modelName%>,<%=itemdesc%>]]></Keywords>
				<Price><![CDATA[<%=price_co%>]]></Price>
				<Manfacturername><![CDATA[<%=brandName%>]]></Manfacturername>
				<Code><![CDATA[<%=partnumber%>]]></Code>
				<Brand><![CDATA[<%=brandName%>]]></Brand>
				<Urltoproductpage><![CDATA[/p-<%=itemid%>-<%=formatSEO(itemdesc)%>.html]]></Urltoproductpage>
				<Colors><![CDATA[<%=color%>]]></Colors>
				<onSale><![CDATA[<%=onSale%>]]></onSale>
				<Model><![CDATA[<%=modelName%>]]></Model>
			</result>
			<%
			end if
			rs.movenext	
		loop
	end if
	%>
	</results>
	<xml_feed_done>1</xml_feed_done>
</xml>