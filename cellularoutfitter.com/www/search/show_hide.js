function showhide(div_name) {
	if (document.getElementById(div_name + "_link").innerHTML == "more")	{
		document.getElementById(div_name + "_div").style.display = "block";
		document.getElementById(div_name + "_link").innerHTML = "hide";
	}
	else {
		document.getElementById(div_name + "_div").style.display = "none";
		document.getElementById(div_name + "_link").innerHTML = "more";
	}
}