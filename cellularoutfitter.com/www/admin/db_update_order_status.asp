<%
pageTitle = "Admin - Update CO Database - Update Order Status"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->

<blockquote>
<blockquote>

<h3>Update CO Database - Update Order Status</h3>

<%
function showNothing(strValue)
	if isNull(strValue) then
		showNothing = "-null-"
	elseif strValue = "" then
		showNothing = "&nbsp;"
	else
		showNothing = strValue
	end if
end function

a = 0
strError = ""
if request.form("submitted2") = "Update" then
	for a = 1 to request.form("totalCount")
		if request.form("tracking" & a) = "-null-" then
			tracking = "null"
		else
			tracking = "'" & SQLquote(request.form("tracking" & a)) & "'"
		end if
		if request.form("thub_posted_to_accounting" & a) = "'W'" then
			thub_posted_date = "'" & now & "'"
		else
			thub_posted_date = "null"
		end if
		SQL = "UPDATE we_Orders SET"
		SQL = SQL & " approved=" & request.form("approved" & a) & ","
		SQL = SQL & " cancelled=" & request.form("cancelled" & a) & ","
		SQL = SQL & " trackingNum=" & tracking & ","
		SQL = SQL & " thub_posted_to_accounting=" & request.form("thub_posted_to_accounting" & a)
'		SQL = SQL & " thub_posted_to_accounting=" & request.form("thub_posted_to_accounting" & a) & ","
'		SQL = SQL & " thub_posted_date=" & thub_posted_date
		SQL = SQL & " WHERE OrderID='" & request.form("OrderID" & a) & "'"
		session("errorSQL") = SQL
		oConn.execute SQL
	next
	%>
	<h3>UPDATED!</h3>
	<%
end if

if request.form("submitted") <> "" or request.form("submitted2") <> "" then
	dim whereSQL
	whereSQL = ""
	if request.form("orderStart") <> "" and isNumeric(request.form("orderStart")) then
		strOrderStartNumber = int(request.form("orderStart"))
		if request.form("orderEnd") <> "" and isNumeric(request.form("orderEnd")) then
			strOrderEndNumber = int(request.form("orderEnd"))
			if strOrderStartNumber > strOrderEndNumber then
				strError = "Start Order # must be lower than End Order #"
			elseif cDbl(strOrderEndNumber) - cDbl(strOrderStartNumber) > 500 then
				strError = "End Order # cannot be more than 500 greater than Start Order #"
			else
				whereSQL = " WHERE A.OrderID >= '" & request.form("orderStart") & "' AND A.OrderID <= '" & request.form("orderEnd") & "'"
			end if
		else
			strError = "You must enter a valid End Order #"
		end if
	elseif request.form("ProcessDate") <> "" then
		if not isDate(request.form("ProcessDate")) then
			strError = "Process Date must be a valid date"
		else
			whereSQL = " WHERE A.thub_posted_date >= '" & dateValue(request.form("ProcessDate")) & "' AND A.thub_posted_date < '" & dateAdd("d",1,dateValue(request.form("ProcessDate"))) & "'"
		end if
	else
		strError = "You must enter a valid Start Order #"
	end if
	
	if strError = "" then
		SQL = "SELECT A.*, B.* FROM we_Orders A INNER JOIN CO_Accounts B ON A.AccountID = B.AccountID"
		SQL = SQL & whereSQL
		SQL = SQL & " AND A.store = 2"
		SQL = SQL & " ORDER BY A.OrderID"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		a = 0
		if RS.eof then
			response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
			response.write "<a href=""db_update_order_status.asp"">Try again.</a>"
		else
			%>
			<p><a href="db_update_order_status.asp">Search For Another Order # Range</a></p>
			<table border="1" cellpadding="0" cellspacing="0" width="90%" align="center">
				<form name="frmUpdateOrders" action="db_update_order_status.asp" method="post">
					<tr>
						<td width="6%" align="center" valign="top"><b>OrderID</b></td>
						<td width="8%" align="center" valign="top"><b>Order&nbsp;Date</b></td>
						<td width="4%" align="center" valign="top"><b>Name</b></td>
						<td width="10%" align="center" valign="top"><b>Shipping&nbsp;Type</b></td>
						<td width="11%" align="center" valign="top"><b>Tracking #</b></td>
						<td width="10%" align="center" valign="top"><b>Scan&nbsp;Date</b></td>
						<td width="10%" align="center" valign="top"><b>Approved</b></td>
						<td width="10%" align="center" valign="top"><b>Cancelled</b></td>
						<td width="10%" align="center" valign="top"> <strong>Fraud Canceled </strong></td>
						<td width="10%" align="center" valign="top"><b>Processed</b></td>
						<td width="14%" align="center" valign="top"><b>Processed&nbsp;Date</b></td>
					</tr>
					<%
					do until RS.eof
						OrderID = RS("OrderID")
						approved = RS("approved")
						scanDate = RS("scanDate")
						cancelled = RS("cancelled")
						thub_posted_to_accounting = RS("thub_posted_to_accounting")
						thub_posted_date = RS("thub_posted_date")
						a = a + 1
						%>
						<tr>
							<td valign="top" align="center"><input type="hidden" name="OrderID<%=a%>" value="<%=OrderID%>"><%=showNothing(OrderID)%></td>
							<td valign="top" align="center"><%=dateValue(RS("OrderDateTime"))%></td>
							<td valign="top" align="center"><%=showNothing(RS("fName")) & "&nbsp;" & showNothing(RS("lName"))%></td>
							<td valign="top" align="center"><%=showNothing(RS("shiptype"))%></td>
							<td valign="top" align="center"><input name="tracking<%=a%>" type="text" id="tracking<%=a%>" value="<%=showNothing(RS("trackingNum"))%>" size="20" maxlength="50" /></td>
							<td valign="top" align="center"><%=showNothing(RS("scanDate"))%></td>
							<td valign="top" align="center">
								<input type="radio" name="approved<%=a%>" value="-1"<%if approved = true then response.write " checked"%>>Yes&nbsp;
								<input type="radio" name="approved<%=a%>" value="null"<%if approved = false or isNull(approved) then response.write " checked"%>>No
							</td>
							<td valign="top" align="center">
								<input type="radio" name="cancelled<%=a%>" value="-1"<%if cancelled = true then response.write " checked"%>>Yes&nbsp;
								<input type="radio" name="cancelled<%=a%>" value="null"<%if cancelled = false or isNull(cancelled) then response.write " checked"%>>No
							</td>
							<td valign="top" align="center">
								<%
								if RS("refundreason") = 99 then
									response.write "Fraud cancelled"
								elseif RS("refundreason") <= 8 and RS("refundreason") >= 1 then
									response.write "RMA"
								else
									response.write RS("refundreason")
									%>
									<a href="db_update_order_status_fraud.asp?orderid=<%=OrderID%>&groundtotal=<%=rs("ordergrandtotal")%>" target="_top">Fraud</a>
									<%
								end if
								%>
							</td>
							<td valign="top" align="center">
								<input type="radio" name="thub_posted_to_accounting<%=a%>" value="'W'"<%if not isNull(thub_posted_to_accounting) then response.write " checked"%>>Yes&nbsp;
								<input type="radio" name="thub_posted_to_accounting<%=a%>" value="null"<%if isNull(thub_posted_to_accounting) then response.write " checked"%>>No
							</td>
							<td valign="top" align="center"><%=showNothing(thub_posted_date)%></td>
						</tr>
						<%
						RS.movenext
					loop
					%>
					<tr>
						<td align="left" colspan="3">
							<input type="checkbox" name="NotProcessed" value="1" onClick="MarkAllAsNotProcessed();">&nbsp;Mark All As NOT Processed
						</td>
						<td align="left" colspan="8">
							<input type="hidden" name="totalCount" value="<%=a%>">
							<input type="hidden" name="orderStart" value="<%=strOrderStartNumber%>">
							<input type="hidden" name="orderEnd" value="<%=strOrderEndNumber%>">
							<input type="submit" name="submitted2" value="Update">
						</td>
					</tr>
				</form>
			</table>
			<p>&nbsp;</p>
			<%
		end if
	end if
else
	%>
	<form name="frmSearchOrders" action="db_update_order_status.asp" method="post">
		<p>Start Order #:&nbsp;&nbsp;<input type="text" name="orderStart"></p>
		<p>End Order #:&nbsp;&nbsp;<input type="text" name="orderEnd"></p>
		<p>- OR - </p>
		<p>Process Date:&nbsp;&nbsp;<input type="text" name="ProcessDate"></p>
		<p><input type="submit" name="submitted" value="Search"></p>
	</form>
	<%
end if
%>

</blockquote>
</blockquote>

<script language="javascript">
function MarkAllAsNotProcessed() {
	for (var i=1; i<<%=a+1%>; ++i) {
		p0 = eval('document.frmUpdateOrders.thub_posted_to_accounting' + i + '[0]');
		p0.checked = false;
		p1 = eval('document.frmUpdateOrders.thub_posted_to_accounting' + i + '[1]');
		p1.checked = true;
	}
}
</script>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
