<%
set Pdf = Server.CreateObject("Persits.Pdf")
set Doc = Pdf.CreateDocument
set Param = Pdf.CreateParam

thisPage = 1
strCheck = ""

do while not RS2.eof
	if inStr(strCheck,RS2("orderID") & ",") = 0 then
		strCheck = strCheck & RS2("orderID") & ","
		Set Page = Doc.Pages.Add
		Set Font = Doc.Fonts("Helvetica")
		' determine if this is a multiple-item order
		sSqlDetails = "SELECT SUM(quantity) AS totalQty FROM we_orderdetails WHERE orderid = '" & RS2("orderID") & "'"
		Set oRsOrderDetails = Server.CreateObject("ADODB.Recordset")
		oRsOrderDetails.open sSqlDetails, oConn, 3, 3
		if not oRsOrderDetails.EOF then
			if oRsOrderDetails("totalQty") > 1 then
				multiItem = "Y"
			else
				multiItem = "N"
			end if
		else
			multiItem = "N"
		end if
		thisPage = thisPage + 1
		
		SQL = "SELECT A.*,B.*,C.* FROM (we_Orders A INNER JOIN CO_Accounts B ON A.accountID=B.accountID)"
		SQL = SQL & " INNER JOIN we_orderdetails C ON A.orderID=C.orderID"
		SQL = SQL & " WHERE A.orderID = '" & RS2("orderID") & "'"
		Set RSmc = Server.CreateObject("ADODB.Recordset")
		RSmc.open SQL, oConn, 3, 3
		
		' add logo image
		set Image = Doc.OpenImage(Server.MapPath("/images/cart/CO_SalesOrder_logo.jpg"))
		Param("x") = 41
		Param("y") = 725
		Param("ScaleX") = .4
		Param("ScaleY") = .4
		Page.Canvas.DrawImage Image, Param
		
		' add address
		strText = "<p>1410 N. Batavia St.<br>Orange, CA 92867</p>"
		Param("x") = 45
		Param("y") = 725
		Param("size") = 9
		Param("HTML") = true
		Page.Canvas.DrawText strText, Param, Font
		
		' add "Multi-Item Order" text, if applicable
		if multiItem = "Y" then
			strText = "<p><font color=""#FF0000"">Multi-Item Order</font></p>"
			Param("x") = 315
			Param("y") = 761
			Param("size") = 12
			Param("HTML") = true
			Page.Canvas.DrawText strText, Param, Font
		end if
		
		Param("size") = 9
		Param("color") = "&H000000"
		
		' add header
		set Table = Doc.CreateTable("rows=3; cols=2; width=150; height=1; CellBorder=.1; border=0")
		Table.Font = Doc.Fonts("Helvetica")
		Table(1, 1).ColSpan = 2
		Table(1, 1).AddText "Sales Receipt", "size=12; color=&H000000; expand=true; alignment=center", Font
		Table(2, 1).AddText "Date", "size=11; expand=true; alignment=center", Font
		Table(2, 2).AddText "Order #", "size=11; expand=true; alignment=center", Param
		Table(3, 1).AddText dateValue(RSmc("OrderDateTime")), "size=14; expand=true; alignment=center", Font
		Table(3, 2).AddText RSmc("OrderID"), "size=14; expand=true; alignment=center", Font
		Page.Canvas.DrawTable Table, "x=433, y=760"
		
		' add barcode
		strParam = "x=290; y=716; height=25; width=125; DrawText=false; type=17"
		strData = RSmc("OrderID")
		Page.Canvas.DrawBarcode strData, strParam
		
		' add customer billing address
		if not isNull(RSmc("bAddress2")) and RSmc("bAddress2") <> "" then
			rows = 7
		else
			rows = 6
		end if
		set Table = Doc.CreateTable("rows=" & rows & "; cols=1; width=250; height=1; CellBorder=0; border=1")
		Table.Font = Doc.Fonts("Helvetica")
		Table(1, 1).AddText "Billing Address", "size=10; expand=true; CellBorder=1; alignment=center", Font
		Table(2, 1).AddText switchChars(" " & RSmc("fName") & " " & RSmc("lName")), "size=9; expand=true; alignment=left", Font
		Table(3, 1).AddText switchChars(" " & RSmc("bAddress1")), "size=9; expand=true; alignment=left", Font
		if rows = 7 then Table(4, 1).AddText switchChars(" " & RSmc("bAddress2")), "size=9; expand=true; alignment=left", Font
		Table(rows - 2, 1).AddText switchChars(" " & RSmc("bCity") & ", " & RSmc("bState") & "  " & RSmc("bZip")), "size=9; expand=true; alignment=left", Font
		Table(rows - 1, 1).AddText switchChars(" " & RSmc("phone")), "size=8; expand=true; alignment=left", Font
		Table(rows, 1).AddText switchChars(" " & RSmc("email")), "size=8; expand=true; alignment=left", Font
		Page.Canvas.DrawTable Table, "x=42, y=690"
		
		with Page.Canvas
			.MoveTo 43, 676
			.LineTo 291, 676
			.ClosePath
			.Stroke
		end with
		
		with Page.Canvas
			.MoveTo 334, 676
			.LineTo 582, 676
			.ClosePath
			.Stroke
		end with
		
		' add customer shipping address
		dim sAddress1, sAddress2, sCity, sState, sZip
		sAddress1 = ""
		sAddress2 = ""
		sCity = ""
		sState = ""
		sZip = ""
		if RSmc("shippingID") > 0 then
			SQL = "SELECT * FROM we_addl_shipping_addr WHERE id='" & RSmc("shippingID") & "'"
			Set RSmc2 = Server.CreateObject("ADODB.Recordset")
			RSmc2.open SQL, oConn, 3, 3
			if not RSmc.eof then
				sAddress1 = RSmc2("sAddress1")
				sAddress2 = RSmc2("sAddress2")
				sCity = RSmc2("sCity")
				sState = RSmc2("sState")
				sZip = RSmc2("sZip")
			end if
		end if
		if sAddress1 = "" then
			sAddress1 = RSmc("sAddress1")
			sAddress2 = RSmc("sAddress2")
			sCity = RSmc("sCity")
			sState = RSmc("sState")
			sZip = RSmc("sZip")
		end if
		if not isNull(sAddress2) and sAddress2 <> "" then
			rows = 5
		else
			rows = 4
		end if
		Set Table = Doc.CreateTable("rows=" & rows & "; cols=1; width=250; height=1; CellBorder=0; border=1")
		Table.Font = Doc.Fonts("Helvetica")
		Table(1, 1).AddText "Shipping Address", "size=10; expand=true; CellBorder=1; alignment=center", Font
		Table(2, 1).AddText switchChars(" " & RSmc("fName") & " " & RSmc("lName")), "size=9; expand=true; alignment=left", Font
		Table(3, 1).AddText switchChars(" " & sAddress1), "size=9; expand=true; alignment=left", Font
		if rows = 5 then
			Table(4, 1).AddText switchChars(" " & sAddress2), "size=9; expand=true; alignment=left", Font
		end if
		Table(rows, 1).AddText switchChars(" " & sCity & ", " & sState & "  " & sZip), "size=9; expand=true; alignment=left", Font
		Page.Canvas.DrawTable Table, "x=333, y=690"
		
		' add order details
		if not isNull(RSmc("BuySafeAmount")) then
			nBuysafeamount = RSmc("BuySafeAmount")
		else
			nBuysafeamount = 0
		end if
		nOrderSubTotal = RSmc("ordersubtotal")
		nShipFee = RSmc("ordershippingfee")
		nOrderGrandTotal = RSmc("ordergrandtotal")
		nTaxAmount = 0
		nCompareSubTotal = cDbl(nOrderSubTotal) + cDbl(nShipFee) + nBuysafeamount
		if nCompareSubTotal <> cDbl(nOrderGrandTotal) then nTaxAmount = cDbl(nOrderGrandTotal) - nCompareSubTotal
		
		sSqlDetails = "SELECT A.itemID, A.PartNumber, A.typeID, A.vendor, A.itemdesc_CO, A.price_CO, A.ItemKit_NEW, B.quantity FROM we_items A"
		sSqlDetails = sSqlDetails & " INNER JOIN we_orderdetails B ON A.itemid = B.itemid"
		sSqlDetails = sSqlDetails & " WHERE B.orderid = '" & RS2("orderID") & "'"
		Set oRsOrderDetails = Server.CreateObject("ADODB.Recordset")
		oRsOrderDetails.open sSqlDetails, oConn, 3, 3
		if not oRsOrderDetails.EOF then
			rows = oRsOrderDetails.recordcount + 12
			if nBuysafeamount > 0 then rows = rows + 1
			set Table = Doc.CreateTable("rows=" & rows & "; cols=5; width=541; height=1; CellBorder=.1; border=0; CellSpacing=0; CellPadding=2;")
			Table.Font = Doc.Fonts("Helvetica")
			with Table.Rows(1)
				.Cells(1).Width = 120
				.Cells(2).Width = 251
				.Cells(3).Width = 30
				.Cells(4).Width = 70
				.Cells(5).Width = 70
			end with
			Table(1, 1).ColSpan = 3
			Table(1, 1).RowSpan = 2
			strText = "Questions regarding your order? Most answers can be found at our self-service Frequently-Asked Questions page. "
			strText = strText & "Simply visit us at: www.CellularOutfitter.com/faq.html. Or click on the ""FAQ"" link on our site."
			Table(1, 1).AddText strText, "size=8; expand=false; alignment=center", Font
			Table(1, 4).AddText "Paid By", "size=9; expand=true; alignment=center", Font
			Table(1, 5).AddText "Ship Via", "size=9; expand=true; alignment=center", Font
			select case RSmc("extOrderType")
				case 1 : strText = "PayPal"
				case 2 : strText = "Google Chekcout"
				case 3 : strText = "eBillme"
				case else : strText = "Credit Card"
			end select
			Table(2, 4).AddText strText, "size=9; expand=true; alignment=center", Font
			Table(2, 5).AddText RSmc("shiptype"), "size=9; expand=true; alignment=center", Font
			Table(3, 1).AddText "Item #", "size=9; expand=true; alignment=center", Font
			Table(3, 2).AddText "Description", "size=9; expand=true; alignment=center", Font
			Table(3, 3).AddText "Qty.", "size=9; expand=true; alignment=center", Font
			Table(3, 4).AddText "Rate", "size=9; expand=true; alignment=center", Font
			Table(3, 5).AddText "Amount", "size=9; expand=true; alignment=center", Font
			b = 3
			mySubTotal = 0
			do until oRsOrderDetails.eof
				b = b + 1
				nID = oRsOrderDetails("itemid")
				if not isNull(oRsOrderDetails("ItemKit_NEW")) then
					nPartNumber = ""
					SQL = "SELECT PartNumber FROM we_items WHERE itemID IN (" & oRsOrderDetails("ItemKit_NEW") & ")"
					set RSparts = Server.CreateObject("ADODB.Recordset")
					RSparts.open SQL, oConn, 3, 3
					do until RSparts.eof
						nPartNumber = nPartNumber & RSparts("PartNumber") & ", "
						RSparts.movenext
					loop
					RSparts.close
					set RSparts = nothing
					nPartNumber = left(nPartNumber,len(nPartNumber)-2)
				else
					nPartNumber = oRsOrderDetails("PartNumber")
				end if
				nQty = oRsOrderDetails("quantity")
				nPrice = oRsOrderDetails("price_CO")
				myPrice = cDbl(nQty) * cDbl(nPrice)
				mySubTotal = mySubTotal + myPrice
				Table(b, 1).AddText nPartNumber, "size=12; expand=true; alignment=center", Font
				Table(b, 2).AddText switchChars(oRsOrderDetails("itemdesc_CO")), "size=9; expand=true; alignment=left", Font
				if oRsOrderDetails("vendor") = "DS" then
					Table(b, 2).AddText vbcrlf & vbcrlf & "(This item will ship separately from the manufacturer.)", "size=7; expand=true; alignment=left", Font
				end if
				Table(b, 3).AddText nQty, "size=12; expand=true; alignment=center", Font
				Table(b, 4).AddText formatCurrency(nPrice), "size=9; expand=true; alignment=right", Font
				Table(b, 5).AddText formatCurrency(myPrice), "size=9; expand=true; alignment=right", Font
				oRsOrderDetails.movenext
			loop
			
			b = b + 1
			Table(b, 1).ColSpan = 5
			Table(b, 1).AddText " ", "size=12; expand=true; alignment=center", Font
			
			nDiscount = abs(cDbl(mySubTotal) - cDbl(nOrderSubTotal))
			if nDiscount > 0 then nDiscount = nDiscount * -1
			
			RowSpan = 7
			if nBuysafeamount > 0 then RowSpan = RowSpan + 1
			b = b + 1
			Table(b, 1).ColSpan = 2
			Table(b, 1).RowSpan = RowSpan
			Table(b, 1).Height = 130
			set Image = Doc.OpenImage(Server.MapPath("/images/cart/co_policy3.gif"))
			Param("x") = 12
			Param("y") = -2
			Param("ScaleX") = .5
			Param("ScaleY") = .5
			Table(b, 1).Canvas.DrawImage Image, Param
			
			Table(b, 3).ColSpan = 2
			Table(b, 3).Height = 17
			Table(b, 3).AddText "DISCOUNT:", "size=9; expand=true; alignment=left", Font
			Table(b, 5).AddText formatCurrency(nDiscount), "size=9; expand=true; alignment=right", Font
			b = b + 1
			if nBuysafeamount > 0 then
				Table(b, 3).ColSpan = 2
				Table(b, 3).AddText "buySAFE Bond:", "size=9; expand=true; alignment=left", Font
				Table(b, 5).AddText formatCurrency(nBuysafeamount), "size=9; expand=true; alignment=right", Font
				b = b + 1
			end if
			Table(b, 3).ColSpan = 2
			Table(b, 3).AddText "SUBTOTAL:", "size=9; expand=true; alignment=left", Font
			Table(b, 5).AddText formatCurrency(mySubTotal), "size=9; expand=true; alignment=right", Font
			b = b + 1
			Table(b, 3).ColSpan = 2
			Table(b, 3).AddText "7.75% SALES TAX" & vbcrlf & "(CA ONLY):", "size=9; expand=true; alignment=left", Font
			Table(b, 5).AddText formatCurrency(nTaxAmount), "size=9; expand=true; alignment=right", Font
			b = b + 1
			Table(b, 3).ColSpan = 2
			Table(b, 3).AddText "SHIPPING & HANDLING:", "size=9; expand=true; alignment=left", Font
			Table(b, 5).AddText formatCurrency(nShipFee), "size=9; expand=true; alignment=right", Font
			b = b + 1
			Table(b, 3).ColSpan = 2
			Table(b, 3).AddText "GRAND TOTAL:", "size=9; expand=true; alignment=left", Font
			Table(b, 5).AddText formatCurrency(nOrderGrandTotal), "size=9; expand=true; alignment=right", Font
			b = b + 1
			Table(b, 3).ColSpan = 2
			Table(b, 3).AddText "TOTAL PAID:", "size=9; expand=true; alignment=left", Font
			Table(b, 5).AddText formatCurrency(nOrderGrandTotal * -1), "size=9; expand=true; alignment=right", Font
			b = b + 1
			Table(b, 3).ColSpan = 2
			Table(b, 3).AddText "TOTAL DUE:", "size=9; expand=true; alignment=left", Font
			Table(b, 5).AddText "$0.00", "size=9; expand=true; alignment=right", Font
			
			b = b + 1
			'set Image = Doc.OpenImage(Server.MapPath("/images/newimages/PDFcoupon3.jpg"))
			'Param("x") = 325
			'Param("y") = 5
			'Param("ScaleX") = .5
			'Param("ScaleY") = .5
			'Table(b, 1).ColSpan = 5
			'Table(b, 1).Height = 70
			'Table(b, 1).Canvas.DrawImage Image, Param
			
			Page.Canvas.DrawTable Table, "x=42, y=613"
		end if
		oRsOrderDetails.close
		set oRsOrderDetails = nothing
	end if
	RS2.movenext
loop

path = server.mappath("tempPDF") & "\" & filenamePDF
Doc.Save path
%>
