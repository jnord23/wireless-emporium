set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path
filename = "productList_pepperjam_CO.txt"
'path = "E:\Inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename
path = server.mappath("\tempCSV\" & filename)

set fs = CreateObject("Scripting.FileSystemObject")
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)
dim SQL, RS
SQL = "SELECT A.itemID,A.PartNumber AS MPN,A.brandID,A.itemDesc_CO,A.itempic_CO,A.price_Retail,A.price_CO,A.Sports,A.flag1,A.UPCCode,"
SQL = SQL & "A.itemLongDetail_CO,A.POINT1,A.POINT2,A.POINT3,A.POINT4,A.POINT5,A.POINT6,A.COMPATIBILITY,A.POINT7,A.inv_qty,"
SQL = SQL & "B.brandName,C.modelName,C.[temp],D.typeName"
SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0"
SQL = SQL & " AND A.price_CO > 0"
SQL = SQL & " AND A.itemLongDetail_CO IS NOT NULL AND A.itemLongDetail_CO <> ''"
SQL = SQL & " AND A.typeid <> 16"
SQL = SQL & " ORDER BY A.itemID"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if not RS.eof then
	dim GRPNAME, id_new_field, stypeName, ModelName, BrandName, ShortDescription, LongDescription, COMPATIBILITY
	file.writeLine "PJN_PRODUCT_FEED_BASIC"
	do until RS.eof
		DoNotInclude = 0
		if RS("inv_qty") < 0 then
			SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
			set RS2 = CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if RS2.eof then DoNotInclude = 1
			RS2.close
			set RS2 = nothing
		end if
		if DoNotInclude = 0 then
			GRPNAME = RS("itemDesc_CO")
			id_new_field = formatSEO(GRPNAME)
			
			if isnull(RS("typeName")) then
				stypeName = "Universal"
			else 
				stypeName = RS("typeName")
			end if
			if isnull(RS("ModelName")) then
				ModelName = "Universal"
			else
				ModelName = RS("BrandName") & " > " & RS("ModelName")
			end if
			if isnull(RS("BrandName")) then
				BrandName = "Universal"
			else 
				BrandName = RS("BrandName")
			end if
			
			ShortDescription = RS("itemLongDetail_CO")
			if not isNull(ShortDescription) then ShortDescription = replace(ShortDescription,vbCrLf," ")
			
			LongDescription = RS("POINT1")
			if not isNull(RS("POINT1")) and RS("POINT1") <> "" then
				if right(RS("POINT1"),1) <> "." and right(RS("POINT1"),1) <> "!" then LongDescription = LongDescription & "."
				LongDescription = LongDescription & " "
			end if
			if not isNull(RS("POINT2")) and RS("POINT2") <> "" then
				LongDescription = LongDescription & RS("POINT2")
				if right(RS("POINT2"),1) <> "." and right(RS("POINT2"),1) <> "!" then LongDescription = LongDescription & "."
				LongDescription = LongDescription & " "
			end if
			if not isNull(RS("POINT3")) and RS("POINT3") <> "" then
				LongDescription = LongDescription & RS("POINT3")
				if right(RS("POINT3"),1) <> "." and right(RS("POINT3"),1) <> "!" then LongDescription = LongDescription & "."
				LongDescription = LongDescription & " "
			end if
			if not isNull(RS("POINT4")) and RS("POINT4") <> "" then
				LongDescription = LongDescription & RS("POINT4")
				if right(RS("POINT4"),1) <> "." and right(RS("POINT4"),1) <> "!" then LongDescription = LongDescription & "."
				LongDescription = LongDescription & " "
			end if
			if not isNull(RS("POINT5")) and RS("POINT5") <> "" then
				LongDescription = LongDescription & RS("POINT5")
				if right(RS("POINT5"),1) <> "." and right(RS("POINT5"),1) <> "!" then LongDescription = LongDescription & "."
				LongDescription = LongDescription & " "
			end if
			if not isNull(RS("POINT6")) and RS("POINT6") <> "" then
				LongDescription = LongDescription & RS("POINT6")
				if right(RS("POINT6"),1) <> "." and right(RS("POINT6"),1) <> "!" then LongDescription = LongDescription & "."
				LongDescription = LongDescription & " "
			end if
			if not isNull(RS("COMPATIBILITY")) and RS("COMPATIBILITY") <> "" then
				COMPATIBILITY = replace(RS("COMPATIBILITY"),vbcrlf,"<br>")
				LongDescription = LongDescription & "Compatible with: " & COMPATIBILITY
				if right(COMPATIBILITY,1) <> "." and right(COMPATIBILITY,1) <> "!" then LongDescription = LongDescription & "."
				LongDescription = LongDescription & " "
			end if
			if not isNull(RS("POINT7")) and RS("POINT7") <> "" then
				LongDescription = LongDescription & RS("POINT7")
				if right(RS("POINT7"),1) <> "." and right(RS("POINT7"),1) <> "!" then LongDescription = LongDescription & "."
			end if
			
			file.write chr(34) & GRPNAME & chr(34) & vbtab	'Name
			file.write RS("itemid") & vbtab	'SKU
			file.write "http://www.CellularOutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html?id=ppjproduct" & vbtab	'DestinationURL
			file.write "http://www.CellularOutfitter.com/productpics/thumb/" & RS("itempic_CO") & vbtab	'ImageURL
			file.write chr(34) & ShortDescription & chr(34) & vbtab	'ShortDescription
			file.write chr(34) & LongDescription & chr(34) & vbtab	'LongDescription
			file.write RS("price_CO") & vbtab	'SalePrice
			file.write RS("price_retail") & vbtab	'Price
			file.write chr(34) & stypeName & ", " & BrandName & ", " & ModelName & ", " & GRPNAME & chr(34) & vbtab	'Keywords
			file.write chr(34) & BrandName & chr(34) & vbtab	'Manufacturer
			file.write vbtab	'PrimaryCategory
			file.write vbcrlf
		end if
		RS.movenext
	loop
end if
RS.close
set RS = nothing
file.close()

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub