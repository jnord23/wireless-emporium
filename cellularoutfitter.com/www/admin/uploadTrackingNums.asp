<%
pageTitle = "Admin - Upload Tracking Numbers"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<font face="Arial,Helvetica">
<blockquote>
<blockquote>
<br><br><br>

<%
server.scripttimeout = 1000 'seconds
dim Path
Path = server.mappath("/tempCSV")

' Create an instance of AspUpload object
Set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	' Capture uploaded file. Return the number of files uploaded
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		' Obtain File object representing uploaded file
		Set File = Upload.Files(1)
		myFile = File.path
		'response.write "<p>" & myFile & "</p>"
		
		uploadDate = upload.form("uploadDate")
		if not isDate(uploadDate) then
			response.write "<h3>The date you entered is not a valid date. Please try again.</h3>"
			response.write "<p><a href=""javascipt:history.back();"">BACK</a></p>"
			response.end
		end if
		
		' Delete all existing XML files in /www/admin/tempCSV
		set fs = CreateObject("Scripting.FileSystemObject")
		Set demofolder = fs.GetFolder(Path)
		Set filecoll = demofolder.Files
		For Each fil in filecoll
			if UCase(right(fil.name,4)) = ".XML" then
				thisfile = Path & "\" & fil.name
				if myFile <> thisfile then
					fs.DeleteFile thisfile
				end if
			end if
		Next
		
		' Remove ampersands:
		if not fs.FileExists(myFile) then
			response.write("File " & myFile & " does not exist, write aborted.<br>")
			response.end
		else
			set readfile = fs.OpenTextFile(myFile, 1)
			strToWrite = ""
			do while readfile.AtEndOfStream <> true
				strToWrite = strToWrite & readfile.readline & vbCrLf
			loop
			readfile.Close()
			
			set writefile = fs.OpenTextFile(myFile, 2)
			strToWrite = replace(strToWrite,"&","")
			strToWrite = replace(strToWrite,"?","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"  "," ")
			'response.write "<p>" & replace(strToWrite,vbCrLf,"<br>") & "</p>"
			writefile.write strToWrite
			writefile.Close()
						
			' Update SQL DB with Tracking Numbers (PIC) matched to OrderIDs (ReferenceID):
			dim mynodes, xmlDoc, i
			set xmlDoc = Server.CreateObject("Msxml2.DOMDocument.3.0")
			xmlDoc.async = false
			xmlDoc.load(myFile)
			set mynodes = xmlDoc.documentElement.selectNodes("Record")
			'response.write "<h3>" & mynodes.length & "</h3>"
			dim a
			a = 0
			for each Node in mynodes
				ReferenceID = ""
				PIC = ""
				for i = 0 to Node.ChildNodes.Length - 1
					if Node.childnodes.item(i).nodename = "PIC" then PIC = Node.childnodes.item(i).text
					if Node.childnodes.item(i).nodename = "ReferenceID" then ReferenceID = Node.childnodes.item(i).text
				next
				'if len(ReferenceID) = 6 and len(PIC) > 0 then
				if len(ReferenceID) = 6 then
					a = a + 1
					'response.write ReferenceID & " | " & PIC & "<br>"
					'SQL = "UPDATE WE_orders SET TrackingNum='" & PIC & "' WHERE OrderID='" & ReferenceID & "'"
					'SQL = "UPDATE WE_orders SET TrackingNum='" & PIC & "', confirmSent='yes', confirmdatetime='" & now & "' WHERE OrderID='" & ReferenceID & "'"
					SQL = "UPDATE WE_orders SET TrackingNum='" & PIC & "', confirmSent='yes', confirmdatetime='" & uploadDate & "' WHERE OrderID='" & ReferenceID & "'"
					response.write "<p>" & SQL & "</p>"
					oConn.execute SQL
				end if
			next
			response.write "<h3>" & a & " records updated!</h3>"
		end if
	end if
else
	%>
	<h3>Select a File to Upload:</h3>
	<form enctype="multipart/form-data" action="uploadTrackingNums.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="text" name="uploadDate" value="<%=date()%>">&nbsp;Date&nbsp;Confirmation&nbsp;Sent</p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if
%>

</blockquote>
</blockquote>
</font>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
