<%
pageTitle = "Item Sales Report for CO"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
if request("submitted") <> "" then
	'Response.ContentType = "application/vnd.ms-excel"
	
	strError = ""
	dateStart = request("dateStart")
	dateEnd = request("dateEnd")
	PartNumber = request.form("PartNumber")
	
	if not isDate(dateStart) or not isDate(dateEnd) then
		strError = strError & "<p>dateStart and dateEnd must both be valid dates</p>"
	else
		dateStart = dateValue(dateStart)
		dateEnd = dateValue(dateEnd)
		if dateEnd < dateStart then strError = strError & "<p>dateEnd must be later than or equal to dateStart</p>"
	end if
	
	strDates = dateStart & "<br>to<br>" & dateEnd
	showblank = "&nbsp;"
	shownull = "-null-"
	
	if strError = "" then
		dateEnd = dateAdd("D",1,dateEnd)
		SQL = "SELECT B.itemID, B.quantity, C.itemDesc_CO, C.price_CO"
		SQL = SQL & " FROM (we_Orders A INNER JOIN we_orderdetails B ON A.orderID=B.orderID)"
		SQL = SQL & " INNER JOIN we_Items C ON B.itemID=C.itemID"
		SQL = SQL & " WHERE A.store = 2"
		if PartNumber <> "" then
			if inStr(PartNumber,"%") > 0 then
				SQL = SQL & " AND C.PartNumber LIKE '" & PartNumber & "'"
			else
				SQL = SQL & " AND C.PartNumber = '" & PartNumber & "'"
			end if
		end if
		if dateStart <> "" then SQL = SQL & " AND A.orderdatetime >= '" & dateStart & "'"
		if dateEnd <> "" then SQL = SQL & " AND A.orderdatetime <= '" & dateEnd & "'"
		SQL = SQL & " ORDER BY B.itemID"
		set RS = Server.CreateObject("ADODB.Recordset")
		'response.write "<h3>" & SQL & "</h3>" & vbCrLf
		'response.end
		RS.open SQL, oConn, 3, 3
		
		response.write "<p><a href=""/admin/report_ItemSales.asp"">Back to Report Criteria</a></p>" & vbcrlf
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			%>
			<table border="1" width="100%" cellpadding="2" cellspacing="0">
				<tr>
					<td><b>Item&nbsp;ID</b></td>
					<td><b>Item&nbsp;Description</b></td>
					<td><b>Qty</b></td>
					<td><b>Our&nbsp;Price</b></td>
					<td><b>TOTAL</b></td>
				</tr>
				<%
				holdItemID = RS("itemID")
				quantity = RS("quantity")
				holdItemDesc = RS("itemDesc_CO")
				holdItemPrice = RS("price_CO")
				RS.movenext
				do until RS.eof
					if RS("itemID") <> holdItemID then
						printItemID = holdItemID
						%>
						<tr>
							<td><%=holdItemID%></td>
							<td><%=holdItemDesc%></td>
							<td><%=quantity%></td>
							<td><%=formatCurrency(holdItemPrice)%></td>
							<td><%=formatCurrency(quantity * holdItemPrice)%></td>
						</tr>
						<%
						total = total + quantity
						grandTotal = grandTotal + (quantity * holdItemPrice)
						holdItemID = RS("itemID")
						quantity = RS("quantity")
						holdItemDesc = RS("itemDesc_CO")
						holdItemPrice = RS("price_CO")
					else
						quantity = quantity + RS("quantity")
					end if
					RS.movenext
				loop
				if printItemID <> holdItemID then
					%>
					<tr>
						<td><%=holdItemID%></td>
						<td><%=holdItemDesc%></td>
						<td><%=quantity%></td>
						<td><%=formatCurrency(holdItemPrice)%></td>
						<td><%=formatCurrency(quantity * holdItemPrice)%></td>
					</tr>
					<%
					total = total + quantity
					grandTotal = grandTotal + (quantity * holdItemPrice)
				end if
				%>
				<tr>
					<td align="center"><b><%=strDates%></b></td>
					<td><b><%=request.form("PartNumber")%></b></td>
					<td><b><%=total%></b></td>
					<td><b>&nbsp;</b></td>
					<td><b><%=formatCurrency(grandTotal)%></b></td>
				</tr>
			</table>
			<br>
			<p><a href="/admin/report_ItemSales.asp">Back to Report Criteria</a></p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<%
		end if
		
		RS.close
		set RS = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
else
	%>
	<form action="report_ItemSales.asp" method="post">
		<table border="0" width="800" align="center">
			<tr>
				<td width="40%" valign="top">
					<p><input type="text" name="dateStart">&nbsp;&nbsp;dateStart</p>
					<p><input type="text" name="dateEnd">&nbsp;&nbsp;dateEnd</p>
				</td>
				<td width="60%" valign="top">
					<p><input type="text" name="PartNumber">&nbsp;&nbsp;Part #
					<br>[For wildcard searches, add "%" to the end of the partial Part Number. examples: SCR-% or SCR-SAM%]</p>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<p>&nbsp;</p>
					<p><input type="submit" name="submitted" value="Generate Report"></p>
				</td>
			</tr>
		</table>
	</form>
	<%
end if
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
