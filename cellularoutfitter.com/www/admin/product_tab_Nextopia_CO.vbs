set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim fs, file, filename, path
filename = "productList_Nextopia_CO.txt"
path = "c:\inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\" & filename
imgAbsolutePath = "c:\inetpub\wwwroot\productpics_co\"

set fs = CreateObject("Scripting.FileSystemObject")
if fs.FileExists(path) then fs.DeleteFile(path)

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)
dim SQL, RS
SQL = 	"select	a.itemid, a.itemdesc_co, a.price_co" & vbcrlf & _
		"	,	replace(replace(replace(convert(varchar(8000), dbo.fn_stripHTML(a.itemlongdetail_co)), char(10), ' '), char(13), ' '), char(9), ' ') itemlongdetail_co, a.itempic_co" & vbcrlf & _
		"	,	isnull(c.brandname, 'Universal') brandName" & vbcrlf & _
		"	,	isnull(d.modelname, 'Universal') modelname" & vbcrlf & _
		"	,	isnull(b.typename, 'Universal') typename" & vbcrlf & _
		"	,	isnull(f.color, '') color" & vbcrlf & _
		"	,	w.inv_qty" & vbcrlf & _
		"	,	a.numberOfSales" & vbcrlf & _
		"from	we_items a with (nolock) join we_types b with (nolock)" & vbcrlf & _
		"	on	a.typeid = b.typeid left outer join we_brands c with (nolock)" & vbcrlf & _
		"	on	a.brandid = c.brandid left outer join we_models d with (nolock)" & vbcrlf & _
		"	on	a.modelid = d.modelid left outer join we_pnDetails e with (nolock)" & vbcrlf & _
		"	on	a.partnumber = e.partnumber left outer join xproductcolors f" & vbcrlf & _
		"	on	a.colorid = f.id join we_items w" & vbcrlf & _
		"	on	a.partnumber = w.partnumber and w.master = 1" & vbcrlf & _
		"where	a.hidelive = 0" & vbcrlf & _
		"	and a.price_co > 0" & vbcrlf & _
		"	and	a.partnumber not like 'WCD%' and a.partnumber not like 'DEC%'" & vbcrlf & _
		"	and	(w.inv_qty > 0 or e.alwaysinstock = 1)" & vbcrlf & _
		"	and	a.itempic_co is not null and a.itempic_co <> ''" & vbcrlf & _
		"	and	a.itemdesc_co is not null and a.itemdesc_co <> ''" & vbcrlf & _
		"order by a.itemid desc"

set rs = oConn.execute(sql)
if not RS.eof then
	file.writeLine "SKU" & vbTab & "Name" & vbTab & "Price" & vbTab & "Description" & vbTab & "Image" & vbTab & "URL" & vbTab & "Category" & vbTab & "Brand" & vbTab & "Color" & vbTab & "Model" & vbTab & "Sales"
	do until RS.eof
		itempic_co = rs("itempic_co")
		if fs.FileExists(imgAbsolutePath & "big\" & itempic_co) then
			itemid = rs("itemid")
			itemlongdetail_co = rs("itemlongdetail_co")
			price_co = rs("price_co")
			itemdesc_co = rs("itemdesc_co")
			brandName = rs("brandName")
			modelName = rs("modelName")
			categoryName = rs("typename")
			color = rs("color")
			sales = rs("numberOfSales")
	
			strline = "WE-" & itemid & vbTab
			strline = strline & itemdesc_co & vbTab
			strline = strline & price_co & vbTab
			strline = strline & itemlongdetail_co & vbTab
			strline = strline & "http://www.cellularoutfitter.com/productpics/big/" & itempic_co & vbTab
			strline = strline & "http://www.cellularoutfitter.com/p-" & itemid & "-" & formatSEO(itemdesc_co) & ".html?IZID=NXTPIA_" & itemid & "&utm_source=Nextopia&utm_medium=INT" & vbTab
			strline = strline & "Cell Phone Accessories > " & brandName & " Accessories > " & modelName & " Accessories > " & categoryName & vbTab
			strline = strline & brandName & vbTab
			strline = strline & color & vbTab
			strline = strline & modelName & vbTab
			strline = strline & sales
	
			file.WriteLine strline		
		end if
		RS.movenext
	loop
end if

RS.close
set RS = nothing
file.close()

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(formatSEO, "---", "-"), "--", "-")		
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub