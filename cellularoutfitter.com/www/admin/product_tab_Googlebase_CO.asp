<%
response.buffer = false
pageTitle = "Create Googlebase Product List TXT for CellularOutfitter.com"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->

<%
Server.ScriptTimeout = 2400 'seconds

dim fs, file, filename, path
filename = "COGoogleBase.txt"
'path = "E:\Inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\" & filename
path = server.mappath("\tempCSV\" & filename)
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List TXT for Googlebase (CellularOutfitter.com)</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							Response.Write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href=""http://www.cellularoutfitter.com/tempCSV/" & filename & """>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
	
<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.Write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	dim expDate, myMonth, myDay, myYear
	expDate = dateAdd("d",21,date)
	myMonth = month(expDate)
	if myMonth < 10 then myMonth = "0" & myMonth
	myDay = day(expDate)
	if myDay < 10 then myDay = "0" & myDay
	if myHour < 10 then myHour = "0" & myHour
	myYear = year(expDate)
	expDate = myYear & "-" & myMonth & "-" & myDay
	
	dim SQL, RS
	dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	SQL = "SELECT A.itemID,A.PartNumber AS MPN,A.itemDesc_CO,A.itemLongDetail_CO,A.POINT1,A.POINT2,A.POINT3,A.POINT4,A.POINT5,A.POINT6,A.COMPATIBILITY,A.POINT7,"
	SQL = SQL & " A.flag1,A.itempic_CO,A.price_CO,A.price_retail,A.inv_qty,A.brandID,"
	SQL = SQL & " B.brandName,C.modelName,C.[temp],D.typeName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0"
	'Comment out once we have itemDesc_CO reliably populated for all CO items:
	SQL = SQL & " AND A.price_CO > 0"
	SQL = SQL & " AND A.itemDesc_CO IS NOT null"
	SQL = SQL & " AND A.typeid <> 16"
	SQL = SQL & " ORDER BY A.itemID"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 3, 3
	if not RS.EOF then
		file.WriteLine "title" & vbtab & "description" & vbtab & "c:description2" & vbtab & "id" & vbtab & "link" & vbtab & "quantity" & vbtab & "image_link" & vbtab & "price" & vbtab & "c:retail_price" & vbtab & "product_type" & vbtab & "brand" & vbtab & "c:cell_phone_model" & vbtab & "condition" & vbtab & "payment_accepted" & vbtab & "mpn" & vbtab & "color" & vbtab & "year" & vbtab & "c:GoogleAffiliateNetworkProductURL:String"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
				set RS2 = CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				if isnull(RS("typeName")) then
					stypeName = "Universal"
				else 
					stypeName = RS("typeName")
				end if
				if isnull(RS("ModelName")) then
					ModelName = "Universal"
				else
					ModelName = RS("ModelName")
				end if
				if isnull(RS("BrandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("BrandName")
				end if
				
				'--color filter
				my_desc = Ucase(replace(RS("itemDesc_CO"),"Blackberry",""))
				my_desc = Ucase(replace(my_desc,"Blackjack",""))
				my_desc = Ucase(replace(my_desc,"Textured",""))
				my_desc = Ucase(replace(my_desc,"Red Sox",""))
				my_desc = Ucase(replace(my_desc,"Blue Jays",""))
				
				if stypeName = "Batteries" or stypeName = "Hands-Free" then
					mycolor = ""
				else
					if inStr(My_desc,"BLACK") > 0 then
						mycolor = "BLACK"
					elseif inStr(My_desc,"BABY BLUE") > 0 then
						mycolor = "BABY BLUE"
					elseif inStr(My_desc,"GREEN") > 0 then
						mycolor = "GREEN"
					elseif inStr(My_desc,"PURPLE") > 0 then
						mycolor = "PURPLE"
					elseif inStr(My_desc,"RED") > 0 then
						mycolor = "RED"
					elseif inStr(My_desc,"HOT PINK") > 0 then
						mycolor = "HOT PINK"
					elseif inStr(My_desc,"PINK") > 0 then
						mycolor = "PINK"
					elseif inStr(My_desc,"BLUE") > 0 then
						mycolor = "BLUE"
					elseif inStr(My_desc,"MAGEBTA") > 0 then
						mycolor = "MAGENTA"
					elseif inStr(My_desc,"YELLOW") > 0 then
						mycolor = "YELLOW"
					elseif inStr(My_desc,"TURQUOISO") > 0 then
						mycolor = "TURQUOISO"
					elseif inStr(My_desc,"BROWN") > 0 then
						mycolor = "BROWN"
					elseif inStr(My_desc,"WHITE") > 0 then
						mycolor = "WHITE"
					elseif inStr(My_desc,"BEIGE") > 0 then
						mycolor = "BEIGE"
					elseif inStr(My_desc,"CLEAR") > 0 then
						mycolor = "CLEAR"
					elseif inStr(My_desc,"SMOKE") > 0 then
						mycolor = "SMOKE"
					elseif inStr(My_desc,"SILVER") > 0 then
						mycolor = "SILVER"
					else
						if stypeName = "Faceplates" then
							mycolor = "MULTI COLORS"
						else
							mycolor = "BLACK"
						end if
					end if
				end if
				'--end of color filter
				
				strItemLongDetail = RS("itemLongDetail_CO")
				strItemLongDetail = strItemLongDetail & "<ul>"
				if not isNull(RS("POINT1")) then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT1") & "</li>"
				if not isNull(RS("POINT2")) then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT2") & "</li>"
				if not isNull(RS("POINT3")) then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT3") & "</li>"
				if not isNull(RS("POINT4")) then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT4") & "</li>"
				if not isNull(RS("POINT5")) then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT5") & "</li>"
				if not isNull(RS("POINT6")) then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT6") & "</li>"
				if not isNull(RS("COMPATIBILITY")) then strItemLongDetail = strItemLongDetail & "<li>" & RS("COMPATIBILITY") & "</li>"
				if not isNull(RS("POINT7")) then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT7") & "</li>"
				strItemLongDetail = strItemLongDetail & "</ul>"
				strItemLongDetail = replace(strItemLongDetail,chr(34),"''")
				sTypeName_nos = Left(sTypeName,Len(sTypeName)-1)
				sProductName = RS("itemDesc_CO")
				'GRPNAME = BrandName & " " & ModelName & " " & nameSEO(sTypeName) & " - " & sProductName
				GRPNAME = BrandName & " " & singularSEO(sTypeName) & " for " & BrandName & " " & ModelName
				
				select case RS("itemid")
					case 20832
						GRPNAME = "Apple iPhone 3G Stylus - Finger Touch Stylus Pen for Apple iPhone 3G"
						strItemLongDetail_2 = "Buy Apple iPhone 3G Stylus and other Apple Antennas/Parts from the #1 cell phone accessory store online  CellularOutfitter. This  Apple iPhone 3G Stylus is a quality Apple iPhone 3G accessory at an unbeatable price. We offer the best accessories for your Apple iPhone 3G cell phone, which you won't find at other retailers. Check out our huge collection of quality antennas/parts for iPhone 3G and get great deals on antennas/parts for iPhone 3G. All orders for Apple iPhone 3G Stylus are backed by our 100% Quality Assurance Guarantee."
					case 24439
						GRPNAME = "iPhone 3G Leather Case - Cellet Signature Vertical Leather Carrying Case/Pouch for iPhone 3G"
						strItemLongDetail_2 = "Buy Cellet Signature Vertical iPhone Leather Case and other iPhone Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This Cellet Signature Vertical iPhone Leather Pouch for iPhone is a quality iPhone 3G leather case at an unbeatable price. We offer the best IPhone leather cases which you won't find at other retailers. Check out our huge collection of quality leather cases for iPhone 3G and get great deals on leather cases for iPhone 3G at the largest . All orders for Cellet Signature Vertical Leather Carrying Case for iPhone are backed by our 100% Quality Assurance Guarantee."
					case 14944
						GRPNAME = "iPhone Leather Cases - Black Neoprene Case for Apple iPhone"
						strItemLongDetail_2 = "Buy Black Neoprene iPhone Case and other iPhone Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This Black Neoprene iPhone Case is a quality iPhone leather case at an unbeatable price. We offer the best leather cases for your iPhone  which you won't find at other retailers. Check out our huge collection of quality leather cases for iPhone and get great deals on iPhone leather cases at the largest cell phone accessory store online. All orders for Black Neoprene iPhone Case are backed by our 100% Quality Assurance Guarantee."
					case 27455
						GRPNAME = "iPhone Leather Cases - Logic Deluxe Leather Vertical Carrying iPhone Case"
						strItemLongDetail_2 = "Buy Logic Deluxe Leather Vertical iPhone Case and other iPhone Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This logic deluxe iPhone leather case is a quality iPhone case at an unbeatable price. We offer the best leather cases for your iPhone, which you won't find at other retailers. Check out our huge collection of quality iPhone leather cases and get great deals on leather cases for iPhone at the largest cell phone accessory store online."
					case 24155
						GRPNAME = "iPhone 3G Chargers - Universal 7 in 1 Retractable Cell Phone Rapid IC Car Charger"
						strItemLongDetail_2 = "Buy Universal 7 in 1 Retractable Cell Phone Rapid IC Car Charger and other iPhone Chargers from the #1 cell phone accessory store online  CellularOutfitter. This Universal 7 in 1 Retractable Cell Phone Rapid IC Car Charger is a quality iPhone 3G charger at an unbeatable price. We offer the best chargers for your iPhone 3G, which you won't find at other retailers. Check out our huge collection of quality iPhone chargers and get great deals on chargers for iPhone 3G."
					case 22785
						GRPNAME = "Apple iPhone 3G Leather Cases - Apple iPhone 3G Snap-On Rubberized Snap-On Case (Purple)"
						strItemLongDetail_2 = "Buy iPhone 3G Snap-On Rubberized (Purple) and other iPhone Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This iPhone 3G Snap-On Rubberized Case (Purple) is a quality iPhone 3G leather case at an unbeatable price. We offer the best iPhone leather cases for your iPhone 3G, which you won't find at other retailers. Check out our huge collection of quality iPhone 3G leather cases and get great deals on leather cases for iPhone 3G at the largest cell phone accessory store online."
					case 26798
						GRPNAME = "Apple All Models w/2.5mm jack Hands-Free - Apple Hands-Free Car Kit - Charger/Holder/FM Transmitter"
						strItemLongDetail_2 = "Apple Cell Phone Hands-Free Car Kit - Charger/Holder/FM Transmitter and other Cell Phone Hands-Free from the #1 cell phone accessory store online  CellularOutfitter. This Apple Cell Phone Hands-Free Car Kit - Charger/Holder/FM Transmitter is a quality Apple All Models w/2.5mm jack hands-free at an unbeatable price. Check out our huge collection of quality cell phone hands-free for All Models w/2.5mm jack and get great deals on cell phone hands-free for All Models w/2.5mm jack at the largest cell phone accessory store online."
					case 20857
						GRPNAME = "iPhone 3G Leather Cases - White iPhone 3G Silicone Skin Case"
						strItemLongDetail_2 = " iPhone 3G Silicone White Skin Case and other iPhone Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This iPhone 3G Silicone Skin Case (White) is a quality iPhone 3G leather case at an unbeatable price. We offer the best leather cases for your iPhone 3G, which you won't find at other retailers. Check out our huge collection of quality iPhone 3G leather cases and get great deals on leather cases for iPhone 3G at the largest cell phone accessory store online."
					case 20929
						GRPNAME = "iPhone 3G Silicone Skin Case (Lime Green) - iPhone 3G Leather Cases"
						strItemLongDetail_2 = "iPhone 3G Silicone Skin Case (Lime Green) and other iPhone Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This iPhone 3G Silicone Skin Case (Lime Green) is a quality iPhone 3G leather case at an unbeatable price. We offer the best iPhone 3G leather cases which you won't find at other retailers. All orders for Apple iPhone 3G Silicone Skin Case (Lime Green) are backed by our 100% Quality Assurance Guarantee."
					case 20688
						GRPNAME = "Retractable-Cord Rapid IC Car Charger for Apple iPhone 3G/iPod Touch 2nd Gen - iPhone 3G Chargers"
						strItemLongDetail_2 = "Retractable-Cord Rapid IC Car Charger for Apple iPhone 3G/iPod Touch 2nd Gen and other iPhone Chargers from the #1 cell phone accessory store online  CellularOutfitter. This Retractable-Cord Rapid IC Car Charger for iPhone 3G/iPod Touch 2nd Gen is a quality iPhone 3G charger at an unbeatable price. Check out our huge collection of quality iPhone 3G chargers and get great deals on chargers for iPhone 3G!"
					case 22831
						GRPNAME = "Apple iPhone Chargers - Naztech N300 3-in-1 Mobile Phone Charger for Apple iPhone/iPod"
						strItemLongDetail_2 = "Naztech N300 Charger for iPhone/iPod and other iPhone/iPod Chargers from the #1 cell phone accessory store online  CellularOutfitter. This Naztech N300 Charger for iPhone/iPod is a quality iPhone charger at an unbeatable price. We offer the best chargers for your iPhone, which you won't find at other retailers. Check out our huge collection of quality chargers for iPhone and get great deals on chargers for iPhone!"
					case 17669
						GRPNAME = "iPhone Faceplates - Graffiti Skull Snap-On Protector Case"
						strItemLongDetail_2 = "Buy iPhone Faceplate - Graffiti Skull Snap-On Protector Case and other iPhone Faceplates from the #1 cell phone accessory store online  CellularOutfitter. This iPhone Faceplate - Graffiti Skull Snap-On Protector Case is a quality Apple iPhone faceplate at an unbeatable price. All orders for Faceplate Apple iPhone Graffiti Skull Snap-On Protector Case are backed by our 100% Quality Assurance Guarantee."
					case 11855
						GRPNAME = "Apple All Models w/2.5mm jack Hands-Free - Pink Portable Speakers for iPod Nano/Mini/Photo/Video"
						strItemLongDetail_2 = "Pink Portable Speakers for iPod Nano/Mini/Photo/Video and other iPhone accessories from the #1 cell phone accessory store online  CellularOutfitter. These Pink iPod Portable Speakers for Nano/Mini/Photo/Video with quality Apple All Models w/2.5mm jack hands-free available @ an unbeatable price. Check out our huge collection of quality hands-free for iPod and get great deals on hands-free. All orders for Portable iPhone/iPod Speakers are backed by our 100% Quality Assurance Guarantee."
					case 17650
						GRPNAME = "Samsung SCH-U740 Faceplates - Alias International Flags Snap-On Protector Case for Samsung SCH-U740"
						strItemLongDetail_2 = "Buy Samsung SCH-U740 Faceplate - Alias International Flags Snap-On Protector Case and other Samsung Faceplates from the #1 cell phone accessory store online  CellularOutfitter. This Samsung Faceplate SCH-U740 Alias International Flags Snap-On Protector Case is a quality Samsung SCH-U740 faceplate at an unbeatable price. We offer the best faceplates for your Samsung SCH-U740 cell phone, which you won't find at other retailers. All orders for Samsung SCH-U740 Faceplate / Case are backed by our 100% Quality Assurance Guarantee."
					case 11753
						GRPNAME = "iPod 3 in 1 Car Kit - Charger/Holder/FM Transmitter (White)"
						strItemLongDetail_2 = "White iPod Car Kit with Charger/Holder/FM Transmitter and other iPod chargers from the #1 cell phone accessory store online  CellularOutfitter. This white iPod Car Kit - Charger/Holder/FM Transmitter is a quality Apple All Models w/2.5mm jack hands-fre at an unbeatable price. All orders for iPod Car Kit - Charger/Holder/FM Transmitter (White) are backed by our 100% Quality Assurance Guarantee."
					case 23534
						GRPNAME = "iPhone 3G Leather Cases - Black iPhone 3G Deluxe Rubberized Slider Case"
						strItemLongDetail_2 = "Buy Black iPhone 3G Deluxe Rubberized Slider Case and other iPhone Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This iPhone 3G Deluxe Rubberized Slider Case is a quality iPhone 3G leather case at an unbeatable price. We offer the best leather cases for your iPhone 3G, which you won't find at other retailers. Check out our huge collection of quality leather cases for iPhone 3G and get great deals on leather cases for iPhone 3G at the largest cell phone accessory store online."
					case 24470
						GRPNAME = "iPhone / iPod Metallic 3.5mm Male to 2.5mm Female Audio Adapter"
						strItemLongDetail_2 = "Buy iPhone/iPod Metallic 3.5mm Male to 2.5mm Female Audio Adapter other iPhone/iPod Hands-Free from the #1 cell phone accessory store online  CellularOutfitter. This iPhone/iPod  Adapter is a quality iPhone hands-free at an unbeatable price. We offer the best hands-free for your iPhone/iPod, which you won't find at other retailers. All orders for this Audio Adapter for iPhone/iPod are backed by our 100% Quality Assurance Guarantee."
					case 13476
						GRPNAME = "Mega Clip Neoprene Case/Pouch for SAMSUNG U340 - Leather Cases"
						strItemLongDetail_2 = "Buy SAMSUNG U340 Mega Clip Neoprene Carrying Case/Pouch and other Samsung Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This Mega Clip Neoprene Case/Pouch for SAMSUNG U340 is a quality Samsung SCH-U340 leather case at an unbeatable price. We offer the best leather cases for your Samsung SCH-U340 cell phone, which you won't find at other retailers. All orders for Mega Clip Neoprene Case/Pouch for SAMSUNG U340 are backed by our 100% Quality Assurance Guarantee."
					case 15920
						GRPNAME = "White iPhone Stereo Earbud Hands-Free"
						strItemLongDetail_2 = "Buy White iPhone Stereo Earbud Hands-Free and other iPhone Hands-Free from the #1 cell phone accessory store online  CellularOutfitter. This White iPhone Stereo Earbud Hands-Free is a quality iPhone hands-free at an unbeatable price. We offer the best hands-free for your iPhone, which you won't find at other retailers. All orders for White iPhone Stereo Earbud Hands-Free are backed by our 100% Quality Assurance Guarantee."
					case 14933
						GRPNAME = "Black iPhone Stereo Earbud Hands-Free"
						strItemLongDetail_2 = "Black iPhone Stereo Earbud Hands-Free and other iPhone Hands-Free from the #1 cell phone accessory store online  CellularOutfitter. This Black iPhone Stereo Earbud Hands-Free is a quality iPhone hands-free at an unbeatable price. Check out our huge collection of quality hands-free for iPhone and get great deals on iPhone hands-free at the largest cell phone accessory store online. All orders for Black iPhone Stereo Earbud Hands-Free are backed by our 100% Quality Assurance Guarantee."
					case 27361
						GRPNAME = "Black Maxim Vertical iPhone Leather Case/Pouch - iPhone Leather Cases"
						strItemLongDetail_2 = "Buy Black Maxim Vertical Leather Case/Pouch and other iPhone Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This Black Maxim Vertical iPhone Leather Case/Pouch for is a quality iPhone leather case at an unbeatable price. We offer the best leather cases for your iPhone cell phone, which you won't find at other retailers. Check out our huge collection of quality leather cases for iPhone and get great deals on leather cases for iPhone at the largest cell phone accessory store online."
					case 21474
						GRPNAME = "Sony Ericsson W350 Data Cable - OEM Sony Ericsson W350 USB Data Transfer Data Cable DCU-60 (DPY901487)"
						strItemLongDetail_2 = "Buy OEM Sony Ericsson W350 USB Data Transfer Cable DCU-60 (DPY901487) and other Sony Ericsson Data Cable from the #1 cell phone accessory store online  CellularOutfitter. This OEM Sony Ericsson W350 USB Data Transfer Cable DCU-60 (DPY901487) is a quality Sony Ericsson W350 data cable at an unbeatable price. We offer the best data cables for your Sony Ericsson W350 cell phone, which you won't find at other retailers. Check out our huge collection of quality data cables for Ericsson W350 and get great deals on data cable & memory for Ericsson W350 at the largest cell phone accessory store online."
					case 23538
						GRPNAME = "iPhone 3G Leather Case -  Green/White iPhone 3G Deluxe Rubberized Slider Case"
						strItemLongDetail_2 = "Green/White iPhone 3G Deluxe Rubberized Slider Case and other iPhone 3G Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This Green/White iPhone 3G Deluxe Rubberized Slider Case is a quality Apple iPhone 3G leather case at an unbeatable price. We offer the best leather cases for your iPhone 3G cell phone, which you won't find at other retailers. Check out our huge collection of quality leather cases for iPhone 3G and get great deals on leather cases for iPhone 3G at the largest cell phone accessory store online."
					case 18537
						GRPNAME = "Samsung Blackjack II SGH-I617 Leather Case - Samsung Blackjack II SGH-I617 Black Snap-On Rubberized Case"
						strItemLongDetail_2 = "Buy Samsung Blackjack II SGH-I617 Black Snap-On Rubberized Snap-On Case and other Samsung Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This Samsung Blackjack II SGH-I617 Black Snap-On Rubberized Case is a quality Samsung Blackjack II SGH-I617 leather case at an unbeatable price. Check out our huge collection of quality leather cases for Blackjack II SGH-I617 and get great deals on Blackjack II SGH-I617 leather cases at the largest cell phone accessory store online. All orders backed by our 100% Quality Assurance Guarantee."
					case 20049
						GRPNAME = "iPhone Faceplates - Faceplate iPhone Black w/Circle Designs Snap-On Protector Case"
						strItemLongDetail_2 = "Buy Faceplate iPhone Black w/Circle Designs Snap-On Protector Case and other iPhone Faceplates from the #1 cell phone accessory store online  CellularOutfitter. This Faceplate iPhone Black w/Circle Designs Snap-On Protector Case is a quality iPhone faceplate at an unbeatable price. We offer the best faceplates for your iPhone, which you won't find at other retailers. Check out our huge collection of quality iPhone faceplates and get great deals on faceplates for iPhone at the largest cell phone accessory store online. All orders are backed by our 100% Quality Assurance Guarantee."
					case 14713
						GRPNAME = "iPhone Holsters/Belt Clips - Belt Clip Holster for iPhone"
						strItemLongDetail_2 = "Buy Belt Clip Holster for iPhone and other iPhone Holsters/Belt Clips from the #1 cell phone accessory store online  CellularOutfitter. This quality Belt Clip/Holster for iPhone is available at an unbeatable price. We offer the best holsters/belt clips for your iPhone, which you won't find at other retailers. Check out our huge collection of quality holsters/belt clips for iPhone and get great deals on holsters/belt clips for iPhone at the largest cell phone accessory store online. All orders are backed by our 100% Quality Assurance Guarantee."
					case 25623
						GRPNAME = "Audiovox Verizon Blitz/UTStarcom Pantech TXT8010 Leather Case - Verizon Blitz/UTStarcom Pantech TXT8010 Deluxe Leather Vertical Case"
						strItemLongDetail_2 = "Buy Verizon Blitz/UTStarcom Pantech TXT8010 Deluxe Leather Vertical Carrying Case/Pouch and other Audiovox Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This quality Verizon Blitz/UTStarcom Pantech TXT8010 Deluxe Leather Vertical Carrying Case/Pouch is available at an unbeatable price. We offer the best leather cases for your Audiovox Verizon Blitz/UTStarcom Pantech TXT8010 cell phone, which you won't find at other retailers. All orders are backed by our 100% Quality Assurance Guarantee."
					case 15266
						GRPNAME = "Nokia E62/E61 Data Cable - OEM Nokia E61 USB Data Transfer Cable (DKU-2)"
						strItemLongDetail_2 = "OEM Nokia E61 USB Data Transfer Cable (DKU-2) and other Nokia Data Cable & Memory from the #1 cell phone accessory store online  CellularOutfitter. This OEM Nokia E61 USB Data Transfer Cable (DKU-2) is a quality Nokia E62/E61 data cable at an unbeatable price. We offer the best data cable for your Nokia E62/E61 cell phone, which you won't find at other retailers. All orders for OEM Nokia E61 USB Data Transfer Cable (DKU-2) are backed by our 100% Quality Assurance Guarantee."
					case 26795
						GRPNAME = "Motorola w/2.5mm jack Hands-Free - Amp'd Mobile Stereo Cube Speaker System"
						strItemLongDetail_2 = "Buy Amp'd Mobile Stereo Cube Speaker System and other Motorola accessories from the #1 cell phone accessory store online  CellularOutfitter. This Amp'd Mobile Stereo Cube Speaker System is a quality Motorola w/2.5mm jack hands-free at an unbeatable price. We offer the best accessories for your Motorola cell phone, which you won't find at other retailers. Check out our huge collection of quality accessories for Motorola cell phones and get great deals on these at the largest cell phone accessory store online. All orders are backed by our 100% Quality Assurance Guarantee."
					case 21022
						GRPNAME = "Blackberry Bold 9000 Data Cable - OEM Blackberry USB Data Transfer Cable (ASY-006610-001)"
						strItemLongDetail_2 = "OEM Blackberry USB Data Transfer Cable (ASY-006610-001) and other Blackberry Data Cables from the #1 cell phone accessory store online  CellularOutfitter. This OEM Blackberry USB Data Transfer Cable (ASY-006610-001) is a quality Blackberry Bold 9000 data cable at an unbeatable price. We offer the best accessories for your Blackberry Bold 9000 cell phone, which you won't find at other retailers. All orders are backed by our 100% Quality Assurance Guarantee."
					case 22492
						GRPNAME = "HTC T-Mobile G1/Google Phone Stylus - Black Finger Touch Stylus Pen for T-Mobile G1/Google Phone"
						strItemLongDetail_2 = "Black Finger Touch Stylus Pen for T-Mobile G1/Google Phone and other HTC Antennas/Parts from the #1 cell phone accessory store online  CellularOutfitter. This quality Black Finger Touch Stylus Pen for T-Mobile G1/Google Phone is available at an unbeatable price. We offer the best accessories for your HTC T-Mobile G1/Google Phone cell phone, which you won't find at other retailers. Check out our huge collection of quality antennas/parts for T-Mobile G1/Google Phone and get great deals at the largest cell phone accessory store online."
					case 15608
						GRPNAME = "Motorola V365 Leather Case - Heavy-Duty Ballistic Nylon Case/Pouch for Motorola V365"
						strItemLongDetail_2 = "Buy Heavy-Duty Ballistic Nylon Case/Pouch for Motorola V365 and other Motorola Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. This Heavy-Duty Ballistic Nylon Case/Pouch for Motorola V365 is a quality Motorola V365 leather case at an unbeatable price. We offer the best leather cases for your Motorola V365 cell phone, which you won't find at other retailers. All orders for Motorola V365 leather cases are backed by our 100% Quality Assurance Guarantee."
					case 21141
						GRPNAME = "Classic Horizontal Apple iPhone Leather Cases - iPhone Accessories"
						strItemLongDetail_2 = "Buy your iPhone Leather Case and other iPhone accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the best quality iPhone Leather Cases for your Apple iPhone. Order now and get great deals on our iPhone Accessories backed by our 100% Quality Assurance Guarantee."
					case 21988
						GRPNAME = "Apple iPod Touch  (2nd Gen) AC/Wall Chargers (white) - iPod Touch Accessories"
						strItemLongDetail_2 = "Buy your Apple iPod Touch Charger and other iPod Touch Accessories from the #1 cell phone accessory store online  CellularOutfitter.  We offer the widest range of the best iPod Touch chargers for your Apple iPod Touch. Order now and get great deals on your iPod Touch Accessories backed by our 100% Quality Assurance Guarantee."
					case 20196
						GRPNAME = "LG Vu/CU920/CU915 Faceplates - LG Cell Phone Accessories"
						strItemLongDetail_2 = "Buy your LG cell phone faceplates/Screen Protector Film and other LG cell phone accessories from the #1 cell phone accessory store online  CellularOutfitter. Check out our huge collection of quality LG Cell Phone faceplates for LG Vu at the largest cell phone accessory store online. Order now and get great deals on your LG Cell Phone Accessories with our 100% Quality Assurance Guarantee."
					case 14622
						GRPNAME = "Apple iPhone Data Cable & Memory - iPhone Accessories"
						strItemLongDetail_2 = "Buy your Apple iPhone USB Data Transfer Cable and other iPhone Accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the best iPhone data cables & memory and other iPhone Accessories for your Apple iPhone, which you won't find at other retailers. All orders for iPhone Accessories are backed by our 100% Quality Assurance Guarantee."
					case 16892
						GRPNAME = "Pantech Duo C810  AC Home/Wall Chargers - Pantech Cell Phone Accessories"
						strItemLongDetail_2 = "Buy your Pantech Duo Charger and other Pantech Accessories from the #1 cell phone accessory store online  CellularOutfitter. Check out our huge collection of quality Pantech chargers for Pantech Cell Phones. Order now and get great deals on your Pantech Accessories backed by our 100% Quality Assurance Guarantee."
					case 21440
						GRPNAME = "Motorola RAZR VE20 Leather Cases - Large Neoprene Carrying Case/Pouch for Motorola RAZR VE20"
						strItemLongDetail_2 = "Buy your Motorola RAZR VE20 Neoprene Carrying Case/Pouch and other Motorola Cell Phone Accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best Motorola RAZR cell phone leather cases. Order now and get great deals on your Motorola Leather Case & other Motorola Cell Phone Accessories backed by our 100% Quality Assurance Guarantee."
					case 18641
						GRPNAME = "LG Voyager VX10000 Faceplates - Screen Protector Film - LG Cell Phone Accessories"
						strItemLongDetail_2 = "Buy your LG Voyager Screen Protector Film and other LG cell phone accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best LG Cell Phone faceplates for your LG Voyager cell phone, which you won't find at other retailers. Order now and get great deals on your LG Cell Phone Accessories backed by our 100% Quality Assurance Guarantee."
					case 17739
						GRPNAME = "Motorola i335 Holsters/Belt Clips - Motorola Cell Phone Accessories"
						strItemLongDetail_2 = "Buy your Motorola Belt Clip Holsters for the Motorola i335 and other Motorola Cell Phone Accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best Motorola holsters/belt clips for your Motorola cell phone, which you won't find at other retailers. Order now and get great deals on your Motorola Cell Phone Accessories  backed by our 100% Quality Assurance Guarantee."
					case 23788
						GRPNAME = "Samsung Eternity SGH-A867 Faceplates - Screen Protector Film"
						strItemLongDetail_2 = "Buy your Samsung Cell Phone faceplates/Screen Protector Film and other Samsung Cell Phone Accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best Samsung Cell Phone Faceplates for your Samsung cell phones. Order now and get great deals on our Samsung Cell Phone Accessories backed by our 100% Quality Assurance Guarantee."
					case 22154
						GRPNAME = "LG Dare VX9700 Faceplates -  Mirror Screen Protector Film - LG Cell Phone Accessories"
						strItemLongDetail_2 = "Buy your LG Dare Mirror Screen Protector Film and other LG Faceplates from the #1 cell phone accessory store online  CellularOutfitter. Check out our huge collection of quality faceplates for LG Dare cell phones. Order now and get great deals on LG Cell Phone Accessories backed by our 100% Quality Assurance Guarantee."
					case 21119
						GRPNAME = "Audiovox Verizon G'zone Boulder Leather Cases - Mega Clip Neoprene Carrying Case/Pouch for Verizon G'zone Boulder"
						strItemLongDetail_2 = "Buy the Mega Clip Neoprene Carrying Case/Pouch for Verizon G'zone Boulder and other Audiovox Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best leather cases for your Audiovox Verizon G'zone Boulder cell phone. Order now and get great deals on your Audiovox Accessories backed by our 100% Quality Assurance Guarantee."
					case 22425
						GRPNAME = "Pantech Matrix C740  Rapid IC Car Charger - Pantech Accessories"
						strItemLongDetail_2 = "Buy Pantech Matrix Chargers and other Pantech Accessories from the #1 cell phone accessory store online  CellularOutfitter. Check out our huge collection of quality Pantech Matrix Chargers for your Pantech Matrix Cell Phone and get great deals at the largest cell phone accessory store online. All orders for Pantech Matrix Accessories are backed by our 100% Quality Assurance Guarantee."
					case 21140
						GRPNAME = "Classic Apple iPhone 3G Horizontal Leather Cases - iPhone Accessories"
						strItemLongDetail_2 = "Buy the iPhone Leather Case for your Apple iPhone 3G and other iPhone Accessories from the #1 cell phone accessory store online  CellularOutfitter.  We offer the widest range of the best iPhone 3G leather cases at great deals. All orders for the iPhone 3G Accessories are backed by our 100% Quality Assurance Guarantee."
					case 20111
						GRPNAME = "Blackberry Curve 8330 Rapid IC Car Charger - Blackberry Accessories"
						strItemLongDetail_2 = "Buy your Blackberry Curve Chargers and other Blackberry Accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of best Blackberry chargers at great deals which you won't find at other retailers. All orders for Blackberry Accessories are backed by our 100% Quality Assurance Guarantee."
					case 21191
						GRPNAME = "LG Vu/CU920/CU915 USB Data Transfer Cable & Memory - LG Cell Phone Accessories"
						strItemLongDetail_2 = "Buy your LG Vu USB Data Transfer Cable and other LG Cell Phone Accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best LG data cable & memory for your LG Vu cell phone at great deals, which you won't find at other retailers. All orders for LG Cell Phone Accessories are backed by our 100% Quality Assurance Guarantee."
					case 19602
						GRPNAME = "LG enV2 VX9100 Rapid IC Car Charger - LG Cell Phone Accessories"
						strItemLongDetail_2 = "Buy the LG enV2 Car Chargers and other LG cell phone accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of best LG Cell Phone Chargers like the LG enV2 cell phone chargers at great deals which you won't find at other retailers. All orders for LG enV2 cell phone accessories are backed by our 100% Quality Assurance Guarantee."
					case 23155
						GRPNAME = "Blackberry Chargers - Retractable-Cord Rapid IC Car Charger for Blackberry Storm 9530"
						strItemLongDetail_2 = "Buy your Blackberry Chargers like the Car Charger for the  Blackberry Storm 9530 and other Blackberry accessories from the #1 cell phone accessory store online  CellularOutfitter. Check out our huge collection of the best quality Blackberry chargers for Storm 9530 and get great deals. All orders for Blackberry Accessories are backed by our 100% Quality Assurance Guarantee."
					case 20697
						GRPNAME = "iPhone 3G Leather Cases - PRO Deluxe Leather Horizontal Carrying Case/Pouch for Apple iPhone 3G"
						strItemLongDetail_2 = "Buy your iPhone Leather Case/Pouch and other iPhone Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best iPhone leather cases for your Apple iPhone 3G at great deals which you won't find at other retailers. All orders for iPhone Accessories are backed by our 100% Quality Assurance Guarantee."
					case 21543
						GRPNAME = "LG Dare VX9700 Faceplates -   Screen Protector Film - LG Cell Phone Accessories"
						strItemLongDetail_2 = "Buy your LG Dare Faceplates/Screen Protector Film and other LG cell phone accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of  LG Cell Phone Faceplates for your and other LG Cell Phone Accessories at great deals, which you won't find at other retailers. All orders for our LG Cell Phone Accessories are backed by our 100% Quality Assurance Guarantee."
					case 23164
						GRPNAME = "Blackberry Batteries - Lithium-ion Battery for Blackberry Storm 9530"
						strItemLongDetail_2 = "Buy your Blackberry Batteries for the Blackberry Storm 9530 and other Blackberry Batteries from the #1 cell phone accessories store online  CellularOutfitter. We offer the widest range of the best Blackberry batteries which you won't find at other retailers. Order now and get great deals on Blackberry Accessories backed by our 100% Quality Assurance Guarantee."
					case 22618
						GRPNAME = "Palm Treo Pro Faceplates - Screen Protector Film - Palm Treo Accessories"
						strItemLongDetail_2 = "Buy you Palm Treo Faceplates/Pro Screen Protector Film and other Palm cell phone accessories from the #1 cell phone accessories store online  CellularOutfitter. We offer the widest range of the best Palm Treo Faceplates for your Palm Treo Pro cell phone which you won't find at other retailers. Order now and get great deals on Palm Treo Cell Phone Accessories backed by our 100% Quality Assurance Guarantee."
					case 10574
						GRPNAME = "LG VX 8300 Holsters/Belt Clips - LG Cell Phone Accessories"
						strItemLongDetail_2 = "Buy your LG Belt Clip Holster and other LG Cell Phone Accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best holsters/belt clips for your LG cell phones, which you won't find at other retailers. Order now and get great deals on LG Cell Phone Accessories backed by our 100% Quality Assurance Guarantee."
					case 24245
						GRPNAME = "Mega Clip Neoprene Carrying Case/Pouch for Motorola Tundra W760r/VA76r - Motorola Cell Phone Accessories"
						strItemLongDetail_2 = "Buy the Motorola Cell Phone Leather Case/Pouch other Motorola Cell Phone Accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best Motorola leather cases for your Motorola cell phones, which you won't find at other retailers. Order now and get great deals on Motorola Cell Phone Accessories backed by our 100% Quality Assurance Guarantee."
					case 22059
						GRPNAME = "Samsung Rugby A837 Holsters/Belt Clips - Samsung Cell Phone Accessories"
						strItemLongDetail_2 = "Buy your Samsung Belt Clip Holsters for your Samsung Cell Phones and other Samsung Cell Phone Accessories from the #1 cell phone accessory store online  CellularOutfitter.  Check out our huge collection of the finest quality Samsung holsters/belt clips for your Rugby at the largest cell phone accessory store online. Order now and get great deals on Samsung Cell Phone Accessories backed by our 100% Quality Assurance Guarantee."
					case 21120
						GRPNAME = "Heavy-Duty Ballistic Nylon Carrying Case/Pouch for Verizon G'zone Boulder - Audiovox Accessories"
						strItemLongDetail_2 = "Buy the Heavy-Duty Ballistic Nylon Carrying Case/Pouch for Verizon G'zone Boulder and other Audiovox Leather Cases from the #1 cell phone accessory store online  CellularOutfitter. Check out our huge collection of quality Audiovox leather cases for Verizon G'zone Boulder at the largest cell phone accessory store online. Order now and get great deals on Audiovox Accessories backed by our 100% Quality Assurance Guarantee."
					case 21144
						GRPNAME = "Blackberry Curve 8300/8310/8320 Leather Carrying Case - Blackberry Curve Accessories"
						strItemLongDetail_2 = "Buy your Blackberry Leather Cases and other Blackberry Accessories from the #1 cell phone accessory store online  CellularOutfitter. Check out our huge collection of the best quality Blackberry leather cases for your Blackberry Curve at the largest cell phone accessory store online. Order now and get great deals on Blackberry Accessories backed by our 100% Quality Assurance Guarantee."
					case 23899
						GRPNAME = "Soft Touch Stylus Pen for LG Incite CT810 - LG Cell Phone Accessories"
						strItemLongDetail_2 = "Buy your LG Stylus Pen for LG your LG Cell Phones and other LG Cell Phone Accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best LG antennas for your LG Incite cell phone, which you won't find at other retailers. Order now and get great deals on LG Cell Phone Accessories backed by our 100% Quality Assurance Guarantee."
					case 21052
						GRPNAME = "Retractable-Cord Rapid IC Car Charger for Blackberry Bold 9000 - Blackberry Bold Charger"
						strItemLongDetail_2 = "Buy your Blackberry Chargers for your Blackberry Bold 9000 and other Blackberry Accessories from the #1 cell phone accessory store online  CellularOutfitter. We offer the widest range of the best Blackberry Chargers for your Blackberry Bold 9000. Order now and get great deals on Blackberry Accessories backed by our 100% Quality Assurance Guarantee."
					
					' Added 1/7/2010
					case 29942
						GRPNAME = "Blackberry Curve 8330 Antennas - OEM Blackberry Track Ball Replacement"
						strItemLongDetail_2 = "Buy OEM BlackBerry Track Ball Replacement and other Other BlackBerry 8330 Antennas and Accessories at #1 Cell Phone Antennas Store - CellularOutfitter.com"
					case 33412
						GRPNAME = "HTC Hero USB Data Transfer Cable - HTC Hero Accessories"
						strItemLongDetail_2 = "Buy Quality USB Data Transfer cable, Other HTC Hero Data Cables,  Memory and other HTC Hero Accessories at the lowest prices only @ CellularOutffiter.com"
					case 32074
						GRPNAME = "Blackberry Curve 8330 Antennas "
						strItemLongDetail_2 = "Buy High Quality BlackBerry Curve Accessories like the BlackBerry Curve 8330 Tracking Ball Replacement w/Ring at the best prices only at CellularOutfitter.com"
					case 31222
						GRPNAME = "HTC myTouch Leather Cases - HTC myTouch Siicon Skin Case (White)"
						strItemLongDetail_2 = "Buy the latest HTC myTouch Leather Cases like the HTC myTouch Silicon Skin Case (White) from the #1 Store for Cell Phone Leather Cases - CellularOutfitter.com"
					case 32222
						GRPNAME = "BlackBerry Curve 8520 Leather Cases - Rubberized Snap On Case"
						strItemLongDetail_2 = "At CellularOutfitter, find the best quality Blackberry Leather Cases like Rubberized Snap on Case fo the BlackBerry Curve at highly discounted prices!"
					case 21988
						GRPNAME = "ipod touch Wall Charger - iPod Touch Accessories"
						strItemLongDetail_2 = "Find Exclusive iPod Touch Accessories like iPod Touch Wall Chargers and other iPod Touch Accessories at Discounted Prices only at CellularOutfitter.com"
					case 11960
						GRPNAME = "iPod Hands-Free - iPod Accessories"
						strItemLongDetail_2 = "Find quaity iPod Accessories like iPod Handsfree at super discounted rates at the #1 Cell Phone Accessories Store Online - CellularOutfitter.com"
					case 29911
						GRPNAME = "BlackBerry Pearl Antennas"
						strItemLongDetail_2 = "CellularOutfitter.com offers a wide range of Quality BlackBerry Pearl 8100 Antennas and other such Cell Phone Accessories at Discounted Prices! Hurry!"
					case 31967
						GRPNAME = "HTC Touch Pro2 Leather Cases"
						strItemLongDetail_2 = "Find the best deals on HTC Touch Pro2 Accessories like HTC Touch Pro2 Leather Cases and other High Quality Cell Phone Accessories only at CellularOutfitter.com"
					case 21191
						GRPNAME = "LG Vu USB Data Transfer Cable - LG Vu Accessories"
						strItemLongDetail_2 = "CellularOutfitter.com offers a wide range of Quaity LG Vu Accessories like LG Vu USB Data Transfer Cable and other LG Cell Phone Accessories at Super Discounted Rates! Hurry!"
					case 20312
						GRPNAME = "LG VX 8100 Data Cables - LG Cell Phone Accessories"
						strItemLongDetail_2 = "Find Awesome LG Cell Phone Accessories like the LG VX Data Cables and other such marvellous cell phone accessories only at CellularOutfitter.com"
					case 11749
						GRPNAME = "iPhone - iPod Hands-free Stereo Headphones"
						strItemLongDetail_2 = "Exclusive iPhone-iPod Hands-free Stereo Headphones of the Top Quality at discounted prices @ CellularOutfitter.com"
					case 24468
						GRPNAME = "iPod Touch Leather Cases"
						strItemLongDetail_2 = "Find Awesome iPod Touch Accessories like the iPod Touch Leather cases and other such marvellous cell phone accessories only at CellularOutfitter.com"
					case 24470
						GRPNAME = "iPhone - iPod Audio Adapter"
						strItemLongDetail_2 = "Grab the best deals on iPhone/iPod Accessories like iPhone Audio Adapter and other Cell Phone Accessories @ CellularOutfitter - the #1 iPhone/ iPod Accessories Store Online!"
					case 24537
						GRPNAME = "LG Rumor Leather Cases - LG Rumor Accessories"
						strItemLongDetail_2 = "Find Quality LG Rumor LX 260 Leather Cases and other LG Rumor Accessories at the #1 Cell Phone Accessories Store Online - CellularOutfitter.com"
					case 28878
						GRPNAME = "Samsung Cell Phone Headsets"
						strItemLongDetail_2 = "Grab the best deals on Samsung Cell Phone Accessories like Samsung Cell Phone Headsets and other Cell Phone Accessories at the #1 Cell Phone Accessories Store Online - CellularOutfitter.com"
					case 28913
						GRPNAME = "Original LG Shine Chargers - Car Chargers"
						strItemLongDetail_2 = "Find the best deals on LG Shine Accessories lile LG Shine Car Chargers ant the #1 Cell Phone Accessories Store Online - CellularOutfitter.com"
					case 33651
						GRPNAME = "HTC Hero Leather Cases -  HTC Accessories"
						strItemLongDetail_2 = "Grab the best deals on Quality HTC Hero Leather Cases and other HTC Accessories only at CellularOutfitter.com "
					case 33653
						GRPNAME = "HTC Hero Leather Cases -  HTC Accessories"
						strItemLongDetail_2 = "Grab the best deals on Quality HTC Hero Leather Cases and other HTC Hero Accessories only at the #1 Cell Phone Accessories Store Online - CellularOutfitter.com"
					case 33902
						GRPNAME = "LG CF360 Faceplates - Colourful Flowers snap on Protector Case"
						strItemLongDetail_2 = "Find Awesome LG CF360 Accessories like the LG CF360 Faceplates and other such marvellous cell phone accessories only at CellularOutfitter.com"
					case 27915
						GRPNAME = "Samsung Magnet Data Cable & Memory - USB Data Transfer Cable"
						strItemLongDetail_2 = "Highly Reliable Samsung Magnet USB Data Transfer Cable of the best quality only at CellularOutfitter.com at Discounted Prices! Hurry!"
					case 31958
						GRPNAME = "HTC Touch Pro2 Holsters"
						strItemLongDetail_2 = "CellularOutfitter.com offers a wide range of Quaity HTC Touch Pro2 Accessories like HTC Touch Pro2 Belt Clip Holsters and other HTC Accessories at Super Discounted Rates! Hurry!"
					case 31979
						GRPNAME = "HTC Dash 3G Faceplates - Black Girrafe Snap-on Protector Case "
						strItemLongDetail_2 = "Shop online for HTC Dash 3G Accessories like the HTC Dash 3G Black giraffe Snap-on Protector Case and more at the #1  HTC Accessories Store Online - CellularOutfitter.com"
					case 32002
						GRPNAME = "Samsung Magnet Faceplates - Pink Butterflies & Flowers Snap-on Protector Case"
						strItemLongDetail_2 = "Shop online at the Market Leader in Cell Phone Accessories - CellularOutfitter.com for the best Samsung Magnet Faceplates and other Samsung Cell Phone Accessories at Super Discounted Rates!"
					case 32016
						GRPNAME = "HTC myTouch Leather Cases - Crystal Clear Skin Case"
						strItemLongDetail_2 = "Find the latest HTC myTouch Leather Case like the Crystal Clear Skin Case for your HTC myTouch Cell Phone at the #1 Cell Phone Accessories store - CellularOutfitter.com"
					case 32046
						GRPNAME = "HTC myTouch Faceplates - Red Snap-on Protector Case"
						strItemLongDetail_2 = "Buy Top Quality HTC myTouch Faceplates like the HTC myTouch 3G Red Snap-on Protector Case at the #1 Store Online for HTC Accessories - CellularOutfitter.com"
					case 32069
						GRPNAME = "Audiovox Gzone Accessories - Audiovox Gzone Batteries"
						strItemLongDetail_2 = "Shop online at the Market Leader in Cell Phone Accessories - CellularOutfitter for Audiovox Gzone Batteries and more at Discounted Prices"
					case 20285
						GRPNAME = "Samsung SGH T439 Data Cable & Memory - Samsung SGH Accessories"
						strItemLongDetail_2 = "Find the latest Samsung SGH Accessories like the Samsung SGH T439 Data Cable and Memory at Super Discounted Prices only at the Online Market Leader in Cell Phone Accessories"
					case 19247
						GRPNAME = "LG Rumor Faceplates - Love Cats Snap-on Protector Case"
						strItemLongDetail_2 = "Quality LG Rumor LX 260 Faceplates and other LG Rumor Accessories at the #1 Cell Phone Accessories Store Online - CellularOutfitter.com"
					case 20405
						GRPNAME = "iPod Touch Leather Case - Vertical  Sleeve Carrying Case"
						strItemLongDetail_2 = "Find Awesome iPod Touch Accessories like the iPod Touch Leather Vertical Sleeve Carrying Case and other such marvellous cell phone accessories only at CellularOutfitter.com"
					
					' Added 1/8/2010
					case 31226
						GRPNAME = "LG Neon Faceplates - LG Neon Screen Protector Film"
						strItemLongDetail_2 = "Grab the best deals on LG Neon Accessories like LG Neon Faceplates at the #1 Store online for Cell Phone Accessories - CellularOutfitter.com"
					case 31215
						GRPNAME = "LG env3 Leather Cases - LG env3 Silicon Case"
						strItemLongDetail_2 = "Buy the LG env3 Silicon Case and other LG Cell Phone Accessories  at unbeatable prices at the #1 Cell Phone Accessory Store online - CellularOutfitter.com"
					case 30861
						GRPNAME = "HTC Ozone Batteries - HTC Accessories"
						strItemLongDetail_2 = "Exclusive HTC Ozone Batteries and other HTC Ozone Accessories of the best quality at discounted prices @ CellularOutfitter.com"
					case 30888
						GRPNAME = "iPhone 3G/3GS Leather Case - iPhone Accessories"
						strItemLongDetail_2 = "Buy Quality iPhone 3G/3GS Leather Cases and Other iPhone Accessories at the lowest prices only @ CellularOutfitter.com"
					case 30890
						GRPNAME = "iPhone 3G/3GS Snap-on Protector Case - iPhone Accessories"
						strItemLongDetail_2 = "Buy Quality iPhone 3G/3GS Faceplates and Other iPhone Accessories at the lowest prices only @ CellularOutfitter.com"
					case 30891
						GRPNAME = "iPhone 3G/3GS Snap-On Protector Case - iPhone Faceplates"
						strItemLongDetail_2 = "Buy Quality iPhone 3G/3GS Leather Cases and Other iPhone Accessories at the lowest prices only @ CellularOutfitter.com"
					case 31234
						GRPNAME = "iPhone 3G/3GS Snap-On Protector Case - iPhone Leather Cases"
						strItemLongDetail_2 = "Buy Quality iPhone 3G/3GS Leather Cases and Other iPhone Accessories at the lowest prices only @ CellularOutfitter.com"
					case 29910
						GRPNAME = "BlackBerry Pearl Antennas - BlackBerry Pearl Accessories"
						strItemLongDetail_2 = "Shop online for BlackBerry Pearl Accessories  like BlackBerry Pearl Antennas more at the #1  Blackberry Accessories Store Online - CellularOutfitter.com"
					case 29935
						GRPNAME = "BlackBerry Curve Leather Case - BlackBerry Curve Accessories"
						strItemLongDetail_2 = "Shop online for BlackBerry Curve Accessories  like BlackBerry Curve Leather Case and more at the #1  Blackberry Accessories Store Online - CellularOutfitter.com"
					case 29941
						GRPNAME = "BlackBerry Bold Chargers - BlackBerry Accessories"
						strItemLongDetail_2 = "Grab the best deals on BlackBerry Bold Accessories like BlackBerry Bold Chargers and other Cell Phone Accessories @ CellularOutfitter - the #1  BlackBerry Accessories Store Online!"
					case 29952
						GRPNAME = "iPhone Leather Cases - iPhone Delux Leather Vertical Carrying Case"
						strItemLongDetail_2 = "Find Awesome iPhone Accessories like the iPhone Leather Cases and other such marvellous cell phone accessories only at CellularOutfitter.com"
					case 29953
						GRPNAME = "iPhone Leather Cases - iPhone Cell Armor Deluxe Leather Vertical Carrying Case"
						strItemLongDetail_2 = "Shop online for iPhone Accessories like iPhone Leather Cases and more at the #1  iPhone Accessories Store Online - CellularOutfitter.com"
					case 30161
						GRPNAME = "LG Xenon Pink Zebra Snap-On Protector Case - LG Cell Phone Faceplates"
						strItemLongDetail_2 = "Grab the best deals on LG Cell Phone Accessories like LG Xenon Faceplates and other Cell Phone Accessories @ CellularOutfitter - the #1 LG Cell Phone Accessories Store Online!"
					case 27869
						GRPNAME = "Samsung Impression SGH Leather Cases - Samsung Cell Phone Accessories"
						strItemLongDetail_2 = "Find the latest Samsung SGH Accessories like the Samsung SGH T439 Leather Case at Super Discounted Prices only at the Online Market Leader in Cell Phone Accessories - CellularOutfitter.com"
					case 33186
						GRPNAME = "Samsung Solstice Faceplates - Samsung Cell Phone Accessories "
						strItemLongDetail_2 = "Find Quality Samsung Solstice Faceplates and other Samsung Solstice Accessories at the #1 Cell Phone Accessories Store Online - CellularOutfitter.com"
					case 33750
						GRPNAME = "BlackBerry Storm2 Leather Cases - BlackBerry Storm2 Accessories"
						strItemLongDetail_2 = "Grab the best deals on BlackBerry Storm2 Accessories like BlackBerry Storm2 Leather Cases and other Cell Phone Accessories @ CellularOutfitter - the #1 BlackBerry Accessories Store Online!"
					case 33758
						GRPNAME = "iPod Touch Star Blast Snap-On Protector Case - iPod Touch Accessories "
						strItemLongDetail_2 = "Shop online for iPod Touch Accessories like the iPod Touch Snap-on Protector Case and more at the #1  iPod Touch Accessories Store Online - CellularOutfitter.com"
					case 33798
						GRPNAME = "BlackBerry Storm2 Finger Touch Stylus - BlackBerry Storm2 Accessories"
						strItemLongDetail_2 = "Find the latest BlackBerry Storm2 Accessories like the BlackBerry Storm2 Finger Touch Stylus at Super Discounted Prices only at the Online Market Leader in Cell Phone Accessories - CellularOutfitter.com"
					case 33805
						GRPNAME = "BlackBerry Storm2 Faceplates - BlackBerry Storm2 Accessories"
						strItemLongDetail_2 = "Find Quality BlackBerry Storm2 Faceplates and other BlackBerry Storm2 Accessories at the #1 Cell Phone Accessories Store Online - CellularOutfitter.com"
					case 33562
						GRPNAME = "Samsung Rogue Faceplates - Samsung Cell Phone Faceplates"
						strItemLongDetail_2 = "At CellularOutfitter, find the best quality Samsung Rogue Faceplates like the Samsung Rogue Hot Pink Plaid Snap-On Protector Case at highly discounted prices!"
					case 33615
						GRPNAME = "BlackBerry Tour Full Body Protector - Invisible Gadget Guard"
						strItemLongDetail_2 = "Shop online for BlackBerry Tour Accessories like the BlackBerry Tour Invisible Gadget Guard more at the #1  BlackBerry Accessories Store Online - CellularOutfitter.com"
					case 33727
						GRPNAME = "BlackBerry Storm2 Leather Cases -BlackBerry Storm2 Soho Kroo Leather Case"
						strItemLongDetail_2 = "At CellularOutfitter, find the best quality Blackberry Leather Cases like the Soho Kroo Leather Carrying Case fo the BlackBerry Curve at highly discounted prices!"
					case 33872
						GRPNAME = "Blackberry Tour Snap-On Protector - BlackBerry Tour Faceplates"
						strItemLongDetail_2 = "Buy Quality BlackBerry Tour Faceplates and Other BlackBerry Tour Accessories at the lowest prices only @ CellularOutffiter.com"
					case 33896
						GRPNAME = "LG env3 Snap-On Protector Case - LG env3 Faceplates"
						strItemLongDetail_2 = "Find Awesome LG env3 Accessories like the LG env3 Faceplates and such marvellous cell phone accessories only at CellularOutfitter.com"
					case 9634
						GRPNAME = "LG VX Pink Neoprene Case - LG VX Leather Case"
						strItemLongDetail_2 = "At CellularOutfitter, find the best quality LG VX Leather Cases like the Pink Neoprene Case and other LG VX Accessories at discounted prices!"
					case 33399
						GRPNAME = "Samsung Intensity SCH Snap-On Protector Case - Samsung SCH Faceplates"
						strItemLongDetail_2 = "Find Quality Samsung Intensity SCH Leather Cases and other Samsung SCH Faceplates at the #1 Cell Phone Accessories Store Online - CellularOutfitter.com"
					case 33723
						GRPNAME = "BlackBerry Storm2 Batteries - BlackBerry Storm2 Accessories"
						strItemLongDetail_2 = "Buy Quality BlackBerry Storm2 Batteries and other BlackBerry Storm2 Accessories at the lowest prices only @ CellularOutfitter.com"
					case 31718
						GRPNAME = "HTC myTouch Silicon Skin Case - HTC myTouch Accessories"
						strItemLongDetail_2 = "Grab the best deals on HTC myTouch Accessories like HTC myTouch Faceplates and other Cell Phone Accessories @ CellularOutfitter - the #1 HTC Accessories Store Online!"
					case 29926
						GRPNAME = "Pantech Matrix Batteries - Pantech Matrix Accessories"
						strItemLongDetail_2 = "Buy Quality Pantech Matrix Batteries and other Pantech Matrix Accessories at the lowest prices only @ CellularOutfitter.com"
					case 20284
						GRPNAME = "Samsung SPH Data Cable and Memory - Samsung SPH Accessories"
						strItemLongDetail_2 = "Find Awesome Samsung SPH Accessories like the Samsung SPH Data Cable and Memory and other such marvellous cell phone accessories only at CellularOutfitter.com"
					case else
						'strItemLongDetail_2 = "Buy " & RS("itemDesc_CO") & " and other " & BrandName & " " & nameSEO(stypeName)
						'strItemLongDetail_2 = strItemLongDetail_2 & " from the #1 cell phone accessory store online  CellularOutfitter. This "
						'strItemLongDetail_2 = strItemLongDetail_2 & sProductName & " is a quality " & BrandName & " " & ModelName & " " & LCase(stypeName_nos)
						'strItemLongDetail_2 = strItemLongDetail_2 & " at an unbeatable price. We offer the best " & LCase(nameSEO(stypeName)) & " for your "
						'strItemLongDetail_2 = strItemLongDetail_2 & BrandName & " " & ModelName & " cell phone, which you won't find at other retailers. "
						'strItemLongDetail_2 = strItemLongDetail_2 & "Check out our huge collection of quality " & trim(LCase(nameSEO(stypeName))) & " for "
						'strItemLongDetail_2 = strItemLongDetail_2 & ModelName & " and get great deals on " & LCase(nameSEO(stypeName)) & " for " & ModelName
						'strItemLongDetail_2 = strItemLongDetail_2 & " at the largest cell phone accessory store online. All orders for " & sProductName
						'strItemLongDetail_2 = strItemLongDetail_2 & " are backed by our 100% Quality Assurance Guarantee."
						
						strItemLongDetail_2 = "Get great offers on quality " & BrandName & " " & singularSEO(stypeName) & " to complement your "
						strItemLongDetail_2 = strItemLongDetail_2 & BrandName & " " & ModelName & ". Huge collection of "
						strItemLongDetail_2 = strItemLongDetail_2 & BrandName & " " & nameSEO(stypeName) & " for "
						strItemLongDetail_2 = strItemLongDetail_2 & BrandName & " " & ModelName & " is available at #1 cell phone accessory store online. "
						strItemLongDetail_2 = strItemLongDetail_2 & "Take a look around our website to find " & BrandName & " " & singularSEO(stypeName)
						strItemLongDetail_2 = strItemLongDetail_2 & " that best suits your " & BrandName & " " & ModelName & "."
				end select
				
				id_new_field = formatSEO(GRPNAME)
				priceCO = formatNumber(cdbl(RS("price_CO")))
				PriceRetail = formatNumber(cdbl(RS("price_Retail")))
				
				if inStr(Lcase(strItemLongDetail),"refurbished") > 0 or inStr(Lcase(RS("itemDesc_CO")),"refurbished") > 0 then
					Condition = "refurbished"
				else
					Condition = "new"
				end if
				
				strline = GRPNAME & vbtab
				strline = strline & chr(34) & strItemLongDetail & chr(34) & vbtab
				strline = strline & chr(34) & strItemLongDetail_2 & chr(34) & vbtab
				strline = strline & RS("itemid") & vbtab
				strline = strline & "http://www.CellularOutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html?source=gbase" & vbtab
				strline = strline & "1000" & vbtab
				strline = strline & "http://www.CellularOutfitter.com/productpics/big/" & RS("itempic_CO") & vbtab
				strline = strline & priceCO & vbtab
				strline = strline & PriceRetail & vbtab  
				strline = strline & nameSEO(stypeName) & vbtab  
				strline = strline & BrandName & vbtab
				strline = strline & ModelName & vbtab
				strline = strline & Condition & vbtab
				strline = strline & "Visa,MasterCard,AmericanExpress,Discover" & vbtab
				'strline = strline & expDate & vbtab
				strline = strline & RS("MPN") & vbtab
				strline = strline & mycolor & vbtab ' need filter out from description
				strline = strline & year(date) & vbtab
				strline = strline & "http://www.CellularOutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html" ' GAN URL
				file.WriteLine strline
			end if
			RS.MoveNext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
