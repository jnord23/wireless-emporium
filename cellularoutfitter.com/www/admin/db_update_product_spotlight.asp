<%
pageTitle = "Admin - Update CO Database - Update Product Spotlight"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->

<h3>Update CO Database - Update Product Spotlight</h3>

<%
strError = ""
if request.form("submitted") = "Update" then
	for a = 1 to 4
		itemID = request.form("itemID" & a)
		if itemID = "" then strError = strError & "You must enter an Item ID in all 4 spots.<br>"
		if not isNumeric(itemID) then strError = strError & "You must enter valid Item ID in spot " & a & ".<br>"
		if strError = "" then
			SQL = "UPDATE CO_spotlight SET"
			SQL = SQL & " itemID = '" & itemID & "',"
			SQL = SQL & " sortOrder = '" & a & "'"
			SQL = SQL & " WHERE id = '" & request.form("id" & a) & "'"
			response.write "<p>" & SQL & "</p>"
			oConn.execute SQL
		end if
	next
	if strError <> "" then
		response.write "<p>" & strError & "</p>" & vbcrlf
	else
		response.write "<h3>UPDATED!</h3>" & vbcrlf
	end if
else
	SQL = "SELECT * FROM CO_spotlight ORDER BY sortOrder"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	%>
	<table border="1" cellpadding="3" cellspacing="0" align="center" class="smlText">
		<form action="db_update_product_spotlight.asp" method="post">
		<%
		do until RS.eof
			a = a + 1
			%>
			<tr>
				<td valign="top" align="center"><b>itemID #<%=a%></b></td>
				<td valign="top" align="center">
					<input type="hidden" name="id<%=a%>" value="<%=RS("id")%>">
					<input type="text" name="itemID<%=a%>" value="<%=RS("itemID")%>" maxlength="5">
				</td>
			</tr>
			<%
			RS.movenext
		loop
		%>
		<tr>
			<td align="center" colspan="2">
				<input type="submit" name="submitted" value="Update">
			</td>
		</tr>
		</form>
	</table>
	<p>&nbsp;</p>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
