set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

'username = "cellularoutfitter"	
'Hostname = "ftp.bloomreach.com"
'Password = "d3sWupHu"
'port = 21
'set sftp = CreateObject("Chilkat.SFtp")
'success = sftp.UnlockComponent("WIRELESSH_zHJQ3Uch8Hni")
''msgbox("1:" & success)
'success = sftp.Connect(hostname,port)
''msgbox("2:" & success)
'success = sftp.AuthenticatePw(username,password)
''msgbox("3:" & success)
'success = sftp.InitializeSftp()
''msgbox("4:" & success)

dim fs, file, filename, path, folderName
set fs = CreateObject("Scripting.FileSystemObject")
dim oXMLHTTP : set oXMLHTTP = CreateObject("Msxml2.ServerXMLHTTP.3.0")
set file = nothing

nRows = 0
filename = "bloomreach.txt"
folderName = "c:\inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\"
imgAbsolutePath = "c:\inetpub\wwwroot\productpics_co\"
path = folderName & filename

if fs.FileExists(path) then fs.DeleteFile(path)

set file = fs.CreateTextFile(path)

dim SQL, RS
dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline, itemDesc
sql	=	"select	isnull(a.typeid, 0) typeid, d.typeName_co, a.itemid, a.partnumber" & vbcrlf & _
		"	,	isnull(a.itemdesc_co, '') itemdesc_co" & vbcrlf & _
		"	,	convert(varchar(8000), " & vbcrlf & _
		"		dbo.fn_stripHTML(" & vbcrlf & _
		"		replace(" & vbcrlf & _
		"		replace(" & vbcrlf & _
		"		replace(" & vbcrlf & _
		"			dbo.fn_decodeHtml(a.itemlongdetail_co) " & vbcrlf & _
		"			+	case when nullif(a.point1, '') is not null then ' - ' + a.point1 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.point2, '') is not null then ' - ' + a.point2 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.point3, '') is not null then ' - ' + a.point3 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.point4, '') is not null then ' - ' + a.point4 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.point5, '') is not null then ' - ' + a.point5 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.point6, '') is not null then ' - ' + a.point6 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.point7, '') is not null then ' - ' + a.point7 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.point8, '') is not null then ' - ' + a.point8 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.point9, '') is not null then ' - ' + a.point9 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.point10, '') is not null then ' - ' + a.point10 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(convert(varchar(2000), a.compatibility), '') is not null then ' - COMPATIBILITY: ' + convert(varchar(2000), a.compatibility) + '.' else '' end" & vbcrlf & _
		"		,	char(10), ' ')" & vbcrlf & _
		"		,	char(13), ' ')" & vbcrlf & _
		"		,	char(9), ' '))) itemlongdetail_co" & vbcrlf & _
		"	,	a.itempic_co, a.price_co, a.price_retail, case when e.alwaysinstock = 1 then 999 else w.inv_qty end inv_qty" & vbcrlf & _
		"	,	isnull(a.brandid, 0) brandid, isnull(a.modelid, 0) modelid, isnull(convert(varchar(1000), b.brandname), 'Universal') brandname, isnull(convert(varchar(1000), c.modelname), 'Universal') modelname" & vbcrlf & _
		"	, 	isnull(f.color, '') color, a.datetimeentd, isnull(a.smartphone, 0) smartphone" & vbcrlf & _
		"from	we_items a with (nolock) left outer join we_brands b with (nolock)" & vbcrlf & _
		"	on	a.brandid = b.brandid left outer join we_models c with (nolock)" & vbcrlf & _
		"	on	a.modelid = c.modelid join we_types d" & vbcrlf & _
		"	on	a.typeid = d.typeid left outer join we_pnDetails e" & vbcrlf & _
		"	on	a.partnumber = e.partnumber left outer join xproductcolors f" & vbcrlf & _
		"	on	a.colorid = f.id join we_items w" & vbcrlf & _
		"	on	a.partnumber = w.partnumber and w.master = 1" & vbcrlf & _
		"where	a.hidelive = 0 and (w.inv_qty > 0 or e.alwaysinstock = 1)" & vbcrlf & _
		"	and a.price_co > 0	" & vbcrlf & _
		"	and a.itemdesc_co is not null and a.itemdesc_co <> ''" & vbcrlf & _
		"	and	a.partnumber not like 'WCD%' and a.partnumber not like 'DEC%'" & vbcrlf & _
		"order by a.itemid desc"

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

strHeadings	 = 	"pid" & vbTab & "launch_date" & vbTab & "leaf_categories" & vbTab & "google_category" & vbTab & "title" & vbTab & "price" & vbTab & "description" & vbTab & "URL" & vbTab & "large_image" & vbTab & "thumb_image" & vbTab & "Bread Crumb" & vbTab & "Availability" & vbTab & "Brand" & vbTab & "Color" & vbTab & "Shipping Info" & vbTab & "modelid" & vbTab & "modelname"
file.WriteLine strHeadings

if not RS.eof then
	do until RS.eof
		typeid = rs("typeid")
		brandid = rs("brandid")
		modelid = rs("modelid")
		itempic = rs("itempic_co")
		brandName = rs("brandName")
		modelName = rs("modelName")
		categoryName = rs("typeName_co")
		color = rs("color")
		
		if fs.FileExists(imgAbsolutePath & "big\" & itempic) then
			itemid = rs("itemid")
			partnumber = rs("partnumber")
			itemdesc = rs("itemdesc_co")
			itemlongdetail = rs("itemlongdetail_co")
			price_our = rs("price_co")
			price_retail = rs("price_retail")
			inv_qty = rs("inv_qty")
'			productURL = "http://www.cellularoutfitter.com/p-" & itemid & "-" & formatSEO(itemdesc) & ".html?DZID=CSE_BloomReach_" & itemid & "&utm_source=BloomReach&utm_medium=CSE"
			productURL = "http://www.cellularoutfitter.com/p-" & itemid & "-" & formatSEO(itemdesc) & ".html"
			launch_date = rs("datetimeentd")
			smartphone = rs("smartphone")
			
			if fs.FileExists(imgAbsolutePath & "big\zoom\" & itempic) then
				bigImageURL = 	"http://www.cellularoutfitter.com/productpics/big/zoom/" & itempic
			else
				bigImageURL = 	"http://www.cellularoutfitter.com/productpics/big/" & itempic		
			end if
			
			if fs.FileExists(imgAbsolutePath & "thumb\" & itempic) then
				thumbImageURL = 	"http://www.cellularoutfitter.com/productpics/thumb/" & itempic		
			end if

			gpc = "Electronics > Communications > Telephony > Mobile Phone Accessories"
			if typeid = 3 or typeid = 7 then
				gpc = gpc & " > Mobile Phone Cases"
			elseif typeid = 6 then
				gpc = gpc & " > Mobile Phone Stands"
			elseif typeid = 16 then
				gpc = "Electronics > Communications > Telephony > Mobile Phones"
				if smartphone then
					gpc = gpc & " > Smartphones"
				else
					gpc = gpc & " > Feature Phones"
				end if
			end if

			strLine = itemid & vbTab 			
			strLine = strLine & launch_date & vbTab
			strLine = strLine & typeid & "," & brandid & "," & modelid & vbTab
			strLine = strLine & gpc & vbTab
			strLine = strLine & itemdesc & vbTab 
			strLine = strLine & price_our & vbTab
			strLine = strLine & itemlongdetail & vbTab
			strLine = strLine & productURL & vbTab 'Manufacturer Item URL

'			strAltViews = ""
'			for iCount = 0 to 7
'				altPic = replace(itempic,".jpg","-" & iCount & ".jpg")
'				if fs.FileExists(imgAbsolutePath & "altviews\zoom\" & altPic) then
'					if strAltViews = "" then
'						strAltViews = "http://www.wirelessemporium.com/productpics/altviews/zoom/" & altPic
'					else
'						strAltViews = strAltViews & ",http://www.wirelessemporium.com/productpics/altviews/zoom/" & altPic						
'					end if
'				elseif fs.FileExists(imgAbsolutePath & "altviews\" & altPic) then
'					if strAltViews = "" then
'						strAltViews = "http://www.wirelessemporium.com/productpics/altviews/" & altPic
'					else
'						strAltViews = strAltViews & ",http://www.wirelessemporium.com/productpics/altviews/" & altPic
'					end if
'				end if
'			next
'			if strAltViews <> "" then bigImageURL = bigImageURL & "," & strAltViews
			
			strLine = strLine & bigImageURL & vbTab 'Item Images
			strLine = strLine & thumbImageURL & vbTab 'Item Images
			strLine = strLine & brandName & "|" & modelName & "|" & categoryName & vbTab
			strLine = strLine & inv_qty & vbTab
			strLine = strLine & brandName & vbTab
			strLine = strLine & color & vbTab
			strLine = strLine & "5.99" & vbTab
			strLine = strLine & modelid & vbTab
			strLine = strLine & modelname
	
			file.WriteLine strLine
		end if
		rs.movenext
	loop
end if
file.close()
set file = nothing

'handle = sftp.OpenFile("/" & filename,"writeOnly","createTruncate")
'If (handle = vbNullString ) Then
'    msgbox(sftp.LastErrorText)
''    response.End()
'End If
'
'success = sftp.UploadFile(handle,path)
'success = sftp.CloseHandle(handle)

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(replace(replace(formatSEO, "*", "-"),"?","-"), "----", "-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function


call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub