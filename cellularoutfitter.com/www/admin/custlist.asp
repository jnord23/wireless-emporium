<%
server.scripttimeout = 180
pageTitle = "Customer List"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/admin/aspdatetime.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<%
dim strDate, strOrderId, strCustId, strFname, strLName, strConfirm, strPhone, strEmail, PgNo, strEmailAddr
strDate = fnRetDateFormat(SQLquote(Request.Form("txtorderdate")),"mm/dd/yyyy")
strLName = SQLquote(Request.Form("txtLName"))
strFname = SQLquote(Request.Form("txtFName"))
strCustId = SQLquote(Request.Form("txtAccountId"))
strPhone = SQLquote(Request.Form("txtPhone"))
strOrderId = SQLquote(Request.Form("txtOrderId"))
strConfirm = SQLquote(Request.Form("txtConfirm"))
strEmail = SQLquote(Request.Form("txtEmail"))
strEmailAddr = SQLquote(Request.Form("txtEmailAddr"))
PgNo = SQLquote(Request.Form("txtpageno"))

if isnull(strOrderId) or len(strOrderId) < 1 then strOrderId = 0

Dim SQL, strFilter, strJoinClause
Dim PgSize, strWhere, RecCount, TotalPages
PgSize = 50
If PgNo = "" Then PgNo = 1
If Not ISNumeric(PgNo) Then PgNo = 1

	historical = 0

	if strOrderId > 0 then
		sql = "select store from we_orders where orderID = " & strOrderId
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 3, 3
		
		if rs.EOF then
			sql = "select store from we_orders_historical where orderID = " & strOrderId
			session("errorSQL") = sql
			Set rs = Server.CreateObject("ADODB.Recordset")
			rs.open sql, oConn, 3, 3
			
			historical = 1
		end if
		
		if rs.EOF then
			session("userMsg") = "No Order Found"
			response.Redirect("/admin/custlist.asp")
		else
			if rs("store") = 0 then useStore = "WE"
			if rs("store") = 1 then useStore = "CA"
			if rs("store") = 2 then useStore = "CO"
			if rs("store") = 3 then useStore = "PS"
			useStoreID = rs("store")
		end if
	elseif len(strEmailAddr) > 0 then
		sql = "select (select top 1 accountID from we_accounts where email = '" & strEmailAddr & "') as weAcct, (select top 1 accountID from ca_accounts where email = '" & strEmailAddr & "') as caAcct, (select top 1 accountID from co_accounts where email = '" & strEmailAddr & "') as coAcct, (select top 1 accountID from ps_accounts where email = '" & strEmailAddr & "') as psAcct"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 3, 3
		
		if not isnull(rs("weAcct")) then useStore = "WE" : useStoreID = 0
		if not isnull(rs("caAcct")) then useStore = "CA" : useStoreID = 1
		if not isnull(rs("coAcct")) then useStore = "CO" : useStoreID = 2
		if not isnull(rs("psAcct")) then useStore = "PS" : useStoreID = 3
	end if
	
	if isnull(useStore) or len(useStore) < 1 then useStore = "CO"
	if isnull(useStoreID) or len(useStoreID) < 1 then useStoreID = 2
	if strOrderId = 0 then strOrderId = ""

if request.querystring("display") <> "none" then
	SQL = "SELECT Distinct Top " & PgSize & " A.accountid, A.FName, A.LName, A.PHONE, A.EMAIL, A.PWORD From " & useStore & "_Accounts A "
	SQL = SQL & " LEFT JOIN WE_ORDERS O ON A.ACCOUNTID = O.ACCOUNTID"
	
	strWhere = " WHERE O.store = " & useStoreID
	
	if strCustId <> "" then strWhere = strWhere & " AND A.ACCOUNTID='" & strCustId & "'"
	if strLName <> "" then strWhere = strWhere & " AND A.LName LIKE '%" & strLName & "%'"
	if strFname <> "" then strWhere = strWhere & " AND A.FName LIKE '%" & strFname & "%'"
	if strPhone <> "" then strWhere = strWhere & " AND A.PHONE LIKE '%" & strPhone & "%'"
	if strOrderId <> "" then strWhere = strWhere & " AND O.ORDERID='" & strOrderId & "'"
	if strEmailAddr <> "" then strWhere = strWhere & " AND A.Email='" & strEmailAddr & "'"
	if strDate <> "" then strWhere = strWhere & " AND O.ORDERDATETIME >= '" & strDate & "'"
	
	if strConfirm = "YES" then
		strWhere = strWhere & " AND O.CONFIRMSENT = 'yes'"
	elseif strConfirm = "NO" then
		strWhere = strWhere & " AND (O.CONFIRMSENT IS NULL OR O.CONFIRMSENT <> 'yes')"
	end if
	
	if strEmail = "YES" then
		strWhere = strWhere & " AND O.APPROVED = 1"
	elseif strEmail = "NO" then
		strWhere = strWhere & " AND (O.APPROVED IS NULL OR O.APPROVED = 0)"
	end if
	
	dim strCountSQL
	strCountSQL = "SELECT Count(Distinct A.AccountId) From " & useStore & "_Accounts A "
	strCountSQL = strCountSQL & " LEFT JOIN WE_ORDERS O ON A.ACCOUNTID = O.ACCOUNTID"
	strCountSQL = strCountSQL & strWhere
	if PgNo <> 1 then
		SQL = SQL & " AND A.AccountId NOT IN"
		SQL = SQL & " (SELECT DISTINCT TOP " & (PgNo-1) * PgSize & " SA.accountid FROM " & useStore & "_Accounts SA"
		SQL = SQL & " LEFT JOIN WE_ORDERS SO ON SA.ACCOUNTID = SO.ACCOUNTID"
		SQL = SQL & replace(replace(Ucase(strWhere),"A.","SA."),"O.","SO.")
		SQL = SQL & " ORDER BY SA.ACCOUNTID)"
	end if
	SQL = SQL & strWhere & " ORDER BY A.ACCOUNTID"
	'response.write "<h3>" & SQL & "</h3>" & vbcrlf
	
	dim RS, objCmd
	set objCmd = Server.CreateObject("ADODB.COMMAND")
	set RS = Server.CreateObject("ADODB.RECORDSET")
	objCmd.CommandText = "CustomerLookup"
	objCmd.CommandType = 4
	objCmd.Parameters.Append objCmd.CreateParameter("@sqlSTR", 200, 1,4000,SQL)
	objCmd.Parameters.Append objCmd.CreateParameter("@strQryRecCnt", 200, 1,2000,strCountSQL)
	objCmd.Parameters.Append objCmd.CreateParameter("@RecCount", 20, 2)
	objCmd.ActiveConnection = oConn
	RS.CursorLocation = 3
	RS.Open objCmd,,3,1
	
	if RS.EOF then
		RS = null
		objCmd = null
		SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
		set objCmd = Server.CreateObject("ADODB.COMMAND")
		set RS = Server.CreateObject("ADODB.RECORDSET")
		objCmd.CommandText = "CustomerLookup"
		objCmd.CommandType = 4
		objCmd.Parameters.Append objCmd.CreateParameter("@sqlSTR", 200, 1,4000,SQL)
		objCmd.Parameters.Append objCmd.CreateParameter("@strQryRecCnt", 200, 1,2000,strCountSQL)
		objCmd.Parameters.Append objCmd.CreateParameter("@RecCount", 20, 2)
		objCmd.ActiveConnection = oConn
		RS.CursorLocation = 3
		RS.Open objCmd,,3,1
	end if
	RecCount = objCmd.Parameters("@RecCount").Value
	TotalPages = TISPL_fnReturnTotalPages(Cdbl(RecCount),Cdbl(PgSize))
end if
%>

<table width="95%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr class="top-text-black" bgcolor="#CCCCCC">
		<td colspan="7" width="100%">
			<table border="0" width="100%" class="top-text-black" bgcolor="#CCCCCC">
				<tr>
					<td colspan="20%" align="left"><a href="#" onClick="window.history.go(-1);"><< BACK</a></td>
					<td colspan="60%" align="center"> Customer Look-up Search Criteria </td>
					<td colspan="20%" align="right"><a href="#" onClick="window.history.go(1);">FORWARD >></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<form name="FRMSEARCH" method="post" action="custlist.asp">
	<tr>
		<td class="mc-text" colspan="14">
			<table border="0">
				<tr>
					<td class="mc-text">First Name : <input class="mc-text" type="text" name="txtfname" size="8" value="<%=strFname%>">&nbsp;</td>
					<td class="mc-text">Last Name : <input class="mc-text" type="text" name="txtlname" size="10" value="<%=strLName%>">&nbsp;</td>
					<td class="mc-text">Customer # : <input class="mc-text" type="text" name="txtaccountid" size="8" value="<%=strCustId%>">&nbsp;</td>
					<td class="mc-text">Phone # : <input class="mc-text" type="text" name="txtphone" size="8" value="<%=strPhone%>"><br></td>
					<td class="mc-text">Email Address : <input class="mc-text" type="text" name="txtEmailAddr" size="25" value="<%=Request.Form("txtEmailAddr")%>"><br></td>
				</tr>
				<tr>
					<td class="mc-text">&nbsp;&nbsp;&nbsp;Order Id : <input class="mc-text" type="text" name="txtorderid" size="8" value="<%=strOrderId%>">&nbsp;</td>
					<td class="mc-text">Order Date : <input class="mc-text" type="text" name="txtorderdate" size="10" maxlength="10" value="<%=strDate%>">&nbsp;</td>
					<td class="mc-text">Email Sent : <select class="mc-text" name="txtemail"><option value="">All</option><option value="YES"<%if strEmail="YES" then Response.Write(" selected")%>>Yes</option><option value="NO" <%If strEmail="NO" Then Response.Write("selected")%>>No</option></select> &nbsp;</td>
					<td class="mc-text" colspan="2">Confirm : <select class="mc-text" name="txtconfirm"><option value="">All</option><option value="YES"<%if strConfirm="YES" then Response.Write(" selected")%>>Yes</option><option value="NO" <%If strConfirm="NO" Then Response.Write("selected")%>>No</option></select></td>
				</tr>
				<tr>
					<td colspan="12" align="center"><input type="SUBMIT" value="SEARCH" class="mc-text" id="SUBMIT1" name="SUBMIT1">&nbsp;&nbsp;&nbsp;<input type="button" value="CLEAR" class="mc-text" onClick="resetAll();"><input type="hidden" name="txtpageno" value="1"></td>
				</tr>
				<tr>
					<td colspan="12" align="center" class="mc-text"><b>Note:</b> Search on Names and Phone number uses pattern matching and retrieves all records containing entered words.<br>Search on Other fields - OrderId, OrderDate, Customer # will look for records containing exact match.</td>
				</tr>
			</table>
		</td>
	</tr>
	<input type="hidden" name="txthidacctid" value="">
	</form>
	
	<%if request.querystring("display") <> "none" then%>
		<tr bgcolor="#CCCCCC" class="mc-text">
			<td valign="middle" colspan="7" align="right">
				Page No :<select name="selpage" class="mc-text" onChange="fngotopg(this.value);">
				<%
				for iLoop = 1 to TotalPages
					response.write "<option value='" & iLoop & "'"
					if cStr(PgNo) = cStr(iLoop) then response.write(" selected")
					response.write ">" & iLoop & "</option>" & vbcrlf
				next
				%>
			</select>
			</td>
		</tr>
		<tr bgcolor="#CCCCCC">
			<td class="top-text-black">&nbsp;Account Id</font></td>
			<td class="top-text-black">First Name</td>
			<td class="top-text-black">Last Name</td>
			<td class="top-text-black">Telephone</td>
			<td class="top-text-black">Email</td>
			<td class="top-text-black">Password</td>
            <td class="top-text-black">Site</td>
		</tr>
		<%
		if not RS.eof then
			do until RS.eof
				%>
				<tr bgcolor="<%if (RS.AbsolutePosition mod 2) = 0 then response.write("lightgrey")%>">
					<td class="mc-text"><a href="#" onClick="fngotoorderpg('<%=RS("ACCOUNTID")%>')"><%=RS("ACCOUNTID")%></a></td>
					<td class="mc-text"><%=RS("FName")%></td>
					<td class="mc-text"><%=RS("LName")%></td>
					<td class="mc-text"><%=RS("Phone")%></td>
					<td class="mc-text"><a href="mailto:<%=RS("Email")%>"><%=RS("Email")%></a></td>
					<td class="mc-text"><%=RS("Pword")%></td>
                    <td><%=useStore%></td>
				</tr>
				<%
				RS.movenext
			loop
		else
			%>
			<tr><td colspan="6" class="mc-text">No Records Found</td></tr>
			<%
		end if
		set objCmd = nothing
		RS.close
		set RS = nothing
	end if
	%>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function fngotopg(pgno){
		document.FRMSEARCH.reset();
		document.FRMSEARCH.txtpageno.value = pgno;
		document.FRMSEARCH.action = "custlist.asp"
		document.FRMSEARCH.submit();
	}
	function fngotoorderpg(acctid){
		document.FRMSEARCH.reset();
		document.FRMSEARCH.txthidacctid.value=acctid;
		document.FRMSEARCH.action = "orderslist.asp?useStore=<%=useStore%>&useStoreID=<%=useStoreID%>&historical=<%=historical%>"
		document.FRMSEARCH.submit();
	}
	function resetAll(){
		for(i=0; i<document.FRMSEARCH.elements.length; i++){
			if (document.FRMSEARCH.elements[i].type == "text"){
				document.FRMSEARCH.elements[i].value = "";
			}else if (document.FRMSEARCH.elements[i].type == "select-one"){
				document.FRMSEARCH.elements[i].selectedIndex = 0;
			}
		}
	}
</script>