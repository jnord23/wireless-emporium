<%
pageTitle = "CO RMA System"
header = 0
%>

<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->

<h3><font face="verdana">RMA System</font></h3>

<%
OrderID = request("OrderID")
strComments = SQLquote(request("comments"))
btnExchange = request.form("btnExchange")
btnRefund = request.form("btnRefund")
btnBackOrder = request.form("btnBackOrder")
btnBackOrderSend = request.form("btnBackOrderSend")
btnNoItem=request.form("btnNoItem")
btnNoItemSend=request.form("btnNoItemSend")
btnModified = request.form("btnModified")
btnModifiedSend = request.form("btnModifiedSend")
btnOtherRefund = request.form("btnOtherRefund")
btnOtherRefundSend = request.form("btnOtherRefundSend")

if OrderID = "" then
	response.write "<b><font color='red'>MISSING ORDER RECORD ID. PLEASE CLICK ON BACK BUTTON TO GO BACK TO PREVIOUS PAGE.</font></b>"
end if

if btnExchange <> "" then
	SQL = "SELECT A.fname,A.lname,A.email FROM CO_Accounts A INNER JOIN WE_Orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 3, 3
	if not RS.eof then
		if not isnull(RS("email")) then strTo = RS("email")
		strFname = RS("fname")
		strLname = RS("lname")
	end if
	'strTo = strTo & ",ruben@CellularOutfitter.com,michael@CellularOutfitter.com"
	if trim(strTo) <> "" then
		select case btnExchange
			case "New EXCHANGE", "Re-Send EXCHANGE"
				if btnExchange = "New EXCHANGE" then
					strSubject = "Instructions for Product Exchange"
					msgSuccess = "New EXCHANGE e-mail SENT"
					strNotes = formatNotes(strSubject & " email sent to customer.")
					RMAstatus = 1
				else
					strSubject = "Re-Send Instructions for Product Exchange"
					msgSuccess = "New EXCHANGE e-mail RE-SENT"
					strNotes = formatNotes(strSubject & " email re-sent to customer.")
					RMAstatus = 1
				end if
				strBody = "<p>Thank you for shopping with CellularOutfitter.com. We apologize for the problems you have encountered with your product.</p>" & vbcrlf
				strBody = strBody & "<p>We offer our famous EZ No-Hassle 30-day Return Policy: If you are not completely satisfied with your order, within 30 DAYS of receipt, you may return original-condition merchandise for an exchange or refund.</p>"
				strBody = strBody & "<p>Please return your exchange item(s) to:<br>" & vbcrlf
				strBody = strBody & "CellularOutfitter.com<br>Attn: EXCHANGE<br>1410 N. Batavia St.<br>Orange, CA&nbsp;&nbsp;92867</p>" & vbcrlf
				strBody = strBody & "Please print out this E-mail and fill in the following information (Returns without this information will not be processed):" & vbcrlf
				strBody = strBody & "<p>Please write the following on the outside of the shipping container:" & vbcrlf
				strBody = strBody & "<p><b>Name on the order:</b>" & vbcrlf
				strBody = strBody & "<p><b>Order number:</b>" & vbcrlf
				strBody = strBody & "<p><b>E-mail Address:</b>" & vbcrlf
				strBody = strBody & "<p><b>Phone Number:</b>" & vbcrlf
				strBody = strBody & "<p><b>Reason for return:</b>" & vbcrlf
				strBody = strBody & "<p><b>Being returned for (circle one): Refund/Exchange/Store Credit</b>" & vbcrlf
				strBody = strBody & "<p><b>If the return is for an exchange please enter the item you would like in the space below:</b>" & vbcrlf
				strBody = strBody & "<p>____________________________________________________________________________</b>" & vbcrlf
				strBody = strBody & "<p>Also, please write the following on the outside of the shipping container: <b>RMA" & OrderID &"</b>" & vbcrlf
				if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
				strBody = strBody & "<p>Best regards,</p>" & vbcrlf
				strBody = strBody & "<p>CellularOutfitter.com Service</b></p>" & vbcrlf
				strBody = strBody & "<p><a href=""mailto:service@CellularOutfitter.com"">service@CellularOutfitter.com</a><br>" & vbcrlf
				strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
				CDOSend strTo,"service@CellularOutfitter.com",strSubject,strBody
			case "Product(s) Received [re-ship]"
				msgSuccess = "Received return, put in re-ship pile."
				strNotes = formatNotes("Received return, put in re-ship pile.")
				RMAstatus = 2
			case "Product(s) Received [exchange]"
				msgSuccess = "Received return, put in exchange pile."
				strNotes = formatNotes("Received return, put in exchange pile.")
				RMAstatus = 2
			case "Process EXCHANGE"
				strSubject = "Your Exchange Has Been Processed"
				msgSuccess = "EXCHANGE PROCESSED"
				strNotes = formatNotes("EXCHANGE PROCESSED")
				RMAstatus = 5
				strBody = "<p>Dear " & strFname & " " & strLname & ",</p>" & vbcrlf
				strBody = strBody & "<p>This e-mail is to inform you that we have received your returned merchandise. Your exchange has been processed."
				strBody = strBody & "Your replacement product(s) will be shipped in 1-2 business days.</p>" & vbcrlf
				strBody = strBody & "<p>Thank you for shopping with CellularOutfitter.com</p>" & vbcrlf
				strBody = strBody & "<p>Please contact us if you have any questions.</p>" & vbcrlf
				if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
				strBody = strBody & "<p>Best regards,</p>" & vbcrlf
				strBody = strBody & "<p>CellularOutfitter.com Service</b></p>" & vbcrlf
				strBody = strBody & "<p><a href=""mailto:service@CellularOutfitter.com"">service@CellularOutfitter.com</a><br>" & vbcrlf
				strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
				CDOSend strTo,"service@CellularOutfitter.com",strSubject,strBody
		end select
		
		SQL = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.Open SQL, oConn, 3, 3
		if not RS.EOF then
			SQL = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RS("ordernotes") & strNotes) & "' WHERE ID = '" & RS("ID") & "'"
		else
			SQL = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQL
		SQL = "UPDATE we_orders SET RMAstatus='" & RMAstatus & "' WHERE orderID = '" & OrderID & "'"
		oConn.execute SQL
		response.write "<h3 align=""center"">" & msgSuccess & "</h3>" & vbcrlf
	else
		response.write "<h3 align=""center"">Could not find e-mail address.</h3>" & vbcrlf
	end if
elseif btnRefund <> "" then
	SQL = "SELECT A.fname,A.lname,A.email FROM CO_Accounts A INNER JOIN WE_Orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 3, 3
	if not RS.eof then
		strFname = RS("fname")
		strLname = RS("lname")
		if not isnull(RS("email")) then strTo = RS("email")
	end if
	'strTo = strTo & ",ruben@CellularOutfitter.com,michael@CellularOutfitter.com"
	if trim(strTo) <> "" then
		select case btnRefund
			case "New REFUND", "Re-Send REFUND"
				if btnRefund = "New REFUND" then
					strSubject = "Instructions for Product Refund"
					msgSuccess = "New REFUND e-mail SENT"
					strNotes = formatNotes(strSubject & " email re-sent to customer.")
					RMAstatus = 3
				else
					strSubject = "Re-Send Instructions for Product Refund"
					msgSuccess = "New REFUND e-mail RE-SENT"
					strNotes = formatNotes(strSubject & " email sent to customer.")
					RMAstatus = 3
				end if
				strBody = "<p>Dear " & strFname & " " & strLname & ",</p>" & vbcrlf
				strBody = strBody & "<p>Thank you for shopping with CellularOutfitter.com. We apologize for the problems you have encountered with your product.</p>" & vbcrlf
				strBody = strBody & "<p>This is an email confirmation indicating that we have received your request for credit.</p>" & vbcrlf
				strBody = strBody & "<p>Please return your item(s) to:<br>" & vbcrlf
				strBody = strBody & "CellularOutfitter.com<br>Attn: REFUND<br>1410 N. Batavia St.<br>Orange, CA&nbsp;&nbsp;92867</p>" & vbcrlf
				strBody = strBody & "<p>Please print out this E-mail and fill in the following information (Returns without this information will not be processed):</p>" & vbcrlf
				strBody = strBody & "<p><b>Name on the order:</b></p>" & vbcrlf
				strBody = strBody & "<p><b>Order number:</b></p>" & vbcrlf
				strBody = strBody & "<p><b>E-mail Address:</b></p>" & vbcrlf
				strBody = strBody & "<p><b>Phone Number:</b></p>" & vbcrlf
				strBody = strBody & "<p><b>Reason for return:</b></p>" & vbcrlf
				strBody = strBody & "<p><b>Being returned for (circle one): Refund/Exchange/Store Credit</b></p>" & vbcrlf
				strBody = strBody & "<p>Also, please write the following on the outside of the shipping container: <b>RMA" & OrderID &"</b></p>" & vbcrlf
				if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
				strBody = strBody & "<p>Please note: Orders over 30 days old are not available for refund. Please review our return policy at "
				strBody = strBody & "<a href=""http://www.CellularOutfitter.com/shipping-policy.html"">http://www.CellularOutfitter.com/shipping-policy.html</a></p>" & vbcrlf
				strBody = strBody & "<p>Best regards,</p>" & vbcrlf
				strBody = strBody & "<p>CellularOutfitter.com Service</b></p>" & vbcrlf
				strBody = strBody & "<p><a href=""mailto:service@CellularOutfitter.com"">service@CellularOutfitter.com</a><br>" & vbcrlf
				strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
				CDOSend strTo,"service@CellularOutfitter.com",strSubject,strBody
			case "Product(s) Received"
			
				dim thisPartNumber, thisQuantity, thisItemID
				'Response.Write("totalcount: "&request.form("totalCount"))
				for a = 1 to request.form("totalCount")
					if request.form("return" & a) = "1" then
						thisPartNumber = request.form("PartNumber" & a)
						thisQuantity = request.form("quantity" & a)
						thisItemID = request.form("itemID" & a)
						thisQuantityold = request.form("quantityold" & a)
						if thisQuantity > thisQuantityold then strError = "Received item qty can not be more then order qty!"
					end if
				next
				
				if strError = "" then
				
				strSubject = "Your return merchandise have been received"
				msgSuccess = "Received return, put in refund  pile."
				strNotes = formatNotes("Received return, put in refund pile.")
				RMAstatus = 4
				'------------------ add inv adjuest when received item
				strBody = "<p>Hello " & strFname & " " & strLname & ",</p>" & vbcrlf
				strBody = strBody & "<p>The following item(s) from your CellularOutfitter.com order #"& OrderID &" return have been received:"
				for a = 1 to request.form("totalCount")
					strBody = strBody & "<ul><li>Part Number: " & request.form("PartNumber" & a) &"</li> "& vbcrlf
					strBody = strBody & "<li>Item Description: " & request.form("quantity" & a) &"</li>" & vbcrlf
					strBody = strBody & "<li>Quantity: " & request.form("quantityold" & a) &"</li></ul>" & vbcrlf 
				next
				strBody = strBody & "<p>Please allow 2-4 business days for your exchange or refund to be processed. Once the exchange or refund is issued you will receive an E-mail to confirm the return is complete.</p>" & vbcrlf
				strBody = strBody & "<p>Thank you for choosing Cellular Outfitter,</p>" & vbcrlf
				'if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
				strBody = strBody & "<p>Best regards,</p>" & vbcrlf
				strBody = strBody & "<p><font color=#999999><b>Cellular</font> <font color=#FF8100>Outfitter</font> Customer Support</b></p>" & vbcrlf
				strBody = strBody & "<p><a href=""mailto:service@CellularOutfitter.com"">service@CellularOutfitter.com</a><br>" & vbcrlf
				strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
				CDOsend strTo,"service@CellularOutfitter.com",strSubject,strBody
				
					for a = 1 to request.form("totalCount")
						if request.form("return" & a) = "1" then
							thisPartNumber = request.form("PartNumber" & a)
							thisQuantity = request.form("quantity" & a)
							thisItemID = request.form("itemID" & a)
							thisQuantityold = request.form("quantityold" & a)
							'Response.Write("<br>thisPartNumber: "&thisPartNumber)
							'Response.Write("<br>thisQuantity" &thisQuantity)
							'Response.Write("<br>thisItemID" &thisItemID)
							SQL = "SELECT ItemKit_NEW FROM we_items WHERE itemID='" & thisItemID & "'"
							set rsPart = server.createobject("ADODB.recordset")
							rsPart.open SQL, oConn, 3, 3
							if not rsPart.eof then
								KIT = rsPart("ItemKit_NEW")
							else
								KIT = null
							end if
							
							call UpdateInvQty(thisPartNumber, thisQuantity, KIT)
							call NumberOfSales(thisItemID, thisQuantity, KIT)
							SQL = "UPDATE we_orderdetails SET returned = " & thisQuantity & " WHERE orderID='" & orderID & "' AND itemID='" & thisItemID & "'"
							'response.write "<p>" & SQL & "</p>"
							oConn.execute SQL
						end if
					next
				else
					response.write "<h3 align=""center""><font color=""#FF0000"">ERROR!<br>" & strError & "</font></h3>" & vbcrlf
					response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>" & vbcrlf
					response.write "</body></html>" & vbcrlf
					response.end
				end if
		'-----------------end add inv adjuest when received item				
			case "Process REFUND"
				strSubject = "Your Refund Has Been Processed"
				strCR = request.form("txtcredit")
				strError = ""
				ordergrandtotal = 0
				if not isNumeric(strCR) then strError = "Credit amount must be a numeric value!"
				if strError = "" then
					strCR = cDbl(strCR)
					SQL = "SELECT ordergrandtotal FROM we_orders WHERE orderid = '" & OrderID & "'"
					Set RS = Server.CreateObject("ADODB.Recordset")
					RS.Open SQL, oConn, 3, 3
					if not RS.EOF then
						ordergrandtotal = cDbl(RS("ordergrandtotal"))
						if strCR > ordergrandtotal then strError = "Refund amount may not exceed Order Total!"
					else
						strError = "Order not found!"
					end if
				end if
				if strError = "" then
					if strCR < ordergrandtotal then
						strNotes = formatNotes("Partial Credit issued for " & formatCurrency(strCR) & ". Email sent to customer.")
						msgSuccess = "PARTIAL REFUND PROCESSED for " & formatCurrency(strCR)
						RMAstatus = 6
						SQL = "UPDATE we_orders SET refundAmount='" & strCR & "', refundDate='" & now & "', refundreason ='"&refundreason&"' WHERE orderID = '" & OrderID & "'"
						'response.write "<p>" & SQL & "</p>" & vbcrlf
						oConn.execute SQL
					else
						strNotes = formatNotes("Full Credit issued for " & formatCurrency(strCR) & ". Email sent to customer.")
						msgSuccess = "FULL REFUND PROCESSED for " & formatCurrency(strCR)
						RMAstatus = 7
						SQL = "UPDATE we_orders SET refundAmount='" & strCR & "', refundDate='" & now & "', refundreason ='"&refundreason&"' WHERE orderID = '" & OrderID & "'"
						'response.write "<p>" & SQL & "</p>" & vbcrlf
						oConn.execute SQL
					end if
					strBody = "<p>Dear " & strFname & " " & strLname & ",</p>" & vbcrlf
					strBody = strBody & "<p>This e-mail is to inform you that we have received your returned merchandise"
					strBody = strBody & " and have processed your credit in the amount of <b>" & formatCurrency(strCR) & "</b>.</p>" & vbcrlf
					strBody = strBody & "<p>Please allow a few days for posting.</p>" & vbcrlf
					strBody = strBody & "<p>Thank you for shopping with CellularOutfitter.com</p>" & vbcrlf
					strBody = strBody & "<p>Please contact us if you have any questions.</p>" & vbcrlf
					if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
					strBody = strBody & "<p>Best regards,</p>" & vbcrlf
					strBody = strBody & "<p>CellularOutfitter.com Service</b></p>" & vbcrlf
					strBody = strBody & "<p><a href=""mailto:service@CellularOutfitter.com"">service@CellularOutfitter.com</a><br>" & vbcrlf
					strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
					CDOSend strTo,"service@CellularOutfitter.com",strSubject,strBody
				
				else
					response.write "<h3 align=""center""><font color=""#FF0000"">ERROR!<br>" & strError & "</font></h3>" & vbcrlf
					response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>" & vbcrlf
					response.write "</body></html>" & vbcrlf
					response.end
				end if
		end select
		
		SQL = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.Open SQL, oConn, 3, 3
		if not RS.EOF then
			SQL = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RS("ordernotes") & strNotes) & "' WHERE ID = '" & RS("ID") & "'"
		else
			SQL = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQL
		SQL = "UPDATE we_orders SET RMAstatus='" & RMAstatus & "' WHERE orderID = '" & OrderID & "'"
		oConn.execute SQL
		response.write "<h3 align=""center"">" & msgSuccess & "</h3>" & vbcrlf
	else
		response.write "<h3 align=""center"">Could not find e-mail address.</h3>" & vbcrlf
	end if
	'--------------RMA Modified ---------------------
	elseif btnModified  <> "" then
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<form name="frmRMAModify" method="post" action="RMA.asp">
			<tr>
				<td colspan="2" class="smlText" align="center">
				  <%
					SQL = "SELECT  A.orderdetailid, A.orderID, A.quantity, B.itemID, B.PartNumber, B.itemDesc_CO FROM we_orderdetails A INNER JOIN we_Items B ON A.itemID=B.itemID WHERE A.orderID = '" & OrderID & "' ORDER BY B.PartNumber"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 3, 3
					a = 0
					if RS.eof then
						response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
					else
						%>
						<%=RS.recordcount%> items found
						<br>
						<table border="1" cellpadding="3" cellspacing="0" width="90%" class="smlText">
							<tr>
								<td align="center" width="20"><b>Item#</b></td>
								<td align="center" width="110"><b>Part&nbsp;Number</b></td>
								<td align="center" width="230"><b>Description</b></td>
								<td align="center" width="40"><b>Qty</b></td>
							  <td align="center" width="40">Inv. Qty</td>
								<td align="center" width="50"><b>Out of Stock</b></td>
							</tr>
							<%
							do until RS.eof
								a = a + 1
								%>
								<tr>
									<td align="center"><%=RS("itemID")%><input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>"></td>
									<td align="center"><nobr><%=RS("PartNumber")%><input type="hidden" name="PartNumber<%=a%>" value="<%=RS("PartNumber")%>"></nobr></td>
									<td align="center"><%=RS("itemDesc_CO")%></td>
									<td align="center">
                                    <input type="text" name="quantity<%=a%>" size="1" value="<%=RS("quantity")%>" maxlength="3">
                                    </td>
									<td align="center">&nbsp;
                                    <%
									SQL3 = "SELECT inv_qty from we_items where itemid = '"&RS("itemID")&"'"
									Set RS3 = Server.CreateObject("ADODB.Recordset")
									RS3.Open SQL3, oConn, 3, 3
									Response.Write(rs3("inv_qty"))
									'RS3.close
									'Set RS3 = nothing
									%>
                                    </td>
									<td align="center"><input type="checkbox" name="BackOrder<%=a%>" value="1"></td>
								</tr>
								<%
								RS.movenext
							loop
							%>
						</table>
						<input type="hidden" name="totalCount" value="<%=a%>">
			 		<%
					end if
					%>
			  </td>
			</tr>
			<tr>
				<td class="bigText" align="center"></td>
			</tr>
			<tr>
				<td class="bigText" align="center">
					<input type="hidden" name="ORDERID" value="<%=OrderID%>">
                    <input type="hidden" name= "comments" id ="show" value="<%=request("comments")%>">
					<input type="submit" name="btnModifiedSend" class="smltext" value="Send Order Modified Notification">
				</td>
			</tr>
		</form>
	</table>
<%	
	'--------------End of sent RMA Modified ---------------------
	'--------------Other Refund----------------------------------
elseif btnOtherRefund <> "" then
%>
    <table width="100%">
        <form name="frmOtherrefund" method="post" action="RMA.asp">
            <tr>
                <td class="bigText" bgcolor="#CCCCCC" align="center">
                    Process Other Refund
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bigText" bgcolor="#CCCCCC" align="center">
                    Enter Credit Amount For Order Id: <%=OrderID%><br>
                    <input type="text" name="txtcredit" size="10">
                    <select name="refundreason" id="refundreason">
                      <option value="9">Other Refund</option>
                     </select>
                 </td>
            </tr>
            <tr>
                <td colspan="2" class="smlText" align="center">
                    Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
                </td>
            </tr>
            <tr>
                <td class="bigText" align="center">
                    <input type="hidden" name="ORDERID" value="<%=OrderID%>">
                    <input type="submit" name="btnOtherRefundSend" id ="btnOtherRefundSend" class="smltext" value="Process Other Refund">
                </td>
            </tr>
        </form>
    </table>
<%
'--------------Other Refund End
elseif btnBackOrder <> "" then
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<form name="frmBackOrder" method="post" action="RMA.asp">
			<tr>
				<td colspan="2" class="smlText" align="center">
					<%
					SQL = "SELECT A.orderID, A.quantity, B.itemID, B.PartNumber, B.itemDesc_CO FROM we_orderdetails A INNER JOIN we_Items B ON A.itemID=B.itemID WHERE A.orderID = '" & OrderID & "' ORDER BY B.PartNumber"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 3, 3
					a = 0
					if RS.eof then
						response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
					else
						%>
						<%=RS.recordcount%> items found
						<br>
						<table border="1" cellpadding="3" cellspacing="0" width="90%" class="smlText">
							<tr>
								<td align="center" width="20"><b>Item#</b></td>
								<td align="center" width="110"><b>Part&nbsp;Number</b></td>
								<td align="center" width="230"><b>Description</b></td>
								<td align="center" width="40"><b>Qty</b></td>
								<td align="center" width="50"><b>BACK-ORDER</b></td>
							</tr>
							<%
							do until RS.eof
								a = a + 1
								%>
								<tr>
									<td align="center"><%=RS("itemID")%><input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>"></td>
									<td align="center"><nobr><%=RS("PartNumber")%><input type="hidden" name="PartNumber<%=a%>" value="<%=RS("PartNumber")%>"></nobr></td>
									<td align="center"><%=RS("itemDesc_CO")%></td>
									<td align="center"><input type="text" name="quantity<%=a%>" size="1" value="<%=RS("quantity")%>" maxlength="3"></td>
									<td align="center"><input type="checkbox" name="BackOrder<%=a%>" value="1"></td>
								</tr>
								<%
								RS.movenext
							loop
							%>
						</table>
						<input type="hidden" name="totalCount" value="<%=a%>">
						<%
					end if
					%>
				</td>
			</tr>
			<tr>
				<td class="bigText" align="center">
					Number&nbsp;of&nbsp;Business&nbsp;Days:&nbsp;<input type="text" name="NumberOfDays" size="10" value="3 to 5">
				</td>
			</tr>
			<tr>
				<td class="bigText" align="center">
					<input type="hidden" name="ORDERID" value="<%=OrderID%>">
					<input type="submit" name="btnBackOrderSend" class="smltext" value="Send BACK-ORDER Notification">
				</td>
			</tr>
		</form>
	</table>
	<%  
elseif btnNOItem <> "" then
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<form name="frmNoItem" method="post" action="RMA.asp">
			<tr>
				<td colspan="2" class="smlText" align="center">
					<%
					SQL = "SELECT A.orderID, A.quantity, B.itemID, B.PartNumber, B.itemDesc_CO FROM we_orderdetails A INNER JOIN we_Items B ON A.itemID=B.itemID WHERE A.orderID = '" & OrderID & "' ORDER BY B.PartNumber"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 3, 3
					a = 0
					if RS.eof then
						response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
					else
						%>
						<%=RS.recordcount%> items found
						<br>
						<table border="1" cellpadding="3" cellspacing="0" width="90%" class="smlText">
							<tr>
								<td align="center" width="20"><b>Item#</b></td>
								<td align="center" width="110"><b>Part&nbsp;Number</b></td>
								<td align="center" width="230"><b>Description</b></td>
								<td align="center" width="40"><b>Qty</b></td>
								<td align="center" width="50"><b>No-Item</b></td>
							</tr>
							<%
							do until RS.eof
								a = a + 1
								%>
								<tr>
									<td align="center"><%=RS("itemID")%><input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>"></td>
									<td align="center"><nobr><%=RS("PartNumber")%><input type="hidden" name="PartNumber<%=a%>" value="<%=RS("PartNumber")%>"></nobr></td>
									<td align="center"><%=RS("itemDesc_CO")%></td>
									<td align="center"><input type="text" name="quantity<%=a%>" size="1" value="<%=RS("quantity")%>" maxlength="3"></td>
									<td align="center"><input type="checkbox" name="BackOrder<%=a%>" value="1"></td>
								</tr>
								<%
								RS.movenext
							loop
							%>
						</table>
						<input type="hidden" name="totalCount" value="<%=a%>">
						<%
					end if
						%>
				</td>
			</tr>
			<tr>
				<td class="bigText" align="center">
					<input type="hidden" name="ORDERID" value="<%=OrderID%>">
					<input type="submit" name="btnNoItemSend" class="smltext" value="Send NO Item Notification">
				</td>
			</tr>
		</form>
	</table>
<%
elseif btnNoItemSend <> "" then
	SQL = "SELECT A.fname, A.lname, A.email FROM CO_Accounts A INNER JOIN we_orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if RS.eof then
		strError = "Record not found!"
	else
		'strEmail = "ruben@CellularOutfitter.com"
		strEmail = RS("email")
		fname = RS("fname")
		lname = RS("lname")
		strName = Ucase(left(fname, 1)) & right(fname,(len(fname)-1)) & " " & Ucase(left(lname, 1)) & right(lname,(len(lname)-1))
		cdo_body = "<html>" & vbcrlf
		cdo_body = cdo_body & "<head>" & vbcrlf
		cdo_body = cdo_body & "<style type='text/css'>" & vbcrlf
		cdo_body = cdo_body & "<!--" & vbcrlf
		cdo_body = cdo_body & ".regText {font-family: Times; font-size: 12pt;}" & vbcrlf
		cdo_body = cdo_body & ".headerText {font-family: Times; font-size: 14pt;}" & vbcrlf
		cdo_body = cdo_body & "-->" & vbcrlf
		cdo_body = cdo_body & "</style>" & vbcrlf
		cdo_body = cdo_body & "</head>" & vbcrlf
		cdo_body = cdo_body & "<body class='regText'>"
		cdo_body = cdo_body & "<table width='100%' cellpadding='8' cellspacing='0' align='center' bgcolor='#eaeaea' class='regText'><tr>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='left' valign='top'><img src='http://www.cellularoutfitter.com/images/CellularOutfitter.jpg' border='0' hspace='12' align='absmiddle'></td>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='center' valign='middle'><b>Cellular Outfitter Order Information</b></td>" & vbcrlf
		cdo_body = cdo_body & "</tr></table>" & vbcrlf
		cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' class='regText'>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2'><br><p><b>Hello " & strName & ",</b></p>"
		cdo_body = cdo_body & "<p>Unfortunately, the following item(s) from your CellularOutfitter.com order #" & OrderID & " are no longer available:</p>" & vbcrlf
		cdo_body = cdo_body & "<ul>" & vbcrlf
		b = 0
		strNotes = formatNotes("Notice sent to customer for item(s):<br>")
		for a = 1 to request.form("totalCount")
			if strError = "" and request.form("BackOrder" & a) = "1" then
				SQL = "SELECT itemDesc_CO FROM we_items WHERE itemID = '" & request.form("itemID" & a) & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if not RS2.eof then
					cdo_body = cdo_body & "<li><b>Part Number: " & request.form("PartNumber" & a) & "<br>" & vbcrlf
					cdo_body = cdo_body & "Item Description: " & RS2("itemDesc_CO") & "<br>" & vbcrlf
					cdo_body = cdo_body & "Quantity: " & request.form("quantity" & a) & "</b></li>" & vbcrlf
					strNotes = strNotes & request.form("quantity" & a) & " x " & RS2("itemDesc_CO") & "<br>"
					b = b + 1
				else
					strError = "Record not found!"
				end if
			end if
		next
		cdo_body = cdo_body & "</ul>" & vbcrlf
		cdo_body = cdo_body & "<p>Please return to our website and choose another item to replace the one above. Simply e-mail us back with your choice, and we will ship it immediately.</p>" & vbcrlf
		cdo_body = cdo_body & "<p>If there is not an item you would like at this time, please reply to this e-mail and let us know if you would prefer Store Credit or a Refund and we will gladly process your request.</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Thank you for choosing Cellular Outfitter,</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Customer Support<br>" & vbcrlf
		cdo_body = cdo_body & "<a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a>.</p>" & vbcrlf
		cdo_body = cdo_body & "<table width='100%' cellpadding='5' cellspacing='5' align='center' bgcolor='#eaeaea'><tr><td colspan='2' class='regText'><a href='http://www.CellularOutfitter.com/track-your-order.asp'><b>Check Your Order Status Online</b></a></td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2' class='regText'><b>Please visit us again soon!</b> - <a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a></td></tr></table>" & vbcrlf
		cdo_body = cdo_body & "</body></html>"
		response.write "<h1 align=""center"">E-mail sent to " & strEmail & "</h1>" & vbcrlf
	end if
		if b = 0 then strError = "You did not select any products!"
	if strError = "" then
		cdo_from = "Automatic E-Mail from CellularOutfitter.com<sales@cellularoutfitter.com>"
		cdo_subject = "CellularOutfitter.com Order Information"
		'cdo_to = "ruben@CellularOutfitter.com"
		cdo_to = strEmail
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		SQL = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.Open SQL, oConn, 3, 3
		if not RS.EOF then
			SQL = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RS("ordernotes") & strNotes) & "' WHERE ID = '" & RS("ID") & "'"
		else
			SQL = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQL
	else
		response.write "<h1 align=""center"">" & strError & "</h1>" & vbcrlf
	end if
	'end of no longer avaliable


elseif btnBackOrderSend <> "" then
	SQL = "SELECT A.fname, A.lname, A.email FROM CO_accounts A INNER JOIN we_orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if RS.eof then
		strError = "Record not found!"
	else
		strEmail = RS("email")
		fname = RS("fname")
		lname = RS("lname")
		strName = Ucase(left(fname, 1)) & right(fname,(len(fname)-1)) & " " & Ucase(left(lname, 1)) & right(lname,(len(lname)-1))
		cdo_body = "<html>" & vbcrlf
		cdo_body = cdo_body & "<head>" & vbcrlf
		cdo_body = cdo_body & "<style type='text/css'>" & vbcrlf
		cdo_body = cdo_body & "<!--" & vbcrlf
		cdo_body = cdo_body & ".regText {font-family: Times; font-size: 12pt;}" & vbcrlf
		cdo_body = cdo_body & ".headerText {font-family: Times; font-size: 14pt;}" & vbcrlf
		cdo_body = cdo_body & "-->" & vbcrlf
		cdo_body = cdo_body & "</style>" & vbcrlf
		cdo_body = cdo_body & "</head>" & vbcrlf
		cdo_body = cdo_body & "<body class='regText'>"
		cdo_body = cdo_body & "<table width='100%' cellpadding='8' cellspacing='0' align='center' bgcolor='#eaeaea' class='regText'><tr>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='left' valign='top'><img src='http://www.cellularoutfitter.com/images/CellularOutfitter.jpg' border='0' hspace='12' align='absmiddle'></td>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='center' valign='middle'><b>Cellular Outfitter Order Information</b></td>" & vbcrlf
		cdo_body = cdo_body & "</tr></table>" & vbcrlf
		cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' class='regText'>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2'><br><p><b>Hello " & strName & ",</b></p>"
		cdo_body = cdo_body & "<p>The following item(s) from your CellularOutfitter.com order are out of stock and currently on backorder:</p>" & vbcrlf
		cdo_body = cdo_body & "<ul>" & vbcrlf
		b = 0
		strNotes = formatNotes("Backorder sent to customer for item(s):<br>")
		for a = 1 to request.form("totalCount")
			if strError = "" and request.form("BackOrder" & a) = "1" then
				SQL = "SELECT itemDesc_CO FROM we_items WHERE itemID = '" & request.form("itemID" & a) & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if not RS2.eof then
					cdo_body = cdo_body & "<li><b>Part Number: " & request.form("PartNumber" & a) & "<br>" & vbcrlf
					cdo_body = cdo_body & "Item Description: " & RS2("itemDesc_CO") & "<br>" & vbcrlf
					cdo_body = cdo_body & "Quantity: " & request.form("quantity" & a) & "</b></li>" & vbcrlf
					strNotes = strNotes & request.form("quantity" & a) & " x " & RS2("itemDesc_CO") & "<br>"
					b = b + 1
				else
					strError = "Record not found!"
				end if
			end if
		next
		cdo_body = cdo_body & "</ul>" & vbcrlf
		cdo_body = cdo_body & "<p>We expect the item(s) to be back in stock within the next " & request.form("NumberOfDays") & " business days. If the status of the back ordered item(s) changes, you will be contacted by E-mail." & "</p>" & vbcrlf
		cdo_body = cdo_body & "<table width='95%'><tr><td class='regText'>"
		cdo_body = cdo_body & "<p>If you have any questions please reply to this E-mail.</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Best Regards,</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Customer Support<br>" & vbcrlf
		cdo_body = cdo_body & "<a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a>.</p>" & vbcrlf
		cdo_body = cdo_body & "</td></tr></table>"
		cdo_body = cdo_body & "<table width='100%' cellpadding='5' cellspacing='5' align='center' bgcolor='#eaeaea'><tr><td colspan='2' class='regText'><a href='http://www.CellularOutfitter.com/track-your-order.asp'><b>Check Your Order Status Online</b></a></td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2' class='regText'><b>Please visit us again soon!</b> - <a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a></td></tr></table>" & vbcrlf
		cdo_body = cdo_body & "</body></html>"
		response.write "<h1 align=""center"">E-mail sent to " & strEmail & "</h1>" & vbcrlf
	end if
	if b = 0 then strError = "You did not select any products to Back-Order!"
	if strError = "" then
		cdo_from = "Automatic E-Mail from CellularOutfitter.com<sales@cellularoutfitter.com>"
		cdo_subject = "Cellular Outfitter Order Information"
		cdo_to = strEmail
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		SQL = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.Open SQL, oConn, 3, 3
		if not RS.EOF then
			SQL = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RS("ordernotes") & strNotes) & "' WHERE ID = '" & RS("ID") & "'"
		else
			SQL = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQL

		SQL = "UPDATE we_orders SET RMAstatus= '8' WHERE orderID = '" & OrderID & "'"
		oConn.execute SQL

	else
		response.write "<h1 align=""center"">" & strError & "</h1>" & vbcrlf
	end if
%>
<%
'----------------- Sent RMA modifiled Note--------------------
elseif btnModifiedSend <> "" then
	SQL = "SELECT A.fname, A.lname, A.email, B.ordersubtotal, B.ordergrandtotal, b.OrderShippingfee FROM CO_accounts A INNER JOIN we_orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'" 
	'Response.Write(SQL)
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if RS.eof then
		strError = "Record not found!"
	else
		shipping = rs("OrderShippingfee")
		strEmail = RS("email")
		fname = RS("fname")
		lname = RS("lname")
		strName = Ucase(left(fname, 1)) & right(fname,(len(fname)-1)) & " " & Ucase(left(lname, 1)) & right(lname,(len(lname)-1))
		cdo_body = "<html>" & vbcrlf
		cdo_body = cdo_body & "<head>" & vbcrlf
		cdo_body = cdo_body & "<style type='text/css'>" & vbcrlf
		cdo_body = cdo_body & "<!--" & vbcrlf
		cdo_body = cdo_body & ".regText {font-family: Arial; font-size: 9pt;}" & vbcrlf
		cdo_body = cdo_body & ".headerText {font-family: Arial; font-size: 11pt;}" & vbcrlf
		cdo_body = cdo_body & "-->" & vbcrlf
		cdo_body = cdo_body & "</style>" & vbcrlf
		cdo_body = cdo_body & "</head>" & vbcrlf
		cdo_body = cdo_body & "<body class='regText'>"
		cdo_body = cdo_body & "<table width='100%' cellpadding='8' cellspacing='0' align='center' bgcolor='#eaeaea' class='regText'><tr>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='left' valign='top'><img src='http://www.CellularOutfitter.com/images/CellularOutfitter.jpg' border='0' hspace='12' align='absmiddle'></td>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='center' valign='middle'><b>Cellular Outfitter Order Information</b></td>" & vbcrlf
		cdo_body = cdo_body & "</tr></table>" & vbcrlf
		cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' class='regText'>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2'><br><p><b>Hello " & strName & ",</b></p>"
		cdo_body = cdo_body & "<p> Unfortunately,  the following item(s) from your CellularOutfitter.com order #"&OrderID&" are no  longer available:</p>" & vbcrlf
		cdo_body = cdo_body & "<ul>" & vbcrlf
		b = 0
		strNotes = formatNotes("Item out of stock, order  modified for item(s):<br>")
		for a = 1 to request.form("totalCount")
			if strError = "" and request.form("BackOrder" & a) = "1" then
				SQL = "SELECT itemDesc FROM we_items WHERE itemID = '" & request.form("itemID" & a) & "'"
				'SQL = "SELECT itemDesc, Price_our FROM we_items WHERE itemID = '" & request.form("itemID" & a) & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if not RS2.eof then
					cdo_body = cdo_body & "<li><b>Part Number: " & request.form("PartNumber" & a) & "<br>" & vbcrlf
					cdo_body = cdo_body & "Item Description: " & RS2("itemDesc") & "<br>" & vbcrlf
					cdo_body = cdo_body & "Quantity: " & request.form("quantity" & a) & "</b></li>" & vbcrlf
					strNotes = strNotes & request.form("quantity" & a) & " x " & RS2("itemDesc") & "<br>"
					b = b + 1
					SQLRMA = "UPDATE we_orderdetails SET quantity= '"&request.form("quantity" & a)&"'"
					SQLRMA = SQLRMA & " WHERE orderID = '" & OrderID & "' and itemID = '" & request.form("itemID" & a) & "'" 
					oConn.execute SQLRMA
				else
					strError = "Record not found!"
				end if
			end if
		next
		cdo_body = cdo_body & "</ul>" & vbcrlf
		cdo_body = cdo_body & "<p>The remaining item(s) on your order have been shipped and are currently in transit to you." & "</p>" & vbcrlf
		cdo_body = cdo_body & "<table width='95%'><tr><td class='regText'>" & vbcrlf
		cdo_body = cdo_body & "<p>The total amount charged has been updated to reflect this change in the order and will also be reflected on the order receipt included in your package.</p>" & vbcrlf
		if strComments <> "" then 
			cdo_body = cdo_body & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
		end if
		cdo_body = cdo_body & "<p>Thank you for choosing Cellular Outfitter,</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Customer Support<br>" & vbcrlf
		cdo_body = cdo_body & "<a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a>.</p>" & vbcrlf
		cdo_body = cdo_body & "</td></tr></table>"
		cdo_body = cdo_body & "<table width='100%' cellpadding='5' cellspacing='5' align='center' bgcolor='#eaeaea'><tr><td colspan='2' class='regText'><a href='http://www.CellularOutfitter.com/orderstatus.asp'><b>Check Your Order Status Online</b></a></td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2' class='regText'><b>Please visit us again soon!</b> - <a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a></td></tr></table>" & vbcrlf
		cdo_body = cdo_body & "</body></html>"
		response.write "<h1 align=""center"">E-mail sent to " & strEmail & "</h1>" & vbcrlf
	end if
	if b = 0 then strError = "You did not select any products to Sent Modifiled!"
	if strError = "" then
		cdo_from = "Automatic E-Mail from CellularOutfitter.com<sales@CellularOutfitter.com>"
		cdo_subject = "Cellular Outfitter Order Information"
		cdo_to = strEmail
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		SQLnote = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		set RSnote = Server.CreateObject("ADODB.Recordset")
		RSnote.Open SQLnote, oConn, 3, 3
		if not RSnote.EOF then
			SQLnote = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RSnote("ordernotes") & strNotes) & "' WHERE ID = '" & RSnote("ID") & "'"
		else
			SQLnote = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQLnote
		shipping = rs("OrderShippingfee")
		SQLTotal = "SELECT A.orderID, A.quantity, B.itemID, b.price_CO FROM we_orderdetails A INNER JOIN we_Items B ON A.itemID=B.itemID WHERE A.orderID = '" & OrderID & "'"
		set RStotal = Server.CreateObject("ADODB.Recordset")
		RStotal.Open SQLTotal, oConn, 3, 3
		if not RStotal.eof then
			do while not rstotal.eof
				newtotal = Cint(rstotal("quantity")) * rstotal("price_CO")
				newsubtotal = newsubtotal + newtotal
				rstotal.movenext
			Loop
				if newsubtotal = 0 then
					newgrandtotal  = 0 
				else
					newgrandtotal = newsubtotal +shipping
				end if
			SQLgrandtotal = "UPDATE we_orders SET RMAstatus= '9', ordersubtotal = '"& newsubtotal &"', orderGrandtotal = '"& newGrandtotal &"' WHERE orderID = '" & OrderID & "'"
			oConn.execute SQLgrandtotal
		end if
	else
		response.write "<h1 align=""center"">" & strError & "</h1>" & vbcrlf
	end if
' end if item modifled sent
'-----other refund send-------
elseif btnOtherRefundSend <> "" then
	SQL = "SELECT a.email, a.fname, a.lname FROM CO_Accounts A INNER JOIN WE_Orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		if not isNull(RS("email")) then strTo = RS("email")
	end if
	if trim(strTo) <> "" then
				strSubject = "Your Refund Has Been Processed"
				strCR = request.form("txtcredit")
				refundreason = request.form("refundreason")
				strError = ""
				strfname = RS("fname")
				strlname = RS("lname")
				ordergrandtotal = 0
				if not isNumeric(strCR) then strError = "Credit amount must be a numeric value!"
				if strError = "" then
					strCR = cDbl(strCR)
					SQL = "SELECT ordergrandtotal FROM we_orders WHERE orderid = '" & OrderID & "'"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 3, 3
					if not RS.eof then
						ordergrandtotal = cDbl(RS("ordergrandtotal"))
						'if strCR > ordergrandtotal then strError = "Refund amount may not exceed Order Total!"
					else
						strError = "Order not found!"
					end if
				end if
				if strError = "" then
					if strCR < ordergrandtotal then
						strNotes = formatNotes("Partial Credit issued for " & formatCurrency(strCR) & ". Email sent to customer.")
						msgSuccess = "PARTIAL REFUND PROCESSED for " & formatCurrency(strCR)
						RMAstatus = 6
						SQL = "UPDATE we_orders SET refundAmount='" & strCR & "', refundDate='" & now & "', refundreason ='"&refundreason&"' WHERE orderID = '" & OrderID & "'"
						'response.write "<p>" & SQL & "</p>" & vbcrlf
						oConn.execute SQL
					else
						strNotes = formatNotes("Full Credit issued for " & formatCurrency(strCR) & ". Email sent to customer.")
						msgSuccess = "FULL REFUND PROCESSED for " & formatCurrency(strCR)
						RMAstatus = 7
						SQL = "UPDATE we_orders SET refundAmount='" & strCR & "', refundDate='" & now & "', refundreason ='"&refundreason&"' WHERE orderID = '" & OrderID & "'"
						'response.write "<p>" & SQL & "</p>" & vbcrlf
						oConn.execute SQL
					end if
					strBody = "<p>Dear " & strFname & " " & strLname & ",</p>" & vbcrlf
					strBody = strBody & "<p>This e-mail is to inform you that we have received your refund request."
					strBody = strBody & " and have processed your credit in the amount of <b>" & formatCurrency(strCR) & "</b>.</p>" & vbcrlf
					strBody = strBody & "<p>Please allow a few days for posting.</p>" & vbcrlf
					strBody = strBody & "<p>Thank you for shopping with CellularOutfitter.com</p>" & vbcrlf
					strBody = strBody & "<p>Please contact us if you have any questions.</p>" & vbcrlf
					if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
					strBody = strBody & "<p>Best regards,</p>" & vbcrlf
					strBody = strBody & "<p><font color=#999999><b>Cellphone</font> <font color=#FF8100>Accents</font> Service</b></p>" & vbcrlf
					strBody = strBody & "<p><a href=""mailto:service@cellularoutfitter.com"">service@cellularoutfitter.com</a><br>" & vbcrlf
					strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
					CDOsend strTo,"service@cellularoutfitter.com",strSubject,strBody
				else
					response.write "<h3 align=""center""><font color=""#FF0000"">ERROR!<br>" & strError & "</font></h3>" & vbcrlf
					response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>" & vbcrlf
					response.write "</body></html>" & vbcrlf
					response.end
				end if
		SQL = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			SQL = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RS("ordernotes") & strNotes) & "' WHERE ID = '" & RS("ID") & "'"
		else
			SQL = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQL
		SQL = "UPDATE we_orders SET RMAstatus='" & RMAstatus & "' WHERE orderID = '" & OrderID & "'"
		oConn.execute SQL
		response.write "<h3 align=""center"">" & msgSuccess & "</h3>" & vbcrlf
	else
		response.write "<h3 align=""center"">Could not find e-mail address.</h3>" & vbcrlf
	end if
%>
<!-- end of other refund sent-->
<%
else
	SQL = "SELECT RMAstatus FROM we_orders WHERE orderid = '" & OrderID & "'"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 3, 3
	if not RS.EOF then
		if not isNull(RS("RMAstatus")) then
			RMAstatus = RS("RMAstatus")
		else
			RMAstatus = 0
		end if
	else
		RMAstatus = 0
	end if
	%>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<form name="frmRefundExchange" method="post" action="RMA.asp">
		<%
		select case RMAstatus
			case 1
				%>
				<tr>
					<td class="bigText" bgcolor="#CCCCCC" align="center">
						Receive EXCHANGE Product(s)
					</td>
				</tr>
				<tr>
					<td colspan="2" class="smlText" align="center">
						Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
					</td>
				</tr>
				<tr>
					<td class="bigText" align="center">
						<input type="hidden" name="ORDERID" value="<%=OrderID%>">
						<input type="submit" name="btnExchange" class="smltext" value="Re-Send EXCHANGE">
						<p>&nbsp;</p>
						<input type="submit" name="btnExchange" class="smltext" value="Product(s) Received [re-ship]">
						<br>
						<input type="submit" name="btnExchange" class="smltext" value="Product(s) Received [exchange]">
					</td>
				</tr>
				<%
			case 2
				%>
				<tr>
					<td class="bigText" bgcolor="#CCCCCC" align="center">
						Process EXCHANGE
					</td>
				</tr>
				<tr>
					<td colspan="2" class="smlText" align="center">
						Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
					</td>
				</tr>
				<tr>
					<td class="bigText" align="center">
						<input type="hidden" name="ORDERID" value="<%=OrderID%>">
						<input type="submit" name="btnExchange" class="smltext" value="Process EXCHANGE">
					</td>
				</tr>
				<%
			case 3
				%>
				<tr>
					<td class="bigText" bgcolor="#CCCCCC" align="center">
						Receive REFUND Product(s)
					</td>
				</tr>
				<tr>
					<td colspan="2" class="smlText" align="center">
						Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
					</td>
				</tr>
               <% '======change here%>
                <tr>
					<td colspan="2" class="smlText" align="center">
						<%
						SQL = "SELECT A.*, B.itemID, B.PartNumber, B.itemDesc, B.inv_qty FROM we_orderdetails A INNER JOIN we_Items B ON A.itemID=B.itemID"
						SQL = SQL & " WHERE A.orderID = '" & OrderID & "' ORDER BY B.PartNumber"
						set RS = Server.CreateObject("ADODB.Recordset")
						RS.open SQL, oConn, 3, 3
						a = 0
						if RS.eof then
							response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
						else
							%>
							<%=RS.recordcount%> items found
							<br>
							<table border="1" cellpadding="3" cellspacing="0" width="90%" class="smlText">
								<tr>
									<td align="center" width="20"><b>Item#</b></td>
									<td align="center" width="110"><b>Part&nbsp;Number</b></td>
									<td align="center" width="230"><b>Description</b></td>
									<td align="center" width="40"><b>Qty</b></td>
									<td align="center" width="50"><b>RECEIVED</b></td>
								</tr>
								<%
								do until RS.eof
									a = a + 1
									%>
									<tr>
										<td align="center"><%=RS("itemID")%><input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>"></td>
										<td align="center"><nobr><%=RS("PartNumber")%><input type="hidden" name="PartNumber<%=a%>" value="<%=RS("PartNumber")%>"></nobr></td>
										<td align="center"><%=RS("itemDesc")%> - <%=RS("inv_qty")%></td>
										<td align="center"><input type="hidden" name="quantityold<%=a%>" size="1" value="<%=RS("quantity")%>" maxlength="3"> 
										  <input type="text" name="quantity<%=a%>" size="1" value="<%=RS("quantity")%>" maxlength="3"></td>
										<td align="center"><input type="checkbox" name="return<%=a%>" value="1"></td>
									</tr>
									<%
									RS.movenext
								loop
								%>
							</table>
							<input type="hidden" name="totalCount" value="<%=a%>">
							<%
						end if
						%>
					</td>
				</tr>
                <%'end of change here%>                
				<tr>
					<td class="bigText" align="center">
						<input type="hidden" name="ORDERID" value="<%=OrderID%>">
						<input type="submit" name="btnRefund" class="smltext" value="Re-Send REFUND">
						<p>&nbsp;</p>
						<input type="submit" name="btnRefund" class="smltext" value="Product(s) Received">
					</td>
				</tr>
				<%
			case 4
				%>
				<tr>
					<td class="bigText" bgcolor="#CCCCCC" align="center">
						Process REFUND
					</td>
				</tr>
				<tr>
					<td colspan="2" class="bigText" bgcolor="#CCCCCC" align="center">
						Enter Credit Amount For Order Id: <%=OrderID%><br>
						<input type="text" name="txtcredit" size="10">
                        <select name="refundreason" id="refundreason">
                            <option value="1">Defective product</option>
                            <option value="2">Wrong Item Shipped</option>
                            <option value="3">Broken on arrival</option>
                            <option value="4">Did not want</option>
                            <option value="5">Returned to Sender</option>
                            <option value="6">Ordered wrong item</option>
                            <option value="7">Shipped wrong item</option>
                            <option value="8">No longer needed</option>
		            	</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="smlText" align="center">
						Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
					</td>
				</tr>
				<tr>
					<td class="bigText" align="center">
						<input type="hidden" name="ORDERID" value="<%=OrderID%>">
						<input type="submit" name="btnRefund" class="smltext" value="Process REFUND">
					</td>
				</tr>
				<%
			case else
				%>
				<tr>
					<td class="bigText" bgcolor="#CCCCCC" align="center">
						Start a New RMA
					</td>
				</tr>
				<tr>
					<td colspan="2" class="smlText" align="center">
						Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
					</td>
				</tr>
				<tr>
					<td class="bigText" align="center">
						<input type="hidden" name="ORDERID" value="<%=OrderID%>">
						<input type="submit" name="btnExchange" class="smltext" value="New EXCHANGE">
						<input type="submit" name="btnRefund" class="smltext" value="New REFUND">
                        <input type="submit" name="btnModified" class="smltext" id="Modified" value="Modified" />
			          	<input type="submit" name="btnOtherRefund" class="smltext" id="OtherRefund" value="OtherRefund" />
                        <br>
						<input type="submit" name="btnBackOrder" class="smltext" value="Send BACK-ORDER Notification"><br>
                        <input type="submit" name="btnNoItem" class="smltext" value="ITEM NO LONGER Avaliable Note">
					</td>
				</tr>
				<%
		end select
		%>
		</form>
	</table>
	<%
end if

if isObject(RS) then
	RS.Close
	SET RS = NOTHING
end if

sub UpdateInvQty(nPartNumber, nProdQuantity, KIT)
	' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
	' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
	if isNull(KIT) then
		SQL = "SELECT itemID,typeID,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND inv_qty > -1"
	else
		SQL = "SELECT itemID,typeID,inv_qty FROM we_items WHERE itemID IN (" & KIT & ")"
	end if
	set oRsItemInfo2 = Server.CreateObject("ADODB.Recordset")
	oRsItemInfo2.open SQL, oConn, 3, 3
	do until oRsItemInfo2.eof
		if oRsItemInfo2("inv_qty") + nProdQuantity > 0 then
			decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty + " & nProdQuantity & " WHERE itemID = '" & oRsItemInfo2("itemID") & "'"
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
		else
			decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE PartNumber = '" & nPartNumber & "'"
			if oRsItemInfo2("typeID") = 2 then decreaseSQL = decreaseSQL & " AND inv_qty > -1"
			if oRsItemInfo2("typeID") <> 3 then
				' Send zero-inventory e-mail to Tony
				cdo_from = "Automatic E-Mail from CellularOutfitter.com<sales@CellularOutfitter.com>"
				cdo_subject = nPartNumber & " is out of stock!"
				cdo_body = nPartNumber & " is out of stock!"
				cdo_to = "tony@wirelessemporium.com,charles@wirelessemporium.com,leanne@wirelessemporium.com,armando@wirelessemporium.com,erica@wirelessemporium.com"
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			end if
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
		end if
		oRsItemInfo2.movenext
	loop
	oRsItemInfo2.close
	set oRsItemInfo2 = nothing
end sub

sub NumberOfSales(nIdProd, QTY, KIT)
	if isNull(KIT) then
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID = '" & nIdProd & "'"
	else
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID IN (" & KIT & ")"
	end if
	'response.write "<li>" & SQL & "</li>" & vbcrlf
	oConn.execute(SQL)
end sub
%>

<form name="CloseWindow"><p align="center"><input type="button" onClick="window.close();" class="smltext" value="Close"></p></form>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
