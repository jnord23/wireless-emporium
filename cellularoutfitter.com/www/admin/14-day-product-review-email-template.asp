<html>
<head>
<title>Product Review from CellularOutfitter.com</title>
<base href="http://www.CellularOutfitter.com">
</head>
<body>
<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; color:#8D8D8D; text-decoration: none;">
	<tr>
		<td width="710" style="border-bottom:1px solid #ccc; padding-bottom:5px;" align="left">
        	<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:14px; font-weight: normal;">
            	<tr>
                	<td width="300" align="left">
			            <a href="http://www.cellularoutfitter.com/?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview" target="_blank" style="text-decoration:none;">
                            <img src="http://www.cellularoutfitter.com/images/review/14-day/co-logo.jpg" border="0" width="285" />
                        </a>
                    </td>
                    <td width="75" align="center" valign="middle"><div style="border-left:1px solid #ccc; border-right:1px solid #ccc; height:30px; padding-top:12px;"><a href="http://www.cellularoutfitter.com/c-3-cell-phone-covers-and-gel-skins.html?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview" target="_blank" style="text-decoration:none; font-weight:bold; color:#666;">COVERS</a></div></td>
                    <td width="90" align="center" valign="middle"><div style="border-right:1px solid #ccc; height:30px; padding-top:12px;"><a href="http://www.cellularoutfitter.com/c-2-cell-phone-chargers-and-data-cables.html?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview" target="_blank" style="text-decoration:none; font-weight:bold; color:#666;">CHARGERS</a></div></td>
                    <td width="90" align="center" valign="middle"><div style="border-right:1px solid #ccc; height:30px; padding-top:12px;"><a href="http://www.cellularoutfitter.com/c-1-cell-phone-batteries.html?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview" target="_blank" style="text-decoration:none; font-weight:bold; color:#666;">BATTERIES</a></div></td>
                    <td width="100" align="center" valign="middle"><div style="border-right:1px solid #ccc; height:30px; padding-top:12px;"><a href="http://www.cellularoutfitter.com/c-5-cell-phone-bluetooth-and-hands-free.html?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview" target="_blank" style="text-decoration:none; font-weight:bold; color:#666;">BLUETOOTH</a></div></td>
                    <td width="55" align="center" valign="middle"><div style="height:30px; padding-top:12px;"><a href="http://www.cellularoutfitter.com/c-7-cell-phone-cases-and-pouches.html?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview" target="_blank" style="text-decoration:none; font-weight:bold; color:#666;">CASES</a></div></td>
                </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td width="710" style="background-color:#217BC7; padding:15px;">
			<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:14px; font-weight: normal;">
            	<tr>
                	<td width="20" align="center"><img src="http://www.cellularoutfitter.com/images/review/14-day/white_square_bullet.gif" border="0" />
                    </td>
                    <td width="690" align="left" style="font-weight:bold; font-size:22px; color:#fff;">Lend Your Expert Advice - Writing a Review
                    </td>
                </tr>
			</table>
        </td>
	</tr>
	<tr>
		<td width="710" style="padding:30px 10px 30px 10px; color:#333; background-color:#f2f2f2; font-size:13px; text-align:justify; line-height:150%;">
			Hello !##CUSTOMER_NAME##!,
			<br><br>
			Shopping online can be a bit of a leap of faith. We here at CellularOutfitter.com want our customers shop with confidence, but we need your help. 
            A customer review is one of the most trusted sources of information available to an online shopper. That's why we are asking you to write a quick review of your recent purchase at Cellular Outfitter. 
            It only takes a couple of minutes and will go a long way towards helping your fellow online shopper.
		</td>
	</tr>
	<tr><td width="710" style="font-size:1px; background-color:#999; height:3px;">&nbsp;</td></tr>
	<tr>
		<td width="710" style="padding-bottom:20px;">
			!##ITEM_TABLE##!
		</td>
	</tr>
	<tr>
		<td width="710" style="padding:30px 10px 30px 10px; color:#333; background-color:#f2f2f2; font-size:13px; text-align:justify; line-height:150%;">
        	While you're at it feel free to review us by sending us a quick e-mail. Maybe we are doing a perfect job, or maybe there are some areas you think we could be doing a little better. 
            We want to provide the best possible shopping experience for all of our customers, and your feedback is an important part of that.
            <br /><br />
            Best Wishes,<br />
            <img src="http://www.cellularoutfitter.com/images/review/14-day/sig-tony.jpg" border="0" width="235" height="47" />
            <br /><br />
            Tony Lee,
            <br />
			Co-Founder, CellularOutfitter.com<br />
			(888) 725 - 7575
		</td>
	</tr>
	<tr>
		<td width="710" style="padding-top:10px;">
			<table width="710" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="104" height="46">
                        <img src="http://www.cellularoutfitter.com/images/email/blast/2012-01-10/co12-sm-connect.gif" alt="" width="104" height="46" border="0" style="display:block;"/></td>
                    <td width="42" height="46"><a style="text-decoration:none;" target="_blank" href="http://www.facebook.com/cellularoutfitter?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview"><img src="http://www.cellularoutfitter.com/images/email/blast/2012-01-10/co12-sm-facebook.gif" alt="" width="42" height="46" border="0" style="display:block;"/></a></td>
                    <td width="33" height="46"><a style="text-decoration:none;" target="_blank" href="http://twitter.com/celloutfitter?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview"><img src="http://www.cellularoutfitter.com/images/email/blast/2012-01-10/co12-sm-twitter.gif" alt="" width="33" height="46" border="0" style="display:block;"/></a></td>
                    <td width="47" height="46"><a style="text-decoration:none;" target="_blank" href="http://www.cellularoutfitter.com/blog/main.asp?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview"><img src="http://www.cellularoutfitter.com/images/email/blast/2012-01-10/co12-sm-blog.gif" alt="" width="47" height="46" border="0" style="display:block;"/></a></td>
                    <td colspan="6" width="293" height="46">
                        <img src="http://www.cellularoutfitter.com/images/email/blast/2012-01-10/co12-sm-null1.gif" alt="" width="293" height="46" border="0" style="display:block;"/></td>
                    <td width="191" height="46"><a style="text-decoration:none;" target="_blank" href="http://www.cellularoutfitter.com/?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview"><img src="http://www.cellularoutfitter.com/images/email/blast/2012-01-10/co12-sm-cocom.gif" alt="" width="191" height="46" border="0" style="display:block;"/></a></td>
                </tr>            
			</table>
		</td>
	</tr>
</table>
</body>
</html>