<%
pageTitle = "CO Admin Site - Main Menu"
header = 1
thisUser = Request.Cookies("admin_CO")("username")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<form name="frmTollFree" action="menu.asp" method="post">
<table width="752" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="100%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="375" valign="top" class="item-contain-black">
						<p>
							<strong><br>Admin Tasks:</strong>
							<ol class="mc-text" style="margin-top:2px;">
								<li><a href="/admin/ProcessOrders.asp">PROCESS ORDERS</a></li>
								<li><a href="/admin/custlist.asp?display=none">Customer Look Up</a></li>
								<li><a href="/admin/upload_CO_productpics.asp">Upload CO Product Pics</a></li>
							</ol>
						</p>
					</td>
					<td width="2" bgcolor="#E95516" rowspan="3"><img src="https://www.cellularoutfitter.com/images/spacer.gif" width="2" height="2" border="0"></td>
					<td width="375" valign="top" class="item-contain-black" rowspan="3">
						<blockquote>
						<!--
                        	not being used right now
                            another test
							<p>
								<strong><br>Product Lists:</strong>
								<ol class="mc-text" style="margin-top:2px;">
									<li><a href="/admin/product_tab_Amazon_Ads_CO.asp">CO Data Feed for Amazon Ads</a></li>
									<li><a href="/admin/product_csv_Fetchback_CO.asp">CO Data Feed for Fetchback</a></li>
									<li><a href="/admin/product_tab_Googlebase_CO.asp">CO Data Feed for Googlebase</a></li>
									<li><a href="/admin/product_csv_LinkConnector_CO.asp">CO Data Feed for Link Connector</a></li>
									<li><a href="/admin/product_csv_Nextag_CO.asp">CO Data Feed for NexTag</a></li>
									<li><a href="/admin/product_tab_Pepperjam_CO.asp">CO Data Feed for Pepperjam</a></li>
									<li><a href="/admin/product_tab_Shopping_CO.asp">CO Data Feed for Shopping.com</a></li>
									<li><a href="/admin/product_tab_shopzilla_CO.asp">CO Data Feed for Shopzilla.com</a></li>
									<li><a href="/admin/product_tab_generic_CO.asp">Generic CO Product List</a></li>
									<li><a href="/admin/product_csv_mediaforge_CO.asp">CO Data Feed for MediaForge</a></li>
                                    <li><a href="/admin/product_tab_bing_CO.asp">CO Data Feed for Bing</a></li>
								</ol>
							</p>
						-->	
							<strong><br>Database Updates:</strong>
							<ol class="mc-text" style="margin-top:2px;">
								<li><a href="/admin/uploadTrackingNums.asp">Upload Tracking Numbers</a></li>
								<li><a href="/admin/db_update_order_status.asp">Update Order Status</a></li>
								<li><a href="/admin/db_update_order_cancellations.asp">Update Order Cancellations</a></li>
								<li><a href="/admin/db_update_coupons.asp">Add/Edit Coupons</a></li>
								<%if thisUser = "eugeneku" or thisUser = "YRL_tony" or thisUser = "johnw" or thisUser = "rosemariep" or thisUser = "kristenr" or thisUser = "rubenl" or thisUser = "charlesa" or thisUser = "ericac" or thisUser = "armandol" or thisUser = "mariog" or thisUser = "michaelc" then%>
									<li><a href="/admin/db_update_ebillme_orders.asp">Update eBillme Orders</a></li>
								<%end if%>
								<li><a href="/admin/db_update_faq.asp">Update FAQs</a></li>
								<li><a href="/admin/db_update_newarrivals.asp">Update New Arrivals</a></li>
								<li><a href="/admin/db_update_product_spotlight.asp">Update Product Spotlight</a></li>
							</ol>						
						</blockquote>
						<blockquote>
							<strong><br>Product Lists:</strong>
							<ol class="mc-text" style="margin-top:2px;">
								<li><a href="/admin/datafeeds.asp">Links to the DataFeeds</a></li>							
							</ol>						
						</blockquote>						
					</td>
				</tr>
				
				<tr>
					<td bgcolor="#E95516" height="2"><img src="https://www.cellularoutfitter.com/images/spacer.gif" width="2" height="2" border="0"></td>
				</tr>
				<tr>
					<td width="375" valign="top" class="item-contain-black">
						<p>
							<strong><br>Reports:</strong>
							<ol class="mc-text" style="margin-top:2px;">
								<li><a href="/admin/report_OrdersStats.asp">Daily Order Statistics</a></li>
								<li><a href="/admin/report_Whales.asp">Large Order Report</a></li>
								<li><a href="/admin/subtotals.asp">Weekly Subtotal Report</a></li>
								<%if thisUser = "eugeneku" or thisUser = "johnw" or thisUser = "rosemariep" or thisUser = "kristenr" or thisUser = "kevine" or thisUser = "michaelc" then%>
									<li><a href="/admin/report_Email_Addresses.asp">E-Mail Address Export</a></li>
									<li><a href="/admin/report_email_opt_outs.asp">E-Mail Opt-Out Report</a></li>
								<%end if%>
								<%if thisUser = "eugeneku" or thisUser = "johnw" or thisUser = "rosemariep" or thisUser = "kristenr" or thisUser = "terryk" then%>
									<li><a href="/admin/report_SalesTax.asp">Sales Tax Report</a></li>
								<%end if%>
								<li><a href="/admin/report_ItemSales.asp">Item Sales Report</a></li>
								<li><a href="/admin/report_ItemSales_Top10.asp">Top 10 Item Sales Report</a></li>
								<li><a href="/admin/report_YahooCrossSells_CO.asp">CO Cross Sells Report</a></li>
								<li><a href="/admin/report_Cart_Abandonments.asp">CO Cart Abandonment Report</a></li>
								<li><a href="/admin/report_CouponStats_CO.asp">CO Coupon Statistics</a></li>
							</ol>
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3" bgcolor="#E95516" height="2"><img src="https://www.cellularoutfitter.com/images/spacer.gif" width="2" height="2" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
</form>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
