<%
pageTitle = "Top 10 Item Sales Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<%
if request.form("submitted") <> "" then
	dim strError, dateStart, dateEnd, TypeID
	strError = ""
	dateStart = request.form("dateStart")
	dateEnd = request.form("dateEnd")
	TypeID = request.form("TypeID")
	
	if not isDate(dateStart) or not isDate(dateEnd) then
		strError = strError & "<p>dateStart and dateEnd must both be valid dates</p>"
	else
		dateStart = dateValue(dateStart)
		dateEnd = dateValue(dateEnd)
		if dateEnd < dateStart then strError = strError & "<p>dateEnd must be later than or equal to dateStart</p>"
	end if
	
	showblank = "&nbsp;"
	shownull = "-null-"
	
	if strError = "" then
		response.write "<p class=""biggerText"">" & pageTitle & " for Date Range:<br>"
		response.write dateStart & " &#150; " & dateEnd & "</p>" & vbcrlf
		if TypeID <> "" then
			SQL = "SELECT typeName FROM we_types WHERE typeID = '" & TypeID & "'"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if not RS.eof then response.write "<p class=""biggerText"">Category: " & RS("typeName") & "</p>" & vbcrlf
		end if
		
		dateEnd = dateAdd("D",1,dateEnd)
		SQL = "SELECT B.itemID, B.quantity, C.itemDesc_CO, C.price_CO"
		SQL = SQL & " FROM (we_Orders A INNER JOIN we_orderdetails B ON A.orderID=B.orderID)"
		SQL = SQL & " INNER JOIN we_Items C ON B.itemID=C.itemID"
		SQL = SQL & " WHERE A.orderdatetime >= '" & dateStart & "' AND A.orderdatetime <= '" & dateEnd & "'"
		if TypeID <> "" then SQL = SQL & " AND C.typeID = '" & TypeID & "'"
		SQL = SQL & " AND A.store = 2"
		SQL = SQL & " ORDER BY B.itemID"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		
		response.write "<p><a href=""/admin/report_ItemSales_Top10.asp"">Back to Report Criteria</a></p>" & vbcrlf
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			SQL = "DELETE FROM temp_ItemSales_Top10 WHERE store = 2"
			oConn.execute SQL
			
			holdItemID = RS("itemID")
			quantity = RS("quantity")
			RS.movenext
			
			do until RS.eof
				if RS("itemID") <> holdItemID then
					SQL = "INSERT INTO temp_ItemSales_Top10 (store,itemID,quantity) VALUES (2,'" & holdItemID & "','" & quantity & "')"
					'response.write "<p>" & SQL & "</p>" & vbCrLf
					oConn.execute SQL
					holdItemID = RS("itemID")
					quantity = RS("quantity")
				else
					quantity = quantity + RS("quantity")
				end if
				RS.movenext
			loop
			
			SQL = "SELECT TOP 10 * FROM temp_ItemSales_Top10 WHERE store = 2 ORDER BY quantity DESC"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			%>
			<table border="1" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td><b>itemID</b></td>
					<td><b>quantity</b></td>
					<td><b>price_CO</b></td>
					<td><b>total</b></td>
					<td><b>itemDesc_CO</b></td>
				</tr>
				<%
				do until RS.eof
					SQL = "SELECT itemid,price_CO,itemDesc_CO FROM we_items WHERE itemid='" & RS("itemID") & "'"
					set RS2 = Server.CreateObject("ADODB.Recordset")
					RS2.open SQL, oConn, 3, 3
					%>
					<tr>
						<td valign="top"><%=RS("itemID")%></td>
						<td valign="top" align="center"><%=RS("quantity")%></td>
						<td valign="top" align="center"><%=formatCurrency(RS2("price_CO"))%></td>
						<td valign="top" align="center"><%=formatCurrency(RS("quantity") * RS2("price_CO"))%></td>
						<td valign="top"><%=RS2("itemDesc_CO")%></td>
					</tr>
					<%
					RS2.close
					set RS2 = nothing
					RS.movenext
				loop
				%>
			</table>
			<br>
			<p><a href="/admin/report_ItemSales_Top10.asp">Back to Report Criteria</a></p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<%
		end if
		
		RS.close
		set RS = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
else
	%>
	<form action="report_ItemSales_Top10.asp" method="post">
		<p><input type="text" name="dateStart">&nbsp;&nbsp;dateStart</p>
		<p><input type="text" name="dateEnd">&nbsp;&nbsp;dateEnd</p>
		<h3>Select a Type:</h3>
		<p>
			<select name="TypeID">
				<option value=""></option>
				<%
				SQL = "SELECT * FROM WE_Types ORDER BY TypeName"
				set RS = Server.CreateObject("ADODB.Recordset")
				RS.open SQL, oConn, 3, 3
				do until RS.eof
					%>
					<option value="<%=RS("TypeID")%>"><%=RS("TypeName")%></option>
					<%
					RS.movenext
				loop
				RS.close
				set RS = nothing
				%>
			</select>
		</p>
		<p><input type="submit" name="submitted" value="Generate Report"></p>
	</form>
	<%
end if
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
