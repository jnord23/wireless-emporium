<%header = 0%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->

<%
Dim OrderID, txtcredit
OrderID = Request.QueryString("OrderID")
If OrderID = "" Then Request.Form("ORDERID")
txtcredit = Request.Form("txtcredit")
strEmail = Request.Form("txtEmail")

if OrderID = "" then
	response.write "<b><font color='red'>MISSING ORDER RECORD ID. PLEASE CLICK ON BACK BUTTON TO GO BACK TO PREVIOUS PAGE.</font></b>"
end if

Dim strMsg
If Request.Form("hidSave") <> "" Then
	dim STRcr, STRtext, STRbody
	STRcr = Request.Form("txtcredit")
	if trim(STRcr) <> "" then
		STRtext = formatNotes("Credit issued for $" & formatnumber(STRcr,2) & ". Email sent to customer.")
	else
		STRtext = ""
	end if
	
	oConn.begintrans

	dim rst
	set rst = server.createobject("Adodb.Recordset")
	rst.Open "SELECT Ordernotes FROM we_ordernotes WHERE OrderID=" & Cdbl(OrderID), oConn
	if not rst.eof then
		strm = rst("Ordernotes")
	else
		strm = "EOF"
	end if
	set rst = nothing
	
	if strm = "EOF" then
		oConn.Execute "INSERT INTO we_ordernotes(OrderID,Ordernotes) VALUES ('" & Cdbl(OrderID) & "','" & STRtext & "')"
	elseif strm <> "" then
		oConn.Execute "UPDATE we_ordernotes SET Ordernotes= '" & strm & STRtext & "' WHERE OrderID=" & Cdbl(OrderID)
	else
		oConn.Execute "UPDATE we_ordernotes SET Ordernotes= '" & STRtext & "' WHERE OrderID=" & Cdbl(OrderID)
	end if
	
	oConn.committrans
	'To get to email id
	dim RSTfetch, STRto
	set RSTfetch = server.createobject("Adodb.Recordset")
	RSTfetch.open "SELECT EMAIL FROM CO_Accounts A, WE_ORDERS O where A.ACCOUNTID = O.ACCOUNTID AND O.ORDERID=" & OrderID, oConn
	if not rstfetch.eof then
		if not isnull(rstfetch(0)) then STRto = rstfetch(0)
	end if
	RSTfetch.close
	set RSTfetch = nothing
	if trim(STRto) <> "" then
		'mail
		STRbody = "<tr><td>Hello and thank you for shopping with Cellular Outfitter.</td></tr><br><br>"
		'STRbody = STRbody & "<tr><td>This is an email confirmation indicating that we have received your return and have credited your account for <b>$" &  formatnumber(STRcr,2) & "</b>. Please allow a few days for posting.</td></tr><br><br>"
		'STRbody = STRbody & "<tr><td>We are sorry that the items did not work out for you and hope that you would consider us for your future cell phone accessory needs.</td></tr><br><br>"
		STRbody = STRbody & "<tr><td>This is an email confirmation indicating that we have received your request for credit and have processed your credit in the amount of <b>$" &  formatnumber(STRcr,2) & "</b>. Please allow a few days for posting.</td></tr><br><br>"
		STRbody = STRbody & "<tr><td>We appreciate your patience and hope that you will consider Cellular Outfitter for your future cell phone accessory needs.</td></tr><br><br>"
		STRbody = STRbody & "<tr><td>Warm regards,</td></tr><br><br>"
		STRbody = STRbody & "<tr><td><font COLOR=gray><b>Cellular </b></font>"
		STRbody = STRbody & "<font color=#FF8100><b>Outfitter</b></font>"
		STRbody = STRbody & "<b>, Inc.</b></td></tr><br>"
		STRbody = STRbody & "<tr><td><B><a href='mailto:support@cellularoutfitter.com'>support@cellularoutfitter.com</a></td></tr><br>"
		STRbody = STRbody & "<tr><td><b><a href='http://www.CellularOutfitter.com'>http://www.CellularOutfitter.com</a></td></tr>"
		CDOSend strTo,"support@cellularoutfitter.com","Your Account Has Been Credited",strBody
	end if
	response.write "<script>self.close()</script>"
Else
	Set objRs = Server.CreateObject("ADODB.RECORDSET")
	objRs.Open "SELECT ordernotes FROM we_ordernotes WHERE orderid=" & OrderID, oConn
	If Not objRs.EOF Then txtNotes = objRs.Fields("ordernotes").value
	objRs.Close
	Set objRs = Nothing
End If
%>

<form name="frmordernotes" method="post" onload="document.frmordernotes.txtcredit.focus();">
	<table width="100%" border="1" cellspacing="0" cellpadding="2" align="center">
		<tr class="bigText" bgcolor="#CCCCCC">
			<td colspan="3" align="center"> Enter Credit Amount For Order Id : <%=OrderID%> </td>
		</tr>
		<tr class="bigText">
			<td colspan="2" align="left"> How much amount you want to credit ?:</td>
		</tr>
		<%If strMsg <> "" Then%>
			<tr class="smlText">
				<td colspan="2" align="center"><b><%=strMsg%></b></td>
			</tr>
		<%End If%>
		<tr class="smlText">
			<td valign="top" align="center">Credit Amount $ :</td>
			<td valign="top" align="left"><input type="text" name="txtcredit" value="" onKeyPress="OnlyDigits();"> (eg $ 9.99)</td>
		</tr>
		<tr class="smlText">
			<td colspan="2" align="center">
				<input type="submit" name="btnsave" class="smltext" value="Submit">&nbsp;&nbsp;
				<input type="button" name="btncancel" class="smltext" value="Cancel" onClick="self.close();">
				<input type="hidden" name="ORDERID" value="<%=OrderID%>">
				<input type="hidden" name="hidsave" value="true">
			</td>
		</tr>
	</table>
</form>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language=javascript>
function OnlyDigits(){
	if (((window.event.keyCode<47)||(window.event.keyCode>58)) && window.event.keyCode!=46){
		window.event.keyCode="0";
	}
}
document.frmordernotes.txtcredit.focus();
</script>
