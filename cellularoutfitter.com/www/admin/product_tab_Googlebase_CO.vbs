set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim oXMLHTTP : set oXMLHTTP = CreateObject("Msxml2.ServerXMLHTTP")

dim fs, file, filename, path
filename = "COGoogleBase.txt"
path = "c:\Inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\" & filename
imgAbsolutePathCO = "c:\inetpub\wwwroot\productpics_co\"
imgAbsolutePathWE = "c:\inetpub\wwwroot\productpics\"
'path = server.mappath("\tempCSV\" & filename)

set fs = CreateObject("Scripting.FileSystemObject")
if fs.FileExists(path) then fs.DeleteFile(path)
set file = fs.CreateTextFile(path)

dim expDate, myMonth, myDay, myYear
expDate = dateAdd("d",21,date)
myMonth = month(expDate)
if myMonth < 10 then myMonth = "0" & myMonth
myDay = day(expDate)
if myDay < 10 then myDay = "0" & myDay
if myHour < 10 then myHour = "0" & myHour
myYear = year(expDate)
expDate = myYear & "-" & myMonth & "-" & myDay

dim SQL, RS
dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline
sql	=	"exec feed_googlebaseCO"

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if not RS.eof then
	file.WriteLine "title" & vbtab & "description" & vbtab & "c:description2" & vbtab & "id" & vbtab & "link" & vbtab & "quantity" & vbTab & "shipping_weight" & vbtab & "image_link" & vbtab & "price" & vbtab & "c:retail_price" & vbtab & "product_type" & vbtab & "brand" & vbtab & "c:cell_phone_model" & vbtab & "condition" & vbtab & "availability" & vbTab & "payment_accepted" & vbtab & "mpn" & vbtab & "color" & vbtab & "year" & vbtab & "c:GoogleAffiliateNetworkProductURL:String" & vbTab & "google_product_category" & vbTab & "adwords_labels" & vbTab & "adwords_grouping" & vbTab & "identifier_exists" & vbTab & "additional_image_link" & vbTab & "color" & vbTab & "Custom Label 0" & vbTab & "Custom Label 1" & vbTab & "Custom Label 2" & vbTab & "Custom Label 3" & vbTab & "Custom Label 4" & vbTab & "Promotion ID" & vbTab & "gtin"
	do until RS.eof
		priceRange = rs("priceRange")
		salesVelocity = rs("salesVelocity")
		partnumber = RS("MPN")
		google_promotion_id = rs("google_promotion_id")
		upc = rs("upccode")
		pt = rs("google_product_type")
		gpc = rs("google_product_category")

		productImageURL = "http://www.cellularoutfitter.com/productpics/big/zoom/" & RS("itempic_CO")
		if not fs.FileExists(imgAbsolutePathCO & "big\zoom\" & RS("itempic_CO")) then
			productImageURL = "http://www.cellularoutfitter.com/productpics/big/" & RS("itempic_CO")
		end if
		
		if fs.FileExists(imgAbsolutePathCO & "big\" & RS("itempic_CO")) then
			if isnull(RS("typeName")) then
				stypeName = "Universal"
			else 
				stypeName = RS("typeName")
			end if
			if isnull(RS("subtypename")) then
				subtypename = "Universal"
			else 
				subtypename = RS("subtypename")
			end if
			if isnull(RS("ModelName")) then
				ModelName = "Universal"
			else
				ModelName = RS("ModelName")
			end if
			if isnull(RS("BrandName")) then
				BrandName = "Universal"
			else 
				BrandName = RS("BrandName")
			end if
			
			'--color filter
			my_desc = Ucase(replace(RS("itemDesc_CO"),"Blackberry",""))
			my_desc = Ucase(replace(my_desc,"Blackjack",""))
			my_desc = Ucase(replace(my_desc,"Textured",""))
			my_desc = Ucase(replace(my_desc,"Red Sox",""))
			my_desc = Ucase(replace(my_desc,"Blue Jays",""))
			
			if stypeName = "Batteries" or stypeName = "Hands-Free" then
				mycolor = ""
			else
				if inStr(My_desc,"BLACK") > 0 then
					mycolor = "BLACK"
				elseif inStr(My_desc,"BABY BLUE") > 0 then
					mycolor = "BABY BLUE"
				elseif inStr(My_desc,"GREEN") > 0 then
					mycolor = "GREEN"
				elseif inStr(My_desc,"PURPLE") > 0 then
					mycolor = "PURPLE"
				elseif inStr(My_desc,"RED") > 0 then
					mycolor = "RED"
				elseif inStr(My_desc,"HOT PINK") > 0 then
					mycolor = "HOT PINK"
				elseif inStr(My_desc,"PINK") > 0 then
					mycolor = "PINK"
				elseif inStr(My_desc,"BLUE") > 0 then
					mycolor = "BLUE"
				elseif inStr(My_desc,"MAGEBTA") > 0 then
					mycolor = "MAGENTA"
				elseif inStr(My_desc,"YELLOW") > 0 then
					mycolor = "YELLOW"
				elseif inStr(My_desc,"TURQUOISO") > 0 then
					mycolor = "TURQUOISO"
				elseif inStr(My_desc,"BROWN") > 0 then
					mycolor = "BROWN"
				elseif inStr(My_desc,"WHITE") > 0 then
					mycolor = "WHITE"
				elseif inStr(My_desc,"BEIGE") > 0 then
					mycolor = "BEIGE"
				elseif inStr(My_desc,"CLEAR") > 0 then
					mycolor = "CLEAR"
				elseif inStr(My_desc,"SMOKE") > 0 then
					mycolor = "SMOKE"
				elseif inStr(My_desc,"SILVER") > 0 then
					mycolor = "SILVER"
				else
					if stypeName = "Faceplates" then
						mycolor = "MULTI COLORS"
					else
						mycolor = "BLACK"
					end if
				end if
			end if
			'--end of color filter
			
			strItemLongDetail = RS("itemLongDetail_CO")

			if not isNull(RS("POINT1")) then strItemLongDetail = strItemLongDetail & " - " & RS("POINT1")
			if not isNull(RS("POINT2")) then strItemLongDetail = strItemLongDetail & " - " & RS("POINT2")
			if not isNull(RS("POINT3")) then strItemLongDetail = strItemLongDetail & " - " & RS("POINT3")
			if not isNull(RS("POINT4")) then strItemLongDetail = strItemLongDetail & " - " & RS("POINT4")
			if not isNull(RS("POINT5")) then strItemLongDetail = strItemLongDetail & " - " & RS("POINT5")
			if not isNull(RS("POINT6")) then strItemLongDetail = strItemLongDetail & " - " & RS("POINT6")
			if not isNull(RS("COMPATIBILITY")) then strItemLongDetail = strItemLongDetail & " - " & RS("COMPATIBILITY")
			if not isNull(RS("POINT7")) then strItemLongDetail = strItemLongDetail & " - " & RS("POINT7")

			strItemLongDetail = replace(strItemLongDetail,chr(34),"''")
			sTypeName_nos = Left(sTypeName,Len(sTypeName)-1)
	'		GRPNAME = BrandName & " " & ModelName & " " & nameSEO(sTypeName) & " - " & RS("itemDesc_CO")
			GRPNAME = RS("itemDesc_CO")
			if len(GRPNAME) > 69 then GRPNAME = left(GRPNAME, 66) & "..."
			
			strItemLongDetail_2 = "Get great offers on quality " & BrandName & " " & singularSEO(stypeName) & " to complement your "
			strItemLongDetail_2 = strItemLongDetail_2 & BrandName & " " & ModelName & ". Huge collection of "
			strItemLongDetail_2 = strItemLongDetail_2 & BrandName & " " & nameSEO(stypeName) & " for "
			strItemLongDetail_2 = strItemLongDetail_2 & BrandName & " " & ModelName & " is available at #1 cell phone accessory store online. "
			strItemLongDetail_2 = strItemLongDetail_2 & "Take a look around our website to find " & BrandName & " " & singularSEO(stypeName)
			strItemLongDetail_2 = strItemLongDetail_2 & " that best suits your " & BrandName & " " & ModelName & "."
			
			id_new_field = formatSEO(GRPNAME)
			priceCO = formatNumber(cdbl(RS("price_CO")))
			PriceRetail = formatNumber(cdbl(RS("price_Retail")))
			
			if inStr(Lcase(strItemLongDetail),"refurbished") > 0 or inStr(Lcase(RS("itemDesc_CO")),"refurbished") > 0 then
				Condition = "refurbished"
			else
				Condition = "new"
			end if
	
			strItemLongDetail = replace(strItemLongDetail, "&amp;", "&")
			strItemLongDetail = replace(strItemLongDetail_2, "&amp;", "&")
	
			strline = GRPNAME & vbtab
			strline = strline & chr(34) & removeHTML(stripCallToAction(replace(strItemLongDetail,"?","-"))) & chr(34) & vbtab
			strline = strline & chr(34) & removeHTML(stripCallToAction(replace(strItemLongDetail_2,"?","-"))) & chr(34) & vbtab
			strline = strline & RS("itemid") & vbtab
			strline = strline & "http://www.CellularOutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html?DZID=PPC_Google_PLA_{adid}_" & RS("itemid") & vbTab
			strline = strline & "1000" & vbtab
			strline = strline & RS("itemweight") & vbtab		
			strline = strline & productImageURL & vbtab
			strline = strline & priceCO & vbtab
			strline = strline & PriceRetail & vbtab
			strline = strline & pt & vbtab
			strline = strline & BrandName & vbtab
			strline = strline & ModelName & vbtab
			strline = strline & Condition & vbtab
			strline = strline & "in stock" & vbtab
			strline = strline & "Visa,MasterCard,AmericanExpress,Discover" & vbtab
			'strline = strline & expDate & vbtab
			strline = strline & partnumber & vbtab
			strline = strline & mycolor & vbtab ' need filter out from description
			strline = strline & year(date) & vbtab
			strline = strline & "http://www.CellularOutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html?DZID=PPC_Google_PLA_{adid}_" & RS("itemid") & vbTab
			strline = strline & gpc & vbtab
			strline = strline & replace(nameSEO(stypeName), "&amp;", "&") & vbtab
			strline = strline & ModelName & vbtab
			strline = strline & "TRUE" & vbTab
			
			strAltViews = ""
			for iCount = 0 to 7
				altPic = replace(RS("itempic"),".jpg","-" & iCount & ".jpg")
				if fs.FileExists(imgAbsolutePathWE & "altviews\zoom\" & altPic) then
					if strAltViews = "" then
						strAltViews = "http://www.cellularoutfitter.com/altPics/zoom/" & altPic
					else
						strAltViews = strAltViews & ",http://www.cellularoutfitter.com/altPics/zoom/" & altPic						
					end if
				elseif fs.FileExists(imgAbsolutePathWE & "altviews\" & altPic) then
					if strAltViews = "" then
						strAltViews = "http://www.cellularoutfitter.com/altPics/" & altPic
					else
						strAltViews = strAltViews & ",http://www.cellularoutfitter.com/altPics/" & altPic						
					end if
				end if				
			next			
			strline = strline & strAltViews & vbTab
			strline = strline & rs("color") & vbTab
			strline = strline & ModelName & vbTab
			strline = strline & stypename & vbTab
			strline = strline & subtypename & vbTab
			strline = strline & priceRange & vbTab
			strline = strline & salesVelocity & vbTab
			strline = strline & google_promotion_id & vbTab
			strline = strline & upc			
			
			if len(strItemLongDetail) > 0 then
				file.WriteLine strline
			end if
		end if
				
		RS.movenext
	loop
end if
file.close()

path = "c:\Inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\COGoogleBase_coupons.txt"
if fs.FileExists(path) then fs.DeleteFile(path)
set file = fs.CreateTextFile(path)

set rs = rs.nextrecordset

headings = "promotion_id" & vbTab & "product_applicability" & vbTab & "long_title" & vbTab & "short_title" & vbTab & "promotion_effective_dates" & vbTab & "redemption_channel" & vbTab & "merchant_logo" & vbTab & "offer_type" & vbTab & "generic_redemption_code"

if not RS.eof then
	file.WriteLine headings
	do until RS.eof
		promoDates = rs("promotion_effective_date") & "T00:00:00-08:00/" & rs("expiration_date") & "T00:00:00-08:00"
		
		strline = rs("promotion_id") & vbTab
		strline = strline & rs("product_applicability") & vbTab
		strline = strline & rs("long_title") & vbTab
		strline = strline & rs("short_title") & vbTab
		strline = strline & promoDates & vbTab
		strline = strline & rs("redemption_channel") & vbTab
		strline = strline & rs("merchant_logo") & vbTab
		strline = strline & rs("offer_type") & vbTab
		strline = strline & rs("promoCode") & vbTab
		
		file.WriteLine strline
		RS.movenext
	loop
end if
file.close()


function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(formatSEO, "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function

function nameSEO(val)
	select case val
		case "Antennas/Parts" : nameSEO = "Antennas"
		case "Faceplates" : nameSEO = "Covers &amp; Screen Guards"
		case "Data Cable & Memory" : nameSEO = "Data Cables &amp; Memory Cards"
		case "Hands-Free" : nameSEO = "Hands-Free Kits &amp; Bluetooth Headsets"
		case "Holsters/Belt Clips" : nameSEO = "Holsters, Belt Clips &amp; Holders"
		case "Leather Cases" : nameSEO = "Cases &amp; Pouches"
		case "Cell Phones" : nameSEO = "Wholesale Unlocked Cell Phones"
		case "Full Body Protectors" : nameSEO = "Invisible Film Protectors"
		case else : nameSEO = val
	end select
end function

function singularSEO(val)
	select case val
		case "Batteries" : singularSEO = "Battery"
		case "Chargers" : singularSEO = "Charger"
		case "Faceplates" : singularSEO = "Cover &amp; Screen Guard"
		case "Hands-Free" : singularSEO = "Hands Free / Bluetooth Headset"
		case "Holsters/Belt Clips" : singularSEO = "Holster / Belt Clip / Holder"
		case "Leather Cases" : singularSEO = "Case / Pouch"
		case "Antennas/Parts" : singularSEO = "Antenna"
		case "Data Cable & Memory" : singularSEO = "Data Cable / Memory Card"
		case "Bling Kits & Charms" : singularSEO = "Bling Kit / Charm"
		case "Cell Phones" : singularSEO = "Wholesale Unlocked Cell Phone"
		case "Full Body Protectors" : singularSEO = "Invisible Film Protector"
		case else : singularSEO = val
	end select
end function

function stripCallToAction(val)
	stripCallToAction = val
	dim a
	a = inStr(1,stripCallToAction,"why pay ridiculous prices",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"don't settle",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"order this quality",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"order this convenient",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this quality",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this convenient",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"order today",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"remember - wireless emporium",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"you won't find a better price",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this stylish",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this vertical",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"for the same product at",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"we're confident you'll be happy",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	stripCallToAction = replace(stripCallToAction,"don't pay retail prices","",1,9,1)
end function

function removeHTML(str)
	removeHTML = replace(str,"<br>"," ",1,99,1)
	removeHTML = replace(replace(removeHTML,"<p>"," ",1,99,1),"</p>"," ",1,99,1)
	removeHTML = replace(replace(removeHTML,"<b>","",1,99,1),"</b>","",1,99,1)
	removeHTML = replace(removeHTML,"<b class='bigtext'>","",1,99,1)
'	removeHTML = replace(replace(removeHTML," - ","",1,99,1),"</li>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<ul>","",1,99,1),"</ul>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<font color=""#FF0000"">","",1,99,1),"</font>","",1,99,1)
end function

function hasImageOnRemote(pRemoteURL)
	dim isExists : isExists = false
	
	oXMLHTTP.Open "HEAD", pRemoteURL, False	'communicating without responseBody, only looks up file header
	oXMLHTTP.Send
	
	if (oXMLHTTP.readyState = "4") and (oXMLHTTP.status = "200") and (instr(oXMLHTTP.getResponseHeader("Content-Type"), "image") > 0) then isExists = true
	
	hasImageOnRemote = isExists
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub