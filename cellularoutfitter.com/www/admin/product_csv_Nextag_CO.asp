<%header = 1%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->

<%
Server.ScriptTimeout = 4000 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_Nextag_CO.csv"
'path = Server.MapPath("tempCSV") & "\" & filename
'path = replace(path,"admin\","")
'path = "E:\Inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename
path = server.mappath("\tempCSV\" & filename)
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Cellular Outfitter Product List Upload for NexTag</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							Response.Write("Creating " & filename & ".<br>")
							Response.Write("<b>CreateFile:</b><br>")
							set fs = CreateObject("Scripting.FileSystemObject")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href=""http://www.wirelessemporium.com/tempCSV/" & filename & """>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.Write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	set file = fs.CreateTextFile(path)
	SQL = "SELECT A.itemID,A.PartNumber AS MPN,A.itemDesc_CO,A.itemLongDetail_CO,A.POINT1,A.POINT2,A.POINT3,A.POINT4,A.POINT5,A.POINT6,A.COMPATIBILITY,A.POINT7," & vbCRLF
	SQL = SQL & " A.flag1,A.UPCCode,A.itempic_CO,A.price_Retail,A.price_CO,A.brandID,A.inv_qty," & vbCRLF
	SQL = SQL & " B.brandName,C.modelName,C.[temp],D.typeName" & vbCRLF
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)" & vbCRLF
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)" & vbCRLF
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID" & vbCRLF
	SQL = SQL & " WHERE A.price_CO > 0 AND A.hideLive = 0 AND A.inv_qty <> 0" & vbCRLF
	SQL = SQL & " AND A.typeid <> 16" & vbCRLF
'	SQL = SQL & " AND (A.itemID IN (20107,23345,24244,24245,29878,31114,36425,49909,49912,50034,51924,55163,56513,56515,31416,34769,56838)"
'	SQL = SQL & " OR (A.PartNumber NOT LIKE 'DEC%' AND A.PartNumber NOT LIKE 'FP2%' AND A.PartNumber NOT LIKE 'FP3%' AND A.PartNumber NOT LIKE 'FP5%' AND A.PartNumber NOT LIKE 'BT1%'"
'	SQL = SQL & " AND A.PartNumber NOT LIKE 'LC0%' AND A.PartNumber NOT LIKE 'LC1%' AND A.PartNumber NOT LIKE 'LC2%' AND A.PartNumber NOT LIKE 'LC3%' AND A.PartNumber NOT LIKE 'LC4%' AND A.PartNumber NOT LIKE 'LC5%'"
'	SQL = SQL & " AND A.PartNumber NOT LIKE 'HF1%' AND A.PartNumber NOT LIKE 'HF2%' AND A.PartNumber NOT LIKE 'HF3%'"
'	SQL = SQL & " AND A.PartNumber NOT LIKE 'FP4%' AND A.PartNumber NOT LIKE 'FP6%' AND A.PartNumber NOT LIKE 'FP7%' AND A.PartNumber NOT LIKE 'FP8%' AND A.PartNumber NOT LIKE 'FP9%'"
'	SQL = SQL & " AND (A.UPCCode IS NULL OR A.UPCCode = '')))"
	SQL = SQL & " AND (A.UPCCode IS NULL OR A.UPCCode = '')" & vbCRLF
	SQL = SQL & " ORDER BY A.itemID" & vbCRLF
	set RS = Server.CreateObject("ADODB.Recordset")

	SQL = " select * from _temp_nexttag_co "
		
'	response.write "<pre>" & SQL & "</pre>"
'	response.end
	
	
	
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "Manufacturer,Manufacturer Part #,Product Name,Product Description,Click-Out URL,Price,Category: Other Format,Category: NexTag Numeric ID,Image URL,Ground Shipping,Stock Status,Product Condition,Marketing Message,Weight,Cost-per-Click,UPC,Distributor ID,MUZE ID,ISBN"
		do until RS.eof
			DoNotInclude = 0
'			if RS("inv_qty") < 0 then
'				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
'				set RS2 = CreateObject("ADODB.Recordset")
'				RS2.open SQL, oConn, 3, 3
'				if RS2.eof then DoNotInclude = 1
'				RS2.close
'				set RS2 = nothing
'			end if
			if DoNotInclude = 0 then
				if RS("price_CO") >= 5 and not isNull(RS("itemDesc_CO")) and RS("itemDesc_CO") <> "" then
					if isnull(RS("itemDesc_CO")) then
						itemDesc_CO = "Universal"
					else 
						itemDesc_CO = RS("itemDesc_CO")
					end if
					if isnull(RS("typeName")) then
						stypeName = "Universal"
					else 
						stypeName = RS("typeName")
					end if
					if isnull(RS("ModelName")) then
						ModelName = "Universal"
					else 
						ModelName = RS("ModelName")
					end if
					if isnull(RS("BrandName")) then
						BrandName = "Universal"
					else 
						BrandName = RS("BrandName")
					end if
					
					if not isNull(RS("itemLongDetail_CO")) or not isNull(RS("POINT1")) or not isNull(RS("POINT2")) or not isNull(RS("POINT3")) or not isNull(RS("POINT4")) or not isNull(RS("POINT5")) or not isNull(RS("POINT6")) or not isNull(RS("COMPATIBILITY")) or not isNull(RS("POINT7")) then
						strItemLongDetail = RS("itemLongDetail_CO") & "<ul>"
						if not isNull(RS("POINT1")) and RS("POINT1") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT1") & "</li>"
						if not isNull(RS("POINT2")) and RS("POINT2") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT2") & "</li>"
						if not isNull(RS("POINT3")) and RS("POINT3") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT3") & "</li>"
						if not isNull(RS("POINT4")) and RS("POINT4") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT4") & "</li>"
						if not isNull(RS("POINT5")) and RS("POINT5") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT5") & "</li>"
						if not isNull(RS("POINT6")) and RS("POINT6") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT6") & "</li>"
						if not isNull(RS("COMPATIBILITY")) and RS("COMPATIBILITY") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("COMPATIBILITY") & "</li>"
						if not isNull(RS("POINT7")) and RS("POINT7") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT7") & "</li>"
						strItemLongDetail = strItemLongDetail & "</ul>"
					else
						strItemLongDetail = RS("itemLongDetail_CO")
					end if
					if not isNull(strItemLongDetail) then strItemLongDetail = replace(strItemLongDetail,chr(34),"''")
					
					id_new_field = formatSEO(itemDesc_CO)
					priceCO = formatNumber(cdbl(RS("price_CO")),2)
					
					strline = chr(34) & BrandName & chr(34) & ","
					strline = strline & RS("itemid") & ","
					strline = strline & chr(34) & itemDesc_CO & chr(34) & ","
					strline = strline & chr(34) & strItemLongDetail & chr(34) & ","
					strline = strline & chr(34) & "http://www.CellularOutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html?refer=nextag" & chr(34) & ","					
					strline = strline & priceCO & ","
					strline = strline & ","
					strline = strline & chr(34) & "500070 : Electronics / Phones & Communications / Cell Phone Accessories" & chr(34) & ","
					strline = strline & chr(34) & "http://www.CellularOutfitter.com/productpics/big/" & RS("itempic_CO") & chr(34) & ","
					strline = strline & "0,"
					strline = strline & "Yes" & ","
					strline = strline & "New" & ","
					strline = strline & "Free Shipping on All Orders,"
					strline = strline & ","
					strline = strline & ","
					strline = strline & RS("UPCCode") & ","
					strline = strline & ","
					strline = strline & ","
					strline = strline & ","
					file.WriteLine strline
				end if
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
