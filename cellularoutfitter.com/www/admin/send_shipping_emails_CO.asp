<%
dim oConn
set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.ConnectionTimeout = 1000		'seconds
oConn.Open

dim nOrderID, bodyText, aCount
aCount = 0
SQL = "SELECT top 3 * FROM we_orders WHERE scanDate IS NOT NULL AND confirmdatetime IS NULL"
SQL = SQL & " AND orderid > 900000"
SQL = SQL & " AND store = 2"
SQL = SQL & " ORDER BY orderID"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
do until RS.eof
	nOrderID = RS("orderID")
	SQL = "UPDATE we_orders SET"
	SQL = SQL & " confirmSent = 'yes',"
	SQL = SQL & " confirmdatetime = '" & now & "'"
	SQL = SQL & " WHERE orderid = '" & nOrderID & "'"
	'oConn.execute SQL
	call SendConfirmEmails(nOrderID)
	bodyText = bodyText & "<p>Order Number " & nOrderID & " processed with Tracking Number " & RS("trackingNum") & "</p>" & vbcrlf
	aCount = aCount + 1
	RS.movenext
loop

if bodyText <> "" then
	bodyText = bodyText & "<h3>" & aCount & " Packages Processed</h3>" & vbcrlf
'	CDOSend "charles@wirelessemporium.com,ruben@wirelessemporium.com,service@wirelessemporium.com,webmaster@wirelessemporium.com","","service@wirelessemporium.com","CO Sent Packages Report for " & date,bodyText
	CDOSend "webmaster@wirelessemporium.com","ruben@wirelessemporium.com","service@wirelessemporium.com","UPS Mail Innovations Report for " & date,bodyText
end if

RS.close
set RS = nothing

sub SendConfirmEmails(nOrderID)
	dim RS2, RS3, RSOrderDetails
	dim extOrderType, nCount, sPromoCode, nOrderGrandTotal, nOrderSubTotal, nOrderTax, nShipFee
	dim storeID, sHeading, statusLink
	dim nID, nPrice, nQty
	dim sShipAddress, cdo_from, cdo_to, cdo_subject, cdo_body, regText, headerText
	if nOrderID <> "" then
		SQL = "SELECT orderID, store, accountID, shippingid, extOrderType, ordersubtotal, ordershippingfee, shiptype, orderTax, ordergrandtotal, trackingNum FROM we_Orders WHERE orderID = '" & nOrderID & "'"
		set RS3 = CreateObject("ADODB.Recordset")
		RS3.open SQL, oConn, 3, 3
		do until RS3.eof
			extOrderType = RS3("extOrderType")
			storeID = RS3("store")
			nOrderGrandTotal = RS3("ordergrandtotal")
			nOrderSubTotal = RS3("ordersubtotal")
			nOrderTax = RS3("orderTax")
			nShipFee = RS3("ordershippingfee")
			SQL = "SELECT B.PromoCode FROM we_orders A INNER JOIN CO_coupons B ON A.couponid = B.couponid WHERE A.orderid = '" & nOrderID & "'"
			set RS2 = CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if not RS2.eof then
				sPromoCode = RS2("PromoCode")
			else
				sPromoCode = ""
			end if
			
			sHeading = "CellularOutfitter.com Order #" & nOrderID & " - Shipment Status"
			
			if isNull(RS3("shippingid")) or RS3("shippingid") = 0 then
				SQL = "SELECT fname, lname, sAddress1, sAddress2, sCity, sState, sZip, sCountry, email, phone FROM CO_Accounts WHERE accountID = '" & RS3("accountID") & "'"
			else
				SQL = "SELECT A.*, B.fname, B.lname, B.email FROM we_addl_shipping_addr A INNER JOIN CO_Accounts B ON A.accountID = B.accountID WHERE A.id = '" & RS3("shippingid") & "'"
			end if
			dim fname, lname, sAddress1, sAddress2, sCity, sState, sZip
			set RS2 = CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if not RS2.eof then
				fname = RS2("fname")
				lname = RS2("lname")
				sAddress1 = RS2("sAddress1")
				sAddress2 = RS2("sAddress2")
				sCity = RS2("sCity")
				sState = RS2("sState")
				sZip = RS2("sZip")
			end if
			sShipAddress = fFormatAddress(fname,lname,sAddress1,sAddress2,sCity,sState,sZip)
			cdo_from = "CellularOutfitter<sales@CellularOutfitter.com>"
			'cdo_to = "" & RS2("email")
			cdo_to = "webmaster@wirelessemporium.com"
			cdo_subject = sHeading
			RS2.close
			set RS2 = nothing
			cdo_body = "<html>" & vbcrlf
			cdo_body = cdo_body & "<head>" & vbcrlf
			cdo_body = cdo_body & "<style type=""text/css"">" & vbcrlf
			cdo_body = cdo_body & "<!--" & vbcrlf
			cdo_body = cdo_body & ".regText {font-family: Times; font-size: 12pt;}" & vbcrlf
			cdo_body = cdo_body & ".headerText {font-family: Times; font-size: 14pt;}" & vbcrlf
			cdo_body = cdo_body & "-->" & vbcrlf
			cdo_body = cdo_body & "</style>" & vbcrlf
			cdo_body = cdo_body & "</head>" & vbcrlf
			cdo_body = cdo_body & "<body class=""regText"">"
			
			cdo_body = cdo_body & "<table width=""100%"" cellpadding=""8"" cellspacing=""0"" align=""center"" bgcolor=""#eaeaea"" class=""regText""><tr>" & vbcrlf
			cdo_body = cdo_body & "<td class=""headerText"" align=""left"" valign=""top""><img src=""http://www.cellularoutfitter.com/images/CellularOutfitter_STANDARD.jpg"" border=""0"" hspace=""12"" align=""absmiddle""></td>" & vbcrlf
			cdo_body = cdo_body & "<td class=""headerText"" align=""center"" valign=""middle""><b>" & sHeading & "</b></td>" & vbcrlf
			cdo_body = cdo_body & "</tr></table>" & vbcrlf
			
			cdo_body = cdo_body & "<table width=""95%"" cellpadding=""5"" cellspacing=""1"" align=""center"" class=""regText"">" & vbcrlf
			cdo_body = cdo_body & "<p><b>Hello " & Ucase(left(fname, 1)) & right(fname,(len(fname)-1)) & ":</b></p>"
			cdo_body = cdo_body & "<p>Thank you for ordering from CellularOutfitter.com! We would like to notify you that your order has been SHIPPED to:</p>"& vbcrlf
			cdo_body = cdo_body & "<p><b>" & sShipAddress & "</b></p>" & vbcrlf
			cdo_body = cdo_body & "<p>ORDER ID: " & nOrderID & "</p>" & vbcrlf
			
			dim sShiptype, sTrackingNum, sTrackingURL, sPleaseAllow
			sShiptype = RS3("shiptype")
			sTrackingNum = RS3("trackingNum")
			sPleaseAllow = ""
			cdo_body = cdo_body & "<p>SHIPPING TYPE: " & sShiptype & "</p>" & vbcrlf
			if not isNull(sTrackingNum) and sTrackingNum <> "" then
				if left(sTrackingNum,7) = "1Z15X4R" then
					sTrackingURL = "<a href=""http://wwwapps.ups.com/WebTracking/OnlineTool?InquiryNumber1=" & sTrackingNum & """>"
				else
					sTrackingURL = "<a href=""http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?orig&strOrigTrackNum=" & sTrackingNum & "&CAMEFROM=OK"">"
				end if
			else
				sTrackingURL = "<a href=""http://www.ups-mi.net/packageID/PackageID.aspx?PID=WE" & nOrderiD & """>"
				sPleaseAllow = " - Please allow 12-24 hours for tracking information to become available."
			end if
			cdo_body = cdo_body & "<p>SHIPMENT TRACKING: " & sTrackingURL & "Click Here</a>" & sPleaseAllow & "</p>" & vbcrlf
			
			cdo_body = cdo_body & "<p>Your order will be shipped in accordance with your selected delivery option. Arrival time for your order will vary depending on your selected delivery option, or your proximity to Southern California (for First Class Mail).</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Thanks again for your business. Should you have any additional questions regarding your order, our site and/or services, please feel free to contact us at <a href=""mailto:sales@CellularOutfitter.com"">sales@CellularOutfitter.com</a>. We are eager to serve your wireless needs again in the future.</p>"
			
			cdo_body = cdo_body & "<table width=""100%"" cellpadding=""5"" cellspacing=""5"" align=""center"" bgcolor=""#eaeaea"">" & vbcrlf
			cdo_body = cdo_body & "<tr><td colspan=""2"" class=""regText""><a href=""http://www.CellularOutfitter.com/track-your-order.asp""><b>Check Your Order Status Online</b></a>" & sPleaseAllow & "</td></tr>" & vbcrlf
			cdo_body = cdo_body & "<tr><td colspan=""2"" class=""regText""><b>Please visit us again soon!</b> - <a href=""http://www.CellularOutfitter.com/"">CellularOutfitter.com</a></td></tr>" & vbcrlf
			cdo_body = cdo_body & "</table>" & vbcrlf
			cdo_body = cdo_body & "</body></html>"
			
			if isNull(extOrderType) or extOrderType <> 6 then
				CDOSend cdo_to,"ruben@wirelessemporium.com",cdo_from,cdo_subject,cdo_body
			end if
			RS3.movenext
		loop
		RS3.close
		set RS3 = nothing
	end if
end sub

function fFormatAddress(fname,lname,sAdd1,sAdd2,sCity,sState,sZip)
	dim cdo_body
	cdo_body = fname & " " & lname & "<br>"
	cdo_body = cdo_body & sAdd1
	if sAdd2 <> "" then cdo_body = cdo_body & "<br>" & sAdd2
	cdo_body = cdo_body & "<br>" & sCity & ", " & sState & "&nbsp;" & sZip & "<br>" & vbcrlf
	fFormatAddress = cdo_body
end function

function CDOSend(strTo,strBCC,strFrom,strSubject,strBody)
	Dim objErrMail
	Set objErrMail = CreateObject("CDO.Message")
	With objErrMail
		.From = strFrom
		.To = strTo
		if strBCC <> "" then .BCC = strBCC
		.Subject = strSubject
		.HTMLBody = strBody
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	End With
	Set objErrMail = nothing
end function
%>
