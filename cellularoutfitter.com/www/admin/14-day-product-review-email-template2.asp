<html>
<head>
<title>Product Review from CellularOutfitter.com</title>
<base href="http://www.CellularOutfitter.com">
</head>
<body>
<table width="720" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; color:#333; text-decoration: none;">
	<tr>
    	<td width="100%">
			<table width="720" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; color:#333; text-decoration: none;">
            	<tr>
                	<td width="520"><img src="http://www.cellularoutfitter.com/images/email/review/ocbanner.gif" border="0" /></td>
                	<td width="200">
                        <table width="200" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; color:#333; text-decoration: none;">
                            <tr>
                            	<td align="left" colspan="4" style="font-size:20px; color:#666;">Connect With Us</td>
							</tr>
                            <tr>
                            	<td align="left"><a href="https://plus.google.com/113926800527068868377" target="_blank"><img src="http://www.cellularoutfitter.com/images/email/review/google.gif" border="0" /></a></td>
                            	<td align="center"><a href="http://www.facebook.com/cellularoutfitter" target="_blank"><img src="http://www.cellularoutfitter.com/images/email/review/FB.gif" border="0" /></a></td>
                            	<td align="center"><a href="http://www.pinterest.com/wirelessemp/" target="_blank"><img src="http://www.cellularoutfitter.com/images/email/review/pintrestlogo.jpg" border="0" /></a></td>
                            	<td align="right"><a href="https://twitter.com/celloutfitter" target="_blank"><img src="http://www.cellularoutfitter.com/images/email/review/Twitter.gif" border="0" /></a></td>
							</tr>
						</table>
                    </td>
                </tr>
            </table>
        </td>
	</tr>
	<tr>
    	<td width="100%" style="padding-top:5px;">
        	<img src="http://www.cellularoutfitter.com/images/email/review/Banner1.gif" border="0" />
        </td>
    </tr>
	<tr>
    	<td width="100%" style="padding:20px 0px 30px 0px;">
            <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0">
                <tr>
					<td width="350">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="145" style="padding:5px; border:3px solid #ccc; -moz-border-radius:5px; border-radius:5px;">
                                	<img src="http://www.cellularoutfitter.com//productPics/big/motorola-razr2-v9-ac-home-wall-charger.jpg" border="0" width="145" />
                                </td>
                                <td width="165" style="padding:5px; font-size:12px; color:#333;" align="left" valign="top">
									Samsung ATIV S Neo AC Home/Wall Charger Samsung ATIV S Neo AC Home/Wall Charger<br><br>
                                    <img src="http://www.cellularoutfitter.com/images/email/review/ClicktoRevew.gif" border="0" /><br>
									
                                    &nbsp; &nbsp; &nbsp; 
                                    <img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                </td>
							</tr>
						</table>
                    </td>
					<td width="350">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="145" style="padding:5px; border:3px solid #ccc; -moz-border-radius:5px; border-radius:5px;">
                                	<img src="http://www.cellularoutfitter.com//productPics/big/motorola-razr2-v9-ac-home-wall-charger.jpg" border="0" width="145" />
                                </td>
                                <td width="165" style="padding:5px; font-size:12px; color:#333;" align="left" valign="top">
									Samsung ATIV S Neo AC Home/Wall Charger Samsung ATIV S Neo AC Home/Wall Charger<br><br>
                                    <img src="http://www.cellularoutfitter.com/images/email/review/ClicktoRevew.gif" border="0" /><br>
									
                                    &nbsp; &nbsp; &nbsp; 
                                    <img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                </td>
							</tr>
						</table>
                    </td>
				</tr>
                <tr>
					<td width="350">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="145" style="padding:5px; border:3px solid #ccc; -moz-border-radius:5px; border-radius:5px;">
                                	<img src="http://www.cellularoutfitter.com//productPics/big/motorola-razr2-v9-ac-home-wall-charger.jpg" border="0" width="145" />
                                </td>
                                <td width="165" style="padding:5px;" align="left" valign="top">
                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="100%" align="left" valign="top">
												<a href="http://www.cellularoutfitter.com/p-262692-samsung-ativ-s-neo-ac-home-wall-charger.html?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview" target="_blank" style="font-size:12px; color:#333; text-decoration:none;">Samsung ATIV S Neo AC Home/Wall Charger Samsung ATIV S Neo AC Home/Wall Charger</a>
                                            </td>
										</tr>
                                        <tr>
                                            <td width="100%" align="center" valign="top" style="padding:20px 0px 5px 0px;">
												<a href="http://www.cellularoutfitter.com/p-262692-samsung-ativ-s-neo-ac-home-wall-charger.html?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview" target="_blank" style="text-decoration:none;"><img src="http://www.cellularoutfitter.com/images/email/review/ClicktoRevew.gif" border="0" /></a>
                                            </td>
										</tr>
                                        <tr>
                                            <td width="100%" align="center" valign="top">
	                                            <a href="http://www.cellularoutfitter.com/p-262692-samsung-ativ-s-neo-ac-home-wall-charger.html?utm_source=Email&utm_medium=FollowUp&utm_campaign=CO14DayReview" target="_blank" style="text-decoration:none;">
                                                    <img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                                    <img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                                    <img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                                    <img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                                    <img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                                </a>
                                            </td>
										</tr>
									</table>
                                </td>
							</tr>
						</table>
                    </td>
					<td width="350">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="145" style="padding:5px; border:3px solid #ccc; -moz-border-radius:5px; border-radius:5px;">
                                	<img src="http://www.cellularoutfitter.com//productPics/big/motorola-razr2-v9-ac-home-wall-charger.jpg" border="0" width="145" />
                                </td>
                                <td width="165" style="padding:5px; font-size:12px; color:#333;" align="left" valign="top">
									Samsung ATIV S Neo AC Home/Wall Charger Samsung ATIV S Neo AC Home/Wall Charger<br><br>
                                    <img src="http://www.cellularoutfitter.com/images/email/review/ClicktoRevew.gif" border="0" /><br>
									
                                    &nbsp; &nbsp; &nbsp; 
                                    <img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                </td>
							</tr>
						</table>
                    </td>
				</tr>
			</table>
        </td>
    </tr>
	<tr>
    	<td width="100%" style="padding-top:5px;">
        	<img src="http://www.cellularoutfitter.com/images/email/review/Banner2.gif" border="0" />
        </td>
    </tr>
	<tr>
    	<td width="100%" style="padding:20px 0px 30px 0px;">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
					<td width="180" align="left" valign="top">
                        <table width="175" border="0" align="left" cellpadding="5" cellspacing="0">
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
                                	<img src="http://www.cellularoutfitter.com//productPics/big/motorola-razr2-v9-ac-home-wall-charger.jpg" border="0" width="140" style="padding:5px; border:3px solid #ccc; -moz-border-radius:5px; border-radius:5px;" />
								</td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px; font-size:12px; color:#333;" align="center">
									Samsung ATIV S Neo AC Home/Wall Charger Samsung ATIV S Neo AC Home/Wall Charger
                                </td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                </td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
									<img src="http://www.cellularoutfitter.com/images/email/review/Buyitnow.gif" border="0" />
                                </td>
							</tr>
						</table>
                    </td>
					<td width="180" align="center" valign="top">
                        <table width="175" border="0" align="center" cellpadding="5" cellspacing="0">
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
                                	<img src="http://www.cellularoutfitter.com//productPics/big/motorola-razr2-v9-ac-home-wall-charger.jpg" border="0" width="140" style="padding:5px; border:3px solid #ccc; -moz-border-radius:5px; border-radius:5px;" />
								</td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px; font-size:12px; color:#333;" align="center">
									Samsung ATIV S Neo AC Home/Wall Charger Samsung ATIV S Neo AC Home/Wall Charger
                                </td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                </td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
									<img src="http://www.cellularoutfitter.com/images/email/review/Buyitnow.gif" border="0" />
                                </td>
							</tr>
						</table>
                    </td>
					<td width="180" align="center" valign="top">
                        <table width="175" border="0" align="center" cellpadding="5" cellspacing="0">
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
                                	<img src="http://www.cellularoutfitter.com//productPics/big/motorola-razr2-v9-ac-home-wall-charger.jpg" border="0" width="140" style="padding:5px; border:3px solid #ccc; -moz-border-radius:5px; border-radius:5px;" />
								</td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px; font-size:12px; color:#333;" align="center">
									Samsung ATIV S Neo AC Home/Wall Charger Samsung ATIV S Neo AC Home/Wall Charger
                                </td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                </td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
									<img src="http://www.cellularoutfitter.com/images/email/review/Buyitnow.gif" border="0" />
                                </td>
							</tr>
						</table>
                    </td>
					<td width="180" align="left" valign="top">
                        <table width="175" border="0" align="left" cellpadding="5" cellspacing="0">
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
                                	<img src="http://www.cellularoutfitter.com//productPics/big/motorola-razr2-v9-ac-home-wall-charger.jpg" border="0" width="140" style="padding:5px; border:3px solid #ccc; -moz-border-radius:5px; border-radius:5px;" />
								</td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px; font-size:12px; color:#333;" align="center">
									Samsung ATIV S Neo AC Home/Wall Charger Samsung ATIV S Neo AC Home/Wall Charger
                                </td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
									<img src="http://www.cellularoutfitter.com/images/email/review/emptystar.gif" border="0" width="24" height="23" />
                                </td>
							</tr>
                            <tr>
                                <td width="165" style="padding:5px;" align="center">
									<img src="http://www.cellularoutfitter.com/images/email/review/Buyitnow.gif" border="0" />
                                </td>
							</tr>
						</table>
                    </td>
				</tr>
			</table>
        </td>
    </tr>
	<tr>
    	<td width="100%" style="padding-top:5px;">
            <table width="720" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; color:#333; text-decoration: none;">
                <tr>
                    <td align="center" width="200" height="40" style="padding-top:5px; background-color:#35628B; color:#fff; font-weight:bold;">SHOP WITH CONFIDENCE:</td>
                    <td align="left" width="520" height="40" style="padding-top:5px; background-color:#35628B; color:#fff;">
                    	110% LOW PRICE GUARANTEE
                        &nbsp; &nbsp; &nbsp; &nbsp;
                        90-DAY RETURNS
                        &nbsp; &nbsp; &nbsp; &nbsp;
                        1-YEAR WARRANTY
					</td>
				</tr>
            </table>
        </td>
    </tr>
	<tr><td width="100%" style="height:20px;">&nbsp;</td></tr>
	<tr>
    	<td width="100%" style="border-top:2px solid #eee; padding-bottom:20px;">
            <table width="720" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; color:#777; text-decoration: none;">
                <tr>
                    <td align="left" width="150" height="40">
			            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" width="90" style="font-size:18px; color:#777;">Shop Now</td>
                                <td align="left" width="60" style="padding-top:3px;">&nbsp;<img src="/images/email/review/PlayButton.gif" border="0" /></td>
                            </tr>
                        </table>                    
                    </td>
                    <td align="left"></td>
                    <td align="left"></td>
                    <td align="right" rowspan="4">
                    	<img src="http://www.cellularoutfitter.com/images/email/review/Stars.gif" border="0" />
                    </td>
				</tr>
                <tr>
                    <td height="30" align="left"> &nbsp; <a href="http://www.cellularoutfitter.com/c-3-cell-phone-covers-and-gel-skins.html" target="_blank" style="text-decoration:underline; color:#777;">Covers & Gel Skins</a></td>
                    <td height="30" align="left"> &nbsp; <a href="http://www.cellularoutfitter.com/c-1-cell-phone-batteries.html" target="_blank" style="text-decoration:underline; color:#777;">Batteries</a></td>
                    <td height="30" align="left"> &nbsp; <a href="http://www.cellularoutfitter.com/c-24-cell-phone-custom-cases.html" target="_blank" style="text-decoration:underline; color:#777;">Custom Cases</a></td>
				</tr>
                <tr>
                    <td height="30" align="left"> &nbsp; <a href="http://www.cellularoutfitter.com/c-7-cell-phone-cases-and-pouches.html" target="_blank" style="text-decoration:underline; color:#777;">Cases & Pouches</a></td>
                    <td height="30" align="left"> &nbsp; <a href="http://www.cellularoutfitter.com/c-5-cell-phone-bluetooth-and-hands-free.html" target="_blank" style="text-decoration:underline; color:#777;">Bluetooth & Hands-Free</a></td>
                    <td height="30" align="left"> &nbsp; <a href="http://www.cellularoutfitter.com/c-2-cell-phone-chargers-and-data-cables.html" target="_blank" style="text-decoration:underline; color:#777;">Chargers & Data Cables</a></td>
				</tr>
                <tr>
                    <td height="30" align="left"> &nbsp; <a href="http://www.cellularoutfitter.com/c-18-cell-phone-screen-protectors.html" target="_blank" style="text-decoration:underline; color:#777;">Screen Protectors</a></td>
                    <td height="30" align="left"> &nbsp; <a href="http://www.cellularoutfitter.com/wholesale-cell-phones.html" target="_blank" style="text-decoration:underline; color:#777;">Wholesale Cell Phones</a></td>
                    <td height="30" align="left"> &nbsp; <a href="http://www.cellularoutfitter.com/c-6-cell-phone-holsters-and-car-mounts.html" target="_blank" style="text-decoration:underline; color:#777;">Holsters & Car Mounts</a></td>
				</tr>
            </table>
        </td>
	</tr>
	<tr>
    	<td width="100%" align="center" style="padding:20px 0px 10px 0px; background-color:#eeeeee; color:#666; line-height:18px;">
        	This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from CellularOufitter.com. If this message has been sent in error, or you would no longer like to receive 
            any more periodic offers from CellularOutfitter.com, you may <a href="http://www.cellularoutfitter.com/unsubscribe.asp" target="_blank" style="color:#35628B; text-decoration:none; font-weight:bold;">click here to unsubscribe</a>.
        </td>
	</tr>
	<tr>
    	<td width="100%" align="center" style="padding:10px 0px 20px 0px; background-color:#eeeeee; color:#666;">
			<a href="http://www.cellularoutfitter.com/privacypolicy.html" target="_blank" style="color:#35628B; text-decoration:none; font-weight:bold;">Privacy Policy</a> &nbsp; | &nbsp; 
            <a href="http://www.cellularoutfitter.com/termsofuse.html" target="_blank" style="color:#35628B; text-decoration:none; font-weight:bold;">Terms and Conditions</a> &nbsp; | &nbsp; 
			<a href="http://www.cellularoutfitter.com/contact-us.html" target="_blank" style="color:#35628B; text-decoration:none; font-weight:bold;">Contact Us</a>
            <br>
            &copy; 2002-2013 CellularOutfitter.com
        </td>
	</tr>    
</table>
</body>
</html>
