<%
response.buffer = false
pageTitle = "E-Mail Opt-Outs for CO Mailing List"
header = 1
Server.ScriptTimeout = 1000 'seconds
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
if request("createCSV") <> "" then
	dim fs, file, filename, path
	filename = "CO_emailopt_out.csv"
	path = Server.MapPath("tempCSV") & "\" & filename
	path = replace(path,"admin\","")
	Set fs = CreateObject("Scripting.FileSystemObject")
	response.write "<b>Create File:</b><br>"
	DeleteFile(path)
	CreateFile(path)
	response.write "Here is the <a href='/tempCSV/" & filename & "'>file</a>"
else
	%>
	<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
		<tr bgcolor="#CCCCCC">
			<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
		</tr>
		<tr>
			<td width="47%" valign="middle">
				<table width="100%" border="0" cellspacing="0" cellpadding="15">
					<tr>
						<td class="normalText">
							<form name="frmCreateCSV" action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
								<p>E-Mail Opt-Outs for CO Mailing List</p>
								<p><input type="submit" name="createCSV" value="Create CSV"></p>
							</form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr bgcolor="#CCCCCC">
			<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
		</tr>
	</table>
	<%
end if

sub DeleteFile(path)
	if fs.FileExists(path) then
		response.write "<p>Deleting " & filename & ".<br>"
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	response.write "Creating " & filename & ".</p>"
	Set file = fs.CreateTextFile(path)
	SQL = "SELECT * FROM CO_emailopt_out"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 3, 3
	file.WriteLine "email,DateTimeEntd"
	if not RS.EOF then
		do while not RS.eof
			strline = chr(34) & RS("email") & chr(34) & ","
			strline = strline & chr(34) & RS("DateTimeEntd") & chr(34)
			file.WriteLine strline
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
