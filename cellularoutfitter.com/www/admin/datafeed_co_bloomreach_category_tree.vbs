set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim fs, file, filename, path, folderName
set fs = CreateObject("Scripting.FileSystemObject")
dim oXMLHTTP : set oXMLHTTP = CreateObject("Msxml2.ServerXMLHTTP.3.0")
set file = nothing

nRows = 0
filename = "bloomreach_cat_tree"
fileext = "txt"
folderName = "c:\inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\"
imgAbsolutePath = "c:\inetpub\wwwroot\productpics_co\"
path = folderName & filename & "." & fileext

if fs.FileExists(path) then fs.DeleteFile(path)

set file = fs.CreateTextFile(path)

dim SQL, RS
dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline, itemDesc
sql	=	"select	distinct typeid category_id, typename name, '' parent_category_id, '/' + convert(varchar(10), typeid) breadCrumb, 1 sequence" & vbcrlf & _
		"	,	case when typeid = 16 then " & vbcrlf & _
		"				'wholesale-cell-phones'" & vbcrlf & _
		"			else " & vbcrlf & _
		"				'c-' + convert(varchar(5), typeid) + '-cell-phone-' + lower(dbo.formatSEO(replace(typeName, '&', 'and')))" & vbcrlf & _
		"		end url" & vbcrlf & _
		"	,	cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'categoryid' category_id_desc, '' parent_category_id_desc, 'categoryid' breadcrumb_desc" & vbcrlf & _
		"from	v_subtypematrix_co" & vbcrlf & _
		"where	typeid in (1,2,3,5,6,7,16,18)" & vbcrlf & _
		"order by 1"

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

strHeadings	 = 	"category_id" & vbTab & "name" & vbTab & "parent_category_id" & vbTab & "breadcrumb" & vbTab & "sequence" & vbTab & "url" & vbTab & "primary_category" & vbTab & "category_id_desc" & vbTab & "parent_category_id_desc" & vbTab & "breadcrumb_desc"
file.WriteLine strHeadings

if not RS.eof then
	do until RS.eof
		category_id = rs("category_id")
		categoryName = rs("name")
		parent_category_id = rs("parent_category_id")
		breadcrumb = rs("breadcrumb")
		sequence = rs("sequence")
		url = rs("url")
		url = "http://www.cellularoutfitter.com/" & url & ".html"
		primary_category = rs("primary_category")
		category_id_desc = rs("category_id_desc")
		parent_category_id_desc = rs("parent_category_id_desc")
		breadcrumb_desc = rs("breadcrumb_desc")
		
		strLine = category_id & vbTab 			
		strLine = strLine & categoryName & vbTab
		strLine = strLine & parent_category_id & vbTab
		strLine = strLine & breadcrumb & vbTab
		strLine = strLine & sequence & vbTab 
		strLine = strLine & url & vbTab
		strLine = strLine & primary_category & vbTab
		strLine = strLine & category_id_desc & vbTab		
		strLine = strLine & parent_category_id_desc & vbTab		
		strLine = strLine & breadcrumb_desc

		file.WriteLine strLine

		rs.movenext
	loop
end if

sql	=	"select	modelid category_id, modelName name, b.brandid parent_category_id, '/' + convert(varchar(10), b.brandid) + '/' + convert(varchar(10), a.modelid) breadCrumb, 2 sequence" & vbcrlf & _
		"	,	'm-' + convert(varchar(10), modelid) + '-' + lower(dbo.formatSEO(b.brandname)) + '-' + lower(dbo.formatSEO(a.modelname)) + '-cell-phone-accessories' url" & vbcrlf & _
		"	,	cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'modelid' category_id_desc, 'brandid' parent_category_id_desc, '/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"from	we_models a join we_brands b" & vbcrlf & _
		"	on	a.brandid = b.brandid" & vbcrlf & _
		"where	a.hidelive = 0" & vbcrlf & _
		"	and	a.modelName not like 'all model%'"

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if not RS.eof then
	do until RS.eof
		category_id = rs("category_id")
		categoryName = rs("name")
		parent_category_id = rs("parent_category_id")
		breadcrumb = rs("breadcrumb")
		sequence = rs("sequence")
		url = rs("url")
		url = "http://www.cellularoutfitter.com/" & url & ".html"
		primary_category = rs("primary_category")
		category_id_desc = rs("category_id_desc")
		parent_category_id_desc = rs("parent_category_id_desc")
		breadcrumb_desc = rs("breadcrumb_desc")
		
		strLine = category_id & vbTab 			
		strLine = strLine & categoryName & vbTab
		strLine = strLine & parent_category_id & vbTab
		strLine = strLine & breadcrumb & vbTab
		strLine = strLine & sequence & vbTab 
		strLine = strLine & url & vbTab
		strLine = strLine & primary_category & vbTab
		strLine = strLine & category_id_desc & vbTab		
		strLine = strLine & parent_category_id_desc & vbTab		
		strLine = strLine & breadcrumb_desc

		file.WriteLine strLine

		rs.movenext
	loop
end if


sql	=	"select	distinct c.brandid category_id, c.brandname name, a.typeid parent_category_id, '/' + convert(varchar(10), a.typeid) + '/' + convert(varchar(10), c.brandid) + '/' + convert(varchar(10), m.modelid) breadCrumb, 3 sequence" & vbcrlf & _
		"	,	replace(a.typeName, '&', 'and') typeName, c.brandname, m.modelname, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'brandid' category_id_desc, 'categoryid' parent_category_id_desc, '/typeid/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"	,	m.modelid" & vbcrlf & _
		"from	v_subtypematrix_co a join we_items b" & vbcrlf & _
		"	on	a.subtypeid = b.subtypeid join we_brands c" & vbcrlf & _
		"	on	b.brandid = c.brandid join we_models m" & vbcrlf & _
		"	on	b.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_co > 0 " & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct c.brandid category_id, c.brandname name, a.typeid parent_category_id, '/' + convert(varchar(10), a.typeid) + '/' + convert(varchar(10), c.brandid) + '/' + convert(varchar(10), m.modelid) breadCrumb, 3" & vbcrlf & _
		"	,	replace(a.typeName, '&', 'and') typeName, c.brandname, m.modelname, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'brandid' category_id_desc, 'categoryid' parent_category_id_desc, '/typeid/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"	,	m.modelid" & vbcrlf & _
		"from	v_subtypematrix_co a join we_subRelatedItems t" & vbcrlf & _
		"	on	a.subtypeid = t.subtypeid join we_brands c" & vbcrlf & _
		"	on	t.brandid = c.brandid join we_items b" & vbcrlf & _
		"	on	t.itemid = b.itemid join we_models m" & vbcrlf & _
		"	on	t.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_co > 0" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct c.brandid category_id, c.brandname name, 5 parent_category_id, '/5/' + convert(varchar(10), c.brandid) + '/' + convert(varchar(10), m.modelid) breadCrumb, 3" & vbcrlf & _
		"	,	'bluetooth and hands-free' typeName, c.brandname, m.modelname, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'brandid' category_id_desc, 'categoryid' parent_category_id_desc, '/typeid/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"	,	m.modelid" & vbcrlf & _
		"from	we_brands c join we_models m" & vbcrlf & _
		"	on	c.brandid = m.brandid" & vbcrlf & _
		"where	m.handsfreetypes is not null" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"order by 1"
		
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if not RS.eof then
	do until RS.eof
		category_id = rs("category_id")
		categoryName = rs("name")
		parent_category_id = rs("parent_category_id")
		breadcrumb = rs("breadcrumb")
		sequence = rs("sequence")
		url = "http://www.cellularoutfitter.com/sb-" & category_id & "-sm-" & rs("modelid") & "-sc-" & parent_category_id & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName")) & "-" & formatSEO(RS("typename")) & ".html"
		primary_category = rs("primary_category")
		category_id_desc = rs("category_id_desc")
		parent_category_id_desc = rs("parent_category_id_desc")
		breadcrumb_desc = rs("breadcrumb_desc")
		
		strLine = category_id & vbTab 			
		strLine = strLine & categoryName & vbTab
		strLine = strLine & parent_category_id & vbTab
		strLine = strLine & breadcrumb & vbTab
		strLine = strLine & sequence & vbTab 
		strLine = strLine & url & vbTab
		strLine = strLine & primary_category & vbTab
		strLine = strLine & category_id_desc & vbTab		
		strLine = strLine & parent_category_id_desc & vbTab		
		strLine = strLine & breadcrumb_desc

		file.WriteLine strLine

		rs.movenext
	loop
end if
file.close()
set file = nothing

call initConnection(ftp,"ftp.bloomreach.com","cellularoutfitter","d3sWupHu")
success = ftp.PutFile(path, filename & "." & fileext)

sub initConnection(byref ftp,hostname,username,password)
	success = 0

	set ftp = CreateObject("Chilkat.Ftp2")
	success = ftp.UnlockComponent("WIRELEFTP_GQ2TEoLE9Bnh")

	ftp.Hostname = hostname
	ftp.Username = username
	ftp.Password = password

	success = ftp.Connect()
	if success <> 1 then 
'			response.write "<br>ftp connection error - " & ftp.LastErrorText & "<br>"
'			response.end
	end if
end sub

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub