<%
'response.buffer = false
pageTitle = "Create Cellphone Accents Product List for Bing"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 2000 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_bing_CO.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
session("errorSQL") = "path:" & path
set fs=Server.CreateObject("Scripting.FileSystemObject")
set f=fs.GetFile(path)
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						Cellular Outfitter Product List for Bing<br />
                        <%="The file was last modified on: " & f.DateLastModified%><br />
						Here is the <a href="../tempCSV/<%=filename%>">file</a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
<%
set f=nothing
set fs=nothing

sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.Write("Deleting " & filename & ".<br>")
		'fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim SQL, RS
	SQL = "SELECT A.itemID,A.PartNumber AS MPN,A.brandID,A.itemDesc_CA,A.itemPic,A.price_Retail,A.price_CA,A.Sports,A.flag1,A.UPCCode,A.itemLongDetail_CA,A.BULLET1,A.BULLET2,A.BULLET3,A.BULLET4,A.BULLET5,A.BULLET6,A.COMPATIBILITY,A.inv_qty,"
	SQL = SQL & "B.brandName,C.modelName,C.[temp],D.typeName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0"
	SQL = SQL & " AND A.partNumber not like '%-MLD-%' AND A.partNumber not like '%-SKN-%'"
	SQL = SQL & " AND A.price_CA IS NOT NULL"
	SQL = SQL & " ORDER BY A.itemID"
	response.Write(SQL)
	response.End()
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		'file.writeLine "PJN_PRODUCT_FEED_BASIC"
		file.write "Title" & vbtab	'Name
		file.write "MerchantProductID" & vbtab	'SKU
		file.write "ProductURL" & vbTab	'DestinationURL
		file.write "ImageURL" & vbtab	'ImageURL
		file.write "Description" & vbtab	'ShortDescription
		file.write "Price" & vbtab	'SalePrice
		file.write "Brand" & vbtab	'Manufacturer
		file.write "Condition" & vbtab
		file.write "B_Category" & vbtab
		file.write vbcrlf
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
				set RS2 = CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				GRPNAME = RS("itemDesc_CA")
				
				if isnull(RS("typeName")) then
					stypeName = "Universal"
				else 
					stypeName = RS("typeName")
				end if
				if isnull(RS("ModelName")) then
					ModelName = "Universal"
				else
					ModelName = RS("BrandName") & " > " & RS("ModelName")
				end if
				if isnull(RS("BrandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("BrandName")
				end if
				
				stritemLongDetail = RS("itemLongDetail_CA")
				stritemLongDetail = stritemLongDetail & "<ul>"
				if not isNull(RS("BULLET1")) then stritemLongDetail = stritemLongDetail & "<li>" & RS("BULLET1") & "</li>"
				if not isNull(RS("BULLET2")) then stritemLongDetail = stritemLongDetail & "<li>" & RS("BULLET2") & "</li>"
				if not isNull(RS("BULLET3")) then stritemLongDetail = stritemLongDetail & "<li>" & RS("BULLET3") & "</li>"
				if not isNull(RS("BULLET4")) then stritemLongDetail = stritemLongDetail & "<li>" & RS("BULLET4") & "</li>"
				if not isNull(RS("BULLET5")) then stritemLongDetail = stritemLongDetail & "<li>" & RS("BULLET5") & "</li>"
				if not isNull(RS("BULLET6")) then stritemLongDetail = stritemLongDetail & "<li>" & RS("BULLET6") & "</li>"
				if not isNull(RS("COMPATIBILITY")) then stritemLongDetail = stritemLongDetail & "<li>" & RS("COMPATIBILITY") & "</li>"
				stritemLongDetail = stritemLongDetail & "</ul>"
				if not isNull(stritemLongDetail) then
					stritemLongDetail = replace(stritemLongDetail,vbCrLf," ")
					stritemLongDetail = replace(stritemLongDetail,"Wireless Emporium","CellphoneAccents.com")
					stritemLongDetail = replace(stritemLongDetail," FREE!"," less!")
					stritemLongDetail = replace(stritemLongDetail,chr(34),"''")
					stritemLongDetail = replace(stritemLongDetail,"<a href='/downloads/","<a href='http://www.CellphoneAccents.com/downloads/")
				end if
				
				file.write GRPNAME & vbtab	'Name
				file.write RS("itemid") & vbtab	'SKU
				file.write "http://www.CellphoneAccents.com/p-" & RS("itemid") & "-" & formatSEO(GRPNAME) & ".html?utm_source=Bing&utm_medium=Shopping&utm_campaign=Product%2BFeed" & vbTab	'DestinationURL
				file.write "http://www.CellphoneAccents.com/productpics/thumb/" & RS("itemPic") & vbtab	'ImageURL
				file.write stritemLongDetail & vbtab	'ShortDescription
				file.write RS("price_CA") & vbtab	'SalePrice
				file.write BrandName & vbtab	'Manufacturer
				file.write "New" & vbtab
				file.write "Electronics" & vbtab
				file.write vbcrlf
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub

function prepYahoo(val)
	prepYahoo = LCase(replace(val,"&trade;","-"))
	prepYahoo = replace(replace(replace(replace(replace(replace(replace(replace(replace(prepYahoo," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
	prepYahoo = replace(replace(replace(replace(replace(prepYahoo,"(","-"),")","-"),";","-"),"+","-"),":","-")
end function
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
