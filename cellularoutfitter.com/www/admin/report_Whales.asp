<%header = 1%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
function YesNo(val)
	if val = true then
		YesNo = "Yes"
	else
		YesNo = "No"
	end if
end function

strError = ""

if request("submitted") <> "" then
	startDate = request("startDate")
	minAmt = request("minAmt")
	if not isDate(startDate) then strError = strError & "<h3>Start Date must be a valid date.</h3>"
	if minAmt = "" then strError = strError & "<h3>You must enter a Minimum Dollar Amount.</h3>"
	
	if strError = "" then
		SQL = "SELECT A.orderid, A.accountid, B.email, A.orderdatetime, A.ordergrandtotal, A.approved, A.cancelled"
		SQL = SQL & " FROM we_orders AS A INNER JOIN CO_accounts AS B ON A.accountid = B.accountid"
		SQL = SQL & " WHERE A.orderdatetime >= '" & startDate & "'"
		SQL = SQL & " AND CAST(A.ordergrandtotal AS money) >= " & minAmt
		SQL = SQL & " AND A.store = 2"
		'SQL = SQL & " AND A.approved = 1"
		'SQL = SQL & " AND (A.cancelled = 0 OR A.cancelled IS null)"
		SQL = SQL & " ORDER BY A.orderdatetime DESC"
		'response.write "<h3>" & SQL & "</h3>" & vbCrLf
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			%>
			<h3><%=RS.recordcount%> records found</h3>
			<table border="1">
				<tr>
					<td><b>Order&nbsp;ID</b></td>
					<td><b>E-Mail</b></td>
					<td><b>Order&nbsp;Notes</b></td>
					<td><b>Order&nbsp;Date/Time</b></td>
					<td><b>Order&nbsp;Grand&nbsp;Total</b></td>
					<td><b>Approved</b></td>
					<td><b>Cancelled</b></td>
				</tr>
				<%
				do until RS.eof
					%>
					<tr>
						<td valign="top"><a href="javascript:printinvoice('<%=RS("orderid")%>','<%=RS("accountid")%>');"><%=RS("orderid")%></a></td>
						<td valign="top"><%=RS("email")%></td>
						<td valign="top"><a href="javascript:fnopennotes('<%=RS("orderid")%>');">show&nbsp;notes</a></td>
						<td valign="top"><%=RS("orderdatetime")%></td>
						<td valign="top"><%=formatCurrency(RS("ordergrandtotal"))%></td>
						<td valign="top"><%=YesNo(RS("approved"))%></td>
						<td valign="top"><%=YesNo(RS("cancelled"))%></td>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<%
		end if
		RS.close
		Set RS = nothing
	else
		response.write "<font color=""#FF0000"">" & strError & "</font>" & vbcrlf
	end if
end if
%>

<form action="report_Whales.asp" method="post">
	<p>Start Date: <input type="text" name="startDate" value="<%=request("startDate")%>"></p>
	<p>Minimum Dollar Amount: <input type="text" name="minAmt" value="<%=request("minAmt")%>"></p>
	<p><input type="submit" name="submitted" value="Submit"></p>
</form>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="javascript">
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}

function fnopennotes(orderid) {
	var url='ordernotes.asp?orderid='+orderid;
	window.open(url,'ordernote','left=0,top=0,width=800,height=300,resizable=1,locationbar=0,menubar=0,toolbar=0');
}
</script>
