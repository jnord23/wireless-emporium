<%
header = 1
securePage = 1
pageTitle = "CO Admin Login"
if request.ServerVariables("HTTPS") = "on" then response.Redirect("http://www.cellularoutfitter.com/admin/default.asp")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
if request.form("password") <> "" and request.querystring("show") = "" then
	dim userID, strPWD
	userID = prepStr(lcase(request.form("userID")))
	strPWD = prepStr(lcase(request.form("password")))
	
	strSQL = "SELECT * FROM WE_AdminUsers WHERE username='" & userID & "'"
	SET oRS = oConn.EXECUTE(strSQL)
	
	'if there is a user by that username from the form
	if not oRS.EOF then
		'then check if the password matches for that user
		if lcase(oRS("password")) = strPWD then
			'if they do match then set some cookies to allow access
			Response.Cookies("admin_CO")("adminID") = oRS("adminID")
			Response.Cookies("admin_CO")("username") = oRS("username")
			Response.Cookies("admin_CO")("fname") = oRS("fname")
			Response.Cookies("admin_CO")("lname") = oRS("lname")
			Response.Cookies("admin_CO")("adminUser") = "True"
			Response.Cookies("admin_CO").Expires = date + 1
			session("adminID") = oRS("adminID")
			session("adminLvl") = oRS("adminLvl")
			session("adminNav") = oRS("adminNav")
			session("adminFullName") = oRS("fname") & " " & oRS("lname")
			'and send them on their way
			oRS.Close
			set oRS = nothing
			if inStr(myServer,"staging.") > 0 then
				response.redirect "http://staging.cellularoutfitter.com/admin/menu.asp"
			else
				response.redirect "http://www.cellularoutfitter.com/admin/menu.asp"
			end if
		else
			'else if the username and password do not match
			if inStr(myServer,"staging.") > 0 then
				response.redirect "http://staging.cellularoutfitter.com/admin/default.asp?show=error"
			else
				response.redirect "http://www.cellularoutfitter.com/admin/default.asp?show=error"
			end if
		end if
	else
		'if there is no user by that name
		'redirect to login page and set variable so we can tell user that there is no user by that name
		if inStr(myServer,"staging.") > 0 then
			response.redirect "http://staging.cellularoutfitter.com/admin/default.asp?show=error"
		else
			response.redirect "http://www.cellularoutfitter.com/admin/default.asp?show=error"
		end if
	end if
else
	dim myAction
	if inStr(myServer,"staging.") > 0 then
		myAction = "http://staging.cellularoutfitter.com/admin/default.asp"
	else
		myAction = "http://www.cellularoutfitter.com/admin/default.asp"
	end if
	%>
	<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
		<tr bgcolor="#CCCCCC">
			<td align="center" colspan="2" valign="middle">&nbsp;</td>
		</tr>
		<tr>
			<td width="47%" valign="middle" bgcolor="eaeaea">
				<table width="85%" border="0" cellspacing="0" cellpadding="15" align="center">
					<tr>
						<td bgcolor="#FFFFFF">
							<form name="login" method="post" action="<%=myAction%>">
								<font face="Verdana, Arial, Helvetica, sans-serif" size="1">
								<br>
								<%
								if request.querystring("show") = "error" then
									%>
									<p class="text"><font color="#CC0000">Sorry, there's a problem with your login. Please try again.</font></p>
									<%
								'elseif request.querystring("show") = "mustlogin" then
								else
									%>
									<p class="text"><font color="#CC0000">Please login to make modifications.</font></p>
									<br>
									<%
								end if
								%>
								</font>
								<table width="250" border="0" cellspacing="0" cellpadding="1">
									<tr>
										<td bgcolor="#000066">
											<table width="100%" border="0" cellpadding="6" bgcolor="#eaeaea" cellspacing="0">
												<tr bgcolor="#FF6600">
													<td width="28%">
														<p><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>Username:</b></font></p>
													</td>
													<td width="72%">
														<input type="text" name="userid" autocomplete="off" />
													</td>
												</tr>
												<tr bgcolor="#FF6600">
													<td width="28%">
														<p><b><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Password:</font></b></p>
													</td>
													<td width="72%">
														<input name="password" type="password" id="password" autocomplete="off" />
													</td>
												</tr>
												<tr bgcolor="#FF6600">
													<td colspan="2">
														<input type="submit" name="Submit" value="Log In"> 
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</form>
						</td>
					</tr>
				</table>
			</td>
			<td valign="top" bgcolor="eaeaea" width="53%">
				<table width="95%" border="0" cellspacing="0" cellpadding="15" align="center">
					<tr>
						<td bgcolor="#FFFFFF">
							<p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Please 
							log in to perform administrative functions.</b> </font></p>
							<p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Forget 
							your login information? Please contact
							<a href="mailto:webmaster@cellularoutfitter.com">webmaster@cellularoutfitter.com</a>.</font></p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr bgcolor="#CCCCCC">
			<td colspan="2" valign="middle">&nbsp;</td>
		</tr>
	</table>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
