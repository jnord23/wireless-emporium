set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")

filename = "co_google_trusted_ship.txt"
path = "c:\inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\" & filename


if fs.FileExists(path) then fs.DeleteFile(path) end if


set file = fs.CreateTextFile(path)

'sql	=	"select	a.orderid, a.trackingnum" & vbcrlf & _
'		"	,	case when a.shiptype like '%first class%' or a.shiptype like '%usps%' then 'USPS'" & vbcrlf & _
'		"			when a.shiptype like '%UPS%' then 'UPS'" & vbcrlf & _
'		"			else 'Other'" & vbcrlf & _
'		"		end carrier_code" & vbcrlf & _
'		"	,	case when a.shiptype like '%first class%' or a.shiptype like '%usps%' then ''" & vbcrlf & _
'		"			when a.shiptype like '%UPS%' then ''" & vbcrlf & _
'		"			else 'Other'" & vbcrlf & _
'		"		end other_carrier_name" & vbcrlf & _
'		"	,	convert(varchar(10), a.scandate, 20) shipdate" & vbcrlf & _
'		"from	we_orders a join v_accounts b" & vbcrlf & _
'		"	on	a.accountid = b.accountid and a.store = b.site_id" & vbcrlf & _
'		"where	a.store = 0" & vbcrlf & _
'		"	and	a.approved = 1" & vbcrlf & _
'		"	and	(a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
'		"	and	a.scandate >= convert(varchar(10), getdate()-3, 20)" & vbcrlf & _
'		"	and	a.scandate < convert(varchar(10), getdate(), 20)" & vbcrlf & _
'		"	and	a.thub_posted_to_accounting is not null" & vbcrlf & _
'		"	--and	a.extordertype = 2" & vbcrlf & _
'		"order by 1"
		
sql	=	"select	distinct a.orderid, a.trackingnum" & vbcrlf & _
		"	,	case when a.shiptype like '%first class%' or a.shiptype like '%usps%' then 'USPS'" & vbcrlf & _
		"			when a.shiptype like '%UPS%' then 'UPS'" & vbcrlf & _
		"			else 'Other'" & vbcrlf & _
		"		end carrier_code" & vbcrlf & _
		"	,	case when a.shiptype like '%first class%' or a.shiptype like '%usps%' then ''" & vbcrlf & _
		"			when a.shiptype like '%UPS%' then ''" & vbcrlf & _
		"			else 'Other'" & vbcrlf & _
		"		end other_carrier_name" & vbcrlf & _
		"	,	convert(varchar(10), a.scandate, 20) shipdate" & vbcrlf & _
		"from	we_orders a join v_accounts b" & vbcrlf & _
		"	on	a.accountid = b.accountid and a.store = b.site_id join we_orderdetails c" & vbcrlf & _
		"	on	a.orderid = c.orderid join we_items d" & vbcrlf & _
		"	on	c.itemid = d.itemid" & vbcrlf & _
		"where	a.store = 2" & vbcrlf & _
		"	and	a.approved = 1" & vbcrlf & _
		"	and	(a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
		"	and	a.scandate >= convert(varchar(10), getdate()-3, 20)" & vbcrlf & _
		"	and	a.scandate < convert(varchar(10), getdate(), 20)" & vbcrlf & _
		"	and	a.thub_posted_to_accounting is not null" & vbcrlf & _
		"	and	b.bAddress1 <> '4040 N. Palm St.'" & vbcrlf & _
		"	--and	(a.extordertype is null or a.extordertype not in (4,5,6,7,8,9))" & vbcrlf & _
		"	--and	c.partnumber not like 'WCD-%' and d.vendor not in ('CM', 'DS', 'MLD') and c.partnumber not like '%HYP%' and d.typeid <> 16" & vbcrlf & _
		"	--and	d.subtypeid not in (1031) and a.shiptype not like '%Int''l%'" & vbcrlf & _
		"	--and	(c.reship = 0 or c.reship is null) and (c.backOrder = 0 or c.backOrder is null)" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct a.orderid, a.trackingnum" & vbcrlf & _
		"	,	case when a.shiptype like '%first class%' or a.shiptype like '%usps%' then 'USPS'" & vbcrlf & _
		"			when a.shiptype like '%UPS%' then 'UPS'" & vbcrlf & _
		"			else 'Other'" & vbcrlf & _
		"		end carrier_code" & vbcrlf & _
		"	,	case when a.shiptype like '%first class%' or a.shiptype like '%usps%' then ''" & vbcrlf & _
		"			when a.shiptype like '%UPS%' then ''" & vbcrlf & _
		"			else 'Other'" & vbcrlf & _
		"		end other_carrier_name" & vbcrlf & _
		"	,	convert(varchar(10), a.scandate, 20) shipdate" & vbcrlf & _
		"from	we_orders a join v_accounts b" & vbcrlf & _
		"	on	a.accountid = b.accountid and a.store = b.site_id join we_orderdetails c" & vbcrlf & _
		"	on	a.orderid = c.orderid join we_items d" & vbcrlf & _
		"	on	c.itemid = d.itemid" & vbcrlf & _
		"where	a.store = 2" & vbcrlf & _
		"	and	a.approved = 1" & vbcrlf & _
		"	and	(a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
		"	and	a.thub_posted_to_accounting is not null" & vbcrlf & _
		"	and	a.trackingnum is not null and a.trackingnum <> ''" & vbcrlf & _
		"	and	a.orderid in ( select orderid from we_orders_missing_trustedstore where store = 2 and uploadedDate is null and cancelled is null)" & vbcrlf & _
		"order by 1"
set rs = CreateObject("ADODB.Recordset")
rs.open SQL, oConn, 0, 1

headers = "merchant order id" & vbtab & "tracking number" & vbtab & "carrier code" & vbtab & "other carrier name" & vbtab & "ship date"
file.writeline headers

if not rs.eof then
	'core columns
	do until rs.eof
		orderid = rs("orderid")
		trackingnum = rs("trackingnum")
		carrier_code = rs("carrier_code")
		other_carrier_name = rs("other_carrier_name")
		shipdate = rs("shipdate")

		strline = 			orderid & vbTab
		strline = strline & trackingnum & vbTab
		strline = strline & carrier_code & vbTab
		strline = strline & other_carrier_name & vbTab
		strline = strline & shipdate

		file.writeline strline
		rs.movenext
	loop
end if
rs.close
set rs = nothing
file.close()

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub