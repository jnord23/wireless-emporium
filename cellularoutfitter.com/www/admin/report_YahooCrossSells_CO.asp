<%
pageTitle = "CO Cross Sells Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
if request("submitted") <> "" then
	'Response.ContentType = "application/vnd.ms-excel"
	strError = ""
	dateStart = request("dateStart")
	dateEnd = request("dateEnd")
	if not isDate(dateStart) or not isDate(dateEnd) then
		strError = strError & "<p>dateStart and dateEnd must both be valid dates</p>"
	else
		dateStart = dateValue(dateStart)
		dateEnd = dateValue(dateEnd)
		if dateEnd < dateStart then strError = strError & "<p>dateEnd must be later than or equal to dateStart</p>"
	end if
	if strError = "" then
		dateEnd = dateAdd("D",1,dateEnd)
		SQL = "SELECT yahooSession, crossSell, checkoutDateTime FROM yahoo_temp"
		SQL = SQL & " WHERE LEN(yahooSession) > 5"
		SQL = SQL & " AND crossSell <> 0"
		SQL = SQL & " AND checkoutDateTime >= '" & dateStart & "' AND checkoutDateTime <= '" & dateEnd & "'"
		SQL = SQL & " ORDER BY checkoutDateTime DESC"
		set RS = Server.CreateObject("ADODB.Recordset")
		'response.write "<h3>" & SQL & "</h3>" & vbCrLf
		'response.end
		RS.open SQL, oConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			response.write "<h3>" & RS.recordcount & " TOTAL records found</h3>" & vbCrLf
			%>
			<table border="1">
				<tr>
					<td><b>Basket</b></td>
					<td><b>Interstitial</b></td>
					<td><b>checkoutDateTime</b></td>
				</tr>
				<%
				do until RS.eof
					%>
					<tr>
						<%
						if RS("crossSell") = 1 then
							%>
							<td valign="top" align="center"><font color="#FF0000" size="+1" face="Arial,Helvetica">X</font></td>
							<td valign="top">&nbsp;</td>
							<%
							COtotalBasket = COtotalBasket + 1
						elseif RS("crossSell") = 2 then
							%>
							<td valign="top">&nbsp;</td>
							<td valign="top" align="center"><font color="#FF0000" size="+1" face="Arial,Helvetica">X</font></td>
							<%
							COtotalInterstitial = COtotalInterstitial + 1
						end if
						%>
						<td valign="top" align="left"><%=RS("checkoutDateTime")%></td>
					</tr>
					<%
					RS.movenext
				loop
				%>
				<tr>
					<td valign="top" align="center"><font color="#FF0000" size="+1" face="Arial,Helvetica"><%=COtotalBasket%></font></td>
					<td valign="top" align="center"><font color="#FF0000" size="+1" face="Arial,Helvetica"><%=COtotalInterstitial%></font></td>
					<td valign="top" align="left"><font color="#000000" size="+1" face="Arial,Helvetica">TOTALS</font></td>
				</tr>
			</table>
			<%
		end if
		
		RS.close
		Set RS = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
else
	%>
	<p>&nbsp;</p>
	<h3>Item Sales Report</h3>
	<br>
	<form action="report_YahooCrossSells_CO.asp" method="post">
		<p><input type="text" name="dateStart">&nbsp;&nbsp;dateStart</p>
		<p><input type="text" name="dateEnd">&nbsp;&nbsp;dateEnd</p>
		<p><input type="submit" name="submitted" value="Generate Report"></p>
	</form>
	<%
end if
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
