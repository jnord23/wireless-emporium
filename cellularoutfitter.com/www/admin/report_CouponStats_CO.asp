<%
pageTitle = "CO Admin Site - Coupon Statistics"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<%
function nullBlank(str)
	if isnull(str) then
		nullBlank = "-null-"
	elseif trim(str) = "" then
		nullBlank = "&nbsp;"
	else
		nullBlank = str
	end if
end function

if request.querystring("couponid") <> "" then
	couponid = request.querystring("couponid")
	dateStart = request.querystring("dateStart")
	dateEnd = request.querystring("dateEnd")
	SQL = "SELECT * FROM we_Orders"
	SQL = SQL & " WHERE orderdatetime >= '" & dateStart & "' AND orderdatetime < '" & dateAdd("D",1,dateEnd) & "'"
	SQL = SQL & " AND couponID = '" & couponid & "'"
	SQL = SQL & " AND approved = 1"
	SQL = SQL & " AND store = 2"
	SQL = SQL & " AND accountID NOT IN (1,8)"
	SQL = SQL & " ORDER BY orderdatetime DESC"
	Set RS = Server.CreateObject("ADODB.Recordset")
	'response.write "<h3>" & SQL & "</h3>" & vbCrLf
	RS.open SQL, oConn, 3, 3
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		response.write "<h3>COUPON DETAILS FOR: " & request.querystring("promocode") & "&nbsp;&#150;&nbsp;" & dateStart & " to " & dateEnd & "</h3>"
		%>
		<table border="1">
			<tr>
				<td align="center"><b>orderID</b></td>
				<td align="left"><b>orderDateTime</b></td>
				<td align="center"><b>orderSubTotal</b></td>
				<td align="center"><b>orderGrandTotal</b></td>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<td valign="top" align="center"><%=nullBlank(RS("orderID"))%></td>
					<td valign="top" align="left"><%=nullBlank(RS("orderDateTime"))%></td>
					<td valign="top" align="center"><%=formatCurrency(RS("orderSubTotal"))%></td>
					<td valign="top" align="center"><%=formatCurrency(RS("orderGrandTotal"))%></td>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	end if
	
	RS.close
	Set RS = nothing
elseif request.querystring("submitted") <> "" then
	dateStart = request.querystring("dateStart")
	dateEnd = request.querystring("dateEnd")
	if not isDate(dateStart) then dateStart = date()
	if not isDate(dateEnd) then dateEnd = date()
	dateStart = dateValue(dateStart)
	dateEnd = dateValue(dateEnd)
	if strError = "" then
		SQL = "SELECT couponid,typeID,promoCode,couponDesc,expiration FROM CO_coupons ORDER BY promoCode"
		set RS = Server.CreateObject("ADODB.Recordset")
		'response.write "<h3>" & SQL & "</h3>" & vbCrLf
		RS.open SQL, oConn, 3, 3
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			response.write "<h3>COUPON STATISTICS FOR: " & dateStart & " to " & dateEnd & "</h3>"
			%>
			<table border="1">
				<tr>
					<td align="center"><b>couponid</b></td>
					<td align="left"><b>type</b></td>
					<td align="left"><b>promoCode</b></td>
					<td align="left"><b>couponDesc</b></td>
					<td align="left"><b>expiration</b></td>
					<td align="center"><b>#&nbsp;Used</b></td>
					<td align="center"><b>Discounted<br>Subtotal</b></td>
				</tr>
				<%
				holdCouponID = RS("couponid")
				myTotal = 0
				usedTotal = 0
				do until RS.eof
					SQL = "SELECT ordersubtotal FROM we_Orders"
					SQL = SQL & " WHERE orderdatetime >= '" & dateStart & "' AND orderdatetime < '" & dateAdd("D",1,dateEnd) & "'"
					SQL = SQL & " AND couponID = '" & RS("couponid") & "'"
					SQL = SQL & " AND approved = 1"
					SQL = SQL & " AND store = 2"
					SQL = SQL & " AND accountID NOT IN (1,8)"
					set RS2 = Server.CreateObject("ADODB.Recordset")
					'response.write "<h3>" & SQL & "</h3>" & vbCrLf
					RS2.open SQL, oConn, 3, 3
					ordersubtotal = 0
					totalCount = RS2.recordcount
					if totalCount > 0 then
						do until RS2.eof
							ordersubtotal = ordersubtotal + cDbl(RS2("ordersubtotal"))
							RS2.movenext
						loop
						%>
						<tr>
							<td valign="top" align="center"><a href="<%="report_CouponStats_CO.asp?couponid=" & RS("couponid") & "&promocode=" & RS("promoCode") & "&dateStart=" & dateStart & "&dateEnd=" & dateEnd%>"><%=nullBlank(RS("couponid"))%></a></td>
							<td valign="top" align="left"><%=nullBlank(RS("typeID"))%></td>
							<td valign="top" align="left"><%=nullBlank(RS("promoCode"))%></td>
							<td valign="top" align="left"><%=nullBlank(RS("couponDesc"))%></td>
							<td valign="top" align="left"><%=nullBlank(RS("expiration"))%></td>
							<td valign="top" align="center"><%=totalCount%></td>
							<td valign="top" align="center"><%=formatCurrency(ordersubtotal)%></td>
						</tr>
						<%
					end if
					RS.movenext
				loop
				%>
			</table>
			<%
		end if
		
		RS.close
		Set RS = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
end if
%>
<p>&nbsp;</p>
<h3>Choose another date range:</h3>
<br>
<form action="report_CouponStats_CO.asp" method="get">
	<p><input type="text" name="dateStart" value="<%=dateStart%>">&nbsp;&nbsp;Start&nbsp;Date</p>
	<p><input type="text" name="dateEnd" value="<%=dateEnd%>">&nbsp;&nbsp;End&nbsp;Date</p>
	<p><input type="submit" name="submitted" value="Generate Report"></p>
</form>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
