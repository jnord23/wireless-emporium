set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

set fs = CreateObject("Scripting.FileSystemObject")
dim oXMLHTTP : set oXMLHTTP = CreateObject("Msxml2.ServerXMLHTTP.3.0")
set file = nothing

nRows = 0
filename = "productList_Strands_CO"
fileext = "txt"
folderName = "c:\inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\"
imgAbsolutePath = "c:\inetpub\wwwroot\productpics_co\"
path = folderName & filename & "." & fileext

if fs.FileExists(path) then fs.DeleteFile(path)

set file = fs.CreateTextFile(path)

sql	=	"-- co strands" & vbcrlf & _
		"select	" & vbcrlf & _
		"	a.itemID as [id]," & vbcrlf & _
		"	a.itemDesc_CO as [title]," & vbcrlf & _
		"	'http://www.CellularOutfitter.com/p-' + convert(varchar(20), a.itemID) + '-' + dbo.formatSEO(a.itemDesc_CO) + '.html' as [link]," & vbcrlf & _
		"	c.typeName as [category]," & vbcrlf & _
		"	left(dbo.fn_stripHTML(" & vbcrlf & _
		"		replace(" & vbcrlf & _
		"		replace(" & vbcrlf & _
		"		replace(" & vbcrlf & _
		"			case when nullif(a.point1, '') is not null then ' - ' + a.point1 else '' end" & vbcrlf & _
		"		+	case when nullif(a.point2, '') is not null then ' - ' + a.point2 else '' end" & vbcrlf & _
		"		+	case when nullif(a.point3, '') is not null then ' - ' + a.point3 else '' end" & vbcrlf & _
		"		+	case when nullif(a.point4, '') is not null then ' - ' + a.point4 else '' end" & vbcrlf & _
		"		+	case when nullif(a.point5, '') is not null then ' - ' + a.point5 else '' end" & vbcrlf & _
		"		+	case when nullif(a.point6, '') is not null then ' - ' + a.point6 else '' end" & vbcrlf & _
		"		+	case when nullif(a.point7, '') is not null then ' - ' + a.point7 else '' end" & vbcrlf & _
		"		+	case when nullif(convert(varchar(8000), a.compatibility), '') is not null then ' - COMPATIBILITY: ' + convert(varchar(max), a.compatibility) else '' end" & vbcrlf & _
		"		+	' - ' + convert(varchar(8000), a.itemlongdetail_CO)	" & vbcrlf & _
		"		,	char(10), ' ')" & vbcrlf & _
		"		,	char(13), ' ')" & vbcrlf & _
		"		,	char(9), ' ')), 2000) as [description]," & vbcrlf & _
		"	a.itemPic_CO," & vbcrlf & _
		"	cast(a.price_retail as decimal(10,2)) as [price]," & vbcrlf & _
		"	c.typename + ', ' + isnull(b.brandname, 'Universal') + ', ' + isnull(b.brandname + ' > ' + d.modelname, 'Universal') + ', ' + a.itemdesc as [tags], " & vbcrlf & _
		"	isnull(b.brandname, 'Universal') brandName, isnull(d.modelname, 'Universal') modelName, isnull(a.handsfreetype, 0) handsfreetype," & vbcrlf & _
		"	isnull(e.color, '') color, a.price_co [salePrice]" & vbcrlf & _
		"from	we_items a with (nolock) join we_types c with (nolock)" & vbcrlf & _
		"	on	a.typeid = c.typeid left outer join we_brands b with (nolock) " & vbcrlf & _
		"	on	a.brandid = b.brandid left outer join we_models d with (nolock)" & vbcrlf & _
		"	on	a.modelid = d.modelid left outer join xproductcolors e with (nolock)" & vbcrlf & _
		"	on	a.colorid = e.id join we_items f with (nolock) " & vbcrlf & _
		"	on	a.partnumber = f.partnumber and f.master = 1 left outer join we_pnDetails g with (nolock)" & vbcrlf & _
		"	on	a.partnumber = g.partnumber" & vbcrlf & _
		"where	a.hidelive = 0 and a.price_CO > 0" & vbcrlf & _
		"	and a.partnumber not like 'WCD%'" & vbcrlf & _
		"	and	(f.inv_qty > 0 or g.alwaysinstock = 1)" & vbcrlf & _
		"	and	a.partnumber not like '%JON-NORD%'" & vbcrlf & _
		"	and	a.itempic_co != '' and a.itempic_co is not null" & vbcrlf & _
		"	and	a.itemdesc_co != '' and a.itemdesc_co is not null"

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

strHeadings	 = 	"id" & vbTab & "title" & vbTab & "link" & vbTab & "category" & vbTab & "description" & vbTab & "image_link" & vbTab & "price" & vbTab & "tags" & vbTab & "brandName" & vbTab & "modelName" & vbTab & "handsfreetype" & vbTab & "color" & vbTab & "salePrice"
file.WriteLine strHeadings

if not RS.eof then
	do until RS.eof
		id = rs("id")
		title = rs("title")
		link = rs("link")
		category = rs("category")
		description = rs("description")
		itempic = rs("itempic_co")
		price = rs("price")
		tags = rs("tags")
		brandName = rs("brandName")
		modelName = rs("modelName")
		handsfreetype = rs("handsfreetype")
		color = rs("color")
		salePrice = rs("salePrice")
		
		if fs.FileExists(imgAbsolutePath & "thumb\" & itempic) then
			strLine = id & vbTab 			
			strLine = strLine & title & vbTab
			strLine = strLine & link & vbTab
			strLine = strLine & category & vbTab
			strLine = strLine & description & vbTab
			strLine = strLine & "http://www.cellularoutfitter.com/productpics/thumb/" & itempic & vbTab
			strLine = strLine & price & vbTab
			strLine = strLine & tags & vbTab
			strLine = strLine & brandName & vbTab
			strLine = strLine & modelName & vbTab
			strLine = strLine & handsfreetype & vbTab
			strLine = strLine & color & vbTab
			strLine = strLine & salePrice
	
			file.WriteLine strLine
		end if
		rs.movenext
	loop
end if
file.close()
set file = nothing

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

function hasImageOnRemote(pRemoteURL)
	dim isExists : isExists = false
	
	oXMLHTTP.Open "HEAD", pRemoteURL, False	'communicating without responseBody, only looks up file header
	oXMLHTTP.Send
	
	if (oXMLHTTP.readyState = "4") and (oXMLHTTP.status = "200") and (instr(oXMLHTTP.getResponseHeader("Content-Type"), "image") > 0) then isExists = true
	
	hasImageOnRemote = isExists
end function
