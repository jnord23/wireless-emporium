<%
pageTitle = "Datafeeds"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<style>
.grid_row_0	{ background-color:#FFFFFF;}
.grid_row_1	{ background-color:#EAEAEA;}
.mouseover	{ background-color:#9BCEFF;}
</style>
<%
dim storeID : storeID = request("cbSite")
if "" = storeID then storeID = 0 end if

sql	=	"select	a.site_id, a.shortDesc, a.longDesc, a.site_url, a.logo" & vbcrlf & _
		"	,	null, null, a.color, b.feedFileName, b.feedFileDesc" & vbcrlf & _
		"	,	b.nDownload, b.lastDownload, b.lastUser, b.multiFile, b.schedule" & vbcrlf & _
		"from	xstore a join xdatafeed b" & vbcrlf & _
		"	on	a.site_id = b.site_id" & vbcrlf & _
		"where	a.site_id = '" & storeID & "'" & vbcrlf & _
		"order by a.site_id, b.feedFileDesc" & vbcrlf
session("errorSQL") = sql		
dim arrList : 	arrList = 	getDbRows(sql)
dim arrSite	:	arrSite	=	getDbRows("select site_id, shortDesc, color from xstore order by 1")

dim fs, fsTemp, isFileExist, filePath, fullFileName, fileName, fileExt
'fullFileName : aaaa.txt
'fileName : aaaa
'fileExt : txt
'filePath : e:\webservice\wirelessemporium.com\aaaa.txt

set fs=Server.CreateObject("Scripting.FileSystemObject")
isFileExist = false
%>
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="100%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top">
						<form name="frmFeed">
						<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
							<tr>
								<td width="100" class="normalText" align="center"><b>SITE</b>&nbsp;
									<select name="cbSite" onChange="this.form.submit();">
										<%
										if not isnull(arrSite) then
											for nRow = 0 to ubound(arrSite, 2)
											%>
												<option style="color:<%=arrSite(2, nRow)%>;" value="<%=arrSite(0, nRow)%>" <%if cstr(arrSite(0, nRow)) = cstr(storeID) then %>selected<% end if%>><%=arrSite(1, nRow)%></option>
											<%
											next
										end if
										%>
									</select>
								</td>
								<td class="normalText" align="left" style="padding-left:10px;"><b>Product Name</b></td>
								<td class="normalText" align="left" style="padding-left:10px;"><b>Date Modified</b></td>
								<td class="normalText" align="left" style="padding-left:10px;"><b>Link</b></td>
								<td class="normalText" align="left" style="padding-left:10px;"><b>Run Schedule</b></td>
							</tr>
							<tr><td colspan="99" width="2" bgcolor="#E95516" style="padding:0px;"><img src="/images/spacer.gif" width="2" height="2" border="0"></td></tr>							
							<%
							if not isnull(arrList) then
								for nRow = 0 to ubound(arrList, 2)
									fullFileName = arrList(8, nRow)
									fileName = left(fullFileName, len(fullFileName)-4)
									fileExt = right(fullFileName, 3)
									filePath = replace(server.mappath("\tempCSV"), "wirelessemporium.com", arrList(2, nRow)&".com") & "\" & fullFileName
									isFileExist = fs.FileExists(filePath)
									if isFileExist then
										set fsTemp = fs.GetFile(filePath)
									end if
									
									if not isFileExist then	'ex) mediaforge20110418.txt, there are some feed that change its filename daily basis following created date.
										fullFileName = fileName & year(now) & Right("0" & month(now), 2) & Right("0" & day(now), 2) & "." & fileExt
										filePath = replace(server.mappath("\tempCSV"), "wirelessemporium.com", arrList(2, nRow)&".com") & "\" & fullFileName
										isFileExist = fs.FileExists(filePath)
										if isFileExist then
											set fsTemp = fs.GetFile(filePath)
										end if
									end if
							%>
							<tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" >
								<td class="smlText" align="center" style="color:<%=arrList(7, nRow)%>"><%=arrList(1, nRow)%></td>
								<td class="smlText" align="left" style="padding-left:10px;"><%=arrList(9, nRow)%></td>
								<td class="smlText" align="left" style="padding-left:10px;">
								<%
									if isFileExist then 
										response.write fsTemp.DateLastModified
									else
										response.write "N/A"
									end if
								%>
								</td>
								<td class="smlText" align="left" style="padding-left:10px;" >
								<%
									if isFileExist then 
										if arrList(13, nRow) > 0 then
								%>
									<a href="<%=arrList(3, nRow)%>/tempCSV/<%=fullFileName%>" target="_blank"><%=fullFileName%></a> - <%=formatnumber(fsTemp.size / 1048576.0, 2) & " MB"%>
								<%
											dim childFS
											for i=0 to arrList(13, nRow)
												if fs.FileExists(replace(server.mappath("\tempCSV"), "wirelessemporium.com", arrList(2, nRow)&".com") & "\" & fileName & i & "." & fileExt) then
													set childFS = fs.GetFile(replace(server.mappath("\tempCSV"), "wirelessemporium.com", arrList(2, nRow)&".com") & "\" & fileName & i & "." & fileExt)
								%>
									<br><a href="<%=arrList(3, nRow)%>/tempCSV/<%=fileName & i & "." & fileExt%>" target="_blank"><%=fileName & i & "." & fileExt%></a> - <%=formatnumber(childFS.size / 1048576.0, 2) & " MB"%>
								<%				
													set childFS = nothing
												end if
											next
										else
								%>
									<a href="<%=arrList(3, nRow)%>/tempCSV/<%=fullFileName%>" target="_blank"><%=fullFileName%></a> - <%=formatnumber(fsTemp.size / 1048576.0, 2) & " MB"%>
								<%		
										end if
								%>	

								<%										
									else
										response.write "N/A"
									end if
								%>
								</td>
								<td class="smlText" align="left" style="padding-left:10px;" ><%=arrList(14, nRow)%></td>								
							</tr>
							</div>
							<%		
									set fsTemp = nothing
								next 
							else
							%>
							<tr><td colspan="99" class="smlText" align="center">No data to display</td></tr>
							<%
							end if
							%>
						</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
