<%
pageTitle = "CO Admin Site - Cart Abandonment Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<p>&nbsp;</p>

<%
if request.form("submitted") <> "" then
	StartDate = request.form("dc1")
	EndDate = request.form("dc2")
	if not isDate(StartDate) then
		strError = "Start Date must be a valid date."
	else
		StartDate = dateValue(StartDate)
	end if
	if not isDate(EndDate) then
		strError = "End Date must be a valid date."
	else
		EndDate = dateValue(EndDate)
	end if
	if strError = "" then
		if StartDate > EndDate then
			strError = "Start Date must be earlier than or equal to End Date."
		end if
	end if
	
	if strError = "" then
		response.write "<h3>Date Range: " & StartDate & " - " & EndDate & "</h3>" & vbcrlf
		
		SQL = "SELECT DISTINCT sessionID FROM ShoppingCart WHERE store = 2 AND dateEntd >= '" & StartDate & "' AND dateEntd < '" & dateAdd("D",1,EndDate) & "'"
		'response.write "<h3>" & SQL & "</h3>" & vbcrlf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		response.write "<h3>Carts started: " & RS.recordcount & "</h3>" & vbcrlf
		
		SQL = SQL & " AND dateRelated IS NOT NULL"
		'response.write "<h3>" & SQL & "</h3>" & vbcrlf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		response.write "<h3>Carts reaching upsell page: " & RS.recordcount & "</h3>" & vbcrlf
		
		SQL = SQL & " AND dateCheckout IS NOT NULL"
		'response.write "<h3>" & SQL & "</h3>" & vbcrlf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		response.write "<h3>Carts reaching upsell AND checkout page: " & RS.recordcount & "</h3>" & vbcrlf
		
		SQL = SQL & " AND purchasedOrderID IS NOT NULL"
		'response.write "<h3>" & SQL & "</h3>" & vbcrlf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		response.write "<h3>Carts reaching upsell AND checkout page AND converting: " & RS.recordcount & "</h3>" & vbcrlf
		
		SQL = "SELECT DISTINCT sessionID FROM ShoppingCart WHERE store = 2 AND dateEntd >= '" & StartDate & "' AND dateEntd < '" & dateAdd("D",1,EndDate) & "' AND purchasedOrderID IS NOT NULL"
		'response.write "<h3>" & SQL & "</h3>" & vbcrlf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		response.write "<h3>Total carts converting: " & RS.recordcount & "</h3>" & vbcrlf
		
		SQL = "SELECT DISTINCT sessionID FROM ShoppingCart A INNER JOIN we_orders B ON A.purchasedOrderID = B.orderid"
		SQL = SQL & " WHERE A.store = 2 AND A.dateEntd >= '" & StartDate & "' AND A.dateEntd < '" & dateAdd("D",1,EndDate) & "'"
		SQL = SQL & " AND B.extOrderType > 1"
		'response.write "<h3>" & SQL & "</h3>" & vbcrlf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		response.write "<h3>Non-CC total carts converting: " & RS.recordcount & "</h3>" & vbcrlf
		
		RS.close
		set RS = nothing
	else
		%>
		<p align="center"><font size="+2" color="red"><%=strError%></font></p>
		<%
	end if
end if
%>
<p>&nbsp;</p>
<h3>Choose dates:</h3>
<br>
<form action="report_Cart_Abandonments.asp" name="frmOrdersReport" method="post">
	<p>
		<input type="text" name="dc1" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmOrdersReport.dc1,document.frmOrdersReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;Start Date
		<br>
		<input type="text" name="dc2" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fEndPop(document.frmOrdersReport.dc1,document.frmOrdersReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;End Date
		<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
	</p>
	<p><input type="submit" name="submitted" value="Generate Report"></p>
</form>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="javascript">
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>
