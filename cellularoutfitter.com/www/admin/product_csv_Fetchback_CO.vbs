set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path
filename = "productList_Fetchback_CO.csv"
'path = "E:\Inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\" & filename
path = server.mappath("\tempCSV\" & filename)

set fs = CreateObject("Scripting.FileSystemObject")
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)
dim SQL, RS, strline
SQL = "SELECT A.itemid, A.PartNumber, A.itemDesc_CO, A.itemLongDetail_CO, A.POINT1, A.POINT2, A.POINT3, A.POINT4, A.POINT5, A.POINT6, A.COMPATIBILITY, A.POINT7,"
SQL = SQL & " A.price_CO, A.itempic_CO, A.UPCCode, A.inv_qty, T.typename"
SQL = SQL & " FROM we_Items A LEFT JOIN we_Types AS T ON A.typeid = T.typeid"
SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0"
SQL = SQL & " AND A.price_CO > 0"
SQL = SQL & " AND A.itemLongDetail_CO IS NOT NULL AND A.itemLongDetail_CO <> ''"
SQL = SQL & " AND A.typeid <> 16"
SQL = SQL & " ORDER BY A.itemID"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if not RS.eof then
	file.WriteLine "product name,product page url,product image url,product category,product description,Product ID,Price"
	do until RS.eof
		DoNotInclude = 0
		if RS("inv_qty") < 0 then
			SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
			set RS2 = CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if RS2.eof then DoNotInclude = 1
			RS2.close
			set RS2 = nothing
		end if
		if DoNotInclude = 0 then
			if isnull(RS("itemDesc_CO")) then
				itemDesc_CO = "Universal"
			else 
				itemDesc_CO = RS("itemDesc_CO")
			end if
			if isnull(RS("typeName")) then
				stypeName = "Universal"
			else 
				stypeName = RS("typeName")
			end if
			
			if not isNull(RS("itemLongDetail_CO")) or not isNull(RS("POINT1")) or not isNull(RS("POINT2")) or not isNull(RS("POINT3")) or not isNull(RS("POINT4")) or not isNull(RS("POINT5")) or not isNull(RS("POINT6")) or not isNull(RS("COMPATIBILITY")) or not isNull(RS("POINT7")) then
				strItemLongDetail = RS("itemLongDetail_CO") & "<ul>"
				if not isNull(RS("POINT1")) and RS("POINT1") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT1") & "</li>"
				if not isNull(RS("POINT2")) and RS("POINT2") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT2") & "</li>"
				if not isNull(RS("POINT3")) and RS("POINT3") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT3") & "</li>"
				if not isNull(RS("POINT4")) and RS("POINT4") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT4") & "</li>"
				if not isNull(RS("POINT5")) and RS("POINT5") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT5") & "</li>"
				if not isNull(RS("POINT6")) and RS("POINT6") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT6") & "</li>"
				if not isNull(RS("COMPATIBILITY")) and RS("COMPATIBILITY") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("COMPATIBILITY") & "</li>"
				if not isNull(RS("POINT7")) and RS("POINT7") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT7") & "</li>"
				strItemLongDetail = strItemLongDetail & "</ul>"
			else
				strItemLongDetail = RS("itemLongDetail_CO")
			end if
			if not isNull(strItemLongDetail) then strItemLongDetail = replace(strItemLongDetail,chr(34),"''")
			
			id_new_field = formatSEO(itemDesc_CO)
			priceCO = formatCurrency(cdbl(RS("price_CO")),2)
			
			strline = chr(34) & itemDesc_CO & chr(34) & ","
			strline = strline & "http://www.cellularoutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html?id=fbproduct,"
			strline = strline & "http://www.cellularoutfitter.com/productpics/big/" & RS("itempic_CO") & ","
			strline = strline & chr(34) & nameSEO(stypeName) & chr(34) & ","
			strline = strline & chr(34) & replace(strItemLongDetail,vbcrlf," ") & chr(34) & ","
			strline = strline & RS("itemID") & ","
			strline = strline & priceCO
			file.WriteLine strline
		end if
		RS.movenext
	loop
end if
RS.close
set RS = nothing
oConn.close
file.close()

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function

function nameSEO(val)
	select case val
		case "Antennas/Parts" : nameSEO = "Antennas"
		case "Faceplates" : nameSEO = "Covers &amp; Screen Guards"
		case "Data Cable & Memory" : nameSEO = "Data Cables &amp; Memory Cards"
		case "Hands-Free" : nameSEO = "Hands-Free Kits &amp; Bluetooth Headsets"
		case "Holsters/Belt Clips" : nameSEO = "Holsters / Car Mounts"
		case "Leather Cases" : nameSEO = "Cases &amp; Pouches"
		case "Cell Phones" : nameSEO = "Wholesale Unlocked Cell Phones"
		case "Full Body Protectors" : nameSEO = "Invisible Film Protectors"
		case else : nameSEO = val
	end select
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub