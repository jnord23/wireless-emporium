<!--#include virtual="/includes/admin/adminsecure.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include file="aspDatetime.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->

<%
Dim oConn
CALL fOpenConn()
Dim OrderID, txtcredit
OrderID = Request.QueryString("OrderID")
strEmail = Request.QueryString("emailid")
If OrderID = "" Then Request.Form("ORDERID")

Sub PrEndPage
	%>
	<HTML>
		<BODY>
			<B><FONT COLOR=RED>MISSING ORDER RECORD ID.
			PLEASE CLICK ON BACK BUTTON TO GO BACK TO PREVIOUS PAGE </FONT>
			</B>
		</BODY>
	</HTML>
	<%
End Sub

If OrderID = "" Then Call PrEndPage()

Dim strMsg
If Request.QueryString("OrderID") <> "" Then
	dim STRcr, STRtext, STRbody
	if trim(strEmail) <> "" then
		STRtext = formatNotes("Requested item re-shipped, Email sent to customer.")
	else
		STRtext = ""
	end if

	oConn.begintrans

	dim rst
	set rst = server.createobject("Adodb.Recordset")
	rst.Open "SELECT Ordernotes FROM we_ordernotes WHERE OrderID=" & Cdbl(OrderID), oConn
	if not rst.eof then
		strm = rst("Ordernotes")
	else
		strm = "EOF"
	end if
	set rst = nothing
	
	if strm = "EOF" then
		oConn.Execute "INSERT INTO we_ordernotes(OrderID,Ordernotes) VALUES ('" & Cdbl(OrderID) & "','" & STRtext & "')"
	elseif strm <> "" then
		oConn.Execute "UPDATE we_ordernotes SET Ordernotes= '" & strm & STRtext & "' WHERE OrderID=" & Cdbl(OrderID)
	else
		oConn.Execute "UPDATE we_ordernotes SET Ordernotes= '" & STRtext & "' WHERE OrderID=" & Cdbl(OrderID)
	end if
	
	oConn.committrans
	'To get to email id
	dim RSTfetch, STRto
	set RSTfetch = server.createobject("Adodb.Recordset")
	RSTfetch.open "SELECT EMAIL From CO_Accounts A, WE_ORDERS O WHERE A.ACCOUNTID=O.ACCOUNTID AND O.ORDERID=" & OrderID, oConn
	if not rstfetch.eof then
		if not isnull(rstfetch(0)) then STRto = rstfetch(0)
	end if
	RSTfetch.close
	set RSTfetch = nothing
	if trim(STRto) <> "" then
		'mail
		STRbody = "<tr><td>Dear Customer, </td></tr><br><br>"
		STRbody = STRbody & "<tr><td>Hello and thank you for shopping with Wireless Emporium.</td></tr><br><br>"
		STRbody = STRbody & "<tr><td>This email is to notify you that we have received your requested return and have re-shipped your item(s) accordingly."
		STRbody = STRbody & " Please allow a few days for delivery via the U.S. Postal Service. If you have any further questions, please don't hesitate"
		STRbody = STRbody & " to contact us.</td></tr><br><br>"
		STRbody = STRbody & "<tr><td>Warm regards,</td></tr><br><br>"
		STRbody = STRbody & "<tr><td><font COLOR=gray><b>Wireless </b></font><font color=#FF8100><b>Emporium</b></font><b>, Inc.</b></td></tr><br>"
		STRbody = STRbody & "<tr><td><B><a href='mailto:service@wirelessemporium.com'>service@wirelessemporium.com</a></td></tr><br>"
		STRbody = STRbody & "<tr><td><b><a href='http://www.WirelessEmporium.com'>http://www.WirelessEmporium.com</a></td></tr>"
		CDOSend strTo,"service@wirelessemporium.com","Your Item(s) Has Been Re-shipped",strBody
	end if
	response.write "<script>self.close()</script>"
Else
	
End If
Call fCloseConn
%>
