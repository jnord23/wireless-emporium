<%
pageTitle = "Product List for Fetchback - CO"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds
dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "productList_Fetchback_CO.csv"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List for Fetchback - CO</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							response.write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	dim fs, file
	response.write("<b>CreateFile:</b><br>")
	set fs = CreateObject("Scripting.FileSystemObject")
	'If the file already exists give an error message.
	if fs.FileExists(path) then
		response.write("File " & filename & " already exists, create aborted.<br>" & vbCrLf)
	else
		response.write("Creating " & filename & ".<br>")
		set file = fs.CreateTextFile(path)
		dim SQL, RS, strline
		SQL = "SELECT A.itemid, A.PartNumber, A.itemDesc_CO, A.itemLongDetail_CO, A.POINT1, A.POINT2, A.POINT3, A.POINT4, A.POINT5, A.POINT6, A.COMPATIBILITY, A.POINT7,"
		SQL = SQL & " A.price_CO, A.itempic_CO, A.UPCCode, A.inv_qty, T.typename"
		SQL = SQL & " FROM we_Items A LEFT JOIN we_Types AS T ON A.typeid = T.typeid"
		SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0"
		SQL = SQL & " AND A.price_CO > 0"
		SQL = SQL & " AND A.itemLongDetail_CO IS NOT NULL AND A.itemLongDetail_CO <> ''"
		SQL = SQL & " AND A.typeid <> 16"
		SQL = SQL & " ORDER BY A.itemID"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			file.WriteLine "product name,product page url,product image url,product category,product description,Product ID,Price"
			do until RS.eof
				DoNotInclude = 0
				if RS("inv_qty") < 0 then
					SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
					set RS2 = Server.CreateObject("ADODB.Recordset")
					RS2.open SQL, oConn, 3, 3
					if RS2.eof then DoNotInclude = 1
					RS2.close
					set RS2 = nothing
				end if
				if DoNotInclude = 0 then
					if isnull(RS("itemDesc_CO")) then
						itemDesc_CO = "Universal"
					else 
						itemDesc_CO = RS("itemDesc_CO")
					end if
					if isnull(RS("typeName")) then
						stypeName = "Universal"
					else 
						stypeName = RS("typeName")
					end if
					
					if not isNull(RS("itemLongDetail_CO")) or not isNull(RS("POINT1")) or not isNull(RS("POINT2")) or not isNull(RS("POINT3")) or not isNull(RS("POINT4")) or not isNull(RS("POINT5")) or not isNull(RS("POINT6")) or not isNull(RS("COMPATIBILITY")) or not isNull(RS("POINT7")) then
						strItemLongDetail = RS("itemLongDetail_CO") & "<ul>"
						if not isNull(RS("POINT1")) and RS("POINT1") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT1") & "</li>"
						if not isNull(RS("POINT2")) and RS("POINT2") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT2") & "</li>"
						if not isNull(RS("POINT3")) and RS("POINT3") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT3") & "</li>"
						if not isNull(RS("POINT4")) and RS("POINT4") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT4") & "</li>"
						if not isNull(RS("POINT5")) and RS("POINT5") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT5") & "</li>"
						if not isNull(RS("POINT6")) and RS("POINT6") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT6") & "</li>"
						if not isNull(RS("COMPATIBILITY")) and RS("COMPATIBILITY") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("COMPATIBILITY") & "</li>"
						if not isNull(RS("POINT7")) and RS("POINT7") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT7") & "</li>"
						strItemLongDetail = strItemLongDetail & "</ul>"
					else
						strItemLongDetail = RS("itemLongDetail_CO")
					end if
					if not isNull(strItemLongDetail) then strItemLongDetail = replace(strItemLongDetail,chr(34),"''")
					
					id_new_field = formatSEO(itemDesc_CO)
					priceCO = formatCurrency(cdbl(RS("price_CO")),2)
					
					strline = chr(34) & itemDesc_CO & chr(34) & ","
					strline = strline & "http://www.cellularoutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html?id=fbproduct,"
					strline = strline & "http://www.cellularoutfitter.com/productpics/big/" & RS("itempic_CO") & ","
					strline = strline & chr(34) & nameSEO(stypeName) & chr(34) & ","
					strline = strline & chr(34) & replace(strItemLongDetail,vbcrlf," ") & chr(34) & ","
					strline = strline & RS("itemID") & ","
					strline = strline & priceCO
					file.WriteLine strline
				end if
				RS.movenext
			loop
		end if
		RS.close
		set RS = nothing
		file.Close()
	end if
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
