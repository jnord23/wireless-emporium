<%
pageTitle = "Product List for MediaForge - CO"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 2000 'seconds
dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "productList_mediaforge_CO.csv"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for MediaForge</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							response.write("<b>DeleteFile:</b>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	else
		response.write "<br>"
	end if
end sub

sub CreateFile(path)
	dim fs, file
	response.write("<b>CreateFile:</b>")
	set fs = CreateObject("Scripting.FileSystemObject")
	'If the file already exists give an error message.
	if fs.FileExists(path) then
		response.write("File " & filename & " already exists, create aborted.<br>" & vbCrLf)
	else
		response.write("Creating " & filename & ".<br>")
		set file = fs.CreateTextFile(path)
		dim SQL, RS, strline
		SQL	=	"	select	a.itemid, a.partnumber, a.itemdesc_co, a.itemlongdetail_co	" & vbcrlf & _
				"		,	a.price_co, a.itempic_co, a.upccode, a.inv_qty, t.typename	" & vbcrlf & _
				"	from	we_items a with (nolock) join we_types t with (nolock)	" & vbcrlf & _
				"		on	a.typeid = t.typeid left outer join 	" & vbcrlf & _
				"			(	" & vbcrlf & _
				"			select	a.partnumber orphan_partnumber	" & vbcrlf & _
				"			from	(	" & vbcrlf & _
				"					select	partnumber	" & vbcrlf & _
				"						,	sum(case when inv_qty > 0 then 1 else 0 end) nMaster	" & vbcrlf & _
				"						,	sum(case when inv_qty < 0 then 1 else 0 end) nSlave	" & vbcrlf & _
				"					from	we_items a with (nolock) 	" & vbcrlf & _
				"					where	hidelive = 0 and inv_qty <> 0 and price_co is not null	" & vbcrlf & _
				"						and	typeid <> 16	" & vbcrlf & _
				"					group by partnumber	" & vbcrlf & _
				"					) a	" & vbcrlf & _
				"			where	a.nMaster = 0	" & vbcrlf & _
				"			) b	" & vbcrlf & _
				"		on	a.partnumber = b.orphan_partnumber	" & vbcrlf & _
				"	where	a.hidelive = 0 and a.inv_qty <> 0	" & vbcrlf & _
				"		and a.price_co is not null	" & vbcrlf & _
				"		and a.typeid <> 16	" & vbcrlf & _
				"		and	b.orphan_partnumber is null	" & vbcrlf

		set RS = Server.CreateObject("ADODB.Recordset")
		dim queryTimeStart, queryTimeEnd
'		response.write "<pre>" & SQL & "</pre>"
		queryTimeStart = now
		RS.open SQL, oConn, 3, 1
		queryTimeEnd = now
		
		response.write "<p><b>Query</b> elapsed time:" & datediff("s", queryTimeStart, queryTimeEnd) & " second(s)<br>"

		if not RS.eof then
			fileWriteTimeStart = now
			file.WriteLine "product name,product page url,product image url,product thumb image url,product category,product description,Product ID,Price"
			do until RS.eof
				if isnull(RS("itemDesc_co")) then
					itemDesc_CO = "Universal"
				else
					itemDesc_CO = RS("itemDesc_co")
				end if
				if isnull(RS("typeName")) then
					stypeName = "Universal"
				else
					stypeName = RS("typeName")
				end if
				
				id_new_field = formatSEO(itemDesc_CO)
				strPriceCO = formatCurrency(RS("price_co"))
				
				strline = chr(34) & itemDesc_CO & chr(34) & ","
				strline = strline & "http://www.cellularoutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html?id=forge" & ","
				strline = strline & "http://www.cellularoutfitter.com/productpics/big/" & RS("itemPic_co") & ","
				strline = strline & "http://www.cellularoutfitter.com/productpics/thumb/" & RS("itemPic_co") & ","				
				strline = strline & chr(34) & nameSEO(stypeName) & chr(34) & ","
				
				if not isnull(RS("itemLongDetail_CO")) then
					strline = strline & chr(34) & replace(RS("itemLongDetail_CO"),vbcrlf," ") & chr(34) & ","
				else
					strline = strline & chr(34) & " " & chr(34) & ","				
				end if
				
				strline = strline & RS("itemID") & ","
				strline = strline & strPriceCO
				file.WriteLine strline

				RS.movenext
			loop

			file.WriteLine "Cell Phone Covers,http://www.cellularoutfitter.com/c-3-cell-phone-covers-screen-guards.html?id=forge,http://www.cellularoutfitter.com/images/types/CO_CAT_banner_covers-screen-guards.jpg,,,,3,"
			file.WriteLine "Cell Phone Cases,http://www.cellularoutfitter.com/c-7-cell-phone-cases-pouches.html?id=forge,http://www.cellularoutfitter.com/images/types/CO_CAT_banner_cases-pouches.jpg,,,,7,"
			file.WriteLine "Bluetooth Headsets,http://www.cellularoutfitter.com/bluetooth.html?id=forge,http://www.cellularoutfitter.com/images/bluetooth.jpg,,,,99,"
			file.WriteLine "Cell Phone Chargers,http://www.cellularoutfitter.com/c-2-cell-phone-chargers.html?id=forge,http://www.cellularoutfitter.com/images/types/CO_CAT_banner_chargers.jpg,,,,2,"
			file.WriteLine "Cell Phone Data Cables,http://www.cellularoutfitter.com/c-13-cell-phone-data-cables-memory-cards.html?id=forge,http://www.cellularoutfitter.com/images/types/CO_CAT_banner_data-cables-memory-cards.jpg,,,,13,"
			file.WriteLine "Cell Phone Antennas,http://www.cellularoutfitter.com/c-12-cell-phone-antennas.html?id=forge,http://www.cellularoutfitter.com/images/types/CO_CAT_banner_antennas.jpg,,,,12,"
			file.WriteLine "Cell Phone Batteries,http://www.cellularoutfitter.com/c-1-cell-phone-batteries.html?id=forge,http://www.cellularoutfitter.com/images/types/CO_CAT_banner_batteries.jpg,,,,1,"
			file.WriteLine "Cell Phone Holsters,http://www.cellularoutfitter.com/c-6-cell-phone-holsters-car-mounts.html?id=forge,http://www.cellularoutfitter.com/images/types/CO_CAT_banner_holsters-car-mounts.jpg,,,,6,"

			fileWriteTimeEnd = now
			response.write "<b>FileWrite</b> elapsed time:" & datediff("n", fileWriteTimeStart, fileWriteTimeEnd) & "min, " & datediff("s", fileWriteTimeStart, fileWriteTimeEnd) mod 60 & " second(s)<p>"
		end if
		RS.close
		set RS = nothing
		file.Close()
	end if
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
