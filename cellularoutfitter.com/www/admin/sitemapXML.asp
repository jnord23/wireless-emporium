<%
dim oConn
set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.ConnectionTimeout = 1000		'seconds
oConn.Open

dim fs, file, path, url
set fs = CreateObject("Scripting.FileSystemObject")
'path = "E:\Inetpub\wwwroot\cellularoutfitter.com\www\sitemap.xml"
path = server.mappath("\sitemap.xml")

if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<sitemapindex xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"">"
file.WriteLine "<sitemap>"
file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/sitemap1.xml</loc>"
file.WriteLine vbtab & "<lastmod>" & RFC3339(now) & "</lastmod>"
file.WriteLine "</sitemap>"
file.WriteLine "<sitemap>"
file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/sitemap2.xml</loc>"
file.WriteLine vbtab & "<lastmod>" & RFC3339(now) & "</lastmod>"
file.WriteLine "</sitemap>"
file.WriteLine "</sitemapindex>"
file.close()
set file = nothing

'sitemap1.xml
path = server.mappath("\sitemap1.xml")
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
file.WriteLine "<url>"
file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com</loc>"
file.WriteLine vbtab & "<priority>1.00</priority>"
file.WriteLine vbtab & "<changefreq>weekly</changefreq>"
file.WriteLine vbtab & "<lastmod>" & RFC3339(now) & "</lastmod>"
file.WriteLine "</url>"

dim temp(100)
'temp(0) = "iphone-accessories.html"
'temp(1) = "b-5-motorola-cell-phone-accessories.html"
'temp(2) = "b-9-samsung-cell-phone-accessories.html"
'temp(3) = "b-4-lg-cell-phone-accessories.html"
'temp(4) = "b-7-nokia-cell-phone-accessories.html"
'temp(5) = "b-6-nextel-cell-phone-accessories.html"
'temp(6) = "b-10-sanyo-cell-phone-accessories.html"
'temp(7) = "b-14-blackberry-cell-phone-accessories.html"
'temp(8) = "b-16-palm-cell-phone-accessories.html"
'temp(9) = "b-17-apple-cell-phone-accessories.html"
'temp(10) = "b-18-pantech-cell-phone-accessories.html"
'temp(11) = "b-2-sony-ericsson-cell-phone-accessories.html"
'temp(12) = "b-20-htc-cell-phone-accessories.html"
'temp(13) = "b-11-siemens-cell-phone-accessories.html"
'temp(14) = "b-1-audiovox-cell-phone-accessories.html"
'temp(15) = "b-13-nec-cell-phone-accessories.html"
'temp(16) = "b-19-t-mobile-sidekick-cell-phone-accessories.html"
'temp(17) = "b-3-kyocera-qualcomm-cell-phone-accessories.html"
'temp(18) = "c-12-cell-phone-antennas.html"
'temp(19) = "c-1-cell-phone-batteries.html"
'temp(20) = "c-14-cell-phone-bling-kits-charms.html"
'temp(21) = "c-2-cell-phone-chargers.html"
'temp(22) = "c-13-cell-phone-data-cables-memory-cards.html"
'temp(23) = "c-3-cell-phone-covers.html"
'temp(24) = "bluetooth.html"
'temp(25) = "c-5-cell-phone-hands-free-kits-bluetooth-headsets.html"
'temp(26) = "c-6-cell-phone-holsters-belt-clips-holders.html"
'temp(27) = "c-7-cell-phone-cases-pouches.html"
'temp(28) = "car-1-alltel-cell-phone-accessories.html"
'temp(29) = "car-2-att-cingular-cell-phone-accessories.html"
'temp(30) = "car-8-boost-mobile-southern-linc-cell-phone-accessories.html"
'temp(31) = "car-9-cricket-cell-phone-accessories.html"
'temp(32) = "car-3-metro-pcs-cell-phone-accessories.html"
'temp(33) = "car-7-prepaid-cell-phone-accessories.html"
'temp(34) = "car-4-sprint-nextel-cell-phone-accessories.html"
'temp(35) = "car-5-t-mobile-cell-phone-accessories.html"
'temp(36) = "car-11-us-cellular-cell-phone-accessories.html"
'temp(37) = "car-6-verizon-cell-phone-accessories.html"
'temp(38) = "apple-accessories.html"
'temp(39) = "audiovox-accessories.html"
'temp(40) = "blackberry-accessories.html"
'temp(41) = "htc-accessories.html"
'temp(42) = "kyocera-qualcomm-accessories.html"
'temp(43) = "lg-accessories.html"
'temp(44) = "motorola-accessories.html"
'temp(45) = "nec-accessories.html"
'temp(46) = "nextel-accessories.html"
'temp(47) = "nokia-accessories.html"
'temp(48) = "palm-accessories.html"
'temp(49) = "panasonic-accessories.html"
'temp(50) = "pantech-accessories.html"
'temp(51) = "samsung-accessories.html"
'temp(52) = "sidekick-accessories.html"
'temp(53) = "siemens-accessories.html"
'temp(54) = "sanyo-accessories.html"
'temp(55) = "sony-ericsson-accessories.html"

'========== BRAND
temp(0) = "/iphone-accessories.html"
temp(1) = "/b-2-sony-ericsson-cell-phone-accessories.html"
temp(2) = "/b-4-lg-cell-phone-accessories.html"
temp(3) = "/b-5-motorola-cell-phone-accessories.html"
temp(4) = "/b-6-nextel-cell-phone-accessories.html"
temp(5) = "/b-7-nokia-cell-phone-accessories.html"
temp(6) = "/b-10-sanyo-cell-phone-accessories.html"
temp(7) = "/b-11-siemens-cell-phone-accessories.html"
temp(8) = "/b-14-blackberry-cell-phone-accessories.html"
temp(9) = "/b-15-other-cell-phone-accessories.html"
temp(10) = "/b-16-palm-cell-phone-accessories.html"
temp(11) = "/b-17-apple-cell-phone-accessories.html"
temp(12) = "/b-18-pantech-cell-phone-accessories.html"
temp(13) = "/b-19-t-mobile-sidekick-cell-phone-accessories.html"
temp(14) = "/b-20-htc-cell-phone-accessories.html"																

'========== CATEGORY
temp(15) = "/c-1-cell-phone-batteries.html"
temp(16) = "/c-2-cell-phone-chargers.html"
temp(17) = "/c-3-cell-phone-covers-screen-guards.html"
temp(18) = "/c-5-cell-phone-hands-free-kits-bluetooth-headsets.html"
temp(19) = "/c-6-cell-phone-holsters-car-mounts.html"
temp(20) = "/c-7-cell-phone-cases-pouches.html"
temp(21) = "/c-12-cell-phone-antennas.html"
temp(22) = "/c-13-cell-phone-data-cables-memory-cards.html"
temp(23) = "/bluetooth.html"
temp(24) = "/wholesale-cell-phones.html"
temp(25) = "/sc-8-sb-0-sm-0-cell-phone-other-gear.html"
temp(26) = "/sc-14-sb-0-sm-0-cell-phone-bling-kits-charms.html"

'========== CARRIER
temp(27) = "/car-1-alltel-cell-phone-accessories.html"
temp(28) = "/car-2-att-cingular-cell-phone-accessories.html"
temp(29) = "/car-3-metro-pcs-cell-phone-accessories.html"
temp(30) = "/car-4-sprint-nextel-cell-phone-accessories.html"
temp(31) = "/car-5-t-mobile-cell-phone-accessories.html"
temp(32) = "/car-6-verizon-cell-phone-accessories.html"
temp(33) = "/car-7-prepaid-cell-phone-accessories.html"
temp(34) = "/car-8-boost-mobile-southern-linc-cell-phone-accessories.html"
temp(35) = "/car-9-cricket-cell-phone-accessories.html"
temp(36) = "/car-11-us-cellular-cell-phone-accessories.html"

'========== Cell Phones				
temp(37) = "/cp-sb-20-htc-wholesale-cell-phones.html"
temp(38) = "/cp-sb-1-utstarcom-wholesale-cell-phones.html"
temp(39) = "/cp-sb-2-sony-ericsson-wholesale-cell-phones.html"
temp(40) = "/cp-sb-4-lg-wholesale-cell-phones.html"
temp(41) = "/cp-sb-5-motorola-wholesale-cell-phones.html"
temp(42) = "/cp-sb-6-nextel-wholesale-cell-phones.html"
temp(43) = "/cp-sb-7-nokia-wholesale-cell-phones.html"
temp(44) = "/cp-sb-9-samsung-wholesale-cell-phones.html"
temp(45) = "/cp-sb-14-blackberry-wholesale-cell-phones.html"
temp(46) = "/cp-sb-15-other-wholesale-cell-phones.html"
temp(47) = "/cp-sb-16-palm-wholesale-cell-phones.html"
temp(48) = "/cp-sb-17-apple-wholesale-cell-phones.html"
temp(49) = "/cp-sb-18-pantech-wholesale-cell-phones.html"
temp(50) = "/cp-sb-19-t-mobile-sidekick-wholesale-cell-phones.html"
				
dim a
for a = 0 to 50
	if "" <> temp(a) and not isnull(temp(a)) then
		URL = temp(a)
		file.WriteLine "<url>"
		file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com" & URL & "</loc>"
		file.WriteLine vbtab & "<priority>0.90</priority>"
		file.WriteLine "</url>"
	end if
next

'============================================== BRAND-MODEL & (HANDS-FREE for its model) START
dim strBrandModelSQL, objRsBrandModel
strBrandModelSQL = 	"select	distinct a.modelid, c.brandname, a.modelname " & vbcrlf & _
					"from	we_models a join we_items b  " & vbcrlf & _
					"	on	a.modelid = b.modelid join we_brands c" & vbcrlf & _
					"	on	a.brandid = c.brandid" & vbcrlf & _
					"where	b.hidelive = 0 " & vbcrlf & _
					"	and b.inv_qty <> 0 " & vbcrlf & _
					"	and b.price_co > 0 " & vbcrlf & _
					"order by 1"
					
set objRsBrandModel	= oConn.execute(strBrandModelSQL)

do until objRsBrandModel.EOF
	url = "m-" & objRsBrandModel("modelID") & "-" & formatSEO(objRsBrandModel("brandName")) & "-" & formatSEO(objRsBrandModel("modelName")) & "-cell-phone-accessories.html"
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/" & url & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"

	url = "hf-sm-" & objRsBrandModel("modelID") & "-" & formatSEO(objRsBrandModel("brandName")) & "-" & formatSEO(objRsBrandModel("modelName")) & "-hands-free-kits-bluetooth-headsets.html"
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/" & url & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"

	objRsBrandModel.movenext
loop
set objRsBrandModel = nothing
'============================================== BRAND-MODEL & (HANDS-FREE for its model) END	

'============================================== category-brand START
dim strCateBrandSQL, objRsCateBrand
strCateBrandSQL	=	"select	distinct c.typeid, c.typename, a.brandid, a.brandname" & vbcrlf & _
					"from	we_items b join we_types c " & vbcrlf & _
					"	on	b.typeid = c.typeid join we_brands a" & vbcrlf & _
					"	on	b.brandid = a.brandid" & vbcrlf & _
					"where	b.typeid in (1,2,3,5,6,7,12,13)" & vbcrlf & _
					"	and b.hidelive = 0" & vbcrlf & _
					"	and b.inv_qty <> 0" & vbcrlf & _
					"	and b.price_co > 0" & vbcrlf & _
					"order by 1"

set objRsCateBrand = oConn.execute(strCateBrandSQL)

do until objRsCateBrand.EOF
	url = "sc-" & objRsCateBrand("typeid") & "-sb-" & objRsCateBrand("brandid") & "-cell-phone-" & formatSEO(objRsCateBrand("typename")) & "-for-" & formatSEO(objRsCateBrand("brandname")) & ".html"
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/" & url & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"
	
	objRsCateBrand.movenext
loop
set objRsCateBrand = nothing
'============================================== category-brand END

'============================================== brand-model-category START
dim strBMCSQL, objRsBMC
strBMCSQL	=	"select	distinct a.modelid, a.modelname, c.brandid, c.brandname, d.typeid, d.typename" & vbcrlf & _
				"from	we_models a join we_items b " & vbcrlf & _
				"	on	a.modelid = b.modelid join we_brands c" & vbcrlf & _
				"	on	a.brandid = c.brandid join we_types d" & vbcrlf & _
				"	on	b.typeid = d.typeid" & vbcrlf & _
				"where	b.hidelive = 0 and patindex('%all %',a.modelname) = 0" & vbcrlf & _
				"	and b.inv_qty <> 0" & vbcrlf & _
				"	and b.price_co > 0" & vbcrlf & _
				"	and	d.typeid not in (8,14)" & vbcrlf & _
				"order by 1"


set objRsBMC = oConn.execute(strBMCSQL)

do until objRsBMC.EOF
	url = "sb-" & objRsBMC("brandID") & "-sm-" & objRsBMC("modelID") & "-sc-" & objRsBMC("typeID") & "-" & formatSEO(objRsBMC("brandName")) & "-" & formatSEO(objRsBMC("modelName")) & "-" & formatSEO(objRsBMC("typeName")) & ".html"
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/" & url & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"	

	url = "sc-" & objRsBMC("typeID") & "-sb-" & objRsBMC("brandID") & "-sm-" & objRsBMC("modelID") & "-" & formatSEO(objRsBMC("typeName")) & "-for-" & formatSEO(objRsBMC("brandName")) & "-" & formatSEO(objRsBMC("modelName")) & ".html"
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/" & url & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"	

	objRsBMC.movenext
loop
set objRsBMC = nothing
'============================================== brand-model-category END

'============================================== carrier-brand START
dim strCarrierBrandSQL, objRsCarrierBrand
strCarrierBrandSQL	=	"	select	distinct a.brandid, b.brandname, c.id carrierid, c.carriername" & vbcrlf & _
						"	from	we_models a join we_brands b " & vbcrlf & _
						"		on	a.brandid = b.brandid join we_carriers c" & vbcrlf & _
						"		on	a.carriercode like '%' + c.carriercode + ',%'" & vbcrlf & _
						"	where	c.id in (1,2,3,4,5,6,7,8,9,11) " & vbcrlf & _						
						"	order by 2" & vbcrlf

set objRsCarrierBrand = oConn.execute(strCarrierBrandSQL)

do until objRsCarrierBrand.EOF
	url = "car-" & objRsCarrierBrand("carrierid") & "-b-" & objRsCarrierBrand("brandID") & "-" & formatSEO(objRsCarrierBrand("carrierName")) & "-" & formatSEO(objRsCarrierBrand("brandName")) & "-cell-phone-accessories.html"
	
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/" & url & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"	

	objRsCarrierBrand.movenext
loop
set objRsCarrierBrand = nothing

'============================================== carrier-brand END		
file.WriteLine "</urlset>"
file.close()
set file = nothing

'sitemap2.xml
'path = "E:\Inetpub\wwwroot\cellularoutfitter.com\www\sitemap2.xml"
path = server.MapPath("\sitemap2.xml")
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"


'============================================== product page START
dim pSQL, objRsProduct
pSQL	=	"select	itemid, itemdesc_co " & vbcrlf & _
			"from	we_items  " & vbcrlf & _
			"where	hidelive = 0 and inv_qty <> 0  " & vbcrlf & _
			"	and price_co > 0 " & vbcrlf & _
			"order by 1"
			
set objRsProduct = oConn.execute(pSQL)

do until objRsProduct.EOF
	URL = "p-" & objRsProduct("itemID") & "-" & formatSEO(objRsProduct("itemDesc_CO")) & ".html"
	
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/" & url & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"	

	objRsProduct.movenext
loop
set objRsProduct = nothing
'============================================== product page END

file.WriteLine "</urlset>"
file.close()
set file = nothing


function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function

function RFC3339(myDate)
	dim myDays, myMonth, myYear, myHours, myMonths, mySeconds
	myDate = dateAdd("h",8,myDate)
	myDays = zeroPad(day(myDate),2)
	myMonth = zeroPad(month(myDate),2)
	myYear = year(myDate)
	myHours = zeroPad(hour(myDate),2)
	myMinutes = zeroPad(minute(myDate),2)
	mySeconds = zeroPad(second(myDate),2)
	RFC3339 = myYear & "-" & myMonth & "-" & myDays & "T" & myHours & ":" & myMinutes & ":" & mySeconds & "Z"
end function
 
function zeroPad(m,t)
	zeroPad = String(t-Len(m),"0") & m
end function
%>