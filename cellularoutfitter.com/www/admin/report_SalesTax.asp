<%
pageTitle = "CO Admin - Sales Tax Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
response.write "<h3>" & pageTitle & "</h3>" & vbCrLf

if request("submitted") <> "" then
	showblank = "&nbsp;"
	shownull = "-null-"
	
	SQL = "SELECT A.*, B.sState, C.ordernotes"
	SQL = SQL & " FROM (WE_orders A INNER JOIN CO_accounts B ON A.accountid=B.accountid)"
	SQL = SQL & " LEFT JOIN WE_ordernotes C ON A.orderid=C.orderid"
	SQL = SQL & " WHERE B.sState='ca' AND month(A.orderdatetime)='" & request("myMonth") & "' AND year(A.orderdatetime)='" & request("myYear") & "'"
	SQL = SQL & " AND A.approved=1 AND A.confirmSent='yes' AND (A.cancelled = 0 OR A.cancelled IS NULL)"
	SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
	SQL = SQL & " AND B.accountID != 1 AND B.accountID != 8 AND B.accountID != 165305 AND B.accountID != 224190 AND B.accountID != 228690"
	SQL = SQL & " AND A.store = 2"
	SQL = SQL & " ORDER BY A.orderdatetime"
	
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	'response.write "<h3>" & SQL & "</h3>" & vbCrLf
	
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		%>
		<table border="1">
			<tr>
				<td><b>orderid</b></td>
				<td><b>orderdatetime</b></td>
				<td><b>ordersubtotal</b></td>
				<td><b>orderTax</b></td>
				<td><b>CalcTax</b></td>
				<td><b>RMAstatus</b></td>
				<td><b>refundAmount</b></td>
				<td><b>shipState</b></td>
				<td><b>ordernotes</b></td>
			</tr>
			<%
			lap = 0
			do until RS.eof
				lap = lap + 1			
				ordergrandtotal = cDbl(RS("ordergrandtotal"))
				ordershippingfee = cDbl(RS("ordershippingfee"))
				ordersubtotal = cDbl(RS("ordersubtotal"))
				'CalcTax = ordergrandtotal - ordershippingfee - ordersubtotal
				CalcTax = ordersubtotal * .0775
				GrandTotal = GrandTotal + ordersubtotal
				CalcTaxTotal = CalcTaxTotal + CalcTax
				%>
				<tr>
					<td><%=RS("orderid")%></td>
					<td><%=RS("orderdatetime")%></td>
					<td><%=formatCurrency(ordersubtotal)%></td>
					<td>
						<%
						orderTax = RS("orderTax")
						fontColor = "#000000"
						if not isNull(orderTax) then
							response.write formatCurrency(orderTax)
							orderTaxTotal = orderTaxTotal + orderTax
							if formatNumber(CalcTax,2) <> formatNumber(orderTax,2) then fontColor = "#FF0000"
						else
							response.write "-null-"
						end if
						%>
					</td>
					<td><font color="<%=fontColor%>"><%=formatCurrency(CalcTax)%></font></td>
					<td>
						<%
						select case RS("RMAstatus")
							case 1 : RMAstatus = "EXCH SENT"
							case 2 : RMAstatus = "EXCH REC'D"
							case 3 : RMAstatus = "REF SENT"
							case 4 : RMAstatus = "REF REC'D"
							case 5 : RMAstatus = "EXCHANGED"
							case 6 : RMAstatus = "PART RFND"
							case 7 : RMAstatus = "FULL RFND"
							case else : RMAstatus = shownull
						end select
						response.write RMAstatus
						%>
					</td>
					<td>
						<%
						if not isNull(RS("refundAmount")) then
							response.write formatCurrency(RS("refundAmount"))
							refundAmount = refundAmount + RS("refundAmount")
						else
							response.write shownull
						end if
						%>
					</td>
					<td><%=RS("sState")%></td>
					<td>
						<%
						if not isNull(RS("ordernotes")) then
							response.write RS("ordernotes")
						else
							response.write "-null-"
						end if
						%>
					</td>
				</tr>
				<%
				RS.movenext
			loop
			%>
			<tr>
				<td><b>&nbsp;</b></td>
				<td><b>&nbsp;</b></td>
				<td><b><%=formatCurrency(GrandTotal)%></b></td>
				<td><b><%=formatCurrency(orderTaxTotal)%></b></td>
				<td><b><%=formatCurrency(CalcTaxTotal)%></b></td>
				<td><b>&nbsp;</b></td>
				<td><b><%=formatCurrency(refundAmount)%></b></td>
				<td><b>&nbsp;</b></td>
				<td><b>&nbsp;</b></td>
			</tr>
		</table>
		<h3><%=lap%> records found</h3>        
		<h3><%=formatCurrency(GrandTotal) & " - " & formatCurrency(refundAmount) & " = " & formatCurrency(GrandTotal - refundAmount)%></h3>
		<h3><%="7.75% of " & formatCurrency(GrandTotal - refundAmount) & " = " & formatCurrency((GrandTotal - refundAmount) * .0775)%></h3>
		<%
	end if
	
	RS.close
	Set RS = nothing
else
	%>
	<form action="report_SalesTax.asp" method="post">
		<p>
			<select name="myMonth">
				<%
				for a = 1 to 12
					response.write "<option value=""" & a & """>" & a & "</option>" & vbCrLf
				next
				a = year(date)
				%>
			</select>&nbsp;&nbsp;Month
		</p>
		<p>
			<select name="myYear">
				<option value="<%=a%>"><%=a%></option>
				<option value="<%=a - 1%>"><%=a - 1%></option>
			</select>&nbsp;&nbsp;Year
		</p>
		<p><input type="submit" name="submitted" value="Generate Report"></p>
	</form>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
