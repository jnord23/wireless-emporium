<%
set weUtil = server.CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

isTestMode = true
strTemplate = ""
strFrom = "sales@cellularoutfitter.com"
strTo = ""
reviewTracking = "utm_source=Email2&utm_medium=FollowUp&utm_campaign=CO14DayReview"
crossSellTracking = "utm_source=Email2&utm_medium=FollowUp&utm_campaign=CO14DayReviewCrossSell"

sql	=	"select	top 20 a.orderid, a.ordergrandtotal, a.orderdatetime, a.thub_posted_date" & vbcrlf & _
		"	,	b.email, b.fname, e.itemid, e.itemdesc_co, e.itempic_co" & vbcrlf & _
		"	,	isnull(sum(cast(f.approved as int)), 0) cnt, isnull(avg(f.rating), 0) avgRating	" & vbcrlf & _
		"from	we_orders a join co_accounts b" & vbcrlf & _
		"	on	a.accountid = b.accountid join we_orderdetails d" & vbcrlf & _
		"	on	a.orderid = d.orderid join we_items e" & vbcrlf & _
		"	on	d.itemid = e.itemid left outer join co_reviews f" & vbcrlf & _
		"	on	e.partnumber = f.partnumbers and f.approved = 1" & vbcrlf & _
		"where	a.approved = 1 " & vbcrlf & _
		"	and	(a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
		"	and a.thub_posted_date >= convert(varchar(10), dateadd(day, -14, getdate()), 20)" & vbcrlf & _
		"	and	a.thub_posted_date < convert(varchar(10), dateadd(day, -13, getdate()), 20)" & vbcrlf & _
		"	and	a.scandate is not null" & vbcrlf & _
		"	and	(d.reship is null or d.reship = 0)" & vbcrlf & _
		"	and	a.store = 2" & vbcrlf & _
		"	and (b.email not like '%@marketplace.amazon.com' and b.email not like '%@seller.sears.com')" & vbcrlf & _
		"	and	a.parentOrderID is null" & vbcrlf & _
		"	and b.email not in (select distinct email from co_emailopt_out) " & vbcrlf & _
		"group by a.orderid, a.ordergrandtotal, a.orderdatetime, a.thub_posted_date, b.email, b.fname, e.itemid, e.itemdesc_co, e.itempic_co" & vbcrlf & _
		"order by b.email, a.orderid, e.itemid"

set rs = oConn.execute(sql)

strTemplate = readTextFile("C:\inetpub\wwwroot\cellularoutfitter.com\www\admin\14-day-product-review-email-template2.html")
dup_strTemplate = strTemplate

lapCount 	= 0
noSkip 		= false
strItems 	= ""
strCustName = ""
strItemIds = ""
do until rs.eof
	lapCount = lapCount + 1
	if lapCount > 4 then noSkip = false

	if strTo <> rs("email") then
		lapCount = 1
		noSkip = true		
	
		if "" <> strItems then
			strItems = strItems & "</tr></table>" & vbcrlf
			strSubject = strCustName & ", Review Your CellularOutfitter.com Item for a Chance to Win $100.00"
			strTemplate = replace(strTemplate, "[ITEMS]", strItems)
			call getCrossSellItems(strItemIds)

			'send an e-mail
'			response.write strTemplate & "<br><br>"
			set objEmail = CreateObject("CDO.Message")
			with objEmail
				.From = strFrom

				if isTestMode then 				
					.To = "doldding82@gmail.com"
					.Bcc = "doldding82@hotmail.com;doldding82@yahoo.com"					
					.Subject = strSubject & " (" & strTo & ")"
					.HTMLBody = strTemplate
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
					.Configuration.Fields.Update
					response.write strSubject & " (" & strTo & ")<br>"
					response.write strTemplate & "<br><br>"
'					.Send
				else
					.To = strTo
'					.Bcc = "doldding82@gmail.com"
					.Subject = strSubject
					.HTMLBody = strTemplate
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
					.Configuration.Fields.Update
'					.Send
				end if
			end with
		
			strTemplate = dup_strTemplate	'refresh the template for an another e-mail.
		end if

		strItemIds = ""
		strItems = 	"<table width=""100%"" border=""0"" align=""left"" cellpadding=""10"" cellspacing=""0"">" & vbcrlf & _
					"	<tr>" & vbcrlf
	end if

	strCustName = rs("fname")
	strTo 		= rs("email")	

	if instr(rs("email"), "@") > 0 and len(strCustName) > 0 and noSkip then
		if lapCount = 3 then
			strItems = strItems & "	</tr>" & vbcrlf
			strItems = strItems & "	<tr>" & vbcrlf
		end if

		if strItemIds = "" then
			strItemIds = rs("itemid")
		else
			strItemIds = strItemIds & "," & rs("itemid")
		end if
		
		productURL = "http://www.cellularoutfitter.com/p-" & rs("itemid") & "-" & formatSEO(rs("itemDesc_co")) & ".html?oid=" & rs("orderid") & "&" & reviewTracking
		
		strItems = strItems & 	"		<td width=""350"" align=""left"">" & vbcrlf & _
								"			<table width=""330"" border=""0"" align=""left"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf & _
								"				<tr>" & vbcrlf & _
								"					<td width=""160"" align=""center"">" & vbcrlf & _
								"						<a href=""" & productURL & """ target=""_blank"" style=""text-decoration:none;""><img src=""http://www.cellularoutfitter.com//productPics/big/" & rs("itempic_co") & """ border=""0"" width=""145"" style=""padding:5px; border:3px solid #ccc; -moz-border-radius:5px; border-radius:5px;"" /></a>" & vbcrlf & _
								"					</td>" & vbcrlf & _
								"					<td width=""167"" align=""left"" valign=""top"" style=""padding-left:3px;"">" & vbcrlf & _
								"						<table width=""167"" border=""0"" align=""left"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf & _
								"							<tr>" & vbcrlf & _
								"								<td width=""167"" align=""left"">" & vbcrlf & _								
								"									<a href=""" & productURL & """ target=""_blank"" style=""font-size:12px; color:#333; text-decoration:none;"">" & rs("itemDesc_co") & "</a><br><br>" & vbcrlf & _
								"									<a href=""" & productURL & """ target=""_blank"" style=""text-decoration:none;""><img src=""http://www.cellularoutfitter.com/images/email/review/ClicktoRevew.gif"" border=""0"" /></a><br>" & vbcrlf & _
								"								</td>" & vbcrlf & _
								"							</tr>" & vbcrlf & _
								"							<tr>" & vbcrlf & _
								"								<td width=""167"" style=""padding-top:5px;"" align=""center"">" & vbcrlf & _
								"									<a href=""" & productURL & """ target=""_blank"" style=""text-decoration:none;"">" & getRatingAvgStar(rs("avgRating")) & "</a>" & vbcrlf & _
								"								</td>" & vbcrlf & _
								"							</tr>" & vbcrlf & _
								"						</table>" & vbcrlf & _
								"					</td>" & vbcrlf & _
								"				</tr>" & vbcrlf & _
								"			</table>" & vbcrlf & _
								"		</td>" & vbcrlf
	end if
	
	rs.movenext
loop

strItems = strItems & "</tr></table>" & vbcrlf
strSubject = strCustName & ", Review Your CellularOutfitter.com Item for a Chance to Win $100.00"
strTemplate = replace(strTemplate, "[ITEMS]", strItems)
call getCrossSellItems(strItemIds)

if lapCount > 0 then
	'send a last one
	set objEmail = CreateObject("CDO.Message")
	with objEmail
		.From = strFrom
	
		if isTestMode then 				
			.To = "doldding82@gmail.com"
			.Bcc = "doldding82@hotmail.com;doldding82@yahoo.com"
			.Subject = strSubject & " (" & strTo & ")"
			.HTMLBody = strTemplate
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			response.write strSubject & " (" & strTo & ")<br>"
			response.write strTemplate & "<br><br>"			
'			.Send
		else
			.To = strTO
			.Bcc = "doldding82@gmail.com"
			.Subject = strSubject
			.HTMLBody = strTemplate
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
		end if
	end with
end if

rs.close
set rs = nothing

sub getCrossSellItems(itemids)
	sql	=	"exec sp_ppUpsell 2, '" & itemids & "', 0, 45"
	set rsCross = oConn.execute(sql)
	
	strCross = ""
	if not rsCross.eof then
		lapCross = 0
		do until rsCross.eof
			lapCross = lapCross + 1
			
			if strCross = "" then
				strCross = 	"<table width=""100%"" border=""0"" align=""left"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf & _
							"	<tr>"
			end if
			
			strText = rsCross("itemdesc")
			if len(strText) > 90 then strText = left(strText, 87) & "..."
			
			crossSellProductURL = "http://www.cellularoutfitter.com/p-" & rsCross("itemid") & "-" & formatSEO(rsCross("itemdesc")) & ".html?" & crossSellTracking
			strCross = strCross	&	"		<td width=""180"" align=""left"" valign=""top"">" & vbcrlf & _
									"			<table width=""175"" border=""0"" align=""left"" cellpadding=""5"" cellspacing=""0"">" & vbcrlf & _
									"				<tr>" & vbcrlf & _
									"					<td width=""165"" style=""padding:5px;"" align=""center"">" & vbcrlf & _
									"						<a href=""" & crossSellProductURL & """ target=""_blank"" style=""text-decoration:none;""><img src=""http://www.cellularoutfitter.com/productPics/big/" & rsCross("itemPic") & """ border=""0"" width=""140"" style=""padding:5px; border:3px solid #ccc; -moz-border-radius:5px; border-radius:5px;"" /></a>" & vbcrlf & _
									"					</td>" & vbcrlf & _
									"				</tr>" & vbcrlf & _
									"				<tr>" & vbcrlf & _
									"					<td width=""165"" height=""70"" style=""padding:5px; font-size:12px; color:#333;"" align=""center"" valign=""top"">" & vbcrlf & _
									"						<a href=""" & crossSellProductURL & """ target=""_blank"" style=""font-size:12px; color:#333; text-decoration:none;"">" & strText & "</a>" & vbcrlf & _
									"					</td>" & vbcrlf & _
									"				</tr>" & vbcrlf & _
									"				<tr>" & vbcrlf & _
									"					<td width=""165"" style=""padding:5px;"" align=""center"">" & vbcrlf & _
									"						<a href=""" & crossSellProductURL & """ target=""_blank"" style=""text-decoration:none;"">" & getRatingAvgStar(rsCross("avgReview")) & "</a>" & vbcrlf & _
									"					</td>" & vbcrlf & _
									"				</tr>" & vbcrlf & _
									"				<tr>" & vbcrlf & _
									"					<td width=""165"" style=""padding:5px;"" align=""center"">" & vbcrlf & _
									"						<a href=""" & crossSellProductURL & """ target=""_blank"" style=""text-decoration:none;""><img src=""http://www.cellularoutfitter.com/images/email/review/Buyitnow.gif"" border=""0"" /></a>" & vbcrlf & _
									"					</td>" & vbcrlf & _
									"				</tr>" & vbcrlf & _
									"			</table>" & vbcrlf & _
									"		</td>" & vbcrlf
			if lapCross > 3 then
				exit do
			end if
			rsCross.movenext
		loop

		if strCross <> "" then
			strCross = strCross & "	</tr></table>"
		end if

		strTemplate = replace(strTemplate, "[CROSS_SELL]", strCross)
	end if
end sub

function getRatingAvgStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""http://www.cellularoutfitter.com/images/email/review/emptystar.gif"" border=""0"" width=""24"" height=""23"" />" & vbcrlf & _
					"<img src=""http://www.cellularoutfitter.com/images/email/review/emptystar.gif"" border=""0"" width=""24"" height=""23"" />" & vbcrlf & _
					"<img src=""http://www.cellularoutfitter.com/images/email/review/emptystar.gif"" border=""0"" width=""24"" height=""23"" />" & vbcrlf & _
					"<img src=""http://www.cellularoutfitter.com/images/email/review/emptystar.gif"" border=""0"" width=""24"" height=""23"" />" & vbcrlf & _
					"<img src=""http://www.cellularoutfitter.com/images/email/review/emptystar.gif"" border=""0"" width=""24"" height=""23"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""http://www.cellularoutfitter.com/images/email/review/Fullstar.gif"" border=""0"" width=""24"" height=""23"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""http://www.cellularoutfitter.com/images/email/review/Halfstar.gif"" border=""0"" width=""24"" height=""23"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""http://www.cellularoutfitter.com/images/email/review/emptystar.gif"" border=""0"" width=""24"" height=""23"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function


Function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
End Function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
end function
%>