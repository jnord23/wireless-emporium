set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")

filename = "co_google_trusted_cancel.txt"
path = "c:\inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\" & filename
if fs.FileExists(path) then fs.DeleteFile(path) end if

set file = fs.CreateTextFile(path)

sql	=	"select	distinct a.orderid, 'MerchantCanceled' reason" & vbcrlf & _
		"from	we_orders a join v_accounts b" & vbcrlf & _
		"	on	a.accountid = b.accountid and a.store = b.site_id join we_orderdetails c" & vbcrlf & _
		"	on	a.orderid = c.orderid join we_items d" & vbcrlf & _
		"	on	c.itemid = d.itemid" & vbcrlf & _
		"where	a.store = 2" & vbcrlf & _
		"	and	a.cancelled = 1" & vbcrlf & _
		"	and	a.orderdatetime >= convert(varchar(10), getdate()-1, 20)" & vbcrlf & _
		"	and	a.orderdatetime < convert(varchar(10), getdate(), 20)" & vbcrlf & _
		"	and	b.bAddress1 <> '4040 N. Palm St.'" & vbcrlf & _
		"	--and	(a.extordertype is null or a.extordertype not in (4,5,6,7,8,9))" & vbcrlf & _
		"	--and	c.partnumber not like 'WCD-%' and d.vendor not in ('CM', 'DS', 'MLD') and c.partnumber not like '%HYP%'" & vbcrlf & _
		"	--and	d.subtypeid not in (1031) and a.shiptype not like '%Int''l%'" & vbcrlf & _
		"	--and	(c.reship = 0 or c.reship is null) and (c.backOrder = 0 or c.backOrder is null)	" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct a.orderid, 'MerchantCanceled' reason" & vbcrlf & _
		"from	we_orders a join v_accounts b" & vbcrlf & _
		"	on	a.accountid = b.accountid and a.store = b.site_id join we_orderdetails c" & vbcrlf & _
		"	on	a.orderid = c.orderid join we_items d" & vbcrlf & _
		"	on	c.itemid = d.itemid" & vbcrlf & _
		"where	a.store = 2" & vbcrlf & _
		"	and	a.cancelled = 1" & vbcrlf & _
		"	and	a.orderid in ( select orderid from we_orders_missing_trustedstore where store = 2 and uploadedDate is null and cancelled = 1)" & vbcrlf & _
		"order by 1"
set rs = CreateObject("ADODB.Recordset")
rs.open SQL, oConn, 0, 1

sql	=	"update we_orders_missing_trustedstore set uploadedDate = getdate()"
oConn.execute(sql)

headers = "merchant order id" & vbtab & "reason"
file.writeline headers

if not rs.eof then
	'core columns
	do until rs.eof
		orderid = rs("orderid")
		reason = rs("reason")

		strline = 			orderid & vbTab
		strline = strline & reason

		file.writeline strline
		rs.movenext
	loop
end if
rs.close
set rs = nothing
file.close()

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub