set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim fs, file, filename, path, folderName
set fs = CreateObject("Scripting.FileSystemObject")
dim oXMLHTTP : set oXMLHTTP = CreateObject("Msxml2.ServerXMLHTTP")

filename = "turnto"
fileext = "txt"
nRows = 0
folderName = "c:\inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\"
path = folderName & filename & "." & fileext

if fs.FileExists(path) then fs.DeleteFile(path)
set file = fs.CreateTextFile(path)

dim expDate, myMonth, myDay, myYear
expDate = dateAdd("d",21,date)
myMonth = month(expDate)
if myMonth < 10 then myMonth = "0" & myMonth
myDay = day(expDate)
if myDay < 10 then myDay = "0" & myDay
if myHour < 10 then myHour = "0" & myHour
myYear = year(expDate)
expDate = myYear & "-" & myMonth & "-" & myDay

dim SQL, RS
dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline, itemDesc
sql	=	"select	itemid, partnumber, itemdesc, itempic, price_our, itemlongdetail, brandname, typename, modelname, url from v_co_turnto"

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

headings = "id" & vbTab & "title" & vbTab & "description" & vbTab & "google_product_category" & vbTab & "product_type" & vbTab & "link" & vbTab & "image_link" & vbTab & "condition" & vbTab & "availability" & vbTab & "price" & vbTab & "brand" & vbTab & "gtin" & vbTab & "mpn" & vbTab & "tax" & vbTab & "shipping" & vbTab & "shipping_weight" & vbTab & "adwords_labels" & vbTab & "adwords_grouping"

if not RS.eof then	
	file.WriteLine headings
	do until RS.eof
		itemid = rs("itemid")
		partnumber = rs("partnumber")
		itemDesc = rs("itemDesc")
		itempic = rs("itempic")
		itemPrice = RS("price_our")
		itemlongdetail = rs("itemlongdetail")
		BrandName = RS("brandName")
		strTypeName = rs("typename")
		ModelName = RS("modelName")
		url = RS("url")		
		compatibility = ""
		itemweight = 2
		Condition = "new"

		imageDir = "c:\inetpub\wwwroot\productpics\models\"
		imageURL = "http://www.cellularoutfitter.com/modelpics/"
		
		if len(itemDesc) > 69 then itemDesc = left(itemDesc, 66) & "..."
		
		strline = itemid & vbTab
		strline = strline & itemDesc & vbTab
		strline = strline & itemlongdetail & vbtab
		strline = strline & "Electronics > Communications > Telephony > Mobile Phone Accessories" & vbTab
		strline = strline & "Electronics > Communications > Telephony > Mobile Phone Accessories" & vbTab
		strline = strline & "http://www.cellularoutfitter.com/" & formatSEO(url) & ".html" & vbTab
		strline = strline & imageURL & itempic & vbTab
		strline = strline & Condition & vbTab
		strline = strline & "in stock" & vbTab
		strline = strline & itemPrice & vbTab
		strline = strline & BrandName & vbTab
		strline = strline & "" & vbTab
		strline = strline & partnumber & vbTab
		strline = strline & "US:CA:8.00:y" & vbTab
		strline = strline & "US:::0.00" & vbTab
		strline = strline & itemweight & vbTab
		strline = strline & strTypeName & vbTab
		strline = strline & ModelName

		file.WriteLine strline

		RS.movenext
	loop
end if
file.close()

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub