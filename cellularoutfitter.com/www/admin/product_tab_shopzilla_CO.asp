<%
pageTitle = "Create Shopzilla Product List TXT for CO"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 2400 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "CO_Shopzilla.txt"
'path = Server.MapPath("tempCSV") & "\" & filename
'path = replace(path,"admin\","")
'path = "E:\Inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename
path = server.mappath("\tempCSV\" & filename)
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Cellular Outfitter Product List upload for Shopzilla</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							Response.Write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href=""http://www.wirelessemporium.com/tempCSV/" & filename & """>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.Write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	Response.Write("Creating " & filename & ".<br>")
	Set file = fs.CreateTextFile(path)
	dim sSql, RS
	SQL = "SELECT A.itemID,A.PartNumber AS MPN,A.itemDesc_CO,A.itemLongDetail,A.itemLongDetail_CO,A.POINT1,A.POINT2,A.POINT3,A.POINT4,A.POINT5,A.POINT6,A.COMPATIBILITY,A.POINT7,"
	SQL = SQL & " A.flag1,A.UPCCode,A.itempic_CO,A.price_Retail,A.price_CO,A.brandID,A.inv_qty,"
	SQL = SQL & " B.brandName,C.modelName,C.[temp],D.typeName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.price_CO > 0 AND A.hideLive = 0 AND A.inv_qty <> 0"
	SQL = SQL & " AND A.price_CO >= 5"
	SQL = SQL & " AND A.typeid <> 16"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "Category" & vbtab & "Mfr" & vbtab & "Title" & vbtab & "Description" & vbtab & "Product URL" & vbtab & "Image URL" & vbtab & "MPID" & vbtab & "SKU" & vbtab & "UPC" & vbtab & "ISBN" & vbtab & "Stock" & vbtab & "Condition" & vbtab & "Ship Wt" & vbtab & "Ship Cost" & vbtab & "Bid" & vbtab & "Promo" & vbtab & "Price"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
				set RS2 = CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				if isnull(RS("itemDesc_CO")) then
					itemDesc_CO = "Universal"
				else 
					itemDesc_CO = RS("itemDesc_CO")
				end if
				
				if not isNull(RS("itemLongDetail_CO")) or not isNull(RS("POINT1")) or not isNull(RS("POINT2")) or not isNull(RS("POINT3")) or not isNull(RS("POINT4")) or not isNull(RS("POINT5")) or not isNull(RS("POINT6")) or not isNull(RS("COMPATIBILITY")) or not isNull(RS("POINT7")) then
					strItemLongDetail = RS("itemLongDetail_CO") & "<ul>"
					if not isNull(RS("POINT1")) and RS("POINT1") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT1") & "</li>"
					if not isNull(RS("POINT2")) and RS("POINT2") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT2") & "</li>"
					if not isNull(RS("POINT3")) and RS("POINT3") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT3") & "</li>"
					if not isNull(RS("POINT4")) and RS("POINT4") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT4") & "</li>"
					if not isNull(RS("POINT5")) and RS("POINT5") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT5") & "</li>"
					if not isNull(RS("POINT6")) and RS("POINT6") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT6") & "</li>"
					if not isNull(RS("COMPATIBILITY")) and RS("COMPATIBILITY") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("COMPATIBILITY") & "</li>"
					if not isNull(RS("POINT7")) and RS("POINT7") <> "" then strItemLongDetail = strItemLongDetail & "<li>" & RS("POINT7") & "</li>"
					strItemLongDetail = strItemLongDetail & "</ul>"
				else
					strItemLongDetail = RS("itemLongDetail_CO")
				end if
				if not isNull(strItemLongDetail) then
					strItemLongDetail = replace(replace(replace(strItemLongDetail,chr(34),"''"),vbcrlf,""),vbtab," ")
				else
					strItemLongDetail = ""
				end if
				
				id_new_field = formatSEO(itemDesc_CO)
				
				if isnull(RS("BrandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("BrandName")
				end if
				
				strline = "11510600" & vbtab
				strline = strline & BrandName & vbtab
				strline = strline & itemDesc_CO & vbtab
				strline = strline & strItemLongDetail & vbtab
				strline = strline & "http://www.CellularOutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html?id=shopzilla" & vbtab
				strline = strline & "http://www.CellularOutfitter.com/productpics/big/" & RS("itempic_CO") & vbtab
				strline = strline & RS("itemid") & vbtab
				strline = strline & RS("itemid") & vbtab
				strline = strline & RS("UPCCode") & vbtab
				strline = strline & "In Stock" & vbtab
				strline = strline & "New" & vbtab
				strline = strline & vbtab
				strline = strline & vbtab
				strline = strline & 0 & vbtab
				strline = strline & vbtab
				strline = strline & vbtab
				strline = strline & RS("price_CO")
				
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
