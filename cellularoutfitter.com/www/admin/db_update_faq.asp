<%
pageTitle = "Admin - Update CO Database - Update FAQs"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->

<h3>Update CO Database - Update FAQs</h3>

<%
strError = ""
if request.form("submitted") = "Update" then
	for a = 1 to request.form("totalCount")
		question = SQLquote(request.form("question" & a))
		answer = SQLquote(request.form("answer" & a))
		SectionID = request.form("SectionID" & a)
		select case SectionID
			case "1" : SectionName = "Order Status"
			case "2" : SectionName = "Shipping/Delivery"
			case "3" : SectionName = "Returns"
			case else : SectionName = "other"
		end select
		ListOrder = request.form("ListOrder" & a)
		if question = "" then strError = strError & "You must enter a Question for FAQ #" & a & ".<br>"
		if answer = "" then strError = strError & "You must enter an Answer for FAQ #" & a & ".<br>"
		if not isNumeric(ListOrder) then strError = strError & "You must enter valid List Order for FAQ #" & a & ".<br>"
		if strError = "" then
			SQL = "UPDATE CO_FAQ SET"
			SQL = SQL & " SectionID='" & SectionID & "',"
			SQL = SQL & " SectionName='" & SectionName & "',"
			SQL = SQL & " ListOrder='" & ListOrder & "',"
			SQL = SQL & " question='" & question & "',"
			SQL = SQL & " answer='" & answer & "'"
			SQL = SQL & " WHERE id='" & request.form("id" & a) & "'"
			response.write "<p>" & SQL & "</p>"
			oConn.execute SQL
		end if
	next
	if strError <> "" then response.write "<p>" & strError & "</p>" & vbcrlf
	strError = ""
	if request.form("questionNEW") <> "" then
		question = SQLquote(request.form("questionNEW"))
		answer = SQLquote(request.form("answerNEW"))
		SectionID = request.form("SectionIDNEW")
		select case SectionID
			case "1" : SectionName = "Order Status"
			case "2" : SectionName = "Shipping/Delivery"
			case "3" : SectionName = "Returns"
			case else : SectionName = "other"
		end select
		ListOrder = request.form("ListOrderNEW")
		if question = "" then strError = strError & "You must enter a Question for FAQ #NEW" & ".<br>"
		if answer = "" then strError = strError & "You must enter an Answer for FAQ #NEW" & ".<br>"
		if not isNumeric(ListOrder) then strError = strError & "You must enter valid List Order for FAQ #NEW" & ".<br>"
		if strError = "" then
			SQL = "INSERT INTO CO_FAQ (SectionID,SectionName,ListOrder,question,answer) VALUES ("
			SQL = SQL & "'" & SectionID & "',"
			SQL = SQL & "'" & SectionName & "',"
			SQL = SQL & "'" & ListOrder & "',"
			SQL = SQL & "'" & question & "',"
			SQL = SQL & "'" & answer & "')"
			response.write "<p>" & SQL & "</p>"
			oConn.execute SQL
		end if
	end if
	if strError <> "" then response.write "<p>" & strError & "</p>" & vbcrlf
	%>
	<h3>UPDATED!</h3>
	<%
else
	if strError = "" then
		SQL = "SELECT * FROM CO_FAQ ORDER BY SectionID, ListOrder"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		a = 0
		if RS.eof then
			response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
			response.write "<a href=""db_update_faq.asp"">Try again.</a>"
		else
			%>
			<table border="1" cellpadding="3" cellspacing="0" width="90%" align="center" class="smlText">
				<form action="db_update_faq.asp" method="post">
					<%
					do until RS.eof
						id = RS("id")
						SectionID = RS("SectionID")
						ListOrder = RS("ListOrder")
						question = RS("question")
						answer = RS("answer")
						a = a + 1
						%>
						<tr>
							<td valign="top" align="left">
								<b>Question:</b><br>
								<input type="text" name="question<%=a%>" value="<%=question%>" size="75" maxlength="240">
								<input type="hidden" name="id<%=a%>" value="<%=id%>">
							</td>
							<td valign="top" align="left">
								<b>Section:</b><br>
								<select name="SectionID<%=a%>">
									<option value="1"<%if SectionID = 1 then response.write " selected"%>>Order Status</option>
									<option value="2"<%if SectionID = 2 then response.write " selected"%>>Shipping/Delivery</option>
									<option value="3"<%if SectionID = 3 then response.write " selected"%>>Returns</option>
									<option value="4"<%if SectionID = 4 then response.write " selected"%>>other</option>
								</select>
							</td>
							<td valign="top" align="left"><b>List&nbsp;Order:</b><br>
								<input type="text" name="ListOrder<%=a%>" value="<%=ListOrder%>" size="2" maxlength="2">
							</td>
						</tr>
						<tr>
							<td valign="top" align="left" colspan="3">
								<b>Answer:</b><br>
								<textarea name="answer<%=a%>" cols="125" rows="4"><%=answer%></textarea>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="3" bgcolor="#999999"><img src="http://www.cellularoutfitter.com/images/spacer.gif" width="1" height="2" border="0"></td>
						</tr>
						<%
						RS.movenext
					loop
					%>
					<tr>
						<td valign="top" align="left" colspan="3">
							<b>ADD&nbsp;NEW</b>
						</td>
					</tr>
					<tr>
						<td valign="top" align="left">
							<b>Question:</b><br>
							<input type="text" name="questionNEW" value="" size="75" maxlength="240">
						</td>
						<td valign="top" align="left">
							<b>Section:</b><br>
							<select name="SectionIDNEW">
								<option value="1">Order Status</option>
								<option value="2">Shipping/Delivery</option>
								<option value="3">Returns</option>
								<option value="4">other</option>
							</select>
						</td>
						<td valign="top" align="left"><b>List&nbsp;Order:</b><br>
							<input type="text" name="ListOrderNEW" value="" size="2" maxlength="2">
						</td>
					</tr>
					<tr>
						<td valign="top" align="left" colspan="3">
							<b>Answer:</b><br>
							<textarea name="answerNEW" cols="125" rows="4"></textarea>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="3" bgcolor="#999999"><img src="http://www.cellularoutfitter.com/images/spacer.gif" width="1" height="2" border="0"></td>
					</tr>
					<tr>
						<td align="center" colspan="3">
							<input type="hidden" name="totalCount" value="<%=a%>">
							<input type="submit" name="submitted" value="Update">
						</td>
					</tr>
				</form>
			</table>
			<p>&nbsp;</p>
			<%
		end if
	end if
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
