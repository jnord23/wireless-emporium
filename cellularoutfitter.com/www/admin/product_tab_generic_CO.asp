<%
response.buffer = false
dim reportType, filename, ANDSQL
select case request.form("reportType")
	case "1"
		pageTitle = "Create Product List TXT for ALL CellularOutfitter.com Products"
		filename = "productList_ALL_CO.txt"
		ANDSQL = ""
	case "2"
		pageTitle = "Create Product List TXT for CellularOutfitter.com ACCESSORIES ONLY"
		filename = "productList_ACCESSORIES_CO.txt"
		ANDSQL = " AND A.typeID <> 16"
	case "3"
		pageTitle = "Create Product List TXT for CellularOutfitter.com PHONES ONLY"
		filename = "productList_PHONES_CO.txt"
		ANDSQL = " AND A.typeID = 16"
	case else
		pageTitle = "Create Generic CellularOutfitter Product List TXT File"
		filename = ""
		ANDSQL = ""
end select
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, path
if filename <> "" then
	path = server.mappath("tempCSV") & "\" & filename
	path = replace(path,"admin\","")
end if
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p><%=pageTitle%></p>
							<p>
								<input type="radio" name="reportType" value="1"<%if request.form("reportType") <> "2" and request.form("reportType") <> "3" then response.write " checked"%>>ALL Products&nbsp;&nbsp;&nbsp;
								<input type="radio" name="reportType" value="2"<%if request.form("reportType") = "2" then response.write " checked"%>>Accessories ONLY&nbsp;&nbsp;&nbsp;
								<input type="radio" name="reportType" value="3"<%if request.form("reportType") = "3" then response.write " checked"%>>Phones ONLY
							</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							response.write "<b>CreateFile:</b><br>"
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='/tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
	
<%
sub DeleteFile(path)
	if path <> "" then
		'If the file already exists, delete it.
		if fs.FileExists(path) then
			response.write "Deleting " & filename & ".<br>"
			fs.DeleteFile(path)
		end if
	end if
end sub

sub CreateFile(path)
	if path <> "" then
		'Create the file and write some data to it.
		dim SQL, RS
		dim ModelName, BrandName, strItemLongDetail, strFeatures, Condition, strline, itemDesc
		response.write "Creating " & filename & ".<br>"
		set file = fs.CreateTextFile(path)
		SQL = "SELECT A.itemID, A.PartNumber, A.itemDesc_CO, A.itemPic_CO, A.price_CO, A.inv_qty, A.itemLongDetail_CO,"
		SQL = SQL & " A.POINT1, A.POINT2, A.POINT3, A.POINT4, A.POINT5, A.POINT6, A.POINT7, A.POINT8, A.POINT9, A.POINT10,"
		SQL = SQL & " B.brandName, C.TypeName, D.ModelName"
		SQL = SQL & " FROM we_Items A INNER JOIN we_brands B ON A.brandID = B.brandID"
		SQL = SQL & " INNER JOIN we_types C ON A.typeID = C.typeID"
		SQL = SQL & " INNER JOIN we_models D ON A.modelID = D.modelID"
		SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty > 0 AND A.price_CO > 0"
		SQL = SQL & ANDSQL
		SQL = SQL & " ORDER BY A.itemID"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			file.WriteLine "id" & vbtab & "link" & vbtab & "price" & vbtab & "title" & vbtab & "description" & vbtab & "image_link" & vbtab & "brand" & vbtab & "condition" & vbtab & "part_number" & vbtab & "features"
			do until RS.eof
				if isnull(RS("modelName")) then
					ModelName = "Universal"
				else
					ModelName = RS("modelName")
				end if
				
				if isnull(RS("brandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("brandName")
				end if
				
				strItemLongDetail = replace(replace(RS("ItemLongDetail_CO"),vbcrlf," "),vbtab," ")
				strItemLongDetail = replace(strItemLongDetail,"<a href='/downloads/","<a href='http://www.cellularoutfitter.com/downloads/")
				
				strFeatures = "<ul>"
				if not isNull(RS("POINT1")) and RS("POINT1") <> "" then strFeatures = strFeatures & "<li>" & RS("POINT1") & "</li>"
				if not isNull(RS("POINT2")) and RS("POINT2") <> "" then strFeatures = strFeatures & "<li>" & RS("POINT2") & "</li>"
				if not isNull(RS("POINT3")) and RS("POINT3") <> "" then strFeatures = strFeatures & "<li>" & RS("POINT3") & "</li>"
				if not isNull(RS("POINT4")) and RS("POINT4") <> "" then strFeatures = strFeatures & "<li>" & RS("POINT4") & "</li>"
				if not isNull(RS("POINT5")) and RS("POINT5") <> "" then strFeatures = strFeatures & "<li>" & RS("POINT5") & "</li>"
				if not isNull(RS("POINT6")) and RS("POINT6") <> "" then strFeatures = strFeatures & "<li>" & RS("POINT6") & "</li>"
				if not isNull(RS("POINT7")) and RS("POINT7") <> "" then strFeatures = strFeatures & "<li>" & RS("POINT7") & "</li>"
				if not isNull(RS("POINT8")) and RS("POINT8") <> "" then strFeatures = strFeatures & "<li>" & RS("POINT8") & "</li>"
				if not isNull(RS("POINT9")) and RS("POINT9") <> "" then strFeatures = strFeatures & "<li>" & RS("POINT9") & "</li>"
				if not isNull(RS("POINT10")) and RS("POINT10") <> "" then strFeatures = strFeatures & "<li>" & RS("POINT10") & "</li>"
				strFeatures = strFeatures & "</ul>"
				strFeatures = replace(strFeatures,vbtab," ")
				if strFeatures = "<ul></ul>" then strFeatures = ""
				
				if inStr(Lcase(strItemLongDetail),"refurbished") > 0 or inStr(Lcase(RS("itemDesc_CO")),"refurbished") > 0 or inStr(Lcase(strFeatures),"refurbished") > 0 then
					Condition = "refurbished"
				else
					Condition = "new"
				end if
				
				itemDesc = RS("itemDesc_CO")
				
				strline = RS("itemid") & vbtab
				strline = strline & "http://www.cellularoutfitter.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc_CO")) & ".html" & vbtab
				strline = strline & RS("price_CO") & vbtab
				strline = strline & itemDesc & vbtab
				strline = strline & strItemLongDetail & vbtab
				strline = strline & "http://www.cellularoutfitter.com/productpics/big/" & RS("itemPic_CO") & vbtab
				strline = strline & BrandName & vbtab
				strline = strline & Condition & vbtab
				strline = strline & RS("PartNumber") & vbtab
				strline = strline & strFeatures
				file.WriteLine strline
				RS.movenext
			loop
		end if
		RS.close
		set RS = nothing
		file.close()
	end if
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
