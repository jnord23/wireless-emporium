<%header = 0%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<table border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td valign="top" width="700" align="center">
			<%
			dim rsOrdProc,strSql,orderId
			orderId = request.querystring("orderId")
			if request.querystring("orderId") = "" then
				%>
				<table width="100%" border="0" align="center" cellpadding="10" cellspacing="0">
					<tr>
						<td valign="top" align="center" bgcolor="#FFFFFF">
							No Order ID specified.
							<br><br>
							<a href="javascript:window.close();">Close</a>
						</td>
					</tr>
				</table>
				<%
				response.end
			end if
			strSql = "SELECT trackingNum"
			strSql = strSql & " FROM we_orders"
			strSql = strSql & " WHERE orderid='" & SQLquote(orderId) & "'"
			session("errorSQL") = strSql
			set rsOrdProc = Server.CreateObject("ADODB.Recordset")
			rsOrdProc.Open strSql, oConn, 3, 3
			%>
			<table width="700" border="0" bgcolor="#FFFFFF" cellpadding="10" cellspacing="0" align="center">
				<tr>
					<td align="center">
						<br>
						<table border="1" align="center" bgcolor="#FFEEEE" cellspacing="0" cellpadding="5">
							<tr>
								<td>
									<%
									if not rsOrdProc.eof then
										'USPS Tracking:
										dim TrackingNum
										TrackingNum = rsOrdProc("trackingNum")
										if not isNull(TrackingNum) then
											dim myXML, xmlDoc, mynodes
											myXML = "<TrackRequest USERID=""664WIREL0371"">"
											myXML = myXML & "<TrackID ID=""" & TrackingNum & """>"
											myXML = myXML & "</TrackID>"
											myXML = myXML & "</TrackRequest>"
											
											Set xmlHttp = CreateObject("Microsoft.XMLHTTP")
											myURL = "http://production.shippingapis.com/ShippingAPI.dll?API=TrackV2&XML=" & myXML
											xmlHttp.open "GET", myURL, False
											xmlHttp.send
											
											'set xmlDoc = CreateObject("MSXML2.DomDocument.3.0")
											set xmlDoc = CreateObject("MSXML2.DOMDocument")
											xmlDoc.async = false
											xmlDoc.loadXML(xmlHttp.responseText)
											'Set ParseErr = xmlDoc.parseError 
											'If ParseErr.errorCode <> 0 Then 
											'	response.write "<p>Error in file: " & vbCrLf & ParseErr.reason & "</p>"
											'End If
											response.write "<p class=""smlText""><strong>Order #" & orderId & "<br><br>Tracking Number: " & TrackingNum & "</strong></p>" & vbCrLf
											'response.write "<pre>" & xmlHttp.responseText & "</pre>"
											set mynodes = xmlDoc.documentElement.selectNodes("TrackInfo")
											For Each Node In mynodes
												For i = 0 TO Node.ChildNodes.Length - 1
													response.write "<p class=""smlText"">" & Node.childnodes.item(i).text & "</p>" & vbCrLf
												Next
											Next
										else
											response.write "<p class=""smlText""><strong>No Tracking Number Found for Order #" & orderId & "!</strong></p>" & vbCrLf
										end if
									else
										response.write "<p class=""smlText""><strong>Order #" & orderId & " Not Found!</strong></p>" & vbCrLf
									end if
									rsOrdProc.Close
									set rsOrdProc = nothing
									%>
									<br>
								</td>
							</tr>
						</table>
						<form><p align="center"><input type="button" value="Close" onClick="javascript:window.close();"></p></form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
