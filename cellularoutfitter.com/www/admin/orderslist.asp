<%
server.scripttimeout = 180
pageTitle = "Orders List"
header = 1
%>

<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/admin/aspdatetime.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->

<%
useStore = SQLquote(request.QueryString("useStore"))
useStoreID = SQLquote(request.QueryString("useStoreID"))
historical = SQLquote(request.QueryString("historical"))

dim adminID
adminID = Request.Cookies("admin_CO")("adminID")
if request.form("totalOrders") <> "" then
	for a = 1 to cInt(request.form("totalOrders"))
		SQL = "SELECT orderID,PhoneOrder FROM we_orders WHERE orderID='" & request.form("orderID" & a) & "'"
		if request.form("PhoneOrder" & a) = "1" then
			SQL = SQL & " AND PhoneOrder='" & request.form("PhoneOrder" & a) & "'"
		else
			SQL = SQL & " AND PhoneOrder IS NULL"
		end if
		session("errorSQL") = SQL
		set RS = server.createobject("ADODB.recordset")
		RS.open SQL, oConn, 3, 3
		
		if RS.EOF then
			sql = replace(lcase(sql),"we_orders","we_orders_historical")
			session("errorSQL") = SQL
			set RS = server.createobject("ADODB.recordset")
			RS.open SQL, oConn, 3, 3
		end if
		
		if RS.eof then
			if request.form("PhoneOrder" & a) = "1" then
				SQL = "UPDATE we_orders SET PhoneOrder='" & adminID & "' WHERE orderID='" & request.form("orderID" & a) & "'"
			else
				SQL = "UPDATE we_orders SET PhoneOrder=NULL WHERE orderID='" & request.form("orderID" & a) & "'"
			end if
			'response.write "<p>" & SQL & "</p>" & vbcrlf
			oConn.execute SQL
		end if
	next
end if
%>

<script language="javascript">
function fngotopg(pgno) {
	document.frmSearch.reset();
	document.frmSearch.txtpageno.value = pgno;
	document.frmSearch.submit();
}
function fnopennotes(orderid) {
	var url='ordernotes.asp?orderid='+orderid;
	window.open(url,'ordernote','left=0,top=0,width=800,height=400,resizable=1,locationbar=0,menubar=0,toolbar=0');
}
function fncreditnotes(orderid) {
	var url='creditnotes.asp?orderid='+orderid;
	window.open(url,'ordernote','left=0,top=0,width=400,height=150,resizable=1,locationbar=0,menubar=0,toolbar=0');
}
function fnRMA(orderid) {
	var url='RMA.asp?orderid='+orderid;
	window.open(url,'ordernote','left=0,top=0,width=500,height=400,resizable=1,locationbar=1,menubar=1,toolbar=1');
}
function fnconfirm(orderid,emailid) {
	var url='xchange.asp?orderid='+orderid+'&emailid='+emailid;
	msg=confirm("Confirm Re-Shipment and Send Item Exchange/Re-Ship Email?")
	if (msg == true) {
		window.open(url,'orderxchange','left=0,top=0,width=400,height=150,resizable=1,locationbar=0,menubar=0,toolbar=0');
	}
}
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid+"&useStore=<%=useStore%>&useStoreID=<%=useStoreID%>&historical=<%=historical%>";
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
function checkTracking(orderid) {
	var url="view_tracking.asp?orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=800,height=350,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
function resetAll() {
	for(i=0; i<document.searchForm.elements.length; i++) {
		if (document.searchForm.elements[i].type == "text") {
			document.searchForm.elements[i].value = "";
		} else if (document.searchForm.elements[i].type == "select-one") {
			document.searchForm.elements[i].selectedIndex = 0;
		}
	}
}
function fnPhoneOrder() {
	document.frmSearch.submit();
}
function fnTracking(shiptype,trackingnum) {
	if (shiptype == 1) {
		window.open("http://wwwapps.ups.com/WebTracking/OnlineTool?InquiryNumber1=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 2) {
		window.open("http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?orig&strOrigTrackNum=" + trackingnum + "&CAMEFROM=OK","tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 3) {
		window.open("http://webtrack.dhlglobalmail.com/?trackingnumber=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else {
		window.open("http://www.ups-mi.net/packageID/PackageID.aspx?PID=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	}
}
</script>

<%
Dim strDate, strOrderId, strCustId, strFname, strLName, strConfirm, strApproved, strPhone, PgNo, strEmailAddr
strDate = fnRetDateFormat(SQLquote(Request.Form("txtorderdate")),"mm/dd/yyyy")
strLName = SQLquote(Request.Form("txtLName"))
strFname = SQLquote(Request.Form("txtFName"))
strCustId = SQLquote(Request.Form("txtAccountId"))
strPhone = SQLquote(Request.Form("txtPhone"))
strOrderId = SQLquote(Request.Form("txtOrderId"))
strConfirm = SQLquote(Request.Form("txtConfirm"))
strApproved = SQLquote(Request.Form("txtApproved"))
strEmailAddr = SQLquote(Request.Form("txtEmailAddr"))
PgNo = SQLquote(Request.Form("txtpageno"))
If SQLquote(Request.Form("txthidacctid")) <> "" Then strCustId = SQLquote(Request.Form("txthidacctid"))
Dim SQL, strFilter, strJoinClause
Dim PgSize, strWhere, RecCount, TotalPages
PgSize = 50
If PgNo = "" Then PgNo = 1
If (Not ISNumeric(PgNo)) Then PgNo = 1

SQL = "SELECT Top " & PgSize & " A.accountid, A.FName, A.LName, A.Email, A.PHONE, A.pword,"
SQL = SQL & " O.ORDERID, O.ORDERDATETIME, O.ORDERSUBTOTAL, O.SHIPTYPE, O.trackingNum, O.ORDERSHIPPINGFEE, O.ORDERGRANDTOTAL, O.APPROVED, O.EMAILSENT, O.CONFIRMSENT, O.extOrderType, O.PhoneOrder"
SQL = SQL & " FROM " & useStore & "_Accounts A "
SQL = SQL & " INNER JOIN WE_ORDERS O ON A.ACCOUNTID = O.ACCOUNTID"

strWhere = " WHERE O.store = " & useStoreID

if strCustId <> "" then strWhere = strWhere & " AND A.ACCOUNTID='" & strCustId & "'"
if strLName <> "" then strWhere = strWhere & " AND A.LName LIKE '%" & strLName & "%'"
if strFname <> "" then strWhere = strWhere & " AND A.FName LIKE '%" & strFname & "%'"
if strPhone <> "" then strWhere = strWhere & " AND A.PHONE LIKE '%" & strPhone & "%'"
if strOrderId <> "" then strWhere = strWhere & " AND O.ORDERID='" & strOrderId & "'"
if strEmailAddr <> "" then strWhere = strWhere & " AND A.Email='" & strEmailAddr & "'"
if strDate <> "" then strWhere = strWhere & " AND O.ORDERDATETIME >= '" & strDate & "'"

dim strCountSQL
strCountSQL = "SELECT Count(O.ORDERID) FROM " & useStore & "_Accounts A"
strCountSQL = strCountSQL & " INNER JOIN WE_ORDERS O ON A.ACCOUNTID = O.ACCOUNTID"
strCountSQL = strCountSQL & strWhere

if PgNo <> 1 then
	SQL = SQL & " AND O.OrderId NOT IN"
	SQL = SQL & " (SELECT Top " & (PgNo-1) * PgSize & " SO.OrderId FROM " & useStore & "_Accounts SA"
	SQL = SQL & " INNER JOIN WE_ORDERS SO ON SA.ACCOUNTID = SO.ACCOUNTID"
	SQL = SQL & replace(replace(Ucase(strWhere),"A.","SA."),"O.","SO.")
	SQL = SQL & " ORDER BY SO.ORDERID)"
end if
		
SQL = SQL & strWhere & " ORDER BY O.ORDERID"
'response.write "<h3>" & SQL & "</h3>" & vbcrlf
session("errorSQL") = SQL
dim RS, objCmd
set objCmd = Server.CreateObject("ADODB.COMMAND")
set RS = Server.CreateObject("ADODB.RECORDSET")
objCmd.CommandText = "CustomerLookup"
objCmd.CommandType = 4
objCmd.Parameters.Append objCmd.CreateParameter("@sqlSTR", 200, 1,4000,SQL)
objCmd.Parameters.Append objCmd.CreateParameter("@strQryRecCnt", 200, 1,2000,strCountSQL)
objCmd.Parameters.Append objCmd.CreateParameter("@RecCount", 20, 2)
objCmd.ActiveConnection = oConn
RS.CursorLocation = 3
RS.Open objCmd,,3,1

if RS.EOF then
	RS = null
	objCmd = null
	SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
	set objCmd = Server.CreateObject("ADODB.COMMAND")
	set RS = Server.CreateObject("ADODB.RECORDSET")
	objCmd.CommandText = "CustomerLookup"
	objCmd.CommandType = 4
	objCmd.Parameters.Append objCmd.CreateParameter("@sqlSTR", 200, 1,4000,SQL)
	objCmd.Parameters.Append objCmd.CreateParameter("@strQryRecCnt", 200, 1,2000,strCountSQL)
	objCmd.Parameters.Append objCmd.CreateParameter("@RecCount", 20, 2)
	objCmd.ActiveConnection = oConn
	RS.CursorLocation = 3
	RS.Open objCmd,,3,1
end if

RecCount = objCmd.Parameters("@RecCount").Value
'TotalPages = TISPL_fnReturnTotalPages(Cdbl(RecCount),Cdbl(PgSize))
TotalPages = TISPL_fnReturnTotalPages(RecCount,PgSize)
%>

<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
	<tr class="top-text-black" bgcolor="#CCCCCC">
		<td colspan="15" width="100%">
			<table border="0" width="100%" class="top-text-black" bgcolor="#CCCCCC">
				<tr>
					<td colspan="20%" align="left"><a href="#" onClick="window.history.go(-1);"><< BACK</a></td>
					<td colspan="60%" align="center"> Order Look-up Search Criteria </td>
					<td colspan="20%" align="right"><a href="#" onClick="window.history.go(1);">FORWARD >></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<form name="searchForm" method="post" action="custlist.asp">
	<tr>
		<td class="mc-text" colspan="14">
			<table border="0">
				<tr>
					<td class="mc-text">First Name : <input class="mc-text" type="text" name="txtfname" size="8" value="<%=strFname%>">&nbsp;</td>
					<td class="mc-text">Last Name : <input class="mc-text" type="text" name="txtlname" size="10" value="<%=strLName%>">&nbsp;</td>
					<td class="mc-text">Customer # : <input class="mc-text" type="text" name="txtaccountid" size="8" value="<%=strCustId%>">&nbsp;</td>
					<td class="mc-text">Phone # : <input class="mc-text" type="text" name="txtphone" size="8" value="<%=strPhone%>"><br></td>
					<td class="mc-text">Email Address : <input class="mc-text" type="text" name="txtEmailAddr" size="25" value="<%=Request.Form("txtEmailAddr")%>"><br></td>
				</tr>
				<tr>
					<td class="mc-text">&nbsp;&nbsp;&nbsp;Order Id : <input class="mc-text" type="text" name="txtorderid" size="8" value="<%=strOrderId%>">&nbsp;</td>
					<td class="mc-text">Order Date : <input class="mc-text" type="text" name="txtorderdate" size="10" maxlength="10" value="<%=strDate%>">&nbsp;</td>
					<td class="mc-text">Email Sent : <select class="mc-text" name="txtemail"><option value="">All</option><option value="YES"<%if strEmail="YES" then Response.Write(" selected")%>>Yes</option><option value="NO" <%If strEmail="NO" Then Response.Write("selected")%>>No</option></select> &nbsp;</td>
					<td class="mc-text" colspan="2">Confirm : <select class="mc-text" name="txtconfirm"><option value="">All</option><option value="YES"<%if strConfirm="YES" then Response.Write(" selected")%>>Yes</option><option value="NO" <%If strConfirm="NO" Then Response.Write("selected")%>>No</option></select></td>
				</tr>
				<tr>
					<td colspan="12" align="center"><input type="SUBMIT" value="SEARCH" class="mc-text" id="SUBMIT1" name="SUBMIT1">&nbsp;&nbsp;&nbsp;<input type="button" value="CLEAR" class="mc-text" onClick="resetAll();"><input type="hidden" name="txtpageno" value="1"></td>
				</tr>
				<tr>
					<td colspan="12" align="center" class="mc-text"><b>Note:</b> Search on Names and Phone number uses pattern matching and retrieves all records containing entered words.<br>Search on Other fields - OrderId, OrderDate, Customer # will look for records containing exact match.</td>
				</tr>
			</table>
		</td>
	</tr>
	<input type="hidden" name="txthidacctid" value="">
	</form>
    <form name="frmSearch" method="post" action="orderslist.asp">
	<tr bgcolor="#CCCCCC" class="mc-text">
		<td valign="middle" colspan="12" align="right">
		Page No :
		<select name="selpage" class="mc-text" onChange="fngotopg(this.value);">
			<%For iLoop=1 To TotalPages%>
				<option value='<%=iLoop%>' <%If CSTR(PgNo)=Cstr(iLoop) Then Response.Write("Selected")%>><%=iLoop%></a>
			<%Next%>
		</select>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td class="top-text-black">Order<br>Id</font></td>
		<td class="top-text-black">Order<br>Date/Time</td>
		<td class="top-text-black">First<br>Name</td>
		<td class="top-text-black">Last<br>Name</td>
		<td class="top-text-black">Email<br>Address</td>
		<td class="top-text-black">Apvd.</td>
		<td class="top-text-black">Order<br>Type</td>
		<td class="top-text-black">Shipping<br>Type</td>
		<td class="top-text-black">Grand<br>Total</td>
		<td class="top-text-black">Phone<br>Order</td>
		<td class="top-text-black">Notes</td>
		<td class="top-text-black">RMA</td>
	</tr>
	<%
	dim a
	a = 0
	if not RS.eof then
		do until RS.eof
			a = a + 1
			SQL = "SELECT RMAstatus FROM we_orders WHERE OrderID = '" & RS("OrderId") & "'"
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			
			if RS2.EOF then
				SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
			end if
			
			if not RS2.eof then
				RMAstatus = RS2("RMAstatus")
			else
				RMAstatus = 0
			end if
			SQL = "SELECT ordernotes FROM we_ordernotes WHERE OrderID = '" & RS("OrderId") & "'"
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if not RS2.eof then
				ordernotes = RS2("ordernotes")
			else
				ordernotes = ""
			end if
			if not RS2.eof then
				RS2.close
				set RS2 = nothing
			end if
			if RS("approved") = true then
				approved = "YES"
			else
				approved = "&nbsp;"
			end if
			select case RS("extOrderType")
				case 1 : OrderType = "Paypal"
				case 2 : OrderType = "Google"
				case 3 : OrderType = "eBillme"
				case 4 : OrderType = "Ebay"
				case else : OrderType = "CO"
			end select
			%>
			<tr bgcolor=<%If (RS.AbsolutePosition mod 2) = 0 Then Response.Write("lightgrey") End If%>>
				<td class="mc-text" height="35"><a href="#" onClick="printinvoice('<%=RS("OrderId")%>','<%=RS("AccountId")%>');"><%=RS("OrderId")%></a></td>
				<td class="mc-text">&nbsp;<%=RS("OrderDateTime")%></td>
				<td class="mc-text">&nbsp;<%=RS("FName")%></td>
				<td class="mc-text">&nbsp;<%=RS("LName")%></td>
				<td class="mc-text">&nbsp;<a href="<%="mailto:" & RS("Email") & "?subject=" & "Regarding your order %28%23" & RS("OrderId") & "%29 from Cellular Outfitter"%>"><%=RS("Email")%></a></td>
				<td class="mc-text">&nbsp;<%=approved%></td>
				<td class="mc-text">&nbsp;<%=OrderType%></td>
				<%if left(RS("trackingNum"),4) = "1Z15" then%>
					<td class="mc-text">&nbsp;<a href="javascript:fnTracking(1,'<%=RS("trackingNum")%>');"><%=RS("ShipType")%></a></td>
				<%elseif left(RS("trackingNum"),2) = "EO" or left(RS("trackingNum"),2) = "91" then%>
					<td class="mc-text">&nbsp;<a href="javascript:fnTracking(2,'<%=RS("trackingNum")%>');"><%=RS("ShipType")%></a></td>
				<%elseif left(RS("trackingNum"),2) = "04" then%>
					<td class="mc-text">&nbsp;<a href="javascript:fnTracking(3,'<%=RS("trackingNum")%>');"><%=RS("ShipType")%></a></td>
				<%elseif RS("trackingNum") <> "" then%>
					<td class="mc-text">&nbsp;<a href="javascript:fnTracking(4,'WE<%=RS("OrderId")%>');"><%=RS("ShipType")%></a></td>
				<%else%>
					<td class="mc-text">&nbsp;<%=RS("ShipType")%></td>
				<%end if%>
				<td class="mc-text">&nbsp;<%=RS("OrderGrandTotal")%></td>
				<td class="mc-text">&nbsp;<input type="checkbox" name="PhoneOrder<%=a%>" value="1" onClick="fnPhoneOrder();"<%if not isNull(RS("PhoneOrder")) and RS("PhoneOrder") <> "" then response.write " checked"%>><input type="hidden" name="orderID<%=a%>" value="<%=RS("orderID")%>"></td>
				<td align="center" class="mc-text" title="<%If ordernotes = "" Then Response.Write("No notes entered") Else Response.Write(Left(Replace(ordernotes,"""",""),30)) End If%>"><a href="#" onClick="fnopennotes('<%=RS("orderid")%>');"><img src='/images/Notes<%If ordernotes = "" Then response.write "_none"%>.gif' border=0></a></td>
				<%
				select case RMAstatus
					case 1 : RMAstatus = "EXCH SENT"
					case 2 : RMAstatus = "EXCH REC'D"
					case 3 : RMAstatus = "REF SENT"
					case 4 : RMAstatus = "REF REC'D"
					case 5 : RMAstatus = "EXCHANGED"
					case 6 : RMAstatus = "PART RFND"
					case 7 : RMAstatus = "FULL RFND"
					case else : RMAstatus = "START RMA"
				end select
				%>
				<td align="center" class="mc-text" title="<%If RMAstatus = "" Then Response.Write("No RMA started") Else Response.Write(Left(Replace(RMAstatus,"""",""),30)) End If%>"><a href="#" onClick="fnRMA('<%=RS("orderid")%>')"><b><%=RMAstatus%></b></a></td>
			</tr>
			<%
			RS.movenext
		loop
	else
		%>
		<tr><td colspan="16" class="mc-text" align="center"><b>No Records Found</b></td></tr>
		<%
	end if
	%>
	<input type="hidden" name="totalOrders" value="<%=a%>">
	</form>
</table>

<%
set objCmd = nothing
RS.close
set RS = nothing
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
