<%
response.buffer = false
pageTitle = "Create CO Product List TXT for Amazon Ads"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 2400 'seconds

dim fs, file, filename, path
filename = "productList_AmazonAds_CO.txt"
'path = "E:\Inetpub\wwwroot\cellularoutfitter.com\www\tempCSV\" & filename
path = server.mappath("\tempCSV\" & filename)
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>CO Product List TXT for Amazon Ads</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							Response.Write("Creating " & filename & ".<br>")
							Response.Write("<b>CreateFile:</b><br>")
							set fs = CreateObject("Scripting.FileSystemObject")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href=""http://www.cellularoutfitter.com/tempCSV/" & filename & """>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.Write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	set file = fs.CreateTextFile(path)
	SQL = "SELECT A.itemID,A.PartNumber AS MPN,A.itemDesc_CO,A.itemLongDetail,A.itemLongDetail_CO,A.POINT1,A.POINT2,A.POINT3,A.POINT4,A.POINT5,A.POINT6,A.COMPATIBILITY,A.POINT7,"
	SQL = SQL & " A.flag1,A.UPCCode,A.itempic_CO,A.price_Retail,A.price_CO,A.brandID,A.inv_qty,"
	SQL = SQL & " B.brandName,C.modelName,C.[temp],D.typeName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0"
	SQL = SQL & " AND A.price_CO > 0"
	SQL = SQL & " AND A.typeid <> 16"
	SQL = SQL & " AND A.PartNumber NOT LIKE 'DEC%'"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "Category" & vbtab & "Title" & vbtab & "Link" & vbtab & "SKU" & vbtab & "Price" & vbtab & "Brand" & vbtab & "Department" & vbtab & "UPC" & vbtab & "Image" & vbtab & "Description" & vbtab & "Manufacturer" & vbtab & "Mfr part number" & vbtab & "Age" & vbtab & "Band material" & vbtab & "Bullet point1" & vbtab & "Bullet point2" & vbtab & "Bullet point3" & vbtab & "Bullet point4" & vbtab & "Bullet point5" & vbtab & "Color" & vbtab & "Color and finish" & vbtab & "Computer CPU speed" & vbtab & "Computer memory size" & vbtab & "Display size" & vbtab & "Digital Camera Resolution" & vbtab & "Display technology" & vbtab & "Flash drive size" & vbtab & "Flavor" & vbtab & "Gender" & vbtab & "Hard disk size" & vbtab & "Height" & vbtab & "Included RAM size" & vbtab & "Item package quantity" & vbtab & "Keywords1" & vbtab & "Keywords2" & vbtab & "Keywords3" & vbtab & "Keywords4" & vbtab & "Keywords5" & vbtab & "League and Team" & vbtab & "Length" & vbtab & "Material" & vbtab & "Maximum age" & vbtab & "Memory Card Type" & vbtab & "Metal type" & vbtab & "Minimum age" & vbtab & "Model Number" & vbtab & "Operating system" & vbtab & "Optical zoom" & vbtab & "Other image-url1" & vbtab & "Other image-url2" & vbtab & "Other image-url3" & vbtab & "Other image-url4" & vbtab & "Other image-url5" & vbtab & "Other image-url6" & vbtab & "Other image-url7" & vbtab & "Other image-url8" & vbtab & "Ring size" & vbtab & "Scent" & vbtab & "Shipping Weight" & vbtab & "Shipping Cost" & vbtab & "Size" & vbtab & "Size per pearl" & vbtab & "Theme HPC " & vbtab & "Total Diamond Weight" & vbtab & "Watch movement" & vbtab & "Weight" & vbtab & "Width"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
				set RS2 = CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				if isnull(RS("itemDesc_CO")) then
					itemDesc_CO = "Universal"
				else 
					itemDesc_CO = RS("itemDesc_CO")
				end if
				if isnull(RS("typeName")) then
					stypeName = "Universal"
				else 
					stypeName = RS("typeName")
				end if
				if isnull(RS("ModelName")) then
					ModelName = "Universal"
				else 
					ModelName = RS("ModelName")
				end if
				if isnull(RS("BrandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("BrandName")
				end if
				
				strItemLongDetail = RS("itemLongDetail_CO")
				if not isNull(strItemLongDetail) then strItemLongDetail = replace(replace(replace(strItemLongDetail,chr(34),"''"),vbcrlf,""),vbtab," ")
				
				id_new_field = formatSEO(itemDesc_CO)
				priceCO = formatNumber(cdbl(RS("price_CO")),2)
				
				strline = "Electronics and Office > Electronics Accessories and Supplies" & vbtab
				strline = strline & chr(34) & itemDesc_CO & chr(34) & vbtab
				strline = strline & "http://www.CellularOutfitter.com/p-" & RS("itemid") & "-" & id_new_field & ".html?utm_source=Amazon&utm_medium=CPC&utm_campaign=Product%2BAds" & vbtab
				strline = strline & RS("itemid") & vbtab
				strline = strline & priceCO & vbtab
				strline = strline & BrandName & vbtab
				strline = strline & vbtab	'Department
				strline = strline & RS("UPCCode") & vbtab
				strline = strline & "http://www.CellularOutfitter.com/productpics/big/" & RS("itempic_CO") & vbtab
				strline = strline & chr(34) & removeHTML(strItemLongDetail) & chr(34) & vbtab
				strline = strline & BrandName & vbtab
				strline = strline & RS("MPN") & vbtab
				
				strline = strline & vbtab	'Age
				strline = strline & vbtab	'Band material
				strline = strline & vbtab	'Bullet point1
				strline = strline & vbtab	'Bullet point2
				strline = strline & vbtab	'Bullet point3
				strline = strline & vbtab	'Bullet point4
				strline = strline & vbtab	'Bullet point5
				strline = strline & vbtab	'Color
				strline = strline & vbtab	'Color and finish
				strline = strline & vbtab	'Computer CPU speed
				strline = strline & vbtab	'Computer memory size
				strline = strline & vbtab	'Display size
				strline = strline & vbtab	'Digital Camera Resolution
				strline = strline & vbtab	'Display technology
				strline = strline & vbtab	'Flash drive size
				strline = strline & vbtab	'Flavor
				strline = strline & vbtab	'Gender
				strline = strline & vbtab	'Hard disk size
				strline = strline & vbtab	'Height
				strline = strline & vbtab	'Included RAM size
				strline = strline & vbtab	'Item package quantity
				strline = strline & vbtab	'Keywords1
				strline = strline & vbtab	'Keywords2
				strline = strline & vbtab	'Keywords3
				strline = strline & vbtab	'Keywords4
				strline = strline & vbtab	'Keywords5
				strline = strline & vbtab	'League and Team
				strline = strline & vbtab	'Length
				strline = strline & vbtab	'Material
				strline = strline & vbtab	'Maximum age
				strline = strline & vbtab	'Memory Card Type
				strline = strline & vbtab	'Metal type
				strline = strline & vbtab	'Minimum age
				strline = strline & vbtab	'Model Number
				strline = strline & vbtab	'Operating system
				strline = strline & vbtab	'Optical zoom
				strline = strline & vbtab	'Other image-url1
				strline = strline & vbtab	'Other image-url2
				strline = strline & vbtab	'Other image-url3
				strline = strline & vbtab	'Other image-url4
				strline = strline & vbtab	'Other image-url5
				strline = strline & vbtab	'Other image-url6
				strline = strline & vbtab	'Other image-url7
				strline = strline & vbtab	'Other image-url8
				strline = strline & vbtab	'Ring size
				strline = strline & vbtab	'Scent
				strline = strline & vbtab	'Shipping Weight
				strline = strline & vbtab	'Shipping Cost
				strline = strline & vbtab	'Size
				strline = strline & vbtab	'Size per pearl
				strline = strline & vbtab	'Theme HPC 
				strline = strline & vbtab	'Total Diamond Weight
				strline = strline & vbtab	'Watch movement
				strline = strline & vbtab	'Weight
				strline = strline & vbtab	'Width
				
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub

function removeHTML(str)
	removeHTML = replace(str,"<br>"," ",1,99,1)
	removeHTML = replace(replace(removeHTML,"<p>"," ",1,99,1),"</p>"," ",1,99,1)
	removeHTML = replace(replace(removeHTML,"<b>","",1,99,1),"</b>","",1,99,1)
	removeHTML = replace(removeHTML,"<b class='bigtext'>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<li>","",1,99,1),"</li>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<ul>","",1,99,1),"</ul>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<font color=""#FF0000"">","",1,99,1),"</font>","",1,99,1)
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
