set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim fs, file, path, nFileIdx, fileBaseName, folderName, nRows
nRows = 0
nFileIdx = 1
folderName = "C:\inetpub\wwwroot\cellularoutfitter.com\www"
fileBaseName = "sitemap"
set fs = CreateObject("Scripting.FileSystemObject")

path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
file.WriteLine "<url>"
file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/</loc>"
file.WriteLine vbtab & "<priority>0.90</priority>"
file.WriteLine vbtab & "<changefreq>weekly</changefreq>"
file.WriteLine vbtab & "<lastmod>" & RFC3339(now) & "</lastmod>"
file.WriteLine "</url>"




dim temp(100)
'========== BRAND
temp(0) = "/iphone-accessories.html"
temp(1) = "/b-17-apple-cell-phone-accessories.html"
temp(2) = "/b-14-blackberry-cell-phone-accessories.html"
temp(3) = "/b-20-htc-cell-phone-accessories.html"
temp(4) = "/b-4-lg-cell-phone-accessories.html"
temp(5) = "/b-5-motorola-cell-phone-accessories.html"
temp(6) = "/b-6-nextel-cell-phone-accessories.html"
temp(7) = "/b-7-nokia-cell-phone-accessories.html"
temp(8) = "/b-16-palm-cell-phone-accessories.html"
temp(9) = "/b-18-pantech-cell-phone-accessories.html"
temp(10) = "/b-9-samsung-cell-phone-accessories.html"
temp(11) = "/b-10-sanyo-cell-phone-accessories.html"
temp(12) = "/b-19-t-mobile-sidekick-cell-phone-accessories.html"
temp(13) = "/b-11-siemens-cell-phone-accessories.html"
temp(14) = "/b-2-sony-ericsson-cell-phone-accessories.html"
temp(15) = "/b-15-other-cell-phone-accessories.html"

'========== CATEGORY
temp(16) = "/c-3-cell-phone-covers-and-gel-skins.html"
temp(17) = "/c-7-cell-phone-cases-and-pouches.html"
temp(18) = "/c-18-cell-phone-screen-protectors.html"
temp(19) = "/c-19-cell-phone-vinyl-skins.html"
temp(20) = "/c-2-cell-phone-chargers-and-data-cables.html"
temp(21) = "/c-1-cell-phone-batteries.html"
temp(22) = "/c-5-cell-phone-bluetooth-and-hands-free.html"
temp(23) = "/c-6-cell-phone-holsters-and-car-mounts.html"
temp(24) = "/sb-0-sm-0-sc-8-cell-phone-other-accessories.html"
temp(25) = "/wholesale-cell-phones.html"

'========== Cell Phones				
temp(26) = "/cp-sb-20-htc-wholesale-cell-phones.html"
temp(27) = "/cp-sb-1-utstarcom-wholesale-cell-phones.html"
temp(28) = "/cp-sb-2-sony-ericsson-wholesale-cell-phones.html"
temp(29) = "/cp-sb-4-lg-wholesale-cell-phones.html"
temp(30) = "/cp-sb-5-motorola-wholesale-cell-phones.html"
temp(31) = "/cp-sb-6-nextel-wholesale-cell-phones.html"
temp(32) = "/cp-sb-7-nokia-wholesale-cell-phones.html"
temp(33) = "/cp-sb-9-samsung-wholesale-cell-phones.html"
temp(34) = "/cp-sb-14-blackberry-wholesale-cell-phones.html"
temp(35) = "/cp-sb-15-other-wholesale-cell-phones.html"
temp(36) = "/cp-sb-16-palm-wholesale-cell-phones.html"
temp(37) = "/cp-sb-17-apple-wholesale-cell-phones.html"
temp(38) = "/cp-sb-18-pantech-wholesale-cell-phones.html"
temp(39) = "/cp-sb-19-t-mobile-sidekick-wholesale-cell-phones.html"
				
dim a
for a = 0 to 39
	if "" <> temp(a) and not isnull(temp(a)) then
		URL = temp(a)
		file.WriteLine "<url>"
		file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com" & URL & "</loc>"
		file.WriteLine vbtab & "<priority>0.90</priority>"
		file.WriteLine "</url>"
	end if
next

'============================================== BRAND-MODEL START
dim strBrandModelSQL, objRsBrandModel
strBrandModelSQL = 	"select 	distinct c.brandname, a.modelid, a.modelname" & vbcrlf & _
					"from 	we_models a join we_items b " & vbcrlf & _
					"	on 	a.modelid = b.modelid join we_brands c " & vbcrlf & _
					"	on 	a.brandid = c.brandid " & vbcrlf & _
					"where 	a.hidelive = 0 and b.hidelive = 0 and b.price_co > 0" & vbcrlf & _
					"	and c.brandType < 2 and c.brandid not in (17) and a.isTablet = 0" & vbcrlf & _
					"union" & vbcrlf & _
					"select distinct c.brandname, a.modelid, a.modelname" & vbcrlf & _
					"from 	we_models a join we_items b " & vbcrlf & _
					"	on 	a.modelid = b.modelid join we_brands c " & vbcrlf & _
					"	on 	a.brandid = c.brandid " & vbcrlf & _
					"where 	a.hidelive = 0 and b.hidelive = 0 and b.price_co > 0 and c.brandid in (17) " & vbcrlf & _
					"order by 1, 2 desc"
					
set objRsBrandModel	= oConn.execute(strBrandModelSQL)

do until objRsBrandModel.EOF
	url = "m-" & objRsBrandModel("modelID") & "-" & formatSEO(objRsBrandModel("brandName")) & "-" & formatSEO(objRsBrandModel("modelName")) & "-cell-phone-accessories.html"
	call fWriteFile(url)

	objRsBrandModel.movenext
loop
set objRsBrandModel = nothing
'============================================== BRAND-MODEL END	

'============================================== category-brand START
dim strCateBrandSQL, objRsCateBrand
strCateBrandSQL	=	"select	distinct 5 typeid, 'bluetooth and hands-free' typename, x.brandid, x.brandName" & vbcrlf & _
					"from	(	select	a.brandid, a.brandName, '''' + replace(handsfreetypes, ',', ''',''') + '''' handsfreetypes" & vbcrlf & _
					"			from	we_brands a join we_models b" & vbcrlf & _
					"				on	a.brandid = b.brandid" & vbcrlf & _
					"			where	a.brandtype < 2 and a.brandid not in (12,8) " & vbcrlf & _
					"				and	b.handsfreetypes is not null	) x join " & vbcrlf & _
					"		(	select	distinct '''' + convert(varchar(10), handsfreetype) + '''' handsfreetype" & vbcrlf & _
					"			from	we_items" & vbcrlf & _
					"			where	hidelive = 0 " & vbcrlf & _
					"				and	inv_qty <> 0" & vbcrlf & _
					"				and	typeid = 5	) y" & vbcrlf & _
					"	on	x.handsfreetypes like '%' + y.handsfreetype + ',%'" & vbcrlf & _
					"union" & vbcrlf & _
					"select	distinct 19 typeid, 'Vinyl Skins' typename, a.brandid, b.brandName" & vbcrlf & _
					"from	we_items_musicskins a join we_brands b" & vbcrlf & _
					"	on	a.brandid = b.brandid" & vbcrlf & _
					"where	a.skip = 0 and a.deleteItem = 0 and a.artist <> '' and a.artist is not null and a.designname <> '' and a.designname is not null	" & vbcrlf & _
					"	and b.brandid not in (12) " & vbcrlf & _
					"	and b.brandtype < 2" & vbcrlf & _
					"union" & vbcrlf & _
					"select	distinct c.typeid, c.typeName, a.brandid, a.brandname" & vbcrlf & _
					"from	we_brands a join we_items b " & vbcrlf & _
					"	on	a.brandid = b.brandid join v_subtypeMatrix_co c" & vbcrlf & _
					"	on	b.subtypeid = c.subtypeid" & vbcrlf & _
					"where	c.typeid not in (5,8,16) and b.hidelive = 0 " & vbcrlf & _
					"	and a.brandid not in (12) " & vbcrlf & _
					"	and a.brandtype < 2 " & vbcrlf & _
					"order by 1, 2, 3"

set objRsCateBrand = oConn.execute(strCateBrandSQL)

do until objRsCateBrand.EOF
	url = "sc-" & objRsCateBrand("typeid") & "-sb-" & objRsCateBrand("brandid") & "-cell-phone-" & formatSEO(replace(replace(objRsCateBrand("typename"), "&amp;", "and"), "&", "and")) & "-for-" & formatSEO(objRsCateBrand("brandname")) & ".html"
	call fWriteFile(url)
	
	objRsCateBrand.movenext
loop
set objRsCateBrand = nothing
'============================================== category-brand END


'============================================== brand-model-category START
dim strBMCSQL, objRsBMC
strBMCSQL	=	"select	distinct 'bluetooth and hands-free' as typename, 5 typeid" & vbcrlf & _
				"	, 	x.brandid, x.brandname, x.modelid, x.modelname" & vbcrlf & _
				"from	(	select	c.brandid, c.brandname, a.modelid, a.modelname, '''' + replace(a.handsfreetypes, ',', ''',''') + '''' handsfreetypes" & vbcrlf & _
				"			from	we_models a join we_items b " & vbcrlf & _
				"				on	a.modelid = b.modelid join we_brands c " & vbcrlf & _
				"				on	a.brandid = c.brandid " & vbcrlf & _
				"			where	a.hidelive = 0 and c.brandtype < 2 and b.hidelive = 0 and a.isPhone = 1" & vbcrlf & _
				"				and a.modelname not like 'all models%' and b.inv_qty <> 0 " & vbcrlf & _
				"				and	a.handsfreetypes is not null ) x join" & vbcrlf & _
				"		(	select	distinct '''' + convert(varchar(10), handsfreetype) + '''' handsfreetype" & vbcrlf & _
				"			from	we_items" & vbcrlf & _
				"			where	hidelive = 0 and inv_qty <> 0 and typeid = 5 ) y" & vbcrlf & _
				"	on	x.handsfreetypes like '%' + y.handsfreetype + ',%'" & vbcrlf & _
				"union" & vbcrlf & _
				"select	distinct 'Vinyl Skins Select' as typename, 19 typeid" & vbcrlf & _
				"	,	c.brandid, c.brandname, a.modelid, a.modelname" & vbcrlf & _
				"from	we_models a join we_items_musicskins b " & vbcrlf & _
				"	on	a.modelid = b.modelid join we_brands c " & vbcrlf & _
				"	on	a.brandid = c.brandid " & vbcrlf & _
				"where	a.hidelive = 0 and c.brandType < 2 and a.isPhone = 1" & vbcrlf & _
				"	and a.modelname not like 'all models%'" & vbcrlf & _
				"	and	b.skip = 0 and b.deleteItem = 0 and b.artist <> '' and b.artist is not null and b.designname <> '' and b.designname is not null" & vbcrlf & _
				"union	" & vbcrlf & _
				"select	distinct case when v.typeid = 19 then v.typename + ' Select' else v.typename end as typename, v.typeid" & vbcrlf & _
				"	,	c.brandid, c.brandname, a.modelid, a.modelname" & vbcrlf & _
				"from	we_models a join we_items b " & vbcrlf & _
				"	on	a.modelid = b.modelid join we_brands c " & vbcrlf & _
				"	on	a.brandid = c.brandid join v_subtypematrix_co v" & vbcrlf & _
				"	on	b.subtypeid = v.subtypeid" & vbcrlf & _
				"where	a.hidelive = 0 and c.brandtype < 2 and b.hidelive = 0 and a.isPhone = 1" & vbcrlf & _
				"	and a.modelname not like 'all models%' and b.inv_qty <> 0 " & vbcrlf & _
				"	and	v.typeid not in (5,8,16)"

set objRsBMC = oConn.execute(strBMCSQL)

do until objRsBMC.EOF
	url = "sb-" & objRsBMC("brandID") & "-sm-" & objRsBMC("modelID") & "-sc-" & objRsBMC("typeID") & "-" & formatSEO(objRsBMC("brandName")) & "-" & formatSEO(objRsBMC("modelName")) & "-" & formatSEO(replace(replace(objRsBMC("typename"), "&amp;", "and"), "&", "and")) & ".html"
	call fWriteFile(url)

	objRsBMC.movenext
loop
set objRsBMC = nothing
'============================================== brand-model-category END

'============================================== product page START
dim pSQL, objRsProduct
pSQL	=	"exec sp_topSellingProductsBySite 2,30,20000"
			
set objRsProduct = oConn.execute(pSQL)

do until objRsProduct.EOF
	URL = "p-" & objRsProduct("itemID") & "-" & formatSEO(objRsProduct("itemDesc_CO")) & ".html"
	call fWriteFile(url)

	objRsProduct.movenext
loop
'============================================== product page END


'============================================== music skins START
'commented on 6/11/2013 for top 20000 pdp pages by Terry
'dim msSQL, objRsMS
'msSQL	=	"select a.id as itemID, a.artist + ' ' + a.designName + ' Music Skins for ' + c.brandname + ' ' + b.modelname as itemDesc_CO" & vbcrlf & _
'			"from 	we_items_musicSkins a join we_models b " & vbcrlf & _
'			"	on 	a.modelID = b.modelID join we_brands c" & vbcrlf & _
'			"	on 	a.brandid = c.brandid " & vbcrlf & _
'			"where 	a.skip = 0 " & vbcrlf & _
'			"	and a.deleteItem = 0 " & vbcrlf & _
'			"	and (a.artist <> '' and a.artist is not null) " & vbcrlf & _
'			"	and (a.designname <> '' and a.designname is not null) "
'			
'set objRsMS = oConn.execute(msSQL)
'
'do until objRsMS.EOF
'	URL = "p-" & objRsMS("itemID") & "-" & formatSEO(objRsMS("itemDesc_CO")) & ".html"
'	call fWriteFile(url)
'
'	objRsMS.movenext
'loop
'============================================== product page END
file.WriteLine "</urlset>"
file.close()
set file = nothing


'============================================== index site map START
path = folderName & "\" & fileBaseName & ".xml"
'If the file already exists, delete it.
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<sitemapindex xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"">"

for i=1 to nFileIdx
	file.WriteLine "<sitemap>"
	file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/" & fileBaseName & i & ".xml</loc>"
	file.WriteLine vbtab & "<lastmod>" & RFC3339(now) & "</lastmod>"
	file.WriteLine "</sitemap>"
next

file.WriteLine "</sitemapindex>"
file.close()
set file = nothing
'============================================== index site map END




sub fWriteFile(pStrUrl)
	nRows = nRows + 1
	
	if nRows > 45000 then
		nFileIdx = nFileIdx + 1
		nRows = 0
		file.WriteLine "</urlset>"
		file.close()
		set file = nothing
		
		path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
		if fs.FileExists(path) then
			fs.DeleteFile(path)
		end if
		set file = fs.CreateTextFile(path)
		file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
		file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
	end if
	
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.cellularoutfitter.com/" & pStrUrl & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"	
end sub

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o"),"!","-")
		formatSEO = replace(replace(formatSEO, "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function

function RFC3339(myDate)
	dim myDays, myMonth, myYear, myHours, myMonths, mySeconds
	myDate = dateAdd("h",8,myDate)
	myDays = zeroPad(day(myDate),2)
	myMonth = zeroPad(month(myDate),2)
	myYear = year(myDate)
	myHours = zeroPad(hour(myDate),2)
	myMinutes = zeroPad(minute(myDate),2)
	mySeconds = zeroPad(second(myDate),2)
	RFC3339 = myYear & "-" & myMonth & "-" & myDays & "T" & myHours & ":" & myMinutes & ":" & mySeconds & "Z"
end function
 
function zeroPad(m,t)
	zeroPad = String(t-Len(m),"0") & m
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub