<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<body>
<center>

<table width="660" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="center" valign="top" colspan="3" style="font-family: Arial, Helvetica, sans-serif; color: #000000; font-size: 11px;">
			If you are having trouble viewing this e-mail, please <a href="http://www.cellularoutfitter.com/Summer2010.asp">click here</a>.
		</td>
	</tr>
	<tr>
		<td align="center" valign="top" colspan="3"><a href="http://www.cellularoutfitter.com/"><img src="http://www.cellularoutfitter.com/images/email/CO_blast_banner1.jpg" width="660" height="180" border="0" alt="Cellular Outfitter - Wholesale Prices to the Public"></a></td>
	</tr>
	<tr>
		<td align="center" valign="top" colspan="3"><a href="http://www.cellularoutfitter.com/"><img src="http://www.cellularoutfitter.com/images/email/CO_10_banner.jpg" width="660" height="106" border="0" alt="Sizzling Hot Summer Deals! 10% off - Coupon Code: COSUMMER10"></a></td>
	</tr>
	<tr>
		<td width="1" align="center" valign="top" bgcolor="#CCCCCC"><img src="http://www.cellularoutfitter.com/images/email/spacer.gif" width="1" height="1" border="0"></td>
		<td width="658" align="center" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="100%" align="center" valign="top">
						<table width="595" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td width="100%" align="left" valign="top">
									<p style="font-family: Arial, Helvetica, sans-serif; color: #000000; font-size: 14px;">
										For a limited time only CellularOutfitter.com is offering an exclusive discount code to our most exclusive customers.
										This is the best opportunity to take advantage of WHOLESALE PRICING at an even greater discount than our published prices.
										Click on any of the category links below to get started and be sure to use coupon code <b>COSUMMER10</b> at checkout.
										This coupon will get you an additional <b><i>10% OFF</i></b> our published wholesale price.
										Buy more and save more now until the end of May - <b><i>Hurry</i></b>!
									</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="100%" align="center" valign="top">
						<table width="595" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td width="100%" align="center" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td align="center" valign="top">
												<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">
													<span style="color: #999999; font-size: 16px;">Covers</span><br>
													<a href="http://www.cellularoutfitter.com/c-3-cell-phone-covers.html"><img src="http://www.cellularoutfitter.com/images/email/covers.jpg" width="158" height="149" border="0" alt="Covers"></a><br>
													<span style="color: #3399CC; font-size: 14px;">Starting at $7.00</span><br>
													<a href="http://www.cellularoutfitter.com/c-3-cell-phone-covers.html"><img src="http://www.cellularoutfitter.com/images/email/find_more_button.jpg" width="124" height="24" border="0" alt="Find More"></a>
												</p>
											</td>
											<td width="7" align="center" valign="top"><img src="http://www.cellularoutfitter.com/images/email/dot_vline.jpg" width="7" height="229" border="0"></td>
											<td align="center" valign="top">
												<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">
													<span style="color: #999999; font-size: 16px;">Bluetooth Headsets</span><br>
													<a href="http://www.cellularoutfitter.com/bluetooth.html"><img src="http://www.cellularoutfitter.com/images/email/bt.jpg" width="158" height="149" border="0" alt="Bluetooth Headsets"></a><br>
													<span style="color: #3399CC; font-size: 14px;">Starting at $15.00</span><br>
													<a href="http://www.cellularoutfitter.com/bluetooth.html"><img src="http://www.cellularoutfitter.com/images/email/find_more_button.jpg" width="124" height="24" border="0" alt="Find More"></a>
												</p>
											</td>
											<td width="7" align="center" valign="top"><img src="http://www.cellularoutfitter.com/images/email/dot_vline.jpg" width="7" height="229" border="0"></td>
											<td align="center" valign="top">
												<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">
													<span style="color: #999999; font-size: 16px;">Chargers</span><br>
													<a href="http://www.cellularoutfitter.com/c-2-cell-phone-chargers.html"><img src="http://www.cellularoutfitter.com/images/email/charger.jpg" width="158" height="149" border="0" alt="Chargers"></a><br>
													<span style="color: #3399CC; font-size: 14px;">Starting at $5.00</span><br>
													<a href="http://www.cellularoutfitter.com/c-2-cell-phone-chargers.html"><img src="http://www.cellularoutfitter.com/images/email/find_more_button.jpg" width="124" height="24" border="0" alt="Find More"></a>
												</p>
											</td>
										</tr>
										<tr>
											<td colspan="5" align="center" valign="top"><img src="http://www.cellularoutfitter.com/images/email/dot_hline.jpg" width="595" height="5" border="0"></td>
										</tr>
										<tr>
											<td align="center" valign="top">
												<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">
													<span style="color: #999999; font-size: 16px;">Cases</span><br>
													<a href="http://www.cellularoutfitter.com/c-7-cell-phone-cases-pouches.html"><img src="http://www.cellularoutfitter.com/images/email/cases.jpg" width="158" height="149" border="0" alt="Cases"></a><br>
													<span style="color: #3399CC; font-size: 14px;">Starting at $5.00</span><br>
													<a href="http://www.cellularoutfitter.com/c-7-cell-phone-cases-pouches.html"><img src="http://www.cellularoutfitter.com/images/email/find_more_button.jpg" width="124" height="24" border="0" alt="Find More"></a>
												</p>
											</td>
											<td width="7" align="center" valign="top"><img src="http://www.cellularoutfitter.com/images/email/dot_vline.jpg" width="7" height="229" border="0"></td>
											<td align="center" valign="top">
												<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">
													<span style="color: #999999; font-size: 16px;">Batteries</span><br>
													<a href="http://www.cellularoutfitter.com/c-1-cell-phone-batteries.html"><img src="http://www.cellularoutfitter.com/images/email/battery.jpg" width="158" height="149" border="0" alt="Batteries"></a><br>
													<span style="color: #3399CC; font-size: 14px;">Starting at $15.00</span><br>
													<a href="http://www.cellularoutfitter.com/c-1-cell-phone-batteries.html"><img src="http://www.cellularoutfitter.com/images/email/find_more_button.jpg" width="124" height="24" border="0" alt="Find More"></a>
												</p>
											</td>
											<td width="7" align="center" valign="top"><img src="http://www.cellularoutfitter.com/images/email/dot_vline.jpg" width="7" height="229" border="0"></td>
											<td align="center" valign="top">
												<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">
													<span style="color: #999999; font-size: 16px;">Data Cables</span><br>
													<a href="http://www.cellularoutfitter.com/c-13-cell-phone-data-cables-memory-cards.html"><img src="http://www.cellularoutfitter.com/images/email/data.jpg" width="158" height="149" border="0" alt="Data Cables"></a><br>
													<span style="color: #3399CC; font-size: 14px;">Starting at $5.00</span><br>
													<a href="http://www.cellularoutfitter.com/c-13-cell-phone-data-cables-memory-cards.html"><img src="http://www.cellularoutfitter.com/images/email/find_more_button.jpg" width="124" height="24" border="0" alt="Find More"></a>
												</p>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="100%" align="right" valign="bottom">
						<img src="http://www.cellularoutfitter.com/images/email/lightgray.jpg" width="658" height="15" border="0">
					</td>
				</tr>
				<tr>
					<td width="100%" align="right" valign="bottom">
						<a href="http://www.cellularoutfitter.com/"><img src="http://www.cellularoutfitter.com/images/email/darkgray.jpg" width="658" height="28" border="0" alt="View Our Website"></a>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" colspan="3">
						<table border="0" width="100%" align="center">
							<tr>
								<td width="100%" align="center" valign="middle">
									<a href="http://www.facebook.com/cellularoutfitter"><img src="http://www.cellularoutfitter.com/images/email/facebook.jpg" width="25" height="23" border="0" alt="Facebook"></a>
									<a href="http://twitter.com/celloutfitter"><img src="http://www.cellularoutfitter.com/images/email/twitter.jpg" width="25" height="23" border="0" alt="Twitter"></a>
									<a href="http://www.cellularoutfitter.com/blog"><img src="http://www.cellularoutfitter.com/images/email/blog.jpg" width="25" height="23" border="0" alt="Blog"></a>
								</td>
							</tr>
							<tr>
								<td width="100%" align="center" valign="middle">
									&nbsp;
									<a href="http://www.cellularoutfitter.com/b-7-nokia-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/Nokia.jpg" border="0" alt="Nokia"></a>
									<a href="http://www.cellularoutfitter.com/b-5-motorola-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/Motorola.jpg" border="0" alt="Motorola"></a>
									<a href="http://www.cellularoutfitter.com/b-4-lg-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/LG.jpg" border="0" alt="LG"></a>
									<a href="http://www.cellularoutfitter.com/b-9-samsung-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/Samsung.jpg" border="0" alt="Samsung"></a>
									<a href="http://www.cellularoutfitter.com/b-16-palm-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/PalmTreo.jpg" border="0" alt="Palm Treo"></a>
									<a href="http://www.cellularoutfitter.com/b-14-blackberry-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/Blackberry.jpg" border="0" alt="Blackberry"></a>
									<a href="http://www.cellularoutfitter.com/b-17-apple-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/Apple.jpg" border="0" alt="Apple"></a>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td width="100%" align="center" valign="middle">
									&nbsp;
									<a href="http://www.cellularoutfitter.com/b-20-htc-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/HTC.jpg" border="0" alt="HTC"></a>
									<a href="http://www.cellularoutfitter.com/b-6-nextel-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/Nextel.jpg" border="0" alt="Nextel"></a>
									<a href="http://www.cellularoutfitter.com/b-18-pantech-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/Pantech.jpg" border="0" alt="Pantech"></a>
									<a href="http://www.cellularoutfitter.com/b-10-sanyo-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/Sanyo.jpg" border="0" alt="Sanyo"></a>
									<a href="http://www.cellularoutfitter.com/b-19-t-mobile-sidekick-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/Sidekick.jpg" border="0" alt="T-Mobile Sidekick"></a>
									<a href="http://www.cellularoutfitter.com/b-11-siemens-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/Siemens.jpg" border="0" alt="Siemens"></a>
									<a href="http://www.cellularoutfitter.com/b-2-sony-ericsson-cell-phone-accessories.html"><img src="http://www.cellularoutfitter.com/images/email/brands/SonyEricsson.jpg" border="0" alt="Sony Ericsson"></a>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td width="100%" align="center" valign="middle" style="font-family: Arial, Helvetica, sans-serif; color: #999999; font-size: 11px;">
									Visit us online at <b><a href="http://www.cellularoutfitter.com">CellularOutfitter.com</a></b><br>
									If you have questions or concerns, please email <b><a href="mailto:info@cellularoutfitter.com">info@cellularoutfitter.com</a></b>.
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="100%" align="right" valign="bottom">
						<img src="http://www.cellularoutfitter.com/images/email/lightgray.jpg" width="658" height="15" border="0">
					</td>
				</tr>
			</table>
		</td>
		<td width="1" align="center" valign="top" bgcolor="#CCCCCC"><img src="http://www.cellularoutfitter.com/images/email/spacer.gif" width="1" height="1" border="0"></td>
	</tr>
</table>

</center>
</body>
</html>