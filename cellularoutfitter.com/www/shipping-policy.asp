<%
pageTitle = "Shipping &amp; Store Policy"
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top_index.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<table border="0" cellspacing="0" cellpadding="0" width="775">
    <tr>
        <td class="top-sublink-gray">
            <a class="top-sublink-gray" href="http://www.CellularOutfitter.com/">Cell Phone Accessories</a>&nbsp;>&nbsp;
            <span class="top-sublink-blue"><%=pageTitle%></span>								</td>
    </tr>
    <tr>
        <td class="top-sublink-gray"><img src="http://www.cellularoutfitter.com/images/spacer.gif" width="1" height="10"></td>
    </tr>
    <tr>
        <td class="static-content-font">
            <img src="http://www.cellularoutfitter.com/images/infopages/shipping-policy.gif" alt="Shipping & Store Policy" width="164" height="114" align="left">
            <p align="justify">
                <% if month(date) = 11 or month(date) = 12 then %>
                <strong>Holiday Shipping Policy</strong>
                <br /><br />
                Cellular  Outfitter  offers  multiple  shipping  options  to  ensure  timely  delivery  of  your  item  for  the  Holidays.  Due  to 
                the  nature  of  first  class  mail  delivery,  our  customers  who  choose  the  First  Class  Shipping  option  are  STRONGLY 
                ADVISED per the USPS to order  AT LEAST NINE (9) SHIPPING DAYS BEFORE DECEMBER 24TH, 2012 (or Tuesday, 
                December  11th,  2012)  to  ensure  timely  delivery  before  Christmas  day.  This  is  primarily  due  to  unpredictability  of 
                Holiday  mail  volume  and  delivery.  If  it  is  beyond  that  date,  and  pre-Christmas  day  delivery  is  critical,  it  is  strongly 
                suggested that customers opt for either our low-cost FLAT-RATE Priority shipping option (approx. 3 business days), 
                or our FLAT-RATE Express shipping option (1-2 business days).
                <ul>
                    <li>
                    	The last suggested pre-Christmas ordering date for FLAT-RATE Priority shipping (approx. 3 business days) is AT LEAST FOUR (4) 
                        SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Tuesday, December 17th, 2013).
                    </li>
                    <li>
                    	The last suggested pre-Christmas ordering date for FLAT-RATE Express shipping (1-2 business days) is AT LEAST THREE (3) 
                        SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Wednesday, December 18th, 2013).
                    </li>
                </ul>
                <strong>UPS Holiday Shipping Policy</strong>
                <br /><br />
                For an additional fee, Cellular Outfitter orders can be shipped via UPS. Orders shipped via UPS will be shipped within 
                1-2 business days and will be charged based on product weight and ship-to location. Tracking numbers & package 
                tracking will be available online at UPS.com. All UPS shipments are insured up to $100.
                <br /><br />
                Orders placed via UPS shipping need to submitted by the following dates to ensure delivery in time for Christmas. 
                <ul>
                    <li>The last suggested pre-Christmas ordering date for UPS Ground (approx. 6 business days) is AT LEAST SIX (6) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Friday, December 13th,2013).</li>
                    <li>The last suggested pre-Christmas ordering date for UPS 3 Day Select (approx. 3 business days) is AT LEAST FIVE (5) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Monday, December 16th,2013).</li>
                    <li>The last suggested pre-Christmas ordering date for UPS 2nd Day Air (approx. 2 business days) is AT LEAST THREE (3) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Wednesday, December 18th, 2013).</li>
                </ul>
                <br><br>
                <strong>CUSTOM DESIGN PRODUCTS:</strong>
                <br><br>
                Due to the nature of custom cases, our customers who choose this style of cases are advised to place their order on
				or before the cut off days listed below for each of the shipping methods we offer:
                <ul>
                	<li>
                    	The last suggested pre-Christmas ordering date is AT LEAST ELEVEN (11) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Friday, December 6th, 2013).
                    </li>
                </ul>
                <br><br>
                <strong>SHIPPING TO CANADA:</strong>
                <br><br>
                Packages shipped to Canada are shipped with the United States Postal Service. We offer two shipment methods for orders 
                being shipped to Canada. We order USPS First Class International and USPS International Priority Mail.
                <ul>
                	<li>
                    	The last suggested pre-Christmas ordering date for USPS First Class International Mail (approx. 10 business days) 
                        is AT LEAST TEN (10) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Monday, December 9th, 2013).
                    </li>
                    <li>
                    	The last suggested pre-Christmas ordering date for USPS International Priority Mail (approx. 5 business days) is 
                        AT LEAST SIX (6) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Friday, December 13th, 2013).
                    </li>
                </ul>
                <% end if %>
                <strong>SHIPPING &amp; HANDLING:</strong><br>
                CellularOutfitter.com offers shipping and handling via USPS First Class Mail (3-10 Business Day Delivery) starting at $5.99 for the first item and a $1.99 shipping and handling fee on each additional item. For an additional Shipping &amp; Handling fee, customers may upgrade to USPS Priority Mail (2-4 Business Day Delivery) or USPS Express Mail (1-2 Business Day Delivery). All orders shipped via USPS Express Mail will require signature upon delivery. Presently, CellularOutfitter does not ship outside of the US and Canada. Additional charges may apply for large-quantity distribution orders and orders that require special delivery instructions. If such an order is placed, the customer will be notified of said charges by a CellularOutfitter.com customer service specialist. This policy pertains to all orders placed within the continental United States of America.
                <br><br>
                <strong>Return/Warranty Policy:</strong><br>
                If you are not happy with the any product, you may return the item for an exchange or refund of the purchase price (minus shipping) within ninety (90) days of receipt. We also offer a ONE YEAR MANUFACTURER WARRANTY on all items. If your item is defective, for up to a full year, simply return the item to CellularOutfitter.com for a prompt exchange.
                <br><br>
                All returns must be accompanied by a Cellular Outfitter RMA number. To obtain an RMA number, please send an e-mail to: <a href="mailto:returns@CellularOutfitter.com">returns@CellularOutfitter.com</a>
                <br><br>
                The information below must be included for any return:
                <br>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="static-content-font">
                    <tr height="25"><td width="300" align="left">- Order Number</td><td align="left">- Date of original purchase</td></tr>
                    <tr height="25"><td width="300" align="left">- Reason for request for RMA</td><td align="left">- Your e-mail address</td></tr>
                </table>
                <p align="justify"><strong>No returns, refunds or exchanges will be processed without an RMA number.</strong>
                <br><br>
                <strong>CUSTOM & FEATURED ARTIST DESIGN RETURN POLICY:</strong><br />
				Since each item is crafted uniquely for you, customized products may not be returned or exchanged.
                <br /><br />
                <strong>CELL PHONE RETURN POLICY:</strong><br>
                All return requests must be made within 7 days of receiving the item. All returned merchandise must be unregistered, in complete original manufacturer's packaging, same condition as sold, with all literature, accessories, instructions, blank warranty cards and documentation. All Phones must be returned in "Like-New Condition", show no signs of use and must have less than <b>25 minutes</b> in total cumulative talk time. 
                <br><br>
                If an item is defective it may only be exchanged with an equivalent product if we are unable to supply the same item or if the item cannot be repaired. Any return shipping cost is the responsibility of the customer. All shipping and handling fees are non-refundable. This includes all refused and unaccepted packages. 
                <br><br>
                All NON-DEFECTIVE cell phone returns will be subject to a 20% restocking fee. 
                <br><br>
                For refunds allow 3-4 business days for the refund to post to your account from the time the refund is issued. 
                <br><br>
                <strong>Non-Defective Returns:</strong><br>
                NON-DEFECTIVE RETURNS OF ACCESSORIES ARE SUBJECT TO A 15% RE-STOCKING FEE. NON-DEFECTIVE RETURNS OF PHONES ARE SUBJECT TO A 20% RE-STOCKING FEE. Such returns will be for store credit or refund at the customer's request. Refunds are only available for order within 30 days of original order date for accessories and 7 days for phones. Refunds will be issued for the current published price of the product(s) minus a 15% or 20% re-stocking fee.
                <br><br>
                <strong>
                NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS: 
                </strong>
                <br />
                NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET  RETURNS MAY BE SUBJECT TO A 15% RE-STOCKING FEE and such returns will be for store credit or refund (at the customer's request). Refunds are only available within 90 days of original order date. Please contact us at <a href="mailto:returns@CellularOutfitter.com">returns@cellularoutfitter.com</a> if you have questions about which products are returnable and which products may be subject to a restocking fee.
                <br><br>
                Customers must contact the headset manufacturer for all defective/warranty issues on headsets that have had the packaging opened. For your convenience, you can contact us at <a href="mailto:returns@CellularOutfitter.com">returns@CellularOutfitter.com</a> for the manufacturers&rsquo; contact information.
                <br><br>
                <strong>ARRIVAL TIMES:</strong><br>
                <strong>This formula will help to determine when your order should arrive: Shipping Time + Shipping Method = Total Delivery Time.</strong>
                In-stock items are shipped out of our warehouse in Southern California within 1-2 business days unless otherwise specified.
                Orders shipped via USPS First Class Mail will arrive anywhere from 3-8 business days <strong>after the order has shipped from our warehouse(s)</strong> and depending on SHIP TO location within the continental United States.
                Estimated time of arrival for orders placed with USPS Priority Mail are anywhere from 2-4 business days after the order has shipped from our warehouse(s) and depending on SHIP TO location within the continental Unites States, or unless specified otherwise.
                Estimated time of arrival for orders placed with USPS Express Mail is approximately <strong>1-2 business days after the order has shipped from our warehouse(s)</strong> and depending on SHIP TO location within the continental Unites States, or unless specified otherwise.
                <strong>Please note</strong> our warehouse does not ship on weekends and we do not offer Saturday, Sunday or holiday delivery.
                <br><br>
                <strong>PLEASE BE ADVISED:</strong><br>
                Regardless of shipping option, CellularOutfitter.com <strong>does not warrant or guarantee published delivery times</strong>. CellularOutfitter.com does guarantee timely processing and fulfillment of all IN-STOCK items upon placement of order (usually within 1-2 business days). Upon fulfillment out of our warehouse(s), it is the obligation of the United States Postal Service for timely delivery of shipment in which the above suggested delivery times apply. It is the responsibility of the customer to acknowledge and adhere to the above policy before placing their order. The SHIP TO address MUST be a valid deliverable address for the United States Postal Service (must be able to receive first class mail). Please provide as complete information as possible to ensure delivery accuracy.
                <br><br>
                <strong>SHIPMENT CONFIRMATION/TRACKING:</strong><br>
                Upon shipment of your order you will receive notification that your order has been sent in accordance with your selected shipping option. We will also include delivery and tracking confirmation in this email which accesses the US Postal Services database. CellularOutfitter.com does not expressly warrant the reliability of any and all information provided by the USPS. Your credit card is charged when your shipment has been sent. Please be advised, if your order requires additional information for processing verification, you may receive one or more of the following: (i) an e-mail requesting additional information; (ii) a phone call from a CellularOutfitter.com processing representative; (iii) or a call from your bank to verify your purchase.
            </p>
        </td>
    </tr>
    <tr>
        <td class="static-content-font">&nbsp;</td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom_index.asp"-->