<%
mobileAccess = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	dim noLeftSide : noLeftSide = 1        
    dim designId: designId = Request.querystring("designId")
    dim aspId : aspId = Request.querystring("aspId")
	dim forceTool : forceTool = prepInt(Request.querystring("forceTool"))
    
    activeContest = 0
	if forceTool = 1 then
		bToolAvail = true
	else
		bToolAvail = false
	end if
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<link rel="stylesheet" type="text/css" href="/includes/css/customCase.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript" src="FBJSBridge.js"></script>
<script type="text/javascript">
    function popuponclick(s)
    {
        // alert("popup");
        my_window = window.open(s,"mywindow","status=1,width=800,height=600");
    }

    function closepopup()
    { 
        // alert("closeup");
        my_window.close ();
    }

    var attributes = {};

    function embedPlayer() {        
        var flashvars = {designId:"<%=prepInt(designId)%>",aspId:"<%=prepInt(aspId)%>"};
        var params = {menu: "false", wmode: "opaque"};	        
        swfobject.embedSWF("index.swf", "flashContent", "650", "870", "9.0", null, flashvars , params, {name:"flashContent"});
    }
    
    //Redirect for authorization for application loaded in an iFrame on Facebook.com 
    function redirect(id,perms,uri) { //alert(id+'=='+perms+'=='+uri);
        var params = window.location.toString().slice(window.location.toString().indexOf('?'));
        top.location = 'https://graph.facebook.com/oauth/authorize?client_id='+id+'&scope='+perms+'&redirect_uri='+uri+params;				 
    }    			
</script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,800' rel='stylesheet' type='text/css'>
<div class="customHeader">
	<div class="fl customHeaderIcon"><img src="/images/custom/headerIcon.gif" border="0" width="83" height="83" /></div>
    <div class="fl customHeaderText"><img src="/images/custom/headerText.gif" border="0" width="714" height="49" /></div>
    <div class="customHeaderRibbon"><img src="/images/custom/priceRibbon.png" border="0" width="153" height="141" /></div>
</div>
<div class="customHeaderLinkBar">
	<!--
	<div class="fr customHeaderLink">Live Chat</div>
    <div class="fr customHeaderLink">FAQ</div>
    <div class="fr customHeaderLink">For Best Quality</div>
    <div class="fr customHeaderLink">Video Tutorial</div>
    -->
</div>
<% if month(date) = 11 or month(date) = 12 then %>
<div style="padding:10px; border:1px solid #CCC; border-radius:5px; width:630px; margin:0px auto 10px auto; text-align:center;">
    The last suggested pre-Christmas ordering date is AT LEAST ELEVEN (11) SHIPPING DAYS BEFORE 
    DECEMBER 24TH, <%=year(date)%> (or Friday, December 6th, <%=year(date)%>). 
</div>
<% end if %>
<div id="ccw">
    <div id="first">
    	<div class="toolBox" id="flashContainer">
        	<% if not bToolAvail then %>
        	<div style="font-size:24px; color:#333; width:100%; text-align:center; padding-top:100px;">
            	Custom Design Tool is currently unavailable while we are performing upgrades.<br /><br />
                Please check back soon.
            </div>
            <% else %>
        	<div id="flashBorder">
				<div id="flashBorderInner">
					<script type="text/javascript">embedPlayer();</script>
                    <div id="fb-root"></div>
                    <div id="flashContent">
                        <style type="text/css" media="screen"> 
                            html, body  { height:100%; }
                            object:focus { outline:none; }
                            #flashContent { display:none; }
                        </style>
                        <link rel="stylesheet" type="text/css" href="bin-release/history/history.css" />
                        <script type="text/javascript" src="bin-release/history/history.js"></script>  
                        <script type="text/javascript" src="bin-release/swfobject.js"></script> 
                        <script type="text/javascript"> 
                            var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
                            document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
                                            + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
                        </script> 
                    </div>
                </div>
            </div>
            <% end if %>
        </div>
        <div class="flashLink" id="getFlash">
            <p>To view this page ensure that Adobe Flash Player version 10.0.0 or greater is installed.</p>
            <a href='http://www.adobe.com/go/getflashplayer'><img src='//www.adobe.com/images/shared/download_buttons/get_flash_player.gif' border="0" alt='Get Adobe Flash player' /></a>
        </div>
    </div>
    <div class="detailSection">
    	<div class="detailTitle"><img src="/images/custom/createCaseTitle.gif" border="0" width="389" height="23" /></div>
        <div class="detailSubTitle">AN INTRODUCTION</div>
        <div class="fl detailText">
        	<div class="detailMiniTitle">CREATE YOUR INSPIRATION</div>
            <div class="detailMiniBody">
            	Our custom configurator tool enables you to position, scale, and rotate your image, resulting in a one-of-a-kind 
                reflection of your personality. By using large, high-resolution images to begin with, you're able to achieve a 
                terrific amount detail in your final printed product. Our advanced ECO-UV printing process transfers the ink deep 
                into the case's surface to ensure long-lasting vibrant designs.
            </div>
            <div class="detailMiniTitle">SUGGESTED IMAGE SIZES</div>
            <div class="detailMiniBody">
            	To fill the full area, we recommend that you use images that are equal to or larger than the recommended size of 
                750 x 1425 @ 200ppi. You can design your images much smaller than these recommended sizes and place them anywhere 
                on the case. If your image is bigger than these sizes, it'll turn out even better since it will be printed at an 
                even higher resolution than the minimum.
            </div>
            <div class="detailMiniTitle">ACCEPTABLE FILE TYPES</div>
            <div class="detailMiniBody">
            	Cellular Outfitter supports images in both JPEG and PNG formats. Both formats will produce great quality products 
                as long as the resolution meets or exceeds our recommendations. With PNG, we support full transparency of your 
                images as you design. However, please make sure the quality of your image is high enough for printing on a product.
            </div>
        </div>
    	<div class="fr detailsImg"><img src="/images/custom/detailsImg.jpg" border="0" width="470" height="604" /></div>
    </div>
    <div class="bottomCustomText">
    	<strong>RETURN POLICY:</strong> Since each item is crafted uniquely for you, customized products may not be returned or exchanged.
        <br /><br />
		<strong>SHIPPING:</strong> Please allow up to 7 business days for production of custom cases prior to shipping time.
    </div>
    <% if activeContest = 1 then %>
    <div id="third">
		<!--#include virtual="/custom/includes/inc_contestRules.asp"-->
    </div>
    <% end if %>
</div>
<script type="text/javascript">
	function showCustomCaseInfo() {
		viewPortW = $(window).width();
		window.scrollTo(0, 0);
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = 'none';	
		ajax("/ajax/ajaxCustomCaseInfographic.asp?screenW="+viewPortW,'popBox');
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").onclick = function () { closeFloatingBox() }
		document.getElementById("popBox").onclick = function () { closeFloatingBox() }
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	function showVidTut() {
		viewPortH = $(window).height();
		window.scrollTo(0, 0);
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = 'none';	
		ajax("/ajax/ajaxCustomVideoTutorial.asp?screenH="+viewPortH,'popBox');
		//var e = document.getElementById('popBox');
		//e.innerHTML = '<iframe width="200" height="108" src="http://www.youtube.com/embed/GrcQ31aW4CI?rel=0&showinfo=0&wmode=opaque&modestbranding=1" frameborder="0" allowfullscreen></iframe>';
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").onclick = function () { closeFloatingBox() }
		document.getElementById("popBox").onclick = function () { closeFloatingBox() }
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	function closeFloatingBox() {
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = '';
		//------------------ change it to original -------------------//
		document.getElementById("popBox").style.position = "fixed";
		document.getElementById("popCover").onclick = function () {}
		document.getElementById("popBox").onclick = function () {}
		//------------------------------------------------------------//
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
		document.getElementById("popCover").innerHTML = "";
		document.getElementById("popBox").innerHTML = "";
	}	
</script>
<!--#include virtual="/includes/template/bottom_index.asp"-->
<!-- CO web3 custom -->