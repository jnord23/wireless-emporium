<%
mobileAccess = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	dim noLeftSide : noLeftSide = 1
	
	sql = "sp_customModels"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<link rel="stylesheet" type="text/css" href="/includes/css/customCaseLandingPage.css" />
<div class="customHeader">
	<div class="fl customHeaderText"><img src="/images/custom/landingPage/headerText.gif" border="0" width="474" height="119" /></div>
    <div class="customHeaderPhone"><img src="/images/custom/landingPage/headerPhone.jpg" border="0" width="308" height="458" /></div>
    <div class="customHeaderRibbon"><img src="/images/custom/priceRibbon1499-2.png" border="0" width="225" height="224" /></div>
</div>
<div class="headBulkText">
	<div class="bulkTextTitle">Make it Your Own</div>
    <div class="bulkTextBody">
    	Create a custom case that will stand out from a normal case by selecting your own image to print on which ever phone 
        model you have. You have absolute control of how you want your unique case to look like through the simple uploading 
        process where you can select any design on your choice. You can also add any text to your images and choose from a 
        variety of different background colors. Once you're completely satisfied with your custom creation, we'll transfer 
        your image directly onto your phone model case using our advanced UV printer. Create a case now and further protect 
        your phone at unbeatable prices and great quality.
    </div>
    <div class="createButton"><a href="/custom/"><img src="/images/custom/landingPage/createButton.gif" border="0" width="401" height="67" /></a></div>
</div>
<div class="detailSection">
    <div class="detailTitle">
    	<div class="fl titleArrow"><img src="/images/custom/landingPage/detailArrow.png" border="0" width="24" height="24" /></div>
        <div class="fl titleText">How It Works</div>
    </div>
    <div class="stepImages">
    	<div class="fl processStep1"><img src="/images/custom/landingPage/detail1_step1.jpg" border="0" width="223" height="276" /></div>
        <div class="fl processOp">+</div>
        <div class="fl processStep2"><img src="/images/custom/landingPage/detail1_step2.jpg" border="0" width="302" height="224" /></div>
        <div class="fl processOp">=</div>
        <div class="fl processStep3"><img src="/images/custom/landingPage/detail1_step3.jpg" border="0" width="132" height="265" /></div>
    </div>
    <div class="stepTextAndNum">
        <div class="fl stepNum1">1</div>
        <div class="fl stepText">Pick A Case</div>
        <div class="fl stepNum2">2</div>
        <div class="fl stepText">Upload Your Image</div>
        <div class="fl stepNum3">3</div>
        <div class="fl stepText">We Print it in High Quality</div>
    </div>
</div>
<div class="extendedDetailSection">
	<div class="detailTitle">
    	<div class="fl titleArrow"><img src="/images/custom/landingPage/detailArrow.png" border="0" width="24" height="24" /></div>
        <div class="fl titleText">Why Choose Us?</div>
    </div>
    <div class="fl whySection">
    	<div class="fl whyIcon"><img src="/images/custom/landingPage/whyIcon1.gif" border="0" width="100" height="97" /></div>
        <div class="fl whyText">
        	<div class="whyTitle">Complete Personalization</div>
            <div class="whyBody">
            	By creating your own custom case, you will have a cell phone case that is truly unique and shows off your 
                individuality. With the freedom to select your own images, you can make your case special and a distinctive 
                investment piece for your cell phone.
            </div>
        </div>
    </div>
    <div class="fl whySection">
    	<div class="fl whyIcon"><img src="/images/custom/landingPage/whyIcon2.gif" border="0" width="100" height="97" /></div>
        <div class="fl whyText">
        	<div class="whyTitle">Brand Your Company or Group</div>
            <div class="whyBody">
            	Market your company brand with our exclusive corporate cases. Create a custom case that reflects your brand 
                identity! Cellular Outfitter offers generous discounts on bulk orders.
            </div>
        </div>
    </div>
    <div class="fl whySection">
    	<div class="fl whyIcon"><img src="/images/custom/landingPage/whyIcon3.gif" border="0" width="100" height="97" /></div>
        <div class="fl whyText">
        	<div class="whyTitle">High-Quality Printing</div>
            <div class="whyBody">
            	In order to ensure long-lasting designs, we have the most advanced and quality driven printer. The ECO-UV 
                printer allows for nothing but high-quality images on your custom case through the process of transferring 
                the ink deep into the case's surface that are both durable and highly detailed.
            </div>
        </div>
    </div>
    <div class="fl whySection">
    	<div class="fl whyIcon"><img src="/images/custom/landingPage/whyIcon4.gif" border="0" width="100" height="97" /></div>
        <div class="fl whyText">
        	<div class="whyTitle">We Customize for Multiple Models</div>
            <div class="whyBody">
            	We not only have cases to customize for the top selling phone models like the iPhone 5 or the Samsung Galaxy 
                S3, but we also have the largest selection of custom cases for a huge variety of phone brands and models.
            </div>
        </div>
    </div>
</div>
<div class="priceRow">
	<div class="fl grayPR1">
    	<div class="prLine1">Brand Name</div>
        <div class="prLine2">Custom Cases</div>
        <div class="prLine3">$34.99</div>
    </div>
    <div class="fl grayPR">
    	<div class="prLine1">Retail Store</div>
        <div class="prLine2">Custom Cases</div>
        <div class="prLine3">$29.99</div>
    </div>
    <div class="fl grayPR">
    	<div class="prLine1">Competitors</div>
        <div class="prLine2">Custom Cases</div>
        <div class="prLine3">$19.99</div>
    </div>
    <div class="fl bluePR">
    	<div class="prLine1">Lowest Price Online</div>
        <div class="coPrice"><img src="/images/custom/coPrice.gif" border="0" /></div>
    </div>
</div>
<div class="fl sampleCases"><img src="/images/custom/landingPage/caseSamples.jpg" border="0" width="520" height="322" /></div>
<div class="fl customerText"><img src="/images/custom/landingPage/customerText.gif" border="0" width="385" height="236" /></div>
<div class="extendedDetailSection">
	<div class="detailTitle">
    	<div class="fl titleArrow"><img src="/images/custom/landingPage/detailArrow.png" border="0" width="24" height="24" /></div>
        <div class="fl titleText">
        	More Custom Models than Anybody!
            <div class="subtitleText">We're Adding More Cell Phone Models And Tablets Everyday</div>
        </div>
    </div>
    <div class="allBrands">
    	<div style="float:left; width:100%;">
		<%
		curBrand = ""
		cntBrand = 0
		do while not rs.EOF
			curBrand = rs("brandName")
			if (cntBrand mod 3) = 0 then response.write "</div><div style=""float:left; width:100%;"">"
		%>
            <div class="fl modelChecklist">
                <div class="brandNameCheck">
                    <div class="fl modelCheckmark"><img src="/images/custom/landingPage/modelCheck.gif" border="0" width="40" height="36" /></div>
                    <div class="fl brandName"><%=curBrand%></div>
                </div>
                <div class="modelList">
                    <ul>
                        <% do while curBrand = rs("brandName") %>
                        <li><%=rs("modelName")%></li>
                        <%
                            rs.movenext
                            if rs.EOF then exit do
                        loop
                        %>
                    </ul>
                </div>
            </div>
        <%
			cntBrand = cntBrand + 1
		loop
		%>
        </div>
    </div>
</div>
<div class="breakBar" style="padding-top:20px;"><img src="/images/custom/landingPage/breakBar.gif" border="0" width="830" height="16" /></div>
<div class="extendedDetailSection">
	<div class="detailTitle">
    	<div class="fl titleArrow"><img src="/images/custom/landingPage/detailArrow.png" border="0" width="24" height="24" /></div>
        <div class="fl titleText">
        	Get Inspired
        </div>
    </div>
    <div class="allPhoneLinks">
        <div class="fl phoneLink">
            <div class="phoneImg"><img src="/images/custom/landingPage/s3.jpg" border="0" width="120" height="248" /></div>
            <div class="phoneName">SAMSUNG GALAXY S3</div>
            <div class="createButton2"><a href="/custom/"><img src="/images/custom/landingPage/createButton2.gif" border="0" width="191" height="32" /></a></div>
        </div>
        <div class="fl phoneLink">
            <div class="phoneImg"><img src="/images/custom/landingPage/iphone5.jpg" border="0" width="120" height="248" /></div>
            <div class="phoneName">APPLE IPHONE 5</div>
            <div class="createButton2"><a href="/custom/"><img src="/images/custom/landingPage/createButton2.gif" border="0" width="191" height="32" /></a></div>
        </div>
        <div class="fl phoneLink">
            <div class="phoneImg"><img src="/images/custom/landingPage/droidDna.jpg" border="0" width="120" height="248" /></div>
            <div class="phoneName">HTC DROID DNA</div>
            <div class="createButton2"><a href="/custom/"><img src="/images/custom/landingPage/createButton2.gif" border="0" width="191" height="32" /></a></div>
        </div>
        <div class="fl phoneLink">
            <div class="phoneImg"><img src="/images/custom/landingPage/epicTouch.jpg" border="0" width="126" height="247" /></div>
            <div class="phoneName">SAMSUNG GALAXY S II EPIC TOUCH</div>
            <div class="createButton2"><a href="/custom/"><img src="/images/custom/landingPage/createButton2.gif" border="0" width="191" height="32" /></a></div>
        </div>
        <div class="fl phoneLink">
            <div class="phoneImg"><img src="/images/custom/landingPage/razrMaxx.jpg" border="0" width="129" height="251" /></div>
            <div class="phoneName">MOTOROLA DROID RAZR MAXX</div>
            <div class="createButton2"><a href="/custom/"><img src="/images/custom/landingPage/createButton2.gif" border="0" width="191" height="32" /></a></div>
        </div>
        <div class="fl phoneLink">
            <div class="phoneImg"><img src="/images/custom/landingPage/lumia.jpg" border="0" width="129" height="251" /></div>
            <div class="phoneName">NOKIA LUMIA 920</div>
            <div class="createButton2"><a href="/custom/"><img src="/images/custom/landingPage/createButton2.gif" border="0" width="191" height="32" /></a></div>
        </div>
        <div class="fl phoneLink">
            <div class="phoneImg"><img src="/images/custom/landingPage/ipadMini.jpg" border="0" width="196" height="262" /></div>
            <div class="phoneName">APPLE IPAD MINI</div>
            <div class="createButton2"><a href="/custom/"><img src="/images/custom/landingPage/createButton2.gif" border="0" width="191" height="32" /></a></div>
        </div>
        <div class="fl phoneLink">
            <div class="phoneImg"><img src="/images/custom/landingPage/note2.jpg" border="0" width="123" height="247" /></div>
            <div class="phoneName">SAMSUNG GALAXY NOTE 2</div>
            <div class="createButton2"><a href="/custom/"><img src="/images/custom/landingPage/createButton2.gif" border="0" width="191" height="32" /></a></div>
        </div>
    </div>
</div>
<!--#include virtual="/includes/template/bottom_index.asp"-->