<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFontCategory.php","add_record");

/*---Basic for Each Page Ends----*/

$catObj = new FontCategory();
$cid  = $_GET['cid']?$_GET['cid']:0;
$catObj->checkCategoryExist($cid);
if(isset($_POST['submit'])) {
	//echo "<pre>"; print_r($_POST); print_r($_FILES); echo "</pre>";exit;
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $catObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $catObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $catObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('categoryName_'.$value,$_POST['categoryName_'.$value], 'req', 'Please Enter Category Name.');			
		}
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;		
		foreach($langIdArr as $key=>$value) {
			$arr_error['categoryName_'.$value]=$obj->fnGetErr($arr_error['categoryName_'.$value]);	
		}	
		
		//Check Category Exists================================================
		foreach($langIdArr as $key=>$value) {
			if($catObj->isCategoryNameExist($_POST['categoryName_'.$value],$value)){ 
				$arr_error['categoryName_'.$value] = "Category already exist. ";
				$str_validate=0;				
			}
		}	
		
		//check Image Extension==============================================
		if($_FILES['categoryImage']['name'] != "") {
			$filename = stripslashes($_FILES['categoryImage']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);	 	 
		
			if(!$catObj->checkExtensions($extension)){ 
				$arr_error[categoryImage] = "Upload Only ".$catObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") ." image extension.";
				$str_validate=0;	
			}
		}		
		//Submit if validation pass : $str_validate is 1=======================================		
		if($str_validate){
			//echo "<pre>"; print_r($_POST); print_r($_FILES); echo "</pre>";exit;
			$_POST = postwithoutspace($_POST);
			$catObj->addRecord($_POST,$_FILES);
		}
	}
}
$sequence = $catObj->findMaxSequence();

$addHadding=($cid)?"Sub":"";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->
<script type="text/javascript">
function hrefBack1(){
	window.location ='manageFontCategory.php?cid=<?=$_GET[cid] ?>';
}
</script>
</head>
<body>
	<? include('includes/header.php'); ?>
	<div id="nav-under-bg"><!-- --></div>
	<form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		<input type="hidden" name="cid" value="<?=$cid?>">
		<div class="main-body-div-new">
			<div class="main-body-div-header">Add Font <?=$addHadding ?>Category</div>
			<!-- left position -->
			<div class="main-body-div4" id="mainDiv">
				<div align="center"><? echo $_SESSION['SESS_MSG']; ?></div>
				<div class="add-main-body-left-new" >
					<ul>
						<li class="add-main-body-left-new-text" style="clear:both; width:500px;" ><span class="small_error_message">
						<? //$_SESSION['SESS_MSG']?>
						</span></li>				  
						<li class="lable"><?=$addHadding ?>Category Name <span class="spancolor">*</span></li>
						<?	$genObj = new GeneralFunctions();
						echo $genObj->getLanguageTextBox('categoryName','m__Category_Name',$arr_error); //1->type,2->name,3->id
						?>
						
						<li class="lable" >Category Image <span class="spancolor">*</span></li>
						<li >
						<input type="file" name="categoryImage" class="wel" value="" />
						<p  style="padding-left:150px;"><? $arr_error[categoryImage] ?></p>
						</li>
						
					</ul>
				</div>
				<div class="main-body-sub">
					<input type="hidden" name="maxsequence" value="<?=$sequence ?>" />
					<input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
					&nbsp;
					<input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
				</div>
			</div>
		</div>
	</form>	
	<? unset($_SESSION['SESS_MSG']); ?>
</body>
</html>