<?
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFont.php","add_record");

/*---Basic for Each Page Ends----*/
$fontObj = new Font();

if(isset($_POST['submit'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
       // echo "<pre>"; print_r($_FILES); echo "</pre>";exit;
	//$errorArr = 0;
						
	/*if($_FILES['fontImage']['name'] != "") {
		$filename = stripslashes($_FILES['fontImage']['name']);
		$extension = findexts($filename);
		$extension = strtolower($extension);	 	 
		
		if($extension != "ttf")
		{ 
			$arr_error[fontImage] = "Upload Only ttf image extension.";
			if($arr_error[fontImage]) 
				$errorArr = 1;	
		}
	}*/
	$obj->fnAdd('fontswf',$_FILES['fontswf']['name'], 'req', 'Please Upload font swf file.');
        $obj->fnAdd('fontttf',$_FILES['fontImage1']['name'], 'req', 'Please Upload font ttf file.');
        $obj->fnAdd('font_ai_name',$_POST['font_ai_name'], 'req', 'Please Enter Font AI Name.');
	
	$arr_error = $obj->fnValidate();
        $str_validate = (count($arr_error)) ? 0 : 1;
        
        $arr_error['fontswf']=$obj->fnGetErr($arr_error['fontswf']);
        $arr_error['fontttf']=$obj->fnGetErr($arr_error['fontttf']);
        $arr_error['font_ai_name']=$obj->fnGetErr($arr_error['font_ai_name']);

	for($i=1,$j=0;$i<6;$i++,$j++){
		$con = "special".$i;
	 	if($_POST[$con])
	 		$special[$j] = $_POST[$con];
	}
	/*$arraycount = count(array_unique($special));
	if($arraycount != count($special)){
		$arr_error['fontttf'] = "Please Upload One file corresponding to one font style.";
		$str_validate=0;
	}*/
	
	if($str_validate){	
		$_POST = postwithoutspace($_POST);	
		$fontObj->addRecord($_POST,$_FILES);
	}	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript">
function addMoreRecord() {
	var ni = document.getElementById('ttfFileUpload');
	var numi = document.getElementById('theValue');
	var num = (document.getElementById('theValue').value -1)+ 2;
	document.getElementById('theValueNos').value += ","+num;	
	numi.value = num;
	var newdiv = document.createElement('div');
	var divIdName = 'my'+num+'Div';
	newdiv.setAttribute('id',divIdName);
	
	if(document.getElementById("theValue").value == 5){
		document.getElementById("addm").style.display = 'none';
	}
	
	var text = '<li class="lable" >Font (TTF File) '+num+'<span class="spancolor">*</span></li><li ><input type="file" name="fontImage'+num+'" value="" />';
	var text2='<select name="special'+num+'" id="special'+num+'" ><option value="normal" >Normal</option><option value="bold" >Bold</option><option value="italic" >Italic</option><option value="underLine" >UnderLine</option><option value="boldI" >Bold Italic</option></select>'
	;
	newdiv.innerHTML = text+'&nbsp;'+text2+'<a href=\'javascript:void(0)\' onclick=\'removeElement(\"'+divIdName+'\",\"'+num+'\")\'><img src="images/drop.png" border="0"></a> </li>';
	ni.appendChild(newdiv);
}

function removeElement(divNum,num) {
	
	var newArray = new Array();
	var newArray2 = new Array();	
	var allNos = document.getElementById('theValueNos').value;
	newArray = allNos.split(',');

	var len = newArray.length;
	var j = 0;
	for(i=0;i<len;i++) {
		//alert(newArray[i]);
		if(newArray[i] != num) {
			newArray2[j] =  newArray[i];
			j++;
		}
	}
	if(document.getElementById("theValue").value <= 5){
		document.getElementById("addm").style.display = '';
	}
	var allNos2 = newArray2.join(',');	
	document.getElementById('theValueNos').value = allNos2;
	
	var numi = document.getElementById('theValue');
	var num2 = (document.getElementById('theValue').value -1);
	numi.value = num2;
	
	var d = document.getElementById('ttfFileUpload');
	var olddiv = document.getElementById(divNum);
	d.removeChild(olddiv);
}

function Check_value(){

if(document.getElementById("name").checked==false && document.getElementById("number").checked==false && document.getElementById("other").checked==false ){
document.getElementById("typeError").innerHTML='Select at least one type !!!';
return false;
}

var val1= '';
var val2= '';
var val3= '';
var val4= '';
var val5= '';
var val6 = document.frmUser.theValue.value;
var arr = new Array();
for(i=1;i<=val6;i++){
		arr.push(document.getElementById("special"+i).value);
}
var arrlenght = arr.length;
//alert(arrlenght);
var i,
        len=arr.length,
        out=[],
        obj={};

        for (i=0;i<len;i++) {
                if (obj[arr[i]] != null) {
                        if (!obj[arr[i]]) {
                                out.push(arr[i]);
                                obj[arr[i]] = 1;
                        }
                } else {
                        obj[arr[i]] = 0;                        
                }
        }
		if(out.length == 0){
			return true;
		}else{
			document.getElementById("errormessage").innerHTML='Upload one TTF files corresponding to one style';
			return false;
		}
}
function hrefBack1(){
	window.location='manageFont.php';
}
</script>
<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->
</head>
<body>
	<? include('includes/header.php'); ?>
	<div id="nav-under-bg"><!-- --></div>
	<form name="frmUser" id="frmUser" method="post"  enctype="multipart/form-data" onsubmit="return Check_value();">
		<input type="hidden" name="cid" value="<?=$cid?>">
		<div class="main-body-div-new">
			<div class="main-body-div-header">Add Fonts</div>
			<!-- left position -->
			<div class="main-body-div4" id="mainDiv">
				<div class="add-main-body-left-new" >
					<ul>
						<li class="add-main-body-left-new-text" style="clear:both; width:500px;padding-bottom:5px;" >
						<span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span>
						</li>
						<!--Type==================================
						<li class="lable">Type <span class="spancolor">:</span></li>
						<li>
						Name<input type="checkbox" name="name" id="name" />
						Numbers<input type="checkbox" name="number" id="number" />
						Others<input type="checkbox" name="other" id="other" checked/>
						<p  style="padding-left:150px;" id="typeError"></p>
						</li>	  
						===========================================-->
						
						<!-- Font Category ============== -->
						<li class="lable">Font Category <span class="spancolor">*</span></li>
						<li class="lable2">
                                                <select name="cid" id="cid">
						<?=$fontObj->getCategoryList('')?>
                                                </select>
						<p  style="padding-left:150px;"></p>
						</li>
                                                
                                                
						<!-- Font AI NAME ============== -->
						<li class="lable">Font AI Name <span class="spancolor">*</span></li>
						<li class="lable2">
                                                <input type="text" name="font_ai_name" id="font_id_name" />
						<p  style="padding-left:150px;"><?= $arr_error['font_ai_name']?></p>
						</li>
						
						
						<!-- Font SWF==================== -->
						<? if($fontObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'SWF_TTF'") == 2){?>
						<li class="lable">Font Swf <span class="spancolor">*</span></li>
						<li class="lable2">
						<input type="file" name="fontswf" id="m__fontswf" />
						<p  style="padding-left:150px;"><?=$arr_error['fontswf'] ?></p>
						</li>
						<? }?>
						<!-- Font TTF===================== -->
						<div id="ttfFileUpload"> 
						<li class="lable" >Font (TTF File) 1<span class="spancolor">*</span></li>
						<li class="lable2">
						<input type="file" name="fontImage1"  value="" id="m__font__Image" />
						
						<select name="special1" id="special1" >
						<option value="normal" >Normal</option>
<!--						<option value="bold" >Bold</option>
						<option value="italic" >Italic</option>
						<option value="underLine" >UnderLine</option>
						<option value="boldI" >Bold Italic</option>-->
						</select>
							
<!--						<p  style="padding-left:150px;"><?=$arr_error['fontttf'] ?></p>-->
						<p  style="padding-left:150px;" id="errormessage"><?=$arr_error['fontttf']?></p>				
						</li>								
						</div> 
						<!-- Hidden Values============== -->
						<input type="hidden" name="theValue" id="theValue" value="1">
						<input type="hidden" name="theValueNos" id="theValueNos" value="1">
						
						<!-- Add More====================== -->
						<span id="addm" style="display:none;">
						<a href="javascript:void(0);" onclick="addMoreRecord();" style="padding-left:300px;" >
						Add More
						</a>
						</span>
					</ul>
				</div>
				
				<div class="main-body-sub">
					<input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
					&nbsp;
					<input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
				</div>
			</div>
		</div>		
	</form>	
	<? unset($_SESSION['SESS_MSG']); ?>
</body>
</html>