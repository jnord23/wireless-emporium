<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageView.php","");
/*---Basic for Each Page Ends----*/

$viewObj = new View();
$result = $viewObj->getResult(base64_decode($_GET['id']));
//print_r($result)
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

</head>
<body>
          <div class="main-body-div-header">View</div>
		  <!-- left position -->
        
            <div class="main-body-div4" id="mainDiv">
              <div class="add-main-body-left-new" style="position:relative;">
                <ul>
                  <li class="add-main-body-left-new-text" style="clear:both; width:500px;" ><span class="small_error_message">
                    <?=$_SESSION['SESS_MSG']?>
                  </span></li>		
                  <li class="lable">Name: </li>
                  
                    <?	$genObj = new GeneralFunctions();
						echo $genObj->getLanguageViewTextBox('viewName',TBL_VIEWDESC,base64_decode($_GET['id']),"viewId"); //1->type,2->name,3->id,4->tablename,5->tableid
					?>
				  <li class="add-main-body-left-new-text" style="clear:both;">&nbsp;</li>
				  <li class="lable">Price: </li>
                  <li><? echo $genObj->displayPrice($result->price); ?></li>


                </ul>
              </div>
              <div class="main-body-sub">&nbsp;</div>
            </div>
</body>
</html>