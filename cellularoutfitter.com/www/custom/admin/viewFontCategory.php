<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFontCategory.php","");
/*---Basic for Each Page Ends----*/

$catObj = new FontCategory();
$cid  = $_GET['id']?$_GET['id']:0;
$result = $catObj->getResult(base64_decode($_GET['id']));
$addHadding=($cid)?"Sub":"";
$imgPath=__FONTCATTHUMBPATH__.$result->categoryImage;
$imgAbsPath=__FONTCATTHUMB__.$result->categoryImage;
$img=_image($imgAbsPath,$imgPath,__NOIMAGEPATH__);
$isDefult=($result->isDefault)?"Yes":"No";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

</head>
<body>
	<div class="main-body-div-header">View Detail</div>
	<!-- left position -->
	<div class="main-body-div4" id="mainDiv">
		<div class="add-main-body-left-new" style="position:relative;">
			<ul>
				<li class="lable"><?=$addHadding ?>Category Name: </li>
				<?	
				$genObj = new GeneralFunctions();
				echo $genObj->getLanguageViewTextBox('categoryName',TBL_FONTCATEGORY_DESCRIPTION,base64_decode($_GET['id']),"catId"); //1->type,2->name,3->id,4->tablename,5->tableid
				?>
				
				<li class="add-main-body-left-new-text" style="clear:both;">&nbsp;</li>
				<li class="add-main-body-right-new-text"></li>	
				<li class="lable" >Category Image: </li>
				<li><img src="<?=$img?>"></li>
				
				<li  class="lable">Default</li>
				<li class="sap"><?=$isDefult?></li>
			</ul>
		</div>	
	</div>
</body>
</html>