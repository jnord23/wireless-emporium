<?
/*---Basic for Each Page Starts----*/	
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCurrency.php","edit_record");
/*---Basic for Each Page Ends----*/

$currObj = new Currency();
require_once('validation_class.php');
$obj = new validationclass();

$cid  = base64_decode($_GET['id'])?base64_decode($_GET['id']):0;
$result = displayWithStripslashes($currObj->getResult(base64_decode($_GET['id'])));
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/validation.js"></script>
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>

<script type="text/javascript" src="datepicker/jquery-1.3.2.js"></script>
<script type="text/javascript" src="datepicker/ui.core.js"></script>
<script type="text/javascript" src="datepicker/ui.datepicker.js"></script>
<link type="text/css" href="datepicker/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="datepicker/demos.css" rel="stylesheet" />
<link type="text/css" href="datepicker/themes/base/ui.all.css" rel="stylesheet"/>

</head>
<body>

          <div class="main-body-div-header">View Detail</div>
		  <!-- left position -->
            <div class="main-body-div4" id="mainDiv">
              <div class="add-main-body-left-new">
                <ul>
                  <li class="add-main-body-left-new-text" style="clear:both; width:500px;" ><span class="small_error_message">
                  <?=$_SESSION['SESS_MSG']?>
                  </span></li>
                  <li class="lable">Currency Name</li>
                  <li class="sap">
				  	<?=$currObj->fetchValue(TBL_CURRENCY_DETAIL,"currencyName","1 and id = '".$result[currencyDetailId]."'")?></li>
                  <li  class="lable">Currency Value </li>
                  <li class="sap"><?=stripslashes($result[currencyValue])?>                 </li>
				  <li  class="lable">Symbol Show In</li>
                  <li class="sap"><? if($result[showIn] == 1) { echo "Left"; } else { echo "Right"; } ?>
                  </li>
				  <li  class="lable">Decimal Places </li>
                  <li class="sap"><? echo ($result[decimalPlace] != "")?$result[decimalPlace]:"0" ?>                 </li>		
				  <li  class="lable">Default: </li>
                  <li class="sap"><? if($result[isDefault] == 1) { echo "Yes"; } else { echo "No"; } ?>
                  </li>	
                </ul>
              </div>
              <div class="main-body-sub">
              </div>
            </div>
</body>
</html>