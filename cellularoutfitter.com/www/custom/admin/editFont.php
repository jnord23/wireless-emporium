<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFont.php","edit_record");
/*---Basic for Each Page Ends----*/

$fontObj = new Font();
if(isset($_POST['submit'])) {    
        require_once('validation_class.php');
	$obj = new validationclass();
        
        
        $obj->fnAdd('font_ai_name',$_POST['font_ai_name'], 'req', 'Please Enter Font AI Name.');
        $arr_error = $obj->fnValidate();
        $str_validate = (count($arr_error)) ? 0 : 1;
        $arr_error['font_ai_name']=$obj->fnGetErr($arr_error['font_ai_name']);
        
        
	$fontType = explode(",",$_POST[fontType]);
	for($i=count($fontType),$j=count($fontType)-1;$i<5 ; $i++,$j++){
	  $special = "special".$i;
	  $fontImage = "fontImage".$i;
	  if($_FILES[$fontImage][name] != '')
	  $fontType[$j]=$_POST[$special];
	}
	
	$num = count($fontType);
	$num2 = count(array_unique($fontType));
	if($num != $num2){
		$errorArr = 1;
		$errorImage = "Please Upload different font style to different TTF file.";
	}
	if($errorArr == 0 && $str_validate){
		$_POST = postwithoutspace($_POST);			
		$fontObj->editRecord($_POST,$_FILES);
	}
		
}

$result = $fontObj->getResult(base64_decode($_GET['id']));
$num = $fontObj->getCount(base64_decode($_GET['id']),'');
$resultvalue = ($num)?$fontObj->getCount(base64_decode($_GET['id']),1):"";
//Unused =================================
$name=($result->name)?"checked":"";
$number=($result->number)?"checked":"";
$other=($result->other)?"checked":"";
$checked=($result->isDefault)?"checked='checked'":"";
$disabled=($result->isDefault)?" disabled='disabled'":"";
//echo "<pre>"; print_r($result); echo "</pre>";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->


<script language="javascript">
function addMoreRecord() {
	var ni = document.getElementById('ttfFileUpload');
	var numi = document.getElementById('theValue');
	var num = (document.getElementById('theValue').value -1)+ 2;
	document.getElementById('theValueNos').value += ","+num;	
	numi.value = num;
	var newdiv = document.createElement('div');
	var divIdName = 'my'+num+'Div';
	newdiv.setAttribute('id',divIdName);
	
	if(document.getElementById("theValue").value == 5){
		document.getElementById("addm").style.display = 'none';
	}
	
	var text = '<li class="lable" >Font (TTF File) '+num+'<span class="spancolor">*</span></li><li ><input type="file" name="fontImage'+num+'" value="" />';
	var text2='<select name="special'+num+'" ><option value="normal" >Normal</option><option value="bold" >Bold</option><option value="italic" >Italic</option><option value="underLine" >UnderLine</option><option value="boldI" >Bold Italic</option></select>'
	;
	newdiv.innerHTML = text+'&nbsp;'+text2+'<a href=\'javascript:void(0)\' onclick=\'removeElement(\"'+divIdName+'\",\"'+num+'\")\'><img src="images/drop.png" border="0"></a> </li>';
	ni.appendChild(newdiv);
}

function removeElement(divNum,num) {
	
	var newArray = new Array();
	var newArray2 = new Array();	
	var allNos = document.getElementById('theValueNos').value;
	newArray = allNos.split(',');

	var len = newArray.length;
	var j = 0;
	for(i=0;i<len;i++) {
		//alert(newArray[i]);
		if(newArray[i] != num) {
			newArray2[j] =  newArray[i];
			j++;
		}
	}
	if(document.getElementById("theValue").value <= 5){
		document.getElementById("addm").style.display = '';
	}
	var allNos2 = newArray2.join(',');	
	document.getElementById('theValueNos').value = allNos2;
	
	var numi = document.getElementById('theValue');
	var num2 = (document.getElementById('theValue').value -1);
	numi.value = num2;
	
	var d = document.getElementById('ttfFileUpload');
	var olddiv = document.getElementById(divNum);
	d.removeChild(olddiv);
}

</script>

</head>
<body>
	<? include('includes/header.php'); ?>
	<div id="nav-under-bg"><!-- --></div>
	<form name="frmUser" id="frmUser" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">		
		<input type="hidden" name="page" value="<?=$_GET['page']?>">
		<input type="hidden" name="deleteswfile" value="<?=$result->fontSWF ?>">	
		<div class="main-body-div-new">
			<div class="main-body-div-header">Edit Font</div>
			<!-- left position -->
			<div class="main-body-div4" id="mainDiv">
				<div class="add-main-body-left-new" >
					<ul>
						<li class="add-main-body-left-new-text" style="clear:both; width:500px;" >
						<span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span>
						</li>
						<!-- Type=========================================
						<li class="lable">Type <span class="spancolor">:</span></li>
						<li>
						Name<input type="checkbox" name="name" id="name"  <?=$name ?> />
						Numbers<input type="checkbox" name="number" id="number"  <?=$number ?> />
						Others<input type="checkbox" name="other" id="other"  <?=$other ?>/>
						<p  style="padding-left:150px;"></p>
						</li>	
						===== =======================================--> 
						<!-- Font Category=============================== -->
						<li class="lable">Font Category <span class="spancolor">*</span></li>
						<li>
                                                <select name="cid" id="cid">
						<?=$fontObj->getCategoryList($result->categoryId)?>
                                                </select>
						<p  style="padding-left:150px;"></p>
						</li>
                                                
                                                <!-- Font AI NAME ============== -->
                                                <?php
                                              
                                                $fontAIName=stripslashes($fontObj->fetchValue(TBL_FONTDESC,'font_ai_name',"fontId='".base64_decode($_GET['id'])."'"));
                                                
                                                ?>
						<li class="lable">Font AI Name <span class="spancolor">*</span></li>
						<li class="lable2">
                                                <input type="text" name="font_ai_name" id="font_id_name"  value="<?=$fontAIName?>"/>
						<p  style="padding-left:150px;"><?= $arr_error['font_ai_name']?></p>
						</li>
						
						<!-- Font SWF======================================== -->
						<? if($fontObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'SWF_TTF'") == 2){?>
						<li class="lable">Font Swf <span class="spancolor">*</span></li>
						<li>
						<input type="file" name="fontswf" />
						<p  style="padding-left:150px;"></p>
						</li>
						<? }?>
						
						<!-- Start : Font TTF Section============================= -->
						<? 
						while($line = $fontObj->getResultObject($resultvalue)){
						$imageid .=$line->id.","; 
						$fontType .= $line->fontType.",";
						?>
						<!-- Hidden Values ===========-->
						<input type="hidden" name="fontName<?=$line->id?>" value="<?=$line->fontName?>">
						<input type="hidden" name="fontType"  value="<?=$fontType ?>" />
						<input type="hidden" name="deletepng<?=$line->id?>"  value="<?=$line->imageName ?>" />
						<input type="hidden" name="deletettf<?=$line->id?>"  value="<?=$line->fontTTF ?>" />
						
						<!--Font Style =================-->
						<li class="lable">Font Style <span class="spancolor"></span></li>
						<li style="padding-top:5px;padding-bottom:6px;"><?=$line->fontType?></li>
						<!-- Font Name ================-->
						<li class="lable">Font Name <span class="spancolor">*</span></li>
						<li><?=$line->fontName?>						
						<p  style="padding-left:150px;"><?=$arr_error[fontName]?></p>
						</li>
						<!-- Font Image ============-->
						<li class="lable" >&nbsp;<span class="spancolor"></span></li>
						<li >
						<img src="<?=__FONTPATH__.$line->imageName ?>" border="0" />
						<p  style="padding-left:150px;">
						<a href='pass.php?action=font&type=deleteSingle&iid=
						<?=base64_encode($line->id)?>&page=<?=$_GET[page] ?>' >
						<img src='images/drop.png' height='16' width='16' border='0' title='Delete' />
						</a>
						</p>
						</li>	
						
						
						<li class="lable" >Font (TTF File) <span class="spancolor"></span></li>
						<li >						
						<input type="file" name="fontImage<?=$line->id?>" class="wel" value="" />
						<p  style="padding-left:150px;"><?=$arr_error[fontImage]?></p>
						</li>
						<? } ?>
						<!-- End : TTF Section============================== -->
						<input type="hidden" name="imageid" value="<?=$imageid?>" />
						<input type="hidden" name="theValue" id="theValue" value="<?=$num+1?>">
						<input type="hidden" name="theValueNos" id="theValueNos" value="<?=$num+1?>">
						
						<li class="add-main-body-left-new-text" style="clear:both;">&nbsp;</li>
						<li class="add-main-body-right-new-text">
						<img src="<?=__FONTPATH__.$result->fontImage?>">
						</li>
						
						<!-- Make Default ============================= -->
						<li  class="lable">Make Default<span class="spancolor"></span></li>
						<li style="padding-top:10px; float:left;">
						<li>
						<input type="checkbox" name="isDefault" <?=$checked ?> <?=$disabled?> />
						</li>
						</li>
						
						<!-- Upload TTF ======================== -->
						<? if($num <= 4 && 0) { ?>
							<div id="ttfFileUpload"> 
							<li class="lable" >Font (TTF File) <?=$num+1?>
							<span class="spancolor">*</span>
							</li>
							<li >
							<input type="file" name="fontImage<?=$num+1?>" value="" id="m__font__Image" />
							<select name="special<?=$num+1?>" >
							<option value="normal" >Normal</option>
							<option value="bold" >Bold</option>
							<option value="italic" >Italic</option>
							<option value="underLine" >UnderLine</option>
							<option value="boldI" >Bold Italic</option>
							</select>	
							<p  style="padding-left:150px;"><?=$errorImage ?></p>				
							</li>
							</div>
							<!-- Add More================================= -->
							<? if( $num < 5){ ?>
								<span id="addm">
								<a href="javascript:void(0);" onclick="addMoreRecord();" style="padding-left:300px;" >Add More</a>
								</span>
							<? }?>
						<?}?>
					</ul>
				</div>
				<div class="main-body-sub">
					<input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
					&nbsp;
					<input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack()"/>
				</div>
			</div>
		</div>
	</form>
	<div id="divTemp" style="display:none;"></div>
	<? unset($_SESSION['SESS_MSG']); ?>
	</body>
</html>