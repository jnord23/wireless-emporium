<?php
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageView.php","edit_record");
/*---Basic for Each Page Starts----*/

$viewObj = new View();
$generalFunctionObj = new GeneralFunctions();

require_once('validation_class.php');
$obj = new validationclass();
$errorArr = 0;
if(isset($_POST['submit'])) {
	$rst = $viewObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $viewObj->getTotalRow($rst);	
	if($num > 0){
		$langIdArr = array();		
		while($line = $viewObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('viewName_'.$value,$_POST['viewName_'.$value], 'req', 'Please Enter Name.');			
		}
		$obj->fnAdd("price", $_POST["price"], "req", "Please enter the Price.");
		$arr_error = $obj->fnValidate();
	    $str_validate = (count($arr_error)) ? 0 : 1;
		$arr_error[price]=$obj->fnGetErr($arr_error[price]);
		foreach($langIdArr as $key=>$value) {
			$arr_error['viewName_'.$value]=$obj->fnGetErr($arr_error['viewName_'.$value]);
			if($arr_error['viewName_'.$value]) 
				$errorArr = 1;
		}
		
		foreach($langIdArr as $key=>$value) {
			if($viewObj->isViewExist($_POST['viewName_'.$value],$value,$_POST[id]))
			{ 
				$arr_error['viewName_'.$value] = "Name already exist. ";
				if($arr_error['viewName_'.$value]) 
					$errorArr = 1;				
			}
		}	
		
	}

	if($errorArr == 0  && isset($_POST['submit']) && empty($arr_error[price])){
	    
		$viewObj->editRecord($_POST);		
	}
}

$result = $viewObj->getResult(base64_decode($_GET['id']));

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/validation.js"></script>
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>

<script type="text/javascript" src="datepicker/jquery-1.3.2.js"></script>
<script type="text/javascript" src="datepicker/ui.core.js"></script>
<script type="text/javascript" src="datepicker/ui.datepicker.js"></script>
<link type="text/css" href="datepicker/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="datepicker/demos.css" rel="stylesheet" />
<link type="text/css" href="datepicker/themes/base/ui.all.css" rel="stylesheet"/>
<script type="text/javascript">

$(function() {
	$('#startDate').datepicker({
		showButtonPanel: true
	});
});

$(function() {
	$('#endDate').datepicker({
		showButtonPanel: true
	});
});
</script>

</head>
<body>
<? include('includes/header.php'); ?>
<div id="nav-under-bg"><!-- --></div>
  <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
  <input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">		
  <input type="hidden" name="page" value="<?=$_GET['page']?>">	
		<div class="main-body-div-new">
          <div class="main-body-div-header">Edit View</div>
		  <!-- left position -->
            <div class="main-body-div4" id="mainDiv">
              <div class="add-main-body-left-new">
                <ul>
                  <li class="add-main-body-left-new-text" style="clear:both; width:500px;" ><span class="small_error_message">
                    <?=$_SESSION['SESS_MSG']?>
                  </span></li>
                  <li class="lable">Name <span class="spancolor">*</span></li>
                    <?	
				  		$genObj = new GeneralFunctions();
						echo $genObj->getLanguageEditTextBox('viewName','m__Name',TBL_VIEWDESC,base64_decode($_GET['id']),"viewId",$arr_error); //1->type,2->name,3->id,4->tablename,5->tableid
					?>
					 <li class="lable">Price <span class="spancolor">*</span></li>
                     <li><input type="text" name="price" id="m__price" maxlength="6" value="<?=$result->price ?>" /><p style="padding-left:150px;"><?=$arr_error[price] ?></p></li>
                </ul>
              </div>
              <div class="main-body-sub">
                <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                &nbsp;
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack()"/>
              </div>
            </div>
</div>

</form>

<? unset($_SESSION['SESS_MSG']); ?>