<?php
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageAttribute.php","add_record");
/*---Basic for Each Page Ends----*/

$catObj = new Attribute();
if(isset($_POST['submit'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $catObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $catObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $catObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
				
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('attributeName_'.$value,$_POST['attributeName_'.$value], 'req', 'Please Enter Attribute Name.');			
		}		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		foreach($langIdArr as $key=>$value) {
			$arr_error['attributeName_'.$value]=$obj->fnGetErr($arr_error['attributeName_'.$value]);
			if($arr_error['attributeName_'.$value]) 
				$errorArr = 1;
		}			
		foreach($langIdArr as $key=>$value) {
			if($catObj->isAttributeNameExist($_POST['attributeName_'.$value],$value))
			{ 
				$arr_error['attributeName_'.$value] = "Attribute already exist. ";
				if($arr_error['attributeName_'.$value]) 
					$errorArr = 1;				
			}
		}								
		if($errorArr == 0 && isset($_POST['submit'])){
			$_POST = postwithoutspace($_POST);
			
			$catObj->addRecord($_POST);
		}
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->
<script type="text/javascript">
function hrefBack1(){
	window.location='manageAttribute.php';
}
</script>
</head>
<body>
	<? include('includes/header.php'); ?>
	<div id="nav-under-bg"><!-- --></div>
	<form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);">
		<input type="hidden" name="cid" value="<?=$cid?>">
		<div class="main-body-div-new">
			<div class="main-body-div-header">Add Attribute</div>
			<!-- left position -->
			<div class="main-body-div4" id="mainDiv">
				<div class="add-main-body-left-new" >
					<ul>
					<li class="add-main-body-left-new-text" style="clear:both; width:500px;" >
					<span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span>
					</li>				  
					
					<li class="lable">Attribute Name <span class="spancolor">*</span></li>
					<?	$genObj = new GeneralFunctions();
					echo $genObj->getLanguageTextBox('attributeName','m__attributeName',$arr_error); //1->type,2->name,3->id
					?>
					</ul>
				</div>
				<div class="main-body-sub">
					<input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
					&nbsp;
					<input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
				</div>
			</div>
		</div>
	</form>	
	<? unset($_SESSION['SESS_MSG']); ?>
</body>
</html>