<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();
/*---Basic for Each Page Starts----*/

$sysObj = new SystemConfig();

if(isset($_POST['submit'])) {
  	$sysObj->addConfiguration($_POST,$_FILES);
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
  function show_value(){
	var sum1 = document.configUser.Allow_user_to_show_identification.length;
	     for (var i=0; i < sum1; i++) {
               if (document.configUser.Allow_user_to_show_identification[0].checked)
			          document.getElementById("ident").style.display='';
			   if (document.configUser.Allow_user_to_show_identification[1].checked)
			          document.getElementById("ident").style.display='none';
			   
         }
  }
</script>
</head>
<body onload="show_value();">
	<? include('includes/header.php'); ?>
	<div id="nav-under-bg"><!-- --></div>
	<div class="main-body-div-less-width">
		<form name="configUser" id="configUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
			
				<div class="main-body-div-header">Config Setting</div>
					<!--start : White Colored section -->
					<div class="main-body-div4" id="mainDiv"><br />
						<!--start : Center Div-->
						<div align="center" >
						<div align="left" style="width:400px">
							<!--start : Session Message -->						
							<? if($_SESSION['SESS_MSG'] != "") { ?>				
								<?=$_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']);?>
							<? } ?>							
							<!--End : Session Message -->
							
							
							<!-- Start : General Configuration -->
							<fieldset class="main">
								<legend style="color:#000000;"><b>&nbsp;
								General Configuration&nbsp;</b>
								</legend>
								<!-- Site Name -->
								<div>
								Site Name :&nbsp;
								<input name="SITE_NAME" id="SITE_NAME" type="text" value="<?=stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'SITE_NAME'"))?>" class="wel" />
								</div>
								<!-- Site Email -->
								<div>
								Site Email :&nbsp;
								<input name="SITE_EMAIL" id="SITE_EMAIL" type="text" value="<?=stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'SITE_EMAIL'"))?>" class="wel" />
								</div>
								<!--Site Logo -->
								<?php
								$logo=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'SITE_LOGO'");
								$logo=__SITELOGOTHUMBPATH__.$logo;					
								?>
								<div>
									<div style="padding-top:10px;">				
									<img src="<?=$logo?>" alt="" />
									</div>
								Site Logo :&nbsp;	
								<input name="SITE_LOGO" type="file" class="wel" />		</div>	
								
							</fieldset>
							<!-- End : General Configuration -->
							
							
							<!-- Start : Front End Configuration  -->
							<fieldset class="main">
								<legend style="color:#000000;">
                                                                <b>&nbsp;Upload Files  Configuration&nbsp;</b>
								</legend>									
								<!-- image extention allowed -->
								<div>Image Extension Allowed :&nbsp;
								<input name="IMAGE_EXTENSION" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'IMAGE_EXTENSION'")?>" class="wel" />	
                                                                </div>
                                                                
                                                                <!-- Designs extention allowed -->
								<div>Designs Extension Allowed :&nbsp;
								<input name="DESIGNS_EXTENSION" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'DESIGNS_EXTENSION'")?>" class="wel" />	
                                                                </div>
							</fieldset>
							<!-- End : Front End Configuration  -->	
							
							<!-- Start : Font SWF/TTF Configuration -->
							<fieldset class="main">
								<legend style="color:#000000;">
								<b>&nbsp;Font Configuration&nbsp;</b>
								</legend>
								<div>Allow TTF With SWF :&nbsp;
									
									<input name="SWF_TTF" type="radio" value="2" style="cursor:pointer;" <? if($sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'SWF_TTF'") == 2) echo "checked";?>/>
									<br />
									Allow TTF Only :<input name="SWF_TTF" type="radio" value="1" style="cursor:pointer;" <? if($sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'SWF_TTF'") == 1) echo "checked";?>/>
								</div>
							</fieldset>
							<!-- End : Font SWF/TTF Condfiguration -->
                                                        
                                                        <!-- Start : Category Configuration -->
							<fieldset class="main" >
								<legend style="color:#000000;">
								<b>&nbsp;Categorie's Level Configuration&nbsp;</b>
								</legend>
								
								<div>
									Category Level  :&nbsp;
									<select name="LEVEL_CATEGORY" size="1" disabled="disabled" > 
									<? for($i=1;$i<5;$i++){ if($sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'LEVEL_CATEGORY'") == $i){
										$sel="selected = 'selected'";
									}else{
										$sel= '';
									}?>
										<option value="<?=$i ?>" <?=$sel ?>><?=$i ?></option>
									<? }?>
									</select>
								</div>
								<div>&nbsp;</div>
								<div>
									Design Category Level  :&nbsp;
									<select name="LEVEL_DESIGN_CATEGORY" size="1" disabled="disabled" > 
									<? for($i=1;$i<5;$i++){ if($sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'LEVEL_DESIGN_CATEGORY'") == $i){
										$sel="selected = 'selected'";
									}else{
										$sel= '';
									}?>
										<option value="<?=$i ?>" <?=$sel ?>><?=$i ?></option>
									<? }?>
									</select>
								</div>
								<div>&nbsp;</div>								
								<div>
									Font Category Level  :&nbsp;
									<select name="LEVEL_FONT_CATEGORY" size="1" disabled="disabled" > 
									<? for($i=1;$i<5;$i++){ if($sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'LEVEL_FONT_CATEGORY'") == $i){
										$sel="selected = 'selected'";
									}else{
										$sel= '';
									}?>
										<option value="<?=$i ?>" <?=$sel ?>><?=$i ?></option>
									<? }?>
									</select></div>
								<div>&nbsp;</div>
							</fieldset>
							<!-- End  : Category Configuration -->
							
							<!-- Start : Thumb Configuration=========================- -->
							
							<fieldset class="main" style="display:none;">
                                                            <legend style="color:#000000;"><b> Thumb Configuration (In Pixels)</b></legend>                                                            
                                                            
                                                            <!-- Raw Product Thumb =============================== ------------>
                                                            <fieldset class="inner">
                                                            <legend style="color:#000000;"><b>Raw Product Thumb </b></legend>
                                                            
                                                                <div>Thumb (Width&nbsp;x&nbsp;Height) :&nbsp;
                                                                         <input name="THUMB_WIDTH" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'THUMB_WIDTH'")?>" class="welnew" />
                                                                         &nbsp;x&nbsp;
									<input name="THUMB_HEIGHT" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'THUMB_HEIGHT'")?>" class="welnew" />
                                                                </div>	
                                                            </fieldset>
                                                            
                                                            <!-- Category Thumb============================================= --->
                                                            <fieldset class="inner">
								<legend style="color:#000000;">
								<b>Category Thumb </b>
								</legend>		
                                                                
                                                                <div>Thumb (Width&nbsp;x&nbsp;Height) :&nbsp;
                                                                         <input name="CATEGORY_THUMB_WIDTH" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'CATEGORY_THUMB_WIDTH'")?>" class="welnew" />
                                                                         &nbsp;x&nbsp;
									<input name="CATEGORY_THUMB_HEIGHT" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'CATEGORY_THUMB_HEIGHT'")?>" class="welnew" />
                                                                </div>	
                                                                <div>Large (Width&nbsp;x&nbsp;Height) :&nbsp;
									<input name="CATEGORY_LARGE_WIDTH" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'CATEGORY_LARGE_WIDTH'")?>" class="welnew" />
                                                                        &nbsp;x&nbsp;
                                                                        <input name="CATEGORY_LARGE_HEIGHT" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'CATEGORY_LARGE_HEIGHT'")?>" class="welnew" />
                                                                </div>	                                                                
								
                                                            </fieldset>
                                                            
                                                            <!-- Design Category Thumb================================ -->
                                                           <fieldset class="inner">
								<legend style="color:#000000;">
								<b>Design Category Thumb </b>
								</legend>
								
                                                                <div>Thumb (Width&nbsp;x&nbsp;Height) :&nbsp;
                                                                         <input name="DESIGNCAT_THUMB_WIDTH" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'DESIGNCAT_THUMB_WIDTH'")?>" class="welnew" />
                                                                         &nbsp;x&nbsp;
									<input name="DESIGNCAT_THUMB_HEIGHT" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'DESIGNCAT_THUMB_HEIGHT'")?>" class="welnew" />
                                                                </div>	
                                                                <div>Large (Width&nbsp;x&nbsp;Height) :&nbsp;
									<input name="DESIGNCAT_LARGE_WIDTH" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'DESIGNCAT_LARGE_WIDTH'")?>" class="welnew" />
                                                                        &nbsp;x&nbsp;
                                                                        <input name="DESIGNCAT_LARGE_HEIGHT" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'DESIGNCAT_LARGE_HEIGHT'")?>" class="welnew" />
                                                                </div>	
                                                            </fieldset>
                                                            
                                                            <!-- Font Category Thumb================================ -->
                                                            <fieldset class="inner">
								<legend style="color:#000000;">
								<b>Font Category Thumb</b>
								</legend>		
                                                                
                                                                <div>Thumb (Width&nbsp;x&nbsp;Height) :&nbsp;
                                                                         <input name="FONTCAT_THUMB_WIDTH" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'FONTCAT_THUMB_WIDTH'")?>" class="welnew" />
                                                                         &nbsp;x&nbsp;
									<input name="FONTCAT_THUMB_HEIGHT" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'FONTCAT_THUMB_HEIGHT'")?>" class="welnew" />
                                                                </div>	
                                                                <div>Large (Width&nbsp;x&nbsp;Height) :&nbsp;
									<input name="FONTCAT_LARGE_WIDTH" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'FONTCAT_LARGE_WIDTH'")?>" class="welnew" />
                                                                        &nbsp;x&nbsp;
                                                                        <input name="FONTCAT_LARGE_HEIGHT" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'FONTCAT_LARGE_HEIGHT'")?>" class="welnew" />
                                                                </div>	
                                                            </fieldset>
                                                            
                                                            <!-- Flag  Thumb================================ -->
                                                            <fieldset class="inner">
								<legend style="color:#000000;">
								<b>&nbsp;Flag Thumb Configuration&nbsp;</b>
								</legend>
								<div>Thumb (Width&nbsp;x&nbsp;Height) :&nbsp;
                                                                         <input name="LANGFLAG_THUMB_WIDTH" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'LANGFLAG_THUMB_WIDTH'")?>" class="welnew" />
                                                                         &nbsp;x&nbsp;
									<input name="LANGFLAG_THUMB_HEIGHT" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'LANGFLAG_THUMB_HEIGHT'")?>" class="welnew" />
                                                                </div>
                                                            </fieldset>
                                                            
                                                            <!-- Design Thumb==================================== -->
                                                            <fieldset class="inner">
								<legend style="color:#000000;">
								<b>Designs Thumb</b>
								</legend>			
                                                                
                                                                 <div>Thumb (Width&nbsp;x&nbsp;Height) :&nbsp;
                                                                         <input name="DESIGNS_THUMB_WIDTH" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'DESIGNS_THUMB_WIDTH'")?>" class="welnew" />
                                                                         &nbsp;x&nbsp;
									<input name="DESIGNS_THUMB_HEIGHT" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'DESIGNS_THUMB_HEIGHT'")?>" class="welnew" />
                                                                </div>	                                                                						
                                                            </fieldset>
                                                              
                                                            
							</fieldset>
	
							<!-- End : Thumb Configuration======================================->  
							
							<!-- Start : Submit/Back Button -->
							<div class="main-body-sub" style="text-align:center; margin-left:0px">			
								<input type="submit" class="main-body-sub-submit" style="cursor:pointer;" name="submit" value="Submit" />&nbsp;
								<input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;" onClick="hrefBack()"/>
							</div>
							<!-- End : Submit/Back Button -->
						</div><!-- End Alight Left -->
						</div><!--End : Center Div-->
					</div><!--End : White Colored section -->
				</div>
				
		</form>
	</div>
</body>
</html>
<style>
    .main{ width:350px;border:1px solid #6F0229; margin-bottom:10px; }
    .inner{width:330px;border:1px solid #6F0229; margin-bottom:10px; }
    .welnew{ margin-top: 10px; width: 40px;}
</style>