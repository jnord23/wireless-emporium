<?php
ob_start();
session_start();
$action = $_GET['action'];
$mode = $_GET['mode'];
$type = $_GET['type'];
/**************************** autoload the classes ****************/
require_once('includes/function/autoload.php');


// Start : Admin========================

if ($action == 'admin') {
 	$admObj = new AdminDetail;
	if($type == 'changestatus'){
		$admObj->changStatus($_GET);
	}
	if($type == 'delete'){
		$admObj->deleteRecord($_GET);
	}
}	

// Start : Category===============================

if ($action == 'category') {
 	$categoryObj = new Category();    
	if($type == 'changestatus'){
		$categoryObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$categoryObj->deleteValue($_GET);
	}
        if($type == 'deleteall'){
            if(!$_POST['Input']){
                
               if($_POST[cid]>0){                   
                   $url="manageCategory.php?cid=".$_POST[cid]."&searchtxt=".$_POST['searchtext'];
               }else{
                    $url="manageCategory.php?searchtxt=".$_POST['searchtext'];
               }
                header("Location:$url");exit;
            } else {
                $categoryObj->deleteAllValues($_POST);
            }			
        }
	if($type == 'SORTORDER'){
		$categoryObj->SortSequence($_GET);
	}
        
       
	
}


// Start : Design Category==========================

if ($action == 'design_category') {
 	$designCatObj = new DesignCategory();    
	if($type == 'changestatus'){
		$designCatObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$designCatObj->deleteValue($_GET);
	}
	 if($type == 'deleteall'){
            if(!$_POST['Input']){
                $url="manageDesignCategory.php?searchtxt=".$_POST['searchtext'];
                header("Location:$url");exit;
            } else {
                $designCatObj->deleteAllValues($_POST);
            }			
        }
	if($type == 'SORTORDER'){
		$designCatObj->SortSequence($_GET);
	}
}


// Start : Font Category=============================
if ($action == 'font_category') {
 	$fontCatObj = new FontCategory();    
	if($type == 'changestatus'){
		$fontCatObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$fontCatObj->deleteValue($_GET);
	}
	 if($type == 'deleteall'){
            if(!$_POST['Input']){
                $url="manageFontCategory.php?searchtxt=".$_POST['searchtext'];
                header("Location:$url");exit;
            } else {
                $fontCatObj->deleteAllValues($_POST);
            }			
        }
	if($type == 'SORTORDER'){
		$fontCatObj->SortSequence($_GET);
	}
}



// Start : FONT======================================
if ($action == 'font') {       
 	$fontObj = new Font();    
	if($type == 'changestatus'){		
		$fontObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$fontObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(isset($_POST[GO])){
                        
			header("Location:manageFont.php?cid=".$_POST[cid]."&searchtxt=".$_POST['searchtxt']);exit;
		}
                if(isset($_POST[Input])){
			$fontObj->deleteAllValues($_POST);
		}
	}
	if($type == 'deleteSingle'){
		$fontObj->deleteAllSingleValues($_GET);
	}
	
}

// Start : Language==================================================
if ($action == 'language') {
 	$categoryObj = new Language();    
	if($type == 'changestatus'){
		$categoryObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$categoryObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$categoryObj->deleteAllValues($_POST);
		}else{
		header("Location:manageLanguage.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
}


// Start : Color================================
if ($action == 'Color') {
 	$colorObj = new Color();    
	if($type == 'changestatus'){
		$colorObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$colorObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageColor.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$colorObj->deleteAllValues($_POST);
		}
	}
}

// Start : View==============================
if ($action == 'view') {
 	$viewObj = new View();    
	if($type == 'changestatus'){
		$viewObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$viewObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageView.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$viewObj->deleteAllValues($_POST);
		}
	}
	if($type == 'SORTORDER'){
		$viewObj->SortSequence($_GET);
	}
}

// Start : Currency=============================
if ($action == 'currency') {
 	$currObj = new Currency();    
	if($type == 'changestatus'){
		$currObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$currObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$currObj->deleteAllValues($_POST);
		}else{
			header("Location:manageCurrency.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
}

// Start : Size=================================
if ($action == 'Size') {
 	$sizeObj = new Size();    
	if($type == 'changestatus'){
		$sizeObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$sizeObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageSize.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$sizeObj->deleteAllValues($_POST);
		}
	}
	if($type == 'SORTORDER'){
		$sizeObj->SortSequence($_GET);
	}

}



// Start : Size Group==============================
if ($action == 'SizeGroup') {
 	$sizeObj = new SizeGroup();    
	if($type == 'changestatus'){
		$sizeObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$sizeObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:managesizegroup.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$sizeObj->deleteAllValues($_POST);
		}
	}
	if($type == 'SORTORDER'){
		$sizeObj->SortSequence($_GET);
	}

}

// Start : Tool============================
if ($action == 'Tool') {
 	$toolObj = new Tool();    
	if($type == 'changestatus'){
		$toolObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$toolObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageTool.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$toolObj->deleteAllValues($_POST);
		}
	}
}

// Start : Attribute========================================

if ($action == 'attribute') {
 	$attributeObj = new Attribute();    
	if($type == 'changestatus'){
		$attributeObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$attributeObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageAttribute.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$attributeObj->deleteAllValues($_POST);
		}			
	}
}



// Start : Attribute Values===============================================

if ($action == 'attributeValues') {
 	$attributeObj = new AttributeValues();    
	if($type == 'changestatus'){
		$attributeObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$attributeObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageAttributeValues.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$attributeObj->deleteAllValues($_POST);
		}			
	}
}



// Start : Raw Product========================

if ($action == 'product') {
       // echo "<pre>"; print_r($_GET); echo "</pre>";exit;
 	$prodObj = new Products();    
	if($type == 'changestatus'){
		$prodObj->changeValueStatus($_GET);
	}
	
	if($type=='subcat'){
	     $genObj = new GeneralFunctions();		 
		$genObj->subCategoryList($_GET['id']);
	}
	
	if($type == 'delete'){
		$prodObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(isset($_POST[GO])){
			header("Location:manageProducts.php?cid=".$_POST[cid]."&searchtxt=".$_POST['searchtxt']);exit;
		} 
                if(isset($_POST['Input'])){
			$prodObj->deleteAllValues($_POST);
		}			
	}
	if($type == 'SORTORDER'){
		$prodObj->sortProductsOrder($_GET);
	}
	if($type=='delProductView'){		
		$prodObj->delProductView($_GET);		
	}
	if($type == 'addProductViewImages'){
		$prodObj->addProductViewImages($_GET);

	}
}

// Start : Upload Raw Image============================
if ($action == 'uploadrawimg') {
 	$productObj = new Products();    
	if($type == 'updateraw'){
		$productObj->updateRawColorProduct($_POST,$_FILES);
	}
}

//Start : Delete Raw Color View Images=========================
if ($action == 'delRawImg') {
 	$productObj = new Products();    
	if($type == 'deletecolorproduct'){
		$productObj->deleteRawImage($_GET);
	}	
}

//Start : Delete Raw Color Product=================================
if ($action == 'deleterawcolorproduct') {
 	$productObj = new Products();    
	if($type == 'deletecolorproduct'){
		$productObj->deleteRawColorImage($_GET);
	}
}

//Start : Commit=============================================
if ($action == 'Welcommit') {	
 	$commitObj = new WelCommit(); 
	if($type == 'generatexml'){
		$commitObj->generateXML($_GET);
	}
}

//Start Design==========================================================================
if ($action == 'design') {
 	$designObj = new Design();
	if($type == 'getSubCategoryDesign'){
		$designObj->getSubCategoryDesign($_GET); 
	}
	if($type == 'delete'){
		$designObj->deleteValue($_GET);
	}
	if($type == 'changestatus'){
		$designObj->changeValueStatus($_GET);
	}
	if($type == 'deleteall'){  
            if(isset($_POST[GO])){                       
                header("Location:manageDesign.php?cid=".$_POST['cid']."&searchtxt=".$_POST['searchtxt']);exit;
            }
            if(isset($_POST['Input'])) {                         
            $designObj->deleteAllValues($_POST);
            }		
	}
}

//Start : Commit==========================
if ($action == 'commited') {
    $commitObj= new Commited;
    if($type == 'generatexml'){	
        $commitObj->generateXML();   
        //$commitObj->createXml();
    }
    if($type == 'generatexmlByTool'){
        $commitObj->createXmlByTool();
    }
}
//Start : Assign To Main Product=======================================================
if ($action == 'assigndesign') {
    $assignProduct= new AssignMainProducts(); 
    if($type == 'deleteall'){
        if($_POST['GO'] && $_POST['action'] == ""){
            header("Location:assign-main-product.php?searchtxt=".$_POST['searchtext']);exit;
        }else{
            $assignProduct->deleteAllValues($_POST);
        }
    }
    if($type == 'assigntomainproduct'){
        $assignProduct->assigntomainproduct($_GET);
    }
}

/*******Main Product**************************************************/
if ($action == 'mainproduct') {
    //echo "<pre>"; print_r($_GET); print_r($_POST);echo "</pre>";exit;
    $mprodObj = new MainProducts();  
    if($type == 'sort_main_product_order'){
        $MainProducts->sort_main_product_order($_GET);
    }
    if($type == 'changestatus'){
        $mprodObj->changeValueStatus($_GET);
    }
    if($type == 'delete'){
        $mprodObj->deleteValue($_GET);
    }
    if($type=="downloadpdf"){
        $mprodObj->getProductDownload($_GET[pid]);
    }
    if($type == 'deleteall'){
        if(isset($_POST['Input'])){
            $mprodObj->deleteAllValues($_POST);
        }
        if(isset($_POST['GO'])){
            header("Location:manageMainProducts.php?searchtxt=".$_POST['searchtext']);exit;
        }
        if($_POST[usertype]!=""){
            header("Location:manageMainProducts.php?type=".$_POST['usertype']);exit;
        }
    }    
    
    if($type == 'SORTORDER'){
        $mprodObj->sortProductsOrder($_GET);
    }

}

/******* PDF **************************************************/
if ($action == 'pdf') {
    //echo "<pre>"; print_r($_GET); print_r($_POST);echo "</pre>";exit; 
    $pdfObj= New GeneratePDF;
    if($type=="download"){
        $pdfObj->createPDF($_GET[pid]);
    }

}

// Start : Backup =============================================================
    
if ($action == 'backup') {
	$bkpObj = new Backup();	
	if($type == 'takeBackup'){
		$bkpObj->takeBackup($_POST);
	}
	if($type == 'downloadBackup'){
		$bkpObj->downloadBackup($_GET);
	}	
	if($type == 'restore'){
		$bkpObj->restoreDB($_GET['id']);
	}
	if($type == 'delete'){
		$bkpObj->deleteValue($_GET);
	}		
}


/*
/=================================== old functions===================================================

if ($action == 'language') {
 	$categoryObj = new Language();    
	if($type == 'changestatus'){
		$categoryObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$categoryObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$categoryObj->deleteAllValues($_POST);
		}else{
		header("Location:managelanguage.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
}




if ($action == 'showState') {
	$generalSettingObj = new GeneralFunctions();	
	$generalSettingObj->getStateInDropdown12($_GET['countryId']);
}

if ($action == 'showStateMULTIPLE') {
	$generalSettingObj = new GeneralFunctions();	
	$generalSettingObj->getShipStateMultipel($_GET['countryId']);
}



if ($action == 'mailtype') {
	$mailSettingObj = new MailSetting();	
	if($type == 'deleteall'){
		header("Location:manageMailType.php?searchtxt=".$_POST['searchtext']);
	}
}

if ($action == 'mailtypeDescription') {
	$mailSettingObj = new MailSetting();	
	if($type == 'deleteall'){
		header("Location:manageMailDescription.php?searchtxt=".$_POST['searchtext']."&id=".$_POST['id']);
	}
}


if ($action == 'newsletter') {
 	$categoryObj = new Newsletter();    
	if($type == 'changestatus'){
		$categoryObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$categoryObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$categoryObj->deleteAllValues($_POST);
		}else{
		header("Location:manageNewsletter.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
}
/// for manage News letter Template
if ($action == 'newsletterTemplate') {
 	$categoryObj = new Newsletter();    
	if($type == 'defaultTemp'){
		$categoryObj->changeDefaultStatus($_GET);
	}if($type == 'changestatus'){
		$categoryObj->changeTemplateStatus($_GET);
	}if($type == 'delete'){
		$categoryObj->deleteNewsletterTemp($_GET);
	}if($type == 'deleteall'){
		if($_POST['Input']){
			$categoryObj->deleteAllTemplateValues($_POST);
		}else{
		header("Location:manageNewsletterTemp.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
	
}
/// for manage Gift Cartifiate
if ($action == 'giftcertificate') {
 	$categoryObj = new GiftCertificate();    
	if($type == 'changestatus'){
		$categoryObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$categoryObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$categoryObj->deleteAllValues($_POST);
		}else{
		header("Location:manageGiftCertificate.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
}


/// for Manage User
if ($action == 'manageuser') {
 	$categoryObj = new User();    
	if($type == 'changestatus'){
		$categoryObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$categoryObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$categoryObj->deleteAllValues($_POST);
		}else{
		header("Location:manageUser.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
}
/// for Manage Meta Data

if ($action == 'metadata') {
 	$categoryObj = new MetaData();    
	if($type == 'delete'){
		$categoryObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$categoryObj->deleteAllValues($_POST);
		}else{
		header("Location:manageMetaData.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
}

// for contact us
if ($action == 'contactUs') {
 	$categoryObj = new ContactUs();    
	if($type == 'delete'){
		$categoryObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$categoryObj->deleteAllValues($_POST);
		}else{
		header("Location:manageContactUs.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
}


//////////////For Bulk Order ///////////////////////
if ($action == 'bulkOrder') {
 	$bulkOrderObj = new BulkOrder();    
	if($type == 'delete'){
		$bulkOrderObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$bulkOrderObj->deleteAllValues($_POST);
		}else{
		header("Location:manageBulkOrder.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
}
/////////////////////////////

//////////////For Specific  Bulk Order ///////////////////////
if ($action == 'specificBulkOrder') {
 	$bulkOrderObj = new SpecificBulkOrder();    
	if($type == 'delete'){	
		$bulkOrderObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$bulkOrderObj->deleteAllValues($_POST);
		}else{
		header("Location:manageSpecificBulkOrder.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}
}
/////////////////////////////

if ($action == 'sendnewsletter') {
 	$categoryObj = new Newsletter();    
	if($type == 'search'){
		header("Location:viewSendNewsletter.php?searchtxt=".$_POST['searchtext']);exit;
	}
}

if ($action == 'staticpagetype') {
		$staticPageObj = new StaticPage();
		if($type == 'delete'){
			$staticPageObj->deleteStaticPageValue($_GET);
		}
		if($type == 'deleteall'){
		if($_POST['Input']){
			$staticPageObj->deleteAllStaticPagesValues($_POST);
		}else{
		header("Location:manageStaticPage.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}		
}

if ($action == 'staticpageDescription') {
	$staticPageObj = new StaticPage();	
	if($type == 'deleteall'){
		header("Location:manageStaticPageDescription.php?searchtxt=".$_POST['searchtext']."&id=".base64_encode($_POST['id']));
	}
	if($type == 'delete'){
		$staticPageObj->deleteValue($_GET);
	}	
}



if ($action == 'backup') {
	$bkpObj = new Backup();	
	if($type == 'takeBackup'){
		$bkpObj->takeBackup($_POST);
	}
	if($type == 'downloadBackup'){
		$bkpObj->downloadBackup($_GET);
	}	
	if($type == 'restore'){
		$bkpObj->restoreDB($_GET['id']);
	}
	if($type == 'delete'){
		$bkpObj->deleteValue($_GET);
	}		
}


// Manage Banner................................................... 

if ($action == 'banner') {
 	$bannerObj = new Banner();    
	if($type == 'changestatus'){
		$bannerObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$bannerObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageBanner.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$bannerObj->deleteAllValues($_POST);
		}
		
	}
}


// Manage Coupon................................................... 

if ($action == 'coupon') {
 	$couponObj = new Coupon();    
	if($type == 'getAllProducts'){
		$couponObj->getAllProducts('',$_GET['keyword'],'1');
	}	
	if($type == 'getAllCustomers'){
		$couponObj->getAllCustomers('',$_GET['keyword'],'1');
	}		
	if($type == 'changestatus'){
		$couponObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$couponObj->deleteValue($_GET);
	}
	if($type == 'generateNumber'){
		$couponObj->generateNumber();
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageCoupon.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$couponObj->deleteAllValues($_POST);
		}		
	}
}

// Manage Zone................................................... 

if ($action == 'zone') {
 	$zoneObj = new Zone();    
	if($type == 'changestatus'){
		$zoneObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$zoneObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageZone.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$zoneObj->deleteAllValues($_POST);
		}			
	}
}

// Manage Country................................................... 

if ($action == 'country') {
 	$countryObj = new Country();    
	if($type == 'changestatus'){
		$countryObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$countryObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageCountry.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$countryObj->deleteAllValues($_POST);
		}			
	}
}


// Manage State................................................... 

if ($action == 'state') {
 	$stateObj = new State();    
	if($type == 'changestatus'){
		$stateObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$stateObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageState.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$stateObj->deleteAllValues($_POST);
		}			
	}
	if($type == 'getCountryList'){
		$genObj = new GeneralFunctions();
		$genObj->getCountryList($_GET['zoneid'],"",$_GET['mode']);
	}
	if($type == 'getStateList'){
		$genObj = new GeneralFunctions();
		$genObj->getStateInDropdown12($_GET['countryID']);
	}
}

// Manage County................................................... 

if ($action == 'county') {
 	$stateObj = new County();    
	if($type == 'changestatus'){
		$stateObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$stateObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:managecounty.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$stateObj->deleteAllValues($_POST);
		}			
	}
	if($type == 'getCountryList'){
		$genObj = new GeneralFunctions();
		$genObj->getCountryList($_GET['zoneid'],"",$_GET['mode']);
	}
}







// Manage Row Product................................................... 

if ($action == 'product') {
 	$prodObj = new Products();    
	if($type == 'changestatus'){
		$prodObj->changeValueStatus($_GET);
	}
	
	if($type=='getCatagoryList'){
	     $genObj = new GeneralFunctions();
		 
		 $genObj->subCategoryList($_GET);
	 }
	
	if($type == 'delete'){
		$prodObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageProducts.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$prodObj->deleteAllValues($_POST);
		}			
	}
	if($type == 'SORTORDER'){
		$prodObj->sortProductsOrder($_GET);
	}
}

// Manage Main Product................................................... 

if ($action == 'mainproduct') {
 	$mprodObj = new MainProducts();    
	if($type == 'changestatus'){
		$mprodObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$mprodObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			if($_POST['catsearch']){
				header("Location:manageMainProducts.php?catId='".base64_encode($_POST['catId']));exit;
			}else{
				//header("Location:manageMainProducts.php?searchtxt=".$_POST['searchtext']);exit;
				header("Location:manageMainProducts.php?searchtxt=".$_POST['searchtext']."&custom=".$_POST['custom']);exit;
			}
		} else {
			$mprodObj->deleteAllValues($_POST);
		}			
	}
	if($type == 'SORTORDER'){
		$mprodObj->SortSequence($_GET);
	}
}



// Manage Sign up fields................................................... 

if ($action == 'signupstatus') {
 	$signupfieldObj = new SignupFields();    
	if($type == 'changestatus'){
		$signupfieldObj->changeValueStatus($_GET);
	}
		if($type == 'deleteall'){
			if(!$_POST['Input']){
			header("Location:manageRegistration.php?searchtxt=".$_POST['searchtext']);exit;
			} else {
			$signupfieldObj->deleteAllValues($_POST);
			}			
		}
}


if ($action == 'signupvalidation') {
 	$signupfieldObj = new SignupFields();    
	if($type == 'changestatus'){
		$signupfieldObj->changeValidationStatus($_GET);
	}
}






// Manage Billing fields................................................... 

if ($action == 'billingstatus') {
 	$signupfieldObj = new BillingFields();    
	if($type == 'changestatus'){
		$signupfieldObj->changeValueStatus($_GET);
	}
		if($type == 'deleteall'){
			if(!$_POST['Input']){
			header("Location:manage-billing-fields.php?searchtxt=".$_POST['searchtext']);exit;
			} else {
			$signupfieldObj->deleteAllValues($_POST);
			}			
		}
}


if ($action == 'billingvalidation') {
 	$signupfieldObj = new BillingFields();    
	if($type == 'changestatus'){
		$signupfieldObj->changeValidationStatus($_GET);
	}
}



// Manage Testimonial fields................................................... 

if ($action == 'testimonialstatus') {
 	$signupfieldObj = new TestimonialFields();    
	if($type == 'changestatus'){
		$signupfieldObj->changeValueStatus($_GET);
	}
		if($type == 'deleteall'){
			if(!$_POST['Input']){
			header("Location:manageTestimonialFields.php?searchtxt=".$_POST['searchtext']);exit;
			} else {
			$signupfieldObj->deleteAllValues($_POST);
			}			
		}
}


if ($action == 'testimonialvalidation') {
 	$signupfieldObj = new TestimonialFields();    
	if($type == 'changestatus'){
		$signupfieldObj->changeValidationStatus($_GET);
	}
}





// Manage Testimonials by urv................................................... 

if ($action == 'Testimonials') {
 	$testimonialObj = new Testimonial();    
	if($type == 'changestatus'){
		$testimonialObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$testimonialObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageTestimonial.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$testimonialObj->deleteAllValues($_POST);
		}
	}
}

// Manage Forum Topic by urv.....13 March............................................. 

if ($action == 'ForumTopic') {
 	$forumTObj = new ForumTopic();    
	if($type == 'changestatus'){
		$forumTObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$forumTObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageTestimonial.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$forumTObj->deleteAllValues($_POST);
		}
	}
}


// Manage Forum Thread by urv.....13 March............................................. 

if ($action == 'ForumThread') {
 	$forumTObj = new ForumThread();    
	if($type == 'changestatus'){
		$forumTObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$forumTObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageTestimonial.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$forumTObj->deleteAllValues($_POST);
		}
	}
}

// Manage Forum Post by urv.....15 March............................................. 

if ($action == 'ForumPost') {
     
	$forumPObj = new ForumPost();
	if($type == 'FindThread'){
		$forumPObj->FindAllThread($_GET);
	}
	if($type == 'changestatus'){
		$forumPObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$forumPObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageForumPost.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$forumPObj->deleteAllValues($_POST);
		}
	}
}

// Manage Ordre ................................................... 

if ($action == 'manageorder') {
	//print_r($_GET);
 	$orderObj = new Order();    
	if($type == 'delete'){
		$orderObj->deleteValue($_GET);
	}
	if($type == 'changestatus'){
		$orderObj->changeValueStatus($_GET);
	}
	if($type == 'changeorderstatus'){
		$orderObj->changeorderstatus($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$orderObj->deleteAllValues($_POST);
		}else{
		
		header("Location:manage-order.php?searchtxt=".$_POST['searchtext']."&PayMethod=".$_POST['PayMethod']."");exit;
		}
	}

}

// Manage Order Production........................ 

if ($action == 'OrderProduction') {
	//print_r($_GET);
 	$orderObj = new OrderProduction();    
	if($type == 'delete'){
		$orderObj->deleteValue($_GET);
	}
	if($type == 'changestatus'){
		$orderObj->changeValueStatus($_GET);
	}
	if($type == 'changeorderstatus'){
		$orderObj->changeorderstatus($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$orderObj->deleteAllValues($_POST);
		}else{
		
		header("Location:orderproduction.php?searchtxt=".$_POST['searchtext']."&PayMethod=".$_POST['PayMethod']."");exit;
		}
	}

}

////manage Menu
if ($action == 'menu') {
 	$menuObj = new MenuDetail();    
	if($type == 'changestatus'){
		$menuObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$menuObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageMenu.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$menuObj->deleteAllValues($_POST);
		}
	}
	if($type == 'SORTORDER'){
		$menuObj->SortSequence($_GET);
	}
	if($type == 'setmenuorder'){
		$menuObj->setmenuorder($_GET);
	}
}





////////////////////////// Raw Products ///////////////////////
if ($action == 'colorSequence') {
 	$productObj = new Products();    
	if($type == 'SORTORDER'){
		$productObj->SortSequence($_GET);
	}
}









////////////////////////// Main Products ///////////////////////


if ($action == 'delMainImg') {
 	$productObj = new MainProducts();    
	if($type == 'deletecolorproduct'){
		$productObj->deleteMainImage($_GET);
		}
	
}

if ($action == 'deletemaincolorproduct') {
 	$productObj = new MainProducts();    
	if($type == 'deletecolorproduct'){
		$productObj->deleteMainColorImage($_GET);
		}
}


if ($action == 'uploadmainimg') {
 	$productObj = new MainProducts();    
	if($type == 'updatemain'){
		$productObj->updateMainColorProduct($_POST,$_FILES);
		}
}

if($action == 'editSize'){

   $productObj = new Products();    
	if($type == 'editSize'){
		$productObj->updateProductSize($_POST,$_FILES);
	}
}

if($action == 'editModelImage'){

   $productObj = new Products();    
	if($type == 'editModelImage'){
	//echo "Hi! There is no need to upload so many images...KISS..Keep is SUPER simple Baby....";
	//exit;
		$productObj->updateProductModelImages($_POST,$_FILES);
	}
}

if($action == 'editMainSize'){

   $productObj = new MainProducts();    
	if($type == 'editSize'){
		$productObj->updateProductSize($_POST);
	}
}


if($action=="orderstatus"){
	$orderstatusObj = new Orderstatus();    
		if($type == "delete"){
		$orderstatusObj->deleteValue($_GET);
		
		}
}


// Manage Design ................................................... 

if ($action == 'design') {
 	$categoryObj = new AssignMainProducts();    
	if($type == 'deleteall'){
		if($_POST['GO'] && $_POST['action'] == ""){
		header("Location:assign-main-product.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
		$categoryObj->deleteAllValues($_POST);
		}
	}
	if($type == 'assigntomainproduct'){
	$categoryObj->assigntomainproduct($_GET);
	}
}

// Manage Hot Design................................................... 

if ($action == 'hotdesign') {
 	$hotdesignObj = new HotDesign();    
	if($type == 'changestatus'){
		$hotdesignObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$attributeObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageHotDesign.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$hotdesignObj->deleteAllValues($_POST);
		}			
	}
}
if ($action == 'hotdesignFront') {
	$hotdesignObj = new HotDesign();
	if($type == 'changestatus'){
		$hotdesignObj->changeValueStatusFront($_GET);
	}
}

/////Manage community
if ($action == 'community') {
 	$comobj = new Pdfupload();    
	if($type == 'delete'){
		$comobj->deleteValue($_GET);
	}
	if($type == 'changestatus'){
		$comobj->changeValueStatus($_GET);
	}
	if($type == 'changeorderstatus'){
		$comobj->changeorderstatus($_GET);
	}
	if($type == 'deleteall'){
		if($_POST['Input']){
			$comobj->deleteAllValues($_POST);
		}else{
		header("Location:managePdf.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}

}

/// for pdf generate in order management
if ($action == 'PDF') {
 	$pdfObj = new GenPDF();    
	if($type == 'generatePDF'){
		$pdfObj->generatePDF($_GET['id']);
	}
	if($type == 'deletePDF'){
		$pdfObj->deletePDF($_GET['id']);
	}
}


/// manage shipping
if ($action == 'shipping') {
 	$viewObj = new Shipping();    
	if($type == 'changestatus'){
		$viewObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$viewObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manage-shipping-methods.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$viewObj->deleteAllValues($_POST);
		}
	}
}

if ($action == 'showDefaultState') {
	$generalSettingObj = new GeneralFunctions();	
	$generalSettingObj->getDefaultStateInDropdown($_GET['countryId']);
}
/////show or hide state/////
if ($action == 'statecheck') {
   	 $obj = new CountryShipping();  
 	 $obj->showStates($_GET);
 }

////////////////////manage discount
if ($action == 'CountryShipping') {
 	$obj = new CountryShipping();    
	if($type == 'delete'){
		$obj->deleteValue($_GET);
	}
	if($type == 'changestatus'){
		$obj->changeValueStatus($_GET);
	}
	 
	if($type == 'deleteall'){
		if($_POST['Input']){
			$obj->deleteAllValues($_POST);
		}else{
		header("Location:manageCountryShipping.php?searchtxt=".$_POST['searchtext']);exit;
		}
	}

}

if ($action == 'UnitPrice') {
  	$obj = new MainProducts();    
	if($type == 'delete'){
 		$obj->deleteUnitPrice($_GET);
	}
 }
 
 if ($action == 'UnitPrice1') {
  	$obj = new Products();    
	if($type == 'delete'){
 		$obj->deleteUnitPrice($_GET);
	}
 }
 
  



/// manage Shipping Ranges
if ($action == 'shippingRange') {
 	$shipObj = new ShippingRange();    
	if($type == 'changestatus'){
		$shipObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$shipObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			//header("Location:manageShipping.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$shipObj->deleteAllValues($_POST);
		}
	}
}

/// manage Ranges
if ($action == 'Range') {
 	$shipObj = new Range();    
	if($type == 'changestatus'){
		$shipObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$shipObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			//header("Location:manageShipping.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$shipObj->deleteAllValues($_POST);
		}
	}
	
	if($type == 'deleteRange'){
		$shipObj->deleteRangeValue($_GET);
	}

}

/// manage  Detail Ranges
if ($action == 'DetailRange') {

 	$shipObj = new ShippingRangeDetail();    
	if($type == 'changestatus'){
		$shipObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$shipObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			//header("Location:manageShipping.php?searchtxt=".$_POST['searchtext']);exit;
		}else{
			$shipObj->deleteAllValues($_POST);
		}
	}
	if($type == 'deleteRange'){
		$shipObj->deleteRangeValue($_GET);
	}

	if($type == 'getRangeDetail'){
		$shipObj->getRangeField($_GET);
	}

}

 //////////////////////////////// Manage Title ////////////////////////////////////
 
if ($action == 'title') {
 	$titleObj = new Title();    
	if($type == 'changestatus'){
		$titleObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$titleObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageTitle.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$titleObj->deleteAllValues($_POST);
		}			
	}
}

 //////////////////////////////////////End Title //////////////////////////
 
if ($action == 'checkUserTitle') {

	$userObj = new User();
	$userObj->checkUserTitle($_GET);
	//print_r($_GET);

}

if ($action == 'clipArt') {
	$clipObj = new ClipArt();
	$clipObj->showClipCat($_GET['id']);
	//print_r($_GET);
}

if ($action == 'valDetail') {
	$clipObj = new ClipArt();
	$clipObj->valDetail($_GET);
	//print_r($_GET);
}

// Manage Brand................................................... 

if ($action == 'brand') {
 	$brandObj = new Brand();    
	if($type == 'changestatus'){
		$brandObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$brandObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageBrand.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$brandObj->deleteAllValues($_POST);
		}			
	}
}

// Manage Hear About ................................................... 

if ($action == 'hearAbout') {
 	$hearAboutObj = new HearAbout();    
	if($type == 'changestatus'){
		$hearAboutObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$hearAboutObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if(!$_POST['Input']){
			header("Location:manageHearAbout.php?searchtxt=".$_POST['searchtext']);exit;
		} else {
			$hearAboutObj->deleteAllValues($_POST);
		}			
	}
}


// Manage Language Font ................................................... 

if ($action == 'languagefont') {
 	$categoryObj = new LanguageFont();    
	if($type == 'changestatus'){		
		$categoryObj->changeValueStatus($_GET);
	}
	if($type == 'delete'){
		$categoryObj->deleteValue($_GET);
	}
	if($type == 'deleteall'){
		if($_POST[Input]=='Go'){
			header("Location:manageLanguageFont.php?searchtxt=".$_POST['cid']);exit;
		}else{
			$categoryObj->deleteAllValues($_POST);
		}
	}
	if($type == 'deleteSingle'){
		$categoryObj->deleteAllSingleValues($_GET);
	}
	
}*/

?>