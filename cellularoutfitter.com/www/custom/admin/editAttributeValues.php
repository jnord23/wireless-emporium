<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageAttributeValues.php","edit_record");
/*---Basic for Each Page Ends----*/

$attrObj = new AttributeValues();
if(isset($_POST['submit'])) {	
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $attrObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $attrObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $attrObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('attributeValuesName_'.$value,$_POST['attributeValuesName_'.$value], 'req', 'Please Enter AttributeValues Name.');			
		}
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		foreach($langIdArr as $key=>$value) {
			$arr_error['attributeValuesName_'.$value]=$obj->fnGetErr($arr_error['attributeValuesName_'.$value]);
			if($arr_error['attributeValuesName_'.$value]) 
				$errorArr = 1;
		}	
		foreach($langIdArr as $key=>$value) {
			if($_POST['prev_attributeId'] != $_POST['attributeId'] || $_POST['attributeValuesName_'.$value] != $_POST['prev_attributeValuesName_'.$value]) {			
				if($attrObj->isAttributeValuesNameExist($_POST['attributeValuesName_'.$value],$value,$_POST['attributeId'],base64_decode($_GET['id'])))
				{ 
					$arr_error['attributeValuesName_'.$value] = "AttributeValues with this attribute already exist. ";
					if($arr_error['attributeValuesName_'.$value]) 
						$errorArr = 1;				
				}
			}
		}	
		

		if($errorArr == 0 && isset($_POST['submit'])){
			$_POST = postwithoutspace($_POST);	
					
			$attrObj->editRecord($_POST);			
		}
	}	
}

$result = $attrObj->getResult(base64_decode($_GET['id']));
//print_r($result);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

</head>
<body>
	<? include('includes/header.php'); ?>
	<div id="nav-under-bg"><!-- --></div>
	<form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" >
		<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">		
		<input type="hidden" name="page" value="<?=$_GET['page']?>">	
		<div class="main-body-div-new">
			<div class="main-body-div-header">Edit AttributeValues</div>
			<!-- left position -->
			<div class="main-body-div4" id="mainDiv">
				<div class="add-main-body-left-new" >
					<ul>
						<li class="add-main-body-left-new-text" style="clear:both; width:500px;" ><span class="small_error_message">
						<?=$_SESSION['SESS_MSG']?>
						</span></li>	
						<li class="lable">Choose Attribute <span class="spancolor">*</span></li>
						<li class="sap"><input type="hidden" name="prev_attributeId" value="<?=$result[attributeId]?>" > 
						<select name="attributeId" >
						<?	
						$genObj = new GeneralFunctions();
						echo $genObj->getAttributeList($result[attributeId]);
						?></select></li>	
						<li class="lable">AttributeValues Name <span class="spancolor">*</span></li>
						<?	$genObj = new GeneralFunctions();
						echo $genObj->getLanguageEditTextBox('attributeValuesName','m__attributeValuesName',TBL_ATTRIBUTE_VALUES_DESCRIPTION,base64_decode($_GET['id']),"attributeValuesId",$arr_error); //1->type,2->name,3->id,4->tablename,5->tableid
						?>
						<li class="lable">Price <span class="spancolor"></span></li>
						<li><input type="text" name="price" value="<?=$result[price] ?>" maxlength="4" /></li>
					</ul>
				</div>
				<div class="main-body-sub">
					<input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
					&nbsp;
					<input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack()"/>
				</div>
			</div>
		</div>
	</form>
	<? unset($_SESSION['SESS_MSG']); ?>
</body>
</html>