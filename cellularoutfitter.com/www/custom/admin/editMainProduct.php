<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageMainProducts.php","edit_record");
/*---Basic for Each Page Ends----*/


$mainProdObj = new MainProducts();
$genObj= new GeneralFunctions;
$result = $mainProdObj->getResult(base64_decode($_GET['id']));
$categoryIdArr=explode("-",$result->categoryId);
$categoryId=$categoryIdArr[1];
                         

/*---Update Record-------*/
if(isset($_POST['update'])) {
    //echo "<pre>"; print_r($post); echo "</pre>";exit;
    require_once('validation_class.php');
    $obj = new validationclass();
    $rst = $mainProdObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
    $num = $mainProdObj->getTotalRow($rst);	
    if($num){
        $langIdArr = array();		
        while($line = $mainProdObj->getResultObject($rst)) {	
            array_push($langIdArr,$line->id);
        }
        
        //Add Error===============
        foreach($langIdArr as $key=>$value) {
            $obj->fnAdd('productName_'.$value,$_POST['productName_'.$value], 'req', 'Please Enter Product Name.');
            $obj->fnAdd('productDesc_'.$value,$_POST['productDesc_'.$value], 'req', 'Please enter the Product Desciption.');
        }
        $obj->fnAdd('productPrice',$_POST['productPrice'], 'req', 'Please Enter Product Price.');
        $obj->fnAdd('productWeight',$_POST['productWeight'], 'req', 'Please Enter Product Weight.');
        $obj->fnAdd('quantity',$_POST['quantity'], 'req', 'Please Enter Product Quantity.');
        
        //Validate============
        $arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1; 
        
        //Get Error=============
        foreach($langIdArr as $key=>$value) {
            $arr_error['productName_'.$value]=$obj->fnGetErr($arr_error['productName_'.$value]); 
            $arr_error['productDesc_'.$value]=$obj->fnGetErr($arr_error['productDesc_'.$value]);  
        }
        $arr_error[productPrice]=$obj->fnGetErr($arr_error[productPrice]);
        $arr_error[productWeight]=$obj->fnGetErr($arr_error[productWeight]);
        $arr_error[quantity]=$obj->fnGetErr($arr_error[quantity]);
        if($str_validate){
            //echo "<pre>"; print_r($post); echo "</pre>";exit;
            $_POST = postwithoutspace($_POST);
            $mainProdObj->editRecord($_POST,$_FILES);
        }
        
    }
    
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/ajax.js"></script>
<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

<!-- Color Picker (END)	-->
<script type="text/javascript">
	function hrefBack1(){
		window.location.href='manageMainProducts.php?type=<?=$_GET[type]?>&page=<?=$_GET[page]?>';                
	}

</script>
<!--Light Box Starts-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--Light Box Ends -->

</head>
<body>
    <? include('includes/header.php'); ?>
    <div id="nav-under-bg"><!-- --></div>
    <div class="main-body-div-new">
        <div class="main-body-div-header">Edit Main Product</div>
        <div class="main-body-div4" id="mainDiv">
            <div align="center"><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
            <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data" >
                <input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">
                <input type="hidden" name="page" value="<?=$_GET['page']?>">
                <div>
                    <div class="add-main-body-left-new" >
                        <ul >
                            <li class="add-main-body-left-new-text" style="clear:both; width:500px;" >
                                <span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span>
                            </li>
                            
                            
                            <!-- Category---------->                            
                            <li class="lable">Category <span class="spancolor">*</span></li>
                            <li class="lable2">
                            <select name="cat" id="cat" onchange="getSubCatByCatId(this.value);">
                            <?=$genObj->getCategoryList($categoryId);?>
                            </select>
                            </li>
                            <!-- Sub Category ------------>                            
                            <li class="lable">Sub Category <span class="spancolor">*</span></li>
                            <li class="lable2" >
                            <select name="subCat" id="subCat" >
                            <?					
                            echo $genObj->subCategoryList($categoryId,$result->subCatId);
                            ?>
                            </select>
                            <span id="subCatSPAN"></span>
                            </li>  
                            
                            <!-- Product Name================= -->
                            <li class="lable">Product Name <span class="spancolor">*</span></li>
                            <?=$genObj->getLanguageEditTextBox('productName','m__productName',TBL_MAIN_PRODUCT_DESCRIPTION,base64_decode($_GET['id']),"productId",$arr_error);?>
                            
                            <!-- Product Price======================= -->						
                            <li class="lable">Product Price <span class="spancolor">*</span></li>
                            <li>
                            <input type="text" name="productPrice" id="m__Product_Price" class="wel" value="<?=$result->productPrice?>"  maxlength="8" onkeyup="return isNum12(this.value);" />
                            <p style="padding-left:150px;" id="price" ><?=$arr_error[productPrice]?></p>	
                            </li>
                            <!-- Product Weight========================= -->
                            <li class="lable">Product Weight <span class="spancolor">*</span></li>
                            <li>
                            <input type="text" name="productWeight" id="m__Product_Weight" class="wel" value="<?=$result->productWeight?>"  maxlength="8" onkeyup="return isNum123(this.value);" />
                            <p style="padding-left:150px;" id="weight" ><?=$arr_error[productWeight]?></p>
                            </li>

                            <!-- Quantity==================== -->
                            <li class="lable">Product Quantity </li>
                            <li>
                            <input type="text" name="quantity" class="wel" value="<?=$result->quantity?>"  />
                            <p style="padding-left:150px;" id="price" ><?=$arr_error[quantity]?></p>
                            </li>

                            <!-- Description========================== -->
                            <li class="lable">Product Description <span class="spancolor">*</span></li>
                            <?
                            echo $genObj->getLanguageEditTextarea('productDesc','m__Product_Description',TBL_MAIN_PRODUCT_DESCRIPTION,base64_decode($_GET['id']),"productId",$arr_error); 
                            ?>
                            <li>&nbsp;</li>	
                            <!-- Product Image--- -->
                            <?
                            $img=$genObj->fetchValue(TBL_MAIN_PRODUCT_VIEW,'imageWithBorder',"product_id='".$result->id."' and isDefault='1'");  
                            ?>
                            <li class="lable" >Product image <span class="spancolor">*</span></li>
                            <li><a href="<?=__MAINPRODUCTEXTLARGEPATH__.$img?>" rel="shadowbox"><img src="<?=__MAINPRODUCTTHUMBPATH__.$img?>" border="0"></a></li>
				
                        </ul>
                    </div>
                    
                </div>
                    <!-- Update Button========================== -->
			<div class="main-body-sub">
				<input type="submit" name="update" class="main-body-sub-submit" style="cursor:pointer;" value="Update" />
				&nbsp;
				<input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
			</div>
            </form>
        </div>
    </div>
    
        

</body>
</html>
