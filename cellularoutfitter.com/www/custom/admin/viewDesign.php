<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageDesign.php","");
/*---Basic for Each Page Ends----*/
$designObj = new Design();
$genObj = new GeneralFunctions();
$id=base64_decode($_GET['id']);
$categoryId=$designObj->fetchValue(TBL_DESIGN,'catId',"id='".$id."'");
$catname=stripslashes($designObj->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,'categoryName',"catId='".$categoryId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'"));
$designName=stripslashes($designObj->fetchValue(TBL_DESIGN_DESC,'designName',"designId='".$id."'"));
$image=$designObj->fetchValue(TBL_DESIGN,'designImage',"id='".$id."'");
$largeImg=__DESIGNLARGEPATH__.$image;
$thumbImg=__DESIGNTHUMBPATH__.$image;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>
<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->


<!--Start : Light Box============================-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--End : Light Box====================================-->
</head>
<body>
    <div class="main-body-div-header">View Detail</div>
    <div class="main-body-div4" id="mainDiv">
        <div align="center"><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
        <div>
            <div class="add-main-body-left-new" >
                <ul>
                    <li class="add-main-body-left-new-text" style="clear:both; width:500px;" >
                        <span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span>
                    </li>
                    <!-- Design Category ================= -->
                    <li class="lable"><b>Design Category <span class="spancolor"></span></b></li>
                    <li class="lable2">  
                    <?=$catname?>
                    </li>
                    <!-- Desgin Name===================== -->
                    <li class="lable"><b>Design Name <span class="spancolor"></span></b></li>
                    <?=$genObj->getLanguageViewTextarea('designName',TBL_DESIGN_DESC,$id,"designId");?>
                </ul>
            </div>
        </div>
        <div style="clear:both; float:left;">
            <div id="divProduct" style="height:15px;text-align:center;font-weight:bold; color:#FF0000;"></div>
            <fieldset style="margin:0px 0px 0px 20px ; font-weight:normal; width:630px; height:400px">
                <legend style="color:#000000;"><b>Design Image Detail</b></legend>
                <p><b> Design Image</b></p>                
                <p style="padding-left:140px;"><span><a href="<?=$largeImg?>" rel="shadowbox"><img src="<?=$thumbImg?>" /></a></span></p>
            </fieldset>
        </div>
    </div>
</body>
</html>