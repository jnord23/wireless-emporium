<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCategory.php","");
/*---Basic for Each Page Ends----*/

$attrObj = new AttributeValues();
$result = $attrObj->getResult(base64_decode($_GET['id']));

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

</head>
<body>
          <div class="main-body-div-header">View Detail</div>
		  <!-- left position -->
        
            <div class="main-body-div4" id="mainDiv">
              <div class="add-main-body-left-new" style="position:relative;">
                <ul>
                  <li class="add-main-body-left-new-text" style="clear:both; width:500px;" ><span class="small_error_message">
                    <?=$_SESSION['SESS_MSG']?>
                  </span></li>
				  <li class="lable">Attribute </li>
                  <li class="sap"><?=$attrObj->fetchValue(TBL_ATTRIBUTE_DESCRIPTION,"attributeName","1 and attributeId = '".$result[attributeId]."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'")?></li>
				  <li>&nbsp;</li>
							
                  <li class="lable">AttributeValues Name: </li>                 
                    <?	$genObj = new GeneralFunctions();
						echo $genObj->getLanguageViewTextBox('attributeValuesName',TBL_ATTRIBUTE_VALUES_DESCRIPTION,base64_decode($_GET['id']),"attributeValuesId"); //1->type,2->name,3->id,4->tablename,5->tableid
					?>
				<li class="add-main-body-left-new-text" style="clear:both;">&nbsp;</li>
                  <li class="add-main-body-right-new-text"></li>	

                </ul>
              </div>
              <div class="main-body-sub">&nbsp;</div>
            </div>
</body>
</html>