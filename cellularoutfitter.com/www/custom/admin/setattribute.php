<?php
session_start();
require_once("includes/function/autoload.php");
require_once("includes/classes/Language.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$languageObj = new Language();
$generalFunctionsObj = new GeneralFunctions();

$_SESSION[languageset]=$_SERVER['REQUEST_URI'];
if(isset($_POST['submit'])){
    $languageObj->addlanguage();
}
if($_POST['langId']){
    $row1 =$languageObj->choselanguage($_POST['langId']);	
    if($row1['languageName'] == "English"){
        $charset = "iso-8859-1";
    }else{
        $charset = "UTF-8";
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$charset ?>" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/validation.js"></script>
<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<!-- Menu head -->
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
</head>
<body>

<? include('includes/header.php'); ?>
<div id="nav-under-bg"><!-- --></div>

<div class="main-body-div-width">
	<div class="main-body-div-header">
	<div class="main-body-header-text-top">Language Translation</div>
	</div>
	<div>&nbsp;</div>
	<div>
	<div  align="center" ><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
	 <div align="center">	  
      <form name="banner" method="POST" action="">
    <select name='langId' id="langId" ><?=$generalFunctionsObj->getLanguageInDropDown($_POST['langId']); ?></select>
          <input type="submit" name="sellanguage" value="Go">
    </form>
	</div>
	<div align="center" style="padding:10px;font-size:14px;font-weight:bold;">
	<?php if($_POST['langId']){ ?>
	Set Attribute &nbsp;&nbsp; You have selected :<?  
   $row =$languageObj->choselanguage($_POST['langId']);
   echo $row['languageName'] ." language";
   }else{ echo "Please Select Language.";} ?>
 </div>
 
<?php
if(!empty($row)){
	echo $tableVal=$languageObj->fetattribute($_POST['langId']);
}
?>
</div>	
</div>

</body>
</html>
<? unset($_SESSION['RESTORE_MSG'])?>