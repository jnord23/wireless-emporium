<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageTool.php","");
/*---Basic for Each Page Ends----*/
$toolObj = new Tool();
$genObj = new GeneralFunctions();
$result = $toolObj->getResult(base64_decode($_GET['id']));

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

</head>
<body>
    <div class="main-body-div-header">View Tool</div>
    <div class="main-body-div4" id="mainDiv">
        <div class="add-main-body-left-new" style="position:relative;">
            <ul>
                <!-- Variable------------->		
                <li class="lable">Variable: </li>
                <li class="lable2"><?=$result->variableName;?></li>              
                
                <!-- Name --------->
                <li class="lable">Name: </li>
                <?=$genObj->getLanguageViewTextBox('toolName',TBL_TOOLDESC,base64_decode($_GET['id']),"id");?>
            </ul>
        </div>
        <div class="main-body-sub">&nbsp;</div>
    </div>
</body>
</html>