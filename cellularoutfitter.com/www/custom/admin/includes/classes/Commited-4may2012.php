<?php 
session_start();
require_once("includes/classes/DB.php");
require_once("includes/classes/MySqlDriver.php");
require_once("config/configure.php");
require_once("config/tables.php");
require_once("includes/classes/createZip.inc.php");
class Commited extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }
	
	function valDetail() {	
	$genTable ='';
	$genTable .='<div class="main-body-sub" style="margin-left:350px;">
    				<input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Commit" />
    	 			</div>';		
	return $genTable;
	}
	
//Update Commit===========================================================================================================
    function updateCommit(){
        $query = mysql_query("select id from ".TBL_COMMITLOG." where status='0'");
        while($sd=mysql_fetch_object($query)){
            $date=date(ymdhis);
            $statusquery=mysql_query("UPDATE ".TBL_COMMITLOG." SET status = '1', commitDate=".$date." WHERE id=".$sd->id." ");
        }
    }
// Start : Create Directories========================================================================================
function createDirectory($dirName){    
    if(!is_dir($dirName)) {
        @mkdir($dirName);
        @chmod($dirName,0777);
    }
} 

//Start : Generate XML========================================================================================
function generateXML($get = NULL){	
    //Backuup=================================
    /*
    $destinationfolder = __BASE_DIR_FRONT__."xmlBackUp/";   
    $this->createDirectory($destinationfolder);
    $folderName = date("Y-m-d h-i-s");               
    $createZip = new createZip;
    $zipFileName = $folderName.".zip";
    $directoryName = $destinationfolder.$ordZipFileName;                
    $this->createDirectory($destinationfolder.$folderName, 0700);                
    $imageextention = "xml"; /// image extention in folder
    
    foreach (glob($sourcefolder."*.".$imageextention) as $filename) {
        $imagename = str_replace($sourcefolder, "", $filename);
        copy($sourcefolder.$imagename, $destinationfolder."/".$folderName."/".$imagename);			
        $fileContents = file_get_contents($sourcefolder.$imagename);  
        $createZip -> addFile($fileContents, $directoryName); 		

    }
    
    $fileName = $destinationfolder.$zipFileName.".zip";
    $fd = fopen ($fileName, "wb");
    $out = fwrite ($fd, $createZip ->getZippedfile());
    fclose ($fd);
    */
    //Update Commit TBL================================================================
    $this->updateCommit();

    $query = "select * from ".TBL_LANGUAGE." where 	isDeleted = '0' and status = '1'";			
    $sql = $this->executeQry($query);
    $num = $this->getTotalRow($sql);
    if($num > 0) {
        $i = 1;			
        while($line = $this->getResultObject($sql)) {				
            $dirPath = __DIR_XML__;
            $this->createDirectory($dirPath);
            $xmlfileArr = $this->getXmlDataArr();
            foreach($xmlfileArr as $key=>$value){
                $xmlFilePath = __DIR_XML__.$key.".xml";
                @unlink($xmlFilePath);
                $xmlFileHandle = fopen($xmlFilePath, 'w') or die("can't open file");
                fwrite($xmlFileHandle, $value);
                fclose($xmlFileHandle);
            }
            $i++;	
        }
    }
    $_SESSION['SESS_MSG'] =  msgSuccessFail("success","xml has been generated successfully.");
    header("Location:manageCommit.php");
    exit;
}

//Start : Get XML Data Array===========================================	
    function getXmlDataArr(){
        $content = $this->getContent();
        $font = $this->getFont(); 
        $data = $this->getData();
        $cliparts = $this->getCliparts();    
        $TextColors = $this->getTextColors();   
        $sendArr = array('Fonts'=>$font,'Contents'=>$content,'Products'=>$data,'Cliparts'=>$cliparts,'TextColors'=>$TextColors);       
        return $sendArr;
    }


//Start : Get Content======================================================================================Done by PK
    function getContent() {
        //Dynamic==========================================       
        $contentQuery = "SELECT TBL1.*, TBL2.toolName FROM ".TBL_TOOL." AS TBL1 
        INNER JOIN ".TBL_TOOLDESC." AS TBL2 ON (TBL1.id = TBL2.Id) 
        WHERE TBL1.status = '1' AND TBL1.isDeleted = '0' AND TBL2.langId = '".$_SESSION['DEFAULTLANGUAGE']."' ";
        $contentSql = $this->executeQry($contentQuery);
        $contentNum = $this->getTotalRow($contentSql);
        if($contentNum){ 
            $tbl ='';
            $tbl .= '<?xml version="1.0" encoding="utf-8"?>            
            <content>	';
            while($tagData = $this->getResultObject($contentSql)) {
                $tbl .= '<'.$tagData->variableName.' value="'.stripslashes(($tagData->toolName)).'"/>';
            }
            $tbl .= '</content>';
        }       
        
        //$tbl=$this->getStaticContent();
        return $tbl; 
    }
    
//Start : Get Font============================================================================================== Done By PK
    function getFont() {
        $query = "SELECT * FROM ".TBL_FONT." as F INNER JOIN ".TBL_FONTDESC." AS FD ON (F.id = FD.fontId) WHERE F.status = '1' AND FD.fontType = 'normal' GROUP BY FD.fontId  ORDER BY F.isDefault DESC";
        
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $tbl 	= '';
        if($num > 0) {
            $tbl .='<?xml version="1.0" encoding="utf-8"?>
            <fonts>';
            while($line = $this->getResultObject($sql)) {
                $tbl .='<font>
                <id>'.$line->fontId.'</id>
                <label></label>	
                <style>'.htmlspecialchars(stripslashes($line->fontName)).'</style>
                <swf>files/font/ttf/'.$line->fontSWF.'</swf>
                <bold>'.$this->isExistFont(TBL_FONTDESC,$line->fontId,'bold').'</bold>
                <italic>'.$this->isExistFont(TBL_FONTDESC,$line->fontId,'italic').'</italic>
                <url>files/font/original/'.$line->imageName .'</url>
                <status>'.$line->status.'</status>
                </font>';
            } 
            $tbl .='</fonts>';
        }      
        return $tbl;
    } 
    
//Start : Get Data=========================================================
    function getData() {        
        $query="SELECT C.*,CD.categoryName,CD.description FROM ".TBL_CATEGORY." AS C INNER JOIN ".TBL_CATEGORY_DESCRIPTION." AS CD  ON (C.id=CD.catId) WHERE 1 AND C.parent_id='0' AND C.isDeleted='0' AND C.status='1' AND CD.langId='".$_SESSION['DEFAULTLANGUAGE']."'";
        $rst = $this->executeQry($query);
        $num = $this->getTotalRow($rst);
        $tbl 	= '';
        if($num > 0) {
            $tbl .='<?xml version="1.0" encoding="utf-8"?>
            <MainXml><products>';
            while($line = $this->getResultObject($rst)) {
                $name=htmlspecialchars(stripslashes($line->categoryName));
                $desc=htmlspecialchars(stripslashes($line->description));
                $img='files/category/thumb/'.$line->categoryImage;
                $tbl .='<product id="'.$line->id.'" title="'.$name.'" subTitle="'.$desc.'" source="'.$img.'" >';
                $tbl .= $this->getSubCatByCatId($line->id);
                $tbl .='</product>';
            }
            $tbl .='</products></MainXml>';
        }  
        
        return $tbl; 
    }

    

    
//Start : Get Company Device========================================================
    function getSubCatByCatId($id) {
        $query="select c.*,cd.categoryName from ".TBL_CATEGORY." as c , ".TBL_CATEGORY_DESCRIPTION." as cd where 1 and c.id = cd.catId and c.parent_id = '".$id."' and c.isDeleted = '0' and c.status = '1' and cd.langId = '".$_SESSION['DEFAULTLANGUAGE']."' ";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $tbl 	= '';
        if($num > 0) {
            while($line = $this->getResultObject($sql)) { 
                $catName=htmlspecialchars(stripslashes($line->categoryName));
                $img='files/category/thumb/'.$line->categoryImage;
                $tbl .='<brand id="'.$line->id.'" title="'.$catName.'" source="'.$img.'">';		
                $tbl .= $this->getProductDetailBySubCatId($line->id);
                $tbl .='</brand>';
            } 
        }
        return $tbl; 			 
    }
// Start : Get Product Company in Device====================================================
    function getProductDetailBySubCatId($id) {
        $query = "SELECT P.*, PD.productName FROM ".TBL_PRODUCT." AS P INNER JOIN ".TBL_PRODUCT_DESCRIPTION." AS PD ON (P.id = PD.productId) WHERE P.isDeleted = '0' AND P.status = '1' AND PD.langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND P.subCatId ='".$id."'";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $tbl 	= '';
        if($num > 0) {
            while($line = $this->getResultObject($sql)) {
                $name= stripslashes(trim(($line->productName)));
               
                $img='files/product/thumb/'.$line->productImage;
                $tbl .='<brandset id="'.$line->id.'" title="'.$name.'" source="'.$img.'">';               
                $tbl .= $this->getProdcuctViewDetailByProductId($line->id);               
                $tbl .='</brandset>';
            } 
        }
        return $tbl; 	
    }
//Start : Get Product View of Company In Device======================================

    function getProdcuctViewDetailByProductId($pId) {
        $query = "SELECT TBL1.*, TBL3.viewName FROM ".TBL_PRODUCT_VIEW." AS TBL1 INNER JOIN ".TBL_VIEW." AS TBL2 ON (TBL1.view_id = TBL2.id) INNER JOIN ".TBL_VIEWDESC." AS TBL3 ON (TBL2.id = TBL3.viewId) WHERE  TBL2.status = '1' AND TBL3.langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND TBL1.product_id ='".$pId."'";
        //echo $query;exit;
        $rst = $this->executeQry($query);
        $num = $this->getTotalRow($rst);             
        if($num > 0) {
            $tbl .= '<views>';             
            while($line = $this->getResultObject($rst)) {                
                $img='files/product/large/'.$line->imageWithBorder;
                $tbl .='<view id="'.$line->view_id.'" title="'.$line->viewName.'" source="'.$img.'" printH="10" printW="16"/>';

            } 
            $tbl .= '</views>';
        }
        return $tbl; 	
    }

    
    //Start : Get Get Cliparts=========================================================
    function getCliparts() {        
        $query="select c.*,cd.categoryName from ".TBL_DESIGNCATEGORY." as c , ".TBL_DESIGNCATEGORY_DESCRIPTION." as cd where 1 and c.id = cd.catId and c.status = '1' and c.isDeleted = '0' and cd.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and c.parent_id = '0'";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        if($num > 0) {
            $tbl = '';
            $tbl .='<?xml version="1.0" encoding="utf-8"?>
            <images>';
            $k=0;
            while($line = $this->getResultObject($sql)) { 
                $catName=htmlspecialchars(stripslashes($line->categoryName));
                $tbl .='<image id="'.$k.'" type="'.$catName.'" name="'.$catName.'" imgType="cliparts">';
                $tbl .= $this->getDesignsInCategory($line->id);
                $tbl .='</image>'; 	  
                $k++;
            }
            $tbl .='</images>'; 
        }	
        //$tbl=$this->getStaticClipartXml();
        return $tbl;
    }
//Start : Get Design In Category================================================
    function getDesignsInCategory($catId) {
        $query="SELECT TBL1.*,TBL2.designName FROM ".TBL_DESIGN." AS TBL1, ".TBL_DESIGN_DESC." AS TBL2 WHERE TBL1.id=TBL2.designId AND TBL1.isDeleted = '0' AND TBL2.langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND TBL1.status = '1' AND TBL1.catId  = '".$catId."'";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $tbl ='';
        if($num > 0) {           
            while($line = $this->getResultObject($sql)) {                               
                $orgImage = __DESIGNORIGINAL__.$line->designImage;               
                list($maxW, $maxH) = getimagesize($orgImage);
                $designName=htmlspecialchars(stripslashes($line->designName));
                $thumbSrc='files/design/thumb/'.$line->designImage;
                $mainSrc='files/design/large/'.$line->designImage;
                echo $tbl .='<subimage name="'.$designName.'"   thumbsrc="'.$thumbSrc.'" mainsrc="'.$mainSrc.'"  imageId="'.$line->id.'"  maxH="'.$maxH.'" maxW="'.$maxW.'" />';            
              	  
            }
        }	  
        return $tbl; 
    }	
		    
//Start : Check Font Exists========================================================
    function isExistFont($table,$fontId,$column){
        $query = "SELECT * FROM $table WHERE fontType = '".$column."' AND fontId ='$fontId'";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        return ($num > 0)?'true':'false';
    }
    
  // Start : Get Text Colors=======================================================
    function getTextColors(){
        $query = "SELECT C.*, CD.colorName FROM ".TBL_COLOR." AS C INNER JOIN ".TBL_COLORDESC." AS CD ON (C.id=CD.id) WHERE C.isDeleted='0' AND C.status='1' AND CD.langId='".$_SESSION['DEFAULTLANGUAGE']."'";
        $rst = $this->executeQry($query);
        $num = $this->getTotalRow($rst);       
        if($num > 0) {
            $tbl .='<?xml version="1.0" encoding="utf-8"?>
            <colors>';
            while($line = $this->getResultObject($rst)) {
                $tbl .='<color><id>'.$line->id.'</id><color>'.$line->colorCode.'</color><title>'.$line->colorName.'</title></color>';
            } 
            $tbl .='</colors>';
        }      
        return $tbl;
    }
 //Start : Get Static content=============================================
    function getStaticContent(){
        $tbl='<?xml version="1.0" encoding="utf-8"?>
                <content>
                  <productTitle value="Products"/>
                  <uploadTitle value="Upload"/>
                  <addImgTitle value="Our Gallery"/>
                  <addTextTitle value="Add Text"/>
                  <previewTitle value="Preview"/>
                  <cartTitle value="Add To Cart"/>

                  <topHelpStep1 value="STEP 1"/>
                  <topHelpProTitle value="CHOOSE YOUR DEVICE"/>
                  <topHelpStep2 value="STEP 2"/>
                  <topHelpUploadTitle value="ADD YOUR FAVOURITE IMAGES AND PHOTOS"/>
                  <topHelpStep3 value="STEP 3"/>
                  <topHelpTextTitle value="ADD TEXT (OPTIONAL)"/>
                  <topHelpStep4 value="STEP 4"/>
                  <topHelpPreviewTitle value="PREVIEW YOUR DESIGN"/>
                  <topHelpStep5 value="STEP 5"/>
                  <topHelpCartTitle value="PURCHASE YOUR SKIN"/>


                  <backColorTitle value="Background Colour"/>


                  <cropUTILabel value="If you want to use this image without cropping, then click on "/>
                  <cropUTIBtnLabel value="Use This Image"/>
                  <cropMsg1 value="Crop Your image using the handler to resize your area."/>
                  <cropMsg2 value="Drag the masked area to position the crop when you are satisfied, then select the Crop &amp; Save button below"/>
                  <cropAndSaveBtnLabel value="Crop &amp; Save"/>
                  <cropCancelBtnLabel value="Cancel"/>
                  <cropImageLoadAlert value="Loading Cropped Image.."/>
                  <croppedImgLoadMsg value="Please Wait.."/>

                  <zoomTitle value="Zoom"/>
                  <deleteTitle value="Delete"/>
                  <scaleTitle value="Scale"/>
                  <rotateTitle value="Rotate"/>
                  <layerTitle value="Layer Order"/>
                  <heightTitle value="Height"/>
                  <widthTitle value="Width"/>
                  <imgLoadTitle value="Image is Loading..."/>
                  <xmlLoadingMsg value="Loading Data..."/>
                  <waitMsg value="Please Wait..."/>
                  <errTitle value="Error"/>
                  <undoTitle value="Undo"/>
                  <redoTitle value="Redo"/>
                  <errImageMsg value="Please browse for Image File first!!"/>
                  <closeLabel value="close"/>
                  <errLargeImgLoad value="Image Loading Error..."/>
                  <errImageExists value="Image Already Exists.."/>
                  <msgAlert value="Message Alert"/>
                  <cartFirstMsg value="Please be patient once you click yes, it may take a little time,thanks."/>
                  <cartMsg value="Very Nice! Do you want to purchase this design?"/>
                  <yesTitle value="Yes"/>
                  <noTitle value="No"/>
                  <uploadingDataMsg value="Uploading Image..."/>
                  <uploadingDataMsgError value="Uploading Error..."/>
                  <reselection value="Re-selection of same device"/>
                  <chooseNewDevice value="Want to choose a new Device?"/>
                  <browseLabel value="Browse"/>
                  <upImgLabel value="Upload Image"/>
                  <uploadInfoText value="*For best results use high quality .jpg or .png images. When uploading large images please be patient as it may take some time."/>
                  <agreeToTermsText value="I have the right to use this image and it does not violate the terms of use."/>
                  <uploadWinLabel value="Upload From My Computer"/>
                  <yourImgStore value="Your Images Store"/>
                  <browseImg value="Browse Images"/>

                  <buynowLabel value="BUY NOW"/>
                  <gobackLabel value="GO BACK"/>

                  <addTextLabel value="Add Text"/>
                  <addTextHereTI value="ADD TEXT HERE"/>
                  <addTextFontType value="Font"/>
                  <addTextFontColor value="Font Color"/>
                  <colorPickerBackColorTitle value="Background Colour"/>
                  <cancelLabel value="Cancel"/>
                  <okLabel value="Ok"/>
                  <savingDesignLabel value="Saving Design.."/>
                  <alertLabel value="Alert !!"/>

                  <deleteItemTitle value="Delete Item"/>
                  <deleteItemMsg value="Do you want to delete this Item?"/>
                  <deleteBtnLabel value="Delete"/>
                  <uploadingImgLabel value="Upoading image.."/>
                  <backBtnLabel value="Back"/>
                  <imagesLoadingTitle value="Images(s) Loading.."/>

                  <saveLabelBtn value="Save"/>
                  <buyDesignLabel value="Buy"/>
                  <saveTitle value="Save Design"/>
                  <saveVeryNice value="Very Nice! Do you want to save this design?"/>
                  <thankYouLabel value="Thank You"/>
                  <yourDesignSaved value="Your design is saved, You can continue designing.."/>
                  <processingLabel value="Processing.."/>
                  <designNotSaved value="Design Not Saved"/>
                </content>';
                return $tbl;
            }
    
    function getStaticClipartXml(){
        $tbl='<?xml version="1.0" encoding="utf-8"?>
                <images>
                  <image id="0" type="Cartoon" name="Cartoon" imgType="cliparts">
                    <subimage name="StockImages1" thumbsrc="files/design/stock/thumb/1.jpg" mainsrc="files/design/stock/large/1.jpg" imageId="11" maxH="250" maxW="250"/>
                    <subimage name="StockImages2" thumbsrc="files/design/stock/thumb/2.jpg" mainsrc="files/design/stock/large/2.jpg" imageId="12" maxH="400" maxW="400"/>
                    <subimage name="StockImages3" thumbsrc="files/design/stock/thumb/3.jpg" mainsrc="files/design/stock/large/3.jpg" show="true" imageId="13" maxH="350" maxW="350"/>
                    <subimage name="StockImages4" thumbsrc="files/design/stock/thumb/4.jpg" mainsrc="files/design/stock/large/4.jpg" show="true" imageId="14" maxH="400" maxW="400"/>
                    <subimage name="StockImages5" thumbsrc="files/design/stock/thumb/5.jpg" mainsrc="files/design/stock/large/5.jpg" show="true" imageId="15" maxH="450" maxW="450"/>
                    <subimage name="StockImages6" thumbsrc="files/design/stock/thumb/6.jpg" mainsrc="files/design/stock/large/1.jpg" show="false" imageId="16" maxH="500" maxW="500"/>
                    <subimage name="StockImages7" thumbsrc="files/design/stock/thumb/7.jpg" mainsrc="files/design/stock/large/2.jpg" show="false" imageId="17" maxH="3000" maxW="3000"/>
                    <subimage name="StockImages8" thumbsrc="files/design/stock/thumb/8.jpg" mainsrc="files/design/stock/large/3.jpg" show="false" imageId="18" maxH="500" maxW="500"/>
                    <subimage name="StockImages9" thumbsrc="files/design/stock/thumb/9.jpg" mainsrc="files/design/stock/large/4.jpg" show="false" imageId="19" maxH="500" maxW="500"/>
                  </image>

                  <image id="1" type="Kids" name="Kids Images" imgType="cliparts">
                    <subimage name="KidsImages1" thumbsrc="files/design/kids/thumb/1.jpg" mainsrc="files/design/kids/large/1.jpg" show="true" imageId="20" maxH="250" maxW="250"/>
                    <subimage name="KidsImages2" thumbsrc="files/design/kids/thumb/2.jpg" mainsrc="files/design/kids/large/2.jpg" show="true" imageId="21" maxH="400" maxW="400"/>
                    <subimage name="KidsImages3" thumbsrc="files/design/kids/thumb/3.jpg" mainsrc="files/design/kids/large/3.jpg" show="true" imageId="22" maxH="350" maxW="350"/>
                    <subimage name="KidsImages4" thumbsrc="files/design/kids/thumb/4.jpg" mainsrc="files/design/kids/large/4.jpg" show="true" imageId="23" maxH="400" maxW="400"/>
                    <subimage name="KidsImages5" thumbsrc="files/design/kids/thumb/5.jpg" mainsrc="files/design/kids/large/5.jpg" show="true" imageId="24" maxH="450" maxW="450"/>
                    <subimage name="KidsImages6" thumbsrc="files/design/kids/thumb/6.jpg" mainsrc="files/design/kids/large/6.jpg" show="false" imageId="25" maxH="500" maxW="500"/>
                    <subimage name="KidsImages7" thumbsrc="files/design/kids/thumb/7.jpg" mainsrc="files/design/kids/large/7.jpg" show="false" imageId="26" maxH="3000" maxW="3000"/>
                    <subimage name="KidsImages8" thumbsrc="files/design/kids/thumb/8.jpg" mainsrc="files/design/kids/large/8.jpg" show="false" imageId="27" maxH="500" maxW="500"/>
                    <subimage name="KidsImages9" thumbsrc="files/design/kids/thumb/9.jpg" mainsrc="files/design/kids/large/9.jpg" show="false" imageId="29" maxH="500" maxW="500"/>
                    <subimage name="KidsImages9" thumbsrc="files/design/kids/thumb/10.jpg" mainsrc="files/design/kids/large/10.jpg" show="false" imageId="30" maxH="500" maxW="500"/>
                  </image>

                  <image id="2" type="Clipart" name="Music Icons" imgType="cliparts">
                    <subimage name="MusicIcons1" thumbsrc="files/design/clipart/thumb/1.jpg" mainsrc="files/design/clipart/large/1.png" show="true" imageId="31" maxH="250" maxW="250"/>
                    <subimage name="MusicIcons2" thumbsrc="files/design/clipart/thumb/2.jpg" mainsrc="files/design/clipart/large/2.png" show="true" imageId="32" maxH="400" maxW="400"/>
                    <subimage name="MusicIcons3" thumbsrc="files/design/clipart/thumb/3.jpg" mainsrc="files/design/clipart/large/3.png" show="true" imageId="33" maxH="350" maxW="350"/>
                    <subimage name="MusicIcons4" thumbsrc="files/design/clipart/thumb/4.jpg" mainsrc="files/design/clipart/large/4.png" show="true" imageId="34" maxH="400" maxW="400"/>
                    <subimage name="MusicIcons5" thumbsrc="files/design/clipart/thumb/5.jpg" mainsrc="files/design/clipart/large/5.png" show="true" imageId="35" maxH="450" maxW="450"/>
                    <subimage name="MusicIcons6" thumbsrc="files/design/clipart/thumb/6.jpg" mainsrc="files/design/clipart/large/6.png" show="false" imageId="36" maxH="500" maxW="500"/>
                    <subimage name="MusicIcons7" thumbsrc="files/design/clipart/thumb/7.jpg" mainsrc="files/design/clipart/large/7.png" show="false" imageId="37" maxH="3000" maxW="3000"/>
                    <subimage name="MusicIcons8" thumbsrc="files/design/clipart/thumb/8.jpg" mainsrc="files/design/clipart/large/8.png" show="false" imageId="38" maxH="500" maxW="500"/>
                    <subimage name="MusicIcons9" thumbsrc="files/design/clipart/thumb/9.jpg" mainsrc="files/design/clipart/large/9.png" show="false" imageId="39" maxH="500" maxW="500"/>
                    <subimage name="MusicIcons9" thumbsrc="files/design/clipart/thumb/10.png" mainsrc="files/design/clipart/large/10.png" show="false" imageId="40" maxH="500" maxW="500"/>
                  </image>
                </images>';
            return $tbl;
    }
}// END OF CLASS=====================================================

	  
?>