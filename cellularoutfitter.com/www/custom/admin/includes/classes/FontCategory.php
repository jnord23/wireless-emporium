<?php 
session_start();
class FontCategory extends MySqlDriver{
//Start : Constructor=========================================================
	function __construct(){
		$this->obj = new MySqlDriver;      
	}
//Start : Show Grid ========================================================
	function valDetail($cid) {
		$cid  = $cid?$cid:0;
		$cond = "1 and c.id = cd.catId and c.parent_id = $cid and c.isDeleted = '0' and cd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
                if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){			
			$searchtxt = addslashes($_REQUEST['searchtxt']);
			$cond .= " AND (cd.categoryName LIKE '%$searchtxt%')";
			stripslashes($_REQUEST['searchtxt']);	
		}
		$query = "select c.*,cd.categoryName from ".TBL_FONTCATEGORY." as c , ".TBL_FONTCATEGORY_DESCRIPTION." as cd where $cond ";                
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------		
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"sequence";
			$order = $_GET[order]? $_GET[order]:"ASC";   
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);			
			if($row > 0) {			
				$i = 1;	
				$genTable .= '<div class="column" id="column1">';			
				while($line = $this->getResultObject($rst)) {
				
					$id1 = substr($line->path,1);
					$id2 = substr($id1,0,-1);
					if($id2){
						$id21 = explode("-", $id2);
					}
					$currentOrder	.=	$line->id.",";
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					$status=($line->status)?"Active":"InActive";				
					
					if ($line->isDefault==1){
						$isDefault = "checked='checked'";
						$onclickstatus = '';
						$chkbox = '';
					}else{
						$isDefault = "";
						$onclickstatus = ' onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'font_category\')"';
						$chkbox = '<input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox">';
					}
					
					$sqlSubCat = $this->executeQry("select count(*) as cnt from ".TBL_FONTCATEGORY." where 1 and parent_id = '".$line->id."' and isDeleted = '0' and status = '0'");
					$lineSubCat = $this->getResultObject($sqlSubCat);
					$subCat = $lineSubCat->cnt?"(".$lineSubCat->cnt.")":"";
					$catname = stripslashes($this->fetchValue(TBL_FONTCATEGORY_DESCRIPTION,"categoryName","1 and catId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'"));
					//Check Multilevel==================================
					$LEVEL_FONT_CATEGORY = $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName ='LEVEL_FONT_CATEGORY'");
					if($LEVEL_FONT_CATEGORY >1 && count($id21) < $LEVEL_FONT_CATEGORY){
						$showCatName = '<a href="manageFontCategory.php?cid='.$line->id.'">'.$catname.'</a>&nbsp;<a href="manageFontCategory.php?cid='.$line->id.'">'.$subCat.'</a>';
					}else{ 
						$showCatName =$catname;
					}												
					$genTable .= '<div class="'.$highlight.'" id="'.$line->id.'">';
					$genTable .= '<div class="dragbox-content">';
					$genTable .= '<ul>';
					$genTable .= '
					<li style="width:50px;">&nbsp;&nbsp;'.$chkbox.'</li>';
					$genTable .= '<li style="width:70px;">'.$i.'</li>';
					//Name=========================================================
					$genTable .= '<li style="width:180px;">'.stripslashes($showCatName).'</li>';
					//Image==========================================================
					$imgPath=__FONTCATTHUMBPATH__.$line->categoryImage;
					$imgAbsPath=__FONTCATTHUMB__.$line->categoryImage;
					$img=_image($imgAbsPath,$imgPath,__NOIMAGEPATH__);
					$genTable .= '<li style="width:130px;"><img src="'.$img.'"</li>';
					//Default=========================================================
					$genTable .= '
					<li style="width:60px;">
					<input type="checkbox" '.$isDefault.' disabled=disabled class="welcheckbox">
					</li>';
					//Status Section============================================
					$genTable .= '<li style="width:80px;">';							
					if($menuObj->checkEditPermission()){							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" '.$onclickstatus.' >'.$status.'</div>';
					}		
					$genTable .= '</li>';
					//View Section==================================================
					$genTable .= '
					<li style="width:80px;">
					<a rel="shadowbox;width=705;height=325" title="'.$catname.'" href="viewFontCategory.php?id='.base64_encode($line->id).'">
					<img src="images/view.png" border="0">
					</a>
					</li>';
					//Edit Section====================================================
					$genTable .= '<li style="width:60px;">';							
					if($menuObj->checkEditPermission()){					
						$genTable .= '<a href="editFontCategory.php?id='.base64_encode($line->id).'&page='.$page.'&cid='.$cid.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
					}	
					$genTable .= '</li>';
					//Delete Section================================================
					$genTable .= '<li  style="width:80px;">';				
					if($menuObj->checkDeletePermission() && $line->isDefault != 1 ){			
						$genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Category  ?')){window.location.href='pass.php?action=font_category&type=delete&cid=".$cid."&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					}					
					$genTable .= '</li>';
					$genTable .= '</ul>';
					$genTable .= '</div>';
					$genTable .= '</div>';	
					$i++;	
				}

				$genTable .= '</div>';
				$genTable .= '
				<div id="dragndrop">
				<input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" />
				</div>';
				switch($recordsPerPage){
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="
				<div style='overflow:hidden; margin:0px 0px 0px 50px;'>
				<table border='0' width='88%' height='50'>
				<tr>
				<td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
				Display 
				<select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
				<option value='10' $sel1>10</option>
				<option value='20' $sel2>20</option>
				<option value='30' $sel3>30</option> 
				<option value='".$totalrecords."' $sel4>All</option>  
				</select> Records Per Page
				</td>
				<td align='center' class='page_info'>
				<inputtype='hidden' name='page' value='".$currpage."'>
				</td>
				<td class='page_info' align='center' width='200'>
				Total ".$totalrecords." records found
				</td>
				<td width='0' align='right'>".$pagenumbers."</td>
				</tr>
				</table>
				</div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}

//Start : Change Status==============================================================
	function changeValueStatus($get) {		
		$status=$this->fetchValue(TBL_FONTCATEGORY,"status","1 and id = '$get[id]'");		
		if($status==1) {
			$stat= 0;
			$status="Inactive";
		} else 	{
			$stat= 1;
			$status="Active";
		}	
		$query = "update ".TBL_FONTCATEGORY." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";		
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
//Start : Delete Value===========================================================
	function deleteValue($get) {
		$xmlArr = array();
		$xm = 1;
		//$result=$this->deleteRec(TBL_CATEGORY,"path like '%-$get[id]-%'");
		$this->executeQry("update ".TBL_FONTCATEGORY." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where path like '%-$get[id]-%'");		
		$queryXml = "update ".TBL_FONTCATEGORY." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where path like '%-$get[id]-%'";
		$xmlArr[$xm]['query'] = addslashes($queryXml);
		$xmlArr[$xm]['identification'] = $get['id'];
		$xmlArr[$xm]['section'] = "update";
		$xm ++;		
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
		echo "<script language=javascript>window.location.href='manageFontCategory.php?cid=$get[cid]&page=$get[page]&limit=$get[limit]';</script>";
	}
//Start : Add Record=========================================================================
	function addRecord($post,$file){	
		//echo "<pre>"; print_r($_POST); print_r($_FILES); echo "</pre>";exit;
		$xmlArr = array();
		$xm = 1;
		$_SESSION['SESS_MSG'] = "";				
		if($file['categoryImage']['name']){			
  			$filename = stripslashes($file['categoryImage']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);			
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = __FONTCATORIGINAL__.$image_name;		
			if($this->checkExtensions($extension)) {					
				$filestatus = move_uploaded_file($file['categoryImage']['tmp_name'], $target);	
				if($filestatus){
					$imgSource = $target;	 
					$thumb = __FONTCATTHUMB__.$image_name;
					$large = __FONTCATLARGE__.$image_name;
					@chmod(__FONTCATTHUMB__,0777);
					@chmod(__FONTCATLARGE__,0777);
					$thumbSize = $this->findSize('FONTCAT_THUMB_WIDTH','FONTCAT_THUMB_HEIGHT',100,100);
					$largeSize = $this->findSize('FONTCAT_LARGE_WIDTH','FONTCAT_LARGE_HEIGHT',100,100);
					exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
					exec(IMAGEMAGICPATH." $imgSource -thumbnail $largeSize $large");
				} else {
					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"There is some error to upload flag.!!!");
				}			
			} else {
				$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
			} 	
		}
		if($_SESSION['SESS_MSG'] == "") {
			$query = "insert into ".TBL_FONTCATEGORY." set parent_id = '$post[cid]',path='',categoryImage = '$image_name', status = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."', sequence = '".$post['maxsequence']."'";
			$this->executeQry($query);
			$inserted_id = mysql_insert_id();
			$this->logSuccessFail('1',$query);
			
			$xmlArr[$xm]['query'] = addslashes($query);
			$xmlArr[$xm]['identification'] = $inserted_id;
			$xmlArr[$xm]['section'] = "insert";
			$xm ++;						
					
			if($post[cid] == 0) 
				$path = "-".$inserted_id."-";
			else				
				$path = $this->fetchValue(TBL_FONTCATEGORY,"path","1 and id = '$post[cid]'").$inserted_id."-";	
			$query = "update ".TBL_FONTCATEGORY." set path = '$path' where id = $inserted_id";

			$xmlArr[$xm]['query'] = addslashes($query);
			$xmlArr[$xm]['identification'] = $inserted_id;
			$xmlArr[$xm]['section'] = "update";
			$xm ++;	

			if($this->executeQry($query)) 
				$this->logSuccessFail('1',$query);		
			else 	
				$this->logSuccessFail('0',$query);
			
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$categoryName = 'categoryName_'.$line->id;
					$query = "insert into ".TBL_FONTCATEGORY_DESCRIPTION." set catId = '$inserted_id', langId = '".$line->id."', categoryName = '".addslashes($post[$categoryName])."'";	

					$xmlArr[$xm]['query'] = addslashes($query);
					$xmlArr[$xm]['identification'] = $inserted_id;
					$xmlArr[$xm]['section'] = "insert";
					$xm ++;	

					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				}	
			}					
			$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been added successfully");
		}
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		@header('location:addFontCategory.php?cid=$post[cid]'); 	
	}
	
	
//Start : Edit Record=======================================================
	function editRecord($post,$file) {
                $preStatus=$this->fetchValue(TBL_FONTCATEGORY,"status","id='".$post[id]."'");
                $isdefault = $post[isDefault]?1:0;                
		
		if($isdefault){
			$this->executeQry("update ".TBL_FONTCATEGORY." set isDefault='0' where id != '$post[id]'");			
			$this->executeQry("update ".TBL_FONTCATEGORY." set isDefault='1', status='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$post[id]'");
					 	
		}
		$_SESSION['SESS_MSG'] = "";
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$categoryName = 'categoryName_'.$line->id;
				$sql = $this->selectQry(TBL_FONTCATEGORY_DESCRIPTION,'1 and catId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0) {
					$query = "insert into ".TBL_FONTCATEGORY_DESCRIPTION." set catId = '$post[id]', langId = '".$line->id."', categoryName = '".$post[$categoryName]."'";
					
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				} else {
					$query = "update ".TBL_FONTCATEGORY_DESCRIPTION." set categoryName = '".addslashes($post[$categoryName])."' where 1 and catId = '$post[id]' and langId = '".$line->id."'";
					
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				}	
			}
                        $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been updated successfully");
		}
			
		if($file['categoryImage']['name']){
  			$filename = stripslashes($file['categoryImage']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);
	
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = __FONTCATORIGINAL__.$image_name;	
			
			if($this->checkExtensions($extension)) {		
				$filestatus = move_uploaded_file($file['categoryImage']['tmp_name'], $target);			
				chmod($target, 0777);
				if($filestatus){
					$imgSource = $target;	 
					$thumb = __FONTCATTHUMB__.$image_name;
					$large = __FONTCATLARGE__.$image_name;
					@chmod(__FONTCATTHUMB__,0777);
					@chmod(__FONTCATLARGE__,0777);
					$thumbSize = $this->findSize('FONTCAT_THUMB_WIDTH','FONTCAT_THUMB_HEIGHT',100,100);
					$largeSize = $this->findSize('FONTCAT_LARGE_WIDTH','FONTCAT_LARGE_HEIGHT',100,100);
					exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
					exec(IMAGEMAGICPATH." $imgSource -thumbnail $largeSize $large");
					
					$preImg = $this->fetchValue(TBL_FONTCATEGORY,"categoryImage","1 and id = '$post[id]'");
					@unlink(__FONTCATORIGINAL__.$preImg);
					@unlink(__FONTCATTHUMB__.$preImg);	
					@unlink(__FONTCATLARGE__.$preImg);							
					$query = "update ".TBL_FONTCATEGORY." set categoryImage = '$image_name', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$post[id]'";

					$xmlArr[$xm]['query'] = addslashes($query);
					$xmlArr[$xm]['identification'] = $post['id'];
					$xmlArr[$xm]['section'] = "update";
					$xm ++;

					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				} else {
					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"There is some error to upload flag.!!!");
				}			
			} else {
				$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
			} 
                        $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been updated successfully");
		}		
		echo "<script>window.location.href='manageFontCategory.php?cid=$post[cid]&page=$post[page]';</script>";exit;
	}
//Start : Delete All Values========================================================
	function deleteAllValues($post){
		$xmlArr = array();
		$xm = 1;

		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageFontCategory.php?cid=$post[cid]&page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(TBL_CATEGORY," path like '%-$val-%'");	
					$this->executeQry("update ".TBL_FONTCATEGORY." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where path like '%-$val-%' and isDefault != '1'");

					$queryXml = "update ".TBL_FONTCATEGORY." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where path like '%-$val-%' and isDefault != '1'";
					$xmlArr[$xm]['query'] = addslashes($queryXml);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;

				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){				    
					$sql="update ".TBL_FONTCATEGORY." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					
					$xmlArr[$xm]['query'] = addslashes($sql);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;

					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
					$sql="update ".TBL_FONTCATEGORY." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$xmlArr[$xm]['query'] = addslashes($sql);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;

					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		echo "<script language=javascript>window.location.href='manageFontCategory.php?cid=$post[cid]&page=$post[page]';</script>";
	}	
	
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_FONTCATEGORY." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect("manageFontCategory.php");
		}	
	}
	
	function getBreadCrumb($cid) {
		$pageName2   = basename($_SERVER['PHP_SELF']);		
		if($cid && $cid != '' && $cid != 0) {
			$breadcrumb = '&nbsp;&nbsp;<a href="'.$pageName2.'">Top</a>';
			$path = $this->fetchValue(TBL_FONTCATEGORY,"path","1 and id = $cid");
			$pathArr = explode('-',$path);
			$ctr = 0;	
			foreach($pathArr as $key=>$value) {
				if($value) {
					if(count($pathArr) - $ctr <= 2)
						$breadcrumb .= "&nbsp;->&nbsp;".stripslashes($this->fetchValue(TBL_FONTCATEGORY_DESCRIPTION,"categoryName","1 and catId = '$value' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'"));
					else					
						$breadcrumb .= "&nbsp;->&nbsp;<a href='".$pageName2."?cid=$value'>".stripslashes($this->fetchValue(TBL_FONTCATEGORY_DESCRIPTION,"categoryName","1 and catId = '$value' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'"))."</a>";
				}	
				$ctr++;
			}
		}
		return $breadcrumb;
	}
	
	function isCategoryNameExist($catname,$langId,$id=''){
		$catname = trim($catname);
		$rst = $this->selectQry(TBL_FONTCATEGORY_DESCRIPTION,"langId = '".$langId."' and categoryName='".addslashes($catname)."' AND catId!='$id'  ","","");
		$row = $this->getTotalRow($rst);
		return $row;
	}
	
	function checkCategoryExist($cid) {
		if($cid) {
			if($cid >= 0 && is_numeric($cid)) {	
				if($cid > 0 && $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'MULTILEVEL_FONT_CATEGORY'") == 1) {
					$getCid = $this->fetchValue(TBL_FONTCATEGORY,"id","1 and id = ".(int)$cid."");
					if(!$getCid)
						redirect('manageFontCategory.php');		
				} else {
					redirect('manageFontCategory.php');					
				}
			} else {
				redirect('manageFontCategory.php');
			}
		}
	}
	
	function SortSequence($get){
	  //print_r($get);
	  $sortedArr 	=   explode("=",$get['url']);
	  $sortedIdArr	=	$sortedArr[1];
	  $Sorted_Order_Arr = explode(",",$sortedIdArr);
	  $Current_Order_Arr = explode(",",substr_replace($get['Current_Order'],"",-1));
	 
	  $i = 0;
	  $sorted_key_arr	=	array();	 
	  foreach($Sorted_Order_Arr as $Sorted_Order_Val){
	     $current_id	=	$Current_Order_Arr[$i];
		 $sql_string	=	"SELECT sequence FROM ".TBL_FONTCATEGORY." WHERE id = '$current_id' ";
         $query 	 	=	mysql_query($sql_string);
		 if($line = mysql_fetch_object($query)){
		    $sorted_key_arr[$Sorted_Order_Val] = $line->sequence;
		 }
		 $i++;
       }
	  //print_r($sorted_key_arr);
	  foreach($sorted_key_arr as $key=>$val){  
	        $sql_string		=	"UPDATE ".TBL_FONTCATEGORY." SET sequence = '$val' WHERE id = '".$key."'";
		   	mysql_query($sql_string);   
	   }
	    
/*		for($i=30;$i<60;$i++){
		     $Qry  = mysql_query("SELECT CAT.* FROM ecart_category as CAT WHERE parent_id = '0'");
			 $n_row = mysql_num_rows($Qry);
			 if($n_row > 1){
			   $k = 1;
			   while($data = mysql_fetch_object($Qry)){
			     mysql_query("UPDATE `ecart_category` SET `sequence` = '$k' WHERE `id` ='$data->id'");
				 $k++;
			   }
			 }
		}*/
		$cid  = $cid?$cid:0;
		$cond = "1 and c.id = cd.catId and c.parent_id = $cid and c.isDeleted = '0' and cd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		$query = "select c.*,cd.categoryName from ".TBL_FONTCATEGORY." as c , ".TBL_FONTCATEGORY_DESCRIPTION." as cd where $cond ";
		
		
				//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($get['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($get['page']); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
		     $orderby = $_GET[orderby]? $_GET[orderby]:"sequence";
		     $order = $_GET[order]? $_GET[order]:"ASC";   
         //   $query .=  " ORDER BY cd.$orderby $order LIMIT ".$offset.", ". $recordsPerPage;
		     $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			
	      $order_query = mysql_query($query);
		  while($line = mysql_fetch_object($order_query)){
		     $currentOrder .= $line->id.",";
		  }
	   	  ?><input type="hidden" name="currentOrder" id="currentOrder" value="<?=$currentOrder?>" /><?php


	}
	
	function findMaxSequence(){
		$query	=	"SELECT max(sequence) as maxsequence FROM ".TBL_FONTCATEGORY." ";
	    $res = $this->executeQry($query);
		$line = $this->getResultObject($res);
		return $line->maxsequence+1;
	}
}// End Class
?>	