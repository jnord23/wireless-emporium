<?php 
session_start();
class AssignMainProducts extends MySqlDriver{
        //Start : Constructor===================================
        function __construct() {
        $this->obj = new MySqlDriver;       
        }

        //Start : Val Detail=============================================================
        function valDetail() {
                //$cond = "1 AND tbl1.userType = 'A' AND tbl1.isDeleted = '0' ";
                if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
                    $searchtxt = $_REQUEST['searchtxt'];

                }
                $cond .= " order by id desc ";
                $query = "select * from ".TBL_ADMIN_DESIGNPRODUCT." where isDeleted = '0' $cond ";

                $sql = $this->executeQry($query);
                $num = $this->getTotalRow($sql);
                $menuObj = new Menu();
                $page =  $_REQUEST['page']?$_REQUEST['page']:1;
                if($num > 0) {			
                        //-------------------------Paging------------------------------------------------			
                        $paging = $this->paging($query); 
                        $this->setLimit($_GET[limit]); 
                        $recordsPerPage = $this->getLimit(); 
                        $offset = $this->getOffset($_GET["page"]); 
                        $this->setStyle("redheading"); 
                        $this->setActiveStyle("smallheading"); 
                        $this->setButtonStyle("boldcolor");
                        $currQueryString = $this->getQueryString();
                        $this->setParameter($currQueryString);
                        $totalrecords = $this->numrows;
                        $currpage = $this->getPage();
                        $totalpage = $this->getNoOfPages();
                        $pagenumbers = $this->getPageNo();		
                        //-------------------------Paging------------------------------------------------
                        $query .=  " LIMIT ".$offset.", ". $recordsPerPage;
                        $rst = $this->executeQry($query); 
                        $row = $this->getTotalRow($rst);
                        if($row > 0) {			
                                $i = 1;			
                                while($line = $this->getResultObject($rst)) {                                       
                                        $currentOrder	.=	$line->id.",";
                                        $highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";

                                        $title = $this->fetchValue(TBL_PRODUCT_DESCRIPTION,'productName',"1 and productId = '$line->productId' and langId = '".$_SESSION['DEFAULTLANGUAGE']."' ");
                                        $viewQry = "select * from ".TBL_ADMIN_PRODUCT_VIEW." where adminDesignId = '".$line->id."'";
                                        $viewRst = $this->executeQry($viewQry);
                                        $genTable .= '<div class="'.$highlight.'" id="'.$line->id.'">';
                                        $genTable .= '<h1 style="height:100px;">';
                                        $genTable .= '<ul>';
                                        $genTable .= '<li style="width:45px;">&nbsp;&nbsp;<input name="chk[]" value="'.$line->id.'" type="checkbox"></li>';
                                        $genTable .= '<li style="width:70px;">'.$i.'</li>';
                                        $genTable .= '<li style="width:200px;">'.$title.'</li>';
                                        while($viewLine = $this->getResultObject($viewRst)){                                       
                                            $image  = __MAINPRODUCTTHUMBPATH__.$viewLine->imageName;
                                            $genTable .='<li style="width:200px;"><img src="'.$image.'"></li>';	
                                        }
                                        $genTable .= '</li>';
                                        $genTable .= '</ul>';
                                        $genTable .= '</h1>';
                                        $genTable .= '</div>';
                                        $i++;	
                                }
                                
                                $genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';

                                switch($recordsPerPage){
                                        case 10:
                                        $sel1 = "selected='selected'";
                                        break;
                                        case 20:
                                        $sel2 = "selected='selected'";
                                        break;
                                        case 30:
                                        $sel3 = "selected='selected'";
                                        break;
                                        case $this->numrows:
                                        $sel4 = "selected='selected'";
                                        break;
                                }
                                $currQueryString = $this->getQueryString();
                                $limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
                                $genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
                                <tr>
                                <td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
                                Display 
                                <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
                                <option value='10' $sel1>10</option>
                                <option value='20' $sel2>20</option>
                                <option value='30' $sel3>30</option> 
                                <option value='".$totalrecords."' $sel4>All</option>  
                                </select> 
                                Records Per Page
                                </td>
                                <td align='center' class='page_info'>
                                <inputtype='hidden' name='page' value='".$currpage."'>
                                </td>
                                <td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td>
                                <td width='0' align='right'>".$pagenumbers."</td>
                                </tr>
                                </table></div>";	
                        }					
                } else {
                        $genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
                }	
                return $genTable;
        }
        //Start : Delete All Values=========================================
        function deleteAllValues($post){

                if(($post[action] == '')){
                    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
                    echo "<script language=javascript>window.location.href='assign-main-product.php?page=$post[page]&limit=$post[limit]';</script>";exit;
                }	
                //Delete Selected==============================
                if($post[action] == 'deleteselected'){
                    $delres = $post[chk];
                    $numrec = count($delres);
                    if($numrec>0){
                        foreach($delres as $key => $val){
                            $this->executeQry("UPDATE ".TBL_ADMIN_DESIGNPRODUCT." SET isDeleted = '1' where id = '$val'");
                        }
                        $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
                    }else{
                        $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
                    }
                }
                
                //Assign Selected======================================
                if($post[action] == 'assigntomainproduct'){
                        $delres = $post[chk];
                        $numrec = count($delres);
                        if($numrec>0){
                                foreach($delres as $key => $val){
                                        //Select From Assign Products=============
                                        $query = "select * from ".TBL_ADMIN_DESIGNPRODUCT." where id = '".$val."' and 	isDeleted = '0' ";
                                        $sql = $this->executeQry($query);
                                        $line = $this->getResultObject($sql);
                                        
                                        //Select From Raw Product====================
                                        $rawProdQry = "select * from ".TBL_PRODUCT." where id = '".$line->productId."' and isDeleted = '0' ";
                                        $rawProdRst = $this->executeQry($rawProdQry);
                                        $rawProdLine = $this->getResultObject($rawProdRst);
                                        
                                        //Insert Into Main Product====================================
                                        $mainProdInsQry = "insert into  ".TBL_MAINPRODUCT." set rowProductId  = '".$line->productId."' , categoryId = '"."-".$line->catId."-"."' , subCatId = '".$line->subCatId."',aspId='".$rawProdLine->aspId."', productPrice = '".$rawProdLine->productPrice."' , quantity = '".$rawProdLine->quantity ."' ,imageName= '".$rawProdLine->productImage."' ,status = '".$rawProdLine->status."' , isDeleted = '".$rawProdLine->isDeleted."' ,	addDate = '".date("Y-m-d H:i:s")."' , 	addedBy = '".$_SESSION['ADMIN_ID']."' , sequence = '0' , userType = 'A' , isCustomizable= '1' , dataArr = '".$line->dataArr."' , prodDesignId = '".$line->prodDesignId."',productWeight = '".$rawProdLine->productWeight ."' ";
                                        $mainProdInsRst = $this->executeQry($mainProdInsQry);
                                        $mainProductId = mysql_insert_id();
                                        
                                        //Select From Raw Product Description & Insert into Main Product=======================
                                        $rawProdDescQry = "select * from ".TBL_PRODUCT_DESCRIPTION." where productId = '".$line->productId."' ";
                                        $rawProdDescRst = $this->executeQry($rawProdDescQry);
                                        while($rawProdDescLine = $this->getResultObject($rawProdDescRst)) {
                                                $mainProdDescInsQry = "Insert Into  ".TBL_MAIN_PRODUCT_DESCRIPTION." set productId = '".$mainProductId."' , langId = '".$rawProdDescLine->langId."' , productName = '".$rawProdDescLine->productName."' , productDesc = '".$rawProdDescLine->productDesc ."' ";
                                                $mainProdDescInsRst = $this->executeQry($mainProdDescInsQry);
                                        }


                                        //Select From Assign Product View & insert Into Main Product View===============================
                                        
                                        $assignViewQry = "select * from ".TBL_ADMIN_PRODUCT_VIEW." where adminDesignId = '".$val."' ";
                                        $assignViewRst = $this->executeQry($assignViewQry);
                                        while($assignViewLine = $this->getResultObject($assignViewRst)){
                                               $mainProdViewInsQry = "Insert Into  ".TBL_MAIN_PRODUCT_VIEW." set product_id = '".$mainProductId."' ,view_id = '".$assignViewLine->viewId."' , imageWithBorder= '".$assignViewLine->imageName."',imageWithoutBorder= '".$assignViewLine->imageName."' , widthWithBorder = '".$assignViewLine->printWidth."' ,heightWithBorder = '".$assignViewLine->printHeight."' ,widthWithoutBorder='0',heightWithoutBorder='0',printwidth='0',printheight='0',isDefault= '".$assignViewLine->isDefault."' ";
                                                $mainProdViewInsRst = $this->executeQry($mainProdViewInsQry);
                                        }
                                        
                                        /*
                                        $prodViewQry="Select * from ".TBL_PRODUCT_VIEW." where product_id = '".$line->productId."'";
                                        $prodViewRst=mysql_query($prodViewQry);
                                        while($lineProdView = mysql_fetch_object($prodViewRst)){
                                            $printwidth=($lineProdView->printwidth)?$lineProdView->printwidth:0;
                                            $printheight=($lineProdView->printheight)?$lineProdView->printheight:0;
                                            $mainProdViewInsQry="INSERT INTO ".TBL_MAIN_PRODUCT_VIEW." SET product_id = '".$mainProductId."', view_id='".$lineProdView->view_id."', imageWithBorder='".$lineProdView->imageWithBorder."',imageWithoutBorder='".$lineProdView->imageWithoutBorder."', 	widthWithBorder='".$lineProdView->widthWithBorder."', heightWithBorder='".$lineProdView->heightWithBorder."' ,widthWithoutBorder='".$lineProdView->widthWithoutBorder."' ,heightWithoutBorder='".$lineProdView->heightWithoutBorder."', printwidth='".$printwidth."', printheight='".$printheight."', isDefault='".$lineProdView->isDefault."' ";
                                            $mainProdViewInsRst  =mysql_query($mainProdViewInsQry);
                                        }*/
                                        
                                        $assignUpdateRst = $this->executeQry("UPDATE ".TBL_ADMIN_DESIGNPRODUCT." SET isDeleted = '1' where id = '$val'");

                                }
                                $_SESSION['SESS_MSG'] =msgSuccessFail("success","Assign Template successfully!!!");
                        }else{
                                $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
                        }
                }

                echo "<script language=javascript>window.location.href='assign-main-product.php?page=$post[page]';</script>";exit;
        }
	
}// End Class
?>	
