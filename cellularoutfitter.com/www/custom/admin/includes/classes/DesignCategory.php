<?php 
session_start();
class DesignCategory extends MySqlDriver{
//Start : Constructor========================================================
	function __construct(){
		$this->obj = new MySqlDriver;       
	}

//Start : Show Grid Detail=====================================================
	function valDetail($cid) {
		$cid  = $cid?$cid:0;
		$cond = "1 and c.id = cd.catId and c.parent_id = $cid and c.isDeleted = '0' and cd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
                 if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){			
			$searchtxt = addslashes($_REQUEST['searchtxt']);
			$cond .= " AND (cd.categoryName LIKE '%$searchtxt%')";
			stripslashes($_REQUEST['searchtxt']);	
		}
		$query = "select c.*,cd.categoryName from ".TBL_DESIGNCATEGORY." as c , ".TBL_DESIGNCATEGORY_DESCRIPTION." as cd where $cond ";		
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging-------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET['orderby']? $_GET['orderby']:"sequence";
			$order = $_GET['order']? $_GET['order']:"ASC";   
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;			
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);			
			if($row > 0) {			
				$i = 1;	
				$genTable .= '<div class="column" id="column1">';			
				while($line = $this->getResultObject($rst)) {
				    $id1 = substr($line->path,1);
					$id2 = substr($id1,0,-1);
					if($id2){
						$id21 = explode("-", $id2);
					}
					
					$currentOrder.=	$line->id.",";
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					$status=($line->status)?"Active":"InActive";					
					
					$sqlSubCat = $this->executeQry("select count(*) as cnt from ".TBL_DESIGNCATEGORY." where 1 and parent_id = '".$line->id."'");
					$lineSubCat = $this->getResultObject($sqlSubCat);
					$subCat = $lineSubCat->cnt?"(".$lineSubCat->cnt.")":"";
					//Check Multilevel==============================
					$LEVEL_DESIGN_CATEGORY = $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName ='LEVEL_DESIGN_CATEGORY'");					
					if($LEVEL_DESIGN_CATEGORY > 1 && count($id21) < $LEVEL_DESIGN_CATEGORY){
						$categoryName = '<a href="manageDesignCategory.php?cid='.$line->id.'">'.$this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'").'</a>&nbsp;<a href="manageDesignCategory.php?cid='.$line->id.'">'.$subCat.'</a>';
					}else{ 
						$categoryName = $this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");
					}	
					$genTable .= '<div class="'.$highlight.'" id="'.$line->id.'">';
					$genTable .= '<div class="dragbox-content">';
					$genTable .= '<ul>';
					$genTable .= '
					<li style="width:50px;">&nbsp;&nbsp;
					<input name="chk[]" value="'.$line->id.'" type="checkbox">
					</li>';
					$genTable .= '<li style="width:100px;">'.$i.'</li>';
					$genTable .= '<li style="width:180px;">'.stripslashes($categoryName).'</li>';
					//Image=========================================================
					$imgPath=__DESIGNCATTHUMBPATH__.$line->categoryImage;
					$imgAbsPath=__DESIGNCATTHUMB__.$line->categoryImage;
					$img=_image($imgAbsPath,$imgPath,__NOIMAGEPATH__);
					$genTable .= '<li style="width:130px;"><img src="'.$img.'" /></li>';
					//Status Section=============================================
					$genTable .= '<li style="width:100px;">';							
					if($menuObj->checkEditPermission()){							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'design_category\')">'.$status.'</div>';
					}
					$genTable .= '</li>';
					//View Section===================================================
					$catName = stripslashes($this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'"));
					$genTable .= '
					<li style="width:100px;">
					<a rel="shadowbox;width=705;height=325" title="'.$catName.'" href="viewDesignCategory.php?id='.base64_encode($line->id).'">
					<img src="images/view.png" border="0">
					</a>
					</li>';
					//Edit Section=================================================
					$genTable .= '<li style="width:75px;">';							
					if($menuObj->checkEditPermission()){					
						$genTable .= '<a href="editDesignCategory.php?id='.base64_encode($line->id).'&page='.$page.'&cid='.$cid.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
					}	
					$genTable .= '</li>';
					$genTable .= '<li>';				
					if($menuObj->checkDeletePermission()){					
						$genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Category  ?')){window.location.href='pass.php?action=design_category&type=delete&cid=".$cid."&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					}							
					$genTable .= '</li>';
					//===============================================================
					$genTable .= '</ul>';
					$genTable .= '</div>';
					$genTable .= '</div>';
				
		
					$i++;	
				}
				$genTable .= '</div>';
				$genTable .= '
				<div id="dragndrop">
				<input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" />
				</div>';

				switch($recordsPerPage){
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="
				<div style='overflow:hidden; margin:0px 0px 0px 50px;'>
				<table border='0' width='88%' height='50'>
				<tr>
				<td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
				Display 
				<select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
				<option value='10' $sel1>10</option>
				<option value='20' $sel2>20</option>
				<option value='30' $sel3>30</option> 
				<option value='".$totalrecords."' $sel4>All</option>  
				</select> Records Per Page
				</td>
				<td align='center' class='page_info'>
				<inputtype='hidden' name='page' value='".$currpage."'>
				</td>
				<td class='page_info' align='center' width='200'>
				Total ".$totalrecords." records found
				</td>
				<td width='0' align='right'>".$pagenumbers."</td>
				</tr>
				</table>
				</div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}
	
//Start : Change Status=================================================================
	function changeValueStatus($get) {
		$xmlArr = array();
		$xm = 1;
		$status=$this->fetchValue(TBL_DESIGNCATEGORY,"status","1 and id = '$get[id]'");		
		if($status==1) {
			$stat= 0;
			$status="Inactive";
		} else 	{
			$stat= 1;
			$status="Active";
		}	
		$query = "update ".TBL_DESIGNCATEGORY." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";		
		$xmlArr[$xm]['query'] = addslashes($query);
		$xmlArr[$xm]['identification'] = $get['id'];
		$xmlArr[$xm]['section'] = "update";
		$xm ++;		
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);		
		if($this->executeQry($query)){
			$this->logSuccessFail('1',$query);		
		}else{ 	
			$this->logSuccessFail('0',$query);
		}
		echo $status;		
	}
	
//Start : Delete Value=============================================================
	function deleteValue($get) {		
		$xmlArr = array();
		$xm = 1;		
		$selQuery = "SELECT id FROM ".TBL_DESIGNCATEGORY." WHERE path like '%-$get[id]-%'"; 
		$delRst = $this->executeQry($selQuery);
		while($lineId = $this->getResultObject($delRst)){
			$ids .= $lineId->id.",";
		}	
		$ids = substr($ids,0,-1);
		$this->deleteRec(TBL_DESIGNCATEGORY, "path like '%-$get[id]-%'");
		$this->deleteRec(TBL_DESIGNCATEGORY_DESCRIPTION, "catId IN ($ids)");
	//	$this->executeQry("update ".TBL_DESIGNCATEGORY." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where path like '%-$get[id]-%'");
		$queryXml = "SELECT id FROM ".TBL_DESIGNCATEGORY." WHERE path like '%-$get[id]-%'";
		$xmlArr[$xm]['query'] = addslashes($queryXml);
		$xmlArr[$xm]['identification'] = $get['id'];
		$xmlArr[$xm]['section'] = "delete";
		$xm ++;
		
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
		echo "<script language=javascript>window.location.href='manageDesignCategory.php?cid=$get[cid]&page=$get[page]&limit=$get[limit]';</script>";
	}
	
//Start : Check Default Category=======================================
	function chkDefaultCategory($cid){	    
		$query = "select TDC.* from ".TBL_DESIGNCATEGORY." as TDC ,  ".TBL_DESIGNCATEGORY_DESCRIPTION." as TDD where TDC.parent_id = '".$cid."' and TDD.categoryName = '".strtolower('other')."' and  TDD.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and  TDC.id = TDD.catId";
		$res = $this->executeQry($query);
		$row = $this->getTotalRow($res);
		$query="insert into ".TBL_DESIGNCATEGORY." set parent_id = '$cid', categoryImage = '$image_name', status = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."',sequence = '".$post['maxsequence']."'";
		
                $this->executeQry($query);
		$inserted_id1 = mysql_insert_id();
		$path1 = "-".$cid."-".$inserted_id1."-";		
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");	$num = $this->getTotalRow($rst);
		if($num){		
			if($DefaultCat == 0) {
				while($line = $this->getResultObject($rst)) {
					$queryDefault = "insert into ".TBL_DESIGNCATEGORY_DESCRIPTION." set catId = '$inserted_id1', langId = '".$line->id."', categoryName = 'other'";	
					if($this->executeQry($queryDefault)){
						$this->logSuccessFail('1',$queryDefault);		
					}else{ 	
						$this->logSuccessFail('0',$queryDefault);
					}
				}
			}
		}
		$queryupdate = "update ".TBL_DESIGNCATEGORY." set path = '$path1' where id = '$inserted_id1'";
		$this->executeQry($queryupdate);
	}	
	
// Start : Add Record===============================================================	
	function addRecord($post,$file) {
		//echo "<pre>"; print_r($post); print_r($file);echo "</pre>";exit;		
		$_SESSION['SESS_MSG'] = "";	
		
		if($file['categoryImage']['name']){
  			$filename = stripslashes($file['categoryImage']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);
	
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = __DESIGNCATORIGINAL__.$image_name;
			if($this->checkExtensions($extension)) {	
				$filestatus = 	move_uploaded_file($file['categoryImage']['tmp_name'], $target);                                
				@chmod($target, 0777);
				if($filestatus){
					$imgSource = $target;	 
					$thumb = __DESIGNCATTHUMB__.$image_name;
					$large = __DESIGNCATLARGE__.$image_name;
					@chmod(__DESIGNCATTHUMB__,0777);
					@chmod(__DESIGNCATLARGE__,0777);
					$thumbSize = $this->findSize('DESIGNCAT_THUMB_WIDTH','DESIGNCAT_THUMB_HEIGHT',100,100);
					$largeSize = $this->findSize('DESIGNCAT_LARGE_WIDTH','DESIGNCAT_LARGE_HEIGHT',100,100);
					exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
					exec(IMAGEMAGICPATH." $imgSource -thumbnail $largeSize $large");
				} else {
					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"There is some error to upload flag.!!!");
				}			
			} else {
				$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
			} 	
		}
		
		if($_SESSION['SESS_MSG'] == "") {
                        $query="insert into ".TBL_DESIGNCATEGORY." set parent_id = '$post[cid]', categoryImage = '$image_name',path='',position='', status = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."',sequence = '".$post['maxsequence']."'";
			
                        $this->executeQry($query);                       
			$inserted_id = mysql_insert_id();			
			if($post[cid] == 0) 
				$path = "-".$inserted_id."-";
			else				
				$path = $this->fetchValue(TBL_DESIGNCATEGORY,"path","1 and id = '$post[cid]'").$inserted_id."-";	
			$query = "update ".TBL_DESIGNCATEGORY." set path = '$path' where id = $inserted_id";
			
			if($this->executeQry($query)) 
				$this->logSuccessFail('1',$query);		
			else 	
				$this->logSuccessFail('0',$query);
			
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$categoryName = 'categoryName_'.$line->id;
					$query = "insert into ".TBL_DESIGNCATEGORY_DESCRIPTION." set catId = '$inserted_id', langId = '".$line->id."', categoryName = '".addslashes($post[$categoryName])."'";	
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				}
			}					
			if($post[cid] == 0){
				//$DefaultCat = $this->chkDefaultCategory($inserted_id);
                        }
					
		} 		
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been added successfully");
		//@header('location:addDesignCategory.php?cid=$post[cid]');		
	}
	
	
//Start : Edit Record====================================================================
	function editRecord($post,$file) {	
                
                /*$isdefault = $post[isDefault]?1:0;
		if($isdefault){
			$this->executeQry("update ".TBL_DESIGNCATEGORY." set isDefault='0' where id != '$post[id]'");			
			$this->executeQry("update ".TBL_DESIGNCATEGORY." set isDefault='1', status='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$post[id]'");
					 	
		}*/
		$_SESSION['SESS_MSG'] = "";
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$categoryName = 'categoryName_'.$line->id;
				$sql = $this->selectQry(TBL_DESIGNCATEGORY_DESCRIPTION,'1 and catId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0) {
					$query = "insert into ".TBL_DESIGNCATEGORY_DESCRIPTION." set catId = '$post[id]', langId = '".$line->id."', categoryName = '".$post[$categoryName]."'";
						
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				} else {
					$query = "update ".TBL_DESIGNCATEGORY_DESCRIPTION." set categoryName = '".addslashes($post[$categoryName])."' where 1 and catId = '$post[id]' and langId = '".$line->id."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				}	
			}	
                        $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been updated successfully");
		}
			
		if($file['categoryImage']['name']){
  			$filename = stripslashes($file['categoryImage']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);
	
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = __DESIGNCATORIGINAL__.$image_name;	
			
			if($this->checkExtensions($extension)) {		
				$filestatus = 	move_uploaded_file($file['categoryImage']['tmp_name'], $target);                               
				chmod($target, 0777);
				if($filestatus){
					// Generate Thumbs=================================================
					$imgSource = $target;	 
					$thumb = __DESIGNCATTHUMB__.$image_name;
					$large = __DESIGNCATLARGE__.$image_name;
					@chmod(__DESIGNCATTHUMB__,0777);
					@chmod(__DESIGNCATLARGE__,0777);
					$thumbSize = $this->findSize('DESIGNCAT_THUMB_WIDTH','DESIGNCAT_THUMB_HEIGHT',100,100);
					$largeSize = $this->findSize('DESIGNCAT_LARGE_WIDTH','DESIGNCAT_LARGE_HEIGHT',100,100);
					exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
					exec(IMAGEMAGICPATH." $imgSource -thumbnail $largeSize $large");
					//Unling Previous Images=============================================
					$preImg = $this->fetchValue(TBL_DESIGNCATEGORY,"categoryImage","1 and id = '$post[id]'");
					@unlink(__DESIGNCATORIGINAL__.$preImg);
					@unlink(__DESIGNCATTHUMB__.$preImg);
					@unlink(__DESIGNCATLARGE__.$preImg);							
					$query = "update ".TBL_DESIGNCATEGORY." set categoryImage = '$image_name', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$post[id]'";
														
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				} else {
					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"There is some error to upload flag.!!!");
				}			
			} else {
				$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
			} 	
                        $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been updated successfully");
		}
		
		echo "<script>window.location.href='manageDesignCategory.php?cid=$post[cid]&page=$post[page]';</script>";exit;
	}
//Start : Delete All Values===========================================================
	function deleteAllValues($post){
	
		$xmlArr = array();
		$xm = 1;
		
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageDesignCategory.php?cid=$post[cid]&page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				
					$selQuery = "SELECT id FROM ".TBL_DESIGNCATEGORY." WHERE path like '%-$val-%'"; 
					$delRst = $this->executeQry($selQuery);
					while($lineId = $this->getResultObject($delRst)){
						$ids .= $lineId->id.",";
					}	
					$ids = substr($ids,0,-1);
					$this->deleteRec(TBL_DESIGNCATEGORY, "path like '%-$val-%'");
					$this->deleteRec(TBL_DESIGNCATEGORY_DESCRIPTION, "catId IN ($ids)");

				    //$result=$this->deleteRec(TBL_CATEGORY," path like '%-$val-%'");	
				//	$this->executeQry("update ".TBL_DESIGNCATEGORY." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where path like '%-$val-%'");
					$queryXml = "SELECT id FROM ".TBL_DESIGNCATEGORY." WHERE path like '%-$val-%'";
					$xmlArr[$xm]['query'] = addslashes($queryXml);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
					$sql="update ".TBL_DESIGNCATEGORY." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$xmlArr[$xm]['query'] = addslashes($sql);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
					$sql="update ".TBL_DESIGNCATEGORY." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$xmlArr[$xm]['query'] = addslashes($sql);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		echo "<script language=javascript>window.location.href='manageDesignCategory.php?cid=$post[cid]&page=$post[page]';</script>";
	}	
	
//Start : Get Result=======================================================================
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_DESIGNCATEGORY." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect('manageDesignCategory.php');
		}	
	}
//Start : Get Bread Crumb====================================================================
	function getBreadCrumb($cid) {
		$pageName2   = basename($_SERVER['PHP_SELF']);		
		if($cid && $cid != '' && $cid != 0) {
			$breadcrumb = '&nbsp;&nbsp;<a href="'.$pageName2.'">Top</a>';
			$path = $this->fetchValue(TBL_DESIGNCATEGORY,"path","1 and id = $cid");
			$pathArr = explode('-',$path);
			$ctr = 0;	
			foreach($pathArr as $key=>$value) {
				if($value) {
					if(count($pathArr) - $ctr <= 2)
						$breadcrumb .= "&nbsp;->&nbsp;".stripslashes($this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '$value' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'"));
					else					
						$breadcrumb .= "&nbsp;->&nbsp;<a href='".$pageName2."?cid=$value'>".stripslashes($this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '$value' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'"))."</a>";
				}	
				$ctr++;
			}
		}
		return $breadcrumb;
	}
//Start : isCategory Name Exist============================================================	
	function isCategoryNameExist($catname,$langId,$id=''){
	
	
		$catname = trim($catname);
		//$query = " SELECT TBL1.categoryName FROM ".TBL_DESIGNCATEGORY_DESCRIPTION." AS TBL1 INNER JOIN					".TBL_DESIGNCATEGORY." AS TBL2 ON (TBL1.catId = TBL2.id) WHERE TBL1.langId = '".$langId."' AND TBL2.isDeleted = '0'";
		//$rst = $this->executeQry($query);	
		$rst = $this->selectQry(TBL_DESIGNCATEGORY_DESCRIPTION,"langId = '".$langId."' and categoryName='".addslashes($catname)."' AND catId!='$id'  ","","");
		$row = $this->getTotalRow($rst);
		//echo $row;exit;
		return $row;
	}
// Start: Check Category Exist=========================================================	
	function checkCategoryExist($cid) {
	
			if($cid) {
			

			if($cid >= 0 && is_numeric($cid)) {	
				if($cid > 0 && $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'LEVEL_DESIGN_CATEGORY'") > 1) {
					$getCid = $this->fetchValue(TBL_DESIGNCATEGORY,"id","1 and id = ".(int)$cid."");
					
					if(!$getCid)
						redirect('manageDesignCategory.php');		
				} else {
					redirect('manageDesignCategory.php');					
				}
			} else {
				redirect('manageDesignCategory.php');
			}
		}
	}
	
//Start : Sort Sequence=============================================================
	function SortSequence($get){	  
		$sortedArr 	=   explode("=",$get['url']);
		$sortedIdArr	=	$sortedArr[1];
		$Sorted_Order_Arr = explode(",",$sortedIdArr);
		$Current_Order_Arr = explode(",",substr_replace($get['Current_Order'],"",-1));
	 
		$i = 0;
		$sorted_key_arr	=	array();	 
		foreach($Sorted_Order_Arr as $Sorted_Order_Val){
			$current_id	=	$Current_Order_Arr[$i];
			$sql_string	=	"SELECT sequence FROM ".TBL_DESIGNCATEGORY." WHERE id = '$current_id' ";
			$query=mysql_query($sql_string);
			if($line = mysql_fetch_object($query)){
				$sorted_key_arr[$Sorted_Order_Val] = $line->sequence;
			}
			$i++;
		}

		foreach($sorted_key_arr as $key=>$val){  
			$sql_string	="UPDATE ".TBL_DESIGNCATEGORY." SET sequence = '$val' WHERE id = '".$key."'";
		   	mysql_query($sql_string);   
		}
	    
/*		for($i=30;$i<60;$i++){
		     $Qry  = mysql_query("SELECT CAT.* FROM ecart_category as CAT WHERE parent_id = '0'");
			 $n_row = mysql_num_rows($Qry);
			 if($n_row > 1){
			   $k = 1;
			   while($data = mysql_fetch_object($Qry)){
			     mysql_query("UPDATE `ecart_category` SET `sequence` = '$k' WHERE `id` ='$data->id'");
				 $k++;
			   }
			 }
		}*/
		$cid  = $cid?$cid:0;
		$cond = "1 and c.id = cd.catId and c.parent_id = $cid and c.isDeleted = '0' and cd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		$query = "select c.*,cd.categoryName from ".TBL_DESIGNCATEGORY." as c , ".TBL_DESIGNCATEGORY_DESCRIPTION." as cd where $cond ";
		
			//-------------------------Paging------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($get['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($get['page']); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"sequence";
			$order = $_GET[order]? $_GET[order]:"ASC";   
			//$query .=  " ORDER BY cd.$orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			
			$order_query = mysql_query($query);
			while($line = mysql_fetch_object($order_query)){
			$currentOrder .= $line->id.",";
		}
	   	?><input type="hidden" name="currentOrder" id="currentOrder" value="<?=$currentOrder?>" /><?php
	}
	
//Start : Find Max Sequence==========================================================
	function findMaxSequence(){
		$query	=	"SELECT max(sequence) as maxsequence FROM ".TBL_DESIGNCATEGORY." ";
		$res = $this->executeQry($query);
		$line = $this->getResultObject($res);
		return $line->maxsequence+1;
	}
	
}// End Class
?>	