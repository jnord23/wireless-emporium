<?php 
session_start();
class Products extends MySqlDriver{
	var $vars;
	function __construct($id = NULL){
		$this->obj = new MySqlDriver;
		$this->id = $id;       
	}
// Start : Show Grid===============================================
	function valDetail() {		
		$cond = "TBL1.isDeleted = '0' AND TBL2.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";		
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){			
			$searchtxt = addslashes($_REQUEST['searchtxt']);
			$cond .= " AND (TBL2.productName LIKE '%$searchtxt%' OR TBL3.categoryName LIKE '%$searchtxt%')";
			stripslashes($_REQUEST['searchtxt']);	
		}
                
                if($_REQUEST['cid']){			
			$cond .= " AND (TBL1.categoryId='".$_REQUEST['cid']."')";			
		}
		
		$cond .= " GROUP BY TBL1.id order by TBL1.addDate DESC";		
		$query = "SELECT TBL3.categoryName, TBL1.*, TBL2.productName FROM ".TBL_PRODUCT." AS TBL1 INNER JOIN ".TBL_PRODUCT_DESCRIPTION." AS TBL2 ON (TBL1.id = TBL2.productId) INNER JOIN ".TBL_CATEGORY_DESCRIPTION." AS TBL3 ON(TBL3.catId=TBL1.categoryId OR TBL3.catId=TBL1.subCatId) WHERE $cond ";
		
		
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			//$orderby = $_GET[orderby]? $_GET[orderby]:TBL_PRODUCT_DESCRIPTION.".productName";
			//$orderby = $_GET[orderby]? $_GET[orderby]:TBL_PRODUCT.".sequence";
			// $order = $_GET[order]? $_GET[order]:"ASC";   
			$query .=  " LIMIT ".$offset.", ". $recordsPerPage;							
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {			
				$i = 1;			
				$genTable .= '<div class="column" id="column1">';		
				while($line = $this->getResultObject($rst)) {
					$currentOrder	.=	$line->id.",";
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					$status=( $line->status)?"Active":"InActive";
					
					$productName = stripslashes($this->fetchValue(TBL_PRODUCT_DESCRIPTION,"productName","1 and productId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'"));
					
					$genTable .= '<div class="'.$highlight.'" id="'.$line->id.'">';			
					$genTable .= '<ul>';
					//Check Box===================
					$genTable .= '
					<li style="width:30px;">
					<input name="chk[]" value="'.$line->id.'" type="checkbox" style="margin-left: 3px;" />
					</li>';
					//S.NO========================
					$genTable .= '<li style="width:40px;">'.$i.'</li>';
					
					//Cat Name====================
					$catName=stripslashes($this->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$line->categoryId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'"));
					$genTable .= '<li style="width:110px;">'.$catName.'</li>';
					//SubCat Name==================
					$subCatName=stripslashes($this->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$line->subCatId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'"));
					$genTable .= '<li style="width:110px;">'.$subCatName.'</li>';
					
					//Product Name==================						
					$genTable .= '
					<li style="width:140px;">'.stripslashes($line->productName).'</li>';
					//Image==========================================
					$imgPath=__RAWPRODUCTTHUMBPATH__.$line->productImage;
					$imgAbsPath=__RAWPRODUCTTHUMB__.$line->productImage;
					$img=_image($imgAbsPath,$imgPath,__NOIMAGEPATH__);
					$genTable .= '
					<li style="width:130px;"><img src="'.$img.'" /></li>';				
					//Status=====================
					$genTable .= '<li style="width:70px;">';
					if($menuObj->checkEditPermission()){							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'product\')">'.$status.'</div>';
					}
					$genTable .= '</li>';
					
					//View============================
					$genTable .= '
					<li style="width:40px;">
					<a rel="shadowbox;width=705;height=625" title="'.stripslashes($productName).'" href="viewProduct.php?id='.base64_encode($line->id).'">
					<img src="images/view.png" border="0">
					</a>
					</li>';
					//Edit=======================
					$genTable .= '<li style="width:40px;">';							
					if($menuObj->checkEditPermission()){					
						$genTable .= '
						<a href="editProduct.php?id='.base64_encode($line->id).'&page='.$page.'">
						<img src="images/edit.png" alt="Edit" width="16" height="16" border="0" />
						</a>';
					}						
					$genTable .= '</li>';
					//Delete==============================
					$genTable .= '<li>';				
					if($menuObj->checkDeletePermission()){
						$genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=product&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					}							
					$genTable .= '</li>';
					$genTable .= '</ul>';					
					$genTable .= '</div>';		
					$i++;	
				}
				$genTable .= '</div>';
				$genTable .= '
				<div id="dragndrop">
				<input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" />
				</div>';
		
				switch($recordsPerPage){
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="
				<div style='overflow:hidden; margin:0px 0px 0px 50px;'>
				<table border='0' width='88%' height='50'>
				<tr>
				<td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
				Display 
				<select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
				<option value='10' $sel1>10</option>
				<option value='20' $sel2>20</option>
				<option value='30' $sel3>30</option> 
				<option value='".$totalrecords."' $sel4>All</option>  
				</select> Records Per Page
				</td>
				<td align='center' class='page_info'>
				<inputtype='hidden' name='page' value='".$currpage."'>
				</td>
				<td class='page_info' align='center' width='200'>
				Total ".$totalrecords." records found
				</td>
				<td width='0' align='right'>".$pagenumbers."</td>
				</tr>
				</table>
				</div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}

//Start : Get View Image Upload==================================================
	function getViewImageUpload(){
	
		$genTable = '';
		$query = "select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_VIEW.".sequence asc" ;
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		if($num > 0){
			$send = array();
			while($line = $this->getResultObject($sql)){
				
				if($line->isValidate == '1'){
					$viewName =  $line->viewName.'<span class="spancolor">*</span>';
					$imgWithBorderId = "m__image";
					$imgWithoutBorderId = "m__image";
					$printWidthId = "m__printWidth";
					$printHeightId = "m__printHeight";
				}
				else{
					$viewName =  $line->viewName;
					$imgWithBorderId = "image";
					$imgWithoutBorderId = "image";
				}
				$this->vars["id"] = $line->id;
				$this->vars["view"] = $viewName;
				$this->vars["isValidate"] = $line->isValidate;
				$this->vars["imgWithBorderId"] = $imgWithBorderId;
				$this->vars["imgWithoutBorderId"] = $imgWithoutBorderId;
				$this->vars["imgWithBorderName"] = "prodImageWithBorder".$line->id;
				$this->vars["imgWithoutBorderName"] = "prodImageWithoutBorder".$line->id;
                                $this->vars["printWidth"] = "printWidth".$line->id;
				$this->vars["printWidthId"] = $printWidthId;
				$this->vars["printHeight"] = "printHeight".$line->id;
				$this->vars["printHeightId"] = $printHeightId;
				$send[] = $this->vars;
				unset($this->vars);
			}
		}
	return $send;
	}
	
//Start : Add Record=======================================================		
	function addRecord($post,$file) {
		//echo "<pre>"; print_r($post); print_r($file); echo "</pre>";exit;
		$_SESSION['SESS_MSG'] = "";		
		if($_SESSION['SESS_MSG'] == "") {
			$max_seq_query = $this->executeQry("select MAX(sequence) as SEQ from ".TBL_PRODUCT." where categoryId = '$post[categoryId]'");			
			if($data  = $this->getResultObject($max_seq_query)){
				$sequence  =  $data->SEQ + 1;
			}	
			
			// Upload Images=============================================		
			if($_FILES["productImage"]['name']){					
				$filename = stripslashes($_FILES["productImage"]['name']);
				$extension = findexts($filename);
				$extension = strtolower($extension);
				$productImage = date("Ymdhis").time().rand().'.'.$extension;
				$target= __RAWPRODUCTORIGINAL__.$productImage;			
				if($this->checkExtensions($extension)) {	
					$productImageStatus = move_uploaded_file($_FILES["productImage"]['tmp_name'], $target);
					$imgsizearr = getimagesize($target);
					@chmod($target, 0777);
					if($productImageStatus){					
						$imgSource = $target;					
						$thumb = __RAWPRODUCTTHUMB__.$productImage;
						$large = __RAWPRODUCTLARGE__.$productImage;
						@chmod(__RAWPRODUCTTHUMB__,0777);
						@chmod(__RAWPRODUCTLARGE__,0777);
						$thumbSize = $this->findSize('THUMB_WIDTH','THUMB_HEIGHT',100,100);
						$largeSize = $this->findSize('LARGE_WIDTH','LARGE_HEIGHT',100,100);
						exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
						exec(IMAGEMAGICPATH." $imgSource -thumbnail $largeSize $large");		
					}else{
						$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"Upload Error Occured.");
					}
				}else {
					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"Image Extension Not Allowed.");
				} 	
			}

			//Insert Into Product Table===============================		
			$query = "insert into ".TBL_PRODUCT." set categoryId = '$post[cat]', subCatId = '$post[subCat]', productPrice = '$post[productPrice]',productWeight = '$post[productWeight]', quantity = '$post[quantity]',productImage='".$productImage."', orientation ='$post[orientation]', height = '".$imgsizearr[1]."', width = '".$imgsizearr[0]."', status = '1', addDate = '".date('Y-m-d H:i:s')."', addedBy = '".$_SESSION['ADMIN_ID']."', sequence  = '".$sequence."'";
			
			$sql = $this->executeQry($query);
			$this->inserted_id = mysql_insert_id();
			
			//Insert Into Product Description Table=======================				
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$productName = 'productName_'.$line->id;
					$productDesc = 'productDesc_'.$line->id;					
					$query = "insert into ".TBL_PRODUCT_DESCRIPTION." set productId = '$this->inserted_id', langId = '".$line->id."', productName = '".$post[$productName]."', productDesc = '".mysql_real_escape_string($post[$productDesc])."'";	
					if($this->executeQry($query)){
						$this->logSuccessFail('1',$query);		
					}else{ 	
						$this->logSuccessFail('0',$query);
					}	
				}	
			}
			//Add Product View=============================
			$this->addProductView($post);			
			$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been added successfully");
		} 		
		$_id = base64_encode($inserted_id);
		header('Location:manageProducts.php');exit;		
	}
//Start : Add Product View================================================	
	function addProductView($post){	
		$rst = $this->selectQry(TBL_VIEW,"status='1' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num > 0){
			
			while($line = $this->getResultObject($rst)) {				
				//Upload Image with border========================================
				if($_FILES["prodImageWithBorder".$line->id]['name']){				
					$filename = stripslashes($_FILES["prodImageWithBorder".$line->id]['name']);
					$extension = strtolower(findexts($filename));					
					$imgWithBorder = date("Ymdhis").time().rand().'.'.$extension;
					$target    = __RAWPRODUCTORIGINAL__.$imgWithBorder;				
					if($this->checkExtensions($extension)) {	
						$filestatusWithBorder = move_uploaded_file($_FILES["prodImageWithBorder".$line->id]['tmp_name'], $target);
						$imgWithBorderSizeArr = getimagesize($target);
						@chmod($target, 0777);
						if($filestatusWithBorder){
							$imgSource = $target;	
                                                        // get image ratio ...........................
                                                        list($oW,$oH)=getimagesize($imgSource);
                                                        $cW=413;
                                                        $cH=442;
                                                        $minW=$cW/$oW;
                                                        $minH=$cH/$oH;
                                                        $minRatio=min($minW,$minH);
                                                        if($minRatio<1){
                                                        $toolImgW= $oW*$minRatio;
                                                        $toolImgH= $oH*$minRatio;
                                                        }
                                                        else{
                                                        $toolImgW= $oW;
                                                        $toolImgH= $oH;
                                                        }
							$thumb = __RAWPRODUCTTHUMB__.$imgWithBorder;
							$large = __RAWPRODUCTLARGE__.$imgWithBorder;
							@chmod(__RAWPRODUCTTHUMB__,0777);
							@chmod(__RAWPRODUCTLARGE__,0777);
							$thumbSize = $this->findSize('THUMB_WIDTH','THUMB_HEIGHT',100,100);
							$largeSize = $toolImgW."x".$toolImgH;
							exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
							exec(IMAGEMAGICPATH." $imgSource -thumbnail $largeSize $large");	
						}
					}else{
						$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
					}				
				}
				//Upload Without Border Images========================================	
				if($_FILES["prodImageWithoutBorder".$line->id]['name']){				
					$filename = stripslashes($_FILES["prodImageWithoutBorder".$line->id]['name']);
					$extension = strtolower(findexts($filename));					
					$imgWithOutBorder = date("Ymdhis").time().rand().'.'.$extension;
					$target    = __RAWPRODUCTORIGINAL__.$imgWithOutBorder;				
					if($this->checkExtensions($extension)) {	
						$filestatusWithOutBorder = move_uploaded_file($_FILES["prodImageWithoutBorder".$line->id]['tmp_name'], $target);
						$imgWithOutBorderSizeArr = getimagesize($target);
						@chmod($target, 0777);
						if($filestatusWithOutBorder){
							$imgSource = $target;	
                                                        
                                                         // get image ratio ...........................
                                                        list($oW,$oH)=getimagesize($imgSource);
                                                        $cW=413;
                                                        $cH=442;
                                                        $minW=$cW/$oW;
                                                        $minH=$cH/$oH;
                                                        $minRatio=min($minW,$minH);
                                                        if($minRatio<1){
                                                        $toolImgW= $oW*$minRatio;
                                                        $toolImgH= $oH*$minRatio;
                                                        }
                                                        else{
                                                        $toolImgW= $oW;
                                                        $toolImgH= $oH;
                                                        }
                                                        
							$thumb = __RAWPRODUCTTHUMB__.$imgWithOutBorder;
							$large = __RAWPRODUCTLARGE__.$imgWithOutBorder;
							@chmod(__RAWPRODUCTTHUMB__,0777);
							@chmod(__RAWPRODUCTLARGE__,0777);
							$thumbSize = $this->findSize('THUMB_WIDTH','THUMB_HEIGHT',100,100);
							$largeSize =$toolImgW."x".$toolImgH;
							exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
							exec(IMAGEMAGICPATH." $imgSource -thumbnail $largeSize $large");	
						}
					}else{
						$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
					} 			
				}
			  	
				//Insert Into Product View Table=======================================
				if($_FILES["prodImageWithBorder".$line->id]['name'] || $_FILES["prodImageWithoutBorder".$line->id]['name']){
				
					if($filestatusWithBorder && $filestatusWithOutBorder){
						$isDefault = ($line->id == $_POST['isDefault'])? "1":"0";
                                                $printWidth = "printWidth".$line->id;
                                                $printheight = "printHeight".$line->id;
                                                $printwidthval = $post[$printWidth];
                                                $printheightval = $post[$printheight];
						$query = "insert into ".TBL_PRODUCT_VIEW." set product_id = '$this->inserted_id', view_id = '".$line->id."', imageWithBorder = '".$imgWithBorder."', imageWithoutBorder = '".$imgWithOutBorder."', widthWithBorder = '".$imgWithBorderSizeArr[0]."', heightWithBorder = '".$imgWithBorderSizeArr[1]."', widthWithoutBorder = '".$imgWithOutBorderSizeArr[0]."', heightWithoutBorder = '".$imgWithOutBorderSizeArr[1]."',printwidth='".$printwidthval."',printheight='".$printheightval."', isDefault = '".$isDefault."'";
						
						if($this->executeQry($query)){
							$this->logSuccessFail('1',$query);		
						}else{ 	
							$this->logSuccessFail('0',$query);
						}
					}
				}
			
			}
		}
	}
//Start : Get View Image Upload Edit===================================================
	function getViewImageUploadEdit($pid,$arr_error){		
		$query = "select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_VIEW.".sequence ASC" ;
		$rst = $this->executeQry($query);
		$num = $this->getTotalRow($rst);			
		if($num > 0){
			$send = array();
			while($line = $this->getResultObject($rst)){
				$queryView = "SELECT * FROM ".TBL_PRODUCT_VIEW." WHERE product_id = '$pid' AND 	view_id = '$line->id'";
				$rstView = $this->executeQry($queryView);
				$numView = $this->getTotalRow($rstView);
				if($numView > 0){	
					$viewName = $line->viewName;
					$delImage = "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){deleteProductView('".base64_decode($_GET['id'])."','".$line->id."');}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";	
					$this->vars["view"] = $viewName;
					$this->vars["delImage"] = $delImage;
					$html = '<table border="0">';
					while($lineView = $this->getResultObject($rstView)){
						$sel = ($lineView->isDefault == '1')?"checked":"";
                                                if($lineView->printwidth != ''){
                                                $prt = '<tr>
                                                        <td>Print Size</td>
                                                        <td>
                                                        <input type="text" value="'.$lineView->printwidth.'" size="3" name="printWidth'.$line->id.'""> X <input type="text" value="'.$lineView->printheight.'" size="3" name="printheight'.$line->id.'">in Inches							

                                                        </td>
                                                        </tr>';
                                                }
						$html .= '
						<tr>
						<td>Outline Image</td>
						<td>
						<img src="'.__RAWPRODUCTTHUMBPATH__.$lineView->imageWithBorder.'" width="50" height="50" />
						</td>
						</tr>';
						$html.='
						<tr>
						<td>&nbsp;</td>
						<td>
						<div>
						<input type="file" name="prodImageWithBorder_'.$line->id.'" />
						<p  style="padding-left:0px; color:#FF0000;">'.$arr_error["prodImageWithBorder_".$line->id.""].'</p>
						</div>	
						</td>
						</tr>';
						$html.='
						<tr>
						<td>Background Image</td>
						<td>
						<img src="'.__RAWPRODUCTTHUMBPATH__.$lineView->imageWithoutBorder.'" width="50" height="50" />								</td>
						</tr>';
						$html.='
						<tr>
						<td>&nbsp;</td>
						<td>
						<div>
						<input type="file" name="prodImageWithoutBorder_'.$line->id.'" />
						<p  style="padding-left:0px; color:#FF0000;">'.$arr_error["prodImageWithoutBorder_".$line->id.""].'</p>
						</div>
						</td>
						</tr>'.$prt;
						$html.='
						<tr>
						<td>Make this Default</td>
						<td><input type="radio" name="isDefault" value="'.$line->id.'" '.$sel.'>
						</td>
						</tr>';
					}
					$html .= '</table>';
					$this->vars["html"] = $html;
				}else{
					$this->vars["view"] = '
					<a onclick="addProductViewImages('.$line->id.');" style="cursor:pointer;">
					Add '.$line->viewName.' View
					</a>';
					if(isset($_FILES['prodImageWithBorder_'.$line->id.'']['name']) || isset($_FILES['prodImageWithoutBorder_'.$line->id.'']['name'])){
						$this->vars["html"] = $this->setproductImagePost($line->id,$arr_error);
					}else{
						$this->vars["html"] = "";
					}
				}
				$this->vars["id"] = $line->id;
				$send[] = $this->vars;
				unset($this->vars);
			}
		return $send;
		}
	}
//Start : Set Product Image Post=================================================
	function setproductImagePost($viewId,$arr_error){		
		$html = '<table border="0">';
		$html .= '<tr>
					<td>&nbsp;</td>
					<td>Image For Tool</td>
					<td><div>
						<input type="file" id="image" name="prodImageWithBorder_'.$viewId.'">
						<p  style="padding-left:0px; color:#FF0000;">'.$arr_error["prodImageWithBorder_".$viewId.""].'</p>
						</div>
					</td>
				 </tr>
				 <tr>
					<td>&nbsp;</td>
					<td>Image For Production</td>
					<td>
					  <div>
					  <input type="file" id="image" name="prodImageWithoutBorder_'.$viewId.'">
					  <p  style="padding-left:0px; color:#FF0000;">'.$arr_error["prodImageWithoutBorder_".$viewId.""].'</p>
					  </div>
					</td>
				 </tr>
				 <tr>
					<td>&nbsp;</td>
					<td>Make this Default&nbsp;</td>
					<td>
					  <input type="radio" name="isDefault" value="'.$viewId.'">
					</td>
				 </tr>';
		$html .= '</table>';
		return $html;
	}

// Delete Product View=============================================================
	function delProductView($get){		
		$imgWithBorder = $this->fetchValue(TBL_PRODUCT_VIEW,"imageWithBorder","product_id = '".$_GET[productId]."' AND view_id='".$_GET[viewId]."'");		
		$imgWithOutBorder = $this->fetchValue(TBL_PRODUCT_VIEW,"imageWithoutBorder","product_id = '".$_GET[productId]."' AND view_id='".$_GET[viewId]."'");						
		$query = "DELETE FROM ".TBL_PRODUCT_VIEW." WHERE 1 AND product_id = '$_GET[productId]' AND view_id = '$_GET[viewId]'"; 
		$rst = $this->executeQry($query);
		$rowAffected=mysql_affected_rows();
		if($rowAffected){
			@unlink(__RAWPRODUCTORIGINAL__.$imgWithBorder);
			@unlink(__RAWPRODUCTTHUMB__.$imgWithBorder);
			@unlink(__RAWPRODUCTLARGE__.$imgWithBorder);
			@unlink(__RAWPRODUCTORIGINAL__.$imgWithOutBorder);
			@unlink(__RAWPRODUCTTHUMB__.$imgWithOutBorder);
			@unlink(__RAWPRODUCTLARGE__.$imgWithOutBorder);	
		}
		echo $rowAffected;
	}
// Add Product View==============================================================
	function addProductViewImages($get){		
		$html = '<table border="0">';
		$html .= '
		<tr>
		<td>&nbsp;</td>
		<td>Image For Tool</td>
		<td><div><input type="file" name="prodImageWithBorder_'.$get['viewId'].'"></div></td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td>Image For Production</td>
		<td><input type="file" id="'.$PfieId.'" name="prodImageWithoutBorder_'.$get['viewId'].'"></td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td>Make this Default&nbsp;</td>
		<td><input type="radio" name="isDefault" value="'.$get['viewId'].'"></td>
		</tr>';
		$html .= '</table>';
		echo $html;
	}
	
//Start : Edit Record============================================================
	function editRecord($post) {
		//echo "<pre>"; print_r($post);print_r($_FILES); echo"</pre>";exit;	
		$_SESSION['SESS_MSG'] = "";		
		if($_SESSION['SESS_MSG'] == "") {		
			//Upload Image==================================================
			if($_FILES["productImage"]['name']){
				$filename = stripslashes($_FILES["productImage"]['name']);
				$extension = findexts($filename);
				$extension = strtolower($extension);
				$productImage = date("Ymdhis").time().rand().'.'.$extension;
				$target= __RAWPRODUCTORIGINAL__.$productImage;			
				if($this->checkExtensions($extension)) {
					$productImageStatus = move_uploaded_file($_FILES["productImage"]['tmp_name'], $target);
					$imgsizearr = getimagesize($target);
					@chmod($target, 0777);
					if($productImageStatus){
							$imgSource = $target;					
							$thumb = __RAWPRODUCTTHUMB__.$productImage;
							$large = __RAWPRODUCTLARGE__.$productImage;
							@chmod(__RAWPRODUCTTHUMB__,0777);
							@chmod(__RAWPRODUCTLARGE__,0777);
							$thumbSize = $this->findSize('THUMB_WIDTH','THUMB_HEIGHT',100,100);
							$largeSize = $this->findSize('LARGE_WIDTH','LARGE_HEIGHT',100,100);
							exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
							exec(IMAGEMAGICPATH." $imgSource -thumbnail $largeSize $large");	
							
							$id=base64_decode($_GET['id']);
							$preImg=$this->fetchValue(TBL_PRODUCT,"productImage","id= '".$id."'");	
							@unlink(__RAWPRODUCTORIGINAL__.$preImg);
							@unlink(__RAWPRODUCTTHUMB__.$preImg);
							@unlink(__RAWPRODUCTLARGE__.$preImg);
					}else{
						$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"Upload Error Occured.");
					}
				}else{
					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"Image Extension Not Allowed.");
				}
				$query = "UPDATE ".TBL_PRODUCT." set productImage='".$productImage."', height = '".$imgsizearr[1]."', width = '".$imgsizearr[0]."', modDate = '".date('Y-m-d H:i:s')."', modBy  = '".$_SESSION['ADMIN_ID']."' WHERE id = '$this->id'";
				$sql = $this->executeQry($query);
			}
		
			//Update Product Table===================================
				
			$query = "UPDATE ".TBL_PRODUCT." set categoryId = '$post[cat]', subCatId = '$post[subCat]', productPrice = '$post[productPrice]', productWeight = '$post[productWeight]' , quantity = '$post[quantity]', orientation = '$post[orientation]', modDate = '".date('Y-m-d H:i:s')."', modBy  = '".$_SESSION['ADMIN_ID']."' WHERE id = '$this->id'";
			$sql = $this->executeQry($query);
			$this->updated_id = $this->id;
			
			//update Product Description Table===================================				
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");	
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$productName = 'productName_'.$line->id;
					$productDesc = 'productDesc_'.$line->id;
                                        $sql = $this->selectQry(TBL_PRODUCT_DESCRIPTION,'1 and  productId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
                                        $numrows = $this->getTotalRow($sql);
                                        if($numrows==0){
                                            $query = "INSERT INTO ".TBL_PRODUCT_DESCRIPTION." set productId = '$post[id]', langId = '".$line->id."', productName = '".addslashes($post[$productName])."', productDesc = '".mysql_real_escape_string($post[$productDesc])."'";	
                                            if($this->executeQry($query)) 
                                                $this->logSuccessFail('1',$query);		
                                            else 	
						$this->logSuccessFail('0',$query);
                                        }else{
                                            $query = "UPDATE ".TBL_PRODUCT_DESCRIPTION." set productName = '".addslashes($post[$productName])."', productDesc = '".mysql_real_escape_string($post[$productDesc])."' WHERE productId = '$this->id' and langId='".$line->id."'";	
                                            if($this->executeQry($query)) 
                                                    $this->logSuccessFail('1',$query);		
                                            else 	
                                                    $this->logSuccessFail('0',$query);
                                        }
					
				}	
			}			
			//Update Product View Table=================================================
			$this->updateProductView($post);			
			echo 	$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been updated successfully");	
		} 
		$_id = base64_encode($this->updated_id );
		header('Location:manageProducts.php');exit;
	}
	
//Start: Update Product View=========================================================
	function updateProductView($post){		
		foreach($_FILES as $key=>$files){
			if($_FILES[$key]['name']){
			  	$fileArr = explode("_",$key);
				$viewId = $fileArr[1]; 
				$filename = stripslashes($_FILES[$key]['name']);
				$extension = strtolower(findexts($filename));
				$extension = strtolower($extension);
				$image_name = date("Ymdhis").time().rand().'.'.$extension;
				$target    = __RAWPRODUCTORIGINAL__.$image_name;				
						
				if($this->checkExtensions($extension)) {	
					$filestatusView = move_uploaded_file($_FILES[$key]['tmp_name'], $target);
					$imgSizeArr = getimagesize($target);
					@chmod($target, 0777);
					if($filestatusView){
						$imgSource = $target;	
                                                // get image ratio ...........................
                                                list($oW,$oH)=getimagesize($imgSource);
                                                $cW=413;
                                                $cH=442;
                                                $minW=$cW/$oW;
                                                $minH=$cH/$oH;
                                                $minRatio=min($minW,$minH);
                                                if($minRatio<1){
                                                $toolImgW= $oW*$minRatio;
                                                $toolImgH= $oH*$minRatio;
                                                }
                                                else{
                                                $toolImgW= $oW;
                                                $toolImgH= $oH;
                                                }
						$thumb = __RAWPRODUCTTHUMB__.$image_name;
						$large = __RAWPRODUCTLARGE__.$image_name;
						@chmod(__RAWPRODUCTTHUMB__,0777);
						@chmod(__RAWPRODUCTLARGE__,0777);
						$thumbSize = $this->findSize('THUMB_WIDTH','THUMB_HEIGHT',100,100);
						$largeSize = $toolImgW."x".$toolImgH;
						exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
						exec(IMAGEMAGICPATH." $imgSource -thumbnail $largeSize $large");
					}
				}
				if($filestatusView){
					if($key == "prodImageWithBorder_".$viewId){
						$columnName = "imageWithBorder";
						$widthCol = "widthWithBorder";
						$hightCol = "heightWithBorder";
					}elseif($key == "prodImageWithoutBorder_".$viewId){
						$columnName = "imageWithoutBorder";
						$widthCol = "widthWithoutBorder";
						$hightCol = "heightWithoutBorder";
					}
					$printwidthname = "printWidth".$viewId;
					$printheightname = "printheight".$viewId;
					$printwidth = $post[$printwidthname];
					$printheight = $post[$printheightname];
					$rstView = $this->executeQry("SELECT id FROM ".TBL_PRODUCT_VIEW." WHERE product_id = '".$this->id."' AND view_id='".$viewId."' ");				
					$numView = $this->getTotalRow($rstView);					
					if($numView > 0){
						$imgeName = $this->fetchValue(TBL_PRODUCT_VIEW,"$columnName","product_id = '".$this->id."' AND view_id='".$viewId."'");
						@unlink(__RAWPRODUCTORIGINAL__.$imgeName);
						@unlink(__RAWPRODUCTTHUMB__.$imgeName);
						@unlink(__RAWPRODUCTLARGE__.$imgeName);
						$query = "update ".TBL_PRODUCT_VIEW." set ".$columnName."='".$image_name."', ".$widthCol." = '".$imgSizeArr[0]."', ".$hightCol." = '".$imgSizeArr[1]."',printwidth='".$printwidth."',printheight='".$printheight."' WHERE product_id = '".$this->id."' AND view_id='".$viewId."' ";
						}
					else
						$query = "INSERT INTO ".TBL_PRODUCT_VIEW." set ".$columnName."='".$image_name."', ".$widthCol." = '".$imgSizeArr[0]."', ".$hightCol." = '".$imgSizeArr[1]."', product_id = '".$this->id."', view_id='".$viewId."',printwidth='".$printwidth."',printheight='".$printheight."' ";
						
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				}else{
						$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
					}
					
			  }
		  }
		  
		//Update Default View In Product View Table==========================================
		$qryDefault = "SELECT * FROM ".TBL_PRODUCT_VIEW." WHERE product_id = '".$this->id."' ";
		$rstDefault = $this->executeQry($qryDefault);
		$numDefault = $this->getTotalRow($rstDefault);
		if($numDefault > 0){
			while($lineDefault = $this->getResultObject($rstDefault)) {
				$isDefault = ($_POST['isDefault'] == $lineDefault->view_id)?"1":"0";
				$query = "update ".TBL_PRODUCT_VIEW." set isDefault ='".$isDefault."' WHERE product_id = '".$this->id."' AND view_id='".$lineDefault->view_id."' ";			
				if($this->executeQry($query)) 
					$this->logSuccessFail('1',$query);		
				else 	
					$this->logSuccessFail('0',$query);
			}
		
		}
		///////////////////  UPDATE ISDEFAULT        ////////////////////
  }
	
//Start : Get Result===========================================
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_PRODUCT." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect("manageProducts.php");
		}	
	}
//Start : Change Status========================================	
	function changeValueStatus($get) {
                //echo TBL_PRODUCT,"status","1 and id = '$get[id]'";exit;
		$status=$this->fetchValue(TBL_PRODUCT,"status","1 and id = '$get[id]'");
		
		if($status==1) {
			$stat= 0;
			$status="InActive";
		} else 	{
			$stat= 1;
			$status="Active";
		}
               
		$query = "update ".TBL_PRODUCT." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
	
	function subCategorySingle($pid,$dashes,$id) {
		//echo $id;
		$dash = str_repeat('&nbsp;',$dashes);
		 $bbb = "select a.id,ad.categoryName from ".TBL_CATEGORY." as a, ".TBL_CATEGORY_DESCRIPTION." as ad where 1 and a.parent_id = '".$pid."' and a.status = '1' and a.isDeleted = '0' and a.id = ad.catId and ad.langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by a.id";
		$sql = $this->executeQry($bbb);
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			while($line = $this->getResultObject($sql)) {
			    if($this->checkForCategory($line->id)){				
					$sel = ($id == $line->id)?'selected="selected"':'';
					$genTable .= '<option value="'.$line->id.'" '.$sel.'>'.$dash.stripslashes($line->categoryName).'</option>';
					$genTable .= $this->subCategorySingle($line->id,($dashes + 1),$id);
				}
			}	
			return $genTable;
		} else {
			return false;
		}		
	}
	
	/*function getCategoryListSingle($id) {
	    
		$genTable = "";
		$aaaa = "select a.id,ad.categoryName from ".TBL_CATEGORY." as a, ".TBL_CATEGORY_DESCRIPTION." as ad where 1 and a.parent_id = 0 and a.status = '1' and a.isDeleted = '0' and a.id = ad.catId and ad.langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by a.id";       
		//echo $aaaa;
		//echo $id;
		//exit;
		$sql = $this->executeQry($aaaa);
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			while($line = $this->getResultObject($sql)) {	
				if($this->checkForCategory($line->id)){					
					$sel = ($id == $line->id)?'selected="selected"':'';
					$genTable .= '<option value="'.$line->id.'" '.$sel.'>'.stripslashes($line->categoryName).'</option>';
					$genTable .= $this->subCategorySingle($line->id,1,$id);
				}
			}	
		}
		return $genTable;			
	}*/
	
	function checkForCategory($id){
	    $num = 0;
		$query = "select p.* from ".TBL_PRODUCT." as p left join ".TBL_PRODUCT_VIEW." pp  on p.id = pp.product_id  where p.categoryId = '".$id."' and  p.isDeleted = '0' ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		return $num;
	}
	
	
	
	
	
	
	function fetchValue_Edit($valueFind , $productId , $viewId){
		$query  = "select ".$valueFind." as findValue from ".TBL_PRODUCT_VIEW." where product_id = '".$productId."' and  view_id = '".$viewId."' limit 0 , 1 ";
		$resu = $this->executeQry($query);
		$line = $this->getResultObject($resu);
		return $line->findValue;
	}


	function deleteAllValues($post){
	  
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageProducts.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(TBL_PRODUCT," path like '%-$val-%'");	
				 $this->executeQry("update ".TBL_PRODUCT." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$val'");
					
					/*$result=$this->deleteRec(TBL_PRODUCT_ATTRIBUTE,"productId='$val'");
					echo "hello";*/
				   	
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
					echo "<script language=javascript>window.location.href='manageProducts.php?page=$post[page]&limit=$post[limit]';</script>";
			
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_PRODUCT,"cat_id='$val'");	
					$sql="update ".TBL_PRODUCT." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_PRODUCT,"cat_id='$val'");	
					$sql="update ".TBL_PRODUCT." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageProducts.php?cid=$post[cid]&page=$post[page]';</script>";
	}	
	
	
	
	

	
	function getProductColorId($pid){
		return $this->fetchValue(TBL_PRODUCT_COLOR,"id","product_id='$pid'");	
	}
	
	
	/*function getProductColors($pid){
		$color = array();
		$sql = "SELECT colorCode,tbl1.id,colorTitle  FROM  ".TBL_PRODUCT_COLOR." AS tbl1 INNER JOIN  ".TBL_PRODUCT_COLOR_DESCRIPTION." AS tbl2 ON (tbl1.id = tbl2.product_color_id) WHERE product_id ='$pid' AND   	
langId='".$_SESSION['DEFAULTLANGUAGE']."' ORDER BY tbl1.sequence asc";
		$rst = $this->executeQry($sql);
		while($row = $this->getResultObject($rst)){
			$color[] = $row;
		}
		return $color;	
	}*/
	
	
	
	function isProductNameExist($productname,$langId,$id=''){
		$productname = addslashes(trim($productname));
                //$query="select MP.id, MPD.productName from ".TBL_MAINPRODUCT." as MP inner join ".TBL_PRODUCT_DESCRIPTION." as MPD on (MP.id=MPD.productId) where MPD.productId!='".$id."' and MPD.langId='".$langId."' and MPD.productName='".$productname."' and MP.isDeleted='0'";
		$rst = $this->selectQry(TBL_PRODUCT_DESCRIPTION,"langId = '".$langId."' and productName='".$catname."' AND productId!='$id'  ","","");
		//$rst=$this->executeQry($query);
                $row = $this->getTotalRow($rst);
		return $row;
	}
	
	function getAttribute(){
		$query = "SELECT ea.* , eat.* FROM ".TBL_ATTRIBUTE." AS ea LEFT JOIN ".TBL_ATTRIBUTE_DESCRIPTION." AS eat ON ea.id = eat.attributeId WHERE ea.status = '1' AND ea.isDeleted = '0' AND eat.langId = '".$_SESSION['DEFAULTLANGUAGE']."'order by eat.attributeName";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
	    $j=0;
		if($num > 0){
		    echo '<font style="padding-left:0px;" id="selectval12" >';
			echo '<input type="checkbox" name="selectval" id="selectval" onclick="selectAllBox();" />Select All';
			echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Price</font>';
		   while($line = $this->getResultObject($sql)) {
				$res = "SELECT av.* , avs.* FROM ".TBL_ATTRIBUTE_VALUES." AS av LEFT JOIN ".TBL_ATTRIBUTE_VALUES_DESCRIPTION." AS avs ON av.id = avs.attributeValuesId WHERE av.attributeId = '".$line->id."' AND  av.status = '1' AND av.isDeleted = '0' AND avs.langId = '".$_SESSION['DEFAULTLANGUAGE']."'order by avs.attributeValuesName";
				$resul = $this->executeQry($res);
				$row = $this->getTotalRow($resul);
				echo '<table width="50%" border="0" align="center"   >';
				echo '<font style="padding-left:145px; width:auto;">';
				echo '<input type="checkbox"  name="attribute[]" value="'.$line->id.'" id="'.$line->id.'menuCheck" onclick="checkAllcheckbox('.$line->id.','.$row.');" />'.stripslashes(stripslashes($line->attributeName));
				echo '</font>';
				echo '<br>';
			    $i=0;
				if($row > 0){
					while($result = $this->getResultObject($resul)) {
					    echo '<tr><td width="50%" align="left" >';
						echo '<font style="padding-left:20px;">';
						echo '<input type="checkbox"  name="attributevalue[]" value="'.$result->id.'" id="'.$line->id.'mCheck'.$i.'" />'.stripslashes(stripslashes($result->attributeValuesName));
						echo '</font>';
						echo '</td><td>';
						echo '<font style="padding-left:20px; width:auto;">';
						echo '<input type="text" name="price'.$result->id.'" size="6" maxlength="6"  />';
						echo '</font>';
						echo '</td></tr>';
						$i++;
				    }
				    $j++;
			    }
				echo '</table>';
		    }
		 }
	}	
	
	function getAttributeEdit($attributeValueId,$attributeId,$price){
	  
		$query = "SELECT ea.* , eat.* FROM ".TBL_ATTRIBUTE." AS ea LEFT JOIN ".TBL_ATTRIBUTE_DESCRIPTION." AS eat ON ea.id = eat.attributeId WHERE ea.status = '1' AND ea.isDeleted = '0' AND eat.langId = '".$_SESSION['DEFAULTLANGUAGE']."'order by eat.attributeName";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$j=0;
		if($num > 0){
			echo '<font >';
			echo '<input type="checkbox" name="selectval" id="selectval" onclick="selectAllBox();" />Select All';
			echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Price</font>';
		    while($line = $this->getResultObject($sql)) {
				$res = "SELECT av.* , avs.* FROM ".TBL_ATTRIBUTE_VALUES." AS av LEFT JOIN ".TBL_ATTRIBUTE_VALUES_DESCRIPTION." AS avs ON av.id = avs.attributeValuesId WHERE av.attributeId = '".$line->id."' AND  av.status = '1' AND av.isDeleted = '0' AND avs.langId = '".$_SESSION['DEFAULTLANGUAGE']."'order by avs.attributeValuesName";
				$resul = $this->executeQry($res);
				$row = $this->getTotalRow($resul);
				if(count($attributeId) > 0 )
			      foreach($attributeId as $selectedId){
					 if($selectedId == $line->id){
						$checked="checked";
						break;
				     }else{
						$checked=""; 
					 }
				 }
				 echo '<table width="50%" border="0" align="center" style="padding-left:20px;"   >';
				 echo '<font style="padding-left:145px;">';
				 echo '<input type="checkbox"  name="attribute[]" '.$checked.' value="'.$line->id.'" id="'.$line->id.'menuCheck" onclick="checkAllcheckbox('.$line->id.','.$row.');" />'.stripslashes($line->attributeName);
				 echo '</font>';
			     $i=0;
				 if($row > 0){
				      while($result = $this->getResultObject($resul)) {
					   echo '<font style="padding-left:20px;">';
					    if(count($attributeValueId) > 0 )
						   foreach($attributeValueId as $attributeselectedId){
							 if($attributeselectedId == $result->id){
								  $checked="checked";
								  break;
							 }else{
								  $checked="";
							 }
						   }
						   echo '<tr><td width="30%" align="left" >';
						   echo '<input type="checkbox"  name="attributevalue[]" '.$checked.' value="'.$result->id.'" id="'.$line->id.'mCheck'.$i.'" />'.stripslashes(stripslashes($result->attributeValuesName));
						   echo '</font>';
						   echo '</td><td>';
						   echo '<font  width:auto;">';
						   echo '<input type="text" name="price'.$result->id.'" value="'.$price['price'.$result->id].'" size="6" maxlength="6" onkeyup="return isNum13(this.value,'.$result->id.');"   />';
						   echo '<p style="padding-left:150px;" id="price'.$result->id.'">';
						   echo '</p>';
						   echo '</font>';
						   echo '</td></tr>';
						   $i++;
					     }
					     $j++;
				       }
				       echo '</table>';
			      }
		     }
	   }
	
	
	
	
	function sortProductsOrder($get){
/*	   echo "<pre>";
	     print_r($get);
	   echo "<pre>";*/
	  $sortedArr 	=   explode("=",$get['url']);
	  $sortedIdArr	=	$sortedArr[1];
	  $Sorted_Order_Arr = explode(",",$sortedIdArr);
	  $Current_Order_Arr = explode(",",substr_replace($get['Current_Order'],"",-1));
	  	  
	  $i = 0;
	  $sorted_key_arr	=	array();	 
	  foreach($Sorted_Order_Arr as $Sorted_Order_Val){
	     $current_id	=	$Current_Order_Arr[$i];
		 $sql_string	=	"SELECT sequence FROM ".TBL_PRODUCT." WHERE id = '$current_id'";
         $query 	 	=	mysql_query($sql_string);
		 if($line = mysql_fetch_object($query)){
		    $sorted_key_arr[$Sorted_Order_Val] = $line->sequence;
		 }
		 $i++;
       }
	 
	  foreach($sorted_key_arr as $key=>$val){  
	        $sql_string		=	"UPDATE ".TBL_PRODUCT." SET sequence = '$val' WHERE id = '$key'";
		   	mysql_query($sql_string);   
	   }

	   $cond = "1 and ".TBL_PRODUCT.".id = ".TBL_PRODUCT_DESCRIPTION.".productId and ".TBL_PRODUCT.".isDeleted = '0' and ".TBL_PRODUCT_DESCRIPTION.".langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
	   $cond .= " group by ".TBL_PRODUCT.".categoryId, ".TBL_PRODUCT.".sequence";
	   $query = "select ".TBL_PRODUCT.".*,".TBL_PRODUCT_DESCRIPTION.".productName from ".TBL_PRODUCT." , ".TBL_PRODUCT_DESCRIPTION." where $cond ";
	   $orderby = $_GET[orderby]? $_GET[orderby]:TBL_PRODUCT.".sequence";
	   $order = $_GET[order]? $_GET[order]:"ASC";   
       //$query .=  " ORDER BY $orderby $order ";
			$paging = $this->paging($query); 
			$this->setLimit($get['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($get['page']); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();	

           $query .=  " LIMIT ".$offset.", ". $recordsPerPage;
            $rst = $this->executeQry($query);  

		  while($line = mysql_fetch_object($rst)){
		     $currentOrder .= $line->id.",";
		  }
	   	  ?><input type="hidden" name="currentOrder" id="currentOrder" value="<?=$currentOrder?>" /><?php
	}
	
	
	function getColorViewImageUploadEdit($id,$colorId,$jj) {
		//echo $id;
	    $query = "select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '1' order by ".TBL_VIEW.".  	
sequence asc";
		$sql2 = $this->executeQry($query);
		$num2 = $this->getTotalRow($sql2);
	//	$genTable .= '<li><input type="hidden" name="totalProductImages" value="'.$num2.'"></li>';
	
		$j = 1;
		while($line2 = $this->getResultObject($sql2)) {
		//echo $j;
				$sqlView  = $this->executeQry("select * from ".TBL_PRODUCT_VIEW." where 1 and product_id = '$id' and view_id = '".$line2->id."' AND color_id='$colorId'" ); 		
				$lineView = $this->getResultObject($sqlView);
		
			$genTable .= ' <div class="admin-product-sub">
            	<ul>';
				$genTable .= "<li id='imgdiv".$lineView->id."' style='hight:100px;'>";
					if($lineView->imageName){
					$genTable .= '<img src="'.__PRODUCTTHUMB__.trim($lineView->imageName) .'" border="0" /><i><a href="javascript:;" onClick="deleteRawProdImg('.$id.','.$colorId.','.$lineView->id.')"><img src="images/close_small.png" alt="Close" title="Close" border="0" /></a></i>';
					 }else{
					$genTable .= '<img src="images/productnotfound.jpg" border="0" width="100" height="100"/>'; 
					 }
				$genTable .= '</li>';
				$genTable .= '<li class="front">'.$this->ChangeViewName($colorId,$line2->id).'</li>
                    <li><input type="file" name="prodImage'.$line2->id.'" value="" /></li>';
				if($jj == 0){
					$genTable .= "<li class='front'>Print Size</li><li><input type='text' name='printWidth".$line2->id."' size='3' value='".$lineView->printWidth."'><li><li class='front-size'>X</li>";	
					$genTable .= " <li><input type='text' name='printHeight".$line2->id."' size='3' value='".$lineView->printHeight."'></li>";		              
				}
				$genTable .= '<input type="hidden" name="viewId'.$line2->id.'" value="'.$line2->id.'">';							
				//$checked = "";	
				if($lineView->isDefault == '1') { $checked = 'checked="checked"'; }	else { $checked = ""; }
				$genTable .= "<li class='default'>Make this Default<span><input type='radio' name='isDefault$colorId' value='".$line2->id."' $checked></span></li>";
				$genTable .= '<li style="clear:both;"></li><li></li>
                </ul>
            </div>';	
			$j++;
			} 
			$genTable .= '<div align="center"><input type="button" name="" value="" class="classsubmit" onClick="imageFormSubmit(this.form);" /></div>';
			
		return $genTable;
	
	}
	
	function ChangeViewName($colorId,$viewId){
	
		$query12 =  "select * from ".TBL_PRODUCT_VIEW." where view_id 	 = '".$viewId."' and color_id = '".$colorId."' ";
		$sql23 = $this->executeQry($query12);
		$line23 = $this->getResultObject($sql23);
	
		$query = "select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '1' order by ".TBL_VIEW.".  	
sequence asc";
		$sql2 = $this->executeQry($query);
		$preTable = '<select name="viewName" onchange="change_view(this.value,'.$colorId.','.$line23->id.');" >';
		while($line = $this->getResultObject($sql2)){
			$sel = '';
			if($line->id == $viewId)
				$sel = "selected = 'selected'";
			$preTable .= '<option value="'.$line->id.'" '.$sel.' >'.$line->viewName.'</option>';
		}
		$preTable .= "</select>";
		return $preTable;
	}
	
	function updateRowProductView($get){
		//print_r($get);
		$query = "update ".TBL_PRODUCT_VIEW." set view_id = '".$get['viewId']."' where id 	 = '".$get['currentId']."' ";
		$query = $this->executeQry($query);
	
	}
	
	function getColorViewImageUploadView($id,$colorId) {
		$sql2 = $this->executeQry("select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '1' order by ".TBL_VIEW.".id asc");
		$num2 = $this->getTotalRow($sql2);
	//	$genTable .= '<li><input type="hidden" name="totalProductImages" value="'.$num2.'"></li>';
	
		$j = 1;
		while($line2 = $this->getResultObject($sql2)) {
		
				$sqlView  = $this->executeQry("select * from ".TBL_PRODUCT_VIEW." where 1 and product_id = '$id' and view_id = '".$line2->id."' AND color_id='$colorId'" ); 		
				$lineView = $this->getResultObject($sqlView);
		
			$genTable .= ' <div class="admin-product-sub">
            	<ul>';
				$genTable .= "<li id='imgdiv".$lineView->id."' style='hight:100px;'>";
					if($lineView->imageName){
					$genTable .= '<img src="'.__PRODUCTTHUMB__.trim($lineView->imageName) .'" border="0" />';
					 }else{
					$genTable .= '<img src="images/productnotfound.jpg" border="0" width="100" height="100"/>'; 
					 }
				$genTable .= '</li>';
				//$checked = "";	
				if($lineView->isDefault == '1') { $checked = 'Yes'; }	else { $checked = "No"; }
				$genTable .= "<li class='default'>Default :<span>$checked</span></li>";
				$genTable .= '<li style="clear:both;"></li><li></li>
                </ul>
            </div>';	
			$j++;
			} 
			
			
		return $genTable;
	
	}
	
	
	function deleteRawImage($get){
		$sqlView  = $this->executeQry("select * from ".TBL_PRODUCT_VIEW." where 1 and id = '$_GET[viewid]'" ); 		
		$lineView = $this->getResultObject($sqlView);	
		$image = $lineView->imageName;
			if($image){
			@unlink(__PRODUCTTHUMB__.$image);
			@unlink(__PRODUCTPATH__.$image);
			}
		$sqlView  = $this->executeQry("DELETE from ".TBL_PRODUCT_VIEW." where 1 and id = '$_GET[viewid]'" );
		echo '<img src="images/productnotfound.jpg" border="0" width="100" height="100"/>';
	}
	
	
	function deleteRawColorImage($get){
	
	$_GET[productId] = base64_decode($_GET[productId]);
		$sqlView  = $this->executeQry("select * from ".TBL_PRODUCT_VIEW." where 1 and product_id = '$_GET[productId]' AND color_id='$_GET[colorId]'" ); 		
		while($lineView = $this->getResultObject($sqlView)){
			$image = $lineView->imageName;
			if($image){
			@unlink(__PRODUCTTHUMB__.$image);
			@unlink(__PRODUCTPATH__.$image);
			}
		}	
		$sqlView  = $this->executeQry("DELETE from ".TBL_PRODUCT_VIEW." where 1 and product_id = '$_GET[productId]' AND color_id='$_GET[colorId]'" );
		
		$sqlView  = $this->executeQry("DELETE from ".TBL_PRODUCT_COLOR." where 1 and id='$_GET[colorId]'" );
		$sqlView  = $this->executeQry("DELETE from ".TBL_PRODUCT_COLOR_DESCRIPTION." where 1 and product_color_id='$_GET[colorId]'" );
		$sqlSequence  = $this->executeQry("update  ".TBL_PRODUCT_COLOR." set sequence =  sequence - 1  where 1 and product_id = '$_GET[productId]' AND id > '$_GET[colorId]'" );
	}
	
	
	function updateRawColorProduct($post,$file){
	    
		$productId = $post['productId'];
		$colorId = $post['colorId'];
		$sql3 = $this->executeQry("update ".TBL_PRODUCT_COLOR." set price='".$post['price']."' , colorCode = '".$post['colorCode']."' where id='".$colorId."'");
		
		$sql3 = $this->executeQry("update ".TBL_PRODUCT_COLOR_DESCRIPTION." set colorTitle='".$post['colorName']."'  where product_color_id='".$colorId."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");
		//print_r($post);
		$sql2 = $this->executeQry("select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '1' order by ".TBL_VIEW.".id asc");
		$num2 = $this->getTotalRow($sql2);
		$j = 1;
			while($line2 = $this->getResultObject($sql2)) {
			$printWidth = $post['printWidth'.$line2->id];
			$printHeight = $post['printHeight'.$line2->id];
			$makeDefaultId = $post['isDefault'.$colorId];
			$imageName = "prodImage".$line2->id;
			
		//	print_r($file);	
		$sql = "SELECT id,imageName FROM ".TBL_PRODUCT_VIEW." WHERE product_id='$productId' AND color_id='$colorId' AND   	
view_id='".$line2->id."'";	
				$rst = $this->executeQry($sql);
				$num = $this->getTotalRow($rst);
				$line = $this->getResultObject($rst);
				if($num){
						///// update image and other info	
						$query2 = "UPDATE ".TBL_PRODUCT_VIEW." set  printWidth = '".$printWidth."', printHeight = '".$printHeight."'";	
							if($makeDefaultId == $line2->id) {
								$query3 = "UPDATE ".TBL_PRODUCT_VIEW." set  isDefault = '0' where product_id='".$productId."' AND color_id='".$colorId."' and view_id != '".$makeDefaultId."'";	
								$this->executeQry($query3);	
								$query2 .= ", isDefault = '1'";
							}
							
							if($file[$imageName]['name']){
							$filename = stripslashes($file[$imageName]['name']);
							$extension = findexts($filename);
							$extension = strtolower($extension);
					
							$image_name = date("Ymdhis").time().rand().'.'.$extension;
							$target    = __PRODUCTORIGINAL__.$image_name;
							if($this->checkExtensions($extension)) {	
								$filestatus = 	move_uploaded_file($file[$imageName]['tmp_name'], $target);
								chmod($target, 0777);	
								if($filestatus){
									$imgSource = $target;	 
									$LargeImage = __PRODUCTLARGE__.$image_name;
									$ThumbImage = __PRODUCTTHUMB__.$image_name;
									chmod(__PRODUCTORIGINAL__,0777);
									chmod(__PRODUCTTHUMB__,0777);
									$fileSize = $this->findSize('PRODUCT_THUMB_WIDTH','PRODUCT_THUMB_HEIGHT',100,100);
									exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize $ThumbImage");
									
									$fileSize2 = $this->findSize('PRODUCT_LARGE_WIDTH','PRODUCT_LARGE_HEIGHT',250,250);
									exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize2 $LargeImage");
									
									$query2 .= ", imageName = '$image_name'";
									
									}
								}
							}
						$query2 .= " WHERE  id='".$line->id."'";			
						$this->executeQry($query2);
						
						$query22 = "UPDATE ".TBL_PRODUCT_VIEW." set  printWidth = '".$printWidth."', printHeight = '".$printHeight."' where product_id = '".$productId."' and view_id = '".$line2->id."'";				
						$this->executeQry($query22);							
				}else{
						//// if view id not exists/////
					if($file[$imageName]['name']){
					$filename = stripslashes($file[$imageName]['name']);
					$extension = findexts($filename);
					$extension = strtolower($extension);
			
					$image_name = date("Ymdhis").time().rand().'.'.$extension;
					$target    = __PRODUCTORIGINAL__.$image_name;
					if($this->checkExtensions($extension)) {	
						$filestatus = 	move_uploaded_file($file[$imageName]['tmp_name'], $target);
						chmod($target, 0777);	
						if($filestatus){
							$imgSource = $target;	 
							$LargeImage = __PRODUCTLARGE__.$image_name;
							$ThumbImage = __PRODUCTTHUMB__.$image_name;
							chmod(__PRODUCTORIGINAL__,0777);
							chmod(__PRODUCTTHUMB__,0777);
							$fileSize = $this->findSize('PRODUCT_THUMB_WIDTH','PRODUCT_THUMB_HEIGHT',100,100);
							exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize $ThumbImage");
							
							$fileSize2 = $this->findSize('PRODUCT_LARGE_WIDTH','PRODUCT_LARGE_HEIGHT',250,250);
							exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize2 $LargeImage");
							
							$query2 = "insert into ".TBL_PRODUCT_VIEW." set product_id = '$productId', color_id = '".$colorId."', view_id = '".$line2->id."', imageName = '".addslashes(trim($image_name))."', printWidth = '".$printWidth."', printHeight = '".$printHight."'";	
							if($makeDefaultId == $line2->id) {
								$query2 .= ", isDefault = '1'";
							}
							$this->executeQry($query2);							
						}								
					}
				}
			}
			
		}//while
	$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been updated successfully!!!");
	header("Location:editProduct.php?id=".base64_encode($productId));
	echo "<script language=javascript>window.location.href='editProduct.php?id=".base64_encode($productId)."';< /script>";
	exit;
	}// function
	
	function getMaxSequence($id){
		$sql = "select max(sequence) as maxvalue from ".TBL_PRODUCT_COLOR." where product_id='".$id."' " ;
	    $rst = $this->executeQry($sql);
		$line = $this->getResultObject($rst);
		//echo $line->maxvalue."urvesh";
		return $line->maxvalue+1;
	}
	
	function getProductSize(){
	    //echo $productid;
		$query = "select ".TBL_SIZE.".*,".TBL_SIZEDESC.".sizeName from ".TBL_SIZE.", ".TBL_SIZEDESC." where 1 and ".TBL_SIZE.".status  = '1' and ".TBL_SIZE.".id = ".TBL_SIZEDESC.".Id and ".TBL_SIZEDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_SIZE.".id asc";
		$sql2 = $this->executeQry($query);
		$num2 = $this->getTotalRow($sql2);
		$table = '<div style="padding:5px;margin:1px 0px 0px 0px;list-style:none; width:auto;"> <div style="width:750px;">';
		
		while($line2 = $this->getResultObject($sql2)){
			$arrayId[] = $line2->id;
			$table .= '<div style="width:90px;float:left;margin:5px 0px 0px 20px;"><div style="width:90px;">'.$line2->sizeName . '<input type = "checkbox" name="size'.$line2->id.'" "'.$checked.'" /></div><div style="width:90px;">Price:<input type="text" name="price'.$line2->id.'" value="'.$price.'" size=4 />' ;
			if($this->fetchValue(TBL_SYSTEMCONFIG,'systemVal',"systemName='ALLOW_WEIGHT'") == 2)
				$table .= 'Weight:<input type="text" name="weight'.$line2->id.'" value="" size=4 />';
				$table .='</div></div>';
		}
	
		$table .= '<div style="clear:both;"></div>
        </div>';
		//$table .= '<div id="ttfFileUpload" style="width:465px;"><li >
                  // <br/><input type="text" name="attributeName1"  value="" id="m__Size__Detail" size = "5"/>';  
					for($i = 0; $i< $num2 ; $i++){
					    //$table .="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						//$table .= '<input type="text" name="attributeValue_'.$arrayId[$i].'_1" size="5" />';
					}
					//$table .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				 echo $table ;
	}
	
	function updateProductSize($post){
	   
	    
	    $sql1 = $this->executeQry("delete from ".TBL_SIZEPRICE." where productId = '".$post[productId]."'");
		$sql1 = $this->executeQry("delete from ".TBL_SIZEATTRIBUTE." where productId = '".$post[productId]."'");
		$sql1 = $this->executeQry("delete from ".TBL_MESURMENT." where pid  = '".$post[productId]."'");
	    $query = "select ".TBL_SIZE.".*,".TBL_SIZEDESC.".sizeName from ".TBL_SIZE.", ".TBL_SIZEDESC." where 1 and ".TBL_SIZE.".status  = '1' and ".TBL_SIZE.".id = ".TBL_SIZEDESC.".Id and ".TBL_SIZEDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_SIZE.".id asc";
		$sql2 = $this->executeQry($query);
		$num2 = $this->getTotalRow($sql2);
		while($line2 = $this->getResultObject($sql2)){
			$arrayId[] = $line2->id;
		 	$sizeId[] = $line2->id;
		}
		for($kw = 1; $kw <= $post[theValue]; $kw++){
			$attributeName = "attributeName".$kw;
			if($post[$attributeName] != ''){
				$querymesurment = "insert into ".TBL_MESURMENT." set name  = '".$post[$attributeName]."',  pid  = '".$post[productId]."'";
				$sql = $this->executeQry($querymesurment);
				$mesurmentId[] = mysql_insert_id();
			}
		}
		$i=0;
		$j = $arrayId[$i];
		while($arrayId[$i] ){
			$sizeid = "size".$j;
			$priceid = "price".$arrayId[$i];
			$weightid = "weight".$arrayId[$i];
		    if( $post[$priceid] != '' && $post[$sizeid]){
				if($post[$weightid])
					$weight = " , weight = '".$post[$weightid]."'";
				$query = "insert into ".TBL_SIZEPRICE." set productId = '".$post[productId]."', sizeId = '$arrayId[$i]', price = '$post[$priceid]' $weight ";
				$sql = $this->executeQry($query);
			}
			for($kk = 1,$m=0; $kk <= $post[theValue]; $kk++,$m++){
				$attributeName = "attributeName".$kk;
		 		if($post[$attributeName] != '' && $post[$sizeid] ){
					$attributeValue = "attributeValue_".$j."_".$kk;
		 			$query = "insert into ".TBL_SIZEATTRIBUTE." set mesurment = '".$mesurmentId[$m]."', sizeId = '".$j."',  productId  = '".$post[productId]."' , lineValue = '".$post[$attributeValue]."'";
					$sql = $this->executeQry($query);
				//$insert_id = mysql_insert_id();
				}
		    }
			$i++;
		 	$j=$arrayId[$i];
		}
		//exit;
		echo "<script>window.location.href='editProduct.php?id=".base64_encode($post[productId])."&page=$post[page]'</script>";
	}
	
	function getAttributePrice($id , $productid){
		$query = "select price from ".TBL_SIZEPRICE." where sizeId='$id' and productId = '$productid'";
		$sql = $this->executeQry($query);
		$line2 = $this->getResultObject($sql);
		return  $line2->price;
	}
	
	function getAttributeweight($id , $productid){
	
		$query = "select weight from ".TBL_SIZEPRICE." where sizeId='$id' and productId = '$productid'";
		$sql = $this->executeQry($query);
		$line2 = $this->getResultObject($sql);
		return  $line2->weight;
	
	}
	
	
	function getAttributeName($productId){
	
	
	    $query = "select ".TBL_SIZE.".*,".TBL_SIZEDESC.".sizeName from ".TBL_SIZE.", ".TBL_SIZEDESC." where 1 and ".TBL_SIZE.".status  = '1' and ".TBL_SIZE.".id = ".TBL_SIZEDESC.".Id and ".TBL_SIZEDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_SIZE.".id asc";
		$sql2 = $this->executeQry($query);
		$num2 = $this->getTotalRow($sql2);
		$table = '<div style="padding:10px;margin:5px 0px 0px 30px;">
		<div style="width:850px;">';
		if($type != 'edit'){
			while($line2 = $this->getResultObject($sql2)){
		 		$arrayId12[] = $line2->id;
		 		$price = $this->getAttributePrice($line2->id,$productId);
				$weight = $this->getAttributeweight($line2->id,$productId);
		 		if($price)
		 			$checked = "checked = 'checked'";
		 	    else
		 	    	$checked = "";
         		$table .= '<div style="width:90px;float:left;margin:5px 0px 0px 50px;"><div style="width:90px;">'.$line2->sizeName . '<input type = "checkbox" name="size'.$line2->id.'" "'.$checked.'" /></div><div style="width:90px;">Price:<input type="text" name="price'.$line2->id.'" value="'.$price.'" size=4 />' ;
				if($this->fetchValue(TBL_SYSTEMCONFIG,'systemVal',"systemName='ALLOW_WEIGHT'") == 2)
					$table .= 'weight:<input type="text" name="weight'.$line2->id.'" value="'.$weight.'" size=4 />';
					$table .='</div></div>';
			}
		  }
		  $table .='</div>';
		$queryValue = "SELECT * FROM ".TBL_MESURMENT." WHERE pid = '".$productId."'";
		$sqlValue = $this->executeQry($queryValue);
		$num = $this->getTotalRow($sqlValue);
		if($num){
		//$table .='<div style="float:left; padding-top:10px;padding-top:10px;width:650px; "><div >';
			$j=1;
			while($line3 = $this->getResultObject($sqlValue)){
				//$table .= '<input type="text" name="attributeName'.$j.'"  value="'.$line3->name.'" id="m__font__Image" size = "5"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			    foreach($arrayId12 as $arrid){
					$query1 = "select * from ".TBL_SIZEATTRIBUTE." where productId = '".$productId."' and mesurment ='".$line3->id."' and sizeId = '".$arrid."'";
					$sqlValue1 = $this->executeQry($query1);
					$line4 = $this->getResultObject($sqlValue1);
						//echo $line4->lineValue.",";
					//$table .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="attributeValue_'.$arrid.'_'.$j.'" value="'.$line4->lineValue.'" size="5" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				//$table .= "</div>";
				$j++;
			}
			
		}
		if($num == 0){
			//echo $this->getProductSize('','','');
			$k = $num+1;
			//$table .= '<div id="ttfFileUpload" style="padding-top:80px;"; ><input type="text" name="attributeName'.$k.'"  value="" id="m__font__Image" size = "5"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';  
			for($ii = 0; $ii< count($arrayId12); $ii++){
				//$table .="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				//$table .= '<input type="text" name="attributeValue_'.$arrayId12[$ii].'_'.$k.'" size="5" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			}
		}
		if($num != 0){ 
			$k = $num+1;
			//$table .= '<br/><div id="ttfFileUpload">
             //       <input type="text" name="attributeName'.$k.'"  value="" id="m__font__Image" size = "5"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';  
			for($ii = 0; $ii< count($arrayId12); $ii++){
				//$table .="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				//$table .= '<input type="text" name="attributeValue_'.$arrayId12[$ii].'_'.$k.'" size="5" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			}
			//$table .='</div>';
		}
		//$table .='</div></div>';
		echo $table;
	}
	
	
	
	function findHiddenValue($productId){
		    $queryValue = "SELECT * FROM ".TBL_MESURMENT." WHERE pid = '".$productId."'";
			$sqlValue = $this->executeQry($queryValue);
			$num = $this->getTotalRow($sqlValue);
			return $num;
	
	}
	
	function getProductArray(){
	
		$query = "select ".TBL_SIZE.".*,".TBL_SIZEDESC.".sizeName from ".TBL_SIZE.", ".TBL_SIZEDESC." where 1 and ".TBL_SIZE.".status  = '1' and ".TBL_SIZE.".id = ".TBL_SIZEDESC.".Id and ".TBL_SIZEDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_SIZE.".id asc";
		$sql2 = $this->executeQry($query);
		$num2 = $this->getTotalRow($sql2);
		while($line2 = $this->getResultObject($sql2)){
			$arrayId[] = $line2->id;
		}
		
		if(count($arrayId) > 0)
			$aary = implode(",",$arrayId);
		else
			$aary = $arrayId;
		return $aary;
	}
	
	function getProductArraySize(){
		$query = "select ".TBL_SIZE.".*,".TBL_SIZEDESC.".sizeName from ".TBL_SIZE.", ".TBL_SIZEDESC." where 1 and ".TBL_SIZE.".status  = '1' and ".TBL_SIZE.".id = ".TBL_SIZEDESC.".Id and ".TBL_SIZEDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_SIZE.".id asc";
		$sql2 = $this->executeQry($query);
		$num2 = $this->getTotalRow($sql2);
		return $num2;
	}
	
	function getCompanyForDevice($get){
		
		$sql = "SELECT tbl1.id, tbl2.categoryName FROM ".TBL_CATEGORY." AS tbl1 INNER JOIN ".TBL_CATEGORY_DESCRIPTION." AS tbl2 ON (tbl1.id = tbl2.catId AND tbl1.isDeleted='0') WHERE 1=1 AND tbl2.langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND tbl1.parent_id != '0' AND tbl1.path like '%-".$get['deviceId']."-%'";
		$query = $this->executeQry($sql);
		$num = $this->getTotalRow($query);
		$tbl = '';
		$tbl = '<select name="deviceCompany" id="m__Company">'
				.'<option value="">Please Select Company</option>';
		if($num > 0){
			while($line = $this->getResultObject($query)){
				$sel = ($line->id == $_POST['deviceCompany']) ? "selected":"";
				$tbl .= '<option value="'.$line->id.'" '.$sel.'>'.$line->categoryName.'</option>';
			}
		}
		$tbl .= '</select>';
	echo $tbl; 	
	}
	
	function getCompanyForDevicePOST($deviceId){
	 $sql = "SELECT tbl1.id, tbl2.categoryName FROM ".TBL_CATEGORY." AS tbl1 INNER JOIN ".TBL_CATEGORY_DESCRIPTION." AS tbl2 ON (tbl1.id = tbl2.catId) WHERE 1=1 AND tbl2.langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND tbl1.parent_id != '0' AND tbl1.path like '%-".$deviceId."-%'";
	
		$query = $this->executeQry($sql);
		$num = $this->getTotalRow($query);
		$tbl = '';
		$tbl = '<select name="deviceCompany" id="m__Company">'
				.'<option value="">Please Select Company</option>';
		if($num > 0){
			while($line = $this->getResultObject($query)){
				$sel = ($line->id == $_POST['deviceCompany']) ? "selected":"";
				$tbl .= '<option value="'.stripslashes($line->id).'" '.$sel.'>'.stripslashes($line->categoryName).'</option>';
			}
		}
		$tbl .= '</select>';
	return $tbl; 	
	}
	
	function getCompanyForDeviceEdit($companyId,$deviceId){
		$sql = "SELECT tbl1.id, tbl2.categoryName FROM ".TBL_CATEGORY." AS tbl1 INNER JOIN ".TBL_CATEGORY_DESCRIPTION." AS tbl2 ON (tbl1.id = tbl2.catId) WHERE 1=1 AND tbl2.langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND tbl1.parent_id != '0' AND tbl1.path like '%-".$deviceId."-%'";
		$query = $this->executeQry($sql);
		$num = $this->getTotalRow($query);
		$tbl = '';
		if($companyId == '0')
			$psel = "selected";
		else	
			$psel = "";
			
		$tbl = '<select name="deviceCompany" id="m__Company">'
				.'<option value="" '.$psel.'>Please Select Company</option>';
		if($num > 0){
			while($line = $this->getResultObject($query)){
				$sel = ($line->id == $companyId) ? "selected":"";
				$tbl .= '<option value="'.stripslashes($line->id).'" '.$sel.'>'.stripslashes($line->categoryName).'</option>';
			}
		}
		$tbl .= '</select>';
	return $tbl; 	
	}
	
	
	
	function getProductView($pid){
	
		$query = "SELECT * FROM ".TBL_PRODUCT_VIEW." WHERE product_id = '$pid'";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		if($num > 0){
			$send = array();
			while($line = $this->getResultObject($sql)){
				$send[] = $line;
			}
		return $send;
		}
	}
	
	
	
	
	
	
	
	
	function getImageName($id)
	{
		$imageName=$this->fetchValue(TBL_PRODUCT,"productImage","id= '".$id."'");
		return $imageName;
	}
	
	function deleteValue($get) {
		
		$this->executeQry("update ".TBL_PRODUCT." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'");
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageProducts.php?page=$get[page]&limit=$get[limit]';</script>";
	}
	
	
}// End Class
?>	