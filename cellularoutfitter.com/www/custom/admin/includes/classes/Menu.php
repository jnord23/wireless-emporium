<?php 
session_start();
class Menu extends MySqlDriver{
        //Start : Constructor===========================
        function __construct() {
            $this->obj = new MySqlDriver;       
        }
        //Start : Display Menu========================================================================
	function displayMenu($pageName='adminArea.php'){
		if (!$_SESSION['ADMINTBL_USERHASH']) 					
			redirect('index.php');		

		$adminMenuID = $this->getMenuDetail();
		$sqlMenu = $this->executeQry("SELECT * FROM ".TBL_MENU." WHERE parentId=0 AND status=1 AND menuId IN($adminMenuID) ORDER BY menuId");
	
		$genTable .= '<div id="ja-mainnav"><div id="jasdl-mainnav"><ul>';
			
		$mainMenuId = $this->getMainMenuId($pageName);
				
		$menuArr = array();
		if($sqlMenu) 
			while ($rowMenu = $this->getResultObject($sqlMenu)){
				$divLiName = "jasdl-mainnav".$rowMenu->menuId;
					
				if ($rowMenu->menuId==$mainMenuId) {
					$_SESSION['CURRENTMENUID'] = $rowMenu->menuId;
					$class = "active";
				} else {
					$class = "";
				}
				
				$menuArr[] = $rowMenu->menuId;	
				
				$genTable .= '<li id="'.$divLiName.'" class="'.$class.'"><a href="'.$rowMenu->menuUrl.'" title="'.$rowMenu->menuName.'"><span class="menu-title">'.$rowMenu->menuName.'</span></a></li>';
			} 
			$genTable .= '</ul></div>
							<SCRIPT type="text/javascript">
								var jasdl_activemenu = new Array(\''.$mainMenuId.'\',77);
							</SCRIPT>
							</div><div id="ja-subnav" class="clearfix" >';
	
			foreach ($menuArr as $menuId){
			$divLiName = "jasdl-subnav".$menuId;
					
			$sqlSubMenu  = $this->executeQry("SELECT * FROM ".TBL_MENU." WHERE parentId='$menuId' AND parentId!=0 AND menuId IN ($adminMenuID) AND status=1 ORDER BY menuId");
			$genTable .= '<ul id="'.$divLiName.'" style="display:none;">';
			while ($rowSubMenu = $this->getResultObject($sqlSubMenu)){ 
				$genTable .= '<li><a href="'.$rowSubMenu->menuUrl.'"><span class="menu-title">'.$rowSubMenu->menuName.'</span></a></li>';
			}
			$genTable .=  '</ul>';
		}
	
		$genTable .= '</div>';
		return $genTable;
	}
	
        //Start : Get Main Menu ID=====================================================================
	function getMainMenuId($pageName){		
		$sqlMenu = $this->executeQry("SELECT * FROM ".TBL_MENU." WHERE menuUrl='$pageName'");
		$rowMenu = $this->getResultObject($sqlMenu);
		if ($rowMenu->parentId==0)
			$menuId = $rowMenu->menuId;
		else
			$menuId = $rowMenu->parentId;
		
		return 	$menuId;
	}
	
        //Start : Get Menu Details====================================================================
	function getMenuDetail(){	
		$sql = $this->executeQry("select * from ".TBL_ADMINPERMISSION." where 1 and adminLevelId = '".$_SESSION['USERLEVELID']."'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			$adminPermissionArr = array();
			while($line = $this->getResultObject($sql))	 {
				array_push($adminPermissionArr,$line->menuid);
			}
			return $adminPermission = implode(',',$adminPermissionArr);
		}  else {
			return 0;
		}
	}
        
        //Start : Check Permission=====================================================================	
	function checkPermission($pageName="",$type=""){			
		$pageName   = $pageName?$pageName:getPageName();		
		$sql = $this->executeQry("select * from ".TBL_MENU." where menuUrl = '$pageName'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
		
			$line = $this->getResultObject($sql);
			$menuId = $line->menuId;
			
			if($type != "") {
				$cond = " AND ".$type."='1'";
			} else {
				$cond = "";
			}
	
			$sql_per = $this->executeQry("select * from ".TBL_ADMINPERMISSION." where adminLevelId = '".$_SESSION['USERLEVELID']."' and menuid = '$menuId' $cond");
			$num_per = $this->getTotalRow($sql_per);
			if($num_per == 0) {
				if($type != "") {
					redirect($pageName);
				} else {
					redirect('adminArea.php');
				}
			}			
		} else {
			redirect('adminArea.php');		
		}
	}
	
        //Start : Check Add Permission===================================================================
	function checkAddPermission($pageName=""){	
		
		$pageName   = $pageName?$pageName:getPageName();

		$sql = $this->executeQry("select * from ".TBL_MENU." where menuUrl = '$pageName'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			$line = $this->getResultObject($sql);
			$menuId = $line->menuId;
			
			$sql_per = $this->executeQry("select * from ".TBL_ADMINPERMISSION." where adminLevelId = '".$_SESSION['USERLEVELID']."' and menuid = '$menuId'");
			$num_per = $this->getTotalRow($sql_per);
			if($num_per > 0) {
				$line_per = $this->getResultObject($sql_per);
				if($line_per->add_record == 1) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	//Start : Check Edit Permission=====================================================================
	function checkEditPermission($pageName=""){	
		
		$pageName   = $pageName?$pageName:getPageName();
		
		$sql = $this->executeQry("select * from ".TBL_MENU." where menuUrl = '$pageName'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			$line = $this->getResultObject($sql);
			$menuId = $line->menuId;
			
			$sql_per = $this->executeQry("select * from ".TBL_ADMINPERMISSION." where adminLevelId = '".$_SESSION['USERLEVELID']."' and menuid = '$menuId'");
			$num_per = $this->getTotalRow($sql_per);
			if($num_per > 0) {
				$line_per = $this->getResultObject($sql_per);
				if($line_per->edit_record == 1) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
        //Start : Check Delete Permission=====================================================================
	function checkDeletePermission($pageName=""){	
		
		$pageName   = $pageName?$pageName:getPageName();
		
		$sql = $this->executeQry("select * from ".TBL_MENU." where menuUrl = '$pageName'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			$line = $this->getResultObject($sql);
			$menuId = $line->menuId;
			
			$sql_per = $this->executeQry("select * from ".TBL_ADMINPERMISSION." where adminLevelId = '".$_SESSION['USERLEVELID']."' and menuid = '$menuId'");
			$num_per = $this->getTotalRow($sql_per);
			if($num_per > 0) {
				$line_per = $this->getResultObject($sql_per);
				if($line_per->delete_record == 1) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}	
	

        //Start : Display Menu==================================================================
        function displayMenu_new(){
        if (!$_SESSION['ADMINTBL_USERHASH']) 					
            redirect('index.php');		

            $adminMenuID = $this->getMenuDetail();
            $sqlMenu = mysql_query("SELECT * FROM ".TBL_MENU." WHERE parentId=0 AND status=1 AND menuId IN($adminMenuID) ORDER BY menuId");?>
            <ul id="nav">
            <? while ($rowMenu = mysql_fetch_object($sqlMenu)){?>
                <li><a href="<?=$rowMenu->menuUrl?>" title="<?=$rowMenu->menuName?>"><?=$rowMenu->menuName?></a>
                <? 
                $sqlSubMenu  = mysql_query("SELECT * FROM ".TBL_MENU." WHERE parentId='".$rowMenu->menuId."' AND parentId!=0 AND menuId IN ($adminMenuID) AND status=1 ORDER BY menuId");
                $numSubMenu = mysql_num_rows($sqlSubMenu);
                if($numSubMenu > 0){ 
                ?>
                    <ul class="submenu"> 
                    <? while ($rowSubMenu = mysql_fetch_object($sqlSubMenu)){ ?>
                    <li><a id="<?=$rowMenu->menuId?>" href="<?=$rowSubMenu->menuUrl?>" title="<?=$rowSubMenu->menuName?>"><?=$rowSubMenu->menuName?></a></li>                           
                    <? }  ?>
                    </ul>
                <? } ?>
                </li>
            <? }  ?>
            </ul><?	 
        }  	
	
}// End Class
?>	