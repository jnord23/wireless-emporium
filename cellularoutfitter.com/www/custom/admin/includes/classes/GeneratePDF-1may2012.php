<?php 
session_start();
error_reporting(0);
require_once("includes/JSON.php");
require_once("includes/classes/pdf.php");
//require_once("includes/createZip.inc.php");
require_once ("includes/phpzip.inc.php");
//require_once("includes/classes/zip.class.php");
require_once("includes/classes/MySqlDriver.php");

class GeneratePDF extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }
	
	function deletePDF($id) {
		$orderRecId = $this->fetchValue(TBL_ORDER,"ordReceiptId","1 and id = '".$id."'");
		$this->executeQry("update ".TBL_ORDER." set pdf_generated = '0' where 1 and id = '".$id."'");
		@unlink('orderfile/'.$orderRecId.".zip");
		$this->DELETE_RECURSIVE_DIRS('pdf/'.$orderRecId);
		$div_id = "status".$id;
		echo '<a style="cursor:pointer;" onClick="javascript:generatePDF_new(\''.$div_id.'\',\''.$id.'\',\'PDF\')">Generate PDF</a>';
	}
	
	function DELETE_RECURSIVE_DIRS($dirname) 
	{ 
		if(is_dir($dirname)){
			$dir_handle=opendir($dirname); 
			while($file=readdir($dir_handle)) 
			{ 
				if($file!="." && $file!="..") 
				{  
					if(!is_dir($dirname."/".$file)) {
						unlink ($dirname."/".$file);
					}
					else
					{ 
						$this->DELETE_RECURSIVE_DIRS($dirname."/".$file);
						@rmdir($dirname."/".$file);
					}
				}
			}
			closedir($dir_handle); 
			@rmdir($dirname);
			return true;
		} else
		return false; 
	}
	
	function createDirectory($dirName)
	{
		if(!is_dir($dirName)) {
			mkdir($dirName);
			@chmod($dirName,0777);
		}
	}
	
	function createPDF($id) {	
		$ordReceiptId = $id;
		$ordZipFileName = $ordReceiptId.".zip";
		$directoryName = "pdf/".$ordZipFileName;
		
		if (!is_file($directoryName)) {
			$query2   = "SELECT p.dataArr FROM ".TBL_MAINPRODUCT." as p WHERE p.id  = '$id' AND p.isDeleted = '0'";
			$sql2 = $this->executeQry($query2);
			$num2 = $this->getTotalRow($sql2);			
			if($num2 > 0) {
				while($line2 = mysql_fetch_object($sql2)) {
					//echo $line2->dataArr;					
					$jsonClass = new JSON;					
					$jsonEncodedData = $jsonClass->decode($line2->dataArr);
					$m = 1;					
					foreach($jsonEncodedData as $fvalue){
						//echo "<pre>";
						//print_r($fvalue);						
						$img_name = "../".str_replace("large","original",$fvalue->source);
						
						list($printW,$printH) = getimagesize($img_name);

						//$printW = ($rowProdDet->printWidth*72);	// Printing Weidth
						//$printH = ($rowProdDet->printHeight*72);// Printing Height 
						
						//$ratio   = $printW/$rowProdDet->wAreaW;
						
						$ratioW = $printW/$fvalue->toolWidth;
						$ratioH = $printH/$fvalue->toolHeight;
						
						
						$size		= array($printW, $printH);
						$pdf		= new PDF("P", "pt", $size);
						$pdf->AddPage();
						//print_r($size);
						//exit;
						$fillColor  = '#'.$fvalue->backColor; // Fill Color;
                                                $fillRed  = hexdec(substr($fillColor, 1, 2));
                                                $fillGreen  = hexdec(substr($fillColor, 3, 2));
                                                    $fillBlue  = hexdec(substr($fillColor, 5, 2));
                                                $pdf->SetLineWidth(0);
                                                $pdf->SetFillColor($fillRed,$fillGreen,$fillBlue);
                                                $pdf->Rect(0, 0 , $printW ,  $printH , 'FD');
						
						//$viewName = $value->view;
						$dirName 	 = "pdf/".$ordReceiptId;
						$this->createDirectory($dirName); // Create First Level Directory								
						//$dirName 	 = "pdf/".$ordReceiptId."/".strtolower($viewName);
						//$this->createDirectory($dirName); // Create Second Level Directory
						$pdfFileName = $ordReceiptId.".pdf";
						$designDirName 	 = "pdf/".$ordReceiptId."/designimage";
						$this->createDirectory($designDirName); // Create Second Level Directory
						
						
						
						$countData = count($fvalue->data);
						//echo $countData;
						//exit;
						if ($countData!=0) {
							foreach($fvalue->data as $value){
								//echo "<pre>";
								//print_r($value);
								if($value->type=='Text') {
									$magick = "/usr/bin/convert";
									
									$color = $value->color;
									if($color=='000000')
											{
												$color='#000005';
											}
									else if($color=='ffffff' || $color=='FFFFFF')
											{
												$color='#fffffe';
											}
									else
											{
												$color='#'.$color;
											}
									$font = '';
									
									$fontTTF = $this->fetchValue(TBL_FONTDESC,"fontTTF","1 and fontId = '".$value->fontId."'");
									
									$font 	= __FONTTTF__.$fontTTF;
		
			
		
									$kerning = 1;//$value->spacing;
									$size = $value->size;
									$gravity = "center";
									$distortEffect = $value->distortEffect;
									$distortValue = $value->distortValue; //exit;
									$rotation = (-($value->rotation));
									//echo $value->text;
									$text = str_replace("\r" , "\n" , $value->text);
		
									$time = rand();
									$name = $designDirName."/".$time.'.png';
								//	$name = '1234.png';
								    $image = $designDirName."/".$time.'.png';//'admin/'.$name;
									$normalImage = $designDirName."/".$time.'_normal.png';
									//$shadowImage = $designDirName."/".$time.'_shadow.png';
		
									$start = ' -background transparent -depth 8';
									//$end = '';
									//echo "$distortEffect-".$distortEffect ;
															
													
									$normalText = '';
									
									$w = 1200;
									$h = 1200;
									
									
									$normalText .= ' -fill "'.$color.'" -size "'.$w.'"x"'.$h.'" label:"'.$text.'"';
		
									$query = $start.' -font "'.$font.'" -gravity "'.$gravity.'" -kerning "'.$kerning.'"'.$normalText.' -trim "'.$image.'"';
									
									/*if($value->flipH=='-1')
								{
									$hflip = " -flip -rotate 180 ";
								}
								else
								{
									$hflip = "";
								}
								if($value->flipV=='-1')
								{
									$vflip = " -flop -rotate 180 ";
								}
								else
								{
									$vflip = "";
								}*/
								
								//exec($magick.$hflip.$vflip.$query);								
								exec($magick.$query);
								//echo $magick.$query;
										
									
									
										$posX = ($value->boundRectX*$ratioW);//
										$posY = ($value->boundRectY*$ratioH);//
										$posW = ($value->pdfWidth*$ratioW);//
										$posH = ($value->pdfHeight*$ratioH);//
										$pdf->RotatedImage($image, $posX, $posY, $posW, $posH, $rotation);
								} // Text	
								
								if($value->type=='Image') {
								
											if (strpos($value->url , "upload"))
												{
												$imagePath = '../'.str_replace('_large' , '' , $value->url);
												$imagePath = str_replace('_original' , '' , $imagePath);
												}
											else
												$imagePath = '../'.str_replace('large' , 'original' , $value->url);
											
											$posX = ($value->x*$ratioW);
											$posY = ($value->y*$ratioH);
											$posW = ($value->width*$ratioW);
											$posH = ($value->height*$ratioH);
											$rotation = (-($value->rotation));
								
										$pdf->RotatedImage($imagePath, $posX, $posY, $posW, $posH, $rotation);
								}
								
								
								$countData--;		
								if(($countData>0) && ($printing==1))
								{
									$pdf->AddPage();
								}			
							}
							$strFileName = 	$k==0 ? $pdfFileName : $strFileName.",".$pdfFileName;
							$strDirName  = 	$k==0 ? strtolower($viewName) : $strDirName.",".strtolower($viewName);
							$pdf->RotatedImage($img_name, 0, 0, $printW, $printH, 0);							
							$pdf->Output($dirName."/".$pdfFileName);							
							$k++;
						}
					}
					//exit;
				}
				
				//Create XML File========================
                                $this->getProductXML($id);
                                
                                //Create ZIP FILE==========================
                                $zipObj = new PHPZip();
                                $dirFileBak="pdf/".$id;
                                $zipfilename ="pdf/".$id.".zip";
                                $zipObj->myZip($dirFileBak, $zipfilename);
                                
                                
                                /* $fileName = "pdf/" . $proofpdf . "/" . $oid . ".zip";
                                $fd = fopen($fileName, "wb");
                                $createZip = new ZipFile($fd);
                                $createZip->addFile($proofzipfile, $pdfFileName, true);
                                $createZip->close();
                                 */
				
			//	$this->executeQry("update ".TBL_ORDER." set pdf_generated = '1' where 1 and id = '".$id."'");
				echo '<a title="Download" href="download.php?fileName='.$ordReceiptId.'.zip"><img height="20" border="0" width="20" title="Download" alt="Download" src="images/download.png"></a>';
                                
			} else {
				echo "Sorry there is no order!!";
			}
		} else {
			echo "PDF already generated!!";
		}
	
	}
	
	
	//// for new pdf
	
	function getDesignForPdf($catId , $approvedFlag=0)
		{
		$SQL = "SELECT 
					D.designId,D.designName, D.designPrice, D.designImage, D.colorable, D.scalable, D.designArray, 
					D.status,D.approve 
				FROM 
					ecart_design AS D, ecart_design_detail AS DD 
				WHERE 
					DD.catId='$catId' AND 
					D.delFlag = 0 AND
					D.approve = $approvedFlag AND
					DD.designId=D.designId ";
					//echo $SQL;
		$sqlDes = mysql_query($SQL);
		while($row = mysql_fetch_object($sqlDes))
		{
			$array[] = $row;
		}
		return $array;
		}
		
		
		function getCategoryDetail($catId) {
		   // echo "SELECT * FROM ecart_design WHERE catId='$catId' and status=1";
			$sqlCat = mysql_query("SELECT * FROM ecart_design_detail WHERE catId='$catId' and status=1");
			$rowCat = mysql_fetch_object($sqlCat);
			return $rowCat;
		}
                
                function getProductXML($pid){
                    $outPut='';
                    $query="Select MPD.productName,MPD.productDesc, MPV.imageWithBorder from ".TBL_MAIN_PRODUCT_DESCRIPTION." as MPD INNER JOIN ".TBL_MAIN_PRODUCT_VIEW." as MPV ON(MPD.productId=MPV.product_id) where MPD.productId='".$pid."'";
                    $rst=$this->executeQry($query);
                    $row=$this->getResultObject($rst);


                    // Create Main Prodcut Detail XML File======================================
                    $outPut .= '<?xml version="1.0" encoding="utf-8"?>'."\n";           
                    $outPut .= '<products>'."\n";
                    $outPut .= '<product id="'.$pid.'" name="'.$row->productName.'" source="'.__MAINPRODUCTLARGEPATH__.$row->imageWithBorder.'"  >'."\n";	    
                    $outPut .= '<link>'."\n";

                    $outPut .= '<a href="'.__BASEURL__.TOOLPAGE.'?designId='.base64_encode($pid).'">Edit</a>'."\n";
                    $outPut .= '</link>'."\n";
                    $outPut .= '</product>'."\n";
                    $outPut .= '</products>';


                    $dirFileBak = __BASE_DIR_FRONT__."admin/pdf/".$pid;                    	    
                    $fp=fopen($dirFileBak."/".$pid.".xml","w");
                    $f=fwrite($fp,$outPut);
                    fclose($fp);	    

                    //Copy Main Product Original Image========================
                    $img=__MAINPRODUCTORIGINAL__.$row->imageWithBorder;            
                    copy($img,$dirFileBak."/".$pid.".png");	
                }
}
?>	