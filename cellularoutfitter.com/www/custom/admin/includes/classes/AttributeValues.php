<?php 
session_start();
class AttributeValues extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function valDetail() {
	    $generalObj = new GeneralFunctions();
		$cond = "1 and ".TBL_ATTRIBUTE_VALUES.".id = ".TBL_ATTRIBUTE_VALUES_DESCRIPTION.".attributeValuesId and ".TBL_ATTRIBUTE_VALUES.".isDeleted = '0' and ".TBL_ATTRIBUTE_VALUES_DESCRIPTION.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND ".TBL_ATTRIBUTE_DESCRIPTION.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' and ".TBL_ATTRIBUTE_DESCRIPTION.".attributeId = ".TBL_ATTRIBUTE_VALUES.".attributeId and ".TBL_ATTRIBUTE.".id = ".TBL_ATTRIBUTE_VALUES.".attributeId  and ".TBL_ATTRIBUTE.".showAttribute = '0'";
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND (".TBL_ATTRIBUTE_VALUES_DESCRIPTION.".attributeValuesName LIKE '%$searchtxt%')";
		}
		$query = "select ".TBL_ATTRIBUTE_VALUES.".*,".TBL_ATTRIBUTE_VALUES_DESCRIPTION.".attributeValuesName,".TBL_ATTRIBUTE_DESCRIPTION.".attributeName from ".TBL_ATTRIBUTE_VALUES." , ".TBL_ATTRIBUTE_VALUES_DESCRIPTION.", ".TBL_ATTRIBUTE_DESCRIPTION.", ".TBL_ATTRIBUTE." where $cond ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:TBL_ATTRIBUTE_VALUES_DESCRIPTION.".attributeValuesName";
		    $order = $_GET[order]? $_GET[order]:"ASC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
									
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "Inactive";
					else
						$status = "Active";
					
					$attributeValuesName = $this->fetchValue(TBL_ATTRIBUTE_VALUES_DESCRIPTION,"attributeValuesName","1 and attributeValuesId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");
										
					$attributeName = $this->fetchValue(TBL_ATTRIBUTE_DESCRIPTION,"attributeName","1 and attributeId = '".$line->attributeId."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");
												
					$genTable .= '<div class="'.$highlight.'">
								 <ul>
								 	<li style="width:50px;">&nbsp;&nbsp;<input name="chk[]" value="'.$line->id.'" type="checkbox"></li>
									<li style="width:100px;">'.$i.'</li>
									<li style="width:170px;">'.stripslashes($attributeValuesName).'</li>
									<li style="width:120px;">'.$attributeName.'</li>
									<li style="width:50px;">'.$generalObj->displayPrice($line->price).'</li><li style="width:60px;">';
									
					if($menuObj->checkEditPermission()) 							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'attributeValues\')">'.$status.'</div>';
									
																											
					$genTable .= '</li>
					<li style="width:70px;"><a rel="shadowbox;width=705;height=325" title="'.stripslashes($attributeValuesName).'" href="viewAttributeValues.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></li>
					<li style="width:75px;">';
									
					if($menuObj->checkEditPermission()) 					
						$genTable .= '<a href="editAttributeValues.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
						
					$genTable .= '</li><li>';
				
					if($menuObj->checkDeletePermission()) 					
						$genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=attributeValues&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					
							
					$genTable .= '</li></ul></div>';
				
		
					$i++;	
				}

				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}
	
	function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_ATTRIBUTE_VALUES,"status","1 and id = '$get[id]'");
		
		if($status==1) {
			$stat= 0;
			$status="Inactive";
		} else 	{
			$stat= 1;
			$status="Active";
		}
	
		$query = "update ".TBL_ATTRIBUTE_VALUES." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
	function deleteValue($get) {
		//$result=$this->deleteRec(TBL_ATTRIBUTE_VALUES,"path like '%-$get[id]-%'");
		$this->executeQry("update ".TBL_ATTRIBUTE_VALUES." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'");
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageAttributeValues.php?page=$get[page]&limit=$get[limit]';</script>";
	}
		
	
	
	
	function addRecord($post) {
		$_SESSION['SESS_MSG'] = "";		
				
		if($_SESSION['SESS_MSG'] == "") {
			$query = "insert into ".TBL_ATTRIBUTE_VALUES." set attributeId = '$post[attributeId]', status = '1', addDate = '".date('Y-m-d H:i:s')."', addedBy = '".$_SESSION['ADMIN_ID']."' , price  = '".$post[price]."'";
			$sql = $this->executeQry($query);
			$inserted_id = mysql_insert_id();

			if($inserted_id) 
				$this->logSuccessFail('1',$query);		
			else 	
				$this->logSuccessFail('0',$query);
			
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$attributeValuesName = 'attributeValuesName_'.$line->id;
					$query = "insert into ".TBL_ATTRIBUTE_VALUES_DESCRIPTION." set attributeValuesId = '$inserted_id', langId = '".$line->id."', attributeValuesName = '".addslashes($post[$attributeValuesName])."'";	
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				}	
			}					
			$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been added successfully");
			/*echo "<script>window.location.href='manageAttributeValues.php';</script>"; */		
		} 	
	}
	
	function editRecord($post) {
		
		$_SESSION['SESS_MSG'] = "";
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$attributeValuesName = 'attributeValuesName_'.$line->id;
				$sql = $this->selectQry(TBL_ATTRIBUTE_VALUES_DESCRIPTION,'1 and attributeValuesId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0) {
					$query = "insert into ".TBL_ATTRIBUTE_VALUES_DESCRIPTION." set attributeValuesId = '$post[id]', langId = '".$line->id."', attributeValuesName = '".addslashes($post[$attributeValuesName])."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				} else {
					$query = "update ".TBL_ATTRIBUTE_VALUES_DESCRIPTION." set attributeValuesName = '".addslashes($post[$attributeValuesName])."' where 1 and attributeValuesId = '$post[id]' and langId = '".$line->id."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				}	
			}	
		}					
		
		 $query = "update ".TBL_ATTRIBUTE_VALUES." set attributeId = '$post[attributeId]', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."', price  = '".$post['price']."' where id = '".$post[id]."'";
		
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);	
			
		echo "<script>window.location.href='manageAttributeValues.php?page=$post[page]';</script>";
	}

	function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageAttributeValues.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(TBL_ATTRIBUTE_VALUES," path like '%-$val-%'");	
					$this->executeQry("update ".TBL_ATTRIBUTE_VALUES." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$val'");
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_ATTRIBUTE_VALUES,"cat_id='$val'");	
					$sql="update ".TBL_ATTRIBUTE_VALUES." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_ATTRIBUTE_VALUES,"cat_id='$val'");	
					$sql="update ".TBL_ATTRIBUTE_VALUES." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageAttributeValues.php?cid=$post[cid]&page=$post[page]';</script>";
	}	
	
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_ATTRIBUTE_VALUES." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultRow($sql);	
		} else {
			redirect("manageAttributeValues.php");
		}	
	}		
	
	function isAttributeValuesNameExist($attributename,$langId,$attributeId,$id=''){
	
		$attributename = trim($attributename);
		$rst = $this->selectQry(TBL_ATTRIBUTE_VALUES_DESCRIPTION,"langId = '".$langId."' and attributeValuesName='".addslashes($attributename)."' AND attributeValuesId!='$id'  ","","");
		
		
		$query = "select ".TBL_ATTRIBUTE_VALUES.".*,".TBL_ATTRIBUTE_VALUES_DESCRIPTION.".* from ".TBL_ATTRIBUTE_VALUES." , ".TBL_ATTRIBUTE_VALUES_DESCRIPTION." where 1 and ".TBL_ATTRIBUTE_VALUES.".id = ".TBL_ATTRIBUTE_VALUES_DESCRIPTION.".attributeValuesId and ".TBL_ATTRIBUTE_VALUES.".attributeId = '".$attributeId."' and ".TBL_ATTRIBUTE_VALUES_DESCRIPTION.".langId = '".$langId."' and ".TBL_ATTRIBUTE_VALUES_DESCRIPTION.".attributeValuesName='".addslashes($attributename)."' AND ".TBL_ATTRIBUTE_VALUES_DESCRIPTION.".attributeValuesId!='$id' ";
		$rst = $this->executeQry($query);		
		$row = $this->getTotalRow($rst);
		$line = $this->getResultObject($rst);

		if($this->fetchValue(TBL_ATTRIBUTE_VALUES,'isDeleted',"id='".$line->attributeValuesId."'")){
		    if($id)
			$rstFind = $this->executeQry("update ".TBL_ATTRIBUTE_VALUES." set isDeleted='1' where id='".$id."'");
			$rstFind = $this->executeQry("update ".TBL_ATTRIBUTE_VALUES." set isDeleted='0' where id='".$line->attributeValuesId."'");
			echo "<script>window.location.href='manageAttributeValues.php';</script>";
			exit;
		}else{
			return $row;
		}
		
	}		
	
}// End Class
?>	