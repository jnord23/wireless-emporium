<?php 
session_start();
error_reporting(0);
require_once("includes/JSON.php");
require_once("includes/classes/pdf.php");
//require_once("includes/createZip.inc.php");
//require_once ("includes/phpzip.inc.php");
require_once("includes/zip.class.php");
require_once("includes/classes/MySqlDriver.php");

class GeneratePDF extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }
	
	function deletePDF($id) {
		$orderRecId = $this->fetchValue(TBL_ORDER,"ordReceiptId","1 and id = '".$id."'");
		$this->executeQry("update ".TBL_ORDER." set pdf_generated = '0' where 1 and id = '".$id."'");
		@unlink('orderfile/'.$orderRecId.".zip");
		$this->DELETE_RECURSIVE_DIRS('pdf/'.$orderRecId);
		$div_id = "status".$id;
		echo '<a style="cursor:pointer;" onClick="javascript:generatePDF_new(\''.$div_id.'\',\''.$id.'\',\'PDF\')">Generate PDF</a>';
	}
	
	function DELETE_RECURSIVE_DIRS($dirname) 
	{ 
		if(is_dir($dirname)){
			$dir_handle=opendir($dirname); 
			while($file=readdir($dir_handle)) 
			{ 
				if($file!="." && $file!="..") 
				{  
					if(!is_dir($dirname."/".$file)) {
						unlink ($dirname."/".$file);
					}
					else
					{ 
						$this->DELETE_RECURSIVE_DIRS($dirname."/".$file);
						@rmdir($dirname."/".$file);
					}
				}
			}
			closedir($dir_handle); 
			@rmdir($dirname);
			return true;
		} else
		return false; 
	}
	
	function createDirectory($dirName)
	{
		if(!is_dir($dirName)) {
			mkdir($dirName);
			@chmod($dirName,0777);
		}
	}
	
	function createPDF($id) {	
		$ordReceiptId = $id;
		$ordZipFileName = $ordReceiptId.".zip";
		$directoryName = "pdf/".$ordZipFileName;
		 $Eps_script='var docPreset = new DocumentPreset;';
		
		if (!is_file($directoryName)) {
			$query2   = "SELECT p.dataArr,p.rowProductId FROM ".TBL_MAINPRODUCT." as p WHERE p.id  = '$id' AND p.isDeleted = '0'";
			$sql2 = $this->executeQry($query2);
			$num2 = $this->getTotalRow($sql2);			
			if($num2 > 0) {
				while($line2 = mysql_fetch_object($sql2)) {
					//echo $line2->dataArr;	
					
					$productionImg = $this->fetchValue('sd_product_view',"productionImg","1 and product_id = '".$line2->rowProductId."'");
									
					$jsonClass = new JSON;					
					$jsonEncodedData = $jsonClass->decode($line2->dataArr);
					$m = 1;					
					foreach($jsonEncodedData as $fvalue){
						//echo "<pre>";
						//print_r($fvalue);
						//exit;	
						list($tool_w,$tool_h) = getimagesize("../".$fvalue->source);					
						$img_name = "../".str_replace("large","original",$fvalue->source);
						//$img_name = "../".$fvalue->source;
						list($printW,$printH) = getimagesize($img_name);
						
						//echo $printW = $tool_w;
						//echo "<br>";
						//echo $printH = $tool_h;
						
						$ratioW = $printW/$tool_w;
						$ratioH = $printH/$tool_h;
						
						$Eps_script=$Eps_script.'docPreset.width = '.$printW.' ;docPreset.height = '.$printH.' ;
							 var docRef = documents.addDocument(DocumentColorSpace.RGB, docPreset);';
							 
							 $fillColor		= "#".$fvalue->backColor; // Fill Color;
							$fillRed		= hexdec(substr($fillColor, 1, 2));
							$fillGreen		= hexdec(substr($fillColor, 3, 2));
							$fillBlue		= hexdec(substr($fillColor, 5, 2));
							 
						$Eps_script=$Eps_script.'var aaaa = docRef.pathItems.rectangle('.$printH.', 0 ,'.$printW.', '.$printH.');
						var colorRef = new RGBColor;
						colorRef.red = '.$fillRed.';
						colorRef.green = '.$fillGreen.';
						colorRef.blue = '.$fillBlue.';
					   aaaa.fillColor = colorRef;';
					   
					   $Eps_script=$Eps_script.'var fName = "CURVE 8900";var newLayer = docRef.layers.add();newLayer.name = fName;';
						
						$dirName 	 = "pdf/".$ordReceiptId;
						$this->createDirectory($dirName); // Create First Level Directory								
						
						//$pdfFileName = $ordReceiptId.".jsx";
						$designDirName 	 = "pdf/".$ordReceiptId."/designimage";
						$this->createDirectory($designDirName); // Create Second Level Directory
						
						
						
						$countData = count($fvalue->data);
					
						if ($countData!=0) {
							foreach($fvalue->data as $value){
							
								switch($value->type)									
										{											
										case 'Image':
											if (strpos($value->url , "upload"))
												{
												$imagePath = '../'.str_replace('_large' , '' , $value->url);
												$imagePath = str_replace('_original' , '' , $imagePath);
												}
											else
												$imagePath = '../'.str_replace('large' , 'original' , $value->url);
												
												//echo "image path".$imagePath;
												
											
											$imageArray = explode("/",$imagePath);
											$position  = count($imageArray)-1;
											$imageRequied = $imageArray[$position];
											copy($imagePath,$dirName."/".$imageRequied);
											$storePdf[] = $dirName."/".$imageRequied;
												
											$posX = ($value->x*$ratioW);
											$posY = ($value->y*$ratioH);
											$posW = ($value->width*$ratioW);
											$posH = ($value->height*$ratioH);
											$posX2 = ($value->aiX*$ratioW);
											$posY2 = ($value->aiY*$ratioH);
											
											$rotation = (-($value->rotation));
											//	$pdf->RotatedImage($imagePath, $posX, $posY, $posW, $posH, $rotation);	
											$imagePath = str_replace("../files/design/original/","D:/illustrator/musicskin/".$ordReceiptId."/",$imagePath);
											$imagePath = str_replace("../files/rawproduct/original/","D:/illustrator/musicskin/".$ordReceiptId."/",$imagePath);
											$imagePath = str_replace("../files/uploads/","D:/illustrator/musicskin/".$ordReceiptId."/",$imagePath);
									
									
												
												
												
						$Eps_script = $Eps_script.'var thisPlacedItem;thisPlacedItem = newLayer.placedItems.add();
						thisPlacedItem.file = File("'.$imagePath.'");';
						
						 $totalH = $printH-$posY;
						 $totalH2 = $printH-$posY2;
						$Eps_script = $Eps_script.'thisPlacedItem.height='.$posH.';';
						$Eps_script = $Eps_script.'thisPlacedItem.width='.$posW.';';
						$Eps_script = $Eps_script.'thisPlacedItem.top='.$totalH.';';
						$Eps_script = $Eps_script.'thisPlacedItem.left='.$posX.';';
						$Eps_script = $Eps_script.'thisPlacedItem.rotate ("'.$rotation.'");';
						$Eps_script = $Eps_script.'thisPlacedItem.top='.$totalH2.';';
						$Eps_script = $Eps_script.'thisPlacedItem.left='.$posX2.';';
																													
											break;
										case 'Text' :
											$imageDestName = $dirName."/k.png";
											//$query = $this->setTextQuery($value, $imageDestName);
											
										
										$posX = ($value->boundRectX*$ratioW);
										$posY = ($value->boundRectY*$ratioH);		
										
										$posW = ($value->pdfWidth*$ratioW);
										$posH = ($value->pdfHeight*$ratioH);
										
										$posX2 = ($value->aiX*$ratioW);
										$posY2 = ($value->aiY*$ratioH);
											
										$rotation = (-($value->rotation));		
								
								
									 $style 	= $value->font;	
								     $text 		= utf8_encode($value->text);
									 $fontSize1	= $value->fontSize+.2;
								     $fontSize	= number_format($fontSize1*$ratio);
									 $txtColor	= '#'.$value->color;//$value->color;
									 $red		= hexdec(substr($txtColor, 1, 2));
									 $green		= hexdec(substr($txtColor, 3, 2));
									 $blue		= hexdec(substr($txtColor, 5, 2));
									 $top       =  $printH-$posY;
									 $top2       =  $printH-$posY2;
									 
									 $font_name = $this->fetchValue(TBL_FONTDESC,"font_ai_name","1 and fontId = '".$value->fontId."'");
																 
									$Eps_script=$Eps_script.'areaTextRef = newLayer.textFrames.add();areaTextRef.top ='.$top.';
						  areaTextRef.left ='.$posX.';var fontName = "'.$font_name.'";	
							 areaTextRef.textRange.characterAttributes.textFont = textFonts.getByName(fontName);areaTextRef.contents = "'.$text.'";areaTextRef.textRange.characterAttributes.size =5;var mytextcolor = new RGBColor();
							 mytextcolor.red = '.$red.';
							 mytextcolor.green = '.$green.';
							  mytextcolor.blue = '.$blue.';
							areaTextRef.textRange.characterAttributes.fillColor = mytextcolor;
							var triangleGroup = areaTextRef.createOutline();
							triangleGroup.top = '.$top.';
triangleGroup.left ='.$posX.';
triangleGroup.width ='.$posW.';
triangleGroup.height ='.$posH.';
triangleGroup.rotate ('.$rotation.');
triangleGroup.top = '.$top2.';
triangleGroup.left ='.$posX2.';';
							
											break;	
											$k++;									
										}
								
								
											
							}
							$img_name = '../files/product/large/'.$productionImg;						
							$imageArray = explode("/",$img_name);
							$position  = count($imageArray);
							 $imageRequied = $imageArray[4];
							copy($img_name,$dirName."/".$imageRequied);
							$storePdf[] = $dirName."/".$imageRequied;
							$orgImagePath = str_replace("../files/product/original/","D:/illustrator/musicskin/".$id."/",$img_name);
							$orgImagePath = str_replace("../files/product/large/","D:/illustrator/musicskin/".$id."/",$img_name);
							$Eps_script = $Eps_script.'var fName = "'.$orgImagePath.'";
							var newLayer = docRef.layers.add();
							newLayer.name = "DIE CUT";	
							var thisPlacedItem;
							thisPlacedItem = newLayer.placedItems.add();
							thisPlacedItem.file = File("'.$orgImagePath.'");
							thisPlacedItem.height='.$printH.';
							thisPlacedItem.width='.$printW.';
							thisPlacedItem.top = '.$printH.';
							thisPlacedItem.left = 0;';						
							$divName = $dirName."/".$id;
							 $myFile = "$divName".".jsx";
							$fh = fopen($myFile, 'w') or die("can't open file");
							fwrite($fh, $Eps_script);
							fclose($fh);
						}
					}
					//exit;
				}
				
				//Create XML File========================
                                $this->getProductXML($id);
                                
                                //Create ZIP FILE==========================
                                //$zipObj = new PHPZip();
                                $dirFileBak = "pdf/".$id;
                                $zipfilename = "pdf/".$id.".zip";
                              
								$xml_filepath = "pdf/".$id."/".$id.".xml";
								$storePdf[]=$xml_filepath;
								
							
								$png_filepath="pdf/".$id."/".$id.".png";
								$storePdf[]=$png_filepath;
								
								$pdf_filepath =  "pdf/".$id."/".$id.".jsx";  
								$storePdf[] = $pdf_filepath;                            
                                $fileName = $zipfilename;
								//
                                $fd = fopen($fileName, "wb");
                             
							 	//print_r($storePdf);
								
								$createZip = new ZipFile($fd);
								foreach ($storePdf as $filzip) {
									$zipfileName=substr($filzip,strrpos($filzip,"/")+1); 
									$createZip->addFile($filzip, $zipfileName, true);
								}
								
                                $createZip->close();
                                 
				
			//	$this->executeQry("update ".TBL_ORDER." set pdf_generated = '1' where 1 and id = '".$id."'");
				echo '<a title="Download" href="download.php?fileName='.$ordReceiptId.'.zip"><img height="20" border="0" width="20" title="Download" alt="Download" src="images/download.png"></a>';
                                
			} else {
				echo "Sorry there is no order!!";
			}
		} else {
			echo "PDF already generated!!";
		}
	
	}
	
	
	//// for new pdf
	
	function getDesignForPdf($catId , $approvedFlag=0)
		{
		$SQL = "SELECT 
					D.designId,D.designName, D.designPrice, D.designImage, D.colorable, D.scalable, D.designArray, 
					D.status,D.approve 
				FROM 
					ecart_design AS D, ecart_design_detail AS DD 
				WHERE 
					DD.catId='$catId' AND 
					D.delFlag = 0 AND
					D.approve = $approvedFlag AND
					DD.designId=D.designId ";
					//echo $SQL;
		$sqlDes = mysql_query($SQL);
		while($row = mysql_fetch_object($sqlDes))
		{
			$array[] = $row;
		}
		return $array;
		}
		
		
		function getCategoryDetail($catId) {
		   // echo "SELECT * FROM ecart_design WHERE catId='$catId' and status=1";
			$sqlCat = mysql_query("SELECT * FROM ecart_design_detail WHERE catId='$catId' and status=1");
			$rowCat = mysql_fetch_object($sqlCat);
			return $rowCat;
		}
                
                function getProductXML($pid){
                    $outPut='';
                    $query="Select MPD.productName,MPD.productDesc, MPV.imageWithBorder from ".TBL_MAIN_PRODUCT_DESCRIPTION." as MPD INNER JOIN ".TBL_MAIN_PRODUCT_VIEW." as MPV ON(MPD.productId=MPV.product_id) where MPD.productId='".$pid."'";
                    $rst=$this->executeQry($query);
                    $row=$this->getResultObject($rst);


                    // Create Main Prodcut Detail XML File======================================
                    $outPut .= '<?xml version="1.0" encoding="utf-8"?>'."\n";           
                    $outPut .= '<products>'."\n";
                    $outPut .= '<product id="'.$pid.'" name="'.$row->productName.'" source="'.__MAINPRODUCTLARGEPATH__.$row->imageWithBorder.'"  >'."\n";	    
                    $outPut .= '<link>'."\n";

                    $outPut .= '<a href="'.__BASEURL__.TOOLPAGE.'?designId='.base64_encode($pid).'">Edit</a>'."\n";
                    $outPut .= '</link>'."\n";
                    $outPut .= '</product>'."\n";
                    $outPut .= '</products>';


                    $dirFileBak = __BASE_DIR_FRONT__."admin/pdf/".$pid;                    	    
                    $fp=fopen($dirFileBak."/".$pid.".xml","w");
                    $f=fwrite($fp,$outPut);
                    fclose($fp);	    

                    //Copy Main Product Original Image========================
                    $img=__MAINPRODUCTORIGINAL__.$row->imageWithBorder;            
                    copy($img,$dirFileBak."/".$pid.".png");	
                }
}
?>	