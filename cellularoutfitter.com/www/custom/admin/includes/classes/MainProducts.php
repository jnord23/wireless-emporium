<?php 

session_start();

class MainProducts extends MySqlDriver{//Start Class
    
    //Start : Constructor====================================
    function __construct() {
        $this->obj = new MySqlDriver; 
    }


    //Start : Val Detail===================================================================
    function valDetail() { 
            $type=$_GET[type]?$_GET[type]:'A';
            $cond = "1  and MP.isDeleted = '0' and MPD.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and MP.userType='".$type."' ";

            if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
                $searchtxt = addslashes($_REQUEST['searchtxt']);
                $cond .= " AND (MPD.productName LIKE '%$searchtxt%')  AND (MP.isCustomizable = '".$_REQUEST['custom']."') ";
            }
           
            $query="select MP.*, MPD.productName from ".TBL_MAINPRODUCT." as MP inner join ".TBL_MAIN_PRODUCT_DESCRIPTION." as MPD on (MP.id=MPD.productId) where $cond";
            $sql = $this->executeQry($query);
            $num = $this->getTotalRow($sql);
            $menuObj = new Menu();
            $page =  $_REQUEST['page']?$_REQUEST['page']:1;
            if($num > 0) {	
            //-------------------------Paging------------------------------------------------

            $paging = $this->paging($query); 
            $this->setLimit($_GET[limit]); 
            $recordsPerPage = $this->getLimit(); 
            $offset = $this->getOffset($_GET["page"]); 
            $this->setStyle("redheading"); 
            $this->setActiveStyle("smallheading"); 
            $this->setButtonStyle("boldcolor");
            $currQueryString = $this->getQueryString();
            $this->setParameter($currQueryString);
            $totalrecords = $this->numrows;
            $currpage = $this->getPage();
            $totalpage = $this->getNoOfPages();
            $pagenumbers = $this->getPageNo();	
            //-------------------------Paging------------------------------------------------

            $orderby = $_GET[orderby]? $_GET[orderby]:"MPD.productName";
            $orderby = $_GET[orderby]? $_GET[orderby]:"MP.addDate";
            $order = $_GET[order]? $_GET[order]:"DESC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
            $rst = $this->executeQry($query); 
            $row = $this->getTotalRow($rst);
            if($row > 0) {	
                $i = 1;	
                $genTable .= '<div class="column" id="column1">';
                while($line = $this->getResultObject($rst)) {
                    $currentOrder.=	$line->id.",";
                    $highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
                    $div_id = "status".$line->id;
                    $status=($line->status)?"Active":"Inactive";
                    //$productName = $this->fetchValue(TBL_MAIN_PRODUCT_DESCRIPTION,"productName","1 and productId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");

                    $genTable .= '<div class="'.$highlight.'" id="'.$line->id.'">';                    
                    $genTable .= '<ul>';
                    $genTable .= '<li style="width:45px;">&nbsp;&nbsp;<input name="chk[]" value="'.$line->id.'" type="checkbox"></li>';
                    $genTable .= '<li style="width:40px;">'.$i.'</li>';

                    //Category Name===========================
                    $catIdArr=explode("-",$line->categoryId);
                    $catId=$catIdArr[1];
                    $categoryName = stripslashes($this->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$catId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'"));
                    $genTable .= '<li style="width:100px;">'.$categoryName.'</li>';

                    //subCategory Name=================================
                    $subCatName=stripslashes($this->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$line->subCatId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'"));
                    $genTable .= '<li style="width:100px;">'.$subCatName.'</li>';


                    //Product Name=====================
                    $productName=stripslashes($line->productName);
                    $genTable .= '<li style="width:160px;">'.$productName.'</li>';

                    //Product Image==============================================
                    $img=$this->fetchValue(TBL_MAIN_PRODUCT_VIEW,'imageWithBorder',"product_id='".$line->id."' and isDefault='1'");                    
                    $genTable .= '
                    <li style="width:90px;">
                    <a href="'.__MAINPRODUCTEXTLARGEPATH__.$img.'" rel="shadowbox"><img src="'.__MAINPRODUCTTHUMBPATH__.$img.'" border="0"></a>
                    <a href="'.SITE_URL.TOOLPAGE.'?designId='.base64_encode($line->id).'">Customize</a>
                    </li>';

                    //Status Section================================
                    $genTable .= '<li style="width:70px;">';
                    if($menuObj->checkEditPermission()){
                        $genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'mainproduct\')">'.$status.'</div>';
                    }
                    $genTable .= '</li>';

                    //View Section======================
                    $genTable .= '<li style="width:30px;"><a rel="shadowbox;width=705;height=325" title="'.$productName.'" href="viewMainProduct.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></li>';

                    //Edit Section========================
                    $genTable .= '<li style="width:50px;">';
                    if($menuObj->checkEditPermission()){
                        $genTable .= '<a href="editMainProduct.php?id='.base64_encode($line->id).'&page='.$page.'&type='.$_GET[type].'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';

                    }
                    $genTable1 .= '</li>';
                    //Delete Section==============================
                    $genTable .= '<li style="width:30px;">';
                    if($menuObj->checkDeletePermission()){
                        $genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=mainproduct&type=delete&userType=".$type."&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
                    }
                    $genTable .= '</li>';
                    //PDF Section==============================
                    //$genTable .= '<li style="width:50px;">';
                    //$genTable .= '<span id="PDF_'.$line->id.'"><a href="javascript:void(0);" onclick="downloadProduct(\''.$line->id.'\');">Generate</a></span>';
                    //$genTable .= '</li>';
                    
                    $genTable .= '<li style="width:50px;">';
                    $genTable .= '<span id="PDF_'.$line->id.'">';
                    $zipPath=__BASE_DIR_ADMIN__."pdf/".$line->id.".zip";                    
                    if(file_exists($zipPath)){
                          $genTable.='<a  href="download.php?fileName='.$line->id.'.zip"><img height="20" border="0" width="20" title="Download PDF" alt="Download PDF" src="images/download.png"></a>';
                    }else{
                            $genTable.='<a href="javascript:void(0);" onclick="generatePDF(\''.$line->id.'\');"><img height="20" border="0" width="20" title="Generate PDF" alt="Download PDF" src="images/pdf.gif"></a>';
                    }
                                        
                  
                    $genTable.='</span>';
                    $genTable .= '</li>';
                     
                    $genTable .= '</ul>';                   
                    $genTable .= '</div>';
                    $i++;	
                }
                $genTable .= '</div>';
                $genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';
                switch($recordsPerPage){
                    case 10:
                    $sel1 = "selected='selected'";
                    break;
                    case 20:
                    $sel2 = "selected='selected'";
                    break;
                    case 30:
                    $sel3 = "selected='selected'";
                    break;
                    case $this->numrows:
                    $sel4 = "selected='selected'";
                    break;
                }

                $currQueryString = $this->getQueryString();
                $limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
                $genTable.="
                <div style='overflow:hidden; margin:0px 0px 0px 50px;'>
                <table border='0' width='88%' height='50'>
                <tr>
                <td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
                Display 
                <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
                <option value='10' $sel1>10</option>
                <option value='20' $sel2>20</option>
                <option value='30' $sel3>30</option> 
                <option value='".$totalrecords."' $sel4>All</option>
                </select> Records Per Page
                </td>
                <td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td>
                <td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td>
                <td width='0' align='right'>".$pagenumbers."</td>
                </tr></table></div>";	

            }	
        } else {
            $genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
        }
        return $genTable;
    }
    /************Type List***********************/
    function getTypeList($type){
        $selA=($type=="A")?"selected='selected'":"";
        $selU=($type=="U")?"selected='selected'":"";
        $tbl.='<option value="A" '.$selA.'>Admin</option>';
        $tbl.='<option value="U" '.$selU.' >User</option>';
        return $tbl;
    }
    
    //Start : Change Status==============================================
        function changeValueStatus($get) {
        $status=$this->fetchValue(TBL_MAINPRODUCT,"status","1 and id = '$get[id]'");
        if($status==1) {
            $stat= 0;
            $status="Inactive";
        } else 	{
            $stat= 1;
            $status="Active";
        }
        $query = "update ".TBL_MAINPRODUCT." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
        if($this->executeQry($query)) 
            $this->logSuccessFail('1',$query);
        else 
            $this->logSuccessFail('0',$query);
        echo $status;		

        }

	
        //Delete Single Value=================================================
        function deleteValue($get) {
            $this->executeQry("update ".TBL_MAINPRODUCT." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'");
            $_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
            echo "<script language=javascript>window.location.href='manageMainProducts.php?type=$_GET[userType]&page=$get[page]&limit=$get[limit]';</script>";exit;

        }

		
//Start : Get Result By ID=====================================================
        function getResult($id) {
            $sql = $this->executeQry("select * from ".TBL_MAINPRODUCT." where id = '$id'");
            $num = $this->getTotalRow($sql);
            if($num > 0) {
                return $line = $this->getResultObject($sql);	
            } else {
                redirect("manageMainProducts.php");
            }	
        }


//Start : Delete Muliple Records==================================================
function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
                    echo "<script language=javascript>window.location.href='manageMainProducts.php?type=$post[usertype]&page=$post[page]&limit=$post[limit]';</script>";exit;
		}
                //Delete Selected===========================
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
                                    $this->executeQry("update ".TBL_MAINPRODUCT." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$val'");
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
                //Enable selected===============================================
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_MAINPRODUCT." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
                // Disableed  selected===============================================
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
                                    $sql="update ".TBL_MAINPRODUCT." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
                                    mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}

		}

		echo "<script language=javascript>window.location.href='manageMainProducts.php?type=$post[usertype]&page=$post[page]';</script>";exit;
	}	
        
         //********* Download Designed Product PDF**************************************************//
	function getProductDownload($pid){          
	    $outPut='';
	    $query="Select MPD.productName,MPD.productDesc, MPV.imageWithBorder from ".TBL_MAIN_PRODUCT_DESCRIPTION." as MPD INNER JOIN ".TBL_MAIN_PRODUCT_VIEW." as MPV ON(MPD.productId=MPV.product_id) where MPD.productId='".$pid."'";
	    $rst=$this->executeQry($query);
	    $row=$this->getResultObject($rst);
            
            
            // Create Main Prodcut Detail XML File======================================
	    $outPut .= '<?xml version="1.0" encoding="utf-8"?>'."\n";           
	    $outPut .= '<products>'."\n";
	    $outPut .= '<product id="'.$pid.'" name="'.$row->productName.'" source="'.__MAINPRODUCTLARGEPATH__.$row->imageWithBorder.'"  >'."\n";	    
	    $outPut .= '<link>'."\n";
             
	    $outPut .= '<a href="'.__BASEURL__.TOOLPAGE.'?designId='.base64_encode($pid).'">Edit</a>'."\n";
	    $outPut .= '</link>'."\n";
	    $outPut .= '</product>'."\n";
	    $outPut .= '</products>';
            
	    
	    $dirFileBak = __BASE_DIR_FRONT__."files/FinalProduct/".$pid;
            
	    $this->createDirectory($dirFileBak);	    
	    $fp=fopen($dirFileBak."/".$pid.".xml","w");
	    $f=fwrite($fp,$outPut);
	    fclose($fp);	    
	    
            //Copy Main Product Original Image========================
	    $img=__MAINPRODUCTORIGINAL__.$row->imageWithBorder;            
	    copy($img,$dirFileBak."/".$pid.".png");	    
	   
	    /********* Generate PDF*******************/
	    require_once("includes/classes/pdf.php");
	    list($pWidth,$pHeight)=getimagesize($img);
	    $pWidth=(($pWidth/300)*72);
	    $pHeight=(($pHeight/300)*72);
	    
	    $pdfFileName = $pid.".pdf";
	    $size=array($pWidth,$pHeight);
	    $pdf = new PDF("P", "pt", $size);
            $pdf->AddPage();
            
            $pdf->RotatedImage($img, 0,0, $pWidth, $pHeight, 0);
            
            $pdfPath=$dirFileBak."/".$pdfFileName;
            $pdf->Output($pdfPath);
            unset($pdf);
	    
	    
	    require_once('includes/phpzip.inc.php');
	    $zipObj = new PHPZip();
	    $zipfilename =__DIR_PDF__.$pid.".zip";
            $zipfilePathname =__DIR_PDFPATH__.$pid.".zip";
	    if($zipObj->myZip($dirFileBak, $zipfilename)){
		echo "<a href='$zipfilePathname'>Download</a>"; 
	    }	    
	    
	}
	
        /********* Create Directory************************/
	function createDirectory($dirName){
		if(!is_dir($dirName)){
		    mkdir($dirName);
		    @chmod($dirName,0777);
		}
	}
		
        
        // Start : Edit Record===========================================================
        function editRecord($post,$file) {
                //echo "<pre>"; print_r($post); echo "</pre>";exit;
                $_SESSION['SESS_MSG'] = "";	
                if($_SESSION['SESS_MSG'] == "") {
                        //Update Main Product Table==================
                        $query = "UPDATE ".TBL_MAINPRODUCT." set categoryId = '-".$post[cat]."-', subCatId = '$post[subCat]', productPrice = '$post[productPrice]', productWeight='$post[productWeight]',quantity = '$post[quantity]', modDate = '".date('Y-m-d H:i:s')."', modBy  = '".$_SESSION['ADMIN_ID']."' WHERE id = '$post[id]'";
                        $sql = $this->executeQry($query);                    

                        //Update Main Product Description Table=====================
                        $rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");	
                        $num = $this->getTotalRow($rst);
                        if($num){			
                            while($line = $this->getResultObject($rst)) {
                                $productName = 'productName_'.$line->id;
                                $productDesc = 'productDesc_'.$line->id;
                                $query = "UPDATE ".TBL_MAIN_PRODUCT_DESCRIPTION." set productName = '".$post[$productName]."', productDesc = '".$post[$productDesc]."' WHERE productId = '$post[id]'";	
                                if($this->executeQry($query)){
                                    $this->logSuccessFail('1',$query);		
                                }else{ 	
                                    $this->logSuccessFail('0',$query);
                                }
                            }	
                        }                 
                        $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been update successfully");
                }             
                header('Location:manageMainProducts.php?type='.$_GET[type].'&page='.$_GET[page]);exit;
        }

        //End : Edit Record==============================
        

}// End Class

?>	