<?php
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageDesign.php","edit_record");
/*---Basic for Each Page Ends----*/
$id=base64_decode($_GET['id']);
$designObj = new Design($id);
$genObj = new GeneralFunctions();
$result = $designObj->getResult($id);

//Update==================================================
if(isset($_POST['update'])) {    
	require_once('validation_class.php');
	$obj = new validationclass();	
	$rst = $designObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $designObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $designObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		//add error===============================================		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('designName_'.$value,$_POST['designName_'.$value], 'req', 'Please enter design name.');			
		}
		//validate=================================		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1; 
		
                
                //get error=======================================			
		foreach($langIdArr as $key=>$value) {
			$arr_error['designName_'.$value]=$obj->fnGetErr($arr_error['designName_'.$value]);			
		}
		
                //Check if name exists=============================
		foreach($langIdArr as $key=>$value){
			if($designObj->isDesignNameExist($_POST['designName_'.$value],$value,base64_decode($_GET['id']))){ 
				$arr_error['designName_'.$value] = "Design already exist. ";	
                                $str_validate=0;
			}
		}
		
		
		//check image extension===============================
                if($_FILES['designImage']['name']){
                    $filename = stripslashes($_FILES['designImage']['name']);
                    $extension = strtolower(findexts($filename));
                    if(!$designObj->checkExtensions($extension,'DESIGNS_EXTENSION')){ 
                        $arr_error[designImage] = "Upload Only ".$designObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='DESIGNS_EXTENSION'") ." Files.";
                        $str_validate=0;	
                    }
		}
                //update if validation pass: $str_validate is 1=======================================		
		if($str_validate){
			$_POST = postwithoutspace($_POST);
			$designObj->editRecord();
		}
	}
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<!--<script language="javascript" src="js/requiredValidation.js"></script>-->
<script language="javascript" src="js/ajax.js"></script>
<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

<!-- Color Picker (END)	-->
<script type="text/javascript">
	function hrefBack1(){
		window.location.href='manageDesign.php';
	}

</script>

</head>
<body>
    <? include('includes/header.php'); ?>
    <div id="nav-under-bg"><!-- --></div>
    <div class="main-body-div-new">
        <div class="main-body-div-header">Edit Design</div>
        <div class="main-body-div4" id="mainDiv">
            <div align="center"><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>

            <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data" >
                <input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">
                <input type="hidden" name="page" value="<?=$_GET['page']?>">
                <div>
                <div class="add-main-body-left-new" >
                    <ul>
                        <li class="add-main-body-left-new-text" style="clear:both; width:500px;" >
                            <span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span>
                        </li>
                        
                        <!-- Category======================================= -->
                        <li class="lable">Design Category <span class="spancolor">*</span></li>
                        <li class="lable2">                            
                            <select name="designCategory" id="">
                            <?=$genObj->getDesignCatList($result[catId]);?>
                            </select>                      
                        </li> 
                   
                        <!-- Design Name===================================== -->
                        <li class="lable">Design Name <span class="spancolor">*</span></li>
                        <?=$genObj->getLanguageEditTextBox('designName','m__design_name',TBL_DESIGN_DESC,$result['id'],"designId",$arr_error); ?>


                        <!-- Image=================================== -->
                        <li class="lable">Design Image<span class="spancolor">*</span></li>
                        <li class="lable2">
                        <span><img src="<?php echo $result['designImageThumb'];?>" /></span>
                        <input type="file" name="designImage" id="m__design_image" />
                        <p style="padding-left:150px;"><?=$arr_error[designImage]?></p>
                        </li>


                    </ul>
                </div>
                </div>

                <div class="main-body-sub">
                    <input type="submit" name="update" class="main-body-sub-submit" style="cursor:pointer;" value="Update" />
                    &nbsp;
                    <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
                </div>
            </form>  
        </div>
    </div>
</body>
</html>
<? unset($_SESSION['SESS_MSG']); ?>
