<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/*---Basic for Each Page Starts----*/

$catObj = new Attribute();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>

<!-- New Drop Down menu Starts-->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<script language='javascript' type='text/javascript' src='js/perpage.js'></script>
<!-- New Drop Down menu Ends-->

<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->

</head>
<body>
<? include('includes/header.php'); ?>
<div id="nav-under-bg"><!-- --></div>
<form name="ecartFrm" method="post" action="pass.php?action=attribute&type=deleteall" >
<input type="hidden" name="cid" value="<?=$cid?>">
<div class="main-body-div-width">
	<div class="main-body-div-header">
	<div class="main-body-header-text-top">Attribute Details</div><span class="main-body-adduser"><b><? if($menuObj->checkAddPermission()) { ?><a href="addAttribute.php">Add New Attribute</a><? } ?></b></span>
	</div>
	<div>&nbsp;</div>
	<div id="search-main-div">
		<ul>
			<li class="selectall">
				<div id="check"> <a href="javascript:void(NULL)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
 <div id="uncheck" style="display:none;"><a href="javascript:void(NULL)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
			</li>
			<li class="action">Action:</li>
			<li>
				<select name="action">
                    	<option value="">Select Action</option>
						<? if(($menuObj->checkDeletePermission())){  ?>
                    	<!--<option value="deleteselected">Delete Selected</option>-->
						<? } ?>
						<? if(($menuObj->checkEditPermission()) ){  ?>
                    	<option value="enableall">Enable Selected</option>	
                    	<option value="disableall">Disable Selected</option>	
						<? } ?>										
                 </select>
			</li>
			<li><input name="Input" type="submit" value="Submit"  class=""/></li>
			<li><input name="searchtext" type="text" class="adminsearch" value="<?=$searchtxt= $_GET['searchtxt']?$_GET['searchtxt']:SEARCHTEXT?>" onclick="clickclear(this, '<?=SEARCHTEXT?>')" onblur="clickrecall(this,'<?=SEARCHTEXT?>')"/></li>
			<li><input name="GO" type="submit" value="GO"  class=""/></li>
			<li class="showall"><a href="<?=basename($_SERVER['PHP_SELF'])?>">Reset</a></li>
		</ul>
</div>
<div style="margin-top:20px;"><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
	<div class="main-body-content-text-div">
							<ul style="text-align:center;">
								<li style="width:50px;">&nbsp;&nbsp;<input type="checkbox" name="checkall" onclick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></li>
								<li style="width:50px;">SL.No</li>
								<li style="width:240px;"><?=orderBy(basename($_SERVER['PHP_SELF']),TBL_ATTRIBUTE_DESCRIPTION.".attributeName","Attribute Name")?></li>								
								<li style="width:100px;">Status</li>
								<li style="width:100px;">View</li>								
								<li style="width:135px;">Edit</li>
								<li style="width:55px;">Delete</li>
							</ul>
						</div>
	<?
		echo $catObj->valDetail();
	?>
</div>
</div>
</form>
</body>
</html>



