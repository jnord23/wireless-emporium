<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();

/*---Basic for Each Page Starts----*/
$catObj = new DesignCategory();
$cid  = $_GET['cid']?$_GET['cid']:"";
$catObj->checkCategoryExist($cid);
if ($action == 'category') {
	if($type == 'deleteall'){
		$categoryObj->deleteAllValues($_POST);
	}
}
$addHadding=($cid)?"Sub":"";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>

<!-- New Drop Down menu Starts-->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<script language='javascript' type='text/javascript' src='js/perpage.js'></script>
<!-- New Drop Down menu Ends-->

<!--				Light Box 			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<!-- the following two lines are only required for the demos -->
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">
	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
//Shadowbox.loadSkin('Vikash', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');
	
window.onload = function(){
	Shadowbox.init();
	/**
	 * Note: The following function call is not necessary in your own project.
	 * It is only used here to set up the demonstrations on this page.
	 */
	//initDemos();
};
	
</script>
<!--				END Ligh Box  	-->

<!--          *****************            DRAG AND DROP  - START        ***************   -->
<script type="text/javascript" src="dragndrop/js/jquery-ui-1.7.2.custom.min.js" ></script>
<script type="text/javascript" >
$(function(){
	$('.dragbox')
	.each(function(){
		$(this).hover(function(){
			$(this).find('ul').addClass('collapse');
		}, function(){
			$(this).find('ul').removeClass('collapse');
		})
		.find('h2').hover(function(){
			$(this).find('.configure').css('visibility', 'visible');
		}, function(){
			$(this).find('.configure').css('visibility', 'hidden');
		})
		.click(function(){
			$(this).siblings('.dragbox-content').toggle();
		})
		.end()
		.find('.configure').css('visibility', 'hidden');
	});
	$('.column').sortable({
		connectWith: '.column',
		handle: 'ul',
		cursor: 'move',
		placeholder: 'placeholder',
		forcePlaceholderSize: true,
		opacity: 0.4,
		stop: function(event, ui){
			$(ui.item).find('ul').click();
			var sortorder='';
			$('.column').each(function(){
				var itemorder=$(this).sortable('toArray');
				var columnId=$(this).attr('id');
				sortorder+=columnId+'='+itemorder.toString()+'&';
			});
			current_order = document.getElementById('currentOrder').value;
			//alert(current_order);
			Page = document.getElementById('page').value;
			Limit = document.getElementById('Pagelimit').value;
            //alert("urvesh");
	        sortorder = sortorder+'&Current Order='+current_order+'&page='+Page+'&limit='+Limit;
		   //alert(sortorder);
		     sortOrderProduct(sortorder,'design_category');
			/*Pass sortorder variable to server using ajax to save state*/
		}
	})
	.disableSelection();
});
</script>
</head>
<body>
	<? include('includes/header.php'); ?>
	<div id="nav-under-bg"><!-- --></div>
	<form name="ecartFrm" method="post" action="pass.php?action=design_category&type=deleteall" >
		<input type="hidden" name="cid" value="<?=$cid?>">
		<input type="hidden" name="page" id="page" value="<?=$_GET[page] ?>" />
		<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET[limit] ?>" />
		<div class="main-body-div-width">
			<div class="main-body-div-header">
				<div class="main-body-header-text-top">
				Design <?=$addHadding ?>Category Details
				</div>
				<span class="main-body-adduser">
				<b>
				<? if($menuObj->checkAddPermission()) { ?><a href="addDesignCategory.php<?=$cid?"?cid=$cid":""?>">Add New <?=$addHadding ?>Category</a><? } ?>
				</b>
				</span>
			</div>
			<div>&nbsp;</div>
			<div><?=$catObj->getBreadCrumb($cid)?></div>
			<div>&nbsp;</div>
			<div id="search-main-div">
				<ul>
				<li class="selectall">
				<div id="check"> <a href="javascript:void(0)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
				<div id="uncheck" style="display:none;"><a href="javascript:void(0)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
				</li>
				<li class="action">Action:</li>
				<li>
				<select name="action">
				<option value="">Select Action</option>
				<? if(($menuObj->checkDeletePermission())){  ?>
				<option value="deleteselected">Delete Selected</option>
				<? } ?>
				<? if(($menuObj->checkEditPermission())){  ?>
				<option value="enableall">Enable Selected</option>	
				<option value="disableall">Disable Selected</option>	
				<? } ?>										
				</select>
				</li>
				<li><input name="Input" type="submit" value="Submit"  class=""/></li>                                
                                <li><input name="searchtext" type="text" class="adminsearch" value="<?=$searchtxt= stripslashes(stripslashes($_GET['searchtxt']))?stripslashes(stripslashes($_GET['searchtxt'])):SEARCHTEXT?>" onclick="clickclear(this, '<?=SEARCHTEXT?>')" onblur="clickrecall(this,'<?=SEARCHTEXT?>')" onkeydown="if (event.keyCode == 13) document.getElementById('btnSearch').click()"/></li>
                                <li><input name="GO" type="submit" value="GO" id="btnSearch"  class=""/></li>
                                <li class="showall"><a href="<?=basename($_SERVER['PHP_SELF'])?>">Reset</a></li>
				</ul>
			</div>	
			<div style="margin-top:20px;"><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
			<div class="main-body-content-text-div">
				<ul style="text-align:center;">
				<li style="width:50px;">&nbsp;&nbsp;<input type="checkbox" name="checkall" onclick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></li>
				<li style="width:50px;">SL.No</li>
				<li style="width:200px;">
				<?=orderBy("manageDesignCategory.php?cid=$cid","categoryName","".$addHadding."Category Name")?>
				</li>
				<li style="width:175px;">Image</li>
<!--                                <li style="width:80px;">Default </li>-->
				<li style="width:100px;">Status</li>
				<li style="width:100px;">View</li>
				<li style="width:135px;">Edit</li>
				<li style="width:55px;">Delete</li>
				</ul>
			</div>
			<? echo $catObj->valDetail($cid);?>
		</div>	
	</form>
</body>
</html>



