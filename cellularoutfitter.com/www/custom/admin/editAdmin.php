<?
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();

$admObj = new AdminDetail();

if(isset($_POST['submit'])) {
	$admObj->editAdminDetail($_POST);
}

$result = $admObj->getResult(base64_decode($_GET['id']));
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>
<script language="javascript" src="js/validation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<!-- Menu head -->
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>

<!--[if lte IE 6]>
<style type="text/css">
.clearfix {height: 1%;}
img {border: none;}
</style>
<![endif]-->
<!--[if gte IE 7.0]>
<style type="text/css">
.clearfix {display: inline-block;}
</style>
<![endif]-->
<!--[if gt IE 7]>
<![endif]-->
<!-- New Drop Down menu -->

</head>
<body>
<? include('includes/header.php'); ?>
<div id="nav-under-bg"><!-- --></div>

		<div class="main-body-div2">
		<div class="main-body-div-header">Edit Admin</div>
		<!-- left position -->
		<form name="frmUser" id="frmUser" method="post" >
		<div class="main-body-div4" id="mainDiv" style="margin-left:-5px;">
		<div id="div_duplicateUser" style="text-align:center; font-weight:bold;">

		</div>
		<div id="div_duplicateEmail" style="text-align:center; font-weight:bold;">

		</div>
		<div id="divMessage" style="text-align:center"></div> <!-- Insertion Message-->
		<div class="add-main-body-left" id="left">
			<ul>	
				<li class="add-main-body-left-text" style="clear:both; width:500px;" ><span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span></li>
				<li class="add-main-body-left-text" style="clear:both;">User Name <span class="spancolor">*</span></li>
				<li><input type="text" name="userName" id="userName" class="wel" value="<?=$result->username?>" />
				<div id="chk_userName" class="small_error_message"></div></li>
   				<li class="add-main-body-left-text" style="clear:both;">Email <span class="spancolor">*</span></li>
				<li><input type="text" name="userEmail" id="userEmail" value="<?=$result->emailId?>"  class="wel" />
				<div id="chk_userEmail" class="small_error_message"></div></li>
				<li class="add-main-body-left-text" style="clear:both;">Password <span class="spancolor">*</span></li>
				<li><input type="password" name="password"  value="" class="wel"/>
				<div id="chk_password" class="small_error_message"></div></li>				
			</ul>
		</div>




<div style="margin-top:20px;float:left;clear:both">
	
			<div id="menu" style="display:none; clear:both; padding:20px 0px 10px 100px; font-size:13px;"><b>Select Menu</b>&nbsp;</div>
         <fieldset style="margin:0px 90px 0px 90px;">
        
<div id="selectMenu">
	<div style="width:490px;overflow:hidden;">
		<table width="98%" border="0" cellspacing="2" cellpadding="2" align="center">
  			<tr>
				<td>&nbsp;</td>
				<td align="center">Add Record</td>
				<td align="center">Edit Record</td>
				<td align="center">Delete Record</td>												
			</tr>
		
								
<?php						
	$i = 0;	$j = 0;
	$sqlMenu = $admObj->executeQry("SELECT * FROM ".TBL_MENU." WHERE parentId=0 AND status=1 ORDER BY menuId");	
	while($rowMenu = $admObj->getResultObject($sqlMenu)){
	$sqlSubMenu   = $admObj->executeQry("SELECT * FROM ".TBL_MENU." WHERE parentId='$rowMenu->menuId' AND parentId!=0 AND status=1");
	$countSubMenu = $admObj->getTotalRow($sqlSubMenu);
	if ($countSubMenu==0) 
	{	
		$insertMenue1=$rowMenu->menuId;
?>
			<tr> 
				<td> <input type="checkbox"  name="menuCheck[]" value="<? echo $insertMenue1;?>" id="menuCheck" onclick="checkAllSingle('<?=$rowMenu->menuId?>','<?=$rowMenu->menu_type?>')" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"menuid","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '$insertMenue1'") > 0) { ?> checked="checked"<? } ?>/><? echo "<b>".$menuName=$rowMenu->menuName."</b>";?></td>

				<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$insertMenue1?>_add" value="1" id="menuCheck_<?=$insertMenue1?>_add" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"add_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '$insertMenue1'")  == 1) { ?> checked="checked"<? } ?> ><? } ?></td>
				<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$insertMenue1?>_edit" id="menuCheck_<?=$insertMenue1?>_edit" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"edit_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '$insertMenue1'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
				<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$insertMenue1?>_del" id="menuCheck_<?=$insertMenue1?>_del" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"delete_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '$insertMenue1'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>								

			</tr>
<?
	} else {
		$divName = "divId".$rowMenu->menuId;
?>
			<tr>
				<td><input type="checkbox"  name="menuCheck[]" value="<?=$rowMenu->menuId?>"  id="menuCheckA<?=$rowMenu->menuId?>"  onclick="checkAll(<?=$countSubMenu?>, '<?=$i?>','<?=$rowMenu->menuId?>','<?=$rowMenu->menu_type?>')" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"menuid","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'") > 0) { ?> checked="checked"<? } ?>/> <? echo "<b>".$rowMenu->menuName."</b><br />";?>	
				</td>
				<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowMenu->menuId?>_add" id="menuCheck_<?=$rowMenu->menuId?>_add" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"add_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
				<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowMenu->menuId?>_edit" id="menuCheck_<?=$rowMenu->menuId?>_edit" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"edit_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
				<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowMenu->menuId?>_del" id="menuCheck_<?=$rowMenu->menuId?>_del" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"delete_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
			</tr>										
<? 
		
		while ($rowSubMenu = mysql_fetch_object($sqlSubMenu)){
		$insertMenue=$rowSubMenu->menuId;
?>
			<tr>
				<td>&nbsp;&nbsp;<input type="checkbox" name="menuCheck[]" value="<?=$insertMenue?>" onclick="checkMenu('<?=$i?>','<?=$rowSubMenu->menu_type?>')" id="menuCheckB<?=$i?>" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"menuid","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowSubMenu->menuId."'") > 0) { ?> checked="checked"<? } ?>/><? echo $rowSubMenu->menuName."<br>"; ?>	</td>
				<td align="center"><? if($rowSubMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowSubMenu->menuId?>_add" id="menuCheckB<?=$i?>_add" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"add_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowSubMenu->menuId."'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
				<td align="center"><? if($rowSubMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowSubMenu->menuId?>_edit" id="menuCheckB<?=$i?>_edit" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"edit_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowSubMenu->menuId."'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
				<td align="center"><? if($rowSubMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowSubMenu->menuId?>_del"  id="menuCheckB<?=$i?>_del" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"delete_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowSubMenu->menuId."'") == 1) { ?> checked="checked"<? } ?>><? } ?></td>
			</tr>													
<? 			$i++; 	
		}	
		$j++;
		} 
	}
?>		</table>	</div>
		</div>
	</fieldset>
	</div>
<div class="main-body-sub">
<input type="hidden" name="adminLevelId" value="<?=$result->adminLevelId?>">
<input type="hidden" name="admin_id" value="<?=$result->id?>">
<input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" onclick="return validationEditAdmin()"/>&nbsp;
<input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onClick="hrefBack()"/>
</div>
	</div>
	
	</form>
	</div>

   <div id="divTemp" style="display:none;"></div> 
<script language="javascript">
<!--

function checkMenu(id,menu_type)
{
	if(menu_type == 0) {
		if (document.getElementById("menuCheckB"+id).checked == true)
			checked = true
		else
			checked = false
		
		document.getElementById("menuCheckB"+id+"_add").checked = checked;	
		document.getElementById("menuCheckB"+id+"_edit").checked = checked;	
		document.getElementById("menuCheckB"+id+"_del").checked = checked;						
	}	
}

function checkAll(count, id, id1, menu_type)
{	
	if (document.getElementById("menuCheckA"+id1).checked == true)
		checked = true
	else
		checked = false

	if(menu_type == 0) {
		document.getElementById("menuCheck_"+id1+"_add").checked = checked;	
		document.getElementById("menuCheck_"+id1+"_edit").checked = checked;	
		document.getElementById("menuCheck_"+id1+"_del").checked = checked;			
	}	

	for(var i=0; i<count; i++)
	{	
		document.getElementById("menuCheckB"+id).checked = checked;	
		
		var ctr=0;
		var field_name = '';
		var frm = document.frmUser;

		for (ctr=0; ctr < frm.length; ctr++)
		{
			field_name = frm.elements[ctr].id;

			//if (field_name.indexOf("menuCheckB"+id+"_add") == -1)
			if ((field_name == "menuCheckB"+id+"_add") || (field_name == "menuCheckB"+id+"_edit") || (field_name == "menuCheckB"+id+"_del"))
			{
				frm.elements[ctr].checked = checked;
			}
		}
		
		//document.getElementById("menuCheckB"+id+"_add").checked = checked;	
		//document.getElementById("menuCheckB"+id+"_edit").checked = checked;	
		//document.getElementById("menuCheckB"+id+"_del").checked = checked;					
		id++;
	}

}

function checkAllSingle(id1,menu_type)
{	
	if(menu_type == 0) {
		if (document.getElementById("menuCheck").checked == true)
			checked = true
		else
			checked = false
		
		document.getElementById("menuCheck_"+id1+"_add").checked = checked;	
		document.getElementById("menuCheck_"+id1+"_edit").checked = checked;	
		document.getElementById("menuCheck_"+id1+"_del").checked = checked;						
	}	
}

function hrefBack()
{
	window.location.href = "administrator.php";
}
</script>
