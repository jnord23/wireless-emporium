<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageMainProducts.php","add_record");
/*---Basic for Each Page Ends----*/

$mainProdObj = new MainProducts();
if(isset($_POST['submit'])) {
    
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $mainProdObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $mainProdObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $mainProdObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		
		$obj->fnAdd('device',$_POST['device'], 'req', 'Please Enter device.');
		$obj->fnAdd('deviceCompany',$_POST['deviceCompany'], 'req', 'Please Enter company.');		
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productName_'.$value,$_POST['productName_'.$value], 'req', 'Please Enter Product Name.');			
		}
		
		
		$obj->fnAdd('productPrice',$_POST['productPrice'], 'req', 'Please Enter Product Price.');
		
		$obj->fnAdd('orientation',$_POST['orientation'], 'req', 'Please Enter Product Orientation.');
		
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productDesc_'.$value,$_POST['productDesc_'.$value], 'req', 'Please enter the Product Desciption.');
		}
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1; 
		
		$arr_error[device]=$obj->fnGetErr($arr_error[device]);
		$arr_error[deviceCompany]=$obj->fnGetErr($arr_error[deviceCompany]);
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['productName_'.$value]=$obj->fnGetErr($arr_error['productName_'.$value]);
			if($arr_error['productName_'.$value]) 
				$errorArr = 1;
		}
		
		foreach($langIdArr as $key=>$value) {
			if($mainProdObj->isProductNameExist($_POST['productName_'.$value],$value))
			{ 
				$arr_error['productName_'.$value] = "Product already exist. ";
				$errorArr = 1;				
			}
		}
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['productDesc_'.$value]=$obj->fnGetErr($arr_error['productDesc_'.$value]);
			if($arr_error['productDesc_'.$value]) 
				$errorArr = 1;
		}
		
		foreach($_FILES as $key=>$fiesArr){
		
			if($_FILES[$key]['name']){
				$filename = stripslashes($_FILES[$key]['name']);
				$extension = findexts($filename);
				$extension = strtolower($extension);
		
				if(!$mainProdObj->checkExtensions($extension))
				{ 
					$arr_error[$key] = "Upload Only ".$mainProdObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") ." image extension.";
					$errorArr = 1;	
				}
			}
		}
		
		
		$arr_error[productPrice]=$obj->fnGetErr($arr_error[productPrice]);
		
		$arr_error[orientation] = $obj->fnGetErr($arr_error[orientation]);
			
		
		if($errorArr == 0 && isset($_POST['submit']) && empty($arr_error[device]) && empty($arr_error[deviceCompany]) && empty($arr_error[productPrice]) && empty($arr_error[productImage]) && empty($arr_error[orientation])){
			//$_POST = postwithoutspace($_POST);
			$mainProdObj->addRecord($_POST,$_FILES);
		}
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/ajax.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

<script type="text/javascript">
function hrefBack1(){
	window.location = 'manageMainProducts.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
<div id="nav-under-bg"><!-- --></div>

		<form name="frmUser" id="frmUser" method="post" enctype="multipart/form-data" onsubmit="javascript: return validateFrm(this);">
		<input type="hidden" name="cid" value="<?=$cid?>">
		<div class="main-body-div-new">
          <div class="main-body-div-header">Add Main Product</div>
		  <!-- left position -->
        
            <div class="main-body-div4" id="mainDiv">
              <div class="add-main-body-left-new" >
                <ul>
				<li class="add-main-body-left-new-text" style="clear:both; width:500px;padding-bottom:5px;" ><span class="small_error_message">
                    <?=$_SESSION['SESS_MSG']?>
                  </span></li>				  
                  <li class="lable">Device <span class="spancolor">*</span></li>
                  <li style="margin-top:10px;">
				  	<?php	
						$genObj = new GeneralFunctions();
						echo $genObj->getCategoryList();
					?>
					<p style="padding-left:150px;"><?=$arr_error[device]?></p>
					</li> 
				  <li class="lable">Company <span class="spancolor">*</span></li>
                  <li style=" margin-top:10px;" id="deviceCompanyDiv">
				  <select name="deviceCompany" id="m__Company">
				  <option value="">Please Select Company</option>
				  </select>
					  <p style="padding-left:150px;"><?=$arr_error[deviceCompany]?></p>
					  </li> 
					  
				  <li class="lable">Product Name <span class="spancolor">*</span></li>
                  
                    <?	$genObj = new GeneralFunctions();
						echo $genObj->getLanguageTextBox('productName','m__productName',$arr_error); //1->type,2->name,3->id
					?>
					
				  <li class="lable">Product Price <span class="spancolor">*</span></li>
                  <li><input type="text" name="productPrice" id="m__Product_Price" class="wel" value="<?=stripslashes($_POST['productPrice'])?>" maxlength="8" onkeyup="return isNum12(this.value);"   /><p style="padding-left:150px;" id="price"><?=$arr_error[productPrice]?></p></li>	
				  
				  <li class="lable">Product Quantity </li>
                  <li><input type="text" name="quantity" class="wel" value="<?=stripslashes($_POST[quantity])?>"  />
				  <p style="padding-left:150px;" id="price"><?=$arr_error[quantity]?></p></li>	
				  
				  <li class="lable">Product Description <span class="spancolor">*</span></li>
                  
                    <?	$genObj = new GeneralFunctions();
						echo $genObj->getLanguageTextarea('productDesc','m__Product_Description',$arr_error); //1->type,2->name,3->id
					?>
					
				  <br />
				  
				  <li class="lable" >Product image  <span class="spancolor">*</span></li>
				  <li><input type="file" id="m__product_image" name="productImage">
						<p  style="padding-left:150px;"><? echo $arr_error[productImage];?></p></li>
						
				<li class="lable" >Product Orientation  <span class="spancolor">*</span></li>
				  <li style="margin-top:10px;"><select name="orientation" id="m__product_orientation">
				  		<option value="">Please Select Orientation</option>
						<option value="1">Verticale</option>
						<option value="0">Horizontal</option>
						</select>
						<p  style="padding-left:150px;"><? echo $arr_error[orientation];?></p></li>		
				  
				
				
				  <li class="lable" >Product view image <span class="spancolor">*</span></li>
				  
				 <li><?php $viewResult = $mainProdObj->getViewImageUpload(); ?>
				  	<table border="0">
					<?php 
						$i = 1;
						foreach($viewResult as $value){
							$sel = ($i == 1)?"checked":"";
							extract($value);
					?>
						<tr>
							<td><?php echo $view; ?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>With Border</td>
							<td><div>
								<input type="file" id="<?php echo $imgWithBorderId;?>" name="<?php echo $imgWithBorderName;?>">
								<p  style="padding-left:0px;"><?=$arr_error["prodImageWithBorder".$id.""]?></p>
								</div>
							</td>
						</tr>

						<tr>
							<td>&nbsp;</td>
							<td>Without Border</td>
							<td>
							<div>
							<input type="file" id="<?php echo $imgWithoutBorderId;?>" name="<?php echo $imgWithoutBorderName;?>">
							<p  style="padding-left:0px;"><?=$arr_error["prodImageWithoutBorder".$id.""]?></p>
							</div>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Make this Default&nbsp;</td>
							<td><input type="radio" name="isDefault" value="<?php echo $id;?>" <?php echo $sel;?> ></td>
						</tr>
					<?php $i++; }?>
				  </table>
				  </li>
              		 <? //=$mainProdObj->getColorViewImageUpload(1); ?>				  				  				  
              
			  </ul>
              </div>
              <div class="main-body-sub">
                <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                &nbsp;
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
              </div>
            </div>
</div>
	</form>
	<script type="text/javascript" >
	function checkAllcheckbox(fid,sid){
	 if(document.getElementById(fid+"menuCheck").checked == true){
		 for (var i = 0; i < sid; i++) {
			document.getElementById(fid+"mCheck"+i).checked = true;	
		 }
	 }else{
	     for (var i = 0; i < sid; i++) {
			document.getElementById(fid+"mCheck"+i).checked = false;	
		 }
	 }
	}
	function selectAllBox(){
	  var aid = document.getElementById("atid").value;
	  var aaid = document.getElementById("atvid").value;
	  var spaid = aid.split(",");
	  var sppaid = aaid.split(",");
	    //alert(sppaid.length);
	    if(document.getElementById("selectval").checked == true){
			for(k=0;k<spaid.length;k++){
			    document.getElementById(spaid[k]+"menuCheck").checked = true;
			    for(l=0;l<sppaid[k];l++){
			      document.getElementById(spaid[k]+"mCheck"+l).checked = true;
			    }
			 }
		 }else{
			    for(i=0;i<sppaid.length;i++){
			      document.getElementById(spaid[i]+"menuCheck").checked = false;
				  for(l=0;l<sppaid[i];l++){
			        document.getElementById(spaid[i]+"mCheck"+l).checked = false;
			      }
		        }
	    }
	}
	</script>
<? unset($_SESSION['SESS_MSG']); ?>