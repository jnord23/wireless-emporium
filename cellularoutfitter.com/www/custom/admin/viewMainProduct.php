<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageMainProducts.php","");
/*---Basic for Each Page Ends----*/
$mpObj = new MainProducts();
$genObj= new GeneralFunctions;
$result = $mpObj->getResult(base64_decode($_GET['id']));
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->

</head>
<body>
    <div class="main-body-div-header">View Detail</div>   
    <div class="main-body-div4" id="mainDiv">
        <div align="center"><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
        <div>
            <div class="add-main-body-left-new" >
                <ul>
                    

                    <!--Category --------->
                    <? 
                    $catIdArr=explode("-",$result->categoryId);
                    $catId=$catIdArr[1];
                    $catName= stripslashes($mpObj->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$catId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'"));
                    ?>
                    <li class="lable"><b>Category</b> <span class="spancolor"></span></li>
                    <li class="lable2"><?=$catName?></li>
                    
                    <!-- Sub Category --->
                    <?
                    $subCatName=$mpObj->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$result->subCatId."'");                    
                    ?>
                    <li class="lable"><b>Sub Category</b> <span class="spancolor"></span></li>
                    <li class="lable2"><?=$subCatName?></li>
                    
                    <!--Product Name --->
                    <?  
                    $productName=$mpObj->fetchValue(TBL_MAIN_PRODUCT_DESCRIPTION,'productName',"productId='".$result->id."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'");
                    $productName= htmlentities(stripslashes($productName));
                    ?>
                    <li class="lable"><b>Product Name</b> </li>
                    <li class="lable2"><?=$productName?></li>

                    <!-- Product Price -->
                    <li class="lable"><b>Product Price</b> <span class="spancolor"></span></li>
                    <li class="lable2"><?=$genObj->displayPrice($result->productPrice)?>  </li>

                    <!-- Product Quantity --------->
                    <li class="lable"><b>Product Quantity</b> </li>
                    <li class="lable2"><?=$result->quantity?></li>

                    <!-- Product Description -------------->
                     <? 
                     $productDes=$mpObj->fetchValue(TBL_MAIN_PRODUCT_DESCRIPTION,'productDesc',"productId='".$result->id."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'"); 
                     $productDes =htmlentities(stripslashes($productDes));
                    ?>
                    <li class="lable"><b>Product Description</b> <span class="spancolor"></span></li>
                    <li class="lable2"><?=$productDes?></li>                   

                    <!-- Product Image -------------->     
                    <?
                    $img=$mpObj->fetchValue(TBL_MAIN_PRODUCT_VIEW,'imageWithBorder',"product_id='".$result->id."' and isDefault='1'");
                    ?>
                    <li class="lable"><b>Product Image</b> </li>
                    <li class="lable2">
                        <a href="<?=__MAINPRODUCTEXTLARGEPATH__.$img?>" rel="shadowbox">
                        <img src="<?=__MAINPRODUCTTHUMBPATH__.$img?>"   />
                        </a>
                    </li>
                </ul>
               
            </div>
        </div>
    </div>
</body>
</html>