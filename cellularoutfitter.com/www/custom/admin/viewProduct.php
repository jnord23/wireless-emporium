<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php","");
/*---Basic for Each Page Ends----*/
$prodObj = new Products();
$genObj= new GeneralFunctions;

//Resutl & Variables============================================
$result = $prodObj->getResult(base64_decode($_GET['id']));
$catName=stripslashes($prodObj->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$result->categoryId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'"));
$subCatName=stripslashes($prodObj->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$result->subCatId."'"));
$productName=stripslashes($prodObj->fetchValue(TBL_PRODUCT_DESCRIPTION,'productName',"productId='".$result->id."'"));
$productDes=stripslashes($prodObj->fetchValue(TBL_PRODUCT_DESCRIPTION,'productDesc',"productId='".$result->id."'")); 

$imgThumbPath=__RAWPRODUCTTHUMBPATH__.$result->productImage;
$imgThumbAbsPath=__RAWPRODUCTTHUMB__.$result->productImage;
$imgThumb=_image($imgThumbAbsPath,$imgThumbPath,__NOIMAGEPATH__);

$imgLargePath=__RAWPRODUCTLARGEPATH__.$result->productImage;
$imgLargeAbsPath=__RAWPRODUCTLARGE__.$result->productImage;
$imgLarge=_image($imgLargeAbsPath,$imgLargePath,__NOIMAGEPATH__);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->

</head>
<body>
<div class="main-body-div-header">View Detail</div>
<div class="main-body-div4" id="mainDiv">
	<div class="add-main-body-left-new" >
		<ul>
			<li class="add-main-body-left-new-text" style="clear:both; width:500px;" >
			<span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span></li>
			<!-- Category======================== -->
			<li class="lable"><b>Category</b> <span class="spancolor"></span></li>
			<li class="lable2"><?=$catName?></li>
			
			<!-- Sub Category ========================== -->		
			<li class="lable"><b>Sub Category</b> <span class="spancolor"></span></li>
			<li class="lable2"><?=$subCatName?></li>
			<!-- Product Name ========================== -->
			<li class="lable"><b>Product Name</b> </li>
			<li class="lable2"> <?=$productName?></li>
			<!-- Product Price====================== -->
			<li class="lable"><b>Product Price</b> <span class="spancolor"></span></li>
			<li class="lable2"><?=$genObj->displayPrice($result->productPrice)?></li>
			
			<!-- Product Weight====================== -->
			<li class="lable"><b>Product Weight</b> <span class="spancolor"></span></li>
			<li class="lable2"><?=$result->productWeight?></li>
			<!-- Product Weight====================== -->		
			<li class="lable"><b>Product Quantity</b> </li>
			<li class="lable2"><?=$result->quantity?></li>
			<!--Product Description============================= -->
			<li class="lable"><b>Product Description</b> <span class="spancolor"></span></li>
			<li class="lable2"><?=$productDes?></li>
			<!-- Product Images=========================== -->		
			<li class="lable"><b>Product Image</b> </li>
			<li class="lable2">		
			<span>
			<a href="<?=$imgLarge?>" rel="shadowbox"><img src="<?=$imgThumb?>" /></a></span></li>
		</ul>
	</div>


	<!-- Show View Images========================= -->
	<?php
	$viewResult = $prodObj->getProductView(base64_decode($_GET['id']));
	if(count($viewResult)>0){		
	?>
		<div style="clear:both; float:left;">
			<div id="divProduct" style="height:15px;text-align:center;font-weight:bold; color:#FF0000;"></div>
			<fieldset style="margin:0px 0px 0px 20px ; font-weight:normal; width:630px; height:auto">
				<legend style="color:#000000;"><b>Product Image Detail</b></legend>
				<?		
				foreach($viewResult as $value){
				$default = ($value->isDefault == '1')?"True":"False";
				$viewName=stripslashes($prodObj->fetchValue(TBL_VIEWDESC,'viewName',"viewId='".$value->view_id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'"));
				?>
					<table border="0">
					<tr>
					<td><b><?=$viewName?></b></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
				
					<tr>
					<td>&nbsp;</td>
					<td><b>Outline Image</b></td>
					<td><a href="<?=__RAWPRODUCTLARGEPATH__.$value->imageWithBorder?>" rel="shadowbox"><img src="<?=__RAWPRODUCTTHUMBPATH__.$value->imageWithBorder?>"/></a></td>
					</tr>
				
					<tr>
					<td>&nbsp;</td>
					<td><b>Background Image</b></td>
					<td> <a href="<?=__RAWPRODUCTLARGEPATH__.$value->imageWithoutBorder?>" rel="shadowbox"><img src="<?=__RAWPRODUCTTHUMBPATH__.$value->imageWithoutBorder?>"/></a> </td>
					</tr>
				
					<tr>
					<td>&nbsp;</td>
					<td><b>Default</b></td>
					<td><?php echo $default;?></td>
					</tr>
				
					</table>
				<?
				}?>
			</fieldset>
		</div>	
	<? } ?>
	<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
</div>
	
</body>
</html>