<%
mobileAccess = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	dim noLeftNav : noLeftNav = 1
		aspId = Request.querystring("aspId")
        designId=Request.querystring("designId")        
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<div style="float:left;"><img src="/images/customPrint/customize-banner1.jpg" border="0" /></div>
<link rel="stylesheet" type="text/css" href="history/history.css" />
<style>.main {padding:0px;} .col-main{ padding:0px;} #flashContent{display:block;}</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript" src="FBJSBridge.js"></script>
<script type="text/javascript">
 
    function popuponclick(s)
    {
        // alert("popup");
        my_window = window.open(s,"mywindow","status=1,width=800,height=600");
    }

    function closepopup()
    { 
        // alert("closeup");
        my_window.close ();
    }

    var attributes = {};

    function embedPlayer() {
        var flashvars = {designId:"<%=prepInt(designId)%>",aspId:"<%=prepInt(aspId)%>"};        
        var params = {menu: "false"};	        
        swfobject.embedSWF("index.swf", "flashContent", "650", "870", "9.0", null, flashvars , params, {name:"flashContent"});
    }
    
    //Redirect for authorization for application loaded in an iFrame on Facebook.com 
    function redirect(id,perms,uri) { //alert(id+'=='+perms+'=='+uri);
        var params = window.location.toString().slice(window.location.toString().indexOf('?'));
        top.location = 'https://graph.facebook.com/oauth/authorize?client_id='+id+'&scope='+perms+'&redirect_uri='+uri+params;				 
    }    			
</script>
<div style="padding:20px 0px 20px 150px;">
	<script type="text/javascript">embedPlayer();</script>
	<div id="fb-root"></div>
    <div id="flashContent">
        <style type="text/css" media="screen"> 
            html, body  { height:100%; }
            object:focus { outline:none; }
            #flashContent { display:none; }
        </style>
        <link rel="stylesheet" type="text/css" href="bin-release/history/history.css" />
        <script type="text/javascript" src="bin-release/history/history.js"></script>  
        <script type="text/javascript" src="bin-release/swfobject.js"></script> 
        <p>To view this page ensure that Adobe Flash Player version 10.0.0 or greater is installed. </p>
        <script type="text/javascript"> 
            var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
            document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
                            + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
        </script> 
    </div>
</div>
<div style="float:left; border-top:3px groove #CCC; width:100%;">
	<div style="float:left;"><img src="/images/customPrint/about-image.jpg" border="0" /></div>
    <div style="float:left; width:400px; margin:35px 0px 0px 15px;">
    	<div style="float:left; width:400px; font-weight:bold; font-size:20px; color:#333;">ABOUT OUR COVERS & FACEPLATES:</div>
        <div style="float:left; width:400px; font-weight:bold; font-size:11px; color:#333; margin-top:15px;">What separates us from the competition?</div>
        <div style="float:left; width:400px; font-weight:bold; font-size:11px; color:#666; margin-top:15px;">
	        Our custom phone covers &amp; cases are not merely stickers or vinyl print outs - the designs are printed out with high quality ink with an industrial printer onto a case made specifically for your phone. For example, if you have an iPhone 4S, your hard protective case will have the design printed on it as if it came straight from the factory.
    	    <br /><br />
        	Wireless Emporium's custom cell phone case printing service combines the best quality, a perfect fit and rock bottom prices in a service that is truly second to none.
        </div>
    </div>
</div>
<div style="float:left; border-top:3px groove #CCC; width:100%; border-bottom:3px groove #CCC; width:100%;">
    <div style="float:left; width:400px; margin-top:35px">
    	<div style="float:left; width:400px; font-weight:bold; font-size:20px; color:#333;">MAKE IT YOURS:</div>
        <div style="float:left; width:400px; font-weight:bold; font-size:11px; color:#666; margin-top:15px;">
	        Our software allows you to let your imagination run wild. Not only can you create custom cell phone cases, but you can also personalize your other electronic devices. Imagine the envy of your friends when your iPhone 4S has a high quality, form fitting case with your favorite sports team.
            <br /><br />
            Through our interactive case creation software, you can upload and edit whatever image you want. Make a statement by changing the colors of your case, adding or modifying text and changing the font or size. Whatever your taste or style, you'll be sure to find an endless world of possibilities with our custom cover printing service.
        </div>
    </div>
    <div style="float:left; margin-left:15px;"><img src="/images/customPrint/make-it-yours-image.jpg" border="0" /></div>
</div>
<!--#include virtual="/includes/template/bottom_index.asp"-->
