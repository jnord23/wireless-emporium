<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
dim brandid
brandid = request.querystring("brandid")
if brandid = "" or not isNumeric(brandid) then
	response.redirect("/")
	response.end
end if

if brandID = 20 then
	sql = "select brandName from we_brands where brandID = " & brandID
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)

	response.Status = "301 Moved Permanently"
	response.AddHeader "Location", "/b-" & brandID & "-" & lcase(replace(brandRS("brandName")," ","-")) & "-cell-phone-accessories.html"
end if

if brandID = 17 then
	call PermanentRedirect("/b-17-apple-cell-phone-accessories.html")
elseif brandID = 5 then
	call PermanentRedirect("/b-5-motorola-cell-phone-accessories.html")
end if

call fOpenConn()
dim SQL, RS
'SQL = "SELECT DISTINCT A.temp, A.modelID, A.modelName, A.modelImg, c.brandName, c.brandImg FROM we_models A with (nolock) INNER JOIN we_items B with (nolock) ON A.modelID = B.modelID"
'SQL = SQL & " INNER JOIN we_brands C with (nolock) ON A.brandid = C.brandid"
'SQL = SQL & " WHERE B.brandid = '" & brandid & "'"
'SQL = SQL & " AND B.hideLive = 0 AND patindex('%All %',A.modelname) = 0"
'SQL = SQL & " AND B.inv_qty <> 0"
'SQL = SQL & " AND B.price_CO > 0"
'SQL = SQL & " ORDER BY A.temp DESC"

sql =   "SELECT DISTINCT A.temp, A.modelID, A.modelName, A.modelImg, c.brandName, c.brandImg, b.typeID, d.typeName FROM we_models A with (nolock) " & vbcrlf & _
		"INNER JOIN we_items B with (nolock) ON A.modelID = B.modelID INNER JOIN we_brands C with (nolock) ON A.brandid = C.brandid left join we_types d " & vbcrlf & _
		"on b.typeID = d.typeID WHERE B.brandid = '" & brandid & "' AND B.hideLive = 0 AND patindex('%All %',A.modelname) = 0 AND B.inv_qty <> 0 AND B.price_CO > 0 " & vbcrlf & _
		"ORDER BY A.temp DESC, a.modelName, d.typeName"
		
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
'response.write SQL & "<br>"
RS.open SQL, oConn, 0, 1

if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if

dim brandName, brandImg
brandName = RS("brandName")
brandImg = RS("brandImg")

productList = ""
do while not RS.EOF
	productList = productList & RS("modelID") & "##" & RS("modelName") & "##" & RS("modelImg") & "##" & RS("typeID") & "##" & RS("typeName") & "$$"
	RS.movenext
loop
productArray = split(productList,"$$")

'response.write "isarray(productArray):" & isarray(productArray) & "<br>"


dim SEtitle, SEdescription, SEkeywords
SEtitle = brandName & " Accessories"
SEdescription = "CellularOutfitter.com is the #1 store online for " & brandName & " Cell Phone Accessories, offering the best deals on " & brandName & " chargers, batteries, covers, cases & much more."
SEkeywords = brandName & " cell phone accessories, " & brandName & " accessories, " & brandName & " cell phone accessory, " & brandName & " cases, " & brandName & " batteries, " & brandName & " chargers"

dim oRsTypes

dim strBreadcrumb
strBreadcrumb = brandName & " Cell Phone Accessories"
%>
<!--#include virtual="/includes/template/top.asp"-->
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="top-sublink-gray" style="padding-bottom:10px;">
                <a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
                <span class="top-sublink-blue"><%=brandName%> Cell Phone Accessories</span>
            </td>
        </tr>
        <tr>
            <td width="100%" style="background-image: url('/images/CO_header.jpg'); background-repeat: no-repeat; background-position: center bottom; height:70px; padding:10px 0px 10px 20px;">
                <h1>
                    <%
                    if brandid = "17" then
                        response.write "Apple iPhone & iPod Accessories"
                    else
                        response.write brandName & " Cell Phone Accessories"
                    end if
                    %>
                </h1>
            </td>
        </tr>
        <tr>
            <td align="center" style="padding-top:10px;">
                <form>
                    <img src="/images/spacer.gif" width="25" height="10">
                    <select name="selectModel" onChange="if(this.value != ''){window.location=this.value;}">
                        <option value="">Select from this list or click on an image below</option>
                        <%
                        for i = 0 to (ubound(productArray) - 1)
                            curArray = split(productArray(i),"##")
                            response.write "<option value=""/m-" & curArray(0) & "-" & formatSEO(brandName) & "-" & formatSEO(curArray(1)) & "-cell-phone-accessories.html"">" & curArray(1) & "</option>" & vbCrLf
                        next
                        %>
                    </select>
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width:780px;">
                    <%
                    a = 0
                    i = 0
					do while i < (ubound(productArray) - 1)
                        a = a + 1
                        curArray = split(productArray(i),"##")
                    %>
                    <div style="float:left; width:260px; padding:0px 0px 10px 0px; border-right:1px solid #000; border-bottom:1px solid #000; height:240px;">
                        <div style="width:100%; text-align:center; float:left; background-color:#000; margin-bottom:5px;"><a class="cellphone-font" style="font-size:12px; color:#fff;" href="/m-<%=curArray(0) & "-" & formatSEO(brandName) & "-" & formatSEO(curArray(1))%>-cell-phone-accessories.html"><%=curArray(1)%></a></div>
                        <div style="float:left; width:120px; text-align:center;"><a href="/m-<%=curArray(0) & "-" & formatSEO(brandName) & "-" & formatSEO(curArray(1))%>-cell-phone-accessories.html"><img src="/modelpics/<%=curArray(2)%>" border="0" alt="<%=brandName & " " & curArray(1)%>"></a></div>
                        <div style="float:left; width:130px; padding-left:10px;">
                            <%
                            curModelID = curArray(0)
							isContinue = true
                            do while isContinue
                            %>
                            <a class="cellphone-category-font" href="/sb-<%=brandID%>-sm-<%=curArray(0)%>-sc-<%=curArray(3)%>-<%=formatSEO(brandname)%>-<%=formatSEO(curArray(1))%>-<%=formatSEO(curArray(4))%>.html" title="<%=nameSEO(curArray(4))%> for <%=brandName%> <%=curArray(1)%>"><%=curArray(4)%></a><br>
                            <%
                                i = i + 1
                                curArray = split(productArray(i),"##")
								if ubound(curArray) < 0 then
									isContinue = false
								else
									if curModelID = curArray(0) then
										isContinue = true
									else
										isContinue = false
									end if
								end if
                            loop
                            %>
                        </div>
                    </div>
                    <%
                        if a = 3 then a = 0
						response.Flush()
                    loop
                    %>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" class="contain-reale-pro">
                <%
                dim holdType, strHandsfreeTypes
                SQL = "SELECT A.modelName, B.itemDesc_CO, B.itemID, A.HandsfreeTypes, C.typeName FROM (we_models A INNER JOIN we_items B ON A.modelID = B.modelID)"
                SQL = SQL & " INNER JOIN we_types C ON B.typeid = C.typeid"
                SQL = SQL & " WHERE B.brandid = '" & brandid & "'"
                SQL = SQL & " AND B.typeid IN (1,2,3,6,7,8,12,13)"
                SQL = SQL & " AND B.hideLive = 0"
                SQL = SQL & " AND patindex('%All %',A.modelname) = 0"
                SQL = SQL & " AND B.inv_qty <> 0"
                SQL = SQL & " AND B.price_CO > 0"
                SQL = SQL & " ORDER BY B.typeid, A.temp DESC, B.itemDesc_CO"
                session("errorSQL") = SQL
                set RS = Server.CreateObject("ADODB.Recordset")
                RS.open SQL, oConn, 0, 1
                holdType = "99999"
                do until RS.eof
                    modelName = RS("modelName")
                    itemDesc_CO = RS("itemDesc_CO")
                    itemID = RS("itemID")
                    HandsfreeTypes = RS("HandsfreeTypes")
                    curTypeName = RS("typeName")
                    
                    if curTypeName <> holdType then
                        response.write "<p class=""orange-subtitle""><b>" & brandName & " " & nameSEO(curTypeName) & "</b></p>" & vbcrlf
                        holdType = curTypeName
                    end if
                    response.write "<p>" & itemDesc_CO & "<br>" & vbcrlf
                    response.write "<a href=""/p-" & itemID & "-" & formatSEO(itemDesc_CO) & ".html"">" & brandName & " " & modelName & " " & singularSEO(curTypeName) & "</a></p>" & vbcrlf
                    if inStr(strHandsfreeTypes,HandsfreeTypes) = 0 then strHandsfreeTypes = strHandsfreeTypes & HandsfreeTypes
                    RS.movenext
                loop
                strHandsfreeTypes = left(strHandsfreeTypes,len(strHandsfreeTypes)-1)
                
                SQL = "SELECT B.itemDesc_CO, B.itemID, A.modelName FROM we_models A INNER JOIN we_items B ON A.modelID = B.modelID"
                SQL = SQL & " WHERE B.brandid = '" & brandid & "'"
                SQL = SQL & " AND B.typeid = 5"
                SQL = SQL & " AND B.HandsfreeType IN (" & strHandsfreeTypes & ")"
                SQL = SQL & " AND B.hideLive = 0"
                SQL = SQL & " AND B.inv_qty <> 0"
                SQL = SQL & " AND B.price_CO > 0"
                SQL = SQL & " ORDER BY A.temp DESC, B.itemDesc_CO"
                session("errorSQL") = SQL
                set RS = Server.CreateObject("ADODB.Recordset")
                RS.open SQL, oConn, 0, 1
                response.write "<p class=""orange-subtitle""><b>" & brandName & " " & nameSEO("Hands-Free") & "</b></p>" & vbcrlf
                do until RS.eof
                    response.write "<p>" & RS("itemDesc_CO") & "<br>" & vbcrlf
                    response.write "<a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc_CO")) & ".html"">" & brandName & " " & RS("modelName") & " " & singularSEO("Hands-Free") & "</a></p>" & vbcrlf
                    RS.movenext
                loop
                %>
            </td>
        </tr>
        <tr><td><!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"--></td></tr>
    </table>
<%
call fCloseConn()
%>
<!--#include virtual="/includes/template/bottom.asp"-->