<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
dim categoryPage : categoryPage = 1
dim categoryid : categoryid = prepInt(request.querystring("categoryid"))
dim cpass : cpass = 0
dim pageTitle : pageTitle = "Category"

if categoryid = 0 then PermanentRedirect("/")

call redirectURL("c", categoryid, request.ServerVariables("HTTP_X_REWRITE_URL"), "")

call fOpenConn()
SQL = "SELECT typeName_CO FROM we_types WHERE typeid = " & categoryid
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if
dim categoryName
categoryName = RS("typeName_CO")

sql = "exec sp_categoryData 2," & categoryid
saveSQL = sql
session("errorSQL") = SQL
set RS = oConn.execute(sql)

strSubTypeID = ""
if not RS.eof then
	parentCatNameSEO = RS("parentTypeNameSEO")
	parentCatName = RS("parentTypeName")
	categoryName = RS("typeName")
	parentTypeID = RS("typeid")
	categoryNameSEO = RS("typeNameSEO")
	do until RS.eof
		strSubTypeID = strSubTypeID & RS("subtypeid") & ","
		RS.movenext
	loop
	strSubTypeID = left(strSubTypeID, len(strSubTypeID) - 1)
end if
if strSubTypeID = "" then strSubTypeID = "9999999" end if

if cpass = 1 then
	sql = "exec sp_cpassData"
else
	sql = "exec sp_brandsByCategory '" & strSubTypeID & "'"
end if
saveSQL = sql
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
'response.write "<pre>" & sql & "</pre><br />"
'response.end
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, catText, H1

H1 = "Cell Phone " & categoryName

dim strBreadcrumb, headerImgAltText
strBreadcrumb = H1
headerImgAltText = H1
%>

<!--#include virtual="/includes/template/top_index.asp"-->
<!-- saveSQL:<%=saveSQL%> -->
<style>
	h1 { font-family: Arial, Helvetica, sans-serif; font-size: 22px; color: #3399CC; margin: 0px; padding: 0px; }
</style>
									<table border="0" cellspacing="0" cellpadding="0" width="775">
										<tr> 
											<td class="top-sublink-gray">
												<a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
												<span class="top-sublink-blue">Cell Phone <%=categoryName%></span>
											</td>
										</tr>
										<tr>
											<td width="100%" style="padding-top:5px;" title="<%=headerImgAltText%>">
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                	<tr>
                                                    	<td style="border-bottom:1px solid #ccc; padding-bottom:5px;"><h1><%=seH1%></h1></td>
                                                    	<td rowspan="2" valign="middle" align="right" width="200"><img src="/images/types/CO_CAT_banner_<%=formatSEO(replace(categoryName, "&", "and"))%>.jpg" border="0" width="190" height="119" alt="<%=headerImgAltText%>" title="<%=headerImgAltText%>"></td>
                                                    </tr>
                                                	<tr>
                                                    	<td>
                                                        	<p class="static-content-font" style="text-align:justify;"><%=SeTopText%></p>
                                                        </td>
                                                    </tr>
                                                </table>
											</td>
										</tr>
										<tr>
											<td width="100%" align="center" style="padding:20px 0px 20px 0px;">
												<div style="width:750px; height:39px;" title="Please Select Your Phone Brand">
													<div style="float:left; height:39;">
														<img src="/images/types/border_left.jpg" width="11" height="39" border="0" />
													</div>
													<div style="float:left; height:39px; background: url('/images/types/border_middle.jpg') repeat-x; width:728px; font-size:22px; padding-top:5px;" align="center">
														<strong>Please Select Your Phone Brand</strong>
													</div>
													<div style="float:left; height:39;">
														<img src="/images/types/border_right.jpg" width="11" height="39" border="0" />												
													</div>
												</div>	
											</td>
										</tr>
										<tr>
											<td align="center" width="100%">
												<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<%
														a = 0
														dim altText
														do until RS.eof
															altText = RS("brandName") '& " " & categoryName 'Removed categoryName to avoid duplicate content penalty
															%>
															<td align="center" valign="top" width="190">
																<table border="0" cellspacing="0" cellpadding="0" width="100%">
																	<tr>
																		<td align="center" valign="middle">
																			<table border="0" cellspacing="0" cellpadding="0" width="100%">
																				<tr>
																					<td align="center" valign="middle"><a href="/sc-<%=categoryID & "-sb-" & RS("brandID") & "-cell-phone-" & formatSEO(categoryName) & "-for-" & formatSEO(RS("brandName"))%>.html"><img src="/images/brands/CO_CATpage_buttons_<%=formatSEO(RS("brandName"))%>.jpg" border="0" width="165" height="63" alt="<%=altText%>" title="<%=altText%>"></a></td>
																				</tr>
																				<tr>
																					<td align="center" valign="bottom"><a href="/sc-<%=categoryID & "-sb-" & RS("brandID") & "-cell-phone-" & formatSEO(categoryName) & "-for-" & formatSEO(RS("brandName"))%>.html" title="<%=altText%>" class="cellphone-category-font-link" style="padding:0px 1px 0px 1px;"><%=RS("brandName")%></a></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
															<%
															RS.movenext
															a = a + 1
															if a = 4 then
																response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""4""><img src=""/images/spacer.gif"" width=""5"" height=""5""></td></tr><tr>" & vbcrlf
																a = 0
															end if
														loop
														if a = 1 then
															response.write "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
														elseif a = 2 then
															response.write "<td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
														elseif a = 3 then
															response.write "<td>&nbsp;</td>" & vbcrlf
														end if
														%>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="center" valign="top">&nbsp;</td>
										</tr>
										<%if SeBottomText <> "" then%>
											<tr>
												<td align="center">
													<p>&nbsp;</p>
													<table border="0" cellspacing="0" cellpadding="2" class="cate-pro-border">
														<tr>
															<td align="left" valign="top" class="static-content-font">
																<%=SeBottomText%>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										<%end if%>
										<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
									</table>
<%
call fCloseConn()
%>
<script>
window.WEDATA.pageType = 'category';
window.WEDATA.categoryData = {
    categoryId: <%= jsStr(categoryid) %>,
    categoryName: <%= jsStr(nameSEO(categoryName))%>
};
</script>
<!--#include virtual="/includes/template/bottom.asp"-->
