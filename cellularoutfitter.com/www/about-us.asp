<%
pageTitle = "About Us"
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
    <table border="0" cellspacing="0" cellpadding="0" width="775">
        <tr>
            <td class="top-sublink-gray">
                <a class="top-sublink-gray" href="http://www.CellularOutfitter.com/">Cell Phone Accessories</a>&nbsp;>&nbsp;<span class="top-sublink-blue"><%=pageTitle%></span>
            </td>
        </tr>
        <tr>
            <td class="top-sublink-gray"><img src="http://www.cellularoutfitter.com/images/spacer.gif" width="1" height="10"></td>
        </tr>
        <tr>
            <td align="left" valign="top" class="static-content-font">
                <p>Why pay retail prices when you can get all of your cell phone accessories at wholesale prices at CellularOutfitter.com? We have thousands of accessories to choose from for every brand of phone like Apple, Samsung, Motorola, HTC, and LG: all at blowout prices. You might be wondering how we are able to offer such incredible prices and still be in business?!? The answer is quite simple - years of cultivating relationships with multiple distributors. Cell phone accessories are our specialty, and we've been in the game for a long time. This allows us to get the best pricing available and pass the savings along to you. We offer the same product in the same packaging as the retail chains without the retail mark up.</p>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" class="static-content-font">&nbsp;</td>
        </tr>
        <tr>
            <td align="left" valign="top" class="static-content-font">
                <ul>
                    <li>Over the years we have established substantial pricing leverage with manufacturers, enabling us to offer you <strong>WHOLESALE PRICING</strong> on all of our products. You are paying the actual &quot;landed-cost&quot; plus a nominal shipping and handling fee to cover processing and fulfillment.</li>
                    <li>We carry thousands upon thousands of accessories for your cell phone that aren't available at big-box chains or smaller retailers. We not only carry the must-have items for the most popular phones, but also the little things that most people don't think of but boost performance and protect the life of your phone.</li>
                    <li>Our warehouse is designed to service you better. We have a team of real-live people carefully processing and packaging your order. In many cases your order is sent out from our warehouse on the <strong>SAME DAY</strong>.  All other orders go out within 1 business day.  Your specific location and what shipping option you have selected determine just how quickly your package arrives.</li>
                    <li>Identity theft is a legitimate concern in today's e-commerce marketplace.  For your piece of mind, we use state of the art secure socket-layer e-commerce shopping technology consistent with the highest standards online to ensure that your information is transmitted safely and securely. We GUARANTEE your information to be safe with us and is NEVER shared with any third parties. All personal information collected is used only for fulfilling orders and for sharing our exclusive offers with you.</li>
                    <li>CellularOutfitter.com firmly believes in the quality of our products and backs that up with an iron-clad <strong>100% Satisfaction Guarantee</strong>. We promise you will be amazed with each and every purchase you make with us. If for ANY reason you are not completely satisfied, simply send your product back to us for an immediate replacement or refund.</li>
                </ul>
            </td>
        </tr>
    </table>
<!--#include virtual="/includes/template/bottom.asp"-->
