<%
	response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
	dim pageTitle : pageTitle = "Category-Brand"
	
	set fso = CreateObject("Scripting.FileSystemObject")
	Set Img = Server.CreateObject("Persits.Jpeg")
	set Jpeg = Server.CreateObject("Persits.Jpeg")
	Jpeg.Quality = 60
	Jpeg.Interpolation = 1
	
	stitchPath = "/images/stitch/categories/"
	
	dim modelPage
	modelPage = 1
	leftGoogleAd = 1
	
	dim brandID, categoryid
	brandID = prepInt(request.querystring("brandID"))
	if brandID = 0 then
		session("errorCode") = 102
		call PermanentRedirect("/")
	end if
	categoryid = prepInt(request.querystring("categoryid"))
	if categoryid = 0 then
		session("errorCode") = 103
		call PermanentRedirect("/")
	end if
	dim cbSort : cbSort = prepStr(request.Form("cbSort"))
	ogCbSort = cbSort
	if cbSort = "" then cbSort = "NO"
	
	response.Cookies("myBrand") = brandID
	response.Cookies("myModel") = 0
	
	call fOpenConn()
	
	if categoryid = 24 then useCatID = 3 else useCatID = categoryid
	sql = "select typename, typeid, subtypeid from v_subTypeMatrix_co where typeid = '" & useCatID & "'"
	session("errorSQL") = SQL

	strSubTypeID = ""
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1

	if RS.eof then
		session("errorCode") = 104
		call PermanentRedirect("/")
	else
		categoryName = RS("typeName")
		parentTypeID = RS("typeid")
		do until RS.eof
			strSubTypeID = strSubTypeID & RS("subtypeid") & ","
			RS.movenext
		loop
		strSubTypeID = left(strSubTypeID, len(strSubTypeID) - 1)
	end if
	if strSubTypeID = "" then strSubTypeID = "9999999" end if
	
	if categoryid = 5 then	'handsfree
		sql	=	"select	distinct '" & categoryName & "' as typename " & vbcrlf & _
				"	, 	x.brandname, x.brandimg, x.temp, x.modelid, x.modelname, x.modelimg, x.releaseyear, x.releasequarter, x.isPhone, x.isTablet, x.topModel" & vbcrlf & _
				"from	(	select	c.brandname, c.brandimg, a.temp, a.modelid, a.modelname, a.modelimg, a.releaseyear, a.releasequarter, '''' + replace(a.handsfreetypes, ',', ''',''') + '''' handsfreetypes, a.isPhone, a.isTablet, a.topModel" & vbcrlf & _
				"			from	we_models a join we_items b " & vbcrlf & _
				"				on	a.modelid = b.modelid join we_brands c " & vbcrlf & _
				"				on	a.brandid = c.brandid " & vbcrlf & _
				"			where	a.isTablet = 0 and a.hidelive = 0 and c.brandid = '" & brandID & "' and c.brandtype < 2 and b.hidelive = 0 " & vbcrlf & _
				"				and a.modelname not like 'all models%' and b.inv_qty <> 0 " & vbcrlf & _
				"				and	a.handsfreetypes is not null ) x join" & vbcrlf & _
				"		(	select	distinct '''' + convert(varchar(10), handsfreetype) + '''' handsfreetype" & vbcrlf & _
				"			from	we_items" & vbcrlf & _
				"			where	hidelive = 0 and inv_qty <> 0 and typeid = 5 ) y" & vbcrlf & _
				"	on	x.handsfreetypes like '%' + y.handsfreetype + ',%'" & vbcrlf & _
				"where	1=1" & vbcrlf				
	elseif categoryid = 19 then 'vinyl skins
		sql	=	"select	distinct '" & categoryName & "' as typename " & vbcrlf & _
				"	,	c.brandname, c.brandimg, a.temp, a.modelid, a.modelname, a.modelimg, a.releaseyear, a.releasequarter, a.isPhone, a.isTablet, a.topModel" & vbcrlf & _
				"from	we_models a join we_items b " & vbcrlf & _
				"	on	a.modelid = b.modelid join we_brands c " & vbcrlf & _
				"	on	a.brandid = c.brandid " & vbcrlf & _
				"where	a.isTablet = 0 and a.hidelive = 0 and c.brandid = '" & brandID & "' and c.brandType < 2 and b.hidelive = 0 " & vbcrlf & _
				"	and a.modelname not like 'all models%' and b.inv_qty <> 0 " & vbcrlf & _
				"	and b.subtypeid in (" & strSubTypeID & ")" & vbcrlf & _
				"union" & vbcrlf & _
				"select	distinct '" & categoryName & "' as typename " & vbcrlf & _
				"	,	c.brandname, c.brandimg, a.temp, a.modelid, a.modelname, a.modelimg, a.releaseyear, a.releasequarter, a.isPhone, a.isTablet, a.topModel" & vbcrlf & _
				"from	we_models a join we_items_musicskins b " & vbcrlf & _
				"	on	a.modelid = b.modelid join we_brands c " & vbcrlf & _
				"	on	a.brandid = c.brandid " & vbcrlf & _
				"where	a.hidelive = 0 and c.brandid = '" & brandID & "' and c.brandType < 2" & vbcrlf & _
				"	and a.modelname not like 'all models%'" & vbcrlf & _
				"	and	b.skip = 0 and b.deleteItem = 0 and b.artist <> '' and b.artist is not null and b.designname <> '' and b.designname is not null	" & vbcrlf
	elseif categoryid = 24 then 'custom cases
		sql =	"select	distinct 'Custom Cases' as typename, c.brandname, c.brandimg, a.temp, a.modelid, a.modelname, a.modelimg, a.releaseyear, a.releasequarter, a.isPhone, a.isTablet, a.topModel" & vbcrlf & _
				"from we_models a " & vbcrlf & _
					"join we_items b on	a.modelid = b.modelid " & vbcrlf & _
					"join we_brands c on a.brandid = c.brandid " & vbcrlf & _
					"join we_ItemsExtendedData d on b.partNumber = d.partNumber " & vbcrlf & _
				"where	a.isTablet = 0 and a.hidelive = 0 and c.brandid = '" & brandID & "' and c.brandType < 2 and b.hidelive = 0 " & vbcrlf & _
				"	and a.modelname not like 'all models%' and b.inv_qty <> 0 " & vbcrlf & _
				"	and d.customize = 1"
	else
		sql	=	"select	distinct '" & categoryName & "' as typename " & vbcrlf & _
				"	,	c.brandname, c.brandimg, a.temp, a.modelid, a.modelname, a.modelimg, a.releaseyear, a.releasequarter, a.isPhone, a.isTablet, a.topModel" & vbcrlf & _
				"from	we_models a join we_items b " & vbcrlf & _
				"	on	a.modelid = b.modelid join we_brands c " & vbcrlf & _
				"	on	a.brandid = c.brandid " & vbcrlf & _
				"where	a.hidelive = 0 and c.brandid = '" & brandID & "' and c.brandtype < 2 and b.hidelive = 0 " & vbcrlf & _
				"	and a.modelname not like 'all models%' and b.inv_qty <> 0 " & vbcrlf & _
				"	and	b.subtypeid in (" & strSubTypeID & ")" & vbcrlf & _
				"union" & vbcrlf & _
				"select	distinct '" & categoryName & "' as typename " & vbcrlf & _
				"	,	c.brandname, c.brandimg, a.temp, a.modelid, a.modelname, a.modelimg, a.releaseyear, a.releasequarter, a.isPhone, a.isTablet, a.topModel" & vbcrlf & _
				"from	we_models a join we_subrelateditems b " & vbcrlf & _
				"	on	a.modelid = b.modelid join we_brands c " & vbcrlf & _
				"	on	a.brandid = c.brandid join we_items d" & vbcrlf & _
				"	on	b.itemid = d.itemid" & vbcrlf & _
				"where	a.hidelive = 0 and c.brandid = '" & brandID & "' and c.brandtype < 2 and d.hidelive = 0 " & vbcrlf & _
				"	and a.modelname not like 'all models%' and d.inv_qty <> 0 " & vbcrlf & _
				"	and	b.subtypeid in (" & strSubTypeID & ")" & vbcrlf
	end if
	
	if brandID <> 17 then
		sql = sql & "	and	isTablet = 0" & vbcrlf
	end if
	if brandID = 15 then
		sql = replace(sql,"c.brandid = '" & brandID & "'","(c.brandid = '" & brandID & "' or c.other = 1)")
	end if

	listSQL = sql & " order by modelname"
'	response.write "<pre>" & listSQL & "</pre>"	
	session("errorSQL") = listSQL
	set listRS = oConn.execute(listSQL)
	
	select case cbSort
		case "AZ" : sql = sql & " order by modelname"
		case "ZA" : sql = sql & " order by modelname desc"
		case "NO" : sql = sql & " order by isPhone desc, isTablet, releaseYear desc, releaseQuarter desc"
		case "ON" : sql = sql & " order by releaseYear, releaseQuarter"
	end select
	session("errorSQL") = sql
	set rs = Server.CreateObject("ADODB.Recordset")
'	response.write "<pre>" & sql & "</pre>"
	rs.open sql, oConn, 0, 1
	
	if RS.eof then
		session("errorCode") = 105
		call PermanentRedirect("/")
	end if
	
	dim brandName, brandImg, categoryName, lap, productList, numTopModels
	numTopModels = 0
	productList = ""
	lap = 0
	do while not RS.EOF
		lap = lap + 1
		if lap = 1 then
			brandName = RS("brandName")
			brandImg = RS("brandImg")
			categoryName = RS("typeName")
		end if
		productList = productList & RS("modelID") & "*" & RS("modelName") & "*" & RS("modelImg") & "*" & RS("topModel") & "#"
		if RS("topModel") then numTopModels = numTopModels + 1
		RS.movenext
	loop
	
	productArray = split(productList,"#")
	itemImgPath = "cat_" & formatSEO(brandName) & "_" & categoryid & "_" & cbSort & ".jpg"
	if instr(request.ServerVariables("SERVER_NAME"), "staging") > 0 then itemImgPath = "staging_" & itemImgPath
	
	deleteOldStitch(itemImgPath)
	
	'if stitch image contains wrong number of items then delete it
	if fso.FileExists(Server.MapPath(stitchPath & itemImgPath)) then
		imgWidth = 70 * (lap)
		set oImg = Server.CreateObject("Persits.Jpeg")
		oImg.Open (Server.MapPath(stitchPath & itemImgPath))
		if isnumeric(imgWidth) and isnumeric(oImg.Width) then	
			if cint(imgWidth) <> cint(oImg.Width) then
				fso.DeleteFile(Server.MapPath(stitchPath & itemImgPath))
			end if
		end if
	end if
	
	if not fso.FileExists(Server.MapPath(stitchPath & itemImgPath)) then	
		'create single product image
		imgWidth = 70 * (lap)
		Jpeg.New imgWidth, 112, &HFFFFFF
		imgX = 0
		imgY = 0
		for i = 0 to (ubound(productArray) - 1)
			curProductArray = split(productArray(i),"*")
			useNA = 0
			inner_itemImgPath = Server.MapPath("/modelPics/thumbs") & "\" & curProductArray(2)
			inner_itemImgPath2 = Server.MapPath("/modelPics") & "\" & curProductArray(2)
			if not fso.FileExists(inner_itemImgPath) then
				if not fso.FileExists(inner_itemImgPath2) then
					useNA = 1
				else
					session("errorSQL") = "inner_itemImgPath2:" & inner_itemImgPath2 & "<br>"
					Img.Open inner_itemImgPath2
					Img.Height = 112
					Img.Width = 70
					Img.Save inner_itemImgPath
				end if
			end if
			if useNA = 1 then
				Img.Open Server.MapPath("/modelPics/thumbs/modelna.jpg")
				Jpeg.Canvas.DrawImage X + (imgX)/2, Y + (imgY)/2, Img
				imgX = imgX + 140
			elseif not isnull(curProductArray(2)) and len(curProductArray(2)) > 0 then
				Img.Open Server.MapPath("/modelPics/thumbs/") & "\" & curProductArray(2)
				Jpeg.Canvas.DrawImage X + (imgX)/2, Y + (imgY)/2, Img
				imgX = imgX + 140
			end if
		next
		
		
		on error resume next
			Jpeg.Save Server.MapPath(stitchPath & itemImgPath)
			errorLap = 0
			do while err.number <> 0
				errorLap = errorLap + 1
				response.Write("<!-- ImgError:" & errorLap & " -->")
				err.Clear
				Jpeg.Save Server.MapPath(stitchPath & itemImgPath)
				if errorLap = 200 then exit do
			loop
		On Error Goto 0
	end if
	
	function deleteOldStitch(stitchName)
		session("errorSQL") = "fileExists: /images/stitch/categories/" & stitchName
		if fso.FileExists(Server.MapPath(stitchPath & stitchName)) then
			set fsTemp = fso.GetFile(Server.MapPath(stitchPath & stitchName))
			createDate = fsTemp.DateLastModified
			if datediff("h",createDate,now) > 1 then
				fso.deleteFile(Server.MapPath(stitchPath & stitchName))
'				response.Write("<!-- cat file deleted -->")
			else
'				response.Write("<!-- cat file valid -->")
			end if
		end if
	end function
	
	'=========================================================================================
	Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
		oParam.CompareMode = vbTextCompare
		oParam.Add "x_categoryID", categoryid
		oParam.Add "x_brandID", brandid	
		oParam.Add "x_categoryName", categoryName
		oParam.Add "x_brandName", brandName
	
'	call redirectURL("cb", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
	'=========================================================================================
	
	dim SEtitle, SEdescription, SEkeywords, topText, bottomText
	
	phoneList = ""
	if isarray(productArray) then
		for i = 0 to (ubound(productArray) - 1)
			curProductArray = split(productArray(i),"*")
			if phoneList = "" then
				phoneList = """" & curProductArray(1) & """"
			else
				phoneList = phoneList & ", """ & curProductArray(1) & """"
			end if
		next	
	end if
	
	dim strBreadCrumb, headerImgAltText, h1
	strBreadCrumb = brandName & " Cell Phone " & categoryName
	
	if categoryid = "3" or categoryid = "7" then
		h1 = brandName & " Cell Phone " & categoryName
	elseif brandid = "17" then
		h1 = "Apple iPhone " & categoryName
	else
		h1 = brandName & " " & categoryName
	end if
	
	headerImgAltText = h1
	
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td class="top-sublink-gray">
            <a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
            <a class="top-sublink-gray" href="/c-<%=categoryid & "-" & formatSEO(categoryName)%>.html">Cell Phone <%=categoryName%></a>&nbsp;>&nbsp;
            <span class="top-sublink-blue"><%=strBreadCrumb%></a></span>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" style="padding:5px 0px 5px 0px;" title="<%=headerImgAltText%>">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td width="214px" align="left"><img src="/images/logos/<%=brandID%>.png" border="0" width="214" height="74" /></td>
                    <td width="*">
						<div style="text-align:center;"><h1 style="color:#000; font-size:24px; margin:0px; padding:0px;"><%=seH1%></h1></div>
                    	<div style="padding-top:10px; font-size:17px; text-align:center;">Select A Phone Model Below To Shop</div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <%if numTopModels > 0 then%>
    <tr><td style="font-size:24px; padding:15px 0px 15px 0px;">Top <%=brandName%> Models</td></tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr><% call drawModels(productArray, itemImgPath, true) %></tr>
            </table>
        </td>
    </tr>
    <%end if%>
    <tr>
    	<td style="padding:20px 0px 20px 20px;">
        	<div style="float:left;"><img src="/images/backgrounds/gray-bar-left.png" border="0" width="22" height="64" /></div>
            <div style="float:left; height:64px; width:735px; background-image:url(/images/backgrounds/gray-bar-background.png)">
            	<div style="float:left;"><img src="/images/icons/gray-bar-bluedot2.png" border="0" width="26" height="64" /></div>
            	<div style="float:left; margin-top:27px; font-weight:bold; padding-left:10px; font-size:11px;">Select Your Cell Phone Model Below.</div>
                <div style="float:left; margin-top:25px; padding-left:20px;">
                	<form name="frmSort" method="post">
                        <select name="cbSort" onChange="this.form.submit();">
                            <option value="AZ" <%if cbSort = "AZ" then %>selected<% end if%>>Sort By A to Z</option>
                            <option value="ZA" <%if cbSort = "ZA" then %>selected<% end if%>>Sort By Z to A</option>
                            <option value="NO" <%if cbSort = "NO" then %>selected<% end if%>>Sort By Newest to Oldest</option>
                            <option value="ON" <%if cbSort = "ON" then %>selected<% end if%>>Sort By Oldest to Newest</option>
                        </select>
                    </form>
                </div>
                <div style="float:left; padding-left:10px;"><img src="/images/backgrounds/gray-bar-or.png" border="0" width="44" height="64" /></div>
                <div style="float:left; margin-top:25px; padding-left:10px;">
                	<select name="selectModel" onChange="if(this.value != ''){window.location=this.value;}" style="width:150px;">
                        <option value="">Select from this list</option>
                        <%
                        do while not listRS.EOF
                            modelID = listRS("modelID")
                            modelName = listRS("modelName")
                        %>
                        <option value="/sc-<%=categoryid & "-sb-" & brandID & "-sm-" & modelID & "-" & formatSEO(categoryName) & "-for-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.html"><%=modelName%></option>
                        <%
							listRS.movenext
                        loop
                        %>
                    </select>
                </div>
            </div>
            <div style="float:left;"><img src="/images/backgrounds/gray-bar-right.png" border="0" width="22" height="64" /></div>
        </td>
    </tr>
    <tr>
        <td align="center" width="100%">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td width="100%">
                        <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                            <tr><% call drawModels(productArray, itemImgPath, false) %></tr>
                        </table>
                    </td>
                </tr>
                <tr><td class="static-content-font" valign="top" style="text-align:left; padding:10px 0px 10px 0px;"><%=seTopText%></td></tr>
                <tr><td class="static-content-font" valign="top" style="text-align:left;"><%=SeBottomText%></td></tr>
                <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script>
window.WEDATA.pageType = 'categoryBrand';
window.WEDATA.pageData = {
	brand: <%= jsStr(brandName) %>,
	brandId: <%= jsStr(brandId) %>,
	category: <%= jsStr(categoryName) %>,
	categoryId: <%= jsStr(categoryId) %>	
};
</script>

<!--#include virtual="/includes/template/bottom.asp"-->
<%
	sub drawModels(byref arrProduct, imgStitch, showTopModel)
		a = 0
		useX = 0
		dim altText
		for i = 0 to (ubound(arrProduct) - 1)
			curProductArray = split(arrProduct(i),"*")
			if categoryid = "7" then
				if brandid = "9" then
					altText = "Samsung " & curProductArray(1) & " cell phone cases and pouches"
				end if
			elseif categoryid = "3" then
				if brandid = "5" then
					altText = "Motorola " & curProductArray(1) & " Cell Phone Covers"
				elseif brandid = "4" then
					altText = "LG " & arrProduct(1) & " covers"
				elseif brandid = "14" then
					altText = "Blackberry " & curProductArray(1) & " Covers"
				end if
				
			end if
			
			if altText = "" then altText = brandName & " " & curProductArray(1) & " " & categoryName end if
			
			if categoryID = 19 then
				useLink = "/sb-" & brandID & "-sm-" & curProductArray(0) & "-sc-" & categoryid & "-" & formatSEO(brandName) & "-" & formatSEO(curProductArray(1)) & "-" & formatSEO(categoryName) & "-select.html"
			else
				useLink = "/sb-" & brandID & "-sm-" & curProductArray(0) & "-sc-" & categoryid & "-" & formatSEO(categoryName) & "-for-" & formatSEO(brandName) & "-" & formatSEO(curProductArray(1)) & ".html"
			end if
			
			bTopModel = curProductArray(3)
			if showTopModel then	'only top models
				if bTopModel then
					a = a + 1
				%>
				<td valign="top" style="<% if a < 5 then %>border-right:1px solid #CCC;<% end if %> border-bottom:1px solid #ccc;">
					<div style="width:160px; margin:10px 0px 10px 0px;">
						<div style="width:70px; margin-left:auto; margin-right:auto;"><a href="<%=useLink%>"><div id="PhonePic<%=a%>" style="width: 70px; height: 112px; background: url(<%=stitchPath & itemImgPath%>?ud=<%=date%>) <%=useX%>px 0px no-repeat; cursor:pointer;" title="<%=altText%>"></div></a></div>
						<div style="text-align:center; margin:5px 5px 0px 5px; width:150px;"><a style="color:#2d5183; text-decoration:underline; font-size:12px;" href="<%=useLink%>" title="<%=brandName & " " & curProductArray(1)%> Cell Phone Accessories"><%=curProductArray(1) & " " & categoryName%></a></div>
					</div>
				</td>
				<%
				end if
			else					' except for top models
				if not bTopModel then
					a = a + 1
				%>
				<td valign="top" style="<% if a < 5 then %>border-right:1px solid #CCC;<% end if %> border-bottom:1px solid #ccc;">
					<div style="width:160px; margin:10px 0px 10px 0px;">
						<div style="width:70px; margin-left:auto; margin-right:auto;"><a href="<%=useLink%>"><div id="PhonePic<%=a%>" style="width: 70px; height: 112px; background: url(<%=stitchPath & itemImgPath%>?ud=<%=date%>) <%=useX%>px 0px no-repeat; cursor:pointer;" title="<%=altText%>"></div></a></div>
						<div style="text-align:center; margin:5px 5px 0px 5px; width:150px;"><a style="color:#2d5183; text-decoration:underline; font-size:12px;" href="<%=useLink%>" title="<%=brandName & " " & curProductArray(1)%> Cell Phone Accessories"><%=curProductArray(1) & " " & categoryName%></a></div>
					</div>
				</td>
				<%					
				end if
			end if
			
			if a = 5 then
				response.write "</tr><tr>"
				a = 0
				row = row + 1
			end if
			useX = useX - 70
		next
		if a = 1 then
			response.write "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
		elseif a = 2 then
			response.write "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
		elseif a = 3 then
			response.write "<td>&nbsp;</td>" & vbcrlf
		end if
	end sub
%>
