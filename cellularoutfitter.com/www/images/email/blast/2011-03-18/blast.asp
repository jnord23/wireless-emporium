<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%
dim baseUrl : baseUrl = "http://www.cellularoutfitter.com"
%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></meta>
<title>March Madness Sale</title>
<BASE HREF="<%=baseUrl%>">
<style>
/*
h1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	font-style: normal;
	color: #ff0074;
	text-decoration: none;
	display: inline;
}

a:link { font-size: 10pt; color: #CC3300; text-decoration: none; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; }
a:visited { font-size: 10pt; color: #CC3300; text-decoration: none; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; }
a:active { font-size: 10pt; color: #CC3300; text-decoration: none; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; }
a:hover { font-size: 10pt; color: #CC3300; text-decoration: none; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-weight:bold;}
*/

</style>
</head>
<%
dim imgLink 	: 	imgLink 		= 	baseUrl & "/images/email/blast/2011-03-18"
dim strParam 	: 	strParam 		= 	"utm_source=Email&utm_medium=Blast&utm_campaign=CO10MARCH"
dim styleTopNav	:	styleTopNav		=	"font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #A9A9A9; font-weight:bold; text-decoration:none;"
%>
<body class="body">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="710px">
	<tr>
    	<td width="710px"><img SRC="<%=baseUrl%>/images/email/blast/null.gif"></td>
    </tr>
    <tr>
		<td width="710px">
			<table border="0" cellpadding="0" cellspacing="0" width="710px" bgcolor="#FFFFFF">
				<tr>
					<td align="right" valign="top" bgcolor="#FFFFFF">
						<font style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; color: #777777; text-decoration:none;" >Can't view this? 
							<a target="_blank" href="<%=imgLink%>/blast.asp?<%=strParam%>">Click here</A>.
						</font>
					</td>					
				</tr>
			</table>
		</td>
	</tr>
    <tr>
    	<td width="710">
        	<table border="0" cellpadding="0" cellspacing="0" width="710">
            	<tr>
                    <td width="710" align="right" valign="middle" height="75px">
						<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
							<tr>
								<td width="253" height="75" align="left" valign="middle" bgcolor="#FFFFFF">
									<a target="_blank" href="<%=baseUrl%>/?<%=strParam%>" style="text-decoration:none;"><img src="<%=baseUrl%>/images/email/blast/logo.jpg" border="0" height="65" width="253"></a>
								</td>
								<td width="9" valign="bottom" align="center" bgcolor="#FFFFFF">
									<img src="<%=baseUrl%>/images/email/blast/top-line.jpg" width="9" height="43">
								</td>
								<td width="65" valign="bottom" align="center" bgcolor="#FFFFFF">
									<a target="_blank" href="<%=baseUrl%>/c-3-cell-phone-covers-screen-guards.html?<%=strParam%>" style="text-decoration:none;"><img src="<%=baseUrl%>/images/email/blast/covers.jpg" width="65" height="43" border="0"></a>
								</td>
								<td width="9" valign="bottom" align="center" bgcolor="#FFFFFF">
									<img src="<%=baseUrl%>/images/email/blast/top-line.jpg" width="9" height="43">
								</td>
								<td width="79" valign="bottom" align="center" bgcolor="#FFFFFF">
									<a target="_blank" href="<%=baseUrl%>/c-2-cell-phone-chargers.html?<%=strParam%>" style="text-decoration:none;"><img src="<%=baseUrl%>/images/email/blast/chargers.jpg" width="79" height="43" border="0"></a>
								</td>
								<td width="9" valign="bottom" align="center" bgcolor="#FFFFFF">
									<img src="<%=baseUrl%>/images/email/blast/top-line.jpg" width="9" height="43">
								</td>
								<td width="74" valign="bottom" align="center" bgcolor="#FFFFFF">
									<a target="_blank" href="<%=baseUrl%>/c-1-cell-phone-batteries.html?<%=strParam%>" style="text-decoration:none;"><img src="<%=baseUrl%>/images/email/blast/batteries.jpg" width="74" height="43" border="0"></a>
								</td>
								<td width="9" valign="bottom" align="center" bgcolor="#FFFFFF">
									<img src="<%=baseUrl%>/images/email/blast/top-line.jpg" width="9" height="43">
								</td>
								<!--
								<td width="85" valign="bottom" align="center" bgcolor="#FFFFFF">
									<a target="_blank" href="<%=baseUrl%>/bluetooth.html?<%=strParam%>" style="text-decoration:none;"><img src="<%=baseUrl%>/images/email/blast/bluetooth.jpg" width="85" height="43" border="0"></a>
								</td>
								<td width="9" valign="bottom" align="center" bgcolor="#FFFFFF">
									<img src="<%=baseUrl%>/images/email/blast/top-line.jpg" width="9" height="43">
								</td>
								-->
								<td width="57" valign="bottom" align="center" bgcolor="#FFFFFF">
									<a target="_blank" href="<%=baseUrl%>/c-7-cell-phone-cases-pouches.html?<%=strParam%>" style="text-decoration:none;"><img src="<%=baseUrl%>/images/email/blast/cases.jpg" width="57" height="43" border="0"></a>
								</td>
							</tr>
						</table>			
					</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
		<td width="710px">
			<table border="0" cellpadding="0" cellspacing="0" width="710px">
				<tr>
					<td colspan="3" width="710" height="90">
						<a target="_blank" href="<%=baseUrl%>/?<%=strParam%>" style="text-decoration:none;"><img src="<%=imgLink%>/blueHeader.jpg" border="0" style="display:block;"></a>
					</td>
				</tr>
				<tr>
					<td rowspan="3" width="270" height="563">
						<a target="_blank" href="<%=baseUrl%>/p-69594-apple-iphone-4-verizon-black-w-steel-kickstand-cover.html?<%=strParam%>" style="text-decoration:none;">
							<img src="<%=imgLink%>/product_1.jpg" border="0" style="display:block;">
						</a>
					</td>
					<td colspan="2" width="440" height="119">
						<a target="_blank" href="<%=baseUrl%>/?<%=strParam%>" style="text-decoration:none;">
							<img src="<%=imgLink%>/promoArea.jpg" border="0" style="display:block;">
						</a>
					</td>
				</tr>
				<tr>
					<td colspan="2" width="440" height="46">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="297" height="46" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #656565; font-weight:bold; text-decoration:none;" bgcolor="#FFFFFF">
									<b>COUPON CODE: <font color="#295185">CO10MARCH</font></b><br>
									<font style="font-size:10px; color:#AEAEAE;">Offer Ends: 3/31/2011</font>
								</td>
								<td width="143" height="46">
									<a target="_blank" href="<%=baseUrl%>/?<%=strParam%>" style="text-decoration:none;">
										<img src="<%=imgLink%>/gradientSidePiece.jpg" border="0" style="display:block;">
									</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="222" height="318">
						<a target="_blank" href="<%=baseUrl%>/p-72173-samsung-galaxy-s-4g-sgh-t959v-super-rapid-ic-micro-usb-car-charger.html?<%=strParam%>" style="text-decoration:none;">
							<img src="<%=imgLink%>/product_2.jpg" border="0" style="display:block;">
						</a>
					</td>
					<td width="218" height="318">
						<a target="_blank" href="<%=baseUrl%>/p-68366--xxl--mega-clip-neoprene-carrying-case-pouch-for-motorola-atrix-4g.html?<%=strParam%>" style="text-decoration:none;">
							<img src="<%=imgLink%>/product_3.jpg" border="0" style="display:block;">
						</a>
					</td>
				</tr>
			</table>
        </td>
    </tr>	
    </tr>	
	<tr>
		<td width="710" height="92">
			<a target="_blank" href="<%=baseUrl%>/?<%=strParam%>" style="text-decoration:none;">
				<img src="<%=imgLink%>/bottom.jpg" border="0" style="display:block;">
			</a>
		</td>
	</tr>	
	<tr>
		<td width="710px">
			<table border="0" cellpadding="0" cellspacing="0" width="710px">
				<tr>
					<td width="218" height="164">
						<a target="_blank" href="http://www.facebook.com/cellularoutfitter/?<%=strParam%>" style="text-decoration:none;">
							<img src="<%=imgLink%>/connect_1.jpg" border="0" style="display:block;">
						</a>	
					</td>
					<td width="244" height="164">
						<a target="_blank" href="http://twitter.com/celloutfitter/?<%=strParam%>" style="text-decoration:none;">
							<img src="<%=imgLink%>/connect_2.jpg" border="0" style="display:block;">
						</a>	
					</td>
					<td width="248" height="164">
						<a target="_blank" href="http://www.cellularoutfitter.com/blog/?<%=strParam%>" style="text-decoration:none;">
							<img src="<%=imgLink%>/connect_3.jpg" border="0" style="display:block;">
						</a>	
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td width="710px" style="font-size:1px;" height="5px">&nbsp;</td>
	</tr>	
	<tr>
		<td width="710px" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; font-weight:normal; text-decoration:none;">
			This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from CellularOutfitter.com.<br> 
			If this message has been sent in error, or you would no longer like to receive any more periodic offers from CellularOutfitter.com, you may <a href="http://www.cellularoutfitter.com/unsubscribe.asp?strEmail=webmaster@cellularoutfitter.com&<%=strParam%>" style="text-decoration:underline; font-size:10px;">click here</a> to unsubscribe.
		</td>
	</tr>
</table>
</body>
</html>
