<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Save {{percentage_off}} On {{model_name}} Accessories!</title>
<style type="text/css">
.ReadMsgBody {
	width: 100%;
}
.ExternalClass {
	width: 100%;
}
</style>
</head>

<body style="margin: 0; padding: 0; width: 100%;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background-color: #f7f7f7;" width="100%" align="center"><!-- Content -->
      
      <table style="margin: 0;" width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px;" width="640" height="50" align="center" valign="middle">Having trouble viewing this email? <a style="color: #006699;" href="http://www.cellularoutfitter.com/images/email/blast/{{campaign_id}}/{{model_id}}/blast.asp?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Click here</a>.</td>
        </tr>
        <tr>
          <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px;" width="640" height="30" align="center" valign="middle">Save <b>{{percentage_off}}&#37;</b> On {{model_name}} Accesories - Use Coupon Code <b>{{promo_code}}</b> @ Checkout! Expires {{expiration_date}}.</td>
        </tr>
      </table>
      <table width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="background-color: #ffffff; color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 1.75;" width="240" height="80" align="center" valign="middle"><a href="http://www.cellularoutfitter.com/?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="http://www.cellularoutfitter.com/images/email/blast/template/images/co-logo.gif" width="220" height="45" alt="CellularOutfitter.com" border="0" /></a> <b>WHOLESALE PRICES</b> ON ACCESSORIES</td>
          <td style="background-color: #ffffff;" width="390" align="right" valign="middle"><a href="http://www.cellularoutfitter.com/?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img src="http://www.cellularoutfitter.com/images/email/blast/template/images/co-values.gif" width="300" height="38" alt="110% Low Price Guarantee | 90-Day Return Policy" border="0" /></a></td>
          <td style="background-color: #ffffff;" width="10"></td>
        </tr>
      </table>
      <table width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=3}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Covers &amp; Skins</a></td>
          <td style="background-color: #124f80;" width="1" height="45"></td>
          <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=7}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Cases &amp; Pouches</a></td>
          <td style="background-color: #124f80;" width="1" height="45"></td>
          <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=18}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Screen Protectors</a></td>
          <td style="background-color: #124f80;" width="1" height="45"></td>
          <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="http://www.cellularoutfitter.com/sb-9-sm-1390-sc-24-cell-phone-custom-cases.html" target="_blank">Custom Covers</a></td>
          <td style="background-color: #124f80;" width="1" height="45"></td>
          <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=2}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Chargers Cables</a></td>
          <td style="background-color: #124f80;" width="1" height="45"></td>
          <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=5}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Bluetooth Heasdsets</a></td>
          <td style="background-color: #124f80;" width="1" height="45"></td>
          <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=6}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Holsters Mounts</a></a></td>
          <td style="background-color: #124f80;" width="1" height="45"></td>
          <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="80" height="45" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="http://www.cellularoutfitter.com/sb-0-sm-0-sc-8-cell-phone-other-accessories.html?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Other Accessories</a></td>
        </tr>
      </table>
      <table width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="background-color: #ebebeb;" width="640"><table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10"></td>
                <td width="320" align="center" valign="middle"><span style="color: #2ecc71; font-family: Verdana, Geneva, sans-serif; font-size: 24px; line-height: 1.75; margin: 0; padding: 0;">{{model_name}}</span><br />
                  <hr style="background-color: #999999; border: 0; height: 1px; margin: 0; width: 300px;" />
                  <span style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 16px; line-height: 1.75; margin: 0; padding: 0;">Cell Phone Accessories</span><br />
                  <table width="320" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 96px; line-height: 1.0;" align="right" valign="middle" width="155" height="115">{{percentage_off}}</td>
                      <td width="10"></td>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 48px; line-height: 1.0;" align="left" valign="middle" width="155" height="115">&#37;<br />
                        OFF</td>
                    </tr>
                  </table>
                  <span style="background-color: #ffffff; border: 1px dotted #999999; color: #333333; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 14px; line-height: 2.5; margin: 0; padding: 0; width: 220px;">USE CODE: <b style="color: #2ecc71;">{{promo_code}}</b></span><br />
                  <a style="background-color: #2ecc71; border: 2px solid #27ae60; color: #ffffff; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin: 0; padding: 10px 0; text-decoration: none; width: 180px;" href="{{model_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">SHOP NOW &rarr;</a></td>
                <td width="20"></td>
                <td width="290" align="left" valign="middle"><a href="{{model_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><br />
                  <img style="display: block;" src="{{model_image}}" width="280" height="310" alt="Shop {{model_name}} Accessories!" border="0" /></a><br /></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="background-color: #ffffff;" width="640"><br />
            <table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10"></td>
                <td style="border: 1px solid #cccccc;" width="198" align="center" valign="middle"><br />
                  <a href="{{rec_item_1_link}}" target="_blank"><img style="display: block;" src="{{rec_item_1_image;size=big}}" width="180" height="180" alt="{{rec_item_1_desc}}" border="0" /></a><br />
                  <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 10px; width: 180px;" align="left"><a style="color: #333333; text-decoration: none;" href="{{rec_item_1_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_1_desc}}</a></p>
                  <table width="200" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 1.5; text-decoration: line-through;" width="105" align="right" valign="middle">Retail: {{rec_item_1_retail_price}}<br />
                        Our Price: {{rec_item_1_normal_price}}</td>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 22px; font-weight: bold; line-height: 1.5;" width="85" align="right" valign="middle">{{rec_item_1_promo_price}}</td>
                      <td width="10"></td>
                    </tr>
                  </table>
                  <br />
                  <a style="text-decoration: none;" href="{rec_item_1_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><span style="background-color: #ffffff; border: 1px dotted #999999; color: #333333; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0; padding: 0; width: 180px;">With Coupon Code:<br />
                  <b style="color: #2ecc71;">{{promo_code}}</b></span></a><br /></td>
                <td width="10"></td>
                <td style="border: 1px solid #cccccc;" width="198" align="center" valign="middle"><br />
                  <a href="{{rec_item_2_link}}" target="_blank"><img style="display: block;" src="{{rec_item_2_image;size=big}}" width="180" height="180" alt="{{rec_item_2_desc}}" border="0" /></a><br />
                  <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 10px; width: 180px;" align="left"><a style="color: #333333; text-decoration: none;" href="{{rec_item_2_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_2_desc}}</a></p>
                  <table width="200" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 1.5; text-decoration: line-through;" width="105" align="right" valign="middle">Retail: {{rec_item_2_retail_price}}<br />
                        Our Price: {{rec_item_2_normal_price}}</td>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 22px; font-weight: bold; line-height: 1.5;" width="85" align="right" valign="middle">{{rec_item_2_promo_price}}</td>
                      <td width="10"></td>
                    </tr>
                  </table>
                  <br />
                  <a style="text-decoration: none;" href="{rec_item_2_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><span style="background-color: #ffffff; border: 1px dotted #999999; color: #333333; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0; padding: 0; width: 180px;">With Coupon Code:<br />
                  <b style="color: #2ecc71;">{{promo_code}}</b></span></a><br /></td>
                <td width="10"></td>
                <td style="border: 1px solid #cccccc;" width="198" align="center" valign="middle"><br />
                  <a href="{{rec_item_3_link}}" target="_blank"><img style="display: block;" src="{{rec_item_3_image;size=big}}" width="180" height="180" alt="{{rec_item_3_desc}}" border="0" /></a><br />
                  <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 10px; width: 180px;" align="left"><a style="color: #333333; text-decoration: none;" href="{{rec_item_3_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_3_desc}}</a></p>
                  <table width="200" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 1.5; text-decoration: line-through;" width="105" align="right" valign="middle">Retail: {{rec_item_3_retail_price}}<br />
                        Our Price: {{rec_item_3_normal_price}}</td>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 22px; font-weight: bold; line-height: 1.5;" width="85" align="right" valign="middle">{{rec_item_3_promo_price}}</td>
                      <td width="10"></td>
                    </tr>
                  </table>
                  <br />
                  <a style="text-decoration: none;" href="{rec_item_1_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><span style="background-color: #ffffff; border: 1px dotted #999999; color: #333333; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0; padding: 0; width: 180px;">With Coupon Code:<br />
                  <b style="color: #2ecc71;">{{promo_code}}</b></span></a><br /></td>
                <td width="10"></td>
              </tr>
            </table>
            <br />
            <table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10"></td>
                <td width="299" align="center" valign="middle"><a href="{rec_item_4_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_4_image;size=big}}" width="180" height="180" alt="{{rec_item_4_desc}}" border="0" /></a><br />
                  <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 10px; width: 250px;" align="center"><a style="color: #333333; text-decoration: none;" href="{{rec_item_4_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_4_desc}}</a></p>
                  <table width="200" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 1.5; text-decoration: line-through;" width="105" align="right" valign="middle">Retail: {{rec_item_4_retail_price}}<br />
                        Our Price: {{rec_item_4_normal_price}}</td>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 22px; font-weight: bold; line-height: 1.5;" width="85" align="right" valign="middle">{{rec_item_4_promo_price}}</td>
                      <td width="10"></td>
                    </tr>
                  </table>
                  <br />
                  <a style="text-decoration: none;" href="{{rec_item_4_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><span style="background-color: #ffffff; border: 1px dotted #999999; color: #333333; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0; padding: 0; width: 180px;">With Coupon Code:<br />
                  <b style="color: #2ecc71;">{{promo_code}}</b></span></a></td>
                <td width="10"></td>
                <td style="background-color: #cccccc" width="2"></td>
                <td width="10"></td>
                <td width="299" align="center" valign="middle"><a href="{rec_item_5_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_5_image;size=big}}" width="180" height="180" alt="{{rec_item_5_desc}}" border="0" /></a><br />
                  <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 10px; width: 250px;" align="center"><a style="color: #333333; text-decoration: none;" href="{{rec_item_5_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_5_desc}}</a></p>
                  <table width="200" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 1.5; text-decoration: line-through;" width="105" align="right" valign="middle">Retail: {{rec_item_5_retail_price}}<br />
                        Our Price: {{rec_item_5_normal_price}}</td>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 22px; font-weight: bold; line-height: 1.5;" width="85" align="right" valign="middle">{{rec_item_5_promo_price}}</td>
                      <td width="10"></td>
                    </tr>
                  </table>
                  <br />
                  <a style="text-decoration: none;" href="{{rec_item_5_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><span style="background-color: #ffffff; border: 1px dotted #999999; color: #333333; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0; padding: 0; width: 180px;">With Coupon Code:<br />
                  <b style="color: #2ecc71;">{{promo_code}}</b></span></a></td>
                <td width="10"></td>
              </tr>
            </table>
            <br />
            <center>
              <table width="620" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background-color: #cccccc;" width="620" height="1"></td>
                </tr>
              </table>
            </center>
            <br />
            <table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10"></td>
                <td width="299" align="center" valign="middle"><a href="{rec_item_6_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_6_image;size=big}}" width="180" height="180" alt="{{rec_item_6_desc}}" border="0" /></a><br />
                  <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 10px; width: 250px;" align="center"><a style="color: #333333; text-decoration: none;" href="{{rec_item_6_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_6_desc}}</a></p>
                  <table width="200" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 1.5; text-decoration: line-through;" width="105" align="right" valign="middle">Retail: {{rec_item_6_retail_price}}<br />
                        Our Price: {{rec_item_6_normal_price}}</td>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 22px; font-weight: bold; line-height: 1.5;" width="85" align="right" valign="middle">{{rec_item_6_promo_price}}</td>
                      <td width="10"></td>
                    </tr>
                  </table>
                  <br />
                  <a style="text-decoration: none;" href="{{rec_item_6_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><span style="background-color: #ffffff; border: 1px dotted #999999; color: #333333; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0; padding: 0; width: 180px;">With Coupon Code:<br />
                  <b style="color: #2ecc71;">{{promo_code}}</b></span></a></td>
                <td width="10"></td>
                <td style="background-color: #cccccc" width="2"></td>
                <td width="10"></td>
                <td width="299" align="center" valign="middle"><a href="{rec_item_7_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_7_image;size=big}}" width="180" height="180" alt="{{rec_item_7_desc}}" border="0" /></a><br />
                  <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 10px; width: 250px;" align="center"><a style="color: #333333; text-decoration: none;" href="{{rec_item_7_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_7_desc}}</a></p>
                  <table width="200" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 1.5; text-decoration: line-through;" width="105" align="right" valign="middle">Retail: {{rec_item_7_retail_price}}<br />
                        Our Price: {{rec_item_7_normal_price}}</td>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 22px; font-weight: bold; line-height: 1.5;" width="85" align="right" valign="middle">{{rec_item_7_promo_price}}</td>
                      <td width="10"></td>
                    </tr>
                  </table>
                  <br />
                  <a style="text-decoration: none;" href="{{rec_item_7_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><span style="background-color: #ffffff; border: 1px dotted #999999; color: #333333; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0; padding: 0; width: 180px;">With Coupon Code:<br />
                  <b style="color: #2ecc71;">{{promo_code}}</b></span></a></td>
                <td width="10"></td>
              </tr>
            </table>
            <br />
            <center>
              <table width="620" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background-color: #cccccc;" width="620" height="1"></td>
                </tr>
              </table>
            </center>
            <br />
            <table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10"></td>
                <td width="299" align="center" valign="middle"><a href="{rec_item_8_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_8_image;size=big}}" width="180" height="180" alt="{{rec_item_8_desc}}" border="0" /></a><br />
                  <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 10px; width: 250px;" align="center"><a style="color: #333333; text-decoration: none;" href="{{rec_item_8_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_8_desc}}</a></p>
                  <table width="200" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 1.5; text-decoration: line-through;" width="105" align="right" valign="middle">Retail: {{rec_item_8_retail_price}}<br />
                        Our Price: {{rec_item_8_normal_price}}</td>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 22px; font-weight: bold; line-height: 1.5;" width="85" align="right" valign="middle">{{rec_item_8_promo_price}}</td>
                      <td width="10"></td>
                    </tr>
                  </table>
                  <br />
                  <a style="text-decoration: none;" href="{{rec_item_8_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><span style="background-color: #ffffff; border: 1px dotted #999999; color: #333333; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0; padding: 0; width: 180px;">With Coupon Code:<br />
                  <b style="color: #2ecc71;">{{promo_code}}</b></span></a></td>
                <td width="10"></td>
                <td style="background-color: #cccccc" width="2"></td>
                <td width="10"></td>
                <td width="299" align="center" valign="middle"><a href="{rec_item_9_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_9_image;size=big}}" width="180" height="180" alt="{{rec_item_9_desc}}" border="0" /></a><br />
                  <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 10px; width: 250px;" align="center"><a style="color: #333333; text-decoration: none;" href="{{rec_item_9_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_9_desc}}</a></p>
                  <table width="200" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 1.5; text-decoration: line-through;" width="105" align="right" valign="middle">Retail: {{rec_item_9_retail_price}}<br />
                        Our Price: {{rec_item_9_normal_price}}</td>
                      <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 22px; font-weight: bold; line-height: 1.5;" width="85" align="right" valign="middle">{{rec_item_9_promo_price}}</td>
                      <td width="10"></td>
                    </tr>
                  </table>
                  <br />
                  <a style="text-decoration: none;" href="{{rec_item_9_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><span style="background-color: #ffffff; border: 1px dotted #999999; color: #333333; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0; padding: 0; width: 180px;">With Coupon Code:<br />
                  <b style="color: #2ecc71;">{{promo_code}}</b></span></a></td>
                <td width="10"></td>
              </tr>
            </table>
            <br /></td>
        </tr>
      </table>
      <table width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px;" width="640" height="30" align="center" valign="middle"><b>SHOP WITH CONFIDENCE:</b>&nbsp;&nbsp;&nbsp;&nbsp;110% LOW PRICE GUARANTEE&nbsp;&nbsp;&nbsp;&nbsp;90-DAY RETURNS&nbsp;&nbsp;&nbsp;&nbsp;1-YEAR WARRANTY</td>
        </tr>
      </table>
      <table width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="background-color: #ffffff;" width="10"></td>
          <td style="background-color: #ffffff; color: #006699; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.5; margin: 0;" width="180" align="left"><br />
            <br />
            Connect With Us:</td>
          <td style="background-color: #ffffff;" width="20"></td>
          <td style="background-color: #ffffff; color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.5; margin: 0;" width="215" align="left"><br />
            <br />
            <a style="color: #006699; text-decoration: none;" href="{{model_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Shop Now &rarr;</a></td>
          <td style="background-color: #ffffff; color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.5; margin: 0;" width="215"></td>
        </tr>
        <tr>
          <td style="background-color: #ffffff;" width="10"></td>
          <td style="background-color: #ffffff; color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.5; margin: 0;" width="180"><table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="40" height="30" align="center" valign="top"><img src="http://www.cellularoutfitter.com/images/email/blast/template/images/ico-facebook-2x.gif" width="20" height="20" alt="Facebook" border="0" /></td>
                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;" width="140" align="left" valign="top"><a style="color: #006699; text-decoration: none;" href="http://www.facebook.com/cellularoutfitter?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Facebook</a></td>
              </tr>
              <tr>
                <td width="40" height="30" align="center" valign="top"><img src="http://www.cellularoutfitter.com/images/email/blast/template/images/ico-twitter-2x.gif" width="20" height="20" alt="Twitter" border="0" /></td>
                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;" width="140" align="left" valign="top"><a style="color: #006699; text-decoration: none;" href="http://twitter.com/celloutfitter?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Twitter</a></td>
              </tr>
              <tr>
                <td width="40" height="30" align="center" valign="top"><img src="http://www.cellularoutfitter.com/images/email/blast/template/images/ico-google-2x.gif" width="20" height="20" alt="YouTube" border="0" /></td>
                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;" width="140" align="left" valign="top"><a style="color: #006699; text-decoration: none;" href="http://www.youtube.com/user/CellularOutfitter?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Google+</a></td>
              </tr>
            </table></td>
          <td style="background-color: #ffffff;" width="20"></td>
          <td style="background-color: #ffffff; color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0;" width="215" align="left"><br />
            <a style="color: #006699; text-decoration: none;" href="http://www.cellularoutfitter.com/c-3-cell-phone-covers-and-gel-skins.html?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Covers &amp; Skins</a><br />
            <a style="color: #006699; text-decoration: none;" href="http://www.cellularoutfitter.com/c-7-cell-phone-cases-and-pouches.html?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Cases &amp; Pouches</a><br />
            <a style="color: #006699; text-decoration: none;" href="http://www.cellularoutfitter.com/c-18-cell-phone-screen-protectors.html?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Screen Protectors</a><br />
            <a style="color: #006699; text-decoration: none;" href="http://www.cellularoutfitter.com/c-24-cell-phone-custom-cases.html?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Custom Covers</a><br />
            <br />
            <br /></td>
          <td style="background-color: #ffffff; color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0;" width="215" align="left"><br />
            <a style="color: #006699; text-decoration: none;" href="http://www.cellularoutfitter.com/c-2-cell-phone-chargers-and-data-cables.html?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Chargers &amp; Cables</a><br />
            <a style="color: #006699; text-decoration: none;" href="http://www.cellularoutfitter.com/c-5-cell-phone-bluetooth-and-hands-free.html?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Bluetooth Headsets</a><br />
            <a style="color: #006699; text-decoration: none;" href="http://www.cellularoutfitter.com/c-6-cell-phone-holsters-and-car-mounts.html?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Holsters &amp; Mounts</a><br />
            <a style="color: #006699; text-decoration: none;" href="http://www.cellularoutfitter.com/sb-0-sm-0-sc-8-cell-phone-other-accessories.html?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Other Accessories</a><br />
            <br />
            <br /></td>
        </tr>
      </table>
      <table width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="background-color: #cccccc;" width="640" height="2"></td>
        </tr>
      </table>
      <table width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="background-color: #ebebeb;" width="10"></td>
          <td style="background-color: #ebebeb; color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0;" width="620" align="center"><br />Offer excludes Bluetooth headsets, custom cases and select OEM products.<br /><br />
            This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from CellularOutfitter.com. If this message has been sent in error, or you would no longer like to receive any more periodic offers from CellularOutfitter.com, you may <a style="color: #006699; font-weight: bold;" href="#" target="_blank">unsubscribe</a>.<br />
            <br />
            <a style="color: #006699; font-weight: bold;" href="http://www.wirelessemporium.com/privacy.asp" target="_blank">Privacy Policy</a> | <a style="color: #006699; font-weight: bold;" href="http://www.wirelessemporium.com/termsofuse.asp" target="_blank">Terms &amp; Conditions</a> | <a style="color: #006699; font-weight: bold;" href="http://www.wirelessemporium.com/contactus.asp" target="_blank">Contact Us</a><br />
            <br />
            &copy;2002-2013 CellularOutfitter.com<br />
            <br /></td>
          <td style="background-color: #ebebeb;" width="10"></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
