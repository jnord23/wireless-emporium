<% mobileDetect() %>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/account/account_base.asp"-->
<%	
	curURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	if instr(curURL,"masterPage.asp") then 
		call CloseConn(oConn)
		call PermanentRedirect("http://www.cellularoutfitter.com/")
	end if
	server.ScriptTimeout = 9999
	
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	dim forceRefresh : forceRefresh = 0
	dim brand : brand = request.QueryString("brand")
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	dim itemID : itemID = prepInt(request.QueryString("itemID"))
	dim musicSkins : musicSkins = prepInt(request.QueryString("musicSkins"))
	dim musicSkinGenre : musicSkinGenre = prepStr(request.QueryString("musicSkinGenre"))
	dim musicSkinArtist : musicSkinArtist = prepInt(request.QueryString("musicSkinArtist"))
	dim pageName : pageName = prepStr(request.QueryString("pageName"))
	dim testMVT : testMVT = prepStr(request.QueryString("testMVT"))
	
	dim emailOrderID : emailOrderID = prepInt(request.QueryString("oid"))
	dim utm_campaign : utm_campaign = prepStr(request.QueryString("utm_campaign"))
	dim utm_email : utm_email = prepStr(request.QueryString("utm_email"))	
	dim utm_newsletter : utm_newsletter = prepStr(request.QueryString("utm_newsletter"))		
	dim showReviewPopup : showReviewPopup = false
	
	dim showPage : showPage = ""
	dim forceStaging : forceStaging = 1
	
	if prepStr(request.QueryString("utm_promocode")) <> "" then
		session("promocode") = prepStr(request.QueryString("utm_promocode"))
		response.Cookies("promocode") = prepStr(request.QueryString("utm_promocode"))
	end if
	if prepInt(Request.Cookies("admin_CO")("adminID")) > 0 then session("adminID") = prepInt(Request.Cookies("admin_CO")("adminID"))
	
	on error resume next
	if request.querystring("id") <> "" then
		Response.Cookies("vendororder") = request.querystring("id")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	elseif request.querystring("refer") <> "" then
		Response.Cookies("vendororder") = request.querystring("refer")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	elseif request.querystring("source") <> "" then
		Response.Cookies("vendororder") = request.querystring("source")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	elseif request.querystring("utm_source") <> "" then
		Response.Cookies("vendororder") = request.querystring("utm_source")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)		
	elseif request.querystring("clickid") <> "" then
		Response.Cookies("vendororder") = "clickid=" & request.querystring("clickid")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	end if
	on error goto 0
	
	serverName = "http://" & request.ServerVariables("SERVER_NAME")

	dim mvtID : mvtID = 0
	dim mvtCssList : mvtCssList = ""
	dim projID : projID = 0
	dim path : path = "/compressedPages/"
	
	if brand <> "" then
		if brand = "hp-palm" then brand = "hp/palm"
		if brand = "sony-ericsson" then brand = "Sony Ericsson"
		if brand = "other-brand" then brand = "other"
		sql = "select brandID from we_brands where brandName like '" & brand & "%'"
		session("errorSQL") = sql
		set brandRS = oConn.execute(sql)
		
		if brandRS.EOF then response.Redirect("/")
		brandID = brandRS("brandID")
		compPageName = "brand_" & brandID & ".htm"
		basePageName = "brandNew2.asp"
		mpPageName = "brand_mp.asp?brandID=" & brandID
	elseif itemID > 0 then
		sql =	"select a.itemDesc_CO, b.brandName, c.modelName, d.typeName_CO " &_
				"from we_Items a " &_
					"left join we_brands b on a.brandID = b.brandID " &_
					"left join we_models c on a.modelID = c.modelID " &_
					"left join we_types d on a.typeID = d.typeID " &_
				"where a.itemID = " & itemID
		session("errorSQL") = sql
		set nameRS = oConn.execute(sql)
		
		if not nameRS.EOF then
			itemDesc_CO = nameRS("itemDesc_CO")
			brandName = nameRS("brandName")
			modelName = nameRS("modelName")
			categoryName = nameRS("typeName_CO")
		end if
		if prepStr(brandName) = "" then brandName = "Universal"
		if prepStr(modelName) = "" then modelName = "Universal"
		
		compPageName = "product_" & itemID & ".htm"
		basePageName = "product.asp"
		mpPageName = "product_mp.asp?itemID=" & itemID & "&curURL=" & server.HTMLEncode(curURL)
		path = "/compressedPages/products/"
		if lcase(utm_campaign) = "co14dayreview" then showReviewPopup = true
	elseif pageName = "BM" then
		compPageName = "bm_" & brandID & "_" & modelID & ".htm"
		basePageName = "brand-model.asp"
		mpPageName = "brand-model_mp.asp?brandID=" & brandID & "&modelID=" & modelID & "&curURL=" & server.HTMLEncode(curURL)
		path = "/compressedPages/bm/"
	elseif pageName = "CB" then
		compPageName = "cb_" & categoryID & "_" & brandID & ".htm"
		basePageName = "category-brand.asp"
		mpPageName = "category-brand_mp.asp?categoryID=" & categoryID & "&brandID=" & brandID & "&curURL=" & server.HTMLEncode(curURL)
		path = "/compressedPages/cb/"
	elseif pageName = "BMC" then
		sql =	"select a.brandName, b.modelName, c.typeName_CO " &_
				"from we_brands a " &_
					"left join we_models b on b.modelID = " & modelID & " " &_
					"left join we_types c on c.typeID = " & categoryID & " " &_
				"where a.brandID = " & brandID
		session("errorSQL") = sql
		set nameRS = oConn.execute(sql)
		
		if not nameRS.EOF then
			brandName = nameRS("brandName")
			modelName = nameRS("modelName")
			categoryName = nameRS("typeName_CO")
		end if
		if prepStr(brandName) = "" then brandName = "Universal"
		if prepStr(modelName) = "" then modelName = "Universal"
		
		if musicSkinArtist > 0 then
			compPageName = "bmc_" & brandID & "_" & modelID & "_" & categoryID & "_msa_" & musicSkinArtist & ".htm"
		elseif musicSkinGenre <> "" then
			compPageName = "bmc_" & brandID & "_" & modelID & "_" & categoryID & "_msg_" & musicSkinGenre & ".htm"
		else
			compPageName = "bmc_" & brandID & "_" & modelID & "_" & categoryID & ".htm"
		end if
		basePageName = "brand-model-category.asp"
		mpPageName = "brand-model-category_mp.asp?brandID=" & brandID & "&modelID=" & modelID & "&categoryID=" & categoryID & "&musicSkins=" & musicSkins & "&musicSkinGenre=" & musicSkinGenre & "&musicSkinArtist=" & musicSkinArtist & "&curURL=" & server.HTMLEncode(curURL)
		path = "/compressedPages/bmc/"
	else
		if isLoggedIn then
			server.transfer(urlRelAccountHomePage)
		else
			setMVT 2,"index_mp.asp"
			compPageName = "index.htm"
			basePageName = "index.asp"
			mpPageName = "index_mp.asp?curURL=" & curURL
			path = "/compressedPages/"
		end if
	end if

	cms_basepage = mpPageName
	if inStr(cms_basepage,"?") > 0 then cms_basepage = left(cms_basepage,inStr(cms_basepage,"?")-1)
		
	compressPageLoc = path & compPageName
	if fso.fileExists(server.MapPath(compressPageLoc)) then
		set f1 = fso.getFile(server.MapPath("/" & basePageName))
		set f2 = fso.getFile(server.MapPath(compressPageLoc))
		if f1.DateLastModified > f2.DateLastModified then forceRefresh = 1
		if forceStaging = 1 then
			if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then forceRefresh = 1
		end if
		if prepInt(f2.Size) < 10000 then forceRefresh = 1
	else
		forceRefresh = 1
	end if

	if forceRefresh = 1 then
		useURL = serverName & "/" & mpPageName
		set XMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
		XMLHTTP.Open "GET", useURL, False
		session("errorSQL") = useURL
		XMLHTTP.Send
		
		allText = xmlHTTP.ResponseText
		
		if instr(lcase(allText),"<script") > 0 then
			lapCnt = 0
			do while len(allText) > 0
				lapCnt = lapCnt + 1
				if instr(lcase(allText),"<script") > 0 then
					nextScript = instr(allText,"<script")
					tempHolding = left(allText,nextScript-1)
					allText = mid(allText,nextScript-1)
					
					tempHolding = replace(tempHolding,vbCrLf,"")
					tempHolding = replace(tempHolding,"  ","")
					showPage = showPage & replace(tempHolding,vbTab,"")
					
					endScript = instr(lcase(allText),"</script>")
					showPage = showPage & vbCrLf & left(allText,endScript+8)
					allText = mid(allText,endScript+9)
				else
					tempHolding = replace(allText,vbCrLf,"")
					tempHolding = replace(tempHolding,"  ","")
					showPage = showPage & replace(tempHolding,vbTab,"")
					allText = ""
				end if	
				if lapCnt = 100 then
					response.Write(showPage)
					call CloseConn(oConn)
					response.End()
				end if
			loop
		else
			showPage = replace(allText,vbCrLf,"")
			showPage = replace(showPage,"  ","")
			showPage = replace(showPage,vbTab,"")
		end if
		
		showPage = cleanMP(showPage)
		
		session("errorSQL") = server.MapPath(compressPageLoc)
		
		set tfile = fso.createTextFile(server.MapPath(compressPageLoc),true,true)
		tfile.writeLine(showPage)
		tfile.close	
		tfile = null
		newFile = 1
	else
		newFile = 0
	end if
	
	if showPage = "" then
		useURL = serverName & compressPageLoc
		set curHTML = fso.OpenTextFile(server.MapPath(compressPageLoc),1,false,-1)
		response.Write(curHTML.readall)
	else
		if instr(lcase(showPage),"problem with the page you are trying to reach") > 0 or instr(showPage,"ASPDescription") > 0 then
			fso.deleteFile(server.MapPath(compressPageLoc))
			errorPage = 1
		else
			response.Write(showPage)
		end if
	end if

	siteId = 2
	if instr(mpPageName, "index_mp.asp") > 0 then pageName = "/"
	if basePageName = "product.asp" then pageName = "product.asp"
'	cmsData(prepStr(pageName))
'	metaArray = split(session("metaTags"),"##")
'	SEtitle = metaArray(0)
'	SEdescription = metaArray(1)
'	SEkeywords = metaArray(2)
'	SeTopText = metaArray(3)
'	SeBottomText = metaArray(4)
	
	fso = null
	f1 = null
	f2 = null
	
	'fix "/masterPage.asp" showing on url for Chrome and FF when access by "cellularoutfitter.com"
	if mpPageName = "index_mp.asp" and curURL = "/masterPage.asp" then
		call CloseConn(oConn)
		call PermanentRedirect("/")
	end if
	call CloseConn(oConn)
%>
<% if newFile = 1 then response.Write("<!-- new file created -->") else response.Write("<!-- old file still fresh -->") %>
<% response.Write("<!-- ErrorCode: " & session("errorCode") & " -->") %>
<%="<!-- sid:" & session.SessionID & " -->"%>
<%="<!-- csid:" & prepInt(request.cookies("mySession")) & " -->"%>
<%="<!-- mvtID:" & mvtID & " -->"%>
<%="<!-- mpPageName:" & mpPageName & " -->"%>
<%="<!-- HTTP_X_REWRITE_URL:" & curURL & " -->"%>
<%="<!-- pageName:" & pageName & " -->"%>
<script language="javascript" src="/frameWork/Utility/base.js"></script>
<script language="javascript">
	var bShowPromoPopup = true;
	<% if utm_newsletter <> "" then %>
		bShowPromoPopup = false;
	<% end if%>
	
	<% if errorPage = 1 then %>window.location = window.location<% end if %>
	function dynamicLoad() {
		<%
		if prepInt(session("adminID")) > 0 then
			curPage = prepStr(request.ServerVariables("HTTP_X_REWRITE_URL"))
		%>
		ajax('/ajax/admin/adminMenu.asp?basePageName=<%=basePageName%>&brandID=<%=brandid%>&modelID=<%=modelid%>&categoryID=<%=categoryid%>&itemID=<%=itemid%>&curPage=<%=server.URLEncode(curPage)%>&cms_basepage=<%=cms_basepage%>','topSelect');
		<% else %>
		document.getElementById('topSelect').style.display = 'none';
		<% end if %>
		
		ajax('/ajax/dynamicUpdate.asp?myCartDetails=1','siteTop-CartBar');
		<% if basePageName = "index.asp" then %>
			ajax('/ajax/dynamicUpdate.asp?featuredProducts=1','featuredProducts');
			//ajax('/ajax/dynamicUpdate.asp?featuredProducts=2','featuredProducts2');
			<% if utm_newsletter <> "" then %>
				addNewsletter('<%=utm_newsletter%>','email');
			<%end if%>
		<% end if %>
		
		<% if basePageName = "product.asp" then %>
			<%if showReviewPopup then%>
				showPopup('writeReview');
				document.getElementById('id_emailorderid').value = '<%=emailOrderID%>';
			<%end if%>		
			ajax('/ajax/productDetails.asp?itemID=<%=itemID%>&modelID=' + modelID + '&typeID=' + typeID,'rightSideItems');
//			ajax('/ajax/productDetails.asp?itemID=<%=itemID%>&myPartNumber=' + myPartNumber + '&prodReviews=1','prodReview')
//			ajax('/ajax/productDetails.asp?itemID=<%=itemID%>&myPartNumber=' + myPartNumber + '&prodReviews=1','prodReview2')
			ajax('/ajax/productDetails.asp?itemID=<%=itemID%>&modelID=' + modelID + '&typeID=' + typeID + '&latDesign=1','relatedItems_new');
		<% end if %>
	}
	setTimeout("fetchCompare()",2000);
	
	function fetchCompare() {
		ajax('/ajax/masterCompare.asp?mpPageName=<%=mpPageName%>&compPageName=<%=path & compPageName%>','compareResultDiv');
	}

	if (bShowPromoPopup) {
		jQuery(window).load(showPromoDetails);
	}
	
</script>
<!-- sip:<%=request.ServerVariables("LOCAL_ADDR")%> -->