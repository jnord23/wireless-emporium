<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
	call fOpenConn()
	
	brandID = request.querystring("brandID")
	modelID = request.querystring("modelID")
	categoryID = request.querystring("categoryID")

	if isnull(brandID) or len(brandID) < 1 then brandID = 0 end if 
	if isnull(modelID) or len(modelID) < 1 then modelID = 0 end if
	if isnull(categoryID) or len(categoryID) < 1 then categoryID = 0 end if

	sql = "select (select typename_co from we_types where typeid = '" & categoryID & "') typename, a.brandName , b.modelName, b.modelImg from we_models b join we_brands a on b.brandID = a.brandID where b.modelID = '" & modelID & "'"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1

	if rs.eof then
		call PermanentRedirect("/")		
		response.End()
	end if
	
	modelName = rs("modelName")
	modelImg = rs("modelImg")
	brandName = rs("brandName")
	categoryName = rs("typeName")
	
	SEtitle = brandName & " " & modelName & " " & categoryName & " at Wholesale Prices"
	SEdescription = "CellularOutfitter.com is the largest store online for discounted " & brandName & " " & modelName & " " & categoryName & " & other " & brandName & " " & modelName & " accessories at wholesale prices."
	SEkeywords = brandName & " " & modelName & " Accessories, " & brandName & " " & modelName & " Cell Phone Accessories, " & brandName & " " & modelName & " " & categoryName & ", " & brandName & " " & modelName & " Cell Phone " & categoryName
	h1 = brandName & " " & modelName & " " & categoryName
	topText = "So you're looking to upgrade the look and performance of your " & brandName & " phone? Look no further than our incredible selection of " & categoryName & " for the " & brandName & " " & modelName & "."
	topText = topText & "Cellular Outfitter offers top quality products and wholesale prices so you have the freedom to create a phone that is a true reflection of you."
	
	dim noDecalSkin : noDecalSkin = false
	dim noMusicSkins : noMusicSkins = false
	
	sql = 	"select	distinct a.genre, a.brand, b.modelName, b.modelImg " & vbcrlf & _
			"from	we_items_musicSkins a join we_models b " & vbcrlf & _
			"	on	a.modelID = b.modelID " & vbcrlf & _
			"where	a.skip = 0 and a.deleteItem = 0 " & vbcrlf & _
			"	and b.modelID = '" & modelID & "'" & vbcrlf & _
			"	and a.genre <> 'Screen Protectors' " & vbcrlf & _
			"order by a.genre"
			
	session("errorSQL") = sql
	curGenres = ""
	genreArray = null
	set musicRS = Server.CreateObject("ADODB.Recordset")
	musicRS.open sql, oConn, 0, 1
	if musicRS.eof then 
		noMusicSkins = true	
	else
		curGenres = ""
		do while not musicRS.EOF
			if instr(musicRS("genre"),",") > 0 then
				genreArray = split(musicRS("genre"),",")
				for i = 0 to ubound(genreArray)
					if curGenres = "" then
						curGenres = genreArray(i)
					else
						if instr(trim(curGenres),trim(genreArray(i))) < 1 then curGenres = curGenres & "," & genreArray(i)
					end if
				next
			else
				if curGenres = "" then
					curGenres = musicRS("genre")
				else
					if instr(trim(curGenres),trim(musicRS("genre"))) < 1 then curGenres = curGenres & "," & musicRS("genre")
				end if
			end if
			musicRS.movenext
		loop
		genreArray = split(curGenres,",")
		session("curGenres") = curGenres
	end if
	
	sql = 	"select	a.itemid, a.brandid, a.modelid, a.typeid, a.subtypeid, a.itemdesc_co, a.itempic_co, a.partnumber, a.itempic, a.flag1, a.price_retail, a.price_co" & vbcrlf & _
			"	,	case when (select top 1 inv_qty from we_items where partnumber = a.partnumber and master = 1 order by inv_qty desc) > 0 then 1 else 0 end as inv_qty" & vbcrlf & _
			"	,	itemkit_new, hotdeal, hidelive, nodiscount" & vbcrlf & _
			"	,	(	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = a.partnumber)) as reviewcnt" & vbcrlf & _
			"from	we_items a" & vbcrlf & _
			"where	a.sports = 0 and a.subtypeid in (1260) and a.modelid = '" & modelid & "' and price_co > 0 and hidelive = 0 and ghost = 0 " & vbcrlf & _
			"order by inv_qty desc, flag1 desc"
'	response.write "<pre>" & sql & "</pre>"
	session("errorSQL") = sql
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	if rs.eof then noDecalSkin = true
	
	if (noMusicSkins and noDecalSkin) then
		call PermanentRedirect("/")
	end if
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
        <td class="top-sublink-gray" width="100%">
            <a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
            <a class="top-sublink-gray" href="/b-<%=brandID & "-" & formatSEO(brandName)%>-cell-phone-accessories.html"><%=brandName%> Cell Phone Accessories</a>&nbsp;>&nbsp;
            <a class="top-sublink-gray" href="/m-<%=modelID & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>-cell-phone-accessories.html"><%=brandName & " " & modelName%> Cell Phone Accessories</a>&nbsp;>&nbsp;
            <span class="top-sublink-blue"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></span>
        </td>
    </tr>
    <tr>
        <td width="100%" valign="top" style="padding-top:10px;">    
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                	<td valign="top" align="left"><img src="/images/category-select/topbox-left.png" border="0" width="15" height="139" />
                    </td>
                	<td valign="top" align="left" style="padding-top:10px; width:790px; height:139px; background: url(/images/category-select/topbox-middle.png) repeat-x;">&nbsp;
                    	<div style="float:left; width:63px; height:129px; padding:10px 0px 0px 10px;"><img src="/modelpics/<%=modelImg%>" border="0" width="63" height="100" alt="<%=h1%>" /></div>
                        <div style="float:left; width:670px; height:130px; padding:5px 0px 0px 20px;">
                            <h1 style="font-size:24px; color:#000;"><%=h1%></h1>
                            <p class="top-text-grey" style="padding:0px; margin:10px 0px 0px 0px;"><%=topText%></p>                        
                        </div>
                    </td>
                	<td valign="top" align="left"><img src="/images/category-select/topbox-right.png" border="0" width="15" height="139" alt="Decal Skin" title="Decal Skin" /></td>
                </tr>
			</table>
		</td>
    </tr>
    <%
	if not noDecalSkin then
	%>
    <tr>
        <td width="100%" valign="top" style="padding-top:10px; border-bottom:1px solid #ccc;" align="left">
        	<img src="/images/category-select/banner-decal.png" border="0" alt="Music Skins" title="Music Skins" />
        </td>
	</tr>
    <tr>
        <td width="100%" valign="top" align="left" style="font-size:11px; color:#515050; padding:3px 0px 20px 0px;">Instantly makeover your cell phone while providing extra protection against scratches with our flexible, colorful & sleek decal skins.</td>
	</tr>
    <tr>
        <td width="100%" valign="top" align="left">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
		<%
		picLap = 0
		a = 0
		set fso = CreateObject("Scripting.FileSystemObject")
		do until rs.eof
			doNotDisplay = false
			itemid = rs("itemid")
			itemDesc_CO = rs("itemdesc_co")

			useImg = "/productpics/decalSkins/thumb/" & rs("itempic")
			if not hasImageOnRemote("http://www.wirelessemporium.com/productpics/thumb", rs("itempic")) then useImg = "/productpics/thumb/imagena.jpg"
			
			altText = brandName & " " & modelName & " " & categoryName & ": " & itemDesc_CO
			useItemDesc = itemDesc_CO
			price_retail = rs("price_retail")
			price_CO = rs("price_co")
			
			if picLap >= 8 then doNotDisplay = true
			
			if not doNotDisplay then
				picLap = picLap + 1
				a = a + 1
		%>
					<td width="195" height="100%" align="center" valign="top" style="padding:5px;">
                        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" align="center" valign="middle" title="<%=altText%>" style="padding-top:10px;">
									<a href="/p-<%=itemid & "-" & formatSEO(useItemDesc)%>.html"><img src="<%=useImg%>" border="0"></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="padding-top:8px;">
                                    <a class="static-content-font-link" style="text-decoration:underline;" href="/p-<%=itemid & "-" & formatSEO(useItemDesc)%>.html" title="<%=altText%>"><%=useItemDesc%></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="bottom" style="padding-top:10px; font-size:20px; font-weight:bold; color:#000;"><%=formatCurrency(price_CO)%></td>
                            </tr>
                            <tr>
                                <td align="center" valign="bottom" class="cat-font" style="font-size:11px; color:#2276BF;">
                                	(<%=(formatPercent((price_retail - price_CO) / price_retail,0))%> OFF retail Price: <s><%=formatCurrency(price_retail)%></s>)
								</td>
                            </tr>
                            <tr>
                                <td align="center" valign="bottom" style="padding-top:5px;">
                                    <a href="/p-<%=itemid & "-" & formatSEO(useItemDesc)%>.html"><img src="/images/category-select/button-details.png" width="152" height="24" border="0" alt="More Info"></a>
                                </td>
                            </tr>
                        </table>
                    </td>        
        <%
			end if
			if a >= 4 then
				a = 0
				response.write "</tr><tr>"
			end if
			rs.movenext
		loop
		if a = 1 then
			response.write "<td width=""205"">&nbsp;</td><td width=""205"">&nbsp;</td><td width=""205"">&nbsp;</td>" & vbcrlf
		elseif a = 2 then
			response.write "<td width=""205"">&nbsp;</td><td width=""205"">&nbsp;</td>" & vbcrlf
		elseif a = 3 then
			response.write "<td width=""205"">&nbsp;</td>" & vbcrlf
		end if
		%>
                </tr>
			</table>
        </td>
	</tr>
    <tr><td align="center" valign="top" width="100%" height="30">&nbsp;</td></tr>
    <%
	end if
	
	if picLap >= 8 then
	%>
    <tr>
    	<td align="center" valign="top" width="100%">
        	<a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-1260-<%=formatSEO(categoryName)%>-for-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.html" title=""><img src="/images/category-select/button-view-decal.png" border="0" width="329" height="55" /></a>
        </td>
    </tr>
    <tr><td align="center" valign="top" width="100%" height="60">&nbsp;</td></tr>    
    <%
	end if
	
	if not noMusicSkins then
    %>
    <tr>
        <td width="100%" valign="top" style="border-bottom:1px solid #ccc;" align="left">
        	<img src="/images/category-select/banner-music.png" border="0" />
        </td>
	</tr>
    <tr>
        <td width="100%" valign="top" align="left" style="font-size:11px; color:#515050; padding:3px 0px 20px 0px;">Music Skins are premium, removable & reusable protective covers for your favorite electronic devices. Made from patented Avery material, they apply easily and leave no residue when peeled away.</td>
	</tr>
    <tr>
        <td width="100%" valign="top" align="center" style="padding-bottom:30px;">
	        <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td colspan="5" align="left" style="padding-bottom:10px;">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-weight:bold; font-size:16px; padding-right:20px;" nowrap="nowrap">Select Music Skins Genre:</td>
                                <td>
                                    <select name="musicGenre" onchange="bmc(this.value)">
                                        <option value="">Select Genre</option>
                                        <% for i = 0 to ubound(genreArray) %>
                                        <option value="<%=formatSEO(trim(genreArray(i)))%>"><%=trim(genreArray(i))%></option>
                                        <% next %>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%
                totalGenre = 0
                do while totalGenre <= ubound(genreArray)
                %>
                <tr>
                    <%
                    for i = 1 to 3
                        if totalGenre > ubound(genreArray) then
                    %>
                    <td style="padding-bottom:10px;">&nbsp;</td>
                    <%
                        else
                            imgName = replace(genreArray(totalGenre),"&","")
                            imgName = replace(imgName,"/Movies","")
                    %>
                    <td align="center" style="padding-bottom:10px;">
                        <a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-1270-music-skins-<%=formatSEO(trim(genreArray(totalGenre)))%>.html"><img src="/images/buttons/ms__<%=trim(imgName)%>.jpg" border="0" width="260" height="100" /></a><br />
                        <a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-1270-music-skins-<%=formatSEO(trim(genreArray(totalGenre)))%>.html" style="font-weight:bold; font-size:16px;"><%=genreArray(totalGenre)%></a>
                    </td>
                    <%
                        end if
                        totalGenre = totalGenre + 1							
                    next
                    %>
                </tr>
                <%
                loop
                %>
            </table>
        </td>
	</tr>
    <%
	end if
	%>
</table>
<%
function hasImageOnRemote(pRemoteURL, pFileName)
	dim isExists : isExists = false
	dim oXMLHTTP : set oXMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP")
	dim strTemp : strTemp = pRemoteURL & "/" & pFileName
	
	oXMLHTTP.Open "HEAD", strTemp, False	'communicating without responseBody, only looks up file header
	oXMLHTTP.Send
	
	if (oXMLHTTP.readyState = "4") and (oXMLHTTP.status = "200") and (instr(oXMLHTTP.getResponseHeader("Content-Type"), "image") > 0) then
		isExists = true
	end if

	set oXMLHTTP = Nothing
	
	hasImageOnRemote = isExists
	
end function
%>
<!--#include virtual="/includes/template/bottom.asp"-->
<script language="javascript">
	function bmc(val) 
	{
		var useVal = val.replace(" ","-");
		
		if ("" != useVal) window.location = '/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=categoryID%>-music-skins-' + useVal + '.html';
	}
</script>