<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->

<html>
<head>
<title>E-mail to a friend</title>
<link href="http://www.cellularoutfitter.com/includes/css/styleCO.css" rel="stylesheet" type="text/css">
</head>

<body>

<script language="JavaScript">
function checkbae(){
	var testresults;
	var goSubmit;
	goSubmit = true;
	var str=document.validEmail.senderEmail.value;
	var str1=document.validEmail.recipientEmail.value;
	var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
	if (filter.test(str) && filter.test(str1)) {
		goSubmit=true;
	} else {
		goSubmit=false;
	}	
	if (goSubmit) {
		document.validEmail.submit();
	} else {
		alert("Please make sure to input valid email addresses in both fields");
	}
}
</script>

<%
dim nItemID
nItemID = request("itemID")
if not isNumeric(nItemID) or trim(nItemID) = "" then
	response.redirect("/")
	response.end
end if

if (cstr(clng(nItemID)) <> cstr(nItemID)) then 
	response.redirect("/")
	response.end
end if
%>

<table width="100%" border="0" cellspacing="0" cellpadding="20" class="mc-text">
	<tr>
		<td width="100%">
			<p><img src="http://www.cellularoutfitter.com/images/CellularOutfitter.jpg" width="320" height="90" border="0" alt="CellularOutfitter.com - Wholesale Prices to the Public!"></p>
			<%
			dim strError
			strError = ""
			
			dim RS, SQL, itemID, sitemDesc_CO, sItemPic, price_Retail, price_CO
			if nItemID = "9999.99" then
				sitemDesc_CO = "site"
			else
				call fOpenConn()
				SQL = "SELECT itemID, itemDesc_CO, itemPic_CO, price_Retail, price_CO FROM we_Items WHERE itemID = '" & nItemID & "'"
				set RS = oConn.execute(SQL)
				if not RS.eof then
					itemID = RS("itemID")
					sitemDesc_CO = RS("itemDesc_CO")
					sItemPic = RS("itemPic_CO")
					price_Retail = RS("price_Retail")
					price_CO = RS("price_CO")
				end if
				RS.close
				set RS = nothing
				call fCloseConn()
			end if
			
			dim sRecipientName, sRecipientEmail, sSenderName, sSenderEmail, sSenderMsg, strSubject
			sRecipientName = trim(request.form("recipientName"))
			sRecipientEmail = trim(request.form("recipientEmail"))
			sSenderName = trim(request.form("senderName"))
			sSenderEmail = trim(request.form("senderEmail"))
			sSenderMsg = trim(request.form("senderMsg"))
			
			if request.form("submitted") = "submitted" then
				if sitemDesc_CO = "" then
					strError = "Sorry, the item you requested to email was not found in our records. Please press the back button and try again."
				elseif len(nItemID) = 0 or len(sSenderEmail) = 0 or len(sRecipientEmail) = 0 then
					strError = "<font color=""#FF0000"">Sorry, there was something wrong with the information that was supplied. Please make sure you filled in the appropriate email addresses and try again.</font><br><br>"
				end if
				if strError = "" then
					BodyText = "<html><body>" & vbcrlf
					BodyText = BodyText & "<table width=""764"" cellpadding=""0"" cellspacing=""0"" border=""0"">" & vbcrlf
					BodyText = BodyText & "<tr>" & vbcrlf
					BodyText = BodyText & "<td align=""right"" valign=""top"" colspan=""2"" rowspan=""2""><img src=""http://www.cellularoutfitter.com/images/emailafriend/border_corner_top_left.jpg"" width=""9"" height=""9"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "<td align=""center"" valign=""top"" bgcolor=""#FE3905""><img src=""http://www.cellularoutfitter.com/images/spacer.gif"" width=""746"" height=""4"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "<td align=""left"" valign=""top"" colspan=""2"" rowspan=""2""><img src=""http://www.cellularoutfitter.com/images/emailafriend/border_corner_top_right.jpg"" width=""9"" height=""9"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "</tr>" & vbcrlf
					BodyText = BodyText & "<tr>" & vbcrlf
					BodyText = BodyText & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF""><img src=""http://www.cellularoutfitter.com/images/spacer.gif"" width=""764"" height=""5"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "</tr>" & vbcrlf
					BodyText = BodyText & "<tr>" & vbcrlf
					BodyText = BodyText & "<td bgcolor=""#FE3905"" width=""4""><img src=""http://www.cellularoutfitter.com/images/spacer.gif"" width=""4"" height=""400"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "<td bgcolor=""#FFFFFF"" width=""5""><img src=""http://www.cellularoutfitter.com/images/spacer.gif"" width=""5"" height=""400"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "<td align=""left"" valign=""top"">" & vbcrlf
					BodyText = BodyText & "<table width=""746"" cellpadding=""5"" cellspacing=""0"" border=""0"">" & vbcrlf
					BodyText = BodyText & "<tr>" & vbcrlf
					BodyText = BodyText & "<td align=""left"" valign=""top""><img src=""http://www.cellularoutfitter.com/images/emailafriend/email_from_your_friend.jpg"" width=""426"" height=""102"" border=""0"" alt=""Email from your Friend""></td>" & vbcrlf
					BodyText = BodyText & "<td align=""right"" valign=""top""><img src=""http://www.cellularoutfitter.com/images/CellularOutfitter.jpg"" width=""320"" height=""90"" border=""0"" alt=""CellularOutfitter.com - Wholesale Prices to the Public!""></td>" & vbcrlf
					BodyText = BodyText & "</tr>" & vbcrlf
					if sitemDesc_CO = "site" then
						BodyText = BodyText & "<tr>" & vbcrlf
						BodyText = BodyText & "<td colspan=""2"">" & vbcrlf
						BodyText = BodyText & "<p>&nbsp;</p>" & vbcrlf
						BodyText = BodyText & "<p><font size=""3"" face=""Arial,Helvetica,sans-serif"" color=""#000000"">Hi " & sRecipientName & ",</font></p>" & vbcrlf
						BodyText = BodyText & "<p><font size=""3"" face=""Arial,Helvetica,sans-serif"" color=""#000000"">I found a great cellphone accessories site!</font></p>" & vbcrlf
						BodyText = BodyText & "<p><font size=""3"" face=""Arial,Helvetica,sans-serif"" color=""#000000""><a href=""http://www.cellularoutfitter.com/"">CellularOutfitter.com</a></font></p>" & vbcrlf
						BodyText = BodyText & "<p>&nbsp;</p>" & vbcrlf
						BodyText = BodyText & "</td>" & vbcrlf
						BodyText = BodyText & "</tr>" & vbcrlf
					else
						BodyText = BodyText & "<tr>" & vbcrlf
						BodyText = BodyText & "<td colspan=""2"">" & vbcrlf
						BodyText = BodyText & "<p><font size=""3"" face=""Arial,Helvetica,sans-serif"" color=""#000000"">Hi " & sRecipientName & ",</font></p>" & vbcrlf
						BodyText = BodyText & "<p><font size=""3"" face=""Arial,Helvetica,sans-serif"" color=""#000000"">Here is the product I found at CellularOutfitter.com:</font></p>" & vbcrlf
						BodyText = BodyText & "<p>&nbsp;</p>" & vbcrlf
						BodyText = BodyText & "</td>" & vbcrlf
						BodyText = BodyText & "</tr>" & vbcrlf
						BodyText = BodyText & "<tr>" & vbcrlf
						BodyText = BodyText & "<td align=""left"" valign=""top"" colspan=""2"">" & vbcrlf
						BodyText = BodyText & "<table width=""600"" cellpadding=""10"" cellspacing=""0"" border=""0"">" & vbcrlf
						BodyText = BodyText & "<tr>" & vbcrlf
						BodyText = BodyText & "<td align=""left"" valign=""top"" width=""100"">" & vbcrlf
						BodyText = BodyText & "<a href=""http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(sitemDesc_CO) & ".html""><img src=""http://www.cellularoutfitter.com/productpics/thumb/" & sItemPic & """ width=""100"" height=""100"" border=""0""></a>" & vbcrlf
						BodyText = BodyText & "</td>" & vbcrlf
						BodyText = BodyText & "<td align=""left"" valign=""top"" width=""500"">" & vbcrlf
						BodyText = BodyText & "<p><a href=""http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(sitemDesc_CO) & ".html""><font size=""4"" face=""Arial,Helvetica,sans-serif"" color=""#3184B8"">" & sitemDesc_CO & "</font></a></p>" & vbcrlf
						BodyText = BodyText & "<table width=""500"" cellpadding=""10"" cellspacing=""0"" border=""0"">" & vbcrlf
						BodyText = BodyText & "<tr>" & vbcrlf
						BodyText = BodyText & "<td align=""left"" valign=""top"" width=""300"">" & vbcrlf
						BodyText = BodyText & "<p>" & vbcrlf
						BodyText = BodyText & "<font size=""2"" face=""Arial,Helvetica,sans-serif"" color=""#999999""><b><s>List Price: " & formatCurrency(price_Retail) & "</s></b></font><br>" & vbcrlf
						BodyText = BodyText & "<font size=""3"" face=""Arial,Helvetica,sans-serif"" color=""#000000""><b>Our Price: <font color=""#FF0000"">" & formatCurrency(price_CO) & "</font></b></font><br>" & vbcrlf
						BodyText = BodyText & "<font size=""2"" face=""Arial,Helvetica,sans-serif"" color=""#FF6600""><b><em>You Save: " & FormatCurrency(price_Retail-price_CO,2) & " (" & FormatPercent(1-(price_CO/price_Retail),0) & ")</em></b></font>" & vbcrlf
						BodyText = BodyText & "</p>" & vbcrlf
						BodyText = BodyText & "</td>" & vbcrlf
						BodyText = BodyText & "<td align=""center"" valign=""bottom"" width=""200"">" & vbcrlf
						BodyText = BodyText & "<a href=""http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(sitemDesc_CO) & ".html""><img src=""http://www.cellularoutfitter.com/images/emailafriend/Buy_Now_button.jpg"" width=""176"" height=""30"" border=""0"" alt=""Buy Now!""></a>" & vbcrlf
						BodyText = BodyText & "</td>" & vbcrlf
						BodyText = BodyText & "</tr>" & vbcrlf
						BodyText = BodyText & "</table>" & vbcrlf
						BodyText = BodyText & "<p><a href=""http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(sitemDesc_CO) & ".html""><font size=""2"" face=""Arial,Helvetica,sans-serif"" color=""#3184B8"">Click here to see this item at CellularOutfitter.com</font></a></p>" & vbcrlf
						BodyText = BodyText & "</td>" & vbcrlf
						BodyText = BodyText & "</tr>" & vbcrlf
						BodyText = BodyText & "</table>" & vbcrlf
						BodyText = BodyText & "</td>" & vbcrlf
						BodyText = BodyText & "</tr>" & vbcrlf
					end if
					BodyText = BodyText & "</table>" & vbcrlf
					BodyText = BodyText & "</td>" & vbcrlf
					BodyText = BodyText & "<td bgcolor=""#FFFFFF"" width=""5""><img src=""http://www.cellularoutfitter.com/images/spacer.gif"" width=""5"" height=""400"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "<td bgcolor=""#FE3905"" width=""4""><img src=""http://www.cellularoutfitter.com/images/spacer.gif"" width=""4"" height=""400"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "</tr>" & vbcrlf
					BodyText = BodyText & "<tr>" & vbcrlf
					BodyText = BodyText & "<td align=""right"" valign=""bottom"" colspan=""2"" rowspan=""2""><img src=""http://www.cellularoutfitter.com/images/emailafriend/border_corner_bottom_left.jpg"" width=""9"" height=""9"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF""><img src=""http://www.cellularoutfitter.com/images/spacer.gif"" width=""746"" height=""5"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "<td align=""left"" valign=""bottom"" colspan=""2"" rowspan=""2""><img src=""http://www.cellularoutfitter.com/images/emailafriend/border_corner_bottom_right.jpg"" width=""9"" height=""9"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "</tr>" & vbcrlf
					BodyText = BodyText & "<tr>" & vbcrlf
					BodyText = BodyText & "<td align=""center"" valign=""top"" bgcolor=""#FE3905""><img src=""http://www.cellularoutfitter.com/images/spacer.gif"" width=""746"" height=""4"" border=""0""></td>" & vbcrlf
					BodyText = BodyText & "</tr>" & vbcrlf
					BodyText = BodyText & "<tr>" & vbcrlf
					BodyText = BodyText & "<td colspan=""5"">" & vbcrlf
					BodyText = BodyText & "<p>&nbsp;</p>" & vbcrlf
					BodyText = BodyText & "<p><font size=""3"" face=""Arial,Helvetica,sans-serif"" color=""#000000"">" & sSenderMsg & "</p>" & vbcrlf
					BodyText = BodyText & "<p><font size=""3"" face=""Arial,Helvetica,sans-serif"" color=""#000000""><a href=""mailto:" & sSenderEmail & """>" & sSenderName & "</a> asked us to send you this email. If you have any questions about shopping at CellularOutfitter.com, please visit our homepage.</font></p>" & vbcrlf
					BodyText = BodyText & "<p><font size=""3"" face=""Arial,Helvetica,sans-serif"" color=""#000000"">* We do not store or share your email address.</font></p>" & vbcrlf
					BodyText = BodyText & "</td>" & vbcrlf
					BodyText = BodyText & "</tr>" & vbcrlf
					BodyText = BodyText & "</table>" & vbcrlf
					BodyText = BodyText & "</body></html>" & vbcrlf
					strSubject = "An e-mail for " & sRecipientName & " from " & sSenderName & "!"
					CDOSend sRecipientEmail,"service@cellularoutfitter.com",strSubject,BodyText
					response.write "<p>&nbsp;</p><p>Information Successfully Sent!</p><p>&nbsp;</p>"
					response.write "<a href=""javascript:window.close();"">Close Window</a><p>&nbsp;</p>"
				end if
			end if
			if request.form("submitted") <> "submitted" or strError <> "" then
				if strError = "" then
					%>
					<p>
						Letting someone know about this <b><%=sitemDesc_CO%></b> is easy!
						<br><br>
						Just fill in the blanks, press &quot;Email a Friend&quot;, 
						and we'll send them an Email for you! CellularOutfitter.com 
						will not use the Email address for any other purpose.
					</p>
					<%
				else
					response.write strError
				end if
				%>
				<br><br>
				<form name="validEmail" method="post" action="http://www.cellularoutfitter.com/tell-a-friend.asp">
					<input type="hidden" name="itemID" value="<%=nItemID%>">
					<table width="400" border="0" cellspacing="1" cellpadding="10" class="mc-text" align="center">
						<tr bgcolor="#EAEAEA">
							<td align="right" nowrap><strong>Name of Recipient:</strong></td>
							<td><input name="recipientName" type="text" value="<%=sRecipientName%>"></td>
						</tr>
						<tr bgcolor="#EAEAEA">
							<td align="right" nowrap><strong>Email Address of Recipient:</strong></td>
							<td><input name="recipientEmail" type="text" value="<%=sRecipientEmail%>"></td>
						</tr>
						<tr bgcolor="#EAEAEA">
							<td align="right" nowrap><strong>Your Name:</strong></td>
							<td><input name="senderName" type="text" value="<%=sSenderName%>"></td>
						</tr>
						<tr bgcolor="#EAEAEA">
							<td align="right" nowrap><strong>Your Email Address:</strong></td>
							<td><input name="senderEmail" type="text" value="<%=sSenderEmail%>"></td>
						</tr>
						<tr align="center" bgcolor="#EAEAEA">
							<td align="right" nowrap><strong>Your Personal Message:</strong></td>
							<td>&nbsp;</td>
						</tr>
						<tr align="center" bgcolor="#EAEAEA">
							<td colspan="2"><textarea name="senderMsg" cols="40" rows="5" id="senderMsg"><%=sSenderMsg%></textarea></td>
						</tr>
						<tr align="center" bgcolor="#EAEAEA">
							<td colspan="2">
								<input name="submitted" type="hidden" value="submitted">
								<input src="http://www.cellularoutfitter.com/images/emailafriend/email_button.jpg" width="147" height="30" border="0" alt="Email a Friend" type="image" onClick="checkbae(); return false;">
							</td>
						</tr>
					</table>
				</form>
				<%
			end if
			%>
			<p align="center"><a href="http://www.cellularoutfitter.com/shipping-policy.html"><font size="2">Click Here to see our Privacy Policy</font></a>&nbsp;&nbsp;</p>
			<p>&nbsp;</p>
		</td>
	</tr>
</table>

</body>
</html>
