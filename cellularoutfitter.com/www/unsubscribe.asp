<%
pageTitle = "Unsubscribe from Cellular Outfitter E-Mails"
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" cellspacing="0" cellpadding="0" width="746">
    <tr>
        <td class="top-sublink-gray">
            <a class="top-sublink-gray" href="http://www.CellularOutfitter.com/">Cell Phone Accessories</a>&nbsp;>&nbsp;
            <span class="top-sublink-blue"><%=pageTitle%></span>
        </td>
    </tr>
    <tr>
        <td class="top-sublink-gray"><img src="http://www.cellularoutfitter.com/images/spacer.gif" width="1" height="10"></td>
    </tr>
    <tr>
        <td align="left" valign="top" class="contain">
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <table border="0" cellspacing="0" cellpadding="0" width="746">
                <%
                if request.form("submitted") = "Unsubscribe Now" then
                    call fOpenConn()
                    email = prepStr(request.form("email"))
					
					if email <> "" and instr(email," ") < 1 then
	                    SQL = "DELETE FROM CO_emailopt WHERE email='" & email & "'"
    	                'response.write "<p>" & SQL & "</p>" & vbcrlf
        	            oConn.execute SQL
            	        SQL = "if (select count(*) from co_emailopt_out where email = '" & email & "') < 1 INSERT INTO CO_emailopt_out (email,DateTimeEntd) VALUES ('" & email & "','" & now & "')"
                	    'response.write "<p>" & SQL & "</p>" & vbcrlf
                    	oConn.execute SQL
					end if
                    call fCloseConn()
                    %>
                    <tr>
                        <td colspan="2" align="center"><h1><br><br><%=email%><br>Has Been Unsubscribed from Cellular Outfitter E-Mails</h1></td>
                    </tr>
                    <%
                else
                    %>
                    <tr>
                        <td colspan="2" align="center">
                            <h3>Unsubscribe from Cellular Outfitter E-Mails</h3>
                            <form action="unsubscribe.html" method="post">
                                <p>If the e-mail address shown below is not the one you wish to unsubscribe, please enter it:</p>
                                <p><input type="text" name="email" size="50" value="<%=request.querystring("strEmail")%>"></p>
                                <p><input type="submit" name="submitted" value="Unsubscribe Now"></p>
                            </form>
                        </td>
                    </tr>
                    <%
                end if
                %>
            </table>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->
