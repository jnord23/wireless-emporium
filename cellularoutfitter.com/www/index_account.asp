<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/account/account_base.asp"-->
<%	
	
	'Authentication Required
	accountLoggedInRequired
	
	'Retrieve user info
	email = getAccountEmail
	if isnull(email) then
		'response.end
	end if
	'response.write email

	makePrimaryModel = prepStr(request.form("makePrimaryModel"))
	if makePrimaryModel <> "" and not isnull(makePrimaryModel) then
		
		'response.end
		sql = "update CO_Accounts set currentModelId = " & makePrimaryModel & " " &_
		"where email = '" & email & "' and isMaster = 1"
		session("errorSQL") = sql
		oConn.execute sql
		
	end if
		
	'Retrieve activated account record
	'Look for a master account first
	sql = "select top 1 * from CO_accounts where email = '" & email & "' and isMaster = 1" &_
	" order by dateEntered desc" 
	' and isActive = 1"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	if rs.eof then
		'Look for a secondary account? Naw... registration by our site, FB, or G will have created a master account
		'?
		'sql = "select top 1 * from CO_accounts where email = '" & email & "' and isActive = 1" &_
		'" order by dateEntered desc" 
		'' and isActive = 1"
		'session("errorSQL") = sql
		'set rs = oConn.execute(sql)		
	end if	

	accountName = rs("fname") & " " & rs("lname")
	accountFirst = rs("fname")
	accountLast = rs("lname")
	accountEmail = rs("email")
	accountCurrentModelId = rs("currentModelId")
	accountRemovedModels = rs("removedModels")
	accountAddedModels = rs("addedModels")
	
	
	addModel = prepStr(request.form("addModel"))
	if addModel <> "" and not isnull(addModel) and addModel <> "00000" then
		
		'response.write ":)" & addModel
		
		if accountAddedModels = "" or isnull(accountAddedModels) then
			uStr = addModel
		else
			if instr(accountAddedModels, addModel) > 0 then
				uStr = accountAddedModels
			else
				uStr = accountAddedModels & "," & addModel	
			end if
		end if
		accountAddedModels = uStr 'Save a trip to the SQL server
		'response.write "hi!"
		sql = "update CO_accounts set " &_
		"addedModels = '" & uStr & "'" &_
		", " &_
		"currentModelId = '" & addModel & "'" &_
		" where email = '" & email & "' and isMaster = 1"
		session("errorSQL") = sql
		oConn.execute sql
		
		'If newly added Model is in Removed List, remove it from that list
		if instr(accountRemovedModels , addModel) > 0 then
			'response.end
			if not isnull(accountRemovedModels) then
				vArray = Split(accountRemovedModels, ",")
				for q = 0 to uBound(vArray)
					if q = 0 then 'It's the first one
						if vArray(q) <> addModel then
							sStr = vArray(q)
						end if
					else 'It's not the first one
						if vArray(q) <> addModel then
							if sStr = "" or isnull(sStr) then
								sStr = vArray(q)
							else
								sStr = sStr & "," & vArray(q)
							end if
						end if
					end if
				next
			end if
			'sStr = replace(accountRemovedModels, addModel, "")
			sql = "update CO_accounts " &_
			"set removedModels = '" & sStr & "'" &_
			"where email = '" & email & "' and isMaster = 1"
			session("errorSQL") = sql
			oConn.execute sql
			accountCurrentModelId = null
			accountRemovedModels = sStr
		end if

		
		'The page processing has passed beyond grabbing the currentModelId, so the page must be reloaded
		response.redirect urlRelMyAccount ' TODO: Change this not to be so specific... Defer to .htaccess when possible
		
		
	end if
	
	removeModel = prepStr(request.form("removeModel"))
	if removeModel <> "" and not isnull(removeModel) then
		
		if accountRemovedModels = "" or isnull(accountRemovedModels) then
			uStr = removeModel
		else
			if instr(accountRemovedModels, removeModel) > 0 then
				uStr = accountRemovedModels
			else
				uStr = accountRemovedModels & "," & removeModel	
			end if
		end if
		accountRemovedModels = uStr 'Save a trip to the SQL server
		'response.write "hi!"
		sql = "update CO_accounts " &_
		"set removedModels = '" & uStr & "' " &_
		"where email = '" & email & "' and isMaster = 1"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)

		'If currentModelId also matches, null it out of the database
		'Response.write "{" & accountCurrentModelId & ":" & removeModel & "}"
		if trim(accountCurrentModelId) = removeModel then
			'response.end
			sql = "update CO_accounts " &_
			"set currentModelId = NULL " &_
			"where email = '" & email & "' and isMaster = 1"
			session("errorSQL") = sql
			oConn.execute sql
			accountCurrentModelId = null
		end if
		'If newly added Model is in Removed List, remove it from that list
		if instr(accountAddedModels , removeModel) > 0 then
			'response.end
			if not isnull(accountAddedModels) then
				vArray = Split(accountAddedModels, ",")
				for q = 0 to uBound(vArray)
					if q = 0 then 'It's the first one
						if vArray(q) <> removeModel then
							sStr = vArray(q)
						end if
					else 'It's not the first one
						if vArray(q) <> removeModel then
							if sStr = "" or isnull(sStr) then
								sStr = vArray(q)
							else
								sStr = sStr & "," & vArray(q)
							end if
						end if
					end if
				next
			end if
			'sStr = replace(accountRemovedModels, addModel, "")
			sql = "update CO_accounts " &_
			"set addedModels = '" & sStr & "'" &_
			"where email = '" & email & "' and isMaster = 1"
			session("errorSQL") = sql
			oConn.execute sql
			accountCurrentModelId = null
			accountRemovedModels = sStr
		end if
		
		'The page processing has passed beyond grabbing the currentModelId, so the page must be reloaded
		response.redirect urlRelMyAccount

	end if
	
	pageTitle = "My Account" 'Where does this show up?
	
	'set xs=Server.CreateObject("ADODB.recordset")
	
	showGoogleAnalyticsLoggedIn = true 'used by /includes/template/inc_HeadTagEnd.asp
%>
<!--#include virtual="/includes/template/top.asp"-->
<script src="http://code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>
<script type="text/javascript">
function ToggleOrder(oNum){
	var oArrow = document.getElementById('ColumnArrow_' + oNum);
	var oBody = document.getElementById('OrderBody_' + oNum);
	var oTotal = document.getElementById('OrderTotal_' + oNum);
	var oMaxMinText = document.getElementById('MaxMin_' + oNum);
	var oPlusMinus = document.getElementById('PlusMinus_' + oNum);
	if (oBody.style.display != 'inherit') {
		//Maximize
		oArrow.src = '/images/account/arrow-down.jpg';
		//$("#oBody").show("slow");
		oBody.style.display = 'inherit';
		oTotal.style.display = 'inherit';
		oMaxMinText.innerHTML = 'Minimize';
		oPlusMinus.src = '/images/account/minus.jpg';	
	}else{
		//Minimize
		oArrow.src = '/images/account/arrow-side.jpg';
		oBody.style.display = 'none';
		oTotal.style.display = 'none';
		oMaxMinText.innerHTML = 'Maximize';
		oPlusMinus.src = '/images/account/plus.jpg';	
	}
}
function accessorySelect() {
	ajax('/ajax/accountPhoneSelector.asp','accessorySelectBox')
}
function newCarrier(carrierCode,menu) {
	document.getElementById("modelSelect").innerHTML = ""
	ajax('/ajax/accountPhoneSelector.asp?menu=' + menu + '&carrierCode=' + carrierCode,'brandSelect')
}
function accessoryBrandSelect(brandData,menu) {
	if (brandData == "") {
		document.getElementById("modelSelect").innerHTML = ""
	}
	else {
		document.getElementById("modelSelect").innerHTML = "Searching for models..."
		brandSplit = brandData.split("#")
		accessoryBrandID = brandSplit[0]
		accessoryBrandName = brandSplit[1]
		ajax('/ajax/accountPhoneSelector.asp?menu=' + menu + '&brandID=' + accessoryBrandID,'modelSelect')
	}
}
function goButton_main() {		
	//validation
	//window.location = '/accessories/' + accessoryBrandName + '/';
	var elm = document.getElementById('modelID');
	var val = elm.options[elm.selectedIndex].value;
	var button = document.getElementById('addModel');
	button.value = val;
	button.click();
	//document.getElementById('addModel').click();
}


</script>
<link rel="stylesheet" type="text/css" href="/includes/css/account.css" />
<div id="AccountWrapper">
    <div id="accountSettings">
        <a href="javascript:void();"><img src="/images/account/button-settings.jpg" /></a>
    </div>
    <div id="accountLogOff">
        <a href="/account/logout.asp"><img src="/images/account/button-sign-out.jpg" /></a>
    </div>
    <h2>Hello, <span class="accountName"><%=accountName%></span></h2>
    <div class="accountHr"></div>
    
    <%
		'Determine primary phone
		
		if accountCurrentModelId <> "" and not isnull(accountCurrentModelId) then
			sql = "select modelName, brandID, modelImg, isPhone from we_Models m where modelID = " & accountCurrentModelId
			set xs = oConn.execute(sql)
			primaryModelName = xs("modelName")
			primaryBrandId = xs("brandID")
			primaryPic = xs("modelImg")
			primaryModelId = accountCurrentModelId
			primaryIsPhone = xs("isPhone")

		else
			sql = "select o.orderdatetime, m.modelID, m.modelName, m.modelImg, m.brandID, m.modelName, m.isPhone " &_
			"from CO_accounts a " &_
			"		left join we_orders o on a.accountid = o.accountid " &_
			"		left join we_orderdetails od on o.orderid = od.orderid " &_
			"		left join we_Items i on od.itemid = i.itemID " &_
			"		left join we_Models m on i.modelID = m.modelID " &_
			"where email = '" & email & "' and modelName != 'Universal' "
			if not isnull(accountRemovedModels) then
				vArray = Split(accountRemovedModels, ",")
				for q = 0 to uBound(vArray)
					sql = sql & "and m.modelID not like '%" & vArray(q) & "%' "
				next
			end if
			
			sql = sql & "order by o.orderdatetime desc"
			session("errorSQL") = sql
			set rs = oConn.execute(sql)
			
			if not rs.eof then
				primaryModelName = rs("modelName")
				primaryBrandId = rs("brandID")
				primaryModelId = rs("modelID")
				primaryPic = rs("modelImg")
				primaryModelId = rs("modelID")
				primaryIsPhone = rs("isPhone")
			else
				primaryModelName = "None"
				primaryBrandId = "None"
				primaryModelId = "None"
				primaryPic = "None"
				primaryModelId = "None"
				primaryIsPhone = false
			end if

		end if
		'if false then
		if not primaryModelName = "None" then
		%>
    
    
    <div class="accountTitleBar">Your <%  if primaryIsPhone then response.write " Cell Phone " else response.write " Device " %></div>
    <div class="accountContent">
    	<h3><%=primaryModelName%> <%  if primaryIsPhone then response.write " Cell Phone " %> Accessories</h3>
    	<div class="quarter" id="first">
        	<div id="imgPrimaryPhoto"><img src="/modelpics/<%=primaryPic%>" height="165" /></div>
            <div>
            <form name="phoneSelectForm" id="phoneSelectForm" action="" method="post">
            <div id="accessorySelectBox">
                <a class="clickable" onclick="accessorySelect()"><img src="/images/account/button-select-different-phone.jpg" /></a><br />
                <input class="removeModel" type="submit" name="removeModel" value="<%=primaryModelId%>" id="PrimaryRemoveModel" />
            </div>
            <input type="submit" style="display:none" id="addModel" name="addModel" value="00000" />
            </form>
            </div>
            <div><form action="" method="post"></div>
        </div>
        <div class="quarter">
        	<ul class="arrows">
            	<li><a href="javascript:void();" onclick="ajax('/ajax/accountBrandModelCategory.asp?brandID=' + <%=primaryBrandId%> + '&modelID=' + <%=primaryModelId%> + '&typeID=' + 3, 'dynAccessory');">Covers &amp; Gel Skins</a></li>
                <li><a href="javascript:void();" onclick="ajax('/ajax/accountBrandModelCategory.asp?brandID=' + <%=primaryBrandId%> + '&modelID=' + <%=primaryModelId%> + '&typeID=' + 7, 'dynAccessory');">Cases &amp; Pouches</a></li>
                <li><a href="javascript:void();" onclick="ajax('/ajax/accountBrandModelCategory.asp?brandID=' + <%=primaryBrandId%> + '&modelID=' + <%=primaryModelId%> + '&typeID=' + 18, 'dynAccessory');">Screen Protectors</a></li>
                <li><a href="javascript:void();" onclick="ajax('/ajax/accountBrandModelCategory.asp?brandID=' + <%=primaryBrandId%> + '&modelID=' + <%=primaryModelId%> + '&typeID=' + 2, 'dynAccessory');">Chargers &amp; Data Cables</a></li>
                <li><a href="javascript:void();" onclick="ajax('/ajax/accountBrandModelCategory.asp?brandID=' + <%=primaryBrandId%> + '&modelID=' + <%=primaryModelId%> + '&typeID=' + 1, 'dynAccessory');">Batteries</a></li>
            </ul>
        </div>
        <div class="quarter">
        	<ul class="arrows">
            	<li><a href="javascript:void();" onclick="ajax('/ajax/accountBrandModelCategory.asp?brandID=' + <%=primaryBrandId%> + '&modelID=' + <%=primaryModelId%> + '&typeID=' + 5, 'dynAccessory');">Bluetooth &amp; Hands-free</a></li>
                <li><a href="javascript:void();" onclick="ajax('/ajax/accountBrandModelCategory.asp?brandID=' + <%=primaryBrandId%> + '&modelID=' + <%=primaryModelId%> + '&typeID=' + 6, 'dynAccessory');">Holsters &amp; Car Mounts</a></li>
                <li><a href="javascript:void();" onclick="ajax('/ajax/accountBrandModelCategory.asp?brandID=' + <%=primaryBrandId%> + '&modelID=' + <%=primaryModelId%> + '&typeID=' + 19, 'dynAccessory');">Vinyl Skins</a></li>
                <li><a href="javascript:void();" onclick="ajax('/ajax/accountBrandModelCategory.asp?brandID=' + <%=primaryBrandId%> + '&modelID=' + <%=primaryModelId%> + '&typeID=' + 16, 'dynAccessory');">Wholesale Cell Phones</a></li>
                <li><a href="javascript:void();" onclick="ajax('/ajax/accountBrandModelCategory.asp?brandID=' + <%=primaryBrandId%> + '&modelID=' + <%=primaryModelId%> + '&typeID=' + 999999, 'dynAccessory');">Other Accessories</a></li>
            </ul>
        	
        </div>
        <% 'rs.movefirst %>
        <% 'rs.movenext %>
        
        <% 
			'Determine Other Phones
			''''''''''''''''''''''''''''''''  V 3.0 ''''''''''''''''''''''''''''''''''''''''''''' Does not sort phones
			sql = "select distinct modelID, modelName, modelImg, brandID from " &_
				"(select m.modelID, m.modelName, m.modelImg, m.brandID from CO_accounts a " &_
				"left join we_orders o on a.accountid = o.accountid " &_
				"left join we_orderdetails od on o.orderid = od.orderid " &_
				"left join we_Items i on od.itemid = i.itemID " &_
				"left join we_Models m on i.modelID = m.modelID where email = '" & email & "' and modelName != 'Universal' "
			if not isnull(accountRemovedModels) then
				sql = sql & "and m.modelID not in (" & accountRemovedModels & ") "
			end if
			sql = sql &	") as distinctModelID "
			''''''''''''''''''''''''''''''''  V 2.0 '''''''''''''''''''''''''''''''''''''''''''''
			'sql = "select distinct e.modelID, e.modelName, f.orderdatetime " &_
			'"from we_orders a " &_
			'"	left join v_accounts b on a.accountid = b.accountid and b.site_id = a.store " &_
			'"	left join we_orderdetails c on a.orderid = c.orderid " &_
			'"	left join we_Items d on c.itemid = d.itemID " &_
			'"	left join we_Models e on d.modelID = e.modelID " &_
			'"	outer apply ( " &_
			'"		select top 1 ia.orderdatetime " &_
			'"		from we_orders ia " &_
			'"			left join v_accounts ib on ia.accountid = ib.accountid and ib.site_id = ia.store " &_
			'"			left join we_orderdetails ic on ia.orderid = ic.orderid " &_
			'"			left join we_Items id on ic.itemid = id.itemID " &_
			'"		where ib.EMAIL = b.EMAIL and id.modelID = d.modelID " &_
			'"		order by ia.orderdatetime desc) f " &_
			'"where b.EMAIL = '" & email & "' and e.modelName <> 'Universal' "
			'if accountCurrentModelId <> "" and not isnull(accountCurrentModelId) then
			'	sql = sql & "and e.modelID not like '" & accountCurrentModelId & "' "
			'end if
			'if not isnull(accountRemovedModels) then
			'	vArray = Split(accountRemovedModels, ",")
			'	for q = 0 to uBound(vArray)
			'		sql = sql & "and e.modelID not like '" & vArray(q) & "' "
			'	next
			'end if
			'sql = sql & "order by f.orderdatetime desc"
			session("errorSQL") = sql
			'response.write sql
			set rs = oConn.execute(sql)
			
			'V1'
			'"select o.orderdatetime, m.modelID, m.modelName, m.modelImg, m.brandID " &_
			'"from CO_accounts a " &_
			'"		left join we_orders o on a.accountid = o.accountid " &_
			'"		left join we_orderdetails od on o.orderid = od.orderid " &_
			'"		left join we_Items i on od.itemid = i.itemID " &_
			'"		left join we_Models m on i.modelID = m.modelID " &_
			'"where email = '" & email & "' and modelName != 'Universal' "
			'if not isnull(accountRemovedModels) then
			'	vArray = Split(accountRemovedModels, ",")
			'	for q = 0 to uBound(vArray)
			'		sql = sql & "and m.modelID not like '%" & vArray(q) & "%' "
			'	next
			'end if
			'sql = sql & "order by o.orderdatetime desc"
			'session("errorSQL") = sql
			'set rs = oConn.execute(sql)
		
					'TODO: Merge RSs or something similar
			
			sql = "Select modelID, modelName from we_Models " &_
			"where modelID is null "
			if not isnull(accountAddedModels) then
				vArray = Split(accountAddedModels, ",")
				for q = 0 to uBound(vArray)
					sql = sql & "or modelID = " & vArray(q)
				next
			end if
			session("errorSQL") = sql
			set ts = oConn.execute(sql)
			

		
		
		
		
		%>
        
        <div class="quarter" id="fourth">
        	<p class="pOtherPhones">Phones you've ordered for</p>
            <ul class="discs">
			<% 
				if not rs.eof then
				while not rs.eof
				%>
						<li><input type="submit" id="makePrimaryModel_<%=rs("modelID")%>" name="makePrimaryModel" value="<%=rs("modelID")%>" style="display:none" /><a href="javascript:document.getElementById('makePrimaryModel_<%=rs("modelID")%>').click();"><%=chopStr(rs("modelName"),20)%></a><div><input class="removeModel" type="submit" name="removeModel" value="<%=rs("modelID")%>" /></div></li>
				<% rs.movenext %>
				<% wend %>
            <% else %>
            	<li class="NoneFound">None found</li>
            <% end if %>
            </ul>
            <p class="pOtherPhones">Phones you've selected</p>
            <ul class="discs">
            			<%
			   if not ts.eof then
			   while not ts.eof
			%>
                    <li><input type="submit" id="makePrimaryModel_<%=ts("modelID")%>" name="makePrimaryModel" value="<%=ts("modelID")%>" style="display:none" /><a href="javascript:document.getElementById('makePrimaryModel_<%=ts("modelID")%>').click();"><%=chopStr(ts("modelName"),20)%></a><div><input class="removeModel" type="submit" name="removeModel" value="<%=ts("modelID")%>" /></div></li>
			<% 
				ts.movenext
				wend
            	else %>
            	<li class="NoneFound">None found</li>
            <% end if %>


            
            
            
            
            </ul>
        </div>
        <div style="clear:both;"></div>
    	</form>
    </div>
    
    
    <div class="accountTitleBar flat"><%  if primaryIsPhone then response.write " Cell Phone " %> Accessories for <%=primaryModelName%></div>
    <div class="accountContent" id="dynAccessory">
		<%
        'sql = " select top 8 a.price_Retail, a.price_CO, a.itemPic_CO, a.itemDesc_CO, a.itemID from we_Items a " &_
		'"left join we_Items b on a.partNumber = b.partNumber and b.master = 1 " &_
        '" where a.brandID = " & primaryBrandId &_
        '" and b.inv_qty > 0 and a.price_CO is not null and a.price_CO > 0 and a.hideLive = 0 " &_
		'" order by a.numberOfSales "
        sql = "select top 8 a.price_Retail, a.price_CO, a.itemPic_CO, a.itemDesc_CO, a.itemID " &_
		"from we_Items a  " &_
			"left join we_Items b on a.partNumber = b.partNumber and b.master = 1 " &_
		"where b.inv_qty > 0 and a.price_CO is not null and a.price_CO > 0 " &_
		"and a.hideLive = 0 and a.brandID = " & primaryBrandId & " " &_
		"and a.modelID = " & primaryModelId & " " &_
		"order by a.numberOfSales desc"
		
		session("errorSQL") = sql
        set rs = oConn.execute(sql)
        
        if not rs.eof then
        while not rs.eof
        %>
        
        <div class="accountSuggestedProduct">
            <div class="photo">
                <a href="/p-<%=rs("itemID")%>-<%=formatSEO(rs("itemDesc_CO"))%>.html"><img src="/productPics/thumb/<%=rs("itemPic_CO")%>" width="100" height="100" /></a>
            </div>
            <div class="link"><a href="/p-<%=rs("itemID")%>-<%=formatSEO(rs("itemDesc_CO"))%>.html"><%=rs("itemDesc_CO")%></a></div>
            <div class="price"><%=FormatCurrency(rs("price_CO"))%></div ><% pctOff = Int(100-((rs("price_CO")/rs("price_Retail"))*100)) %>
            <div class="savings">(<%=pctOff%>% OFF Retail Price: <del><%=FormatCurrency(rs("price_Retail"))%>)</del></div >
        </div>
        
        
		<% 
        rs.movenext
        wend
        end if
        %>
        <div style="clear:both;"></div>
    </div>
    
	<% else %>

    <div class="accountTitleBar">Select Your Phone</div>
    <div class="accountContent" id="dynAccessory">
        <form action="" method="post">
			<% 
				'TODO: Reuse rs
			   'rs.movefirst


			'Determine Other Phones
			''''''''''''''''''''''''''''''''  V 3.0 ''''''''''''''''''''''''''''''''''''''''''''' Does not sort phones
			sql = "select distinct modelID, modelName, modelImg, brandID from " &_
				"(select m.modelID, m.modelName, m.modelImg, m.brandID from CO_accounts a " &_
				"left join we_orders o on a.accountid = o.accountid " &_
				"left join we_orderdetails od on o.orderid = od.orderid " &_
				"left join we_Items i on od.itemid = i.itemID " &_
				"left join we_Models m on i.modelID = m.modelID where email = '" & email & "' and modelName != 'Universal' "
			if not isnull(accountRemovedModels) then
				sql = sql & "and m.modelID not in (" & accountRemovedModels & ") "
			end if
			sql = sql &	") as distinctModelID "
			''''''''''''''''''''''''''''''''  V 2.0 '''''''''''''''''''''''''''''''''''''''''''''
			'sql = "select distinct e.modelID, e.modelName, f.orderdatetime " &_
			'"from we_orders a " &_
			'"	left join v_accounts b on a.accountid = b.accountid and b.site_id = a.store " &_
			'"	left join we_orderdetails c on a.orderid = c.orderid " &_
			'"	left join we_Items d on c.itemid = d.itemID " &_
			'"	left join we_Models e on d.modelID = e.modelID " &_
			'"	outer apply ( " &_
			'"		select top 1 ia.orderdatetime " &_
			'"		from we_orders ia " &_
			'"			left join v_accounts ib on ia.accountid = ib.accountid and ib.site_id = ia.store " &_
			'"			left join we_orderdetails ic on ia.orderid = ic.orderid " &_
			'"			left join we_Items id on ic.itemid = id.itemID " &_
			'"		where ib.EMAIL = b.EMAIL and id.modelID = d.modelID " &_
			'"		order by ia.orderdatetime desc) f " &_
			'"where b.EMAIL = '" & email & "' and e.modelName <> 'Universal' "
			'if accountCurrentModelId <> "" and not isnull(accountCurrentModelId) then
			'	sql = sql & "and e.modelID not like '" & accountCurrentModelId & "' "
			'end if
			'if not isnull(accountRemovedModels) then
			'	vArray = Split(accountRemovedModels, ",")
			'	for q = 0 to uBound(vArray)
			'		sql = sql & "and e.modelID not like '" & vArray(q) & "' "
			'	next
			'end if
			'sql = sql & "order by f.orderdatetime desc"
			session("errorSQL") = sql
			'response.write sql
			set rs = oConn.execute(sql)
			
			'TODO: Merge RSs or something similar
			
			sql = "Select modelID, modelName from we_Models " &_
			"where modelID is null "
			if not isnull(accountAddedModels) then
				vArray = Split(accountAddedModels, ",")
				for q = 0 to uBound(vArray)
					sql = sql & "or modelID = " & vArray(q)
				next
			end if
			session("errorSQL") = sql
			set ts = oConn.execute(sql)

			
			


			   
				''Determine Other Phones
				''''''''''''''''''''''''''''''''  V 1.0 '''''''''''''''''''''''''''''''''''''''''''''
				'sql = "select o.orderdatetime, m.modelID, m.modelName, m.modelImg, m.brandID " &_
				'"from CO_accounts a " &_
				'"		left join we_orders o on a.accountid = o.accountid " &_
				'"		left join we_orderdetails od on o.orderid = od.orderid " &_
				'"		left join we_Items i on od.itemid = i.itemID " &_
				'"		left join we_Models m on i.modelID = m.modelID " &_
				'"where email = '" & email & "' and modelName != 'Universal' "
				'
				'if not isnull(accountRemovedModels) then
				'	'vArray = Split(accountRemovedModels, ",")
				'	
				'	for q = 0 to uBound(vArray)
				'		sql = sql & "and m.modelID not like '%" & vArray(q) & "%' "
				'	next
				'	response.end
				'else
				'	'response.Write("test")
				'	'response.End()
				'end if
				'
				'sql = sql & "order by o.orderdatetime desc"
				'session("errorSQL") = sql
				'set rs = oConn.execute(sql)
				%>
			
        <div class="quarter fourthRight" id="fourth">
        	<p class="pOtherPhones">Phones you've ordered for</p>
            <ul class="discs">
				
			<%
			   if not rs.eof then
			   while not rs.eof
			%>








                    <li><input type="submit" id="makePrimaryModel_<%=rs("modelID")%>" name="makePrimaryModel" value="<%=rs("modelID")%>" style="display:none" /><a href="javascript:document.getElementById('makePrimaryModel_<%=rs("modelID")%>').click();"><%=chopStr(rs("modelName"),20)%></a><div><input class="removeModel" type="submit" name="removeModel" value="<%=rs("modelID")%>" /></div></li>
			<% 
				rs.movenext
				wend
            	else %>
            	<li class="NoneFound">None found</li>
            <% end if %>
            
            <p class="pOtherPhones">Phones you've selected</p>
            <ul class="discs">

			<%
			   if not ts.eof then
			   while not ts.eof
			%>
                    <li><input type="submit" id="makePrimaryModel_<%=ts("modelID")%>" name="makePrimaryModel" value="<%=ts("modelID")%>" style="display:none" /><a href="javascript:document.getElementById('makePrimaryModel_<%=ts("modelID")%>').click();"><%=chopStr(ts("modelName"),20)%></a><div><input class="removeModel" type="submit" name="removeModel" value="<%=ts("modelID")%>" /></div></li>
			<% 
				ts.movenext
				wend
            	else %>
            	<li class="NoneFound">None found</li>
            <% end if %>
            






            </ul>
        </div>
    	</form>
    	<form name="phoneSelectForm" id="phoneSelectForm" action="" method="post">
            <p>We couldn't determine which cell phone you have.</p><p>Would you like to choose one?</p>
            <div class="home_shopByBttns1">
                <div id="accessorySelectBox"><a class="clickable" onclick="accessorySelect()"><img src="/images/account/button-select-different-phone.jpg" /></a></div>
            </div>
            <input type="submit" style="display:none" id="addModel" name="addModel" value="00000" />
    	</form>
        <div style="clear:both;"></div>
        
        
	</div>
    
    <% end if %>
    
    


    <h3>Order History</h3>
    <div class="accountTitleBar flat">
    	<div class="ColumnContainer">
        	<div class="Column1">Date Ordered</div>
            <div class="Column2">Order Number</div>
            <div class="Column3">Status</div>
            <div class="Column4">Details</div>
        </div>
    </div>
    <div class="accountContent">

		<%
		' TODO
		' Optimize this page to use a single query and do while loops on it
		'sql = "select o.orderdatetime, o.orderid, o.cancelled, o.shipped, o.approved, " &_
		'"o.RMAstatus, od.itemid, i.brandID, i.typeID, i.subtypeID, i.itemDesc, i.itemPic, m.modelName " &_
		'"	from CO_accounts a " &_
		'"		left join we_orders o on a.accountid = o.accountid " &_
		'"		left join we_orderdetails od on o.orderid = od.orderid " &_
		'"		left join we_Items i on od.itemid = i.itemID " &_
		'"		left join we_Models m on i.modelID = m.modelID " &_
		'"where email = '" & email & "' and store = 2 " &_
		'"order by orderdatetime desc"
		sql = "select o.orderdatetime, o.orderid, o.cancelled, o.shipped, o.approved, o.RMAstatus, o.ordergrandtotal " &_
		"	from CO_accounts a " &_
		"		left join we_orders o on a.accountid = o.accountid " &_
		"where email = '" & email & "' and store = 2 " &_
		"order by orderdatetime desc"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		'rs.Open sql, oConn, adOpenDynamic
	
		if not rs.eof then
			intOrderNum = 0
			do while not rs.eof
			
			
				orderDateTime = rs("orderdatetime")
				orderId = rs("orderid")
				orderCancelled = rs("cancelled")
				orderShipped = rs("shipped")
				orderApproved = rs("approved")
				orderRMAStatus = rs("RMAstatus")
				orderGrandTotal = rs("ordergrandtotal")
				'Need to move these into nested loop and prefix with order details
				
				'if previousOrderId <> orderId then
					
					%>
				
				<div class="OrderContainer" id="OrderContainer_<%=intOrderNum%>">
					<div class="OrderRow" id="OrderRow_<%=intOrderNum%>">
						<div class="ColumnContainer">
							<div class="ColumnArrow"><a href="javascript:void();" onclick="ToggleOrder('<%=intOrderNum%>')"><img src="/images/account/arrow-side.jpg" id="ColumnArrow_<%=intOrderNum%>" /></a></div>
							<div class="Column1 DateOrdered"><%=dateonly(orderDateTime)%></div>
							<div class="Column2 OrderNumber"><%=orderId%></div>
							<div class="Column3 OrderStatus"><%
                            
							if orderCancelled = true then
								%><span class="OrderStatusCancelled">Cancelled</span><%
								StatusMessage = "Your order #" & orderId & " was cancelled. "
							else
								if not isnull(orderRMAStatus) then
									%><span class="OrderStatusRMA">RMA</span><%
									StatusMessage = "Your order #" & orderId & " has one or more items placed on RMA status. "
								else
									if orderShipped then
										%><span class="OrderStatusShipped">Shipped</span><%
										StatusMessage = "Your order #" & orderId & " was shipped. "
									else
										if orderApproved then
											%><span class="OrderStatusProcessing">Processing</span><%
											StatusMessage = "Your order #" & orderId & " has been received and is currently being processed. " &_
											"All orders are processed within 1-2 business days.  You will receive an order shipment confirmation after the order has been shipped from our warehouse."
										else
											%><span class="OrderStatusError">Not Available</span><%
											StatusMessage = "Information on your order #" & orderId & " is not currently available. " &_
											"Please contact us with any questions on your order."
										end if
									end if
								end if
							end if
							                            
                            %></div>
							<div class="Column4 DetailsToggle"><a href="javascript:void();" onclick="ToggleOrder('<%=intOrderNum%>')"><img src="/images/account/plus.jpg" id="PlusMinus_<%=intOrderNum%>" /> <span id="MaxMin_<%=intOrderNum%>">Maximize</span></a></div>
						</div>
					</div>
					<div class="OrderBody" id="OrderBody_<%=intOrderNum%>">
						<div class="OrderExplanation"><p><%=StatusMessage%></p></div>
						<% 	
							sql = "select od.itemid, i.brandID, i.typeID, i.subtypeID, i.itemDesc, i.itemPic_CO, m.modelName " &_
							"	from CO_accounts a " &_
							"		left join we_orders o on a.accountid = o.accountid " &_
							"		left join we_orderdetails od on o.orderid = od.orderid " &_
							"		left join we_Items i on od.itemid = i.itemID " &_
							"		left join we_Models m on i.modelID = m.modelID " &_
							"where email = '" & email & "' and store = 2 and o.orderid = " & orderId & " " &_
							"order by orderdatetime desc"
							session("errorSQL") = sql
							
							set xs = oConn.execute(sql)
							
							
							orderItemDetailCounter = 0
							do while not xs.eof
							
								orderItemId = xs("itemid")
								orderBrandID = xs("brandID")
								orderTypeId = xs("typeID")
								orderSubTypeId = xs("subtypeID")
								orderItemDesc = xs("itemDesc")
								orderItemPic = xs("itemPic_CO")
								orderModelName = xs("modelName")
								%>
								<div class="OrderItem" id="OrderItemDetail_<%=orderItemDetailCounter%>">
									<div class="OrderItemPhoto"><img src="/productPics/thumb/<%=orderItemPic%>" width="62" height="62" /></div>
									<div class="OrderItemTitle"><a href=""><%=orderItemDesc%></a></div>
									<div class="OrderItemNo">Item No: <%=orderItemId%></div>
								</div>
								<div style="clear:both"></div>
								<% 
								'rs.movenext
								'if rs.eof then eFlag = true
								'session("eFlag") = eFlag
								'orderItemDetailCounter = orderItemDetailCounter + 1
								'loop while orderId = rs("orderId") and not rs.eof
								xs.movenext
							loop
						''rs.movefirst
						''for L = 0 to intOrderNum
						''	'rs.moveprevious 'forward-only cursor won't allow moveprevious method
						''	if not rs.eof then
						''		rs.movenext
						''	end if
						''next
						%>
					</div>
					<div class="OrderTotal flat" id="OrderTotal_<%=intOrderNum%>">
						<div class="OrderTotalText">Total Amount:</div>
						<div class="OrderTotalAmount">$<%=orderGrandTotal%></div>
					</div>
				</div>
				                
                <% 
				'end if
				'previousOrderId = orderId
				%>
				
				<% 'if eFlag then exit do %>
				<% rs.movenext %>
				
				<% if not rs.eof then 'while will never allow rs.eof, so this must follow rs.movenext %>
				<div class="OrderSeparator"></div>
				<% end if %>
				
				<% 'if not rs.eof then rs.MoveNext 'the reader is advanced by the inner / nested loop for each item %>
                <% 'response.write intOrderNum %>
				<% intOrderNum = intOrderNum + 1 %>
                <% if rs.eof then exit do %>
			<% loop %>
		<% else %>
			<p>You have not made any orders under this account with us.</p>
		<% end if %>        
        
        <!--
        <% for o = 1 to 1 %>
        

		<div class="OrderContainer">
        	<div class="OrderRow">
                <div class="ColumnContainer">
                    <div class="ColumnArrow"><a href="javascript:void();"><img src="/images/account/arrow-side.jpg" /></a></div>
                    <div class="Column1 DateOrdered">Tue. 06/17/2012</div>
                    <div class="Column2 OrderNumber">#1506544</div>
                    <div class="Column3 OrderStatus"><span class="OrderStatusShipped">Shipped</span></div>
                    <div class="Column4 DetailsToggle"><a href="javascript:void();"><img src="/images/account/plus.jpg" /> View Details</a></div>
                </div>
            </div>
            <div class="OrderBody" style="display:none;">
            	<div class="OrderExplanation"><p>Your order #1508039 has been received and is currently being processed.  All orders are processed within 1-2 business days.  You will receive an order shipment confirmation after the order has been shipped from our warehouse.</p></div>
                <% for i = 1 to 3 %>
                <div class="OrderItem">
                	<div class="OrderItemPhoto"><img src="http://static.trustedreviews.com/94%7C000023989%7C3659_s3fronts.jpg" width="62" height="62" /></div>
                    <div class="OrderItemTitle"><a href="">Samsung Galaxy S3 Anti-Glare Scren Protector Film</a></div>
                    <div class="OrderItemNo">Item No: LC7-SAM-SIII-210</div>
                </div>
                <div style="clear:both"></div>
                <% next %>
                
            </div>
            <div class="OrderTotal flat" style="display:none;">
                <div class="OrderTotalText">Total Amount:</div>
                <div class="OrderTotalAmount">$34.99</div>
            </div>
        </div>
        
		<div class="OrderSeparator"></div>

        <% next %>
		-->
        
    </div>

</div>

<%
rs.close
'rs = nothing
%>
<!--#include virtual="/includes/template/bottom.asp"-->
