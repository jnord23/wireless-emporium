<%
response.buffer = true

dim SEtitle, SEdescription, SEkeywords, pageTitle
SEtitle = "Cheap Cell Phone Accessories | Cell Phone Covers | Cell Phone Cases | Mobile Batteries"
SEdescription = "Buy Cheap Cell Phone Accessories, mobile Phone Covers, cases, batteries & chargers at discount wholesale prices for all major brands like Motorola, LG, Nokia, Samsung and other Cell phone accessories!"
SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone, cellular accessories, cell phone accessories wholesale, phone bling, phone accessories, cell phone cases, cell phone covers"
pageTitle = "Home"
homepage = 1

myModel = prepInt(request.Cookies("myModel"))
myBrand = prepInt(request.Cookies("myBrand"))

holidaySale = 0
if date > cdate("11/24/2011") and date < cdate("11/30/2011") then holidaySale = 1
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top_index.asp"-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td style="height:10px;">&nbsp;</td></tr>
    <tr>
        <td style="border-top:2px solid #cccccc; border-bottom:2px solid #cccccc; border-right:2px solid #cccccc;">
            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <%
                if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then activeCol = "staging" else activeCol = "active"
				if holidaySale = 1 then
	                sql = "select * from co_mainbanners where id in (7,8,9,10) order by orderNum"
				else
	                sql = "select * from co_mainbanners where " & activeCol & " = 1 order by orderNum"
				end if

				session("errorSQL") = sql
                set rs = Server.CreateObject("ADODB.Recordset")
                rs.open sql, oConn, 0, 1

                lap = 0
                bannerRotation = ""
                do while not rs.EOF
                    lap = lap + 1
                    if rs("defaultBanner") then
                        def_img = rs("picLoc")
                        def_link = rs("link")
                        def_cell = lap
                    end if
                    bannerRotation = bannerRotation & "image" & lap & " = new Image();" & vbcrlf
                    bannerRotation = bannerRotation & "image" & lap & ".src = '/images/mainbanner/" & rs("picLoc") & "';" & vbcrlf
                    if lap = 1 then useClass = "activeBigTab" else useClass = "inactiveBigTab"
                %>
                <tr>
                    <td class="<%=useClass%>" align="left" valign="top" style="height:53px; padding-top:15px; width:200px;" id="cell-<%=lap%>" onMouseOver="chgBanner('<%=rs("link")%>','<%=rs("picLoc")%>',<%=lap%>)">
                        <% if instr(rs("link"),"http://www") > 0 then %>
                        <a href="<%=rs("link")%>" target="_blank" style="color:#FFF; text-decoration:none; font-family:Arial, Helvetica, sans-serif;<%=rs("tabTitleFont")%>"><%=rs("tabTitle")%></a><br>
                        <a href="<%=rs("link")%>" target="_blank" style="color:#FFF; text-decoration:none; font-family:Arial, Helvetica, sans-serif;<%=rs("tabSubTitleFont")%>"><%=rs("tabSubTitle")%></a>
						<% else %>
                        <a href="<%=rs("link")%>" style="color:#FFF; text-decoration:none; font-family:Arial, Helvetica, sans-serif;<%=rs("tabTitleFont")%>"><%=rs("tabTitle")%></a><br>
                        <a href="<%=rs("link")%>" style="color:#FFF; text-decoration:none; font-family:Arial, Helvetica, sans-serif;<%=rs("tabSubTitleFont")%>"><%=rs("tabSubTitle")%></a>
                        <% end if %>
                    </td>
                    <% if lap = 1 then %>
                    <td rowspan="4" id="curBanner" valign="top"><a href="<%=def_link%>" id="link1"><div id="banner" style="width: 620px; height: 275px; background: url(/images/mainBanner/<%=def_img%>) no-repeat; cursor:pointer;"></div></a></td>
                    <% end if %>
                </tr>
                <%
                    rs.movenext
                loop
                %>
            </table>
        </td>
    </tr>
	<!--#include virtual="/includes/asp/inc_bottom_index.asp"-->
    <tr>
        <td class="top-text-grey" style="padding:10px 1px 10px 0px; text-align:justify;" width="100%">
			<h1 style="color:#3399CC; font-weight:bold; font-size:14px; margin:0px; display:inline;">Cell Phone Accessories</h1>
			&nbsp; Welcome to the primary destination online for cell phone accessories at wholesale pricing to the public! 
			We offer quality cell phone solutions for businesses, government, educational institutions and of course, the individual consumer. 
			We carry 1000s of genuine, 100% quality assurance guaranteed products minus the retail mark-ups. 
			Why shop around? Purchase more and save more with the <strong>lowest prices online guaranteed!</strong>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom_index.asp"-->