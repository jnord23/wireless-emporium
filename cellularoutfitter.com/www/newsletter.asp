<%
pageTitle = "Thank you for subscribing to the CellularOutfitter.com Newsletter!"
dim fso : set fso = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<table border="0" cellspacing="0" cellpadding="0" width="746" onclick="window.location='/'">
    <tr>
        <td class="top-sublink-gray">
            <a class="top-sublink-gray" href="http://www.CellularOutfitter.com/">Cell Phone Accessories</a>&nbsp;>&nbsp;
            <span class="top-sublink-blue"><%=pageTitle%></span>
        </td>
    </tr>
    <tr>
        <td class="top-sublink-gray"><img src="http://www.cellularoutfitter.com/images/spacer.gif" width="1" height="10"></td>
    </tr>
    <tr>
        <td valign="top" class="top-sublink-gray" width="541">
            <%
			dim semail
            semail = trim(request.form("semail"))
            strError = ""
            if semail <> "" then
                if instr(trim(semail), "@") > 0 and instr(trim(semail), " ") = 0 and instr(trim(semail), ".@") = 0 then
                    if isValidEmail(semail) then									
                        sql = "exec sp_emailSub 2,'" & semail & "',''"
                        session("errorSQL") = sql
                        set emailRS = oConn.execute(sql)
                        
                        if emailRS("returnValue") = "duplicate" then
                            %>
                            <div style="width:450px; margin:30px auto; font-size:16px; font-weight:bold; color:#000;">
                                Hi,<br />
                                Thank you for our interest in joining our email program. Our records indicate that you are already registered 
                                making you ineligible for this offer. We're sorry for any inconvenience this may cause but be sure to check 
                                your inbox for our latest newsletter as it may include a coupon that you're eligible for.
                                <br /><br />
                                Sincerely,<br />                                                    
                                CellularOutfitter.com
                            </div>
                            <%
                        else
                            dim cdo_to, cdo_from, cdo_subject, cdo_body
                            set curHTML = fso.OpenTextFile(server.MapPath("/images/email/blast/template/co-welcome-email.htm"),1,false,0)
                            cdo_body = curHTML.readall
							cdo_body = replace(cdo_body,"{{campaign_id}}","welcome1")
							cdo_body = replace(cdo_body,"{{promo_code}}","WELCOME25")
                            
                            cdo_to = semail
                            cdo_from = "webmaster@CellularOutfitter.com"
                            cdo_subject = "Welcome and Prepare Yourself for Serious Savings"
                            
                            CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
                            %>
                            <div style="width:450px; margin:30px auto; font-size:16px; font-weight:bold; color:#000;">
                                Thanks for joining!
                                <br /><br />
                                We are excited that you decided to join our exclusive email membership club. You're now set to 
                                get access to private sales, handcrafted deals and coupon codes, as well as the latest in cell 
                                phone news.
                                <br /><br />
                                Here at CellularOutfitter.com, we're 100% dedicated in ensuring that you have the best cell 
                                phone accessory shopping experience. We're always working hard to ensure we have the biggest 
                                selection at the lowest possible prices. That is why we have been the #1 name in cell phone 
                                accessories since 2001. 
                                <br /><br />
                                Looking forward to seeing you soon,<br />
                                The Cellular Outfitter Team
                            </div>
                <%
                        end if
                    else
                        strError = "Please enter a valid email address and try again. (" & semail & ")"
                    end if
                else
                    strError = "Please make sure you enter a valid email address and try again. (" & semail & ")"
                end if
            else
                strError = "Email error, please double check that you have entered a valid email. (" & semail & ")"
            end if
            
            if strError <> "" then
            %>
            <p>
                <strong>
                    <br><br>
                    <h3><%=strError%></h3>
                </strong>
            </p>
            <%
            end if
            %>
        </td>
    </tr>
</table>
<%
Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address
	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function
%>
<!--#include virtual="/includes/template/bottom.asp"-->
