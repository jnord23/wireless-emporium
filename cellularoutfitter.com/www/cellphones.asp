<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
brandPage = 1
curPageName = "cellphones"
categoryName = "Cell Phones"

dim carrierID : carrierID = prepInt(request.querystring("carrierid"))
dim brandID : brandID = prepInt(request.querystring("brandid"))
dim sortBy : sortBy = prepStr(request.querystring("sb"))
if brandid = 0 then	response.redirect("/")

call fOpenConn()
sql	=	"select	a.itemid, a.itemdesc_co, a.itempic_co, a.price_retail, a.price_co, b.brandname, c.modelname, c.modelimg, e.carrierName " & vbcrlf & _
		"from	we_items a join we_brands b " & vbcrlf & _
		"	on	a.brandid = b.brandid join we_models c " & vbcrlf & _
		"	on	a.modelid = c.modelid left join we_pndetails d " & vbcrlf & _
		"	on	a.partnumber = d.partnumber left outer join we_carriers e" & vbcrlf & _
		"	on	a.carrierid = e.id" & vbcrlf & _
		"where	a.brandid = '" & brandid & "' " & vbcrlf & _
		"	and a.typeid = '16' " & vbcrlf & _
		"	and a.hidelive = 0 " & vbcrlf & _
		"	and (a.inv_qty <> 0 or d.alwaysinstock = 1) " & vbcrlf & _
		"	and a.price_co is not null and a.price_co > 0" & vbcrlf

if carrierID > 0 then sql = sql & "	and a.carrierid = '" & carrierid & "'" & vbcrlf
if sortBy = "" or sortBy = "lh" then sql = sql & "ORDER BY A.price_CO" & vbcrlf
if sortBy = "hl" then sql = sql & "ORDER BY A.price_CO desc" & vbcrlf
if sortBy = "az" then sql = sql & "ORDER BY c.modelName" & vbcrlf
if sortBy = "za" then sql = sql & "ORDER BY c.modelName desc" & vbcrlf
if sortBy = "no" then sql = sql & "ORDER BY C.releaseYear desc, C.releaseQuarter desc" & vbcrlf
if sortBy = "on" then sql = sql & "ORDER BY C.releaseYear, C.releaseQuarter" & vbcrlf
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if

brandName = RS("brandName")
modelName = RS("modelName")
modelImg = RS("modelImg")
carrierName = RS("carrierName")

'=========================================================================================
Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
	oParam.CompareMode = vbTextCompare
	oParam.Add "x_brandID", brandID
	oParam.Add "x_brandName", brandName
	
'call redirectURL("pb", brandid, request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
'=========================================================================================	

dim SEtitle, SEdescription, SEkeywords, topText
select case brandid
	case "14"
		SEtitle = "Blackberry Wholesale Cell Phones | Unlocked Mobile Phones at Wholesale Prices"
		SEdescription = "CellularOutfitter.com offers a wide collection of discount Blackberry Cell Phones ?Buy Blackberry unlocked mobile Phones and other cell phone accessories at wholesale prices."
end select

if SEtitle = "" then SEtitle = brandName & " Cell Phones | Mobile Phones at Wholesale Prices"
if SEdescription = "" then SEdescription = "CellularOutfitter.com offers a wide collection of discount " & brandName & " Cell Phones ?Buy " & brandName & " Cell Phones and other cell phone accessories at wholesale prices."
if SEkeywords = "" then SEkeywords = brandName & " Accessories, " & brandName & " Cell Phone Accessories, " & brandName & " Unlocked Cell Phones"
if topText = "" then
	topText = "You've selected <b>" & brandName & " " & modelName & "</b> " & nameSEO(categoryName) & ". Find the right " & nameSEO(categoryName) & " for your <b>" & brandName & " " & modelName & "</b> at wholesale prices. No membership required! We offer the <a href=""http://www.cellularoutfitter.com/lowest-price.html"" class=""text-desc-link"">lowest prices online guaranteed</a>.</p>"
	topText = topText & "<p class=""static-content-font"">CellularOutfitter.com offers the best " & nameSEO(categoryName) & " for the " & brandName & " " & modelName & " at wholesale prices. We stand behind the quality of our products with an iron-clad 100% Quality Assurance Guarantee. We offer only quality " & brandName & " " & modelName & " " & nameSEO(categoryName) & " at wholesale prices. Shop around and see what we mean!"
end if

dim strBreadcrumb
strBreadcrumb = "Wholesale " & brandName & " Cell Phones"
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<style type="text/css">#sortOptions { display:none; }</style>
</noscript>
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td class="top-sublink-gray">
												<a class="top-sublink-gray" href="/" >Cell Phone Accessories</a>&nbsp;>&nbsp;
                                                <a class="top-sublink-gray" href="/wholesale-cell-phones.html">Wholesale Cell Phones</a>&nbsp;>&nbsp;
												<span class="top-sublink-blue"><%=strBreadcrumb%></span>
											</td>
										</tr>
										<tr>
											<td><img src="/images/spacer.gif" width="1" height="10"></td>
										</tr>
										<tr>
											<td width="100%" align="left" valign="top">
												<style>
													h1 {margin:10px;}
												</style>
												<%if carrierid > 0 then%>
                                                <h1><%=brandName & " " & carrierName%> Wholesale Cell Phones</h1>
                                                <%else%>
                                                <h1><%=brandName%> Wholesale Cell Phones</h1>
                                                <%end if%>
												
											</td>
										</tr>
										<tr>
											<td width="100%" align="left" valign="top" class="static-content-font">
												<%=topText%>
											</td>
										</tr>
										<tr>
											<td height="25" align="center" valign="middle"><img src="/images/hline4.jpg" width="750" height="5" border="0"></td>
										</tr>
                                        <tr>
                                        	<td id="sortOptions">
                                            	<div style="padding-right:30px; font-size:12px; float:right;">
                                                	<%
													curPage = request.ServerVariables("HTTP_X_REWRITE_URL")
													if instr(curPage,"?") > 0 then curPage = left(curPage,instr(curPage,"?")-1)
													%>
                                                    <select name="sortBy" onchange="window.location=this.value">
                                                        <option value="<%=curPage%>?sb=hl"<% if sortBy = "hl" then %> selected="selected"<% end if %>>Price High to Low</option>
                                                        <option value="<%=curPage%>?sb=lh"<% if sortBy = "" or sortBy = "lh" then %> selected="selected"<% end if %>>Price Low to High</option>
                                                        <option value="<%=curPage%>?sb=az"<% if sortBy = "az" then %> selected="selected"<% end if %>>Name A-Z</option>
                                                        <option value="<%=curPage%>?sb=za"<% if sortBy = "za" then %> selected="selected"<% end if %>>Name Z-A</option>
                                                        <option value="<%=curPage%>?sb=no"<% if sortBy = "no" then %> selected="selected"<% end if %>>Newest to Oldest</option>
                                                        <option value="<%=curPage%>?sb=on"<% if sortBy = "on" then %> selected="selected"<% end if %>>Oldest to Newest</option>
                                                    </select>
                                                </div>
                                                <div style="color:#000; padding:2px 5px 0px 0px; font-size:12px; float:right; font-weight:bold;">Sort by:</div>
                                            </td>
                                        </tr>
										<tr>
											<td>
												<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0" style="padding-bottom:20px;">
													<tr>
														<%
														dim altText, DoNotDisplay, RSkit, RSextra
														a = 0
														do until RS.eof
															%>
                                                            <td width="193" height="100%" align="center" valign="top">
                                                                <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td width="170" height="120" align="center" valign="middle" title="<%=altText%>">
                                                                            <div style="display:block; position: relative; z-index:0;">
                                                                                <a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc_CO"))%>.html"><img src="/productPics/thumb/<%=RS("itempic_CO")%>" border="0"></a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr><td height="8" align="center" valign="top"><img src="/images/spacer.gif" width="1" height="8" border="0"></td></tr>
                                                                    <tr>
                                                                        <td height="37" align="center" valign="top">
                                                                            <a class="static-content-font-link" href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc_CO"))%>.html" title="<%=altText%>"><%=RS("itemDesc_CO")%></a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr><td height="8" align="center" valign="top"><img src="/images/spacer.gif" width="1" height="8" border="0"></td></tr>
                                                                    <% if outOfStock then %>
                                                                    <tr>
                                                                        <td align="center" valign="bottom" class="red-price-small" style="padding-bottom:19px;">
                                                                            OUT OF STOCK
                                                                        </td>
                                                                    </tr>
                                                                    <% else %>
                                                                    <tr>
                                                                        <td align="center" valign="bottom" class="cat-font">
                                                                            Retail Price: <s><%=formatCurrency(RS("price_retail"))%></s>
                                                                        </td>
                                                                    </tr>
                                                                    <tr><td height="3" align="center" valign="top"><img src="/images/spacer.gif" width="1" height="3" border="0"></td></tr>
                                                                    <tr>
                                                                        <td align="center" valign="bottom" class="nevi-font-bold">
                                                                            Wholesale Price: <span class="red-price-small"><%=formatCurrency(RS("price_CO"))%></span>
                                                                        </td>
                                                                    </tr>
                                                                    <%
                                                                    end if
                                                                    %>
                                                                    <tr>
                                                                        <td align="center" valign="bottom" style="padding-top:5px;">
                                                                            <a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc_CO"))%>.html"><img src="/images/moreInfo.png" width="94" height="20" border="0" alt="More Info"></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
															<%
															a = a + 1
															if a = 4 then
																response.write "</tr><tr><td align=""center"" valign=""middle"" colspan=""4"" height=""25""><img src=""/images/hline4.jpg"" width=""750"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
																a = 0
															end if
															RS.movenext
														loop
														if a = 1 then
															response.write "<td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td>" & vbcrlf
														elseif a = 2 then
															response.write "<td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td>" & vbcrlf
														elseif a = 3 then
															response.write "<td width=""170"">&nbsp;</td>" & vbcrlf
														end if
														%>
													</tr>
												</table>
											</td>
										</tr>
										<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
									</table>
<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->
