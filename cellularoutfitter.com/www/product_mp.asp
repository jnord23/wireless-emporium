<%
response.buffer = true
response.Expires = -1
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
if instr(request.ServerVariables("HTTP_X_REWRITE_URL"),".aspl") > 0 then
	call CloseConn(oConn)
	response.Status = "301 Moved Permanently"
	response.AddHeader "Location", replace(request.ServerVariables("HTTP_X_REWRITE_URL"),".aspl",".html")
end if

dim promoBigBanner : promoBigBanner = "penPromo.jpg"
dim promoSmallBanner : promoSmallBanner = "penPromoSmall.jpg"

'dim promoBigBanner2 : promoBigBanner2 = "stickyPromo.jpg"
'dim promoSmallBanner2 : promoSmallBanner2 = "stickyPromoSmall.jpg"

pageTitle = "product.asp"
randomize
verifyHumanCode = int(rnd*61565161)+1
if left(verifyHumanCode,1) < 4 then
	vh_letter = "a"
elseif left(verifyHumanCode,1) < 7 then
	vh_letter = "b"
else
	vh_letter = "b"
end if
if right(verifyHumanCode,1) < 4 then
	vh_num = 1
elseif left(verifyHumanCode,1) < 7 then
	vh_num = 2
else
	vh_num = 3
end if

showReviewPopup = false
utm_campaign = request("utm_campaign")
if lcase(utm_campaign) = "co14dayreview" then showReviewPopup = true

set fso = CreateObject("Scripting.FileSystemObject")
Set Jpeg = Server.CreateObject("Persits.Jpeg")
Set vhImg = Server.CreateObject("Persits.Jpeg")
Jpeg.Quality = 100
Jpeg.Interpolation = 1

Jpeg.New 60, 30, &HFFFFFF
vhImg.Open server.MapPath("/images/verifyHuman/" & vh_letter & ".gif")
Jpeg.Canvas.DrawImage 0, 0, vhImg
vhImg.Open server.MapPath("/images/verifyHuman/" & vh_num & ".gif")
Jpeg.Canvas.DrawImage 30, 0, vhImg
if not fso.fileExists(server.MapPath("/images/verifyHuman/results/" & verifyHumanCode & ".jpg")) then
	Jpeg.Save server.MapPath("/images/verifyHuman/results/" & verifyHumanCode & ".jpg")
end if

abUser = prepInt(request.Cookies("abProductPage"))
if abUser = 0 then
	randomize
	abUser = int(rnd*2)+1
	response.Cookies("abProductPage") = abUser
end if
if abUser = 2 then
	curPageURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	curPageURL = mid(curPageURL,instr(curPageURL,itemID)+len(itemID))
	newPageURL = "/p-" & itemID & "-new" & curPageURL
	'PermanentRedirect(newPageURL)
end if

dim itemid, productPage, curURL
dim curPageName : curPageName = "Product Page"
productPage = 1
itemid = prepInt(request.querystring("itemid"))
dim testVersion : testVersion = prepVal(request.QueryString("testVersion"))
curURL = prepVal(request.QueryString("curURL"))
if itemid = 0 then server.transfer("404error.asp")
dim musicSkins : musicSkins = 0

if itemid => 1000000 then
	musicSkins = 1
	noindex = 1
end if

dim modelID, typeID

function getRatingAvgStarBig(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""/images/product/review/star-empty.png"" border=""0"" />" & vbcrlf & _
					"<img src=""/images/product/review/star-empty.png"" border=""0"" />" & vbcrlf & _
					"<img src=""/images/product/review/star-empty.png"" border=""0"" />" & vbcrlf & _
					"<img src=""/images/product/review/star-empty.png"" border=""0"" />" & vbcrlf & _
					"<img src=""/images/product/review/star-empty.png"" border=""0"" />" & vbcrlf
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""/images/product/review/star-full.png"" border=""0"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""/images/product/review/star-full.png"" border=""0"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""/images/product/review/star-empty.png"" border=""0"" /> "
			end if
		next		
	end if
	
	getRatingAvgStarBig = strRatingImg
end function


function getRatingAvgStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarFull.gif"" border=""0"" width=""14"" height=""13"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarHalf.gif"" border=""0"" width=""8"" height=""13"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function

arrVideo = null
sql = "exec sp_getVideosByItemID " & itemid & ", 2"
session("errorSQL") = sql
arrVideo = getDbRows(sql)

curVideoLink = ""
if not isnull(arrVideo) then
	for nRow = 0 to ubound(arrVideo,2)
		if curVideoLink <> arrVideo(0, nRow) then
			curVideoLink = arrVideo(0, nRow)
			if vID = "" then
				vID = arrVideo(2, nRow)
			else
				vID = vID & "," & arrVideo(2, nRow)
			end if
		end if
	next
end if


if musicSkins = 1 then 'hack in original pricing as constant .. currently no blow-out support for this table [knguyen/20110602]
	SQL = 	"select cast('False' as bit) as customize, '' as itemDimensions, cast('True' as bit) as alwaysInStock, '' as POINT1, '' as POINT2, '' as POINT3, '' as POINT4, '' as POINT5, '' as POINT6, '' as POINT7, '' as POINT8, '' as POINT9, '' as POINT10, " &_
			"'' as COMPATIBILITY, '' as itemLongDetail_CO, 0 as HandsfreeType, null as ItemKit_NEW, 'musicSkins' as vendor, '' as UPCCode, 'new' as Condition, " &_
			"1 as NoDiscount, c.defaultImg, c.sampleImg, c.musicSkinsID as partNumber, c.brandID, 20 as typeID, c.image as itemPic, c.image as itemPic_co, " &_
			"100 as inv_qty, c.id as itemID, 'false' as hideLive, c.price_co, c.msrp as price_retail, " &_
			"c.brand + ' ' + c.model + ' ' + c.artist + ' ' + c.designName + ' Music Skins' as itemDesc_co, b.modelID, 20 as typeID, b.modelName, b.modelImg, c.brand as brandName, " &_
			"'music skins' as typeName, 'OriginalPrice' as [ActiveItemValueType], c.price_we as [OriginalPrice], c.preferredImg from we_items_musicSkins c left join we_models b " &_
			"on c.modelID = b.modelID where c.id = '" & itemID & "'"
else
	SQL = "exec sp_getItemForPdp 2, " & itemID
end if
'response.write "<pre>" & sql & "</pre>"
set RS = oConn.execute(SQL)

if not RS.eof then
	dim partnumber, brandID, itemDesc_CO, itempic_CO, price_retail, price_CO, itemLongDetail_CO, KIT, COMPATIBILITY, download_URL, download_TEXT, myPartNumber
	dim modelName, modelImg, brandName, categoryName, handsFreeType, upcCode, condition, noDiscount, defaultImg, sampleImg, inv_qty, hideLive
	dim ActiveItemValueType, OriginalPrice, preferredImg
	dim POINT(10)
	itemDimensions = RS("itemDimensions")	
	if isnull(rs("customize")) then customize = False else customize = rs("customize")
	if isnull(rs("alwaysInStock")) then alwaysInStock = False else alwaysInStock = rs("alwaysInStock")
	if musicSkins = 1 then
		partnumber = "CO" & cDbl(itemid) + 250
		COMPATIBILITY = RS("COMPATIBILITY")
		itemLongDetail_CO = RS("itemLongDetail_CO")
		handsFreeType = RS("HandsfreeType")
		KIT = RS("ItemKit_NEW")
		vendor = RS("vendor")
		upcCode = RS("UPCCode")
		condition = RS("Condition")
		noDiscount = RS("NoDiscount")
		defaultImg = RS("defaultImg")
		sampleImg = RS("sampleImg")
		myPartNumber = RS("partNumber")
		brandID = RS("brandID")
		typeID = RS("typeID")
		itempic_CO = RS("itempic_CO")
		itempic = RS("itempic")
		prodQty = RS("inv_qty")
		hideLive = RS("hideLive")
		price_CO = RS("price_CO")
		price_retail = RS("price_retail")
		itemDesc_CO = RS("itemDesc_co")
		typeID = 20
		modelID = RS("modelID")
		modelName = RS("modelName")
		modelImg = RS("modelImg")
		brandName = RS("brandName")
		prodTypeName = RS("typeName")
		ActiveItemValueType = RS("ActiveItemValueType")
		OriginalPrice = RS("OriginalPrice")
		preferredImg = RS("preferredImg")
		partNumber = myPartNumber
		itemDesc_CO = insertDetails(itemDesc_CO)
		for a = 1 to 10
			POINT(a) = RS("POINT" & a)
		next
		productColor = ""
	else
		prodQty = rs("masterQty")
		masterID = rs("masterID")
		masterDesc = rs("masterDesc_CO")
		brandID = RS("brandID")
		modelID = RS("modelID")
		typeID = RS("typeID")
		vendor = rs("vendor")
		myPartNumber = RS("partnumber")
		itemDesc_CO = RS("itemDesc_CO")
		itempic_WE = RS("itempic")
		itempic_CO = RS("itempic_CO")
		price_retail = RS("price_retail")
		price_CO = RS("price_CO")
		KIT = RS("ItemKit_NEW")
		COMPATIBILITY = RS("COMPATIBILITY")
		itemLongDetail_CO = RS("itemLongDetail_CO")
		for a = 1 to 10
			POINT(a) = RS("POINT" & a)
		next
		noDiscount = rs("noDiscount")
		modelName = RS("modelName")
		modelImg = RS("modelImg")
		brandName = RS("brandName")
		categoryName = RS("typeName")
		partnumber = "CO" & cDbl(itemid) + 250
		productColor = rs("color")
		partNumber = myPartNumber
		itemDesc_CO = cleanMP(insertDetails(itemDesc_CO))
		master = rs("master")
	end if
	itemDesc_CO = replace(replace(server.HTMLEncode(itemDesc_CO),"&amp;lt;","<"),"&amp;gt;",">")
'	itemLongDetail_CO = replace(replace(server.HTMLEncode(itemLongDetail_CO),"&amp;lt;","<"),"&amp;gt;",">")
	itemLongDetail_CO = replace(replace(itemLongDetail_CO,"&lt;","<"),"&gt;",">")

	for a = 1 to 10
		POINT(a) = replace(replace(server.HTMLEncode(prepVal(POINT(a))),"&amp;lt;","<"),"&amp;gt;",">")
	next
else
	server.transfer("404error.asp")
end if

if prepInt(masterID) > 0 then
	if prepInt(masterID) <> prepInt(itemID) then
		setCanonical = "/p-" & prepInt(masterID) & "-" & formatSEO(masterDesc) & ".html"
	end if
end if

if left(myPartNumber,3) = "WCD" then 
	call CloseConn(oConn)
	response.Redirect("/")
end if

setModelID = modelID
dim useUvp : useUvp = ""
if typeID = 1 then useUvp = "Guaranteed to work upon delivery - you can count on our batteries"
if typeID = 2 then useUvp = "Keep your phone powered up while on the go"
if typeID = 3 then useUvp = "Shields your phone from scratches, dings and bumps"
if typeID = 5 then useUvp = "Hands-free operation for maximum convenience"
if typeID = 6 then useUvp = "Holds your device safely and securely in place"
if typeID = 7 then useUvp = "Provides heavy duty protection w/ a timeless design"
if typeID = 8 then useUvp = "All of the best accessories for your favorite devices"
if typeID = 16 then useUvp = "The same great devices at a fraction of the cost!"
if typeID = 18 then useUvp = "Easily prevent smashed, scratched or damaged screens!"
if typeID = 24 then useUvp = "Show your phone off while protecting your pricey device"

if instr(modelName,"universal") > 0 then
	if isnull(session("curModelID")) or len(session("curModelID")) < 1 then session("curModelID") = modelID
	if session("curModelID") <> modelID then modelID = session("curModelID")
end if

dim contentText, bottomText

dim strBreadcrumb
strBreadcrumb = brandName & " " & modelName & " " & singularSEO(categoryName)

set fso = CreateObject("Scripting.FileSystemObject")
dim jpeg : set jpeg = Server.CreateObject("Persits.Jpeg")

itemImgPath = server.MapPath("/productpics/big/" & itempic_CO)
if musicSkins = 1 then
	if isnull(preferredImg) then																					
		if hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsLarge", itempic_CO) then
			sql = "update we_items_musicSkins set preferredImg = 1 where id = " & itemID
			session("errorSQL") = sql
			oConn.execute(sql)
			useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
		elseif hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault", defaultImg) then
			sql = "update we_items_musicSkins set preferredImg = 2 where id = " & itemID
			session("errorSQL") = sql
			oConn.execute(sql)
			useImg = "http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/" & defaultImg
		else
			useImg = "/productPics/thumb/imagena.jpg"
		end if
	elseif preferredImg = 1 then
		useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
	elseif preferredImg = 2 then
		useImg = "/productpics/musicSkins/musicSkinsDefault/" & defaultImg
	else
		useImg = "/productPics/thumb/imagena.jpg"
	end if
elseif instr(myPartNumber,"DEC-SKN") > 0 then
	useImg = "/weImages/big/" & itempic_WE
else
	if not fso.FileExists(itemImgPath) then
		if not fso.FileExists(replace(itemImgPath,"\big\","\big\zoom\")) then
			useImgID = 1
			useImg = "/productPics/big/imagena.jpg"
		else
			useImgID = 2
			useImg = "/productPics/big/zoom/" & itempic_CO
		end if
	else
		useImgID = 3
		useImg = "/productPics/big/" & itempic_CO
	end if
end if

bZoom = false
objItemImgFull = "<img itemprop=""image"" src=""http://www.cellularoutfitter.com/" & useImg & """ alt=""" & itemImgPath & "-" & itemDesc_CO & """ border=""0"" id=""imgLarge"" height=""300"" />"
'if fso.fileExists(server.MapPath("/productpics/big/zoom/" & itempic_CO)) then 
	bZoom = true
	objItemImgFull = "<img onclick=""showZoom();"" style=""cursor:pointer;"" itemprop=""image"" src=""http://www.cellularoutfitter.com/" & useImg & """ alt=""" & itemDesc_CO & """ border=""0"" id=""imgLarge"" height=""300"" />"
	objItemImgFull = objItemImgFull & "<div align=""center"" style=""padding-top:5px;""><a href=""javascript:showZoom();""><img src=""/images/product/co-pdp-zoom-icon.png"" border=""0"" /></a></div>"
'end if
	
sql = 	"select c.brandName, b.modelName from we_items a left join we_models b on a.modelID = b.modelID left join we_brands c on b.brandID = c.brandID where a.partNumber = '" & myPartNumber & "'" &_
		"union " &_
		"select c.brandName, b.modelName from we_relatedItems a left join we_Models b on a.modelid = b.modelID left join we_Brands c on b.brandID = c.brandID where itemID = " & itemID & " order by c.brandName, b.modelName"
session("errorSQL") = sql
set compRS = oConn.execute(sql)

compatLap = 0
compatList = ""
do while not compRS.EOF
	compatLap = compatLap + 1
	if compatLap > 6 then compatLap = 1
	if compatLap < 4 then bgColor = "#fff" else bgColor = "#ebebeb"
	compatList = compatList & "<div style='float:left; width:200px; font-size:11px; padding-right:15px; background-color:" & bgColor & "; height:15px;'>" & compRS("brandName") & " " & compRS("modelName") & "</div>"
	compRS.movenext
loop

noLeftSide = 1

'This section is only for the "Add Screen Protection" up-sell, but must come after 'modelID' has been set above
SQL = "select top 1 a.itemID, a.itemDesc_CO, a.itemPic_CO, a.price_co from we_Items a " &_
	"left join we_Items b " &_
	"on a.PartNumber = b.PartNumber " &_
	"and b.master = 1 " &_
	"where a.modelID = " & setModelID & " and " &_
	"a.typeID = 18 and " &_
	"b.inv_qty > 0 and " &_
	"a.price_co > 0 and " &_
	"a.hideLive = 0 and " &_
	"(a.PartNumber like '%-01' or " &_
	"a.PartNumber like '%-02' or " &_
	"a.PartNumber like '%-04') " &_
	"order by a.PartNumber"
session("errorSQL") = SQL
screenProtectorSQL = sql
set addsp_rs = Server.CreateObject("ADODB.Recordset")
addsp_rs.open SQL, oConn, 0, 1

addsp_itemID = -1
if not addsp_rs.eof then
	addsp_itemID = addsp_rs("itemID")
	addsp_itemDesc_CO = addsp_rs("itemDesc_CO")
	addsp_itemPic_CO = addsp_rs("itemPic_CO")
	addsp_price_co = addsp_rs("price_co")	
end if
AddSP_RS.close

isUnlockedPhone = 0
if instr(lcase(itemDesc_CO),"unlocked") > 0 and typeID = 16 then isUnlockedPhone = 1
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<link rel="stylesheet" type="text/css" href="/includes/css/pdp.css" />
<div>
    <div id="promoBanner1" style="margin:10px 0px 10px 0px; cursor:pointer;" onclick="checkPromo('yes')"><img src="/images/banners/<%=promoBigBanner%>" border="0" /></div>
    <!--<div id="promoBanner2" style="margin:10px 0px 10px 0px; cursor:pointer;" onclick="checkPromo('yes')"><img src="/images/banners/<%=promoBigBanner2%>" border="0" /></div>-->
</div>
<!-- rich snippets-->
<div itemscope itemtype="http://data-vocabulary.org/Product">
    <span itemprop="condition" content="new"></span>
    <span itemprop="color" content="<%=productColor%>"></span>
    <span itemprop="price" content="<%=price_co%>"></span>
    <span itemprop="brand" content="<%=brandName%>"></span>
    <span itemprop="model" content="<%=modelName%>"></span>
    
<table width="100%" border="0" bordercolor="#FF0000" cellspacing="0" cellpadding="0" align="center">
	<!--#include virtual="/includes/asp/inc_breadcrumbs.asp"-->
    <tr>
    	<td style="padding-top:10px; width:900px;" valign="top">
            <div style="width:300px; float:left;">
            	<div style="float:left;position:relative" id="mainProdPic_<%=useImgID%>">
                	<%=objItemImgFull%>
				<% 
				sql = 	"select top 10 a.itemID, DiscountPrice, DiscountPercent, OriginalPrice, a.price_Retail from we_Items a " &_
						"left join " &_
						"( " &_
						"	select v.SiteId, v.ItemId, v.OriginalPrice, v.DiscountPrice, v.DiscountPercent " &_
						"	from ItemValue v " &_
						"	where v.IsActive = 1 " &_
						") as vt  on vt.ItemId = a.ItemId " &_
						"where SiteId = " & 2 &_
						"and a.itemID = " & itemID &_
						"and OriginalPrice > price_" & "CO"
				set sale = Server.CreateObject("ADODB.Recordset")
				set sale = oConn.execute(sql)
				if not sale.eof then
					isOnSale = true
				end if
				sale.close
				
				if isOnSale then
					%><div style="display:block;position:absolute;top:220px;left:0px;width:205px;height:30px;z-index:3;background:url(/images/co-sale-icon-lrg.png)"></div><%
				end if
				if customize then
					%><div onclick="window.location='/custom/?aspid=<%=itemID%>'" style="display:block;position:absolute;top:50px;right:0px;width:132px;height:132px;z-index:3;background:url(/images/custom/co-customize-lrg5.png); cursor:pointer;"></div><%
				end if
				%></div>
                <div style="width:300px; height:70px; padding-top:10px; float:left;">
                	<%
					curPic = mid(useImg,instrrev(useImg,"/")+1)
					if fso.fileExists(server.MapPath(useImg)) then
						if not fso.fileExists(server.MapPath("/productPics/alts/thumbs/" & curPic)) then
							jpeg.Open Server.MapPath(useImg)
							jpeg.Height = 60
							jpeg.Width = 60
							jpeg.Save Server.MapPath("/productPics/alts/thumbs/" & curPic)
						end if
					end if

					if vID <> "" then
					%>
					<a href="javascript:ajaxProductVideo('<%=vID%>',0);" title="Play Video" style="float:left; border:1px solid #CCC; margin:0px 3px 3px 0px;"><img src="/images/pdp/ico-video.png" width="40" height="40" alt="Play Video" /></a>
                    <%end if%>
                    
					<div style="float:left; cursor:pointer; border:1px solid #CCC; margin:0px 3px 3px 0px;" onmouseover="fnPreviewImage('<%=useImg%>')" class="altImg"><img src="/productPics/alts/thumbs/<%=curPic%>" width="40" height="40" /></div>
                    <%
					for iCount = 0 to 7
                        path = "/weImages/AltViews/" & replace(itempic_WE,".jpg","-" & iCount & ".jpg")
                        src = replace(path,".jpg","_thumb.jpg")
						if fso.FileExists(Server.MapPath(path)) and not fso.FileExists(Server.MapPath(src)) then
							jpeg.Open Server.MapPath(path)
							jpeg.Height = 40
							jpeg.Width = 40
							jpeg.Save Server.MapPath(src)
						end if
                        if fso.FileExists(Server.MapPath(path)) and fso.FileExists(Server.MapPath(src)) then
                    %>
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin:0px 3px 3px 0px;" onmouseover="fnPreviewImage('<%=path%>')" class="altImg"><img src="<%=src%>" width="40" height="40" /></div>
                    <%
                        else
							if not fso.FileExists(Server.MapPath(path)) then
								response.Write("<!-- can't find p:" & Server.MapPath(path) & " -->")
							else
								response.Write("<!-- can't find s:" & Server.MapPath(src) & " -->")
							end if
						end if
                    next
					
					curPic = ""
					if typeID = 17 then curPic = "gg_atl_image1.jpg"
					if typeID = 20 then curPic = "ms_altview1.jpg##ms_altview2.jpg"
					if typeID = 19 then curPic = "CO-DecalSkin2.jpg"
					if curPic <> "" then
						picArray = split(curPic,"##")
						for i = 0 to ubound(picArray)
							curPic = picArray(i)
							if not fso.fileExists(server.MapPath("/productPics/alts/thumbs/" & curPic)) then
								jpeg.Open Server.MapPath("/altPics/" & curPic)
								jpeg.Height = 60
								jpeg.Width = 60
								jpeg.Save Server.MapPath("/productPics/alts/thumbs/" & curPic)
							end if
					%>
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin-right:3px;" onmouseover="fnPreviewImage('/altPics/<%=curPic%>')" class="altImg"><img src="/productPics/alts/thumbs/<%=curPic%>" width="60" height="60" /></div>
                    <%
						next
					end if
					%>
                </div>
            </div>
            <div id="buyNow1" style="float:left; margin-left:20px; width:420px; padding-left:20px;">
            	<div id="itemDesc_1" style="padding-bottom:10px;"><span itemprop="name"><h1><%=itemDesc_CO%></h1></span></div>
                <div class="tb" style="color:#999; font-size:12px; padding-bottom:10px;">
                    <div style="float:left;">Item ID: <%=itemID%></div>
                </div>                
                <!-- rich snippet review -->
                <span itemprop="review" itemscope itemtype="http://data-vocabulary.org/Review-aggregate">
                <div style="padding-bottom:10px; border-bottom:1px dotted #CCC;">
                	<%
					sql = "exec getReviews 2, " & itemid & ", 'NO'"
					session("errorSQL") = sql
					set objRsReviews = oConn.execute(sql)
					
					dim arrStarCount(4) : arrStarCount(0) = cint(0) : arrStarCount(1) = cint(0) : arrStarCount(2) = cint(0) : arrStarCount(3) = cint(0) : arrStarCount(4) = cint(0)
					avgRating = 0.0
					reviewCnt = 0
					nRecommend = 0
					prodReviewHeight = 500
					if not objRsReviews.eof then
						do until objRsReviews.eof
							reviewCnt = reviewCnt + 1
							starRating = objRsReviews("starRating")
							avgRating = cdbl(avgRating) + cdbl(starRating)
							if objRsReviews("isRecommend") then nRecommend = nRecommend + 1
							
							select case cint(starRating)
								case 1 : arrStarCount(0) = cint(arrStarCount(0)) + 1
								case 2 : arrStarCount(1) = cint(arrStarCount(1)) + 1
								case 3 : arrStarCount(2) = cint(arrStarCount(2)) + 1
								case 4 : arrStarCount(3) = cint(arrStarCount(3)) + 1
								case 5 : arrStarCount(4) = cint(arrStarCount(4)) + 1
							end select
							objRsReviews.movenext
						loop
						objRsReviews.movefirst
						avgRating = avgRating / reviewCnt
						response.write getRatingAvgStar(avgRating) & " &nbsp; <span itemprop=""rating"">" & formatnumber(avgRating,1) & "</span> &nbsp; (<span itemprop=""count"">" & formatnumber(reviewCnt,0) & "</span> Reviews)<br>"
						response.write "<a style=""font-size:11px; text-decoration:underline;"" onclick=""showReview()"" href=""#prodTabs"">Read All Reviews</a> &nbsp;/&nbsp; <a style=""font-size:11px; text-decoration:underline;"" href=""#"" onclick=""showReviewPopup()"">Write A Review</a>"
					else
						prodReviewHeight = 280
						response.write getRatingAvgStar(avgRating) & " &nbsp; " & formatnumber(avgRating,1) & " &nbsp; (" & formatnumber(reviewCnt,0) & " Reviews)<br>"
						response.write "<a style=""font-size:11px; text-decoration:underline;"" href=""#"" onclick=""showReviewPopup()"">Be the first to review this product!</a>"
					end if
					%>
                </div>
                </span>
                <!--// rich snippet review -->
                <!-- rich snippet product details -->
                <span itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">
                <meta itemprop="currency" content="USD" />
                <!--#include virtual="/includes/template/product/buyNow_b.asp"-->
                </span>
            </div>
            <%
			if recentItemCnt > 0 then sideHeight = 999 else sideHeight = 972
			sideHeight = sideHeight + (recentItemCnt*192)
			%>
            <div id="relatedItems_new" style="float:left; width:600px;">
                <div class="strandsRecs fl" tpl="PROD-4" item="<%=itemID%>"></div>
                <div class="strandsRecs fl" tpl="prod_1" item="<%=itemID%>"></div>
            </div>
            <div id="detailTabs" style="float:left; border:1px solid #ccc; margin-top:57px; position:relative; width:780px;">
            	<div style="position:absolute; top:-27px; left:-1px;" id="prodTabs">
                	<div style="float:left;"><img id="tabHeaderLeft1" src="/images/backgrounds/productTab_on_left.gif" border="0" width="28" height="27" /></div>
                    <div id="tabHeader1" class="prodTab_on" onclick="togglePdpTab(1,4)">Product Description</div>
                    <div style="float:left;"><img id="tabHeaderRight1" src="/images/backgrounds/productTab_on_right.gif" border="0" width="28" height="27" /></div>
                    
                    <div style="float:left;"><img id="tabHeaderLeft2" src="/images/backgrounds/productTab_off_left.gif" border="0" width="28" height="27" /></div>
                    <div id="tabHeader2" class="prodTab_off" onclick="togglePdpTab(2,4)">Compatibility</div>
                    <div style="float:left;"><img id="tabHeaderRight2" src="/images/backgrounds/productTab_off_right.gif" border="0" width="28" height="27" /></div>
                    
                    <div style="float:left;"><img id="tabHeaderLeft3" src="/images/backgrounds/productTab_off_left.gif" border="0" width="28" height="27" /></div>
                    <div id="tabHeader3" class="prodTab_off" onclick="togglePdpTab(3,4)">Reviews</div>
                    <div style="float:left;"><img id="tabHeaderRight3" src="/images/backgrounds/productTab_off_right.gif" border="0" width="28" height="27" /></div>
                    
                    <div style="float:left;"><img id="tabHeaderLeft4" src="/images/backgrounds/productTab_off_left.gif" border="0" width="28" height="27" /></div>
                    <div id="tabHeader4" class="prodTab_off" onclick="togglePdpTab(4,4)">Q &amp; A</div>
                    <div style="float:left;"><img id="tabHeaderRight4" src="/images/backgrounds/productTab_off_right.gif" border="0" width="28" height="27" /></div>                    
                </div>
                <div style="padding:10px; color:#333; font-size:12px;" id="tabBody1">
					<div><span itemprop="description"><%=itemLongDetail_CO%></span></div>
                    <%
					for i = 1 to 10
						if prepVal(POINT(i)) <> "" then
					%>
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;"><%=POINT(i)%></span></div>
                    <%
						end if
					next
					
					if prepVal(itemDimensions) <> "" then
					%>
					<div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Product Dimensions: </span><span style="font-size:13px; font-weight:bold;"><%=itemDimensions%></span></div>
					<%
					end if
					%>					
                    <div id="contentText" style="padding-top:5px;"><%=SeBottomText%></div>
                </div>
                <div style="padding:10px; color:#333; font-size:12px; display:none; overflow:auto; height:280px;" id="tabBody2">
					<%=compatList%>
                </div>
                <div style="padding:10px; color:#333; font-size:12px; display:none; overflow:auto; height:<%=prodReviewHeight%>px;" id="tabBody3">
				<%
				if objRsReviews.EOF then
				%>
				    <div style="color:#222; font-size:13px;">
                        <div style="font-size:18px;"><%=itemDesc_CO%></div>
                        <div style="display:table; padding-top:5px;">
                            <div style="float:left;"><%=getRatingAvgStarBig(avgRating)%></div>
                            <div style="float:left; padding-left:10px; font-size:16px; font-weight:bold;"><%=formatnumber(avgRating,1)%></div>
                            <div style="float:left; padding:2px 0px 0px 10px;">(based on <%=formatnumber(reviewCnt,0)%> reviews)</div>
                        </div>
                        <div style="display:table; margin:20px 0px 20px 0px;">
                            <div style="float:left; padding:0px 0px 0px 40px; width:329px; border-right:1px solid #ccc;">
                                <div style="float:left; width:100%; font-size:16px; font-weight:bold;">Overall Rating Totals:</div>
                                <div style="float:left; width:100%; padding-top:10px;">
                                	<%
									for idx = ubound(arrStarCount) to 0 step -1
									%>
	                            	<div style="float:left; width:100%; padding-top:3px;">
                                        <div style="float:left; color:#034076; text-decoration:underline;"><%=(idx+1)%> star</div>
                                        <div style="float:left; margin-left:5px; width:200px;">
	                                        <div style="float:left; background-color:#e6e6e6; width:100%; height:15px;"></div>
                                        </div>
                                        <div style="float:left; margin-left:5px;">0</div>
                                    </div>
                                    <%
									next
									%>
								</div>
                            </div>
                            <div style="float:left; width:320px; padding:0px 0px 0px 50px;">
                                <div style="float:left; width:100%; font-size:16px; font-weight:bold; padding-top:20px;">Be the first to review this product!</div>
                                <div style="float:left; width:100%; padding-top:10px;">
                                    <img src="/images/product/review/cta-write-review.png" border="0" alt="write a review" style="cursor:pointer;" onclick="showReviewPopup()" />
                                </div>
                            </div>
                        </div>
					</div>
				<%
				else
				%>
                	<div style="color:#222; font-size:13px;">
                        <div style="font-size:18px;"><%=itemDesc_CO%></div>
                        <div style="display:table; padding-top:5px;">
                            <div style="float:left;"><%=getRatingAvgStarBig(avgRating)%></div>
                            <div style="float:left; padding-left:10px; font-size:16px; font-weight:bold;"><%=formatnumber(avgRating,1)%></div>
                            <div style="float:left; padding:2px 0px 0px 10px;">(based on <%=formatnumber(reviewCnt,0)%> reviews)</div>
                        </div>
                        <div style="display:table; margin:20px 0px 20px 0px;">
                        	<div style="float:left; padding:0px 0px 0px 40px; width:329px; border-right:1px solid #ccc;">
                            	<div style="float:left; width:100%; font-size:16px; font-weight:bold;">Overall Rating Totals:</div>
                            	<div style="float:left; width:100%; padding-top:10px;">
                                	<%
									for idx = ubound(arrStarCount) to 0 step -1
										ratingPerc = round(arrStarCount(idx) / reviewCnt * 100,0)
									%>
	                            	<div style="float:left; width:100%; padding-top:3px;">
                                        <div style="float:left; color:#034076; text-decoration:underline;"><%=(idx+1)%> star</div>
                                        <div style="float:left; margin-left:5px; width:200px;">
	                                        <div style="float:left; background-color:#034076; width:<%=ratingPerc%>%; height:15px;"></div>
	                                        <div style="float:left; background-color:#e6e6e6; width:<%=(100-ratingPerc)%>%; height:15px;"></div>
                                        </div>
                                        <div style="float:left; margin-left:5px;"><%=arrStarCount(idx)%></div>
                                    </div>
                                    <%
									next
									%>
                                </div>
                            </div>
                        	<div style="float:left; width:320px; padding:0px 0px 0px 50px;">
                            	<div style="float:left; width:100%; font-size:16px; font-weight:bold;">Customer Recommendation:</div>
                            	<div style="float:left; width:100%; padding-top:10px;">
	                            	<div style="float:left; border:1px solid #ccc; background-color:#f7f7f7; border-radius:5px; padding:5px 10px 5px 10px; color:#034076; font-size:30px;"><%=formatpercent(nRecommend / reviewCnt, 0)%></div>
	                            	<div style="float:left; padding:5px 0px 0px 10px;">
                                    	<div><%=nRecommend%> out of <%=reviewCnt%> reviewers</div>
                                    	<div>would recommend this.</div>
                                    </div>
                                </div>
                            	<div style="float:left; width:100%; padding-top:10px;">
                                	<img src="/images/product/review/cta-write-review.png" border="0" alt="write a review" style="cursor:pointer;" onclick="showReviewPopup()" />
                                </div>
                            </div>
                        </div>
                        <div style="width:100%; display:table; padding:5px 0px 5px 0px; margin:20px 0px 20px 0px; border-top:1px solid #ccc; border-bottom:1px solid #ccc; background-color:#f7f7f7;">
                            <div style="float:left; padding:2px 0px 0px 10px; font-weight:bold;">Sort By:</div>
                            <div style="float:left; padding-left:10px;">
                                <select name="cbSort" onchange="onReviewSort(this.value)">
                                    <option value="NO">Newest</option>
                                    <option value="ON">Oldest</option>
                                    <option value="BR">Best Rating</option>
                                </select>
                            </div>
                            <div style="float:left; padding:2px 0px 0px 30px; font-weight:bold;">Share: </div>
                            <div style="float:left; padding-left:10px;">
	                            <a style="vertical-align:bottom;" href="http://twitter.com/share" data-url="<%=canonicalURL%>" class="twitter-share-button" data-count="none" data-via="CellOutfitter">Tweet</a>
                            </div>
							<div style="float:left; margin-left:5px;">
								<fb:like href="<%=canonicalURL%>" send="false" layout="button_count" show_faces="false" font="arial"></fb:like>
                            </div>
                        </div>
                        <div id="reviewBody">
                	<%
					bgColor = "#fff"
					do while not objRsReviews.EOF
						reviewID = objRsReviews("id")
						reviewTitle = objRsReviews("reviewTitle")
						reviewerNickName = objRsReviews("reviewerNickName")
						reviewerLocation = objRsReviews("reviewerLocation")
						reviewerAge = objRsReviews("reviewerAge")
						reviewerGender = objRsReviews("reviewerGender")
						reviewerType = objRsReviews("reviewerType")
						starRating = objRsReviews("starRating")
						ownershipPeriod = objRsReviews("ownershipPeriod")
						entryDate = objRsReviews("entryDate2")

						isRecommend = objRsReviews("isRecommend")
						isProPrice = objRsReviews("isProPrice")
						isProDesign = objRsReviews("isProDesign")
						isProWeight = objRsReviews("isProWeight")
						isProProtection = objRsReviews("isProProtection")
						isConFit = objRsReviews("isConFit")
						isConRobust = objRsReviews("isConRobust")
						isConMaintenance = objRsReviews("isConMaintenance")
						isConWeight = objRsReviews("isConWeight")
						thumbsUpCount = objRsReviews("thumbsUpCount")
						thumbsDownCount = objRsReviews("thumbsDownCount")
						
						otherCon = objRsReviews("otherCon")
						otherPro = objRsReviews("otherPro")
						reviewBody = objRsReviews("reviewBody")
						%>
                        <div style="display:table; border-bottom:1px solid #ccc; margin:10px 0px 10px 0px; padding-bottom:10px;">
                        	<div style="float:left; width:250px;">
                                <div style="float:left; background-color:#f7f7f7; width:230px; padding:10px;">
                                    <div style="float:left; width:100%;">
                                        <div style="float:left; padding-top:2px;"><img src="/images/product/review/ico-head.png" border="0" /></div>
                                        <div style="float:left; padding-left:5px;">
                                            <div style="font-weight:bold; color:#034076;"><%=reviewerNickName%></div>
                                            <div><%=reviewerLocation%></div>
                                        </div>
                                    </div>
                                    <div style="float:left; width:100%; padding-top:15px;">
                                    	<%if isRecommend then%>
                                        <div style="float:left;"><img src="/images/product/review/recommend-check.png" border="0" alt="I recommend this!" /></div>
                                        <div style="float:left; padding-left:3px; font-style:italic;"><span style="font-size:15px; font-weight:bold;">Yes,</span> I recommend this!</div>
                                        <%else%>
                                        <div style="float:left;"><img src="/images/product/review/recommend-x.png" border="0" /></div>
                                        <div style="float:left; padding-left:3px; font-style:italic;"><span style="font-size:15px; font-weight:bold;">No,</span> I don't recommend this!</div>
                                        <%end if%>
                                    </div>
                                    <div style="float:left; width:100%; padding-top:15px;">
                                        <div>
                                        	<strong>Age:</strong> 
                                            <%
											if not isnull(reviewerAge) then
												select case cint(reviewerAge)
													case 1 : response.write "17 & under"
													case 2 : response.write "18 - 25"
													case 3 : response.write "26 - 35"
													case 4 : response.write "36 - 45"
													case 5 : response.write "46 - 55"
													case 6 : response.write "56+"
													case else : response.write "N/A"
												end select
											else
											%>
                                            	N/A
                                            <%end if%>
										</div>
                                        <div style="padding-top:7px;">
                                        	<strong>Gender:</strong> 
                                            <%
											if not isnull(reviewerGender) then
												if reviewerGender = "M" then
													response.write "Male"
												else
													response.write "Female"
												end if
											else
											%>
                                            	N/A
                                            <%end if%>
										</div>
                                        <div style="padding-top:7px;">
                                        	<strong>Description:</strong> 
                                            <%
											if not isnull(reviewerType) then
												select case ucase(reviewerType)
													case "STU" : response.write "Student"
													case "BO" : response.write "Business Owner"
													case "TE" : response.write "Technology Enthusiast"
													case "PA" : response.write "Phone Addict"
													case "CPU" : response.write "Casual Phone User"
												end select
											else
											%>
                                            	N/A
                                            <%end if%>
										</div>
                                        <div style="padding-top:7px;">
                                        	<strong>Ownership:</strong> 
                                            <%
											if not isnull(ownershipPeriod) then
												select case cint(ownershipPeriod)
													case 1 : response.write "2 ~ 4 Weeks"
													case 2 : response.write "1 ~ 2 Months"
													case 3 : response.write "3 ~ 12 Months"
													case 4 : response.write "1 year +"
												end select
											else
											%>
                                            	N/A
                                            <%end if%>
										</div>
                                    </div>
                                    <div style="float:left; width:100%; padding-top:15px; border-bottom:1px solid #ccc;"></div>
                                    <div style="float:left; width:100%; padding-top:15px;">
                                        <div>
                                        	<strong>Pros:</strong> 
                                            <%
											strPros = ""
											if isProPrice then strPros = "Great Price, "
											if isProDesign then strPros = strPros & "Beautifully Designed, "
											if isProWeight then strPros = strPros & "Functional, "
											if isProProtection then strPros = strPros & "Convenient, "
											if strPros <> "" then response.write left(strPros, len(strPros)-2)
											if not isnull(otherPro) then 
												if strPros <> "" then response.write "<br>"
												response.write "<span style=""font-style:italic;"">""" & otherPro & """</span>"
											end if
											%>
										</div>
                                        <div style="padding-top:7px;">
                                        	<strong>Cons:</strong>
                                            <%
											strCons = ""
											if isConFit then strCons = "Poor Value, "
											if isConRobust then strCons = strCons & "Doesn't Fit, "
											if isConMaintenance then strCons = strCons & "Not What I Expected, "
											if isConWeight then strCons = strCons & "Doesn't Work Well, "
											if strCons <> "" then response.write left(strCons, len(strCons)-2)
											if not isnull(otherCon) then 
												if strCons <> "" then response.write "<br>"
												response.write "<span style=""font-style:italic;"">""" & otherCon & """</span>"
											end if
											%>
										</div>
                                    </div>
                                </div>
                                <!--
                                <div style="float:left; width:100%; padding-top:10px;">
	                                <div style="float:left; font-weight:bold; padding:5px 0px 0px 10px;">Share:</div>
	                                <div style="float:left; padding-left:10px;"><img src="/images/product/review/share-facebook.png" border="0" /></div>
	                                <div style="float:left; padding-left:10px;">
										<a style="vertical-align:bottom;" href="http://twitter.com/share" data-url="<%=canonicalURL%>" class="twitter-share-button" data-count="none" data-via="CellOutfitter">Tweet</a>
										<img src="/images/product/review/share-twitter.png" border="0" />
									</div>
                                </div>
                                -->
							</div>
                        	<div style="float:left; margin-left:20px; width:470px;">
                                <div style="float:left; width:100%;">
                                    <div style="float:left;"><%=getRatingAvgStarBig(starRating)%></div>
                                    <div style="float:left; padding-left:10px; font-size:16px; font-weight:bold;"><%=formatnumber(starRating,1)%></div>
                                </div>
                                <div style="float:left; width:100%; font-size:18px; padding-top:3px;"><%=reviewTitle%></div>
                                <div style="float:left; width:100%; font-size:12px; padding-top:3px; color:#999; font-style:italic;">Posted on <%=entryDate%></div>
                                <div style="float:left; width:100%; padding-top:10px; line-height:22px; font-size:14px;"><%=reviewBody%></div>
                                <div style="float:left; width:100%; padding-top:20px;">
                                	<div style="float:left; font-weight:bold; padding-top:7px;">Was this review helpful?</div>
                                	<div style="float:left; padding-left:10px;"><img src="/images/product/review/ico-thumbs-up.png" border="0" style="cursor:pointer;" onclick="updateThumbs(<%=reviewID%>,1,<%=thumbsUpCount%>)" /></div>
                                	<div style="float:left; font-weight:bold; padding:7px 0px 0px 3px;" id="thumbsUpCount<%=reviewID%>"><%=thumbsUpCount%></div>
                                	<div style="float:left; padding-left:10px;"><img src="/images/product/review/ico-thumbs-down.png" border="0" style="cursor:pointer;" onclick="updateThumbs(<%=reviewID%>,0,<%=thumbsDownCount%>)" /></div>
                                	<div style="float:left; font-weight:bold; padding:7px 0px 0px 3px;"  id="thumbsDownCount<%=reviewID%>"><%=thumbsDownCount%></div>
								</div>
                            </div>
                        </div>
                        <%
						objRsReviews.movenext
					loop
					%>
                    	</div> <!-- //reviewBody -->
                    </div>
                    <%
				end if
				%>
                </div>
				<div style="padding:10px; color:#333; font-size:12px; display:none;" id="tabBody4">
					<script type="text/javascript">
                        var TurnToItemSku = "<%=itemid%>";
                        document.write(unescape("%3Cscript src='" + document.location.protocol + "//static.www.turnto.com/sitedata/" + turnToConfig.siteKey + "/" + TurnToItemSku + "/d/itemjs' type='text/javascript'%3E%3C/script%3E"));
                    </script>
                	<table border="0" width="100%"><tr><td align="center"><span class="TurnToItemTeaser"></span></td></tr></table>
                    <div>
						<%
                        sql = 	"exec sp_pullQandAByPartNumber '" & partnumber & "', 2"
                        session("errorSQL") = sql
                        arrUGC = getDbRows(sql)
                        
                        if not isnull(arrUGC) then
                        %>
                        <div style="overflow:auto; height:280px;">
                            <!--#include virtual="/includes/asp/inc_onpageTurnTo.asp"-->
                        </div>
                        <%
                        end if
                        %>                    
                    </div>
                </div>
            </div>
            <div id="trustLogos" style="float:left; margin:20px 0px 10px 0px; text-align:center; width:780px;"><img src="/images/icons/trust.jpg" border="0" /></div>
			<div id="googleAd" style="float:left; margin:20px 0px 20px 0px; text-align:center; width:780px;">
            	<script type="text/javascript">
					<!--
					google_ad_client = "ca-pub-1001075386636876";
					/* CO CA Bottom */
					google_ad_slot = "7359681427";
					google_ad_width = 728;
					google_ad_height = 90;
					//-->
				</script>
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
            </div>
        </td>
        <td valign="top">
        	<div style="float:right; width:200px; background-color:#e5e5e5; padding:10px; border-radius:10px;" id="rightSideTestimonial1">
            	<div style="float:left; width:180px; padding:10px; background-color:#fff; border:1px solid #c3c3c3; border-radius:10px;">
                	<div style="float:left; width:170px; padding-bottom:10px; font-weight:bold; color:#002b55; font-size:13px; text-align:center;">Your Recommendations:</div>
                    <div class="fl strandsRecs" tpl="PROD-2" style="margin-left:25px;" item="<%=itemID%>"></div>
                </div>
            </div>
            <div style="float:right; width:200px; background-color:#e5e5e5; margin-top:10px; padding:10px; border-radius:10px;" id="rightSideTestimonial1">
            	<div style="float:left; width:180px; padding:10px; background-color:#f9f9f9; border:1px solid #c3c3c3; border-radius:10px;">
                	<div style="float:left; width:170px; padding-bottom:10px;"><img src="/images/icons/testimonial-header.jpg" border="0" /></div>
                    <%
					nComments = 0
					sql = "sp_getTestimonials 3, 2"
					set rsComment = oConn.execute(sql)
					do until rsComment.eof
						nComments = nComments + 1
					%>
					<div style="float:left; width:19px; padding-right:10px;"><img src="/images/icons/yellow-star.jpg" border="0" /></div>
                    <div style="float:left; width:140px; font-size:12px; padding-bottom:10px; color:#666;">
                    	"<%=rsComment("quote")%>"
                    </div>
                    <div style="float:left; width:160px; font-size:12px; color:#999;">
                    	<span style="font-size:16px; font-weight:bold; color:#3178b6"><%=rsComment("attName")%></span><br />
                        from <%=rsComment("attCity") & " " & rsComment("attState")%>
                    </div>
                    <%
						if nComments < 3 then
						%>
                        <div style="float:left; padding:10px 0px 10px 0px; width:170px;"><hr color="#CCCCCC" /></div>
                        <%	
						end if
						rsComment.movenext
					loop
					%>
                </div>
            </div>
            <!--
            <div style="float:right; width:200px; background-color:#e5e5e5; margin-top:20px; padding:10px; border-radius:10px;" id="rightSideLivePurchase">
            	<div id="livePurchaseDiv" style="float:left; width:180px; padding:10px; background-color:#f9f9f9; border:1px solid #c3c3c3; border-radius:10px;"></div>
            </div>
            -->
        </td>
    </tr>
</table>
</div>
<%
function hasImageOnRemote(pRemoteURL, pFileName)
	dim isExists : isExists = false
	dim oXMLHTTP : set oXMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP")
	dim strTemp : strTemp = pRemoteURL & "/" & pFileName
	
	oXMLHTTP.Open "HEAD", strTemp, False	'communicating without responseBody, only looks up file header
	oXMLHTTP.Send
	
	if (oXMLHTTP.readyState = "4") and (oXMLHTTP.status = "200") and (instr(oXMLHTTP.getResponseHeader("Content-Type"), "image") > 0) then
		isExists = true
	end if

	set oXMLHTTP = Nothing
	
	hasImageOnRemote = isExists
	
end function
%>
<!--// rich snippets-->
<%if bZoom then%>
	<!--#include virtual="/includes/asp/inc_zoom.asp"-->
<%end if%>
<script type="text/javascript">
	window.WEDATA.pageType = 'product';
	window.WEDATA.productData = {
		manufacturer: <%=jsStr(brandName)%>,
		partNumber: <%= jsStr(partNumber)%>,
		modelId: <%= jsStr(modelID)%>,
		modelName: <%= jsStr(modelName)%>,
		itemId: <%= jsStr(itemID)%>,
		typeId: <%= jsStr(typeId)%>,
		categoryName: <%= jsStr(categoryName) %>,
		description: <%= jsStr(itemDesc_CO) %>,
		stock: <%= jsStr(prodQty) %>,
		alwaysInStock: <%= jsStr(LCase(alwaysInStock)) %>,
		price: <%= jsStr(price_co) %>
	};

</script>


<!--#include virtual="/includes/template/bottom.asp"-->
<script>
	//ajax('/ajax/productDetails.asp?itemID=<%=itemID%>&modelID=<%=modelID%>&typeID=<%=typeID%>&latDesign=1&strands=1','relatedItems_new')
	var itemID = <%=prepInt(itemID)%>;
	var runSalesTimer = 1;
	
	if (runSalesTimer == 1) {
		setTimeout("changeSalesTime()",1000)
	}
	
	function ajaxProductVideo(videoIDs,idx) {
		ajax('/ajax/productVideo.asp?ids='+videoIDs+'&idx='+idx,'popBox');
		document.getElementById('popCover').style.display = '';
		document.getElementById('popBox').style.display = '';
	}
	
	function strandsAddToCart(formName) {
		if (typeof StrandsTrack=="undefined"){ StrandsTrack=[]; }
		StrandsTrack.push({event:"addshoppingcart",item:itemID} );
		jQuery(window).trigger('addToCart', formName);
		eval("document." + formName + ".submit()");
	}
	
	function changeSalesTime() {
		var saleHours = parseInt(document.getElementById("saleHours").innerHTML);
		var saleMinutes = parseInt(document.getElementById("saleMinutes").innerHTML);
		var saleSeconds = parseInt(document.getElementById("saleSeconds").innerHTML);
		
		if (saleSeconds > 0) {
			saleSeconds--
			if (saleSeconds < 10) {
				document.getElementById("saleSeconds").innerHTML = "0" + saleSeconds
			}
			else {
				document.getElementById("saleSeconds").innerHTML = saleSeconds
			}
			setTimeout("changeSalesTime()",1000)
		}
		else if (saleMinutes > 0) {
			saleMinutes--
			if (saleMinutes < 10) {
				document.getElementById("saleMinutes").innerHTML = "0" + saleMinutes
			}
			else {
				document.getElementById("saleMinutes").innerHTML = saleMinutes
			}
			document.getElementById("saleSeconds").innerHTML = 59
			setTimeout("changeSalesTime()",1000)
		}
		else if (saleHours > 0) {
			saleHours--
			if (saleHours < 10) {
				document.getElementById("saleHours").innerHTML = "0" + saleHours
			}
			else {
				document.getElementById("saleHours").innerHTML = saleHours
			}
			document.getElementById("saleMinutes").innerHTML = 59
			document.getElementById("saleSeconds").innerHTML = 59
			setTimeout("changeSalesTime()",1000)
		}
	}
	
	var	correctAnswer = false;
	function showReviewAll()
	{
		var objMoreReview = document.getElementById('tblMoreReview');
		var	objArrow = document.getElementById('id_img_reviewArrow');
		
		if ("none" == tblMoreReview.style.display)
		{
			objMoreReview.style.display = '';
			document.getElementById('a_showmore').innerHTML = "hide more..";
		}
		else 
		{
			objMoreReview.style.display = 'none';
			document.getElementById('a_showmore').innerHTML = "show more..";			
		}	
	}

	function showReviewPopup() {
		try {
			viewPortH = $(window).height(); //get browser's viewport height.
		}
		catch(err) {
			viewPortH = 700;
		}		
		ajax('/ajax/productreview.asp?screenH='+viewPortH+'&itemid=<%=itemID%>','popBox');
		document.getElementById('popCover').style.display = '';
		document.getElementById('popBox').style.display = '';
	}
	
	<% if showReviewPopup then %>
	setTimeout("showReviewPopup()",500)
	<% end if %>
	
	function rateReview(id) {
		for(i=1;i<6;i++) {
			if (i <= id)
				document.getElementById('reviewStar'+i).src='/images/product/review/star-full.png';
			else
				document.getElementById('reviewStar'+i).src='/images/product/review/star-empty.png';
		}
		
		document.getElementById('id_hidStar').value = id;

		switch(id)
		{
		case 1:
			document.getElementById('reviewStarDesc1').innerHTML = '1 Star';
			document.getElementById('reviewStarDesc2').innerHTML = 'Poor';
			break;
		case 2:
			document.getElementById('reviewStarDesc1').innerHTML = '2 Stars';
			document.getElementById('reviewStarDesc2').innerHTML = 'Fair';
			break;
		case 3:
			document.getElementById('reviewStarDesc1').innerHTML = '3 Stars';
			document.getElementById('reviewStarDesc2').innerHTML = 'Good';
			break;
		case 4:
			document.getElementById('reviewStarDesc1').innerHTML = '4 Stars';
			document.getElementById('reviewStarDesc2').innerHTML = 'Very Good';
			break;
		case 5:
			document.getElementById('reviewStarDesc1').innerHTML = '5 Stars';
			document.getElementById('reviewStarDesc2').innerHTML = 'Excellent';
			break;
		default:
			document.getElementById('reviewStarDesc1').innerHTML = '5 Stars';
			document.getElementById('reviewStarDesc2').innerHTML = 'Excellent';
			break;
		}
	}	
	
	function validateReview() {
		var frm = document.frmProductReview;
		
		frm.txtEmail.className = "";
		frm.txtReviewTitle.className = "";
		frm.txtReview.className = "";
		frm.txtNickname.className = "";
		document.getElementById('msgEmail').style.display = 'none';
		document.getElementById('msgReviewTitle').style.display = 'none';
		document.getElementById('msgReview').style.display = 'none';
		document.getElementById('msgNickname').style.display = 'none';
		
		var reviewerEmail = frm.txtEmail.value;
		var reviewerNickName = frm.txtNickname.value;
		var reviewerLocation = frm.txtLocation.value;
		var reviewerAge = frm.cbAge.value;
		var reviewerGender = frm.cbGender.value;
		var reviewerType = getSelectedRadioValue(frm.rdoJob);
		var orderID = <%=prepInt(request.queryString("oid"))%>;
		var itemID = <%=itemid%>;
		var starRating = frm.hidStar.value;
		var ownershipPeriod = frm.cbOwnership.value;
		var reviewTitle = frm.txtReviewTitle.value;
		var reviewBody = frm.txtReview.value;
		var isRecommend = getSelectedRadioValue(frm.rdoRecommend);
		var otherPro = frm.txtPros.value;
		var otherCon = frm.txtCons.value;

		var isProPrice = 0;
		if (frm.chkPros1.checked) isProPrice = 1;
		var isProDesign = 0;
		if (frm.chkPros2.checked) isProDesign = 1;
		var isProWeight = 0;
		if (frm.chkPros3.checked) isProWeight = 1;
		var isProProtection = 0;
		if (frm.chkPros4.checked) isProProtection = 1;

		var isConFit = 0;
		if (frm.chkCons1.checked) isConFit = 1;
		var isConRobust = 0;
		if (frm.chkCons2.checked) isConRobust = 1;
		var isConMaintenance = 0;
		if (frm.chkCons3.checked) isConMaintenance = 1;
		var isConWeight = 0;
		if (frm.chkCons4.checked) isConWeight = 1;

		if (!validateEmail(reviewerEmail)) { 
			document.getElementById('msgEmail').style.display = '';
			frm.txtEmail.className = "alertField";
			frm.txtEmail.focus();
			return false;
		}

		if (reviewTitle == '') { 
			document.getElementById('msgReviewTitle').style.display = '';
			frm.txtReviewTitle.className = "alertField";
			frm.txtReviewTitle.focus();
			return false;
		}

		if (reviewBody == '') { 
			document.getElementById('msgReview').style.display = '';
			frm.txtReview.className = "alertField";
			frm.txtReview.focus();
			return false;
		}

		if (reviewerNickName == '') { 
			document.getElementById('msgNickname').style.display = '';
			frm.txtNickname.className = "alertField";
			frm.txtNickname.focus();
			return false;
		}

		var updateURL	=	"/ajax/productReviewUpdate.asp";
		var updateParam	=	"siteID=2";
		updateParam	=	updateParam + "&reviewerEmail=" + reviewerEmail;
		updateParam	=	updateParam + "&reviewerNickName=" + escape(reviewerNickName);
		updateParam	=	updateParam + "&reviewerLocation=" + escape(reviewerLocation);
		updateParam	=	updateParam + "&reviewerAge=" + reviewerAge;
		updateParam	=	updateParam + "&reviewerGender=" + reviewerGender;
		updateParam	=	updateParam + "&reviewerType=" + reviewerType;
		updateParam	=	updateParam + "&orderID=" + orderID;
		updateParam	=	updateParam + "&itemID=" + itemID;
		updateParam	=	updateParam + "&starRating=" + starRating;
		updateParam	=	updateParam + "&ownershipPeriod=" + ownershipPeriod;
		updateParam	=	updateParam + "&reviewTitle=" + escape(reviewTitle);
		updateParam	=	updateParam + "&reviewBody=" + escape(reviewBody);
		updateParam	=	updateParam + "&isRecommend=" + isRecommend;
		updateParam	=	updateParam + "&isProPrice=" + isProPrice;
		updateParam	=	updateParam + "&isProDesign=" + isProDesign;
		updateParam	=	updateParam + "&isProWeight=" + isProWeight;
		updateParam	=	updateParam + "&isProProtection=" + isProProtection;
		updateParam	=	updateParam + "&otherPro=" + escape(otherPro);
		updateParam	=	updateParam + "&isConFit=" + isConFit;
		updateParam	=	updateParam + "&isConRobust=" + isConRobust;
		updateParam	=	updateParam + "&isConMaintenance=" + isConMaintenance;
		updateParam	=	updateParam + "&isConWeight=" + isConWeight;
		updateParam	=	updateParam + "&otherCon=" + escape(otherCon);

		ajaxPost(updateURL, updateParam, 'popBox');
	}
	
	var strThumbs = ",";
	function updateThumbs(reviewID,isThumbsUp,curCount)	{
		if ((isThumbsUp == 1)&&(strThumbs.search(","+reviewID+",") == -1)) {
			strThumbs = strThumbs + reviewID + ",";
			ajax('/ajax/productReviewThumbs.asp?reviewID='+reviewID+'&isThumbsUp='+isThumbsUp,'dumpZone');
			document.getElementById('thumbsUpCount'+reviewID).innerHTML = parseInt(curCount) + 1;
		}
		
		if ((isThumbsUp == 0)&&(strThumbs.search(","+reviewID+",") == -1)) {
			strThumbs = strThumbs + reviewID + ",";
			ajax('/ajax/productReviewThumbs.asp?reviewID='+reviewID+'&isThumbsUp='+isThumbsUp,'dumpZone');
			document.getElementById('thumbsDownCount'+reviewID).innerHTML = parseInt(curCount) + 1;
		}
	}
	
	function onReviewSort(strSort) {
		ajax('/ajax/productReviewSort.asp?itemid=<%=itemid%>&strSort='+strSort+'&canonicalURL=<%=server.URLEncode(canonicalURL)%>','reviewBody');
	}
	
	//setTimeout("ajax('/ajax/livePurch.asp','livePurchaseDiv')",1000)
</script>
<script language="javascript" src="/includes/js/productPage.js?v=20131003"></script>
<!-- Begin Curebit product integration code -->
  <script type="text/javascript" src="http://www.curebit.com/javascripts/api/all-0.2.js"></script>
  <script type="text/javascript">

    curebit.init({site_id: 'cellular-outfitter'});

    var products = [{
          url:	'http://www.cellularoutfitter.com<%=request.ServerVariables("HTTP_X_REWRITE_URL")%>', /*REQUIRED VARIABLE */
          <% if musicSkins = 1 then %>
		  image_url: 'http://www.cellularoutfitter.com/productpics/musicSkins/musicSkinsLarge/<%=itempic_CO%>', /* REQUIRED VARIABLE */
		  <% else %>
		  image_url: 'http://www.cellularoutfitter.com/productPics/big/<%=itempic_CO%>', /* REQUIRED VARIABLE */
		  <% end if %>
          title:	<%= jsStr(itemDesc_CO) %>, /* REQUIRED VARIABLE */
          product_id: '<%="CO" & (itemID+250)%>', /* REQUIRED VARIABLE */
          price:	'<%=price_CO%>' /* OPTIONAL VARIABLE */
      },
      {
          url:	'http://www.cellularoutfitter.com<%=request.ServerVariables("HTTP_X_REWRITE_URL")%>', /*REQUIRED VARIABLE */
          <% if musicSkins = 1 then %>
		  image_url: 'http://www.cellularoutfitter.com/productpics/musicSkins/musicSkinsLarge/<%=itempic_CO%>', /* REQUIRED VARIABLE */
		  <% else %>
		  image_url: 'http://www.cellularoutfitter.com/productPics/big/<%=itempic_CO%>', /* REQUIRED VARIABLE */
		  <% end if %>
          title:	<%= jsStr(itemDesc_CO) %>, /* REQUIRED VARIABLE */
          product_id: '<%="CO" & (itemID+250)%>', /* REQUIRED VARIABLE */
          price:	'<%=price_CO%>' /* OPTIONAL VARIABLE */
      }];

    curebit.register_products(products);

  </script>

<!-- End Curebit product integration code -->
<script language="javascript">
	var myPartNumber = '<%=myPartNumber%>'
	var modelID = <%=prepInt(modelID)%>
	var typeID = <%=prepInt(typeID)%>
</script>
<!-- mp -->