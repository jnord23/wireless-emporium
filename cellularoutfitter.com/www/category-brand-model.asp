<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
dim productListingPage
productListingPage = 1
leftGoogleAd = 1

dim modelid, categoryid
modelid = request.querystring("modelid")
if modelid = "" or not isNumeric(modelid) then
	response.redirect("/")
	response.end
end if
categoryid = request.querystring("categoryid")
if categoryid = "" or not isNumeric(categoryid) then
	response.redirect("/")
	response.end
end if
brandid = request.querystring("brandid")
if brandid = "" or not isNumeric(brandid) then
	response.redirect("/")
	response.end
end if

if categoryid = 8 then
	Response.Status="301 Moved Permanently"
	Response.AddHeader "Location","/sb-0-sm-0-sc-8-other-accessories.html"
end if

call fOpenConn()
sql = "select a.modelName, b.brandName, c.typeName from we_models a left join we_brands b on b.brandID = " & brandID & " left join we_types c on c.typeID = " & categoryid & " where modelID = " & modelID
session("errorSQL") = sql
set rs = Server.CreateObject("ADODB.Recordset")
rs.open SQL, oConn, 3, 3

if not rs.eof then
	Response.Status="301 Moved Permanently"
	Response.AddHeader "Location","/sb-" & brandID & "-sm-" & modelID & "-sc-" & categoryid & "-" & formatSEO(RS("typename")) & "-" & formatSEO(rs("brandname")) & "-" & formatSEO(rs("modelName")) & ".html"
end if
call fCloseConn()

dim pageTitle
pageTitle = itemDesc_CO

call fOpenConn()
dim SQL, RS
if modelid <> "0" then
	SQL = "SELECT A.*, B.modelName, B.modelImg, C.brandID, C.brandName, D.typeName FROM ((we_items A INNER JOIN we_models B ON A.modelID = B.modelID)"
	SQL = SQL & " INNER JOIN we_brands C ON A.brandid = C.brandid)"
	SQL = SQL & " INNER JOIN we_types D ON A.typeid = D.typeid"
else
	SQL = "SELECT A.*, 'UNIVERSAL' AS modelName, 'UNIVERSAL.JPG' AS modelImg, 0 AS brandID, 'UNIVERSAL' AS brandName, D.typeName FROM we_items A INNER JOIN we_types D ON A.typeid = D.typeid"
end if
SQL = SQL & " WHERE A.modelid = '" & modelid & "'"
SQL = SQL & " AND A.typeid = '" & categoryid & "'"
SQL = SQL & " AND A.hideLive = 0"
SQL = SQL & " AND A.inv_qty <> 0"
SQL = SQL & " AND A.price_CO > 0"
SQL = SQL & " ORDER BY A.flag1 DESC"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3

if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if

dim brandName, brandID, categoryName, modelName, modelImg
brandName = RS("brandName")
brandID = RS("brandID")
categoryName = RS("typeName")
modelName = RS("modelName")
modelImg = RS("modelImg")

'=========================================================================================
Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
	oParam.CompareMode = vbTextCompare
	oParam.Add "x_brandID", brandID
	oParam.Add "x_modelID", modelid
	oParam.Add "x_categoryID", categoryid
	oParam.Add "x_brandName", brandName
	oParam.Add "x_modelName", modelName
	oParam.Add "x_categoryName", categoryName		
	
call redirectURL("cbm", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
'=========================================================================================	

SQL = "SELECT itemID, brandID, modelID, typeID, subtypeID, itemDesc_CO, itempic_CO, flag1, price_Retail, price_CO, inv_qty, ItemKit_NEW, hotDeal, hideLive, relatedId FROM ("
SQL = SQL & "SELECT itemID, brandID, modelID, typeID, subtypeID, itemDesc_CO, itempic_CO, flag1, ISNULL(price_Retail, 0.00) AS price_Retail, ISNULL(price_CO, 0.00) AS price_CO, inv_qty, ItemKit_NEW, hotDeal, hideLive, 0 AS relatedId"
SQL = SQL & " FROM we_Items AS a WHERE (Sports=0 AND typeID='" & categoryid & "' AND modelID='" & modelid & "' AND (inv_qty <> 0 OR ItemKit_NEW IS NOT NULL)) AND price_CO > 0"
SQL = SQL & " UNION ALL "
SQL = SQL & "SELECT c.ITEMID, c.brandid, c.modelid, c.typeid, c.subtypeid, b.itemDesc_CO, b.itempic_CO, b.flag1, ISNULL(b.price_Retail, 0.00) AS price_Retail, ISNULL(b.price_CO, 0.00) AS price_CO, b.inv_qty, b.ItemKit_NEW, b.hotDeal, b.hideLive, c.Related_Id AS relatedId"
SQL = SQL & " FROM we_Items AS b INNER JOIN we_relatedItems AS c ON c.ITEMID = b.itemID WHERE b.Sports=0 AND c.typeid='" & categoryid & "' AND c.modelid='" & modelid & "' AND b.price_CO > 0"
SQL = SQL & ") AS d"
SQL = SQL & " WHERE hideLive = 0 AND (inv_qty <> 0 OR ItemKit_NEW IS NOT NULL)"
SQL = SQL & " ORDER BY flag1 DESC"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3

dim OtherGear, Bling
if categoryid = "8" and modelid = "0" then
	OtherGear = true
else
	OtherGear = false
end if
if categoryid = "14" and modelid = "0" then
	Bling = true
else
	Bling = false
end if

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, H1
if OtherGear = true then
	SEtitle = "Cell Phone Charms & Bling Kits: Crystal Charms, Phone Pets Charms at Wholesale Prices"
	SEdescription = "Get the best deals on cell phone charms & bling kits like crystal charms, phone pets charms at CellularOutfitter.com - The #1 store online for cell phone accessories at wholesale prices."
	SEkeywords = "cell phone charms, cell phone bling, cell bling, crystal cell phone charms, bling kits, cell phone bling kits"
	topText = "GET NOTICED every time you raise your phone to your ear! Bling your cell phone with popular <a href=""/p-7679-pink-and-clear-rhinestone-crystals--300-count-peel--n--stick-.html"" title=""Cell Phone Bling"">Cell Phone Bling</a> Kits & <a href=""/p-18634-crystal-charms---heart-in-heart.html"" title=""Cell Phone Charms"">Cell Phone Charms</a> AT WHOLESALE COST! Cell phone bling kits have grown in popularity and are used by cell phone users and famous stars across the world. They are a quick and easy way to personalize your cell phone. Best of all, when you buy your bling kit at CellularOutfitter.com, you know you're getting the BEST DEAL ONLINE ?GUARANTEED!</p>"
	topText = topText & "<p class=""static-content-font"">We offer a wide range of cell phone charms like crystal charms & phone pets charms. Apart from cell phone charms & bling, we offer a complete line of accessories for your cell phone at WHOLESALE PRICES! Buy Phone Pets Charms like the pig, black cat, mouse or yellow, pink, blue <a href=""/p-7676-purple-and-clear-rhinestone-crystals--300-count-peel--n--stick-.html"" title=""Crystal Cell Phone Charms"">crystal cell phone charms</a> at wholesale prices!"
elseif Bling = true then
	SEtitle = "Cell Phone Charms & Bling Kits: Crystal Charms, Phone Pets Charms at Wholesale Prices"
	SEdescription = "Get the best deals on Cell phone charms and phone bling kits like crystal charms, phone bling crystals, phone pets charms at CellularOutfitter.com at wholesale prices!"
	SEkeywords = "cell phone charms, cellphone charms, cell phone bling, phone bling, crystal cell phone charms, bling kits, cell phone bling kits, phone bling crystals"
	topText = "Get noticed every time you raise your phone to your ear! Bling your cell phone with popular <a href=""/p-7679-pink-and-clear-rhinestone-crystals--300-count-peel--n--stick-.html"" title=""Cell Phone Bling"">cell phone bling</a> kits & <a href=""/p-18634-crystal-charms---heart-in-heart.html"" title=""Cell Phone Charms"">cell phone charms</a> AT WHOLESALE COST! Cell phone bling kits have grown in popularity and are used by cell phone users and famous stars across the world. They are a quick and easy way to personalize your cell phone. Best of all, when you buy your bling kit at CellularOutfitter.com, you know you're getting the BEST DEAL ONLINE ?GUARANTEED!</p>"
	topText = topText & "<p class=""static-content-font"">We offer a wide range of cell phone charms like crystal cell phone charms, phone pets charms and phone bling crystals. Apart from wholesale cell phone charms & bling, we offer a complete line of accessories for your cell phone at WHOLESALE PRICES! Buy wholesale cell phone charms such as Phone Pets Charms like the pig, black cat, mouse, or buy yellow, pink, or blue <a href=""/p-7676-purple-and-clear-rhinestone-crystals--300-count-peel--n--stick-.html"" title=""Crystal Cell Phone Charms"">crystal cell phone charms</a> at wholesale prices!"
	bottomText = "CellularOutfitter: The Best resource Online to accessorize your cell phone with the latest <a href="""" title=""Cell Phone Accessories"">cell phone accessories</a>. We are the largest online retailers of cell phone cases, cell phone covers, cell phone chargers and other accessories which help you to personalize, optimize and protect your phone."
elseif categoryid = "3" and brandid = "5" and modelid = "419" then
	SEtitle = "Motorola KRZR Covers: Motorola Cell Phone Accessories"
	SEdescription = "CellularOutfitter.com is the #1 store online for Motorola Cell Phone Accessories like Cell Phone Covers for the Motorola KRZR."
	SEkeywords = "motorola krzr covers, motorola krzr accessories, motorola krzr k1m covers, motorola krzr k1m accessories"
	topText = "You've selected Motorola KRZR K1m Covers. CellularOutfitter.com is the #1 Store for all your cell phone accessories needs where you will find the right Covers for your Motorola KRZR K1m at wholesale prices. No membership required! We offer the <a href=""/lowest-price.html"">lowest prices online guaranteed</a>.</p>"
	topText = topText & "<p class=""static-content-font"">CellularOutfitter.com offers the best Covers for the Motorola KRZR K1m at wholesale prices. We stand behind the quality of our products with an iron-clad 100% Quality Assurance Guarantee. We offer only quality Motorola KRZR K1m Covers at wholesale prices. Shop around and see what we mean!"
	bottomText = "CellularOutfitter.com ?the #1 Cell Phone Accessories Store Online.</p><p class=""static-content-font"">Grab the best deals on the latest <a href=""/"" title=""Motorola KRZR Accessories"">Motorola Cell Phone Accessories</a> like Motorola KRZR Accessories and more right here! CellularOutfitter.com is one of the primary sources for cell phone accessories online catering to a large clientele who will vouch for its quality products and credibility. This faith that our customers have in us is our drive to sell the best Motorola cell phone accessories like <a href=""/sc-3-sb-5-cell-phone-covers-for-motorola.html"" title=""Motorola KZRZ Covers"">Motorola covers</a>, <a href=""/c-7-cell-phone-cases-pouches.html"" title=""Motorola Cell Phone Cases"">cell phone cases</a>, covers & more at the most competitive prices online."
end if

if categoryid = "1" then
	if brandid = "4" then
		SEtitle = "LG Batteries | LG Cell Phone Battery at Wholesale Prices"
		SEdescription = "CellularOutfitter.com offers a wide collection of the LG Batteries ?Buy LG phone Battery and other cell phone accessories at wholesale prices"
		H1 = "LG Batteries"
	elseif brandid = "5" then
		SEtitle = "Motorola batteries | Motorola Cell Phone battery at Wholesale Prices"
		SEdescription = "CellularOutfitter.com offers a wide collection of the Motorola Batteries ?Buy Motorola phone Battery and other cell phone accessories at wholesale prices"
		H1 = "Motorola Batteries"
	elseif brandid = "9" then
		SEtitle = "Samsung Batteries | Samsung Cell Phone Batteries"
		SEdescription = "CellularOutfitter.com offers a wide collection of the Samsung Batteries ?Buy Samsung phone Battery and other cell phone accessories at wholesale prices"
		H1 = "Samsung Batteries"
	elseif brandid = "14" then
		SEtitle = "Blackberry Batteries | Cell Phone Battery for Blackberry at Wholesale Prices"
		SEdescription = "CellularOutfitter.com offers a wide collection of the Blackberry Batteries ?Buy Blackberry phone Battery and other cell phone accessories at wholesale prices"
		H1 = "Blackberry Batteries"
	elseif brandid = "20" then
		SEtitle = "HTC Batteries | HTC Phone Battery at Wholesale Prices"
		SEdescription = "CellularOutfitter.com offers a wide collection of the HTC Batteries ?Buy HTC phone Battery and other cell phone accessories at wholesale prices"
		H1 = "HTC Batteries"
	end if
elseif categoryid = "2" then
	if brandid = "5" then
		SEtitle = "Motorola Chargers | Motorola Cell Phone Chargers"
		SEdescription = "CellularOutfitter.com offers a wide collection of Motorola Chargers ?Buy Motorola phone Chargers and other cell phone accessories at wholesale prices"
		H1 = "Motorola Chargers"
	elseif brandid = "14" then
		SEtitle = "Blackberry Chargers | Blackberry Cell Phone Charger"
		SEdescription = "CellularOutfitter.com offers a wide collection of Blackberry Chargers ?Buy Blackberry phone Chargers and other cell phone accessories at wholesale prices"
		H1 = "Blackberry Chargers"
	elseif brandid = "20" then
		SEtitle = "HTC Chargers | HTC Cell Phone Charger"
		SEdescription = "CellularOutfitter.com offers a wide collection of HTC Chargers ?Buy HTC phone Chargers and other cell phone accessories at wholesale prices"
		H1 = "HTC Chargers"
	end if
elseif categoryid = "5" then
	if brandid = "9" then
		SEtitle = "Samsung Hands-Free Kits & Bluetooth Headsets | Cell Phone Hands-Free Kits & Bluetooth Headsets"
		SEdescription = "CellularOutfitter.com offers a wide collection of the best Samsung Hands-Free Kits & Bluetooth Headsets ?Buy Samsung cell phone Hands-Free Kits & Bluetooth Headsets and other cell phone accessories at wholesale prices."
	end if
elseif categoryid = "7" then
	if brandid = "4" then
		SEtitle = "LG Cell Phone Cases & Pouches"
		SEdescription = "CellularOutfitter.com offers a wide collection of LG Cases & Pouches ?Buy LG cell phone Cases &amp; Pouches and other cell phone accessories at wholesale prices."
		H1 = "LG Cell Phone Cases & Pouches"
	elseif brandid = "17" then
		SEtitle = "Apple iPhone Cases and Covers ?iPhone Pouches and Cases ?Cheap iPhone Accessories"
		SEdescription = "Find the latest iphone cases and covers & iphone phouches at wholesale prices and other iPhone Accessories at the no 1 Cell Phone Accessories Store Online"
		H1 = "Apple iPhone Cases & Pouches"
	end if
end if

if SEtitle = "" then
	if categoryid = "3" or categoryid = "7" then
		SEtitle = brandName & " " & modelName & " " & nameSEO(categoryName) & ": Huge Discounts - Wholesale Prices!!"
	else
		SEtitle = nameSEO(categoryName) & " for " & brandName & " " & modelName & ": Huge Discounts - Wholesale Prices!!"
	end if
end if
if SEdescription = "" then SEdescription = "CellularOutfitter.com is the best store online for " & brandName & " " & modelName & " " & nameSEO(categoryName) & " &amp; other Cell Phone Accessories for " & brandName & " " & modelName & " at wholesale prices."
if SEkeywords = "" then SEkeywords = brandName & " " & modelName & " " & nameSEO(categoryName) & ", " & brandName & " " & modelName & " Cell Phone " & nameSEO(categoryName) & ", " & nameSEO(categoryName) & " for " & brandName & " " & modelName & ", " & nameSEO(categoryName) & " for " & brandName & " " & modelName & " Cell Phones, " & brandName & " " & modelName & " " & singularSEO(categoryName) & ", " & singularSEO(categoryName) & " for " & brandName & " " & modelName
if topText = "" then
	select case categoryid
		case "1"
			topText = "Are you looking to upgrade the life of your <a href=""/sc-1-sb-" & brandID & "-cell-phone-batteries-for-" & formatSEO(brandName) & ".html"" title=""" & brandName & " Batteries"">" & brandName & " phone's battery</a>? Or maybe you just want to restore your phone's performance to the same level as when you bought it? In either case, Cellular Outfitter has the " & brandName & " " & modelName & " <a href=""/c-1-cell-phone-batteries.html"" title=""Cell Phone Battery"">cell phone battery</a> to fit your needs. We offer the same quality " & brandName & " " & modelName & " batteries as other places but at wholesale prices that can't be beat."
		case "2"
			topText = "Because we carry a wide variety of <a href=""/sc-2-sb-" & brandID & "-cell-phone-chargers-for-" & formatSEO(brandName) & ".html"" title=""" & brandName & " Chargers"">" & brandName & " cell phone chargers</a>, we are confident that you will find the perfect charger for your " & brandName & " " & modelName & " phone. All of our <a href=""/c-1-cell-phone-batteries.html"" title=""Cell Phone Battery"">cell phone chargers</a> are priced to offer you wholesale savings, including OEM chargers and specialty chargers like retractable cord chargers and heavy duty chargers. Make sure your " & brandName & " " & modelName & " doesn't die, leaving you disconnected, by picking up a car charger or a spare wall charger today."
		case "3"
			topText = "So you're looking to upgrade the look and performance of your <a href=""/sc-3-sb-" & brandID & "-cell-phone-covers-screen-guards-for-" & formatSEO(brandName) & ".html"" title=""" & brandName & " Cell Phone Covers & Screen Guards"">" & brandName & " phone</a>? Look no further than our incredible selection of <a href=""/c-3-cell-phone-covers-screen-guards.html"" title=""Cell Phone Faceplates and Screen Guards"">faceplates and screen guards</a> for the " & brandName & " " & modelName & ". Cellular Outfitter offers top quality products and wholesale prices so you have the freedom to create a phone that is a true reflection of you."
		case else
			topText = "You've selected <b>" & brandName & " " & modelName & "</b> " & nameSEO(categoryName) & ". Find the right " & nameSEO(categoryName) & " for your <b>" & brandName & " " & modelName & "</b> at wholesale prices. No membership required! We offer the <a href=""/lowest-price.html"">lowest prices online guaranteed</a>.</p>"
			topText = topText & "<p class=""static-content-font"">CellularOutfitter.com offers the best " & nameSEO(categoryName) & " for the " & brandName & " " & modelName & " at wholesale prices. We stand behind the quality of our products with an iron-clad 100% Quality Assurance Guarantee. We offer only quality " & brandName & " " & modelName & " " & nameSEO(categoryName) & " at wholesale prices. Shop around and see what we mean!"
	end select
end if

if modelid = 968 and (categoryid = "3" or categoryid = "7") then
	topText = "<span style=""color:#d01d00;"">The iPhone 4 is one of the best phones on the market ... except for that pesky death grip issue. CellularOutfitter is here to save the day with a selection of guards and cases, all suited to resolve the antenna issues. From sleek jet black to Ed Hardy and everything in between, CellularOutfitter is here to ensure you get the best out of your iPhone 4.</span><br><br>" & topText
end if

if H1 = "" then
	if OtherGear = true or Bling = true then
		H1 = nameSEO(categoryName) & " for All Phone Models"
	elseif categoryid = "3" or categoryid = "7" then
		H1 = brandName & " " & modelName & " " & nameSEO(categoryName)
	else
		H1 = nameSEO(categoryName) & " for " & brandName & " " & modelName
	end if
end if

dim strBreadcrumb
strBreadcrumb = "Cell Phone " & nameSEO(categoryName) & " for " & brandName & " " & modelName
%>

<!--#include virtual="/includes/template/top.asp"-->
								<td width="5">&nbsp;</td>
								<td width="775" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="100%" class="top-sublink-gray">
												<a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
												<%
												if OtherGear = true or Bling = true then
													%>
													<span class="top-sublink-blue">Cell Phone <%=nameSEO(categoryName)%></span>
													<%
												else
													%>
													<a class="top-sublink-gray" href="/c-<%=categoryID & "-cell-phone-" & formatSEO(categoryName)%>.html">Cell Phone <%=nameSEO(categoryName)%></a>&nbsp;>&nbsp;
													<a class="top-sublink-gray" href="/sc-<%=categoryID & "-sb-" & brandID & "-cell-phone-" & formatSEO(categoryName) & "-for-" & formatSEO(brandName)%>.html">Cell Phone <%=nameSEO(categoryName) & " for " & brandName%></a>&nbsp;>&nbsp;
													<span class="top-sublink-blue">Cell Phone <%=nameSEO(categoryName) & " for " & brandName & " " & modelName%></span>
													<%
												end if
												%>
											</td>
										</tr>
										<tr>
											<td><img src="/images/spacer.gif" width="1" height="10"></td>
										</tr>
										<tr>
											<td width="100%" align="center">
												<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="200" align="center" valign="top">
															<img src="/images/spacer.gif" width="200" height="5" border="0">
															<%
															if OtherGear = true then
																%>
																<img src="/images/types/CO_CAT_banner_other-gear.jpg" border="0" alt="Cell Phone Novelty & Fun Gear">
																<%
															elseif Bling = true then
																%>
																<img src="/images/types/CO_CAT_banner_charms.jpg" border="0" alt="Bling Kits & Charms">
																<%
															else
																%>
																<img src="/modelpics/<%=modelImg%>" border="0" alt="<%=nameSEO(categoryName) & " for " & brandName & " " & modelName%>">
																<%
															end if
															%>
														</td>
														<td align="center" class="static-content-font">
															<p class="nevi-font-bold"><%=h1%></p>
															<%=topText%>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="top-sublink-gray"><img src="/images/spacer.gif" width="1" height="10"></td>
										</tr>
										<tr>
											<td valign="middle" width="100%" height="68" style="background-image: url('/images/CO_header_items.jpg'); background-repeat: no-repeat; background-position: center bottom;">
												<style>
													h1 {
														margin-left: 10px;
													}
												</style>
												<h1><%=h1%></h1>
											</td>
										</tr>
										<tr>
											<td><img src="/images/spacer.gif" width="1" height="10" border="0"></td>
										</tr>
										<tr>
											<td align="center" valign="top" width="100%">
												<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td align="center" valign="top" width="100%">
															<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<%
																	dim altText, DoNotDisplay, RSkit, RSextra
																	a = 0
																	if categoryid = 3 or categoryid = 7 then
																		SQL = "SELECT MIN(price_Retail) AS minPrice_Retail, MIN(price_CO) AS minPrice_CO FROM we_items WHERE price_CO > 0 AND hidelive = 0 AND inv_qty > 0 AND typeid = 17 AND modelID = '" & modelID & "'"
																		set RSextra = Server.CreateObject("ADODB.Recordset")
																		RSextra.open SQL, oConn, 3, 3
																		if not RSextra.eof then
																			if RSextra("minPrice_Retail") > 0 and RSextra("minPrice_CO") > 0 then
																				%>
																				<td align="center" valign="top" width="193" height="100%">
																					<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
																						<tr>
																							<td align="center" valign="middle" width="170" height="120">
																								<a href="/sc-17-sb-<%=brandID & "-sm-" & modelID%>-invisible-film-protectors-for-<%=formatSEO(brandName) & "-" & formatSEO(modelName)%>.html"><img src="/images/CO_decal.jpg" border="0" alt="Invisible Film Protectors & Decal Skins"></a>
																							</td>
																						</tr>
																						<tr><td align="center" valign="top" height="8"><img src="/images/spacer.gif" width="1" height="8" border="0"></td></tr>
																						<tr>
																							<td align="center" valign="top" height="37">
																								<a class="static-content-font-link" href="/sc-17-sb-<%=brandID & "-sm-" & modelID%>-invisible-film-protectors-for-<%=formatSEO(brandName) & "-" & formatSEO(modelName)%>.html" title="Invisible Film Protectors & Decal Skins">Invisible Film Protectors & Decal Skins</a>
																							</td>
																						</tr>
																						<tr><td align="center" valign="top" height="8"><img src="/images/spacer.gif" width="1" height="8" border="0"></td></tr>
																						<tr>
																							<td align="center" valign="bottom">
																								<span class="nevi-font-bold">Starting At: </span><span class="red-price-small"><%=formatCurrency(RSextra("minPrice_CO"))%></span>
																								<img src="/images/spacer.gif" width="117" height="8" border="0"><br>
																							</td>
																						</tr>
																					</table>
																				</td>
																				<%
																				a = 1
																			end if
																		end if
																		RSextra.close
																		set RSextra = nothing
																	end if
																	
																	do until RS.eof
																		DoNotDisplay = 0
																		if not isNull(RS("ItemKit_NEW")) then
																			SQL = "SELECT inv_qty FROM we_Items WHERE itemID IN (" & RS("ItemKit_NEW") & ")"
																			set RSkit = Server.CreateObject("ADODB.recordset")
																			RSkit.open SQL, oConn, 3, 3
																			do until RSkit.eof
																				if RSkit("inv_qty") < 1 then DoNotDisplay = 1
																				RSkit.movenext
																			loop
																			RSkit.close
																			set RSkit = nothing
																		end if
																		if DoNotDisplay = 0 then
																			if categoryid = "3" and brandid = "5" and modelid = "419" then
																				altText = "Motorola cell phone covers for Motorola KRZR"
																			else
																				altText = RS("itemDesc_CO")
																			end if
																			if Bling = true then altText = "Cell Phone Charms & Bling : " & altText
																			%>
																			<td width="193" height="100%" align="center" valign="top">
																				<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td width="170" height="120" align="center" valign="middle">
																							<a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc_CO"))%>.html"><img src="/productPics/thumb/<%=RS("itempic_CO")%>" border="0" alt="<%=altText%>"></a>
																						</td>
																					</tr>
																					<tr><td height="8" align="center" valign="top"><img src="/images/spacer.gif" width="1" height="8" border="0"></td></tr>
																					<tr>
																						<td height="37" align="center" valign="top">
																							<a class="static-content-font-link" href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc_CO"))%>.html" title="<%=altText%>"><%=RS("itemDesc_CO")%></a>
																						</td>
																					</tr>
																					<tr><td height="8" align="center" valign="top"><img src="/images/spacer.gif" width="1" height="8" border="0"></td></tr>
																					<tr>
																						<td align="center" valign="bottom" class="cat-font">
																							Retail Price: <s><%=formatCurrency(RS("price_retail"))%></s>
																						</td>
																					</tr>
																					<tr><td height="3" align="center" valign="top"><img src="/images/spacer.gif" width="1" height="3" border="0"></td></tr>
																					<tr>
																						<td align="center" valign="bottom" class="nevi-font-bold">
																							Wholesale Price: <span class="red-price-small"><%=formatCurrency(RS("price_CO"))%></span>
																						</td>
																					</tr>
																					<tr><td height="3" align="center" valign="top"><img src="/images/spacer.gif" width="1" height="3" border="0"></td></tr>
																					<tr>
																						<td align="center" valign="bottom">
																							<a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc_CO"))%>.html"><img src="/images/button_moreinfo_blue.jpg" width="117" height="21" border="0" alt="More Info"></a>
																						</td>
																					</tr>
																				</table>
																			</td>
																			<%
																			a = a + 1
																			if a = 4 then
																				response.write "</tr><tr><td align=""center"" valign=""middle"" colspan=""4"" height=""25""><img src=""/images/hline4.jpg"" width=""750"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
																				a = 0
																			end if
																		end if
																		RS.movenext
																	loop
																	if a = 1 then
																		response.write "<td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td>" & vbcrlf
																	elseif a = 2 then
																		response.write "<td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td>" & vbcrlf
																	elseif a = 3 then
																		response.write "<td width=""170"">&nbsp;</td>" & vbcrlf
																	end if
																	%>
																</tr>
															</table>
														</td>
													</tr>
													<%if bottomText <> "" then%>
														<tr>
															<td align="center">
																<p>&nbsp;</p>
																<table border="0" cellspacing="0" cellpadding="2" class="thin-border">
																	<tr>
																		<td align="left" valign="top" class="contain">
																			<p class="static-content-font"><%=bottomText%></p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													<%end if%>
													<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
													<tr>
														<td>&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>

<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->
