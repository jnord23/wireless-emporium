<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
call PermanentRedirect("/c-5-cell-phone-bluetooth-and-hands-free.html")

dim categoryPage : categoryPage = 1
dim categoryid : categoryid = 99

dim pageTitle
pageTitle = "Bluetooth Superstore"

call fOpenConn()
SQL = "SELECT A.itemID,A.brandID,A.PartNumber AS MPN,A.itemDesc_CO,A.itempic_CO,A.price_Retail,A.price_CO,A.Sports,A.flag1,A.UPCCode,"
SQL = SQL & "A.itemLongDetail_CO,A.POINT1,A.POINT2,A.POINT3,A.POINT4,A.POINT5,A.POINT6,A.COMPATIBILITY,A.POINT7,"
SQL = SQL & "B.brandName"
SQL = SQL & " FROM we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID"
SQL = SQL & " WHERE A.HandsfreeType = 2"
SQL = SQL & " AND A.hideLive = 0"
SQL = SQL & " AND A.inv_qty <> 0"
SQL = SQL & " AND A.price_CO > 0 "

dim nSorting
nSorting = request.form("nSorting")
select case nSorting
	case "1" : SQL = SQL & "ORDER BY A.numberofsales DESC"
	case "2" : SQL = SQL & "ORDER BY A.price_CO DESC"
	case "3" : SQL = SQL & "ORDER BY A.price_CO"
	case "4" : SQL = SQL & "ORDER BY A.brandid, A.DatetimeEntd DESC"
	case "5" : SQL = SQL & "ORDER BY A.brandid, A.DateTimeEntd"
	case "6" : SQL = SQL & "ORDER BY B.brandname"
	case "7" : SQL = SQL & "ORDER BY B.brandName DESC"
	case else : SQL = SQL & "ORDER BY A.flag1 DESC"
end select

set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if

dim brandName
brandName = RS("brandName")

dim SEtitle, SEdescription, SEkeywords, topText

sql	=	"select	seTitle, seDescription, seKeywords, topText, bottomText, text1, h1tag" & vbcrlf & _
		"from	xcategorytext" & vbcrlf & _
		"where	site_id = 2" & vbcrlf & _
		"	and	categoryID = '" & categoryid & "'" & vbcrlf 
session("errorSQL") = sql
set objRsSEO = oConn.execute(sql)

if not objRsSEO.eof then
	SEtitle			=	objRsSEO("seTitle")
	SEdescription	=	objRsSEO("seDescription")
	SEkeywords		=	objRsSEO("seKeywords")
	topText			=	objRsSEO("topText")
	bottomText		=	objRsSEO("bottomText")
	catText			=	objRsSEO("text1")
	H1				=	objRsSEO("h1tag")
end if
objRsSEO = null

dim strBreadcrumb, headerImgAltText
strBreadcrumb = "Bluetooth Accessories"
%>

<!--#include virtual="/includes/template/top_index.asp"-->

								<td width="5">&nbsp;</td>
								<td width="775" valign="top" style="padding-top:10px;">
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td class="top-sublink-gray">
												<a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
												<span class="top-sublink-blue">Cell Phone Bluetooth</span>
											</td>
										</tr>
										<tr>
											<td width="100%" style="padding-top:5px;" title="<%=headerImgAltText%>">
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                	<tr>
                                                    	<td style="border-bottom:1px solid #ccc; padding-bottom:5px;"><h1><%=H1%></h1></td>
                                                    	<td rowspan="2" valign="middle" align="right" width="200"><img src="/images/bluetooth.jpg" border="0" width="190" height="119" alt="Bluetooth Headsets / Hands-Free / Headphones" title="Bluetooth Headsets / Hands-Free / Headphones"></td>
                                                    </tr>
                                                	<tr>
                                                    	<td><p class="static-content-font" style="text-align:justify;"><%=topText%></p></td>
                                                    </tr>
                                                </table>
											</td>
										</tr>
										<tr>
											<td height="40" align="right" valign="middle">
												<table width="426" border="0" cellspacing="0" cellpadding="0">
													<form id="form1" name="form1" method="post" action="/bluetooth.html">
													<tr>
														<style type="text/css">
														<!--
														.sortStyle {
														background-color:#ccccff;
														}
														-->
														</style>
														<td width="213" align="right" valign="middle"></td>
														<td width="213" align="center" valign="middle">
															<select name="nSorting" id="nSorting" onChange="this.form.submit();">
																<option value="0" class="sortStyle">Sort By +++++++</option>
																<option value="1" class="sortStyle"<%if nSorting = "1" then response.write " selected"%>>Best Selling</option>
																<option value="2" class="sortStyle"<%if nSorting = "2" then response.write " selected"%>>Price: High to Low</option>
																<option value="3" class="sortStyle"<%if nSorting = "3" then response.write " selected"%>>Price: Low to High</option>
																<option value="4" class="sortStyle"<%if nSorting = "4" then response.write " selected"%>>Product: Newest to Oldest</option>
																<option value="5" class="sortStyle"<%if nSorting = "5" then response.write " selected"%>>Product: Oldest to Newest</option>
																<option value="6" class="sortStyle"<%if nSorting = "6" then response.write " selected"%>>Brand Name: A - Z</option>
																<option value="7" class="sortStyle"<%if nSorting = "7" then response.write " selected"%>>Brand Name: Z - A</option>
															</select>
														</td>
													</tr>
													</form>
												</table>
											</td>
										</tr>
										<tr>
											<td width="100%" align="center" valign="top">
												<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
													<tr>
														<td width="100%" align="center" valign="top">
															<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
																<tr>
																	<%
																	dim altText
																	a = 0
																	set fso = CreateObject("Scripting.FileSystemObject")
																	do until RS.eof
																		if fso.fileExists(server.mappath("/productPics/thumb/" & RS("itempic_CO"))) then
																			altText = brandName & " " & modelName & " " & singularSEO(categoryName) & ": " & RS("itemDesc_CO")
																		%>
																		<td width="193" height="100%" align="center" valign="top">
																			<table width="180" height="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td width="170" height="120" align="center" valign="middle">
																						<a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc_CO"))%>.html"><img src="/productPics/thumb/<%=RS("itempic_CO")%>" border="0" alt="<%=altText%>"></a>
																					</td>
																				</tr>
																				<tr><td height="8" align="center" valign="top"><img src="/images/spacer.gif" width="1" height="8" border="0"></td></tr>
																				<tr>
																					<td height="37" align="center" valign="top">
																						<a class="static-content-font-link" href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc_CO"))%>.html" title="<%=altText%>"><%=RS("itemDesc_CO")%></a>
																					</td>
																				</tr>
																				<tr><td height="8" align="center" valign="top"><img src="/images/spacer.gif" width="1" height="8" border="0"></td></tr>
																				<tr>
																					<td align="center" valign="bottom" class="cat-font">
																						Retail Price: <s><%=formatCurrency(RS("price_retail"))%></s>
																					</td>
																				</tr>
																				<tr><td height="3" align="center" valign="top"><img src="/images/spacer.gif" width="1" height="3" border="0"></td></tr>
																				<tr>
																					<td align="center" valign="bottom" class="nevi-font-bold">
																						Wholesale Price: <span class="red-price-small"><%=formatCurrency(RS("price_CO"))%></span>
																					</td>
																				</tr>
																				<tr>
																					<td align="center" valign="bottom" style="padding-top:5px;">
																						<a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc_CO"))%>.html"><img src="/images/moreInfo.png" width="94" height="20" border="0" alt="More Info"></a>
																					</td>
																				</tr>
																			</table>
																		</td>
																		<%
																			a = a + 1
																			if a = 4 then
																				response.write "</tr><tr><td align=""center"" valign=""middle"" colspan=""4"" height=""25""><img src=""/images/hline4.jpg"" width=""750"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
																				a = 0
																			end if
																		end if
																		RS.movenext
																	loop
																	fso = null
																	if a = 1 then
																		response.write "<td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td>" & vbcrlf
																	elseif a = 2 then
																		response.write "<td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td>" & vbcrlf
																	elseif a = 3 then
																		response.write "<td width=""170"">&nbsp;</td>" & vbcrlf
																	end if
																	%>
																</tr>
															</table>
														</td>
													</tr>
													<%if bottomText <> "" then%>
														<tr>
															<td align="center">
																<p>&nbsp;</p>
																<table border="0" cellspacing="0" cellpadding="2" class="thin-border">
																	<tr>
																		<td align="left" valign="top" class="contain">
																			<p class="static-content-font"><%=bottomText%></p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													<%end if%>
													<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
													<tr>
														<td>&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>

<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->
