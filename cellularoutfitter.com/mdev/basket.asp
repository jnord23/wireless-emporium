<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "Basket"
	
	dim itemID : itemID = prepInt(request.Form("itemID"))
	dim qty : qty = prepInt(request.Form("qty"))
	dim mySession : mySession = prepStr(request.cookies("mySession"))
	dim cartTotal : cartTotal = 0
	dim promoItemID : promoItemID = PrepInt(request.Form("promoItemID"))
	dim prodAdj : prodAdj = 0
	dim removePromo : removePromo = prepInt(request.QueryString("removePromo"))
	if removePromo = 1 then session("promocode") = null	

	if mySession = "" then
		mySession = session.SessionID
		response.cookies("mySession") = mySession
		response.cookies("mySession").expires = now+30
	end if

	if instr(request.ServerVariables("SERVER_NAME"),"mdev") > 0 then
		checkoutLink = "/checkout1.html"
	else
		checkoutLink = "https://m.cellularoutfitter.com/checkout1.html"
	end if
	
	if itemID > 0 then
		sql = "delete from shoppingCart where sessionID = " & mySession & " and purchasedOrderID is not null"
		session("errorSQL") = sql
		oConn.execute(sql)
		if qty = 0 then
			sql = "delete from shoppingCart where itemID = " & itemID & " and sessionID = " & mySession
		else
			sql =	"if (select count(*) from shoppingCart where store = 2 and itemID = " & itemID & " and sessionID = " & mySession & ") > 0 " &_
						"update shoppingCart " &_
						"set qty = " & qty & " " &_
						"where itemID = " & itemID & " and sessionID = " & mySession & " " &_
					"else " &_
						"insert into shoppingCart (" &_
							"store,sessionID,accountID,itemID,qty,mobileOrder,ipAddress" &_
						") values(" &_
							"2," & mySession & ",0," & itemID & "," & qty & ",1,'" & request.ServerVariables("REMOTE_ADDR") & "'" &_
						")"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		
		if promoItemID > 0 then
			if instr(sql,"insert into shoppingCart") > 0 then
				sql =	"if (select inv_qty from we_Items where itemID = " & promoItemID & ") > 0 " &_
							"if (select count(*) from shoppingCart where sessionID = '" & mySession & "' and itemID = " & promoItemID & ") = 0 " &_
								"insert into ShoppingCart " &_
									"(store,sessionID,itemID,qty,customCost,lockQty,ipAddress,mobileOrder) " &_
									"values " &_
									"(2,'" & mySession & "'," & promoItemID & ",1,0,1,'" & request.ServerVariables("REMOTE_ADDR") & "',1)"
				session("errorSQL") = sql
				oConn.execute SQL
			end if
		end if
		
		prodAdj = 1
		'response.Redirect("/basket.html")
	end if
	
	sql = "select * from shoppingCart where store = 2 and sessionID = '" & mySession & "' and purchasedOrderID IS NULL and customCost is null"
	set rs = oConn.execute(sql)
	
	if rs.EOF then
		sql = "delete from ShoppingCart where store = 2 and sessionID = '" & mySession & "'"
		oConn.execute SQL
	end if
	
	sql = "exec sp_itemsInBasket 2, " & mySession
	session("errorSQL") = sql
	nSubTotal = 0	'this is for promo calculation on "/cart/includes/inc_promoFunctions.asp"
	set cartItemsRS = oConn.execute(sql)
	
	if cartItemsRS.EOF then
		if prodAdj = 1 then
			if prepStr(request.Cookies("customerEmail")) <> "" then
				sql = "exec sp_remarketing 2," & mySession & ",'" & request.Cookies("customerEmail") & "',0"
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
		end if
	else
		if prodAdj = 1 then
			if prepStr(request.Cookies("customerEmail")) <> "" then
				sql = "exec sp_remarketing 2," & mySession & ",'" & request.Cookies("customerEmail") & "',1"
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
		end if
		
		do while not cartItemsRS.EOF
			lineQty = cartItemsRS("qty")
			linePrice = cartItemsRS("price_CO")
			if not isnull(cartItemsRS("customCost")) then price = cartItemsRS("customCost")
			nSubTotal = nSubTotal + (linePrice*lineQty)
			cartItemsRS.movenext
		loop
		cartItemsRS.movefirst
	end if
%>
<!--#include virtual="/template/promoCalc.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/template/topCO.asp"-->
<%
	discountTotal = prepInt(discountTotal)
	discountedCartTotal = 0
    promoCodeUsed = request.form("txtPromo")
    promoCodeIsValid = false

    if request.form("submitted") and request.form("submitted") <> 1 then
        promoCodeIsValid = true
    end if

%>
<script>
window.WEDATA.pageType = "cart";
window.WEDATA.pageData = {
    cartItems: [],
    promoCode: <%= jsStr(promoCodeUsed) %>,
    promoIsValid: <%= jsStr(LCase(promoCodeIsValid)) %>,
    couponId: <%= jsStr(couponId) %>,
    subTotal: <%= jsStr(nSubtotal) %>,
    weight: <%= jsStr(totalWeight) %>
};
</script>
<script>
	window.onload = function () {
	<%if request.form("submitted") = "1" and prepStr(sPromoCode) <> "" then%>
		var p = $('#cartSummaryBox').position();
		$('html,body').animate({ scrollTop: p.top-10 }, 'slow');
	<%end if%>
	}
</script>
<script language="javascript" src="/template/js/basket.js"></script>
<div class="cartItemCnt">
	<div class="centerContain pd10">You have <span class="orangeCntTxt"><%=miniTotalQuantity%></span> in your cart:</div>
</div>
<% if miniTotalQuantity = "0 Items" then %>
<div class="noItemsInCart">YOU DO NOT HAVE ANY ITEMS IN YOUR SHOPPING CART</div>
<% else %>
<div class="cartReview">
	<div class="centerContain">
		<%
        itemDetails = ""
        do while not cartItemsRS.EOF
            itemID = cartItemsRS("itemID")
            qty = cartItemsRS("qty")
            itemDesc = cartItemsRS("itemDesc_CO")
            itemPic = cartItemsRS("itemPic_CO")
            price = cartItemsRS("price_CO")
            retail = cartItemsRS("price_retail")
			lockQty = cartItemsRS("lockQty")
			customCost = cartItemsRS("customCost")
			if not isnull(customCost) then price = customCost
            itemDetails = itemDetails & qty & "##" & itemID & "##" & itemDesc & "##" & price & "@@"
        %>
        <div class="cartItem">
            <div class="fl itemPic"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html"><img src="/productpics/big/<%=itemPic%>" width="100" height="100" border="0" /></a></div>
            <div class="fl cartItemDetails">
                <div class="itemDesc"><%=itemDesc%></div>
                <div class="itemQty">
                    <div class="fl qtyTxt">Qty.</div>
                    <% if lockQty then %>
                    <div class="fl lockQtySelect"><%=qty%></div>
                    <% else %>
                    <div class="fl qtySelect">
                        <form name="itemForm_<%=itemID%>" action="/basket.html" method="post">
                        <select name="qty" class="invisibleSelect" onchange="document.itemForm_<%=itemID%>.submit();">
                            <% for i = 0 to 20 %>
                            <option value="<%=i%>"<% if qty = i then %> selected="selected"<% end if %>><%=i%></option>
                            <% next %>
                        </select>
                        <input type="hidden" name="itemID" value="<%=itemID%>" />
                        </form>
                    </div>
                    <% end if %>
                    <div style="clear:both;"></div>
                    <div class="inStock">
                        <div class="fl greenCheck"></div>
                        <div class="fl inStockTxt">In Stock - Ships Today!</div>
                    </div>
                    <div style="clear:both;"></div>
                    <% if qty > 1 then %>
                    <div class="basePrice">Item Price: <%=formatCurrency(price,2)%></div>
                    <div class="totalPrice">Item Total: <%=formatCurrency(price*qty,2)%></div>
                    <% else %>
                    <div class="totalPrice">Item Price: <%=formatCurrency(price,2)%></div>
                    <% end if %>
                    <div style="clear:both;"></div>
                    <div class="deleteItem" data-itemid="<%=itemID%>" onclick="br_cartTracking('remove', '<%=itemID%>', '', '<%=replace(itemDesc, "'", "\'")%>', ''); removeItem(<%=itemID%>);">
                        <div class="fl deleteItemIcon"></div>
                        <div class="fl deleteItemTxt">Remove Item(s)</div>
                    </div>
                </div>
            </div>
        </div>
        <%
            cartTotal = cartTotal + (price*qty)
            cartItemsRS.movenext
        loop
        cartItemsRS = null
        
		discountedCartTotal = cartTotal - discountTotal
		
        if itemDetails <> "" then
            itemDetailsArray = split(left(itemDetails,len(itemDetails)-2),"@@")
			
			if prepStr(sPromoCode) = "" then
        %>
        <div id="promoTitle" class="promoBox">
        	<a href="javascript:showPromoField()" class="promoLnk">Do you have a promo code?</a>
			<%if request.form("submitted") = "1" then%>
            <div class="clr" style="color:#f00;">The code you entered is invalid.</div>
            <%end if%>
        </div>
        <div id="frmPromo" style="display:none;">
        	<form name="frmPromo" method="post">
        	<div class="cartItem">
            	<div class="tb" style="padding:10px;">
                    <div class="tb" style="color:#000034; font-weight:bold;">Do you have a promo code?</div>
                    <div class="tb" style="padding-top:5px;">
                        <div class="fl"><input type="text" name="txtPromo" value="Enter promo code" class="promoField" onclick="this.value=''" /></div>
                        <div class="fl"><div class="promoApply" onclick="submitPromoCode()">Apply</div></div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="submitted" value="1" />
            </form>
        </div>
       	<%
			end if
		%>
        <div id="cartSummaryBox" class="cartItem">
            <div class="cartSummaryTitle">CART SUMMARY</div>
            <div class="summaryRow">
                <div class="fl rowTitle">Cart Total:</div>
                <div class="fr rowValue"><%=formatCurrency(cartTotal,2)%></div>
            </div>
            <%
			if sPromoCode <> "" then
				session("promocode") = sPromoCode
			%>
            <div class="summaryRow">
                <div class="fl rowTitle">Discount:</div>
                <a href="/basket.html?removePromo=1"><div class="fr deleteItemIcon" style="margin:-2px 0px 0px 2px;"></div></a>
                <div class="fr rowValue discountPrice"><%=formatcurrency(0-discountTotal,2)%></div>
            </div>
			<%end if%>
            <div class="summaryRow">
                <div class="fl rowTitle">Shipping Total:</div>
                <div class="fr rowValue">--</div>
            </div>
            <div class="summaryRowBlue">
                <div class="fl rowTitle">Subtotal:</div>
                <div class="fr rowValue"><%=formatCurrency(discountedCartTotal,2)%></div>
            </div>
        </div>
        <%
        end if
        %>
    </div>
</div>
<% if prepInt(emphasizePayPal) = 1 then %>
<div class="payPalBox1">
    <div class="payPalBox">
        <div class="payPalText">Secure 2-Click Checkout Using</div>
        <div class="payPalPrimary" onclick="document.PaypalForm_primary.submit()">
            <form name="PaypalForm_primary" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.html" style="margin:0px;">
                <%
                itemLap = 0
                for i = 0 to ubound(itemDetailsArray)
                    singleItemArray = split(itemDetailsArray(i),"##")
                    qty = singleItemArray(0)
                    itemID = singleItemArray(1)
                    itemDesc = singleItemArray(2)
                    itemPrice = singleItemArray(3)
                %>
                <input type="hidden" name="L_NUMBER<%=itemLap%>" value="<%=itemID%>">
                <input type="hidden" name="L_NAME<%=itemLap%>" value="<%=itemDesc%>">
                <input type="hidden" name="L_QTY<%=itemLap%>" value="<%=qty%>">
                <input type="hidden" name="L_AMT<%=itemLap%>" value="<%=itemPrice%>">
                <%
                    itemLap = itemLap + 1
                next
                
                if sPromoCode <> "" then
                %>
                <input type="hidden" name="promo" value="<%=sPromoCode%>">
                <%
                end if
                %>
                <input type="hidden" name="numItems" value="<%=itemLap%>">
                <input type="hidden" name="paymentAmount" value="<%=discountedCartTotal%>">
                <input type="hidden" name="ItemsAndWeight" value="<%=itemLap%>|<%=cartItems%>">
                <input type="hidden" name="mobileOrder" value="1">
            </form>
        </div>
    </div>
    <div class="otherCheckouts2">Or checkout with...</div>
    <div class="checkoutBoxBlank">
        <a href="<%=checkoutLink%>" title="proceed to checkout"><div class="checkoutBttn2"></div></a>
        <div class="creditCards2"></div>
    </div>
</div>
<div class="payPalBox2">
	<div class="checkoutBox">
        <a href="<%=checkoutLink%>" title="proceed to checkout"><div class="checkoutBttn"></div></a>
        <div class="creditCards"></div>
    </div>
    <div class="otherCheckouts"<% if prepInt(disablePayPal) = 1 then %> style="display:none;"<% end if %>>Or checkout with...</div>
    <div class="payPal" onclick="document.PaypalForm.submit()"<% if prepInt(disablePayPal) = 1 then %> style="display:none;"<% end if %>>
        <form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.html" style="margin:0px;">
            <%
            itemLap = 0
            for i = 0 to ubound(itemDetailsArray)
                singleItemArray = split(itemDetailsArray(i),"##")
                qty = singleItemArray(0)
                itemID = singleItemArray(1)
                itemDesc = singleItemArray(2)
                itemPrice = singleItemArray(3)
            %>
            <input type="hidden" name="L_NUMBER<%=itemLap%>" value="<%=itemID%>">
            <input type="hidden" name="L_NAME<%=itemLap%>" value="<%=itemDesc%>">
            <input type="hidden" name="L_QTY<%=itemLap%>" value="<%=qty%>">
            <input type="hidden" name="L_AMT<%=itemLap%>" value="<%=itemPrice%>">
            <script>
                window.WEDATA.pageData.cartItems.push({itemId: <%= jsStr(itemID) %>, qty: <%= jsStr(qty) %>, price: <%= jsStr(itemPrice) %>});
            </script>

            <%
                itemLap = itemLap + 1
            next
            
            if sPromoCode <> "" then
            %>
            <input type="hidden" name="promo" value="<%=sPromoCode%>">
            <%
            end if
            %>
            <input type="hidden" name="numItems" value="<%=itemLap%>">
            <input type="hidden" name="paymentAmount" value="<%=discountedCartTotal%>">
            <input type="hidden" name="ItemsAndWeight" value="<%=itemLap%>|<%=cartItems%>">
            <input type="hidden" name="mobileOrder" value="1">
        </form>
    </div>
</div>
<% else %>
<div class="checkoutBox">
	<a href="<%=checkoutLink%>" title="proceed to checkout"><div class="checkoutBttn"></div></a>
    <div class="creditCards"></div>
</div>
<div class="otherCheckouts"<% if prepInt(disablePayPal) = 1 then %> style="display:none;"<% end if %>>Or checkout with...</div>
<div class="payPal" onclick="document.PaypalForm.submit()"<% if prepInt(disablePayPal) = 1 then %> style="display:none;"<% end if %>>
	<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.html" style="margin:0px;">
		<%
        itemLap = 0
        for i = 0 to ubound(itemDetailsArray)
            singleItemArray = split(itemDetailsArray(i),"##")
            qty = singleItemArray(0)
            itemID = singleItemArray(1)
            itemDesc = singleItemArray(2)
            itemPrice = singleItemArray(3)
        %>
        <input type="hidden" name="L_NUMBER<%=itemLap%>" value="<%=itemID%>">
        <input type="hidden" name="L_NAME<%=itemLap%>" value="<%=itemDesc%>">
        <input type="hidden" name="L_QTY<%=itemLap%>" value="<%=qty%>">
        <input type="hidden" name="L_AMT<%=itemLap%>" value="<%=itemPrice%>">
        <script>
                window.WEDATA.pageData.cartItems.push({itemId: <%= jsStr(itemID) %>, qty: <%= jsStr(qty) %>, price: <%= jsStr(itemPrice) %>});
        </script>

        <%
            itemLap = itemLap + 1
        next
		
		if sPromoCode <> "" then
		%>
        <input type="hidden" name="promo" value="<%=sPromoCode%>">
        <%
		end if
        %>
        <input type="hidden" name="numItems" value="<%=itemLap%>">
        <input type="hidden" name="paymentAmount" value="<%=discountedCartTotal%>">
        <input type="hidden" name="ItemsAndWeight" value="<%=itemLap%>|<%=cartItems%>">
        <input type="hidden" name="mobileOrder" value="1">
    </form>
</div>
<% end if %>
<% end if %>
<div class="continueShopping">
	<div class="fl continueShoppingArrows"></div>
    <div class="fl continueShoppingTxt"><a href="/" class="linkBlue">CONTINUE SHOPPING</a></div>
</div>
<script>
window.WEDATA.pageData.weight = <%= jsStr(totalWeight) %>;
</script>
<!--#include virtual="/template/bottom.asp"-->