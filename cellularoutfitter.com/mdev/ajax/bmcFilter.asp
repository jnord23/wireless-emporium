<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_BasePage.asp"--><%	
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%

	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	
	if categoryID = 18 then useCatID = 999 else useCatID = categoryID
	
	if useCatID = 16 then
		sql = 	"select	distinct c.id subid, c.carriername subName" & vbcrlf & _
				"from	we_items a join we_carriers c" & vbcrlf & _
				"	on	a.carrierid = c.id left outer join we_pndetails d" & vbcrlf & _
				"	on	a.partnumber = d.partnumber" & vbcrlf & _
				"where	a.typeid = '16' " & vbcrlf & _
				"	and	a.brandid = '" & brandid & "'" & vbcrlf & _
				"	and a.hidelive = 0 " & vbcrlf & _
				"	and (a.inv_qty <> 0 or d.alwaysinstock = 1) " & vbcrlf & _
				"	and a.price_co > 0 	" & vbcrlf & _
				"	and	c.id in (1,2,3,4,5,6,7,8,9,11)" & vbcrlf & _
				"order by subid"
	else
		sql =	"select b.subTypeID subid, b.subTypeName subName " & vbcrlf & _
				"from we_typeMatrix a " & vbcrlf & _
					"left join we_subTypes b on a.origSubTypeID = b.subTypeID " & vbcrlf & _
				"where b.hidelive = 0 and b.subtypeid not in (1064,1031) and (maskTypeID = " & prepInt(useCatID) & ")"
	end if
	if customFilter = 1 then sql = replace(sql,"(maskTypeID","(b.subtypeid = 1284 or maskTypeID")
	session("errorSQL") = sql
	set rsSub = oConn.execute(sql)
	
	sql = "select color, colorCodes from XProductColors order by color"
	session("errorSQL") = sql
	set colorRS = oConn.execute(sql)	
%>
<form name="filterForm">
<div class="tb narrowResults mCenter">
	<div class="closeX" onclick="closeFloatingBox()"></div>
	<div class="tb narrowResultsMainTitle">Narrow Your Results By:</div>
    <div class="tb narrowResultsSubTitleBar" onclick="narrowResultSubClick('style')">
    	<div class="fl narrowResultsSubTitle">Style</div>
        <div id="styleArrow" class="fr narrowResultsContract"></div>
    </div>
    <div class="tb pb10" id="styleOptions">
		<%
        do while not rsSub.EOF
            subID = prepInt(rsSub("subID"))
            subName = rsSub("subName")
            subName = replace(subName,"& Hybrid","")
            subName = replace(subName," Cases","")
            subName = replace(subName," Covers","")
            subName = replace(subName,"-Covers","")
        %>
        <div class="tb filterOptRow">
            <div class="fl filterCheckbox"><input type="checkbox" id="<%=subID%>" name="styleType" value="<%=subID%>" onclick="updateFilter()"<% if carrierid = subID then %> checked="checked"<% end if %> /></div>
            <div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter(<%=subID%>)"><%=subName%></a></div>
        </div>
        <%
            rsSub.movenext
        loop
        %>
    </div>
    <div class="tb narrowResultsSubTitleBar" onclick="narrowResultSubClick('price')">
    	<div class="fl narrowResultsSubTitle">Price Range</div>
        <div id="priceArrow" class="fr narrowResultsExpand"></div>
    </div>
    <div class="tb pb10" id="priceOptions" style="display:none;">
    	<div class="tb filterOptRow">
            <div class="fl"><input type="checkbox" id="price_4" name="priceRange" value="4" onclick="updateFilter()" /></div>
            <div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('price_4')">$0 to $4.99</a></div>
        </div>
        <div class="tb filterOptRow">
            <div class="fl"><input type="checkbox" id="price_5" name="priceRange" value="5" onclick="updateFilter()" /></div>
            <div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('price_5')">$5 to $9.99</a></div>
        </div>
        <div class="tb filterOptRow">
            <div class="fl"><input type="checkbox" id="price_10" name="priceRange" value="10" onclick="updateFilter()" /></div>
            <div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('price_10')">$10 to $19.99</a></div>
        </div>
        <div class="tb filterOptRow">
            <div class="fl"><input type="checkbox" id="price_20" name="priceRange" value="20" onclick="updateFilter()" /></div>
            <div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('price_20')">$20 to $29.99</a></div>
        </div>
        <div class="tb filterOptRow">
            <div class="fl"><input type="checkbox" id="price_30" name="priceRange" value="30" onclick="updateFilter()" /></div>
            <div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('price_30')">$30 and Up</a></div>
        </div>
    </div>
    <div class="tb narrowResultsSubTitleBar" onclick="narrowResultSubClick('color')">
    	<div class="fl narrowResultsSubTitle">Color</div>
        <div id="colorArrow" class="fr narrowResultsExpand"></div>
    </div>
    <div class="tb pb10" id="colorOptions" style="display:none;">
    	<%
		do while not colorRS.EOF
			color = colorRS("color")
			hexColor = colorRS("colorCodes")
		%>
		<div class="tb filterOptRow">
			<div class="fl"><input type="checkbox" id="color_<%=color%>" name="colorSelect" value="<%=color%>" onclick="updateFilter()" /></div>
			<div class="fl colorBox" style="background-color:<%=hexColor%>;" onclick="selectFilter('color_<%=color%>')"></div>
			<div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('color_<%=color%>')"><%=color%></a></div>
		</div>
		<%
			colorRS.movenext
		loop
		%>
    </div>
    <div class="tb narrowResultsBottomBar">
    	<div class="fl narrowResultsReset" onclick="resetFilter()">Reset</div>
        <div class="fr norrowResultsApply" onclick="closeFloatingBox()">Apply</div>
    </div>
</div>
</form>
<%
	call CloseConn(oConn)
%>