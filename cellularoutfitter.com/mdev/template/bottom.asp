    <div class="secureIcons">
    	<div class="fl nortonIcon"><a href="https://sealinfo.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en" target="_blank"><img src="/images/mobile/icons/norton.gif" border="0" /></a></div>
        <div class="fr mcAfeeIcon">
        	<!-- START SCANALERT CODE -->
            <a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.cellularoutfitter.com"><img width="65" height="37" border="0" src="//images.scanalert.com/meter/www.wirelessemporium.com/63.gif" alt="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee SECURE is a Trademark of McAfee, Inc.'); return false;"></a>
            <!-- END SCANALERT CODE -->
        </div>
    </div>
    <%if pageName = "Basket" then%>
    <div style="font-size:11px; padding-top:15px; text-align:center; color:#666;">*Cart ID: <%=mySession%></div>
    <%end if%>
    <!--
	<div class="footerBar">
        <div class="fl footerHome"><a href="/" class="footerLink">HOME</a></div>
        <div class="fl footerContactUs"><a href="/contact.html" class="footerLink">CONTACT US</a></div>
        <div class="fl footerFullSite"><a href="/trackOrder.html" class="navLink" title="Track Order">TRACK ORDER</a></div>
        <div class="fl footerTerms"><a href="/terms.html" class="footerLink">TERMS</a></div>
    </div>
	-->
	<div class="footerBarNew">
        <div class="fl footerHome"><a href="/" class="footerLink">HOME</a></div>
        <!-- <div class="fl footerContactUs"><a href="/contact.html" class="footerLink">CONTACT US</a></div> -->
        <div class="fl footerOrderTrack"><a href="/trackOrder.html" class="navLink" title="Track Order">ORDER TRACKING</a></div>
        <div class="fl footerFaq"><a href="/faq.html" class="footerLink">HELP</a></div>
		<!-- <div class="fl footerTerms"><a href="/terms.html" class="footerLink">TERMS</a></div> -->
    </div>
    <div class="copyriteTxt">
        <% if instr(fullSiteLink,"basket") > 0 then fullSiteLink = replace(fullSiteLink,"basket","cart/basket") %>
		<% if instr(fullSiteLink,"checkout1") > 0 then fullSiteLink = replace(fullSiteLink,"checkout1","cart/checkout") %>
        <% if instr(fullSiteLink,"checkout2") < 1 then %>
        Visit the CellularOutfitter <a href="<%=fullSiteLink%>?mobileAccess=2" class="fullSiteLink">FULL SITE</a>
        <% end if %>
        <br /><br />
        Copyright &copy; <%=year(date)%> CellularOutfitter.com<br />
        All Right Reserved.
    </div>
    <div class="backToTopBar" onclick="backToTop()">
    	<div class="centerContain">
	        <div class="fl backToTopTxt">BACK TO TOP</div>
    	    <div class="fl backToTopArrow"></div>
        </div>
    </div>
</div>
<%call printPIxel(3)%>
<!--#include virtual="/includes/asp/pixels/inc_br.asp"-->
<%
'response.write "welcomeCouponUsed:" & session("welcomeCouponUsed") & "<br>"
'response.write "prepInt(session(""welcomeCouponUsed"")):" & prepInt(session("welcomeCouponUsed")) & "<br>"
%>
<%if prepInt(request.cookies("welcomeCouponUsed")) = 0 and pageName <> "Basket" and pageName <> "Basket2" and pageName <> "Checkout" and pageName <> "Declined" and pageName <> "Confirm" then%>
<!-- 25% floating icon start -->
<div id="icon25" class="discount-container">
    <div class="discount" onclick="showEmailWidget()"></div>
    <div class="btn-close-sml" onclick="closeEmailIcon()"></div>
</div>
<div id="floating-banner" style="display:none;">
	<div class="clickable" onclick="showEmailWidget();">
		<div class="discount-text"></div>
		<div class="tap-here"></div>
	</div>
	<div class="banner-close" onclick="closeEmailIcon2()"></div>
</div>
<%end if%>
<div id="overlay1">
    <div onclick="closeEmailWidget('overlay1')" class="btn-close"></div>
    <div class="modal-title-container">
        <div class="modal-title-left"></div>
        <div class="modal-title-center">
            <div class="check"><img src="/images/mobile/emailpromo/checkmark.png" /></div>
            <div class="modal-title">Save 25% on today's order, now!</div>
        </div>
        <div class="modal-title-right"></div>
    </div>
    <div class="modal-content">
        <p>Sign-up to receive our emails and take 25% off your order right now!</p>
        <p>As a member, you'll receive access to secret sales, special discounts and insider information before anyone else!</p>
        <p style="text-align:center;"><input id="id_modal_email" class="modal-email" type="text" name="email" value="Enter your email address and save!" onfocus="this.value='';" /></p>
        <p style="text-align:center;">
            <input type="button" class="modal-button" value="SUBMIT" onclick="addNewsletter(document.getElementById('id_modal_email').value,'Modal Widget');" />
        </p>
    </div>
</div>
<div id="overlay2"></div>
<div id="fade" onclick="closeEmailWidget('overlay1');"></div>
<!-- 25% floating icon end -->

<%
call CloseConn(oConn)
%>
</body>
</html>
<div id="dumpZone" style="display:none;"></div>
<div id="testZone"></div>
<script type="text/javascript">
	var useHttps = <%=prepInt(securePage)%>;
	var winLoc = window.location.toString();
	winLoc = winLoc.toLowerCase();
	
	<%if prepStr(request.QueryString("utm_newsletter")) <> "" then%>
	setTimeout(function(){ 
		showEmailWidget();
		addNewsletter('<%=prepStr(request.QueryString("utm_newsletter"))%>','newsletter'); 
	}, 500);
	<%end if%>
	
	<% if instr(request.ServerVariables("SERVER_NAME"),"mdev") < 1 then %>
	if (useHttps == 1 && winLoc.indexOf("https:") < 0) {
		window.location = winLoc.replace("http:","https:")
	}
	else if (useHttps == 0 && winLoc.indexOf("http:") < 0) {
		window.location = winLoc.replace("https:","http:")
	}
	<% end if %>
	
	ajax('/ajax/itemsInCart.asp?mySession=<%=mySession%>','inCart')

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-1097464-3']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
//		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
 
	var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-38672929-1']);
	 _gaq.push(['_setDomainName', 'cellularoutfitter.com']);
	 _gaq.push(['_trackPageview']);
	
	(function() {
	   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 
	 function addNewsletter(email,loc) {
		 _gaq.push(['_trackEvent', 'Email Signup', 'Submission', 'Mobile', loc]);
		 if(typeof(_vis_opt_top_initialize) == "function") {
			// Code for Custom Goal: Email Sign-Up
			 _vis_opt_goal_conversion(202);
			// uncomment the following line to introduce a half second pause to ensure goal always registers with the server
			// _vis_opt_pause(500);
		}
		
		document.getElementById('overlay2').innerHTML = '';
		ajax('/ajax/newsletter.asp?semail=' + email + '&brandID=<%=prepInt(brandID)%>&modelID=<%=prepInt(modelID)%>&typeID=<%=prepInt(typeID)%>&itemID=<%=prepInt(itemID)%>','overlay2');
		jQuery(window).trigger('newsletterSignup', [loc, email]);
		showHide('overlay2', null);
		showHide(null,'overlay1');
	}
</script>
<!-- new server! -->