	if (runSalesTimer == 1) {
		setTimeout("changeSalesTime()",1000)
	}
	
	function changeSalesTime() {
		var saleMinutes = parseInt(document.getElementById("saleMinutes").innerHTML);
		var saleSeconds = parseInt(document.getElementById("saleSeconds").innerHTML);
		
		if (saleSeconds > 0) {
			saleSeconds--
			if (saleSeconds < 10) {
				document.getElementById("saleSeconds").innerHTML = "0" + saleSeconds
			}
			else {
				document.getElementById("saleSeconds").innerHTML = saleSeconds
			}
			setTimeout("changeSalesTime()",1000)
		}
		else if (saleMinutes > 0) {
			saleMinutes--
			document.getElementById("saleMinutes").innerHTML = saleMinutes
			document.getElementById("saleSeconds").innerHTML = 59
			setTimeout("changeSalesTime()",1000)
		}
		else {
			alert("Your products are not longer guaranteed to be in stock!\nPlease hurry to complete your purchase to have the best chance at still getting your items.")
		}
	}
	
	function getCustomerData(email) {
		ajax('/ajax/customerData.asp?email=' + email,'customerInfo')
		setTimeout("displayData(document.getElementById('customerInfo').innerHTML)",2000)
	}
	
	function displayData(dataReturn) {
		if (dataReturn != "no data" && dataReturn != "") {
			var dataArray = dataReturn.split("@@")
			if (dataArray[0] != undefined && dataArray[1] != undefined && dataArray[2] != undefined) {
				alert("Welcome back!\nWe have saved your address information from a previous purchase.\nPlease review the saved info before checking out.")
				if (document.checkout1Form.fname.value == "") { document.checkout1Form.fname.value = dataArray[0] }
				if (document.checkout1Form.lname.value == "") { document.checkout1Form.lname.value = dataArray[1] }
				if (document.checkout1Form.bAddress1.value == "") { document.checkout1Form.bAddress1.value = dataArray[2] }
				if (document.checkout1Form.bAddress2.value == "") { document.checkout1Form.bAddress2.value = dataArray[3] }
				if (document.checkout1Form.bCity.value == "") { document.checkout1Form.bCity.value = dataArray[4] }
				if (document.checkout1Form.bState.selectedIndex == 0) {
					var stateOptions = document.checkout1Form.bState.length
					for (i=0;i<stateOptions;i++) {
						if (document.checkout1Form.bState.options[i].value == dataArray[5]) {
							document.checkout1Form.bState.selectedIndex = i
						}
					}
				}
				if (document.checkout1Form.bZip.value == "") {
					document.checkout1Form.bZip.value = dataArray[6]
				}
				if (document.checkout1Form.phone.value == "") { document.checkout1Form.phone.value = dataArray[7] }
				
				if (dataArray[2] != dataArray[8] || dataArray[6] != dataArray[12]) {
					document.checkout1Form.shippingAddr.checked = true
					shipAddrOptions(document.checkout1Form.shippingAddr.checked)
					document.checkout1Form.sAddress1.value = dataArray[8]
					document.checkout1Form.sAddress2.value = dataArray[9]
					document.checkout1Form.sCity.value = dataArray[10]
					var stateOptions = document.checkout1Form.sState.length
					for (i=0;i<stateOptions;i++) {
						if (document.checkout1Form.sState.options[i].value == dataArray[11]) {
							document.checkout1Form.sState.selectedIndex = i
						}
					}
					document.checkout1Form.sZip.value = dataArray[12]
				}
			}
		}
	}
	
	function remarket(email,mySession) {
		ajax('/ajax/remarketing.asp?email=' + email + '&mySession=' + mySession + '&siteID=2','dumpZone')
	}
	
	function cartSummary() {
		if (document.getElementById("expandCartIcon").className == "fl expandCart") {
			document.getElementById("expandCartIcon").className = "fl contractCart";
			document.getElementById("onOrder").style.display = '';
		}
		else {
			document.getElementById("expandCartIcon").className = "fl expandCart";
			document.getElementById("onOrder").style.display = 'none';
		}
	}