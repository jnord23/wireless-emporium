//alert("2: " + document.documentElement.clientHeight)
var winHeight = window.innerHeight;
var winWidth = window.innerWidth;

if (winHeight > winWidth) {
	//subtract 320 for the header
	winHeight = winHeight - 320;
}
else {
	winHeight = winHeight - 120;
}
//divide by 200 for each row
var visibleRows = Math.ceil(winHeight / 200);
//divide by 175 for each column
var visibleCols = Math.ceil(winWidth / 175);
//total visible items at load time
var maxVisibleProd = visibleCols * visibleRows;
//show the visible items
for (i=1;i<=maxVisibleProd;i++) {
	document.getElementById("singleDeviceBox_" + i).innerHTML = document.getElementById("singleDeviceBox_" + i).innerHTML.replace("tempimg","img");
	document.getElementById("singleDeviceBox_" + i).style.display = '';
	var scrollValue = document.documentElement.scrollTop;
	if (scrollValue == 0) { scrollValue = document.body.scrollTop; }
	if (scrollValue > 0) { userScrolled() }
}

function userScrolled() {
	var scrollValue = document.documentElement.scrollTop;
	if (scrollValue == 0) { scrollValue = document.body.scrollTop; }
	scrollValue = scrollValue + 175;
	visibleRows = Math.ceil((winHeight + scrollValue) / 200);
	var newMax = visibleCols * visibleRows;
	if (newMax > maxVisibleProd) {
		for (i=maxVisibleProd;i<=newMax;i++) {
			document.getElementById("singleDeviceBox_" + i).innerHTML = document.getElementById("singleDeviceBox_" + i).innerHTML.replace("tempimg","img");
			document.getElementById("singleDeviceBox_" + i).style.display = '';
		}
		maxVisibleProd = newMax;
	}
}