<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "Help"
	
	dim reset : reset = prepInt(request.QueryString("reset"))
	if reset = 1 then
		response.Cookies("mobileBrandID") = 0
		response.Cookies("mobileModelID") = 0
	end if
	
	sql = "select typeID, typeName_CO from we_types where typeID not in (20,24,22,12,14,9,4,15,23,13,17,21,25) order by order_CoMobile"
	session("errorSQL") = sql
	set categoryListRS = oConn.execute(sql)
	
	dim section : section = request.QueryString("section")
	dim faqid : faqid = request.QueryString("faqid")
	dim search : search = request.QueryString("search")
	
	
	sql = "select * from XFaq"
	session("errorSQL") = sql
	set testRS = oConn.execute(sql)
	
	function strip_tags(strHTML)
		dim regEx
		Set regEx = New RegExp
		With regEx
			.Pattern = "<(.|\n)+?>"
			.IgnoreCase = true
			.Global = true
		End With
		strip_tags = regEx.replace(strHTML, "")
	end function
	
%>
<!--#include virtual="/template/topCO.asp"-->
<link href='/template/css/faq.css' rel='stylesheet' type='text/css' />
<div class="faq-search">
	<div class="centerContain">
		<form name="frm_search" method="get" onsubmit="return checkSearchLength();">
			<input type="text" name="search" id="search" class="fl search-field" value="Search by keyword or sku #" onfocus="if(this.value='Search by keyword or sku #') this.value = '';" onblur="if(!this.value) this.value='Search by keyword or sku #';" />
			<input type="submit" class="fl search-submit" value="" />
		</form>
	</div>
</div>
<div class="faq">
	<div class="centerContain">
		<% if search = "" and section = "" and faqid = "" then 	%>
		<div class="faq-crumb">Help</div>
		<div class="faq-sections">
			<ul>
				<%
					sqlSections = "SELECT * FROM XFaqCategory WHERE siteid = 2"
					session("errorSQL") = sqlSections
					set sectionLinksRS = oConn.execute(sqlSections)
					if not sectionLinksRS.EOF then
						do until sectionLinksRS.EOF
							section_id = sectionLinksRS("category_id")
							section_name = sectionLinksRS("category_name")
				%>
					<li><a href="faq.html?section=<%=section_id%>"><%=section_name%></a></li>
				<%
							sectionLinksRS.movenext
						loop
					end if
				%>
			</ul>
		</div>
		<% 
			elseif section <> "" and faqid = "" then 
				sqlSection = "SELECT * FROM XFaqCategory WHERE category_id = '" & section & "' AND siteid=2"
				session("errorSQL") = sqlSection
				set sectionRS = oConn.execute(sqlSection)
				if not sectionRS.EOF then
					section_name = sectionRS("category_name")
		%>
		<div class="faq-crumb"><a href="faq.html">Help</a> &raquo; <%=section_name%></div>
		<div class="faq-sections">
			<ul>
			<%
					sqlFaq = "SELECT f.* FROM XFaq as f LEFT JOIN XFaqToCategory as f2c ON f2c.faq_id = f.faq_id WHERE f2c.category_id = '" & section & "' AND f.siteid = 2 ORDER BY f2c.ordernum"
					set faqLinksRS = oConn.execute(sqlFaq)
					if not faqLinksRS.EOF then
						do until faqLinksRS.EOF
							faq_id = faqLinksRS("faq_id")
							faq_title = faqLinksRS("faq_title")
			%>
				<li><a href="faq.html?section=<%=section%>&faqid=<%=faq_id%>"><%=faq_title%></a></li>
			<%
							faqLinksRS.movenext
						loop
					end if
				end if
			%>
			</ul>
		</div>
		<%
			elseif faqid <> "" then
				if section <> "" then
					sqlFaq = "SELECT f2c.category_id, f2c.ordernum, c.category_name, f.faq_id, f.faq_title, f.faq_content FROM XFaq as f LEFT JOIN XFaqToCategory as f2c ON f2c.faq_id = f.faq_id AND f2c.category_id = " & section & " LEFT JOIN XFaqCategory as c ON c.category_id = f2c.category_id WHERE f.faq_id = '" & faqid & "' AND f.siteid = 2"
				else 
					sqlFaq = "SELECT f2c.category_id, f2c.ordernum, c.category_name, f.faq_id, f.faq_title, f.faq_content FROM XFaq as f LEFT JOIN XFaqToCategory as f2c ON f2c.faq_id = f.faq_id LEFT JOIN XFaqCategory as c ON c.category_id = f2c.category_id WHERE f.faq_id = '" & faqid & "' AND f.siteid = 2"
				end if
				
				set faqRS = oConn.execute(sqlFaq)
				if not faqRS.EOF then
					faq_category_id = faqRS("category_id")
					faq_ordernum = faqRS("ordernum")
					faq_category_name = faqRS("category_name")
					faq_category_id = faqRS("category_id")
					faq_title = faqRS("faq_title")
					faq_content = faqRS("faq_content")
					faq_content = replace(faq_content, "track-your-order.html", "trackOrder.html")
					faq_content = replace(faq_content, "[promo code screen shot]", "<img src=""/images/faq/CO-promo-location-mobile.jpg"" border=""0"" width=""310"" />")
					faq_content = replace(faq_content, "javascript:doContactus();", "/contact-us.html")
					
					sqlNext = "SELECT TOP 1 f.*, f2c.category_id, f2c.ordernum FROM XFaq as f LEFT JOIN XFaqtoCategory as f2c ON f2c.faq_id = f.faq_id WHERE f2c.category_id = " & faq_category_id & " AND f.siteid = 2 AND f2c.ordernum > " & faq_ordernum & " ORDER BY f2c.ordernum"
					set nextRS = oConn.execute(sqlNext)
					if not nextRS.EOF then
						next_faq_id = nextRS("faq_id")
						next_section_id = Trim(nextRS("category_id"))
					else
						sqlNext = "SELECT TOP 1 f.*, f2c.category_id, f2c.ordernum FROM XFaq as f LEFT JOIN XFaqtoCategory as f2c ON f2c.faq_id = f.faq_id WHERE f2c.category_id > " & faq_category_id & " AND f.siteid = 2 ORDER BY f2c.category_id, f2c.ordernum"
						set nextRS = oConn.execute(sqlNext)
						if not nextRS.EOF then
							next_faq_id = nextRS("faq_id")
							next_section_id = Trim(nextRS("category_id"))
						else 
							sqlNext = "SELECT TOP 1 f.*, f2c.category_id, f2c.ordernum FROM XFaq as f LEFT JOIN XFaqtoCategory as f2c ON f2c.faq_id = f.faq_id WHERE f.siteid = 2 ORDER BY f2c.category_id, f2c.ordernum"
							set nextRS = oConn.execute(sqlNext)
							if not nextRS.EOF then
								next_faq_id = nextRS("faq_id")
								next_section_id = Trim(nextRS("category_id"))
							else 
								next_faq_id = 0
							end if
						end if
					end if
		%>
		<div class="faq-crumb"><a href="faq.html">Help</a> &raquo; <a href="faq.html?section=<%=faq_category_id%>"><%=faq_category_name%></a></div>
		<div class="faq-content">
			<h2><%=faq_title%></h2>
			<%=faq_content%>
		</div>
		<% if next_faq_id <> "" then %>
		<div class="faq-next"><a href="faq.html?section=<%=next_section_id%>&faqid=<%=next_faq_id%>">Next Question &raquo;</a></div>
		<% end if %>
		<%
				end if
			else
				arr_search = split(search)
				where = ""
				for each keyword in arr_search
					keyword = replace(keyword, "'", "&#039;")
					if where <> "" then
						where = where & " OR "
					end if
					
					where = where & " faq_title LIKE '%" & keyword & "%' OR faq_content LIKE '%" & keyword & "%'"
				next
				sqlSearch = "SELECT * FROM XFaq"
				if where <> "" then
					sqlSearch = sqlSearch & " WHERE " & where
				end if
				
				set searchRS = oConn.execute(sqlSearch)
		%>
			<div class="faq-crumb"><a href="faq.html">Help</a> &raquo; Search Results for '<%=search%>'</div>
			<ul>
		<%
				if not searchRS.EOF then
					do until searchRS.EOF
						faq_id = searchRS("faq_id")
						faq_title = searchRS("faq_title")
						faq_content = searchRS("faq_content")
						for each keyword in arr_search
							faq_title = replace(faq_title, keyword, "<span class='highlight'>" & keyword & "</span>")
							stripped = strip_tags(faq_content)
							faq_content = replace(strip_tags(faq_content), keyword, "<span class='highlight'>" & keyword & "</span>")
							content_length = Len(faq_content)
							if content_length > 200 then
								faq_content = Left(faq_content, 200) & "..."
							end if
						next
		%>
						<li>
							<a href="faq.html?faqid=<%=faq_id%>"><%=faq_title%></a>
							<p><%=faq_content%></p>
						</li>
		<%
						searchRS.movenext
					loop
				end if
		%>
			</ul>
		<%
			end if
		%>
		<div class="contact-title">Not What You're Looking For?</div>
		<div class="contact-button">
			<a href="contact-us.html"><img src="/images/faq/contact-us.png" /></a>
		</div>
	</div>
</div>
<script type="text/javascript">
	function checkSearchLength() {
		var search = document.getElementById('search').value.trim();
		var sch_len = search.length;

		if(sch_len < 2) { 
			alert('Search term should be at least 2 characters.'); 
			return false; 
		}
	}
</script>
<!--#include virtual="/template/bottom.asp"-->