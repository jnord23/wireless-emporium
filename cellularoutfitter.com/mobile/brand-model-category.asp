<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "BMC"
	
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim sortBy : sortBy = prepStr(request.Form("sortBy"))
	if categoryID = 0 then response.Redirect("/")
	if brandID = 0 then response.Redirect("/")
	
	sql = "exec sp_coCategoryDetails " & categoryID
	session("errorSQL") = sql
	set categoryDetailsRS = oConn.execute(sql)
	if categoryDetailsRS.EOF then response.Redirect("/") else categoryName = categoryDetailsRS("categoryName")
	categoryDetailsRS = null
	
	sql = "exec sp_brandDetailsByBrandID " & brandID
	session("errorSQL") = sql
	set brandDetailsRS = oConn.execute(sql)
	if brandDetailsRS.EOF then response.Redirect("/") else brandName = brandDetailsRS("brandName")
	brandDetailsRS = null
	brandName = replace(brandName," Ericsson","")
	brandName = replace(brandName,"/Qualcomm","")
	
	sql = "exec sp_modelDetailsByID " & modelID
	session("errorSQL") = sql
	set modelDetailsRS = oConn.execute(sql)
	if modelDetailsRS.EOF then
		response.Redirect("/")
	else
		modelName = modelDetailsRS("modelName")
		modelImg = modelDetailsRS("modelImg")
	end if
	modelDetailsRS = null
	
	if sortBy = "" then sortBy = "pop"
	
	if categoryID = 5 then
		sql = "exec sp_bmcdProducts " & modelID & ", 5, '" & sortBy & "', 2, 0, 0, 5"
	elseif categoryID = 8 then
		sql = "exec sp_bmcdProducts " & modelID & ", 8, '" & sortBy & "', 2, 0, 0, 8"
	else
		sql = "exec sp_bmcdProducts	" & modelID & ", " & categoryID & ", '" & sortBy & "', 2, 0, " & brandID & ", 0"
	end if
	prodSQL = sql
	session("errorSQL") = sql
	set productsRS = oConn.execute(sql)

'========= BAD nested loop	
'	function reviewScore(partNumber)
'		sql = "select AVG(rating) as avgRating from co_Reviews with (nolock) where PartNumbers = '" & partNumber & "' and approved = 1"
'		session("errorSQL") = sql
'		set reviewRS = oConn.execute(sql)
'		if not reviewRS.EOF then
'			reviewScore = "<img src='/images/mobile/icons/" & cint(reviewRS("avgRating")) & "StarGreen.gif' border='0' />"
'		end if
'	end function

	useTime = now
	totalSeconds = datediff("s",useTime,date+1)
	totalMinutes = totalSeconds/60
	if cint(totalMinutes) > totalMinutes then totalMinutes = cint(totalMinutes)-1 else totalMinutes = cint(totalMinutes)
	totalSeconds = totalSeconds - (totalMinutes*60)
	totalHours = totalMinutes/60
	if cint(totalHours) > totalHours then totalHours = cint(totalHours)-1 else totalHours = cint(totalHours)
	totalMinutes = totalMinutes - (totalHours*60)	
	
	totalSeconds = right("00" & totalSeconds, 2)
	totalMinutes = right("00" & totalMinutes, 2)
%>
<!--#include virtual="/template/topCO.asp"-->
<div id="main">
    <div class="tb breadCrumbBox mCenter">
        <div class="fl crumb"><a href="/" class="breadCrumb">Home</a></div>
        <div class="fl bcSplitBox"><div class="bradCrumbSplit"></div></div>
        <div class="fl crumb"><a href="/c-<%=categoryID%>-<%=formatSEO("cell phone " & categoryName)%>.html" class="breadCrumb"><%=catNameSwap(categoryName)%></a></div>
        <div class="fl bcSplitBox"><div class="bradCrumbSplit"></div></div>
        <div class="fl crumb"><a href="/sc-<%=categoryID%>-sb-<%=brandID%>-<%=formatSEO("cell phone " & categoryName & " for " & brandName)%>.html" class="breadCrumb"><%=brandName%></a></div>
    </div>
    <div class="curCategory">
        <div class="curCat"><%=brandName & " " & modelName & " " & catNameSwap(categoryName)%></div>
        <div class="catImg">
            <%if fso.FileExists("/modelpics/thumbs/" & modelImg) then%>
            <img src="/modelpics/thumbs/<%=modelImg%>" width="70" height="112" alt="<%=brandName & " " & modelName & " " & categoryName%>" />
            <%else%>
            <img src="/modelpics/<%=modelImg%>" width="70" height="112" alt="<%=brandName & " " & modelName & " " & categoryName%>" />
            <%end if%>
        </div>
        <div class="changeCat"><a href="/sc-<%=categoryID%>-sb-<%=brandID%>-cell-phone-<%=formatSEO(categoryName)%>-for-<%=formatSEO(brandName)%>.html" title="Change device" class="blackLink">Choose Another Device</a></div>
    </div>
    <div class="sortBar">
        <div class="tb mCenter">
            <div class="fl sortBox">
                <form name="sortForm" method="post">
                    <select name="sortBy" class="sortObject" onchange="document.sortForm.submit()">
                        <option value="">SORT BY</option>
                        <option value="pop">Popular</option>
                        <option value="new">Newest</option>
                        <option value="low">Lowest Price</option>
                        <option value="high">Highest Price</option>
                    </select>
                    <input type="hidden" name="brandID" value="<%=brandID%>" />
                    <input type="hidden" name="modelID" value="<%=modelID%>" />
                    <input type="hidden" name="categoryID" value="<%=categoryID%>" />
                </form>
            </div>
            <div class="fl filterButton" onclick="showBmcFilter(<%=categoryID%>)">Filter By</div>
        </div>
    </div>
    <div class="tb currentlyViewing">
        <div class="tb mCenter curViewBox">
            <div class="fl eyeIcon"></div>
            <div class="fl cvText">Currently viewing <span id="viewCnt" class="viewCntTxt"></span> items</div>
        </div>
    </div>
    <div class="categoryMenu">
        <%
        dim productCnt : productCnt = 0
        dim productCnt2 : productCnt2 = 0
        dim previousItemID : previousItemID = 0
        dim bypassProd : bypassProd = 0
        do while not productsRS.EOF
            itemID = productsRS("itemID")
            subtypeID = productsRS("subtypeID")
            itemPartNumber = productsRS("partNumber")
            itemDesc = productsRS("itemDesc_CO")
            itemPic = productsRS("itemPic_CO")
            itemPrice = productsRS("price_co")
            itemRetail = productsRS("price_retail")
            reviewCnt = productsRS("reviews")
			reviewAvg = productsRS("reviewAvg")
			if isnull(reviewAvg) then reviewAvg = 0
            hasQty = prepInt(productsRS("hasQty"))
            if len(modelName) > 18 then useClass = "menuTxt2" else useClass = "menuTxt"
            
            displayItem = 1
            
            if hasQty = 0 and prepInt(reviewCnt) = 0 then displayItem = 0
            if itemID = previousItemID then displayItem = 0
            if prepInt(itemPrice) = 0 then displayItem = 0
            
            if displayItem = 0 then bypassProd = bypassProd + 1
            
            if displayItem = 1 then
                productCnt = productCnt + 1
                if productCnt < 11 then
                    useClass = "productRow"
                    useImg = "img"
                else
                    useClass = "productRow2"
                    useImg = "tempimg"
                end if
                
                if fso.fileExists(server.MapPath("/productpics/big/" & itemPic)) then
                    productCnt2 = productCnt2 + 1
        %>
        <div id="singleProdBox_<%=productCnt2%>" class="<%=useClass%>">
            <div class="fl productLeft" onclick="goHere('/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html')">
                <div class="productPic" align="center">
                    <<%=useImg%> src="/productpics/big/<%=itemPic%>" border="0" width="100" height="100" />
                    <% if reviewAvg > 0 then %>
                    <div class="tb">
						<img src='/images/mobile/retina/items/<%=cint(reviewAvg)%>StarGreen.png' width="78" border='0' />
					</div>
					<% end if %>
                </div>
            </div>
            <div class="fl productRight" onclick="goHere('/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html')">
                <div class="productDetails"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html" class="itemDescLink" title=""><%=itemDesc%></a></div>
                <div class="salesTag">
                    <div class="time-left"><span class="hour" id="hour"><%=totalHours%></span> hours left at this price!</div>
                    <div class="hidden-min" id="hidden-min"><%=totalMinutes%></div>
                    <div class="hidden-sec" id="hidden-sec"><%=totalSeconds%></div>
                </div>
                <div class="tb retail">
                    <div class="fl retailTitle">Regular Price:</div>
                    <div class="fl retailPrice"><%=formatCurrency(itemRetail)%></div>
                    <div class="fl retailPriceRed">&nbsp;(<%=formatpercent((itemRetail-itemPrice)/itemRetail,0)%> OFF)</div>
                    <div class="fl retailPriceGrey">&nbsp;(<%=formatpercent((itemRetail-itemPrice)/itemRetail,0)%> OFF)</div>
                </div>
                <div class="tb ourPrice">
                    <div class="fl priceTitle">Wholesale Price:</div>
                    <div class="fl productPrice"><%=formatCurrency(itemPrice)%></div>
                </div>
                <% if hasQty = 1 then %>
                <div class="tb inStockBox">
                    <div class="fl inStockIcon"></div>
                    <div class="fl inStockText">In Stock:</div>
                    <div class="fl inStockText2">Ships within 24 hrs.</div>
                </div>
                <% else %>
                <div class="tb inStockBox" style="color:#F00;">Currently Out of Stock</div>
                <% end if %>
                <div class="hide prodID"><%=itemID%></div>
                <div class="hide" id="filter_subtypeid_<%=productCnt2%>"><%=subtypeID%></div>
                <div class="hide" id="filter_price_<%=productCnt2%>"><%=itemPrice%></div>
                <div class="hide prodDesc" id="filter_itemDesc_<%=productCnt2%>"><%=itemDesc%></div>
            </div>
            <div class="fl" style="width:100%; padding:7px 0px 0px 0px;">
                <div class="fr btnMoreDetail" onclick="goHere('/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html')"></div>
                <div class="fr divMLT" style="margin-right:3px;"></div>
            </div>
        </div>
        <%
                end if
            end if
            previousItemID = itemID
            productsRS.movenext
        loop
        %>
    </div>
    <div class="clr"></div>
    <div class="centerContain tb" style="padding:10px 0px 10px 0px; margin-top:10px;">
        <a href="javascript:location.href='/search/search.html'+BR.mobile.getTrendingUrl();"><div id="btnTrending"></div></a>
    </div>
</div>    
<script language="javascript">
	var runSalesTimer = 1
	if (runSalesTimer == 1) setTimeout("changeSalesTime()",1000);
		
	var pageName = "BMC";
	var maxProducts = <%=productCnt2%>;
	var useFilter = "";
	var useFilter2 = "";
	var useFilter3 = "";
	var filterCheck = 0;
	document.getElementById("viewCnt").innerHTML = maxProducts;
</script>
<script>
window.WEDATA.pageType = 'brandModelCategory';
window.WEDATA.pageData = {
	brand: <%= jsStr(brandName) %>,
	brandId: <%= jsStr(brandId) %>,
	model: <%= jsStr(modelName) %>,
	modelId: <%= jsStr(modelId) %>,
	category: <%= jsStr(categoryName) %>,
	categoryId: <%= jsStr(categoryId) %>,
	type: <%= jsStr(strTypeToken) %>

};
</script>

<!--#include virtual="/template/bottom.asp"-->
<!-- productCnt: <%=productCnt%> -->
<!-- productCnt: <%=productCnt2%> -->