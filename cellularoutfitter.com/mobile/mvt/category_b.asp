<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "category"
	useUARedirect = true
	
	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	if categoryID = 0 then response.Redirect("/")
	
	sql = "exec sp_coCategoryDetails " & categoryID
	session("errorSQL") = sql
	set categoryDetailsRS = oConn.execute(sql)
	
	if categoryDetailsRS.EOF then
		response.Redirect("/")
	else
		categoryName = replace(categoryDetailsRS("categoryName"), "&", "and")
	end if
	categoryDetailsRS = null
	
	sql = "exec sp_brands"
	session("errorSQL") = sql
	set brandsRS = oConn.execute(sql)
%>
<!--#include virtual="/template/topCO.asp"-->
<div id="main">
    <div class="curCategory">
        <div class="viewingTitle">CURRENTLY VIEWING:</div>
        <div class="curCat"><%=catNameSwap(categoryName)%></div>
        <div class="catImg"><img src="/images/mobile/categories/<%=categoryID%>.jpg" border="0" /></div>
        <div class="changeCat" onclick="goHome()"><a href="/" title="m.CellularOutfitter.com" class="whiteLink">CHANGE CATEGORY</a></div>
    </div>
    <div class="titleBar">
        <div class="centerContain">
            <div class="fl titleTxt">CHOOSE YOUR DEVICE BRAND</div>
            <div class="fl titleArrow"></div>
        </div>
    </div>
    <div class="categoryMenu">
        <div class="centerContain">
            <%
            do while not brandsRS.EOF
                brandID = brandsRS("brandID")
                brandName = brandsRS("brandName")
                brandName = replace(brandName," Ericsson","")
                brandName = replace(brandName,"/Qualcomm","")
            %>
            <div class="fl menuItem" onclick="goHere('/sc-<%=categoryID%>-sb-<%=brandID%>-cell-phone-<%=formatSEO(categoryName)%>-for-<%=formatSEO(brandName)%>.html')">
                <div class="menuPic"><img src="/images/mobile/brands/<%=brandID%>.gif" border="0" /></div>
                <div class="menuTxt"><a href="/sc-<%=categoryID%>-sb-<%=brandID%>-cell-phone-<%=formatSEO(categoryName)%>-for-<%=formatSEO(brandName)%>.html" class="whiteLink" title=""><%=brandName%></a></div>
            </div>
            <%
                brandsRS.movenext
            loop
            %>
        </div>
    </div>
</div>
<!--#include virtual="/template/bottom.asp"-->