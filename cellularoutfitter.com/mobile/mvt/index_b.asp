<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "Home"
	useUARedirect = true
	
	dim reset : reset = prepInt(request.QueryString("reset"))
	if reset = 1 then
		response.Cookies("mobileBrandID") = 0
		response.Cookies("mobileModelID") = 0
	end if
	
	sql = "select typeID, typeName_CO from we_types where typeID not in (20,24,22,12,14,9,4,15,23,13,17,21,25) order by order_CoMobile"
	session("errorSQL") = sql
	set categoryListRS = oConn.execute(sql)
%>
<!--#include virtual="/template/topCO.asp"-->
<div id="main">
    <div class="Banner"></div>
    <div class="titleBar">
        <div class="centerContain">
            <div class="fl titleTxt">SHOP BY PRODUCT CATEGORY</div>
            <div class="fl titleArrow"></div>
        </div>
    </div>
    <div class="categoryMenu">
        <div class="centerContain">
            <%
            do while not categoryListRS.EOF
                categoryID = categoryListRS("typeID")
				categoryTitle = catNameSwap(categoryListRS("typeName_CO"))
				if categoryTitle = "Unlocked Cell Phones" then categoryTitle = "Unlocked<br>Cell Phones"
                categoryName = replace(categoryListRS("typeName_CO"), "&", "and")
                if len(categoryTitle) > 19 then useClass = "menuTxt2" else useClass = "menuTxt"
            %>
            <div class="fl menuItem" onclick="window.location='/c-<%=categoryID%>-cell-phone-<%=formatSEO(categoryName)%>.html'">
                <div class="menuPic"><img src="/images/mobile/categories/<%=categoryID%>.jpg" border="0" /></div>
                <div class="<%=useClass%>"><a href="/c-<%=categoryID%>-cell-phone-<%=formatSEO(categoryName)%>.html" class="whiteLink" title=""><%=categoryTitle%></a></div>
            </div>
            <%
                categoryListRS.movenext
            loop
            %>
        </div>
        <div class="clr"></div>
        <div class="centerContain tb" style="padding:10px 0px 10px 0px; margin-top:10px;">
        	<div style="width:90%; margin:auto; border-top:1px solid #ccc;"></div>
        	<a href="javascript:location.href='/search/search.html'+BR.mobile.getJfyUrl();"><div id="btnJFY"></div></a>
        	<a href="javascript:location.href='/search/search.html'+BR.mobile.getTrendingUrl();"><div id="btnTrending"></div></a>
        </div>
    </div>
</div>
<!--#include virtual="/template/bottom.asp"-->