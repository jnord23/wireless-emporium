<div id="mbox-global"></div>
<div id="mbox-page"></div>
<!-- <%=request.ServerVariables("LOCAL_ADDR")%> -->
<div id="popCover" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.6; filter:alpha(opacity=60);">&nbsp;</div>
<div id="popBox" style="height:2000px; display:none; width:100%; position:fixed; left:0px; top:0px; z-index:3001;margin:auto;"></div>
<div class="wholeSite">
	<% if prepInt(miniTop) = 0 then %>
	<div class="topValueProp">
    	<div class="centerContain">
    		<div class="fl propArrows"></div>
	       	<div class="fl topValuePropText">110% Low Price Guarantee + 90-Day Returns</div>
        </div>
    </div>
    <% end if %>
    <div class="header">
    	<div class="centerContain">
            <div class="fl"><a href="/" title="m.CellularOutfitter.com"><img src="/images/mobile/coLogo.gif" border="0" /></a></div>
            <div class="fl topBttn1"><a href="/" title="m.CellularOutfitter.com"><img src="/images/mobile/buttons/home.gif" border="0" /></a></div>
            <!--<div class="fl topBttn1"><a href="/search" title="Product Search"><img src="/images/mobile/buttons/search.gif" border="0" /></a></div>-->
            <div class="fl topBttn2" onclick="window.location='/basket.html'">
                <div class="cartItemCount" id="inCart"><a href="/basket.html" title="View Cart" class="cartItemQty"></a></div>
            </div>
        </div>
    </div>
    <div class="topNav">
    	<div class="centerContain">
            <!--
            <div class="fl navTxt Login" id="account_nav"></div>
            <div class="fl navTxt Track"><a href="/trackOrder.html" class="navLink" title="Track Order">TRACK ORDER</a></div>
            <div class="fl navTxt Call"><a href="tel:<%=mobilePhoneNumber%>" class="navLink" title="Call Us">CALL US</a></div>
            -->
            <div class="fl navNumber"><a href="tel:<%=mobilePhoneNumber%>" class="navPhoneNumber" title="Call Us">Order Now: <%=mobilePhoneNumber%></a></div>
            <div class="fr accountLogin" id="account_nav"></div>
        </div>
    </div>
	<!--#include virtual="/template/frmSearch.asp"-->
    <% if pageName <> "product.asp" and pageName <> "Checkout" and pageName <> "Basket" and pageName <> "Confirm" and pageName <> "Search" then %>
    <div class="promoBanner" onclick="showPromoDetails()"></div>
    <div id="promoDetails" class="tb promoDetails" onclick="showPromoDetails()" style="display:none;">
    	<div class="tb promoCenterBox">
            <div class="fl promoItem"></div>
            <div class="fl promoText">
                <div class="tb promoCheckRow">
                    <div class="fl promoCheckmark"></div>
                    <div class="fl promoDetailTxt">A pen AND stylus in one!</div>
                </div>
                <div class="tb promoCheckRow">
                    <div class="fl promoCheckmark"></div>
                    <div class="fl promoDetailTxt">Perfect for navigating on touch screens.</div>
                </div>
                <div class="tb promoCheckRow">
                    <div class="fl promoCheckmark"></div>
                    <div class="fl promoDetailTxt">Sleek and super smooth twisting barrel with black ink.</div>
                </div>
                <div class="tb promoHideBox">[X] HIDE</div>
            </div>
        </div>
    </div>
    <% end if %>
	<div style="clear:both;"></div>