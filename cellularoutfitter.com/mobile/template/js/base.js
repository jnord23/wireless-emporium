	var nextGrowStage = 0
	var scrollUpdate = 11
	var curTab = "Details"
	var maxLap = 0
	
	function backToTop() {
		window.scroll(0,0)
	}
	
	function goHome() {
		window.location = "/"
	}
	
	function goHere(url) {
		window.location = url
	}
	
	function remarketing(email) {
		ajax('/ajax/remarketing.asp?email=' + email + '&mySession=' + mySession + '&siteID=2','dumpZone')
	}
	
	function showHide(show,hide) {
		if (document.getElementById(show) != null) document.getElementById(show).style.display = 'block';
		if (document.getElementById(hide) != null) document.getElementById(hide).style.display = 'none';
	}
	
	function userScrolled() {
		if (typeof(pageName) != "undefined") {
			if (pageName == "BMC") {
//				if (document.body.scrollTop > nextGrowStage) {
				if ($(document).scrollTop() > nextGrowStage) {	
					nextGrowStage = nextGrowStage + 200
					for (i=0;i<4;i++) {
						document.getElementById("singleProdBox_" + scrollUpdate).setAttribute("class", "productRow")
						document.getElementById("singleProdBox_" + scrollUpdate).innerHTML = document.getElementById("singleProdBox_" + scrollUpdate).innerHTML.replace("tempimg","img");
						scrollUpdate++;
					}
				}
			}
		}
	}
	
	function zoomImg(imgLoc) {
		document.getElementById("imgLarge").src = imgLoc
	}
	
	function showTab(tabName) {
		document.getElementById(curTab + "Tab").setAttribute("class", "fl detailTab_off tab" + curTab)
		document.getElementById(tabName + "Tab").setAttribute("class", "fl detailTab_on tab" + tabName)
		document.getElementById(curTab + "View").setAttribute("class", "baseView hideView")
		document.getElementById(tabName + "View").setAttribute("class", "baseView showView")
		curTab = tabName
		if (tabName == "Details") { document.getElementById("activeTabArrow").style.left = '50px'; }
		if (tabName == "Compatibility") { document.getElementById("activeTabArrow").style.left = '155px'; }
		if (tabName == "Reviews") { document.getElementById("activeTabArrow").style.left = '265px'; }
	}
	
	function checkForm() {
		if (document.cuForm.name.value == "") {
			alert("You must enter your name");
			document.cuForm.name.focus();
			return false;
		}
		else if (document.cuForm.email.value == "") {
			alert("You must enter your email");
			document.cuForm.email.focus();
			return false;
		}
		else if (document.cuForm.subject.selectedIndex == 0) {
			alert("You must select an inquiry type");
			document.cuForm.subject.focus();
			return false;
		}
		else if (document.cuForm.question.value == "") {
			alert("You must enter a question");
			document.cuForm.question.focus();
			return false;
		}
		else {
			return true;
		}
	}
	
	function toFormSubmit() {
		if (document.trackingForm.orderid.value == "") {
			alert("You must enter your orderID");
			document.trackingForm.orderid.focus();
			return false;
		}
		if (document.trackingForm.lastname.value == "" && document.trackingForm.orderemail.value == "") {
			alert("You must enter your last name or email address");
			document.trackingForm.orderid.focus();
			return false;
		}
		else {
			return true;
		}
	}
	
	function shipAddrOptions(checked) {
		if (checked == true) {
			document.getElementById("shippingFields").className = ''
		}
		else {
			document.getElementById("shippingFields").className = 'shippingAddress'
		}
	}
	
	function revealShippingOptions() {
		if (document.getElementById("shippingOptionDiv").className == 'shippingOptions hidden') {
			document.getElementById("shippingOptionDiv").className = 'shippingOptions'
		}
		else {
			document.getElementById("shippingOptionDiv").className = 'shippingOptions hidden'
		}
	}
	
	function showShippingOnTotal() {
		if (document.checkout1Form.shiptype[0].checked) {
			shipCost = document.checkout1Form.shipcost0.value
		}
		else if (document.checkout1Form.shiptype[1].checked) {
			shipCost = document.checkout1Form.shipcost2.value
		}
		else if (document.checkout1Form.shiptype[2].checked) {
			shipCost = document.checkout1Form.shipcost3.value
		}
		var newTotal = parseFloat(document.checkout1Form.discountedSubTotal.value) + parseFloat(shipCost) + parseFloat(taxAmt)
		newTotal = newTotal.toFixed(2)
		shipCost = parseFloat(shipCost).toFixed(2)
		document.getElementById("shippingCostDiv").innerHTML = '$' + shipCost
		document.getElementById("grandTotalDiv").innerHTML = '$' + newTotal
		document.checkout1Form.grandTotal.value = newTotal
		document.checkout1Form.shippingTotal.value = shipCost
		document.getElementById("shipSummary").className = 'summaryRow'
		document.getElementById("estSummary").className = 'summaryRowDark hidden'
		document.getElementById("grandSummary").className = 'summaryRowDark'
		document.checkout1Form.showShipping.value = 1
	}
	
	function checkTax(stateCode) {
		if (stateCode == "CA") {
			if (document.getElementById("shipSummary").className == 'summaryRow') {
				if (document.checkout1Form.shiptype[0].checked) {
					shipCost = document.checkout1Form.shipcost0.value
				}
				else if (document.checkout1Form.shiptype[1].checked) {
					shipCost = document.checkout1Form.shipcost2.value
				}
				else if (document.checkout1Form.shiptype[2].checked) {
					shipCost = document.checkout1Form.shipcost3.value
				}
			}
			else {
				shipCost = 0
			}
			taxAmt = parseFloat(document.checkout1Form.itemTotal.value) * taxValue
			var newTotal = parseFloat(document.checkout1Form.discountedSubTotal.value) + parseFloat(shipCost) + parseFloat(taxAmt)
			newTotal = newTotal.toFixed(2)
			shipCost = parseFloat(shipCost).toFixed(2)
			taxAmt = taxAmt.toFixed(2)
			document.checkout1Form.taxAmt.value = taxAmt
			document.getElementById("taxDiv").innerHTML = "$" + taxAmt
			document.checkout1Form.grandTotal = newTotal
			document.getElementById("estTotalDiv").innerHTML = "$" + newTotal
			document.getElementById("grandTotalDiv").innerHTML = "$" + newTotal
		}
		else {
			if (document.getElementById("shipSummary").className == 'summaryRow') {
				if (document.checkout1Form.shiptype[0].checked) {
					shipCost = document.checkout1Form.shipcost0.value
				}
				else if (document.checkout1Form.shiptype[1].checked) {
					shipCost = document.checkout1Form.shipcost2.value
				}
				else if (document.checkout1Form.shiptype[2].checked) {
					shipCost = document.checkout1Form.shipcost3.value
				}
			}
			else {
				shipCost = 0
			}
			taxAmt = parseFloat(0)
			var newTotal = parseFloat(document.checkout1Form.itemTotal.value) + parseFloat(shipCost)
			newTotal = newTotal.toFixed(2)
			shipCost = parseFloat(shipCost).toFixed(2)
			taxAmt = taxAmt.toFixed(2)
			document.checkout1Form.taxAmt.value = taxAmt
			document.getElementById("taxDiv").innerHTML = "$" + taxAmt
			document.checkout1Form.grandTotal = newTotal
			document.getElementById("estTotalDiv").innerHTML = "$" + newTotal
			document.getElementById("grandTotalDiv").innerHTML = "$" + newTotal
		}
	}
	
	function checkout1FormReview() {
		var fname = document.checkout1Form.fname.value
		var lname = document.checkout1Form.lname.value
		var bAddress1 = document.checkout1Form.bAddress1.value
		var bAddress2 = document.checkout1Form.bAddress2.value
		var bCity = document.checkout1Form.bCity.value
		var bState = document.checkout1Form.bState.value
		var bZip = document.checkout1Form.bZip.value
		var phone = document.checkout1Form.phone.value
		var email = document.checkout1Form.email.value
		
		var shippingAddr = document.checkout1Form.shippingAddr.checked
		var sAddress1 = document.checkout1Form.sAddress1.value
		var sAddress2 = document.checkout1Form.sAddress2.value
		var sCity = document.checkout1Form.sCity.value
		var sState = document.checkout1Form.sState.value
		var sZip = document.checkout1Form.sZip.value
		
		if (shippingAddr == true) {
			if (sAddress1 == "") {
			   alert("You must enter your shipping address");
			   document.checkout1Form.sAddress1.focus();
			   return false;
			}
			else if (sCity == "") {
			   alert("You must enter your shipping city");
			   document.checkout1Form.sCity.focus();
			   return false;
			}
			else if (sState == "") {
			   alert("You must enter your shipping state");
			   document.checkout1Form.sState.focus();
			   return false;
			}
			else if (sZip == "") {
			   alert("You must enter your shipping zip/postal code");
			   document.checkout1Form.sZip.focus();
			   return false;
			}
		}
		else {
			document.checkout1Form.sAddress1.value = document.checkout1Form.bAddress1.value
			document.checkout1Form.sAddress2.value = document.checkout1Form.bAddress2.value
			document.checkout1Form.sCity.value = document.checkout1Form.bCity.value
			document.checkout1Form.sZip.value = document.checkout1Form.bZip.value
			var stateOptions = document.checkout1Form.sState.length
			for (i=0;i<stateOptions;i++) {
				if (document.checkout1Form.sState.options[i].value == bState) {
					document.checkout1Form.sState.selectedIndex = i
				}
			}
		}
		
		if (fname == "") {
		   alert("You must enter your first name");
		   document.checkout1Form.fname.focus();
		   return false;
		}
		else if (lname == "") {
		   alert("You must enter your last name");
		   document.checkout1Form.lname.focus();
		   return false;
		}
		else if (bAddress1 == "") {
		   alert("You must enter your billing address");
		   document.checkout1Form.bAddress1.focus();
		   return false;
		}
		else if (bCity == "") {
		   alert("You must enter your billing city");
		   document.checkout1Form.bCity.focus();
		   return false;
		}
		else if (bState == "") {
		   alert("You must enter your billing state");
		   document.checkout1Form.bState.focus();
		   return false;
		}
		else if (bZip == "") {
		   alert("You must enter your billing zip/postal code");
		   document.checkout1Form.bZip.focus();
		   return false;
		}
		else if (phone == "") {
		   alert("You must enter your phone number");
		   document.checkout1Form.phone.focus();
		   return false;
		}
		else if (email == "") {
		   alert("You must enter your email");
		   document.checkout1Form.email.focus();
		   return false;
		}
		else if (shippingAddr) {
		   if (sAddress1 == "") {
			   alert("You must enter your shipping address");
			   document.checkout1Form.sAddress1.focus();
			   return false;
		   }
		   else if (sCity == "") {
			   alert("You must enter your shipping city");
			   document.checkout1Form.sCity.focus();
			   return false;
		   }
		   else if (sState == "") {
			   alert("You must enter your shipping state");
			   document.checkout1Form.sState.focus();
			   return false;
		   }
		   else if (sZip == "") {
			   alert("You must enter your shipping zip/postal code");
			   document.checkout1Form.sZip.focus();
			   return false;
		   }
		}
		
		if (document.checkout1Form.cc_cardOwner != undefined) {
		   var cc_cardOwner = document.checkout1Form.cc_cardOwner.value
		   var cardNum = document.checkout1Form.cardNum.value
		   var secCode = document.checkout1Form.secCode.value
		   var cc_month = document.checkout1Form.cc_month.value
		   var cc_year = document.checkout1Form.cc_year.value
		   
		   if (cc_cardOwner == "") {
			   alert("You must enter the cardholder's name");
			   document.checkout1Form.cc_cardOwner.focus();
			   return false;
		   }
		   else if (cardNum == "") {
			   alert("You must enter your credit card number");
			   document.checkout1Form.cardNum.focus();
			   return false;
		   }
		   else if (secCode == "") {
			   alert("You must enter your security code");
			   document.checkout1Form.secCode.focus();
			   return false;
		   }
		   else if (cc_month == "") {
			   alert("You must enter your expiration month");
			   document.checkout1Form.cc_month.focus();
			   return false;
		   }
		   else if (cc_year == "") {
			   alert("You must enter your expiration year");
			   document.checkout1Form.cc_year.focus();
			   return false;
		   }
		}
		return true;
	}
	
	function checkout2FormReview() {
		 var cc_cardOwner = document.checkout2Form.cc_cardOwner.value
		 var cardNum = document.checkout2Form.cardNum.value
		 var secCode = document.checkout2Form.secCode.value
		 var cc_month = document.checkout2Form.cc_month.value
		 var cc_year = document.checkout2Form.cc_year.value
		 
		 if (cc_cardOwner == "") {
			 alert("You must enter the cardholder's name");
			 document.checkout2Form.cc_cardOwner.focus();
			 return false;
		 }
		 else if (cardNum == "") {
			 alert("You must enter your credit card number");
			 document.checkout2Form.cardNum.focus();
			 return false;
		 }
		 else if (secCode == "") {
			 alert("You must enter your security code");
			 document.checkout2Form.secCode.focus();
			 return false;
		 }
		 else if (cc_month == "") {
			 alert("You must enter your expiration month");
			 document.checkout2Form.cc_month.focus();
			 return false;
		 }
		 else if (cc_year == "") {
			 alert("You must enter your expiration year");
			 document.checkout2Form.cc_year.focus();
			 return false;
		 }
		 else {
			 return true;
		 }
	}
	
	function getCityState(zipCode,which) {
		document.getElementById("dumpZone").innerHTML = ""
		ajax('/ajax/getCityState.asp?zipcode=' + zipCode,'dumpZone')
		setTimeout("checkCityStateResult('" + which + "')",500)
	}
	
	function checkCityStateResult(which) {
		var result = document.getElementById("dumpZone").innerHTML
		if (result == "") {
			if (maxLap < 5) { setTimeout("checkCityStateResult()",500) }
			maxLap++
		}
		else {
			if (result != "none") {
				resultArray = result.split("##")
				cityName = resultArray[0]
				stateCode = resultArray[1]
				if (which == "bill") {
					document.checkout1Form.bCity.value = cityName;
					var stateOptions = document.checkout1Form.bState.length
					for (i=0;i<stateOptions;i++) {
						if (document.checkout1Form.bState.options[i].value == stateCode) {
							document.checkout1Form.bState.selectedIndex = i
							checkTax(stateCode)
						}
					}
				}
				else {
					document.checkout1Form.sCity.value = cityName;
					var stateOptions = document.checkout1Form.sState.length
					for (i=0;i<stateOptions;i++) {
						if (document.checkout1Form.sState.options[i].value == stateCode) {
							document.checkout1Form.sState.selectedIndex = i
						}
					}
				}
			}
		}
	}
	
	function jumpToReviews() {
		window.location='#ReviewsTab'
		showTab('Reviews')
	}
	
	function removeItem(itemID) {
		eval("document.itemForm_" + itemID + ".qty.selectedIndex = 0")
		eval("document.itemForm_" + itemID + ".submit()")
	}
	
	ajax('/ajax/accountMenu.asp','account_nav');
	function showLoginDialog() {
		var randomnumber=Math.floor(Math.random()*100001);
		ajax("/ajax/accountLoginDialog.asp?rn="+randomnumber,'popBox');
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function showBmcFilter(categoryID) {
		window.scroll(0,50)
		var randomnumber=Math.floor(Math.random()*100001);
		ajax("/ajax/bmcFilter.asp?categoryID=" + categoryID + "&rn="+randomnumber,'popBox');
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
		filterCheck = 0;
		setTimeout("checkFilterBox()",100)
	}
	
	function checkFilterBox() {
		tempUseFilter = useFilter;
		tempUseFilter2 = useFilter2;
		tempUseFilter3 = useFilter3;
		
		if (document.getElementById("popBox").innerHTML == "") {
			filterCheck++
			if (filterCheck < 5) {
				setTimeout("checkFilterBox()",200)
			}
		}
		else {
			//set filter options
			if (tempUseFilter != "") {
				useFilterArray = tempUseFilter.split("##")
				for (setFilter=0;setFilter<=useFilterArray.length;setFilter++) {
					if (useFilterArray[setFilter] != "" && useFilterArray[setFilter] != undefined) {
						selectFilter(useFilterArray[setFilter])
					}
				}
			}
			if (tempUseFilter2 != "") {
				useFilter2Array = tempUseFilter2.split("##")
				for (setFilter=0;setFilter<=useFilter2Array.length;setFilter++) {
					if (useFilter2Array[setFilter] != "" && useFilter2Array[setFilter] != undefined) {
						selectFilter("price_" + useFilter2Array[setFilter])
					}
				}
			}
			if (tempUseFilter3 != "") {
				useFilter3Array = tempUseFilter3.split("##")
				for (setFilter=0;setFilter<=useFilter3Array.length;setFilter++) {
					if (useFilter3Array[setFilter] != "" && useFilter3Array[setFilter] != undefined) {
						selectFilter("color_" + useFilter3Array[setFilter])
					}
				}
			}
		}
	}
	
	function closeFloatingBox() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
		document.getElementById("popCover").innerHTML = "";
		document.getElementById("popBox").innerHTML = "";
	}
	
	function resetFilter() {
		for (x=1;x<=maxProducts;x++) {
			document.getElementById("singleProdBox_" + x).style.display = '';
		}
		useFilter = "";
		useFilter2 = "";
		useFilter3 = "";
		closeFloatingBox()
		document.getElementById("viewCnt").innerHTML = maxProducts
	}
	
	function selectFilter(subID) {
		if (document.getElementById(subID).checked == true) {
			document.getElementById(subID).checked = false;
		}
		else {
			document.getElementById(subID).checked = true;
		}
		updateFilter()
	}
	
	function updateFilter() {
		useFilter = "##";
		useFilter2 = "##";
		useFilter3 = "##";
		for(i=0;i<document.filterForm.elements.length;i++) {
			if (document.filterForm.elements[i].checked == true) {
				if (document.filterForm.elements[i].name == "styleType") {
					useFilter = useFilter + document.filterForm.elements[i].value + '##'
				}
				else if (document.filterForm.elements[i].name == "colorSelect") {
					useFilter3 = useFilter3 + document.filterForm.elements[i].value + '##'
				}
				else {
					useFilter2 = useFilter2 + document.filterForm.elements[i].value + '##'
				}
			}
		}
		if (useFilter == "##") { useFilter = "" }
		if (useFilter2 == "##") { useFilter2 = "" }
		if (useFilter3 == "##") { useFilter3 = "" }
		currentlyShowing = 0
		
		for (x=1;x<=maxProducts;x++) {
			document.getElementById("singleProdBox_" + x).style.display = '';
		}
		
		if (useFilter == "" &&  useFilter2 == "" && useFilter3 == "") {
			document.getElementById("viewCnt").innerHTML = maxProducts
			return;
		}
		else {
			if (useFilter != "") {
				for (x=1;x<=maxProducts;x++) {
					thisSubtypeID = document.getElementById("filter_subtypeid_" + x).innerHTML;
					thisPrice = parseFloat(document.getElementById("filter_price_" + x).innerHTML);
					thisItemDesc = document.getElementById("filter_itemDesc_" + x).innerHTML.toLowerCase();
					invalidPrice = 1;
					if (useFilter.indexOf("#" + thisSubtypeID + "#") < 0) {
						document.getElementById("singleProdBox_" + x).style.display = 'none';
					}
					else {
						if (useFilter2 != "") {
							if (useFilter2.indexOf("#4#") > 0) {
								if (thisPrice >= 0 && thisPrice < 5) {
									invalidPrice = 0;
								}
							}
							if (useFilter2.indexOf("#5#") > 0) {
								if (thisPrice >= 5 && thisPrice < 10) {
									invalidPrice = 0;
								}
							}
							if (useFilter2.indexOf("#10#") > 0) {
								if (thisPrice >= 10 && thisPrice < 20) {
									invalidPrice = 0;
								}
							}
							if (useFilter2.indexOf("#20#") > 0) {
								if (thisPrice >= 20 && thisPrice < 30) {
									invalidPrice = 0;
								}
							}
							if (useFilter2.indexOf("#30#") > 0) {
								if (thisPrice >= 30) {
									invalidPrice = 0;
								}
							}
							if (invalidPrice == 1) {
								document.getElementById("singleProdBox_" + x).style.display = 'none';
							}
							else {
								if (useFilter3 != "") {
									useFilter3Array = useFilter3.split("##")
									for (z=0;z<useFilter3Array.length;z++) {
										if (useFilter3Array[z] != "") {
											if (thisItemDesc.indexOf(useFilter3Array[z].toLowerCase()) >= 0) {
												currentlyShowing++
												if (currentlyShowing < 11) {
													document.getElementById("singleProdBox_" + x).className = "productRow"
													document.getElementById("singleProdBox_" + x).innerHTML = document.getElementById("singleProdBox_" + x).innerHTML.replace("tempimg","img")
												}
											}
											else {
												document.getElementById("singleProdBox_" + x).style.display = 'none';
											}
										}
									}
								}
								else {
									currentlyShowing++
									if (currentlyShowing < 11) {
										document.getElementById("singleProdBox_" + x).className = "productRow"
										document.getElementById("singleProdBox_" + x).innerHTML = document.getElementById("singleProdBox_" + x).innerHTML.replace("tempimg","img")
									}
								}
							}
						}
						else if (useFilter3 != "") {
							useFilter3Array = useFilter3.split("##")
							for (z=0;z<useFilter3Array.length;z++) {
								if (useFilter3Array[z] != "") {
									if (thisItemDesc.indexOf(useFilter3Array[z].toLowerCase()) >= 0) {
										currentlyShowing++
										if (currentlyShowing < 11) {
											document.getElementById("singleProdBox_" + x).className = "productRow"
											document.getElementById("singleProdBox_" + x).innerHTML = document.getElementById("singleProdBox_" + x).innerHTML.replace("tempimg","img")
										}
									}
									else {
										document.getElementById("singleProdBox_" + x).style.display = 'none';
									}
								}
							}
						}
						else {
							currentlyShowing++
							if (currentlyShowing < 11) {
								document.getElementById("singleProdBox_" + x).className = "productRow"
								document.getElementById("singleProdBox_" + x).innerHTML = document.getElementById("singleProdBox_" + x).innerHTML.replace("tempimg","img")
							}
						}
					}
				}
			}
			else if (useFilter2 != "") {
				for (x=1;x<=maxProducts;x++) {
					thisSubtypeID = document.getElementById("filter_subtypeid_" + x).innerHTML;
					thisPrice = parseFloat(document.getElementById("filter_price_" + x).innerHTML);
					thisItemDesc = document.getElementById("filter_itemDesc_" + x).innerHTML.toLowerCase();
					invalidPrice = 1;
					
					if (useFilter2.indexOf("#4#") > 0) {
						if (thisPrice >= 0 && thisPrice < 5) {
							invalidPrice = 0;
						}
					}
					if (useFilter2.indexOf("#5#") > 0) {
						if (thisPrice >= 5 && thisPrice < 10) {
							invalidPrice = 0;
						}
					}
					if (useFilter2.indexOf("#10#") > 0) {
						if (thisPrice >= 10 && thisPrice < 20) {
							invalidPrice = 0;
						}
					}
					if (useFilter2.indexOf("#20#") > 0) {
						if (thisPrice >= 20 && thisPrice < 30) {
							invalidPrice = 0;
						}
					}
					if (useFilter2.indexOf("#30#") > 0) {
						if (thisPrice >= 30) {
							invalidPrice = 0;
						}
					}
					if (invalidPrice == 1) {
						document.getElementById("singleProdBox_" + x).style.display = 'none';
					}
					else {
						if (useFilter3 != "") {
							useFilter3Array = useFilter3.split("##")
							for (z=0;z<useFilter3Array.length;z++) {
								if (useFilter3Array[z] != "") {
									if (thisItemDesc.indexOf(useFilter3Array[z].toLowerCase()) >= 0) {
										currentlyShowing++
										if (currentlyShowing < 11) {
											document.getElementById("singleProdBox_" + x).className = "productRow"
											document.getElementById("singleProdBox_" + x).innerHTML = document.getElementById("singleProdBox_" + x).innerHTML.replace("tempimg","img")
										}
									}
									else {
										document.getElementById("singleProdBox_" + x).style.display = 'none';
									}
								}
							}
						}
						else {
							currentlyShowing++
							if (currentlyShowing < 11) {
								document.getElementById("singleProdBox_" + x).className = "productRow"
								document.getElementById("singleProdBox_" + x).innerHTML = document.getElementById("singleProdBox_" + x).innerHTML.replace("tempimg","img")
							}
						}
					}
				}
			}
			else if (useFilter3 != "") {
				for (x=1;x<=maxProducts;x++) {
					thisSubtypeID = document.getElementById("filter_subtypeid_" + x).innerHTML
					thisPrice = parseFloat(document.getElementById("filter_price_" + x).innerHTML)
					thisItemDesc = document.getElementById("filter_itemDesc_" + x).innerHTML.toLowerCase()
					useFilter3Array = useFilter3.split("##")
					for (z=0;z<useFilter3Array.length;z++) {
						if (useFilter3Array[z] != "") {
							if (thisItemDesc.indexOf(useFilter3Array[z].toLowerCase()) >= 0) {
								currentlyShowing++
								if (currentlyShowing < 11) {
									document.getElementById("singleProdBox_" + x).className = "productRow"
									document.getElementById("singleProdBox_" + x).innerHTML = document.getElementById("singleProdBox_" + x).innerHTML.replace("tempimg","img")
								}
							}
							else {
								document.getElementById("singleProdBox_" + x).style.display = 'none';
							}
						}
					}
				}
			}
		}
		document.getElementById("viewCnt").innerHTML = currentlyShowing
	}
	
	function narrowResultSubClick(filterOption) {
		if (document.getElementById(filterOption + "Options").style.display == '') {
			document.getElementById(filterOption + "Arrow").className = "fr narrowResultsExpand"
			document.getElementById(filterOption + "Options").style.display = 'none'
		}
		else {
			document.getElementById(filterOption + "Arrow").className = "fr narrowResultsContract"
			document.getElementById(filterOption + "Options").style.display = ''
		}
	}
	
	function showPromoDetails() {
		var promo = document.getElementById("promoDetails")
		if (promo.style.display == '') {
			promo.style.display = 'none'
		}
		else {
			promo.style.display = ''
		}
	}
	
	// bloomreach add/remove cart tracking
	function br_cartTracking(tType, itemid, color, prodName, price) {
		try
		{
			if (tType == 'add') {
				BrTrk.getTracker().logEvent('Cart', tType, {'prod_id': itemid, 'sku': itemid, 'prod_color': color, 'prod_name': prodName}, {'price' : price});
			} else {
				BrTrk.getTracker().logEvent('Cart', tType, {'prod_id': itemid, 'sku': itemid, 'prod_color': color, 'prod_name': prodName});
			}
			
		}
		catch(e) {}    
	}
	

	/*email acquisition widget*/
	function closeEmailWidget(widgetid) {
		showHide(null,widgetid);
		showHide(null,'fade');
	}
	
	function showEmailWidget() {
		showHide('overlay1', null);
		showHide('fade', null);
	}	

	function closeEmailIcon() {
		showHide(null, 'icon25');
		ajax('/ajax/newsletter.asp?semail=closewidget','dumpZone');
	}
	
	function closeEmailIcon2() {
		showHide(null, 'floating-banner');
		ajax('/ajax/newsletter.asp?semail=closewidget','dumpZone');
	}	
	
	function changeSalesTime() {
		var saleHours = parseInt(document.getElementById("hour").innerHTML);
		var saleMinutes = parseInt(document.getElementById("hidden-min").innerHTML);
		var saleSeconds = parseInt(document.getElementById("hidden-sec").innerHTML);
		
		if (saleSeconds > 0) {
			saleSeconds--;
			if (saleSeconds < 10) {
				document.getElementById("hidden-sec").innerHTML = saleSeconds;
			}
			else {
				document.getElementById("hidden-sec").innerHTML = saleSeconds;
			}
			setTimeout("changeSalesTime()",1000);
		}
		else if (saleMinutes > 0) {
			saleMinutes--;
			if (saleMinutes < 10) {
				document.getElementById("hidden-min").innerHTML = saleMinutes;
			}
			else {
				document.getElementById("hidden-min").innerHTML = saleMinutes;
			}
			document.getElementById("hidden-sec").innerHTML = 59;
			setTimeout("changeSalesTime()",1000);
		}
		else if (saleHours > 0) {
			saleHours--;
			document.getElementById("hour").innerHTML = saleHours;
			document.getElementById("hidden-min").innerHTML = 59;
			document.getElementById("hidden-sec").innerHTML = 59;
			setTimeout("changeSalesTime()",1000);
		}
	}	
	
