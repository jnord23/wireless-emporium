<%
	dim sPromoCode
	if request.form("txtPromo") <> "" then
		sPromoCode = request.form("txtPromo")
	else
		sPromoCode = session("promocode")
	end if
	
	if sPromoCode <> "" then
		'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
		dim couponid : couponid = 0
		sql = "SELECT * FROM CO_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
		set rsPromo = oConn.execute(sql)
		if not rsPromo.eof then
			dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
			couponid = rsPromo("couponid")
			promoMin = rsPromo("promoMin")
			promoPercent = rsPromo("promoPercent")
			typeID = rsPromo("typeID")
			excludeBluetooth = rsPromo("excludeBluetooth")
			BOGO = rsPromo("BOGO")
			couponDesc = rsPromo("couponDesc")
			'FreeItemPartNumber = rsPromo("FreeItemPartNumber")
		else
			sPromoCode = ""
		end if
	end if
%>