<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "Track Order"
	
	dim orderID : orderID = prepInt(request.Form("orderID"))
	dim lastname : lastname = prepStr(request.Form("lastname"))
	dim orderemail : orderemail = prepStr(request.Form("orderemail"))
	dim shipType : shipType = ""
	dim noResults : noResults = 0
	dim orderDetailsShown : orderDetailsShown = 0
	dim saddress1, saddress2, sCity, sstate, szip, confirmSent, shipDate, fname, lname, trackingNum, shipBy
	
	if orderID > 0 then
		sql = "exec sp_orderTracking " & orderID & ",'" & orderemail & "','" & lastname & "',2"
		session("errorSQL") = sql
		set orderTrackingRS = oConn.execute(sql)
		
		if not orderTrackingRS.EOF then
			saddress1 = orderTrackingRS("saddress1")
			saddress2 = orderTrackingRS("saddress2")
			sCity = orderTrackingRS("sCity")
			sstate = orderTrackingRS("sstate")
			szip = orderTrackingRS("szip")
			
			if inStr(1,Ucase(orderTrackingRS("shiptype")),"FIRST") > 0 then
				shipType = "F"
			elseif inStr(1,ucase(orderTrackingRS("shiptype")),"PRIORITY") > 0 then
				shipType = "P"
			elseif inStr(1,ucase(orderTrackingRS("shiptype")),"EXPRESS") > 0 then
				shipType = "E"
			elseif inStr(1,ucase(orderTrackingRS("shiptype")),"UPS") > 0 then
				shipType = "U"
			end if
			
			confirmSent = orderTrackingRS("ConfirmSent")
			shipDate = orderTrackingRS("Date")
			fname = orderTrackingRS("fname")
			lname = orderTrackingRS("lname")
			trackingNum = orderTrackingRS("trackingNum")
		else
			noResults = 1
		end if
	end if
%>
<!--#include virtual="/template/topCO.asp"-->
<%
if shipType <> "" then
	orderDetailsShown = 1
	if confirmSent = "yes" then
		if shipType = "F" then
			shipBy = "USPS First Class Mail"
			shipTime = "4-10"
		elseif shipType = "P" then
			shipBy = "USPS Priority Mail"
			shipTime = "2-4"
		elseif shipType = "E" then
			shipBy = "USPS Express Mail"
			shipTime = "1-2"
		elseif shipType = "U" then
			shipBy = "UPS"
			shipTime = ""
		end if
%>
<div class="orderStatusTitle">Order Status Result</div>
<div class="pullResult">
	Your order #<%=orderID%> was processed and shipped on <%=shipDate%> by <%=shipBy%>.
    Your order was shipped to:
    <br><br>
    <%=fname & " " & lname & "<br>" & saddress1 & " " & saddress2 & "<br>" & sCity & ", " & sstate & " " & szip%>
    <br><br>
    <% if shipType = "F" then %>
    <a href="http://webtrack.dhlglobalmail.com/?mobile=&trackingnumber=<%=trackingNum%>" target="_blank">Click here</a> to check track your shipment.
    <% elseif shipType = "P" or shipType = "E" then %>
    <p><strong>Your USPS Tracking Number: <a href="https://tools.usps.com/go/TrackConfirmAction!execute.action?formattedLabel=<%=TrackingNum%>" target="_blank"><%=TrackingNum%></a></strong></p>
    <% elseif shipType = "U" then %>
    <p><strong>Your UPS Tracking Number: <a href="http://wwwapps.ups.com/WebTracking/OnlineTool?InquiryNumber1=<%=TrackingNum%>" target="_blank"><%=TrackingNum%></a></strong></p>
    <% end if %>
    <br><br>
    <% if shipTime <> "" then %>
    Please allow <%=shipTime%> business days for delivery.
    If you do not receive your order in the <%=shipTime%> business days please contact us at:
    <% else %>
    For inquiries, please contact customer service at:
    <% end if %>
    <a href="mailto:service@cellularoutfitter.com">service@cellularoutfitter.com</a>.
</div>
<%
	else
%>
<div class="orderStatusTitle">Order Status Result</div>
<div class="pullResult">
	Your order #<%=orderID%> has been received and is currently being processed.
    <br /><br />
	All orders are processed within 1-2 business days.
    <br /><br />
	You will receive an order shipment confirmation after the order has been shipped from our warehouse.
</div>
<%
	end if
elseif noResults = 1 then
%>
<div class="orderStatusTitle">Order Status Result</div>
<div class="pullResult2">
	No order information found with the entered information
    <br /><br />
    Please double check all entered fields
</div>
<%
end if

if orderDetailsShown = 0 then
	if orderID = 0 then orderID = ""
%>
<div class="orderStatusText">
    <h1 style="font-size:16px;">Check your order status online! </h1>
    
    <h3>Please be advised: </h3>
    <p>
    <ul>
        <li>We currently offer tracking ONLY on UPS and USPS Express & Priority deliveries. </li>
        <li>All shipment and arrival times are approximate. Cellular Outfitter does not expressly warrant or guarantee arrival dates. </li>
        <li>All shipments are delivered via the United States Postal Service. </li>
	</ul>
</div>
<form action="/trackOrder.html" method="post" name="trackingForm">
<div class="trackingFormBox">
	<div class="formInstructions">Order ID is required,<br />plus at least one of the other 2 values.</div>
    <div class="toFormRow">
        <div class="fl toFormTitle"><span class="requiredField">*</span>OrderID#:</div>
        <div class="fl toFormValue"><input type="text" name="orderid" value="<%=orderID%>" /></div>
    </div>
    <div class="toFormRow">
        <div class="fl toFormTitle">Last Name:</div>
        <div class="fl toFormValue"><input type="text" name="lastname" id="lastname" value="<%=lastName%>" /></div>
    </div>
    <div class="toFormRow">
        <div class="fl toFormTitle">Order Email:</div>
        <div class="fl toFormValue"><input type="text" name="orderemail" value="<%=orderemail%>" /></div>
    </div>
    <div class="submitBttn"><input type="image" src="/images/buttons/button-submit.png" onclick="return(toFormSubmit());" /></div>
</div>
</form>
<% end if %>
<!--#include virtual="/template/bottom.asp"-->