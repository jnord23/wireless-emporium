<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "Login"
	
	if isLoggedIn then
		response.redirect urlRelMyAccount
	end if
	
	if prepStr(request.form("submitButton")) = "Login" then
		inputEmail = prepStr(request.form("inputEmail"))
		inputPassword = prepStr(request.form("inputPassword"))
		
		sql = "exec sp_coLogin '" & inputEmail & "','" & inputPassword & "'"
		session("errorSQL") = sql
		set loginRS = oConn.execute(sql)
		
		if loginRS.EOF then
			isLoggedIn = false
			loginError = true
		else
			session("acctID") = prepInt(loginRS("accountid"))
			session("accountEmail") = inputEmail
			response.Redirect("/")
		end if
		
	end if
%>
<!--#include virtual="/template/topCO.asp"-->
<style type="text/css">
.errorMessage {
	color:red;
	display:none;
	font-size:smaller;
}
.hide {
	display:none;
}
#divLoginFacebook {
	background:url(/images/account/button-facebook.jpg) no-repeat;
	width:175px;
	height:34px;
	cursor:pointer;
}
#divLoginGoogle {
	background:url(/images/account/button-google.jpg) no-repeat;
	width:175px;
	height:35px;
	cursor:pointer;
}

</style>
<script type="text/javascript">
function chkField(inputField, errorMessage) {
	//alert("test")
	//alert(document.LoginForm_B)
	//var f = eval("document.LoginForm_B."  + inputField + ".value");//Works
	var f = document.getElementsByName(inputField).item(0).value;//Works
	//alert(f)
	var e = document.getElementById(errorMessage);
	if(f == '') {
		e.style.display = 'inline';
		//alert('value is blank');
	}else{
		e.style.display = 'none';
		//alert('value NOT blank');
	};
	if(inputField == 'inputEmail'){
		var eVal = document.getElementById('valEmail');
		if(f.value != ''){
			eVal.style.display = 'none';
		}
	}
};
function chkForm() {
	chkField('inputEmail','rqdEmail');
	chkField('inputPassword','rqdPassword');
	
	var e3 = document.getElementById('rqdEmail').style.display;
	var e4 = document.getElementById('valEmail').style.display;
	var e5 = document.getElementById('rqdPassword').style.display;


	if (e3 == 'inline' || e4 == 'inline' || e5 == 'inline') {
		//alert('Please complete the entire form and try again.');
		return false;
	}else{
		return true;
	}
}
</script>
<div id="divLoginWrapper">
	<div class="centerContain">
    
        <h1>Login</h1>
        
        <p>To login to your account, please complete the form below:</p>    
    
        <% if loginError then %>
        <p class="loginErrorMsg">Your email and password could not be verified.  Please try again.</p>
        <% end if %>
    
        <div id="theForm">
        <form name="LoginForm_B" action="" method="post" onsubmit="return chkForm()">
                    
        <table width="300" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100" align="right"><label for="inputEmail">Email Address</label></td>
            <td><input type="text" id="inputEmail" name="inputEmail" value="<%=inputEmail%>" onblur="chkField('inputEmail','rqdEmail')" /><span class="errorMessage" id="rqdEmail"> * Required</span><span class="errorMessage" id="valEmail"> * Invalid</span></td>
          </tr>
          <tr>
            <td align="right"><label for="inputPassword">Password</label></td>
            <td><input type="password" id="inputPassword" name="inputPassword" value="<%=inputPassword%>" onblur="chkField('inputPassword','rqdPassword')" /><span class="errorMessage" id="rqdPassword"> * Required</span></td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td><input type="submit" id="submitButton" name="submitButton" value="Login" /></td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td>
                <div id="divLoginFacebook" onclick="window.location.href='<%=urlRelFacebook%>'"></div>
            </td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td>
                <div id="divLoginGoogle" onclick="window.location.href='<%=urlRelLoginGoogle%>'"></div>
            </td>
          </tr>
        </table>
        </form>
          
        <p>Forgot your password?  Click <a href="/account/password_forgot.asp">here</a>.</p>
        
        <p>If you do not have an account, please <a href="/account/register.asp">register</a>.</p>    
    
        </div>
    </div>
</div>
<!--#include virtual="/template/bottom.asp"-->