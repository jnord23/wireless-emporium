<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	dim promoItemID : promoItemID = 259959
	pageName = "product.asp"
	
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	dim itemID : itemID = prepInt(request.QueryString("itemID"))
	if itemID = 0 then response.Redirect("/")
	
	useTime = now
	totalSeconds = datediff("s",useTime,date+1)
	totalMinutes = totalSeconds/60
	if cint(totalMinutes) > totalMinutes then totalMinutes = cint(totalMinutes)-1 else totalMinutes = cint(totalMinutes)
	totalSeconds = totalSeconds - (totalMinutes*60)
	totalHours = totalMinutes/60
	if cint(totalHours) > totalHours then totalHours = cint(totalHours)-1 else totalHours = cint(totalHours)
	totalMinutes = totalMinutes - (totalHours*60)
	
	sql = "exec sp_productDetailsByItemID " & itemID & ",2"
	session("errorSQL") = sql
	set productRS = oConn.execute(sql)
	
	if productRS.EOF then response.Redirect("/")
	
	brandID = productRS("brandID")
	modelID = productRS("modelID")
	categoryID = productRS("typeID")
	partNumber = productRS("partNumber")
	itemDesc = productRS("itemDesc_CO")
	wePic = replace(productRS("itemPic"),".jpg","")
	itemPic = productRS("itemPic_CO")
	retail = productRS("price_retail")
	price = productRS("price_CO")
	inv_qty = prepInt(productRS("inv_qty"))
	itemLongDesc = productRS("itemLongDetail_CO")
	point1 = productRS("point1")
	point2 = productRS("point2")
	point3 = productRS("point3")
	point4 = productRS("point4")
	point5 = productRS("point5")
	point6 = productRS("point6")
	point7 = productRS("point7")
	point8 = productRS("point8")
	point9 = productRS("point9")
	point10 = productRS("point10")
	productColor = productRS("color")
	alwaysInStock = productRS("alwaysInStock")
	modelName = productRS("modelName")
	brandName = productRS("brandName")
	categoryName = productRS("typeName")
	reviews = productRS("reviewCnt")
	
	sql = 	"select c.brandName, b.modelName from we_items a left join we_models b on a.modelID = b.modelID left join we_brands c on b.brandID = c.brandID where a.partNumber = '" & partNumber & "'" &_
			"union " &_
			"select c.brandName, b.modelName from we_relatedItems a left join we_Models b on a.modelid = b.modelID left join we_Brands c on b.brandID = c.brandID where itemID = " & itemID & " order by c.brandName, b.modelName"
	session("errorSQL") = sql
	set compRS = oConn.execute(sql)
	
	if fso.fileExists(server.MapPath("/productpics/big/" & itemPic)) then
		useImgLink = "/productpics/big/" & itemPic
	elseif fso.fileExists(server.MapPath("/productpics/big/zoom/" & itemPic)) then
		useImgLink = "/productpics/big/zoom/" & itemPic
	end if
	
	compatLap = 0
	compatList = ""
	bgColor = "#fff"
	do while not compRS.EOF
		if prepStr(compRS("brandName")) <> "" then
			compatList = compatList & "<div style='float:left; width:260px; font-size:11px; padding-right:15px; background-color:" & bgColor & "; height:15px;'>" & compRS("brandName") & " " & compRS("modelName") & "</div>"
			if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
		end if
		compRS.movenext
	loop
	
	dblAvgRating = cdbl(0)
	
	sql = "select starRating as rating, reviewerNickName as nickname, reviewTitle, reviewBody as review, entryDate from XReview where itemID in (select itemid from we_items where partnumber = '" & partNumber & "') and isApproved = 1"
	session("errorSQL") = sql
	set reviewsRS = oConn.execute(sql)
	
	function altExists(imgLoc)
		if fso.fileExists(server.MapPath(imgLoc)) then
			altExists = true
		else
			altExists = false
		end if
	end function
	
	function reviewScore(partNumber, byref numAvg)
'		sql = "select reviewRatingAverageForPartNumber as avgRating from we_itemsStatsBySite where siteID = 2 and itemID = " & itemID
		sql = 	"select	isnull(avg(starRating), 0) as avgRating " & vbcrlf & _
				"from	XReview " & vbcrlf & _
				"where	siteID = 2 and itemID in (select itemid from we_items where partnumber = '" & partNumber & "')"
		session("errorSQL") = sql
		set reviewRS = oConn.execute(sql)
		if not reviewRS.EOF then
			numAvg = cint(prepInt(reviewRS("avgRating")))
			reviewScore = "<img src='/images/mobile/icons/big" & numAvg & "Star.gif' border='0' />"
		end if
	end function
%>
<!--#include virtual="/template/topCO.asp"-->
<div id="main">
    <!-- rich snippets mark up -->
    <div itemscope itemtype="http://data-vocabulary.org/Product">
        <div class="tb breadCrumbBox mCenter">
            <div class="fl crumb"><a href="/" class="breadCrumb">Home</a></div>
            <div class="fl breadCrumbSplit"></div>
            <div class="fl crumb"><a href="/c-<%=categoryID%>-<%=formatSEO("cell phone " & replace(categoryName,"&","and"))%>.html" class="breadCrumb"><%=catNameSwap(categoryName)%></a></div>
            <div class="fl breadCrumbSplit"></div>
            <div class="fl crumb"><a href="/sc-<%=categoryID%>-sb-<%=brandID%>-<%=formatSEO("cell phone " & replace(categoryName,"&","and") & " for " & brandName)%>.html" class="breadCrumb"><%=brandName%></a></div>
            <div class="fl breadCrumbSplit"></div>
            <div class="fl crumb"><a href="/m-<%=modelID%>-accessories-for-<%=formatSEO(brandName & " " & modelName)%>.html" class="breadCrumb"><%=modelName%></a></div>
            <div class="fl bradCrumbSplit"></div>
            <div class="fl crumb"><a href="/m-<%=modelID%>-accessories-for-<%=formatSEO(brandName & " " & modelName)%>.html" class="breadCrumb">Accessories</a></div>
        </div>
        <div style="clear:both; height:7px;"></div>
        <div class="promoBanner" onclick="showPromoDetails()"></div>
        <div id="promoDetails" class="tb promoDetails" onclick="showPromoDetails()" style="display:none;">
            <div class="tb promoCenterBox">
                <div class="fl promoItem"></div>
                <div class="fl promoText">
                    <div class="tb promoCheckRow">
                        <div class="fl promoCheckmark"></div>
                        <div class="fl promoDetailTxt">A pen AND stylus in one!</div>
                    </div>
                    <div class="tb promoCheckRow">
                        <div class="fl promoCheckmark"></div>
                        <div class="fl promoDetailTxt">Perfect for navigating on touch screens.</div>
                    </div>
                    <div class="tb promoCheckRow">
                        <div class="fl promoCheckmark"></div>
                        <div class="fl promoDetailTxt">Sleek and super smooth twisting barrel with black ink.</div>
                    </div>
                    <div class="tb promoHideBox">[X] HIDE</div>
                </div>
            </div>
        </div>
        <div class="pageDivide"></div>
        <% if (inv_qty > 0 or alwaysInStock) and price > 0 then %>
        <div id="withPromo" class="addButton_top">
            <form action="/basket.html" method="post">
            <input type="hidden" name="qty" value="1" />
            <input type="hidden" name="itemID" value="<%=itemID%>" />
            <input type="hidden" name="promoItemID" value="<%=promoItemID%>" />
            <input type="image" name="addToCart" src="/images/mobile/buttons/addToCart_2Tone.gif" width="282" height="36" border="0" value="add to cart" onclick="br_cartTracking('add', '<%=itemid%>', '<%=productColor%>', '<%=replace(itemDesc, "'", "\'")%>', '<%=price%>');" />
            </form>
        </div>
        <% end if %>
        <div class="itemNumbers">PART #: <%=partnumber%> | ITEM ID: <%=itemID%></div>
        <span itemprop="name"><div class="productTitle2"><%=itemDesc%></div></span>
        <% if reviews > 0 then %>
        <div class="productReviewStars" onclick="jumpToReviews()">
            <div class="fl starRating"><%=reviewScore(partNumber, dblAvgRating)%></div>
            <div class="fl reviewCnt">(<%=reviews%>)</div>
        </div>
        <% end if %>
        <div class="productPic"><img id="imgLarge" itemprop="image" src="<%=useImgLink%>" border="0" width="300" /></div>
        <% if altExists("/productPics/alts/thumbs/" & wePic & "-0.jpg") then %>
        <div class="altImages2">
            <div class="centerContain">
                <div class="fl altImg2" onclick="zoomImg('/productpics/big/<%=itemPic%>')"><img src="/productPics/alts/thumbs/<%=itemPic%>" border="0" /></div>
                <div class="fl altImg2" onclick="zoomImg('/altPics/<%=wePic%>-0.jpg')"><img src="/productPics/alts/thumbs/<%=wePic%>-0.jpg" border="0" /></div>
                <% if altExists("/productPics/alts/thumbs/" & wePic & "-1.jpg") then %><div class="fl altImg2" onclick="zoomImg('/altPics/<%=wePic%>-1.jpg')"><img src="/productPics/alts/thumbs/<%=wePic%>-1.jpg" border="0" /></div><% end if %>
            </div>
        </div>
        <% end if %>
        <% if (inv_qty > 0 or alwaysInStock) and price > 0 then %>
        <span itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">    
            <meta itemprop="currency" content="USD" />
            
            <div class="tb mCenter pricingRow">
                <div class="fl priceTitle_b">Retail Price:</div>
                <div class="fl retailPrice_b"><%=formatCurrency(retail,2)%></div>
            </div>
            <div class="tb mCenter pricingRow">
                <div class="fl priceTitle_b">Wholesale Price:</div>
                <div class="fl wholesalePrice_b">$<%=formatnumber(price,2)%></div>
            </div>
            <div class="tb mCenter pricingRow">
                <div class="fl priceTitle_b">You Save:</div>
                <div class="fl percentOffValue_b"><%=formatCurrency(retail-price,2)%> (<%=formatPercent(1-(price/retail),0)%>)</div>
            </div>
            <div class="inStock">
                <div class="centerContain">
                    <div class="fl fastShip"><img src="/images/mobile/icons/fastShip.gif" border="0" /></div>
                    <div class="fl inStockTxt">
                        <div class="inStockGreen"><span itemprop="availability" content="in_stock">In stock and ready to ship!</span></div>
                        <div class="inStockBlack">Leaves our U.S. warehouse within 24 hours.</div>
                    </div>
                </div>
            </div>
        </span>
        <div class="addToCart">
            <div id="withPromoBottom" class="addButton">
                <form action="/basket.html" method="post">
                <input type="hidden" name="qty" value="1" />
                <input type="hidden" name="itemID" value="<%=itemID%>" />
                <input type="hidden" name="promoItemID" value="<%=promoItemID%>" />
                <input type="image" name="addToCart" src="/images/mobile/buttons/addToCart_2Tone.gif" width="282" height="36" border="0" value="add to cart" onclick="br_cartTracking('add', '<%=itemid%>', '<%=productColor%>', '<%=replace(itemDesc, "'", "\'")%>', '<%=price%>');" />
                </form>
            </div>
            <div class="saleMsg tb">
                <div class="saleTag fl"></div>
                <div class="saleMsgTxt fl">
                    Only <span class="hoursLeft"><%=totalHours%> hours</span> left to get the lowest pricing in store history. Order now!
                </div>
            </div>
            <div class="centerContain">
                <div class="safeShopping">
                    <div class="fl checkmark"></div>
                    <div class="fl safeMsg">Safe &amp; Secure Online Ordering.</div>
                </div>
                <div class="safeShopping">
                    <div class="fl checkmark"></div>
                    <div class="fl safeMsg">110% Low Price Guarantee.</div>
                </div>
                <div class="safeShopping">
                    <div class="fl checkmark"></div>
                    <div class="fl safeMsg">Unconditional 90-Day Return Policy.</div>
                </div>
            </div>
        </div>
        <% else %>
        <div class="outOfStock">This Item is Currently Out of Stock</div>
        <% end if %>
        <div class="detailTabs">
            <div class="centerContain pr">
                <div id="DetailsTab" class="fl detailTab_on tabDetails" onclick="showTab('Details')">DETAILS</div>
                <div id="CompatibilityTab" class="fl detailTab_off tabCompatibility" onclick="showTab('Compatibility')">COMPATIBILITY</div>
                <div id="ReviewsTab" class="fl detailTab_off tabReviews" onclick="showTab('Reviews')">REVIEWS</div>
                <div id="activeTabArrow" class="activeTab"></div>
            </div>
        </div>
        <div id="DetailsView" class="baseView showView">
            <span itemprop="description">
            <div class="centerContain">
                <%=itemLongDesc%>
                <ul>
                    <% if prepStr(point1) <> "" then %><li><%=point1%></li><% end if %>
                    <% if prepStr(point2) <> "" then %><li><%=point2%></li><% end if %>
                    <% if prepStr(point3) <> "" then %><li><%=point3%></li><% end if %>
                    <% if prepStr(point4) <> "" then %><li><%=point4%></li><% end if %>
                    <% if prepStr(point5) <> "" then %><li><%=point5%></li><% end if %>
                    <% if prepStr(point6) <> "" then %><li><%=point6%></li><% end if %>
                    <% if prepStr(point7) <> "" then %><li><%=point7%></li><% end if %>
                    <% if prepStr(point8) <> "" then %><li><%=point8%></li><% end if %>
                    <% if prepStr(point9) <> "" then %><li><%=point9%></li><% end if %>
                    <% if prepStr(point10) <> "" then %><li><%=point10%></li><% end if %>
                </ul>
            </div>
            </span>
        </div>
        <div id="CompatibilityView" class="baseView hideView">
            <div class="centerContain"><%=compatList%></div>
        </div>
        <div id="ReviewsView" class="baseView hideView">
            <div class="centerContain">
                <%
                if reviewsRS.EOF then
                %>
                <div class="noReviews">No Reviews Have Been Left For This Product</div>
                <%
                else
                    do while not reviewsRS.EOF
                        nickname = reviewsRS("nickname")
                        rating = reviewsRS("rating")
                        title = reviewsRS("reviewTitle")
                        review = reviewsRS("review")
                        reviewDate = reviewsRS("entryDate")
                %>
                <div class="fl reviewBox">
                    <div class="fl reviewTopRow">
                        <div class="fl reviewer"><%=title%></div>
                        <div class="fl reviewScore"><img src="/images/mobile/icons/<%=rating%>StarGold.gif" /></div>
                    </div>
                    <div class="fl reviewContent">
                        <div class="reviewTitle">Reviewed By: <%=nickname%></div>
                        <div class="reviewTxt"><%=review%></div>
                    </div>
                    <div class="reviewDate"><%=reviewDate%></div>
                </div>
                <%
                        reviewsRS.movenext
                    loop
                end if
                %>
            </div>
        </div>
        <span style="display:none;" itemprop="review" itemscope itemtype="http://data-vocabulary.org/Review-aggregate">
            <span itemprop="rating"><%=round(dblAvgRating, 2)%></span> out of 5 stars, based on <span itemprop="count"><%=formatnumber(reviews,0)%></span> reviews
        </span> 

		<!--        
        <div id="mlt_box">
            <div class="centerContain tb" style="padding:10px 0px 10px 0px; margin-top:10px; color:#444; font-size:26px; text-align:center;">
                MORE LIKE THIS
            </div>
            <div class="mltGredient centerContain tb"></div>
            <div id="brm-mlt-widget">
            </div>

            <div style="display:none;">
                <div id="br-prod-title"><%=itemDesc%></div>
                <div id="br-prodid"><%=itemID%></div>    
            </div>
        </div>
        -->
        
		<div class="centerContain tb" style="padding:10px 0px 10px 0px; margin-top:10px;">
        	<div style="width:90%; margin:auto; border-top:1px solid #ccc;"></div>
        	<a href="javascript:location.href='/search/search.html'+BR.mobile.getJfyUrl();"><div id="btnJFY"></div></a>
        	<div style="width:90%; margin:10px auto auto auto; border-top:1px solid #ccc;"></div>
        </div>        
    </div>
    <!--// rich snippets mark up -->
</div>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">window.scroll(0,150)</script>