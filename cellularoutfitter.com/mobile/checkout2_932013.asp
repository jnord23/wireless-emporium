<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<%
	pageName = "Checkout"
	securePage = 1
	
	dim itemID : itemID = prepInt(request.Form("itemID"))
	dim qty : qty = prepInt(request.Form("qty"))
	dim mySession : mySession = prepStr(request.cookies("mySession"))
	dim cartTotal : cartTotal = 0
	dim cartItemNum : cartItemNum = 0
	
	dim fname : fname = prepStr(request.Form("fname"))
	dim lname : lname = prepStr(request.Form("lname"))
	dim sAddress1 : sAddress1 = prepStr(request.Form("sAddress1"))
	dim sAddress2 : sAddress2 = prepStr(request.Form("sAddress2"))
	dim sCity : sCity = prepStr(request.Form("sCity"))
	dim sState : sState = prepStr(request.Form("sState"))
	dim sZip : sZip = prepStr(request.Form("sZip"))
	dim phone : phone = prepStr(request.Form("phone"))
	dim email : email = prepStr(request.Form("email"))
	dim bAddress1 : bAddress1 = prepStr(request.Form("bAddress1"))
	dim bAddress2 : bAddress2 = prepStr(request.Form("bAddress2"))
	dim bCity : bCity = prepStr(request.Form("bCity"))
	dim bState : bState = prepStr(request.Form("bState"))
	dim bZip : bZip = prepStr(request.Form("bZip"))
	dim shippingTotal : shippingTotal = prepInt(request.Form("shippingTotal"))
	dim subTotal : subTotal = prepInt(request.Form("subTotal"))
	dim taxAmt : taxAmt = prepInt(request.Form("taxAmt"))
	dim grandTotal : grandTotal = prepInt(request.Form("grandTotal"))
	dim showShipping : showShipping = prepInt(request.Form("showShipping"))
	dim totalQty : totalQty = prepInt(request.Form("totalQty"))
	dim shiptypeID : shiptypeID = prepInt(request.Form("shiptypeID"))
	
	if sAddress1 = "" then
		sAddress1 = bAddress1
		sAddress2 = bAddress2
		sCity = bCity
		sState = bState
		sZip = bZip
	end if
	
	if grandTotal = 0 then response.Redirect("/checkout1.html")
	if showShipping = 1 then
		shipSummaryClass = "summaryRow"
		estTotalClass = "summaryRowDark hidden"
		grandTotalClass = "summaryRowDark"
	else
		shipSummaryClass = "summaryRow hidden"
		estTotalClass = "summaryRowDark"
		grandTotalClass = "summaryRowDark hidden"
	end if
	
	if mySession = "" then
		mySession = session.SessionID
		response.cookies("mySession") = mySession
		response.cookies("mySession").expires = now+30
	end if
	
	sql = "exec sp_itemsInBasket 2, " & mySession
	session("errorSQL") = sql
	set cartItemsRS = oConn.execute(sql)
%>
<!--#include virtual="/template/topCO.asp"-->
<form method="post" action="/framework/processing.html" name="checkout2Form">
<div class="checkoutSteps">
	<div class="centerContain">
	    <div class="fl lockImg"></div>
    	<div class="fl step1 inactiveCheckoutStep">1. Your Info</div>
	    <div class="fl step2">2. Payment</div>
    	<div class="fl step2 inactiveCheckoutStep">3. Review</div>
    </div>
</div>
<div class="checkoutTitleBar">Secure 128-Bit SSL Encrypted Payment</div>
<div class="lightBg">
	<div class="centerContain">
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Cardholder Name:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="cc_cardOwner" value="" /></div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Credit Card #:</div>
            <div class="fr creditCards"><img src="/images/mobile/icons/creditCardsSmall.gif" border="0" /></div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="cardNum" value="" maxlength="16" /></div>
        <div class="creditCardExamples">
            <div class="fieldExamples">No Spaces/Dashes</div>
            <div class="fieldExamples">Example: 1234123412341234</div>
        </div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Security Code:</div>
        </div>
        <div class="formFieldSmall"><input class="invisibleTextSmall" type="text" name="secCode" value="" maxlength="4" /></div>
        <div class="formTitle">
            <div class="fl requiredField"></div>
            <div class="fl">Expiration Date:</div>
        </div>
        <div>
            <div class="fl formFieldSmall">
                <select class="invisibleSelectSmall" name="cc_month">
                    <% for i = 1 to 12 %>
                    <option value="<%=i%>"><%=i%></option>
                    <% next %>
                </select>
            </div>
            <div class="fl expDateEntrySpacer">/</div>
            <div class="fl formFieldSmall">
                <select class="invisibleSelectSmall" name="cc_year">
                    <%
                    startingYear = year(now)
                    for i = startingYear to startingYear + 15 %>
                    <option value="<%=i%>"><%=i%></option>
                    <% next %>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="checkoutTitleBar">Billing Address</div>
<div class="lightBgBordered">
	<div class="centerContain">
        <div class="billingAddressBox">
            <div class="billingAddress">
                <%=fname & " " & lname%><br />
                <%=bAddress1%><br />
                <% if bAddress2 <> "" then %>
                <%=bAddress2%><br />
                <% end if %>
                <%=bCity & ", " & bState & " " & bZip%>
            </div>
            <div class="changeBilling" onclick="history.back(-1);">
                <div class="fl"><img src="/images/mobile/icons/deleteItem.gif" border="0" /></div>
                <div class="fl chgBillingTxt">Change Billing Address</div>
            </div>
        </div>
    </div>
</div>
<div class="cartItem">
    <div class="cartSummaryTitle">CART SUMMARY</div>
    <div class="summaryRow">
        <div class="fl rowTitle">Cart Total:</div>
        <div class="fr rowValue"><%=formatCurrency(subTotal,2)%></div>
    </div>
    <div class="summaryRow">
        <div class="fl rowTitle">Tax:</div>
        <div id="taxDiv" class="fr rowValue"><%=formatCurrency(taxAmt,2)%></div>
    </div>    
    <div id="shipSummary" class="<%=shipSummaryClass%>">
        <div class="fl rowTitle">Shipping:</div>
        <div id="shippingCostDiv" class="fr rowValue"><%=formatCurrency(shippingTotal,2)%></div>
    </div>
    <div id="estSummary" class="<%=estTotalClass%>">
        <div class="fl rowTitle">Estimated Total:</div>
        <div id="estTotalDiv" class="fr rowValueGreen"><%=formatCurrency(subTotal+taxAmt,2)%></div>
    </div>
    <div id="grandSummary" class="<%=grandTotalClass%>">
        <div class="fl rowTitle">Grand Total:</div>
        <div id="grandTotalDiv" class="fr rowValueGreen"><%=formatCurrency(grandTotal,2)%></div>
    </div>
</div>
<div class="proceedButton">
	<input type="image" name="Proceed to Checkout" src="/images/mobile/buttons/submitPayment.gif" border="0" onclick="return(checkout2FormReview())" />
    <input type="hidden" name="fname" value="<%=fname%>" />
    <input type="hidden" name="lname" value="<%=lname%>" />
    <input type="hidden" name="sAddress1" value="<%=sAddress1%>" />
    <input type="hidden" name="sAddress2" value="<%=sAddress2%>" />
    <input type="hidden" name="sCity" value="<%=sCity%>" />
    <input type="hidden" name="sState" value="<%=sState%>" />
    <input type="hidden" name="sZip" value="<%=sZip%>" />
    <input type="hidden" name="phone" value="<%=phone%>" />
    <input type="hidden" name="phoneNumber" value="<%=phone%>" />
    <input type="hidden" name="email" value="<%=email%>" />
    <input type="hidden" name="bAddress1" value="<%=bAddress1%>" />
    <input type="hidden" name="bAddress2" value="<%=bAddress2%>" />
    <input type="hidden" name="bCity" value="<%=bCity%>" />
    <input type="hidden" name="bState" value="<%=bState%>" />
    <input type="hidden" name="bZip" value="<%=bZip%>" />
    <input type="hidden" name="shippingTotal" value="<%=shippingTotal%>" />
    <input type="hidden" name="subTotal" value="<%=subTotal%>" />
    <input type="hidden" name="nCATax" value="<%=taxAmt%>" />
    <input type="hidden" name="grandTotal" value="<%=grandTotal%>" />
    <input type="hidden" name="nTotalQuantity" value="<%=totalQty%>" />
    <input type="hidden" name="shiptypeID" value="<%=shiptypeID%>" />
    <input type="hidden" name="shiptype" value="<%=shiptypeID%>" />
    <input type="hidden" name="mobile" value="1" />
    <%
	do while not cartItemsRS.EOF
		cartItemNum = cartItemNum + 1
	%>
    <input type="hidden" name="ssl_item_number_<%=cartItemNum%>" value="<%=cartItemsRS("itemID")%>">
	<input type="hidden" name="ssl_item_qty_<%=cartItemNum%>" value="<%=cartItemsRS("qty")%>">
	<input type="hidden" name="ssl_item_price_<%=cartItemNum%>" value="<%=cartItemsRS("price_CO")%>">
    <%
		cartItemsRS.movenext
	loop
	%>
    <input type="hidden" name="ssl_ProdIdCount" value="<%=cartItemNum%>" />
</div>
</form>
<script language="javascript">
	var taxValue = <%=Application("taxMath")%>
	var taxAmt = 0
</script>
<!--#include virtual="/template/bottom.asp"-->