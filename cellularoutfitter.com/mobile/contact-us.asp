<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "Help"
	
	dim reset : reset = prepInt(request.QueryString("reset"))
	if reset = 1 then
		response.Cookies("mobileBrandID") = 0
		response.Cookies("mobileModelID") = 0
	end if
	
	sql = "select typeID, typeName_CO from we_types where typeID not in (20,24,22,12,14,9,4,15,23,13,17,21,25) order by order_CoMobile"
	session("errorSQL") = sql
	set categoryListRS = oConn.execute(sql)
	
	dim section : section = request.QueryString("section")
	dim faqid : faqid = request.QueryString("faqid")
	
	
	sql = "select * from XFaq"
	session("errorSQL") = sql
	set testRS = oConn.execute(sql)
	
%>
<!--#include virtual="/template/topCO.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_Email_Validation.asp"-->
<link href='/template/css/faq.css' rel='stylesheet' type='text/css' />
<div class="faq-search">
	<div class="centerContain">
		<form name="frm_search" method="get" onsubmit="return checkSearchLength();" action="faq.html">
			<input type="text" name="search" id="search" class="fl search-field" value="Search by keyword or sku #" onfocus="if(this.value='Search by keyword or sku #') this.value = '';" onblur="if(!this.value) this.value='Search by keyword or sku #';" />
			<input type="submit" class="fl search-submit" value="" />
		</form>
	</div>
</div>
<div class="contact-us">
	<div class="centerContain">
		<%
            if request.form("submitted") = "1" then
                dim cdo_to, cdo_from, cdo_subject, cdo_body
                cdo_to = "support@cellularoutfitter.com"
'                cdo_to = "ruben@wirelessemporium.com"
                cdo_from = prepStr(request.form("email"))
'                cdo_from = "sales@cellularoutfitter.com"
                if IsValidEmail(cdo_from) = true then
                    cdo_subject = "Contact Us"
                    cdo_body = "<p>Name: " & prepStr(request.form("name")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Phone: " & prepStr(request.form("phonenumber")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Type of Inquiry: " & prepStr(request.form("subject")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Order #: " & prepStr(request.form("orderno")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Product: " & prepStr(request.form("product")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Question: " & prepStr(replace(request.form("question"),vbcrlf,"<br>")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Date/Time Sent: " & now & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<table width='500' border='0' cellspacing='3' cellpadding='3' align='center'>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td height='50' colspan='2'>&nbsp;</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='500'><table width='500' border='0' cellspacing='0' cellpadding='0'>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='207' height='50' align='center'><img src='/images/coLogo.gif'></td><td width='293'>&nbsp;</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td colspan='2'>&nbsp;</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td><table width='400' cellspacing='0' cellpadding='0' align='center'>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Name </td><td width='10'> : </td><td> " & prepStr(request.form("name")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>E-Mail : </td><td width='10'> : </td><td> " & prepStr(request.form("email")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Type of Inquiry  </td><td width='10'> : </td><td> " & prepStr(request.form("subject")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Phone : </td><td width='10'> : </td><td> " & prepStr(request.form("phonenumber")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Product : </td><td width='10'> : </td><td> " & prepStr(request.form("product")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Order #  </td><td width='10'> : </td><td> " & prepStr(request.form("orderno")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Question : </td><td width='10'> : </td><td> " & prepStr(request.form("question")) & "</td>" & vbcrlf
                    cdo_body = cdo_body & "</tr></table></td>" & vbcrlf
                    cdo_body = cdo_body & "</tr></table></td>" & vbcrlf
                    cdo_body = cdo_body & "</tr></table>" & vbcrlf
'                    CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
                    CDOSend2 cdo_to,cdo_from,cdo_subject,cdo_body,"","sales@cellularoutfitter.com"
'                    CDOSend2 cdo_to,cdo_from,cdo_subject,cdo_body,"","ruben@wirelessemporium.com"
                    %>
                    <p>
                        <strong>
                            <br><br>
                            Thank you for contacting CellularOutfitter.com!
                        </strong>
                    </p>
                    <p>
                        <strong>
                            A Representative from the appropriate department will contact you promptly.
                            If your inquiry is sent between normal business hours (8am-5pm PST M-F)
                            expect an answer in the next few hours or within one business day.
                        </strong>
                    </p>
                    <p>
                        <strong>
                            <br><br>
                            CellularOutfitter.com
                        </strong>
                    </p>
                    <%
                else
                    %>
                    <p>
                        <strong>
                            <br><br>
                            The e-mail address you entered was not valid.
                        </strong>
                    </p>
                    <p>
                        <strong>
                            Please <a href="javascript:history.back();">go back</a> and try again.
                        </strong>
                    </p>
                    <p>
                        <strong>
                            <br><br>
                            CellularOutfitter.com
                        </strong>
                    </p>
                    <%
                end if
            else
                %>
		<div class="contact-header">Contact Us</div>
		<div class="back-to-help"><a href="faq.html">Help Topics &#8594;</a></div>
		<form name="contactus" method="POST">
			<input type="hidden" name="submitted" value="1">
			<div class="field-label">Type of Inquiry: <span class="req">*</span></div>
			<div class="field-required">* REQUIRED</div>
			<div class="field">
				<select name="subject" class="long-field">
					<option value="Request for RMA">Request for RMA</option>
					<option value="Product Sales & Usage Inquiries">Product Sales & Usage Inquiries</option>
					<option value="Post-Order Customer Service Inquiries">Post-Order Customer Service Inquiries</option>
					<option value="Billing Inquiries">Billing Inquiries</option>
					<option value="Marketing Inquiries">Marketing Inquiries</option>
					<option value="General Inquiries">General Inquiries</option>
				</select>
			</div>
			<div class="field-label">Order Number: <span class="req">*</span></div>
			<div class="field">
				<input type="text" class="short-field" name="orderno" value="" />
			</div>
			<div class="field-label">First & Last Name: <span class="req">*</span></div>
			<div class="field">
				<input type="text" class="long-field" name="name" value="" />
			</div>
			<div class="field-label">Email Address: <span class="req">*</span></div>
			<div class="field">
				<input type="text" class="long-field" name="email" value="" />
			</div>
			<div class="field-label">Phone Number: <span class="req">*</span></div>
			<div class="field">
				<input type="text" class="long-field" name="phonenumber" value="" />
			</div>
			<div class="field-label">Question: <span class="req">*</span></div>
			<div class="field">
				<textarea name="question" class="txtarea"></textarea>
			</div>
			<div class="submit">
				<input type="submit" class="submit-contact" value="Submit &raquo;" />
			</div>
		</form>
		<script language="javascript1.2">
			function reset() {
				document.contactus.name.value="";
				document.contactus.email.value="";
				document.contactus.subject.value="";
				document.contactus.phonenumber.value="";
				document.contactus.product.value="";
				document.contactus.orderno.value="";
				document.contactus.question.value="";
			}
			
			function validateEmail(email) {
				var splitted = email.match("^(.+)@(.+)$");
				if (splitted == null) return false;
				if (splitted[1] != null) {
					var regexp_user=/^\"?[\w-_\.]*\"?$/;
					if (splitted[1].match(regexp_user) == null) return false;
				}
				if (splitted[2] != null) {
					var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
					if (splitted[2].match(regexp_domain) == null) {
						var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
						if (splitted[2].match(regexp_ip) == null) return false;
					}
					return true;
				}
				return false;
			}
			
			function validate() {
				if (document.contactus.name.value.length==0) {
					alert("Please Enter Name");
					return false;
				}
				if (document.contactus.subject.value=="Select here...") {
					alert("Please select the type of Inquiry");
					return false;
				}
				if (document.contactus.question.value.length==0) {
					alert("Please Enter Question");
					return false;
				}
				if (document.contactus.email.value.length==0) {
					alert("Please Enter Email Address");
					return false;
				}
				if (!validateEmail(document.contactus.email.value)) { 
					alert("please enter valid Email address");
					return false;
				}
				document.contactus.submit()
				return false;
			}
		</script>
		<%
			end if
		%>
	</div>
</div>
<script type="text/javascript">
	function checkSearchLength() {
		var search = document.getElementById('search').value.trim();
		var sch_len = search.length;

		if(sch_len < 2) { 
			alert('Search term should be at least 2 characters.'); 
			return false; 
		}
	}
</script>
<!--#include virtual="/template/bottom.asp"-->