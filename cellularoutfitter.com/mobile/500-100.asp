<%
'Option Explicit

Const lngMaxFormBytes = 200

if session("acctID") = 577086 then
	forceErrorData = 1
else
	forceErrorData = 0
end if
forceErrorData = 0

Dim objASPError, blnErrorWritten, strServername, strServerIP, strRemoteIP
Dim strMethod, lngPos, datNow, strQueryString, strURL

If Response.Buffer Then
	Response.Clear
	Response.Status = "500 Internal Server Error"
	Response.ContentType = "text/html"
	Response.Expires = 0
End If

Set objASPError = Server.GetLastError

dim pageTitle
pageTitle = "The page cannot be displayed"
%>
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->

					<td valign="top" width="800" align="center">
						<table border="0" cellspacing="0" cellpadding="0" width="746">
							<tr>
								<td align="left" valign="top" width="100%" class="breadcrumbFinal"><img src="/images/spacer.gif" width="1" height="10"></td>
							</tr>
							<tr>
								<td align="left" valign="top">
									<% if Request.Cookies("admin")("username") = "jnord" or request.cookies("myAccount") = "575054" or forceErrorData = 1 then %>
                                    <table border="0" cellpadding="3" cellspacing="0">
                                    	<tr>
                                        	<td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Category:</td>
                                        	<td align="left"><%=objASPError.Category%></td>
                                        </tr>
                                        <tr>
                                        	<td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">ASPCode:</td>
                                        	<td align="left"><%=objASPError.ASPCode%></td>
                                        </tr>
                                        <tr>
                                        	<td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">ASPDescription:</td>
                                        	<td align="left"><%=objASPError.ASPDescription%></td>
                                        </tr>
                                        <tr>
                                        	<td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Number:</td>
                                        	<td align="left"><%=objASPError.Number%></td>
                                        </tr>
                                        <tr>
                                        	<td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Description:</td>
                                        	<td align="left"><%=objASPError.Description%></td>
                                        </tr>
                                        <tr>
                                        	<td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">File:</td>
                                        	<td align="left"><%=objASPError.File%></td>
                                        </tr>
                                        <tr>
                                        	<td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Source:</td>
                                        	<td align="left"><%=objASPError.Source%></td>
                                        </tr>
                                        <tr>
                                        	<td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Column:</td>
                                        	<td align="left"><%=objASPError.Column%></td>
                                        </tr>
                                        <tr>
                                        	<td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Line:</td>
                                        	<td align="left"><%=objASPError.Line%></td>
                                        </tr>
                                        <tr>
                                        	<td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">errorSQL:</td>
                                        	<td align="left"><%=session("errorSQL")%></td>
                                        </tr>
                                    </table>
                                    <%
										response.End()
									end if
									%>
									<%if inStr(objASPError.Description,"Communication link failure") > 0 or inStr(objASPError.Description,"[DBNETLIB]") > 0 then%>
										<p style="font-size:28px;color:#000000;font-weight:bold;font-family:Verdana,Arial,Helvetica,sans-serif;">Website Temporarily Down</p>
										<h4>
											To place an order via telephone, call us at:<br>
											(888) 725-7575<br>
											(Available Mon-Fri 9am-5pm Pacific Time)<br>
										</h4>
										<p>There is a problem with the web server.</p>
										<hr>
										<p>Our sincere apologies for any inconvenience this may have caused.</p>
										<hr>
										<p>Contact the <a href="mailto:webmaster@wirelessemporium.com?subject=From%20Website%20Down%20Page">Webmaster</a> if you wish to report any other information concerning this URL address.</p>
									<%else%>
										<p style="font-size:28px;color:#000000;font-weight:bold;font-family:Verdana,Arial,Helvetica,sans-serif;">Website Error</p>
										<h4>
											To place an order via telephone, call us at:<br>
											(888) 725-7575<br>
											(Available Mon-Fri 9am-5pm Pacific Time)
										</h4>
										<hr>
										<p>There is a problem with the page you are trying to reach and it cannot be displayed.</p>
										<hr>
										<p>An e-mail has been sent to the <a href="mailto:webmaster@wirelessemporium.com?subject=From%20ASP%20Error%20Page">Webmaster</a> about this problem.</p>
										<p>Our sincere apologies for any inconvenience this may have caused.</p>
										<hr>
										<p>Contact the <a href="mailto:webmaster@wirelessemporium.com?subject=From%20ASP%20Error%20Page">Webmaster</a> if you wish to report any other information concerning this URL address.</p>
									<%end if%>
									<p>&nbsp;</p>
								</td>
							</tr>
						</table>
					</td>
	
<%
strBody = strBody & Server.HTMLEncode(objASPError.Category)
If objASPError.ASPCode > "" Then strBody = strBody & Server.HTMLEncode(", " & objASPError.ASPCode) & vbCrLf

' Here is where we can decide what NOT to send an e-mail for based on sepcific ASP error
dontSendIt = 0
if inStr(objASPError.ASPDescription,"&refer=froogle") > 0 or inStr(objASPError.ASPDescription,"&refer=gawfroogle") > 0 or inStr(objASPError.Description,"&refer=froogle") > 0 or inStr(objASPError.Description,"&refer=gawfroogle") > 0 then dontSendIt = 1

strBody = strBody & Server.HTMLEncode(" (0x" & Hex(objASPError.Number) & ")" ) & "<br>" & vbCrLf
If objASPError.ASPDescription > "" Then 
	strBody = strBody & Server.HTMLEncode(objASPError.ASPDescription) & "<br>" & vbCrLf
elseIf (objASPError.Description > "") Then 
	strBody = strBody & Server.HTMLEncode(objASPError.Description) & "<br>" & vbCrLf
end if
blnErrorWritten = False

' Only show the Source if it is available and the request is from the same machine as IIS
If objASPError.Source > "" Then
	strServername = LCase(Request.ServerVariables("SERVER_NAME"))
	strServerIP = Request.ServerVariables("LOCAL_ADDR")
	strRemoteIP = Request.ServerVariables("REMOTE_ADDR")
	If (strServerIP = strRemoteIP) And objASPError.File <> "?" Then
		strBody = strBody & Server.HTMLEncode(objASPError.File) & vbCrLf
		strBody = strBody & ", line " & objASPError.Line
		strBody = strBody & ", column " & objASPError.Column
		strBody = strBody & "<br>" & vbCrLf
		strBody = strBody & "<font style=""COLOR:000000; FONT: 8pt/11pt courier new""><b>" & vbCrLf
		strBody = strBody & Server.HTMLEncode(objASPError.Source) & "<br>" & vbCrLf
		If objASPError.Column > 0 Then strBody = strBody & String((objASPError.Column - 1), "-") & "^<br>"
		strBody = strBody & "</b></font>" & vbCrLf
		blnErrorWritten = True
	End If
End If

If Not blnErrorWritten And objASPError.File <> "?" Then
	strBody = strBody & "<b>" & Server.HTMLEncode(objASPError.File) & vbCrLf
	If objASPError.Line > 0 Then strBody = strBody & Server.HTMLEncode(", line " & objASPError.Line)
	If objASPError.Column > 0 Then strBody = strBody & ", column " & objASPError.Column
	strBody = strBody & "</b><br>" & vbCrLf
End If

strBody = strBody & "<br><br>" & vbCrLf
strBody = strBody & "<ul>" & vbCrLf
strBody = strBody & "<li>Browser Type:<br>" & vbCrLf
strBody = strBody & Server.HTMLEncode(Request.ServerVariables("HTTP_USER_AGENT")) & vbCrLf
if inStr(Request.ServerVariables("HTTP_USER_AGENT"),"http://www.google.com/bot.html") > 0 or inStr(Request.ServerVariables("HTTP_USER_AGENT"),"http://help.yahoo.com/help/us/ysearch/slurp") > 0 then dontSendIt = 1
strBody = strBody & "<br><br></li>" & vbCrLf
strBody = strBody & "<li>Page:<br>" & vbCrLf

strMethod = Request.ServerVariables("REQUEST_METHOD")
strBody = strBody & strMethod & " " & vbCrLf
If strMethod = "POST" Then
	strBody = strBody & Request.TotalBytes & " bytes to " & vbCrLf
End If
	
strBody = strBody & Request.ServerVariables("SCRIPT_NAME") & vbCrLf
strBody = strBody & "</li>" & vbCrLf
If strMethod = "POST" Then
	strBody = strBody & "<p><li>POST Data:<br>" & vbCrLf
	' On Error in case Request.BinaryRead was executed in the page that triggered the error.
	On Error Resume Next
	If Request.TotalBytes > lngMaxFormBytes Then
		strBody = strBody & Server.HTMLEncode(Left(Request.Form, lngMaxFormBytes)) & " . . ." & vbCrLf
	Else
		strBody = strBody & Server.HTMLEncode(Request.Form) & vbCrLf
	End If
	On Error Goto 0
	strBody = strBody & "</li>" & vbCrLf
End If

strBody = strBody & "<br><br></li>" & vbCrLf
strBody = strBody & "<li>Time:<br>" & vbCrLf

datNow = Now()
strBody = strBody & Server.HTMLEncode(FormatDateTime(datNow, 1) & ", " & FormatDateTime(datNow, 3))
on error resume next
Session.Codepage = bakCodepage 
on error goto 0

SERVER_NAME = Request.ServerVariables("SERVER_NAME")
SCRIPT_NAME = Request.ServerVariables("SCRIPT_NAME")
QUERY_STRING = Request.ServerVariables("QUERY_STRING")

strBody = strBody & "<br><br></li>" & vbCrLf
strBody = strBody & "</ul>" & vbCrLf
strBody = strBody & "<p>Site User: " & Request.Cookies("admin")("username") & "</p>" & vbCrLf
strBody = strBody & "<p>REMOTE_ADDR: " & Request.ServerVariables("REMOTE_ADDR") & "</p>" & vbCrLf
strBody = strBody & "<p>SERVER_NAME: " & Request.ServerVariables("SERVER_NAME") & "</p>" & vbCrLf
strBody = strBody & "<p>SCRIPT_NAME: " & Request.ServerVariables("SCRIPT_NAME") & "</p>" & vbCrLf
strBody = strBody & "<p>HTTP_REFERRER: " & Request.ServerVariables("HTTP_REFERER") & "</p>" & vbCrLf
strBody = strBody & "<p>&nbsp;</p>" & vbCrLf
strBody = strBody & "<p>LAST SQL:<br>" & session("errorSQL") & "</p>" & vbCrLf
strBody = strBody & "<p>&nbsp;</p>" & vbCrLf
strBody = strBody & "<p>LINK:<br><a href='http://" & SERVER_NAME & SCRIPT_NAME & "?" & QUERY_STRING & "'>" & SERVER_NAME & SCRIPT_NAME & "?" & QUERY_STRING & "</a></p>" & vbCrLf
strBody = strBody & "<p>&nbsp;</p>" & vbCrLf

'set objErrMail = Server.CreateObject("CDO.Message")
'with objErrMail
'	.From = "service@wirelessemporium.com"
'	.To = "jon@wirelessemporium.com,terry@wirelessemporium.com"
'	.Subject = "CO Mobile Website Problem"
'	.HTMLBody = strBody
'	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
'	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
'	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
'	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
'	.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
'	.Configuration.Fields.Update
'	.Send
'end with

if inStr(Request.ServerVariables("HTTP_USER_AGENT"),"scanalert") > 0 then dontSendIt = 1
if inStr(Request.ServerVariables("HTTP_USER_AGENT"),"twiceler") > 0 then dontSendIt = 1
if inStr(strBody,"DECLARE @S VARCHA") > 0 then dontSendIt = 1

if dontSendIt = 0 then
	objErrMail.Send
end if
set objErrMail = nothing
%>
<!-- Google Analytics Start -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics End -->