<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "idmyphone"
%>
<!--#include virtual="/template/topCO.asp"-->
<div id="main">
    <div class="centerContain" style="color:#333; font-size:18px; font-family:Arial, Helvetica, sans-serif;" align="center">
	<%
	uAgnt = replace(request.ServerVariables("HTTP_USER_AGENT"), "'", "''")
	sql = "exec mobileRedirectByUA '" & uAgnt & "', 1"
	session("errorSQL") = sql
	set rsRedirectUA = oConn.execute(sql)
	if not rsRedirectUA.eof then
		UABrandID = rsRedirectUA("brandid")
		UABrandName = rsRedirectUA("brandName")
		UAModelID = prepInt(rsRedirectUA("modelid"))
		UAModelName = rsRedirectUA("modelName")
		UAModelImg = rsRedirectUA("modelImg")		
		UAPhoneTypeID = rsRedirectUA("phoneTypeID")
		UAPhoneTypeNme = rsRedirectUA("phoneTypeName")
		UAIsApple = rsRedirectUA("isApple")
		if UAModelID > 0 and UABrandID > 0 then
'			response.write "UAModelID:" & UAModelID & "<br>"
'			response.write "UABrandID:" & UABrandID & "<br>"
'			response.write "UAModelImg:" & UAModelImg & "<br>"
		%>
		<div class="tb mt10 pt10">We've detected that you are using the</div>
		<div class="tb mt10 bold" style="font-size:24px; color:#286EAC;"><%=UABrandName%>&nbsp;<%=UAModelName%></div>
        <div class="tb mt10">
            <img src="/modelpics/<%=UAModelImg%>" width="70" alt="<%=UABrandName & " " & UAModelName%>" />
        </div>
		<div class="tb mt10 pt10">Shop customer favorites for your phone:</div>
		<div class="tb mt10 strandsRecs" tpl="CTGY-2" dfilter="modelname::<%=UAModelName%>" ></div>
        <%
		else
		%>
	    <div class="tb mtb15">We were unable to detect your phone device.</div>
        <%
		end if
	else	
	%>
    <div class="tb mtb15">We were unable to detect your phone device.</div>
    <%
	end if
	%>
    </div>
    <div class="clr mtb15">&nbsp;</div>
</div>
<script type="text/javascript" src="//bizsolutions.strands.com/sbsstatic/js/sbsLib-1.0.min.js"></script>
<script>
	window.onload = function() { 
		strands();
		
		$('html, body').animate({
			scrollTop: $("#main").offset().top
		}, 500);
	}
	
	function getRatingAvgStar(rating) {
		var nRating = rating;
		var strRatingImg = "<img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' />";

		if (!isNaN(nRating)) {
			if ((parseFloat(nRating) > 0)||(parseFloat(nRating) <=5)) {
				strRatingImg = "";
				for (nCnt=1; nCnt<=5; nCnt++) {
					if (parseFloat(nRating) >= parseFloat(nCnt)) {
						strRatingImg = strRatingImg + "<img src='/images/review/greenStarFull.gif' border='0' width='14' height='13' /> ";
					} else if (parseFloat(nRating) >= parseFloat((nCnt - 1) + 0.1)) {
						strRatingImg = strRatingImg + "<img src='/images/review/greenStarHalf.gif' border='0' width='8' height='13' /> ";
					} else {
						strRatingImg = strRatingImg + "<img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> ";
					}
				}
			}
		}

		return strRatingImg;
	}
	
	function strands_rendering(rec_info) {
		var rec = rec_info.recommendations;
		var content = "";
		var lap = 0;
		for(i=0;i<=rec.length-1;i++){
//			alert(JSON.stringify(rec[i]));			
			var price_retail = rec[i]["metadata"]["price"];
			var price_co = rec[i]["metadata"]["properties"]["saleprice"];
			var itemDesc = rec[i]["metadata"]["name"];
			if (itemDesc.length > 70) itemDesc = itemDesc.substring(0,66) + '...';
			var itempic = (rec[i]["metadata"]["picture"]).replace("/thumb/", "/big/");
			if (!isNaN(price_retail) && !isNaN(price_co)) {
				lap = i + 1;
				var savePercent = Math.ceil((price_retail - price_co) / price_retail * 100.0);
				content = content + "<div style='float:left; width:155px; padding:30px 5px 0px 0px;'>";
				content = content + "	<div style='float:left; width:100%; text-align:center; cursor:pointer;' onclick=\"location.href='" + rec[i]["metadata"]["link"] + "'\"><img src='" + itempic + "' border='0' width='100' height='100' /></div>";
				content = content + "	<div style='float:left; width:100%; padding-top:5px; height:55px; font-weight:bold; font-size:13px; color:#333; cursor:pointer;' title='" + rec[i]["metadata"]["name"] + "' onclick=\"location.href='" + rec[i]["metadata"]["link"] + "'\">" + itemDesc + "</div>";
				content = content + "	<div style='float:left; width:100%; padding-top:15px; color:#888; font-size:12px;'>Retail Price: <span style='text-decoration:line-through;'>$" + price_retail + "</span></div>";
				content = content + "	<div style='float:left; width:100%; padding-top:5px; color:#333; font-weight:bold; font-size:13px;'>";
				content = content + "		Wholesale Price: <span style='color:#ca0702'>$" + price_co + "</span>";
				content = content + "	</div>";
				content = content + "	<div style='float:left; width:100%; padding-top:10px;'>" + getRatingAvgStar(rec[i]["rating"]) + "</div>";
				content = content + "</div>";
				if (lap == 2) break;
			}
		}
		$(".strandsRecs").html(content);
	}
	
	function strands() {
		try{ 
			SBS.Recs.setRenderer(strands_rendering, "CTGY-2");
			SBS.Worker.go("Kx7AmRMCrW"); 
		} catch (e){}	
	}
</script>
<!--#include virtual="/template/bottom.asp"-->
