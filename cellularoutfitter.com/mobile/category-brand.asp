<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "category-brand"
	useUARedirect = true
	
	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim sortBy : sortBy = prepStr(request.Form("sortBy"))
	dim useTopModel : useTopModel = false
	if categoryID = 0 then response.Redirect("/")
	if brandID = 0 then response.Redirect("/")

	set fs = CreateObject("Scripting.FileSystemObject")
	
	sql = "exec sp_coCategoryDetails " & categoryID
	session("errorSQL") = sql
	set categoryDetailsRS = oConn.execute(sql)
	if categoryDetailsRS.EOF then response.Redirect("/") else categoryName = categoryDetailsRS("categoryName")
	categoryDetailsRS = null
	
	sql = "exec sp_brandDetailsByBrandID " & brandID
	session("errorSQL") = sql
	set brandDetailsRS = oConn.execute(sql)
	if brandDetailsRS.EOF then response.Redirect("/") else brandName = brandDetailsRS("brandName")
	brandDetailsRS = null
	brandName = replace(brandName," Ericsson","")
	brandName = replace(brandName,"/Qualcomm","")
	
	sql = "exec sp_modelsByCatAndBrand " & categoryID & ", " & brandID & ", '" & categoryName & "', 1,'" & sortBy & "', 1"
	if brandID = 17 then
		sql = "exec sp_modelsByCatAndBrand " & categoryID & ", " & brandID & ", '" & categoryName & "', 1,'" & sortBy & "', 1, 0"
	end if
'	response.write sql
	session("errorSQL") = sql
	set modelsRS = oConn.execute(sql)
	if not modelsRS.eof then
		if modelsRS("topmodel") then useTopModel = true
	end if
%>
<!--#include virtual="/template/topCO.asp"-->
<div id="main">
    <div class="curCategory">
        <div class="viewingTitle">CURRENTLY VIEWING:</div>
        <div class="curCat"><%=brandName & " " & catNameSwap(categoryName)%></div>
        <div class="catImg" style="background:url(/images/mobile/brands/<%=brandID%>.gif) 60px -25px no-repeat;"></div>
        <div class="changeCat"><a href="/c-<%=categoryID%>-<%=formatSEO(categoryName)%>.html" title="brand select" class="whiteLink">CHANGE BRAND</a></div>
    </div>
    
	<%if useTopModel then%>
    <div class="titleBar">
        <div class="centerContain">
        	<!--
            <div class="fl titleTxt">CHOOSE YOUR <%=ucase(brandName)%> DEVICE</div>
            <div class="fl titleArrow"></div>
            -->
            <div class="titleTxt2">
            <%if brandID = 17 then%>
            	SELECT AN <%=ucase(brandName)%> MODEL BELOW TO SHOP
            <%else%>
            	SELECT A <%=ucase(brandName)%> MODEL BELOW TO SHOP
            <%end if%>
			</div>
        </div>
    </div>
    <div class="sortBar">
	    <div class="centerContain">
    		<div class="titleTopModel">TOP <%=ucase(brandName)%> MODELS</div>
		</div>
    </div>
    <div class="categoryMenu">
        <div class="centerContain">
        	<%call modelLoop(modelsRS, true)%>
        </div>
    </div>
    <div class="clr"></div>
    <%end if%>
    
    <div class="titleBar">
        <div class="centerContain">
	        <div class="titleTxt2">
            <%if brandID = 17 then%>
            	SELECT AN <%=ucase(brandName)%> MODEL BELOW TO SHOP
            <%else%>
            	SELECT A <%=ucase(brandName)%> MODEL BELOW TO SHOP
            <%end if%>
            </div>
        </div>
    </div>
    <div class="sortBar">
	    <div class="centerContain">
    		<div class="titleTopModel">
            <%if useTopModel then%>
            	OTHER <%=ucase(brandName)%> MODELS
            <%else%>
            	<%=ucase(brandName)%> MODELS
            <%end if%>
			</div>
		</div>
    </div>
	<div class="sortBar2">
        <div class="sortBox2">
            <form name="sortForm" method="post">
                <select name="sortBy" class="sortObject" onchange="if (this.value != '') document.sortForm.submit()">
                    <option value="">SORT BY</option>
                    <option value="AZ" <%if sortBy = "AZ" then%>selected<%end if%>>A-Z</option>
                    <option value="ZA" <%if sortBy = "ZA" then%>selected<%end if%>>Z-A</option>
                    <option value="NO" <%if sortBy = "NO" then%>selected<%end if%>>New-Old</option>
                    <option value="ON" <%if sortBy = "ON" then%>selected<%end if%>>Old-New</option>
                </select>
            </form>
        </div>
    </div>
    <div class="horiDiv"></div>
    <div class="categoryMenu">
        <div class="centerContain">
        	<%call modelLoop(modelsRS, false)%>
        </div>
    </div>
    <div class="clr"></div>
    <div class="centerContain tb" style="padding:10px 0px 10px 0px; margin-top:10px;">
        <div style="width:90%; margin:auto; border-top:1px solid #ccc;"></div>
        <a href="javascript:location.href='/search/search.html'+BR.mobile.getTrendingUrl();"><div id="btnTrending"></div></a>
    </div>        
</div>
<%
sub modelLoop(byref pModelRS, topModel)
	useCarrierLogo = "1301,1259,1258,"
	dim modelCnt : modelCnt = 0
	
	if topModel then
		do while not pModelRS.EOF
			hidelive = pModelRS("hidelive")
			international = pModelRS("international")
			dbTopModel = pModelRS("topModel")
			if not hidelive and not international and dbTopModel = 1 then 
				modelCnt = modelCnt + 1			
				modelID = pModelRS("modelID")
				modelName = pModelRS("modelName")
				modelImg = pModelRS("modelImg")
				if len(modelName) > 18 then useClass = "menuTxt2" else useClass = "menuTxt"
			%>
		<div class="fl menuItem" onclick="goHere('/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=categoryID%>-<%=formatSEO(categoryName & " for " & brandName & "-" & modelName)%>.html')">
			<div class="menuPic" style="position:relative;">
				<%if instr(useCarrierLogo, modelID & ",") > 0 then%>
					<div style="position:absolute; bottom:5px; left:5px;"><img src="/images/brands/b_carrierLogo_<%=modelID%>.png" border="0" /></div>
				<%end if%>
	
				<%if fs.FileExists("/modelpics/thumbs/" & modelImg) then%>
				<img src="/modelpics/thumbs/<%=modelImg%>" border="0" width="70" height="112" />
				<%else%>
				<img src="/modelpics/<%=modelImg%>" border="0" width="70" height="112" />
				<%end if%>
			</div>
			<div class="<%=useClass%>"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=categoryID%>-<%=formatSEO(categoryName & " for " & brandName & "-" & modelName)%>.html" class="whiteLink" title=""><%=modelName%></a></div>
		</div>
		<%
			end if
			pModelRS.movenext
		loop
	else
		do while not pModelRS.EOF
			hidelive = pModelRS("hidelive")
			international = pModelRS("international")
			dbTopModel = pModelRS("topModel")
			if not hidelive and not international and dbTopModel <> 1 then 
				modelCnt = modelCnt + 1			
				modelID = pModelRS("modelID")
				modelName = pModelRS("modelName")
				modelImg = pModelRS("modelImg")
				if len(modelName) > 18 then useClass = "menuTxt2" else useClass = "menuTxt"
			%>
		<div id="singleDeviceBox_<%=modelCnt%>" class="fl menuItem"<% if modelCnt > 20 then %> style="display:none;"<% end if %> onclick="goHere('/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=categoryID%>-<%=formatSEO(categoryName & " for " & brandName & "-" & modelName)%>.html')">
			<div class="menuPic" style="position:relative;">
				<%if instr(useCarrierLogo, modelID & ",") > 0 then%>
					<div style="position:absolute; bottom:5px; left:5px;"><img src="/images/brands/b_carrierLogo_<%=modelID%>.png" border="0" /></div>
				<%end if%>
	
				<%if fs.FileExists("/modelpics/thumbs/" & modelImg) then%>
				<tempimg src="/modelpics/thumbs/<%=modelImg%>" border="0" width="70" height="112" />
				<%else%>
				<tempimg src="/modelpics/<%=modelImg%>" border="0" width="70" height="112" />
				<%end if%>
			</div>
			<div class="<%=useClass%>"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=categoryID%>-<%=formatSEO(categoryName & " for " & brandName & "-" & modelName)%>.html" class="whiteLink" title=""><%=modelName%></a></div>
		</div>
		<%
			end if
			pModelRS.movenext
		loop
	end if

	pModelRS.movefirst
end sub
%>
<script>
window.WEDATA.pageType = 'categoryBrand';
window.WEDATA.pageData = {
    brand: <%= jsStr(brandName) %>,
    brandId: <%= jsStr(brandId) %>,
    category: <%= jsStr(categoryName) %>,
    categoryId: <%= jsStr(categoryId) %>
};
</script>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript" src="/template/js/brand.js"></script>