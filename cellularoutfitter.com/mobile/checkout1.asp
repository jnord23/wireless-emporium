<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<%
	pageName = "Checkout"
	securePage = 1
	weDataStr = ""
	dim itemID : itemID = prepInt(request.Form("itemID"))
	dim qty : qty = prepInt(request.Form("qty"))
	dim mySession : mySession = prepStr(request.cookies("mySession"))
	dim cartTotal : cartTotal = 0
	dim totalQty : totalQty = 0
	
	if mySession = "" then
		mySession = session.SessionID
		response.cookies("mySession") = mySession
		response.cookies("mySession").expires = now+30
	end if
	
	sql = "exec sp_itemsInBasket 2, " & mySession
	session("errorSQL") = sql
	set cartItemsRS = oConn.execute(sql)
	
	do while not cartItemsRS.EOF
		qty = cartItemsRS("qty")
		price = cartItemsRS("price_CO")
		customCost = cartItemsRS("customCost")
        if not isnull(customCost) then
            itemPrice = customCost
            cartTotal = cartTotal + (customCost*qty)
            if prepInt(customCost) > 0 then totalQty = totalQty + qty
        else
            itemPrice = price
            cartTotal = cartTotal + (price*qty)
            totalQty = totalQty + qty
        end if
        weDataStr = weDataStr & "window.WEDATA.pageData.cartItems.push({itemId: "& jsStr(cartItemsRS("itemId")) & ", qty: " & jsStr(qty) &", price: "& jsStr(itemPrice) & "});"

		cartItemsRS.movenext
	loop
	
	shipcost0 = 4.00 + (1.99 * totalQty)
	shipcost2 = 8.00 + (1.99 * totalQty)
	shipcost3 = 21.00 + (1.99 * totalQty)
	
	nSubTotal = cartTotal 'this is for promoFunction.asp
%>
<!--#include virtual="/template/promoCalc.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/template/topCO.asp"-->
<%
	discountTotal = prepInt(discountTotal)
	discountedCartTotal = cartTotal - discountTotal
%>
<form method="post" action="/checkout2.html" name="checkout1Form">
<div class="checkoutSteps">
	<div class="centerContain">
		<div class="fl lockImg"></div>
    	<div class="fl step1">1. Your Info</div>
	    <div class="fl step2 inactiveCheckoutStep">2. Payment</div>
    	<div class="fl step2 inactiveCheckoutStep">3. Review</div>
    </div>
</div>
<div class="checkoutTitleBar" onclick="revealShippingOptions()">
	<div class="fl">Shipping Options</div>
    <div class="fr expandDiv">+</div>
</div>
<div id="shippingOptionDiv" class="shippingOptions hidden">
	<div class="centerContain">
        <div class="shippingOptionRow">
            <div class="fl shippingRadio">
                <input type="radio" name="shiptype" value="0" checked="checked" />
                <input type="hidden" name="shipcost0" value="<%=shipcost0%>" />
                <input type="hidden" name="shipcost0_show" value="<%=formatCurrency(shipcost0,2)%>" />
            </div>
            <div class="fl shippingTxt">USPS First Class (4-10 business days)</div>
        </div>
        <div class="shippingOptionRow">
            <div class="fl shippingRadio">
                <input type="radio" name="shiptype" value="2" />
                <input type="hidden" name="shipcost2" value="<%=shipcost2%>" />
                <input type="hidden" name="shipcost2_show" value="<%=formatCurrency(shipcost2,2)%>" />
            </div>
            <div class="fl shippingTxt">USPS Priority Mail (2-4 business days)</div>
        </div>
        <div class="shippingOptionRow">
            <div class="fl shippingRadio">
                <input type="radio" name="shiptype" value="3" />
                <input type="hidden" name="shipcost3" value="<%=shipcost3%>" />
                <input type="hidden" name="shipcost3_show" value="<%=formatCurrency(shipcost3,2)%>" />
            </div>
            <div class="fl shippingTxt">USPS Express Mail (1-2 business days)</div>
        </div>
        <div class="applyShipping" onclick="showShippingOnTotal()">Apply</div>
        <div class="mustApply">(MUST press Apply to update total)</div>
    </div>
</div>
<div class="checkoutTitleBar">Billing Address</div>
<div class="lightBg">
	<div class="centerContain">
    	<div class="fullEntryDetails">
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">First Name:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="fname" value="" /></div>
        </div>
        <div class="fullEntryDetails">
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">Last Name:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="lname" value="" /></div>
        </div>
        <div class="fullEntryDetails">
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">Address 1:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="bAddress1" value="" /></div>
        </div>
        <div class="fullEntryDetails">
            <div class="formTitle">
                <div class="fl requiredField"></div>
                <div class="fl">Address 2:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="bAddress2" value="" /></div>
        </div>
        <div class="fullEntryDetails">
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">Zip/Postal Code:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="bZip" value="" onchange="getCityState(this.value,'bill')" /></div>
        </div>
        <div class="fullEntryDetails">
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">City:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="bCity" value="" /></div>
        </div>
        <div class="fullEntryDetails">
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">State/Province:</div>
            </div>
            <div class="formField">
                <select class="invisibleSelect" name="bState" onchange="checkTax(this.value)">
                    <option value=""></option>
                    <%getStates(bState)%>
                </select>
            </div>
        </div>
        <div class="fullEntryDetails">
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">Phone:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="tel" name="phone" value="" /></div>
        </div>
        <div class="fullEntryDetails">
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">Email:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="email" value="" onchange="remarketing(this.value)" /></div>
        </div>
    </div>
</div>
<div class="checkoutTitleBar">Shipping Address</div>
<div class="lightBgBordered">
	<div class="centerContain">
        <div class="moreOptions">
            <div class="fl checkbox"><input type="checkbox" name="shippingAddr" value="0" onchange="shipAddrOptions(this.checked)" /></div>
            <div class="fl diffShip">Different Than Billing Address</div>
        </div>
        <div id="shippingFields" class="shippingAddress">
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">Address 1:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="sAddress1" value="" /></div>
            <div class="formTitle">
                <div class="fl requiredField"></div>
                <div class="fl">Address 2:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="sAddress2" value="" /></div>
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">Zip/Postal Code:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="sZip" value="" onchange="getCityState(this.value,'ship')" /></div>
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">City:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="sCity" value="" /></div>
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">State/Province:</div>
            </div>
            <div class="formField">
                <select class="invisibleSelect" name="sState">
                    <option value=""></option>
                    <%getStates(bState)%>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="cartItem">
    <div class="cartSummaryTitle">CART SUMMARY</div>
    <div class="summaryRow">
        <div class="fl rowTitle">Cart Total:</div>
        <div class="fr rowValue"><%=formatCurrency(cartTotal,2)%></div>
    </div>
    <div class="summaryRow">
        <div class="fl rowTitle">Tax:</div>
        <div id="taxDiv" class="fr rowValue"><%=formatCurrency(0,2)%></div>
    </div>
	<%
    if sPromoCode <> "" then
        session("promocode") = sPromoCode
    %>
    <div class="summaryRow">
        <div class="fl rowTitle">Discount:</div>
        <div class="fr rowValue discountPrice"><%=formatcurrency(0-discountTotal,2)%></div>
    </div>
    <%end if%>
    <div id="shipSummary" class="summaryRow hidden">
        <div class="fl rowTitle">Shipping:</div>
        <div id="shippingCostDiv" class="fr rowValue"><%=formatCurrency(shipcost0,2)%></div>
    </div>
    <div id="estSummary" class="summaryRowDark">
        <div class="fl rowTitle">Estimated Total:</div>
        <div id="estTotalDiv" class="fr rowValueGreen"><%=formatCurrency(discountedCartTotal,2)%></div>
    </div>
    <div id="grandSummary" class="summaryRowDark hidden">
        <div class="fl rowTitle">Grand Total:</div>
        <div id="grandTotalDiv" class="fr rowValueGreen"><%=formatCurrency(discountedCartTotal,2)%></div>
    </div>
</div>
<div class="proceedButton">
	<input type="image" name="Proceed to Checkout" src="/images/mobile/buttons/proceedToCheckout.gif" border="0" onclick="return(checkout1FormReview())" />
    <input type="hidden" name="subtotal" value="<%=cartTotal%>" />
    <input type="hidden" name="itemTotal" value="<%=cartTotal%>" />
    <input type="hidden" name="discountedSubTotal" value="<%=discountedCartTotal%>" />
    <input type="hidden" name="shippingTotal" value="<%=shipcost0%>" />
    <input type="hidden" name="taxAmt" value="0" />
    <input type="hidden" name="grandTotal" value="<%=discountedCartTotal+shipcost0%>" />
    <input type="hidden" name="showShipping" value="0" />
    <input type="hidden" name="totalQty" value="<%=totalQty%>" />
</div>
</form>
<script language="javascript">
	var taxValue = <%=Application("taxMath")%>;
	var taxAmt = 0;
	var mySession = <%=prepInt(mySession)%>;
</script>
<script>
window.WEDATA.pageType = 'checkout1';
window.WEDATA.pageData = {cartItems: []}
<%= weDataStr %>
</script>
<!--#include virtual="/template/bottom.asp"-->