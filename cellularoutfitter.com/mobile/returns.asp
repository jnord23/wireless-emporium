<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "Returns"
%>
<!--#include virtual="/template/topCO.asp"-->
<div class="returnPage">
	<div class="returnText">
        <strong>SHIPPING & HANDLING:</strong><br />
        CellularOutfitter.com offers shipping and handling via USPS First Class Mail (3-10 Business Day Delivery) starting at $5.95 for the first item and a $1.99 shipping and handling fee on each additional item. For an additional Shipping & Handling fee, customers may upgrade to USPS Priority Mail (2-4 Business Day Delivery) or USPS Express Mail (1-2 Business Day Delivery). All orders shipped via USPS Express Mail will require signature upon delivery. Presently, CellularOutfitter does not ship outside of the US and Canada. Additional charges may apply for large-quantity distribution orders and orders that require special delivery instructions. If such an order is placed, the customer will be notified of said charges by a CellularOutfitter.com customer service specialist. This policy pertains to all orders placed within the continental United States of America.
        <br /><br />
        <strong>Return/Warranty Policy:</strong><br />
        If you are not happy with the any product, you may return the item for an exchange or refund of the purchase price (minus shipping) within ninety (90) days of receipt. We also offer a ONE YEAR MANUFACTURER WARRANTY on all items. If your item is defective, for up to a full year, simply return the item to CellularOutfitter.com for a prompt exchange.
        <br /><br />
        All returns must be accompanied by a Cellular Outfitter RMA number. To obtain an RMA number, please send an e-mail to: returns@CellularOutfitter.com
        <br /><br />
        The information below must be included for any return:<br />
        - Order Number	- Date of original purchase<br />
        - Reason for request for RMA	- Your e-mail address
        <br /><br />
        No returns, refunds or exchanges will be processed without an RMA number.
        <br /><br />
        <strong>CELL PHONE RETURN POLICY:</strong><br />
        All return requests must be made within 7 days of receiving the item. All returned merchandise must be unregistered, in complete original manufacturer's packaging, same condition as sold, with all literature, accessories, instructions, blank warranty cards and documentation. All Phones must be returned in "Like-New Condition", show no signs of use and must have less than 25 minutes in total cumulative talk time.
        <br /><br />
        If an item is defective it may only be exchanged with an equivalent product if we are unable to supply the same item or if the item cannot be repaired. Any return shipping cost is the responsibility of the customer. All shipping and handling fees are non-refundable. This includes all refused and unaccepted packages.
        <br /><br />
        All NON-DEFECTIVE cell phone returns will be subject to a 20% restocking fee.
        <br /><br />
        For refunds allow 3-4 business days for the refund to post to your account from the time the refund is issued.
        <br /><br />
        <strong>Non-Defective Returns:</strong><br />
        NON-DEFECTIVE RETURNS OF ACCESSORIES ARE SUBJECT TO A 15% RE-STOCKING FEE. NON-DEFECTIVE RETURNS OF PHONES ARE SUBJECT TO A 20% RE-STOCKING FEE. Such returns will be for store credit or refund at the customer's request. Refunds are only available for order within 30 days of original order date for accessories and 7 days for phones. Refunds will be issued for the current published price of the product(s) minus a 15% or 20% re-stocking fee.<br />
        NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS:<br />
        NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS MAY BE SUBJECT TO A 15% RE-STOCKING FEE and such returns will be for store credit or refund (at the customer's request). Refunds are only available within 90 days of original order date. Please contact us at returns@cellularoutfitter.com if you have questions about which products are returnable and which products may be subject to a restocking fee.
        <br /><br />
        Customers must contact the headset manufacturer for all defective/warranty issues on headsets that have had the packaging opened. For your convenience, you can contact us at returns@CellularOutfitter.com for the manufacturers' contact information.
        <br /><br />
        <strong>ARRIVAL TIMES:</strong><br />
        This formula will help to determine when your order should arrive: Shipping Time + Shipping Method = Total Delivery Time. In-stock items are shipped out of our warehouse in Southern California within 1-2 business days unless otherwise specified. Orders shipped via USPS First Class Mail will arrive anywhere from 3-8 business days after the order has shipped from our warehouse(s) and depending on SHIP TO location within the continental United States. Estimated time of arrival for orders placed with USPS Priority Mail are anywhere from 2-4 business days after the order has shipped from our warehouse(s) and depending on SHIP TO location within the continental Unites States, or unless specified otherwise. Estimated time of arrival for orders placed with USPS Express Mail is approximately 1-2 business days after the order has shipped from our warehouse(s) and depending on SHIP TO location within the continental Unites States, or unless specified otherwise. Please note our warehouse does not ship on weekends and we do not offer Saturday, Sunday or holiday delivery.
        <br /><br />
        <strong>PLEASE BE ADVISED:</strong><br />
        Regardless of shipping option, CellularOutfitter.com does not warrant or guarantee published delivery times. CellularOutfitter.com does guarantee timely processing and fulfillment of all IN-STOCK items upon placement of order (usually within 1-2 business days). Upon fulfillment out of our warehouse(s), it is the obligation of the United States Postal Service for timely delivery of shipment in which the above suggested delivery times apply. It is the responsibility of the customer to acknowledge and adhere to the above policy before placing their order. The SHIP TO address MUST be a valid deliverable address for the United States Postal Service (must be able to receive first class mail). Please provide as complete information as possible to ensure delivery accuracy.
        <br /><br />
        <strong>SHIPMENT CONFIRMATION/TRACKING:</strong><br />
        Upon shipment of your order you will receive notification that your order has been sent in accordance with your selected shipping option. We will also include delivery and tracking confirmation in this email which accesses the US Postal Services database. CellularOutfitter.com does not expressly warrant the reliability of any and all information provided by the USPS. Your credit card is charged when your shipment has been sent. Please be advised, if your order requires additional information for processing verification, you may receive one or more of the following: (i) an e-mail requesting additional information; (ii) a phone call from a CellularOutfitter.com processing representative; (iii) or a call from your bank to verify your purchase. 
    </div>
</div>
<!--#include virtual="/template/bottom.asp"-->