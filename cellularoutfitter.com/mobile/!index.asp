<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "Home"
	
	dim reset : reset = prepInt(request.QueryString("reset"))
	if reset = 1 then
		response.Cookies("mobileBrandID") = 0
		response.Cookies("mobileModelID") = 0
	end if
%>
<!--#include virtual="/template/top.asp"-->
<%	
	sql = "select brandID, brandName from we_brands where phoneSale = 1 and other = 0 order by brandName"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
%>
<div class="homePageBanner"><img src="/images/mobile/banners/homePageBanner1.jpg" border="0" /></div>
<div class="selectByBrand">
    <div class="CallToAction">SELECT A DEVICE BRAND BELOW:</div>
    <div class="ctaDownArrow"></div>
    <div class="allBrandImgs">
    	<%
		do while not brandRS.EOF
			brandID = brandRS("brandID")
			brandName = brandRS("brandName")
			brandName = replace(brandName,"/Palm","")
			brandName = replace(brandName,"/Qualcomm","")
			brandName = replace(brandName," Ericsson","")
		%>
        <div class="fl brandButton" onclick="window.location='/<%=lcase(brandName)%>-cell-phone-accessories.htm'">
        	<div class="brandImg"><img src="/images/mobile/homeBrands/<%=lcase(brandName)%>.png" /></div>
            <div class="brandText"><a class="brandLink" href="/<%=lcase(brandName)%>-cell-phone-accessories.htm"><%=brandName%></a></div>
        </div>
        <%
			brandRS.movenext
		loop
		brandRS.movefirst
		%>
        <div class="fl brandButton" onclick="window.location='/other-cell-phone-accessories.htm'">
        	<div class="brandImg"><img src="/images/mobile/homeBrands/others.png" /></div>
            <div class="brandText"><a class="brandLink" href="/other-cell-phone-accessories.htm">Others</a></div>
        </div>
    </div>
</div>
<div class="selectByPulldown">
    <div class="CallToAction">FIND ACCESSORIES FOR YOUR DEVICE:</div>
    <div class="ctaDownArrow"></div>
    <form id="basicForm" name="prodSelectForm" onsubmit="return(chkForm())" method="post" action="/bmc.htm">
    <div id="homeSelectBox">
        <div id="selectBoxContainer">
            <select id="actualSelectBox" name="brandID" onchange="newBrandSelected()">
                <option value="">Choose A Device Brand</option>
                <%
                dim brandList : brandList = ""
                do while not brandRS.EOF
                %>
                <option value="<%=brandRS("brandID")%>"><%=brandRS("brandName")%></option>
                <%
                    brandList = brandList & brandRS("brandID") & "@@" & formatSEO(brandRS("brandName")) & "##"
                    brandRS.movenext
                loop
                %>
            </select>
            <%
            brandList = left(brandList,len(brandList)-2)
            brandArray = split(brandList,"##")
            for i = 0 to ubound(brandArray)
                detailArray = split(brandArray(i),"@@")
            %>
            <input type="hidden" name="brand_<%=detailArray(0)%>" value="<%=detailArray(1)%>" />
            <%
            next
            %>
        </div>
    </div>
    <div id="homeSelectBox">
        <div id="selectBoxContainer2">
            <select id="actualSelectBox2" name="modelID" disabled="disabled">
                <option value="">Choose A Device Model</option>
            </select>
        </div>
    </div>
    <div id="homeSelectBox">
        <div id="selectBoxContainer3">
            <select id="actualSelectBox2" name="typeID" disabled="disabled">
                <option value="">Choose A Product Type</option>
            </select>
        </div>
    </div>
    </form>
</div>
<div class="CallToAction">WHY SHOP WITH US?</div>
<div class="ctaDownArrow"></div>
<div class="whyShopItem">
	<div class="fl whyShopCheckmark"><img src="/images/mobile/icons/whyShopCheckmark.gif" /></div>
    <div class="fl whyShopText">Over 1,000,000 Orders Shipped!</div>
</div>
<div class="whyShopItem">
	<div class="fl whyShopCheckmark"><img src="/images/mobile/icons/whyShopCheckmark.gif" /></div>
    <div class="fl whyShopText">All Orders Ship Within 24 Hours</div>
</div>
<div class="whyShopItem">
	<div class="fl whyShopCheckmark"><img src="/images/mobile/icons/whyShopCheckmark.gif" /></div>
    <div class="fl whyShopText">90-Day EZ No-Hassle Return Policy</div>
</div>
<div class="whyShopItem">
	<div class="fl whyShopCheckmark"><img src="/images/mobile/icons/whyShopCheckmark.gif" /></div>
    <div class="fl whyShopText">Safe &amp; Secure SSL Encrypted Checkout</div>
</div>
<div class="safeShopping"><img src="/images/mobile/icons/safeShopping.jpg" /></div>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	var mobileBrandID = <%=prepInt(request.Cookies("mobileBrandID"))%>
	var mobileModelID = <%=prepInt(request.Cookies("mobileModelID"))%>
</script>
<script language="javascript" src="/template/js/index.js"></script>