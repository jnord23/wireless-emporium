<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "Brand-Model"
	
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim sortBy : sortBy = prepStr(request.Form("sortBy"))
	if modelID = 0 then response.Redirect("/")
	
	sql = "exec sp_brandModelNamesByModelID " & modelID
	session("errorSQL") = sql
	set brandModelDetailsRS = oConn.execute(sql)
	if brandModelDetailsRS.EOF then
		response.Redirect("/")
	else
		brandName = brandModelDetailsRS("brandName")
		modelName = brandModelDetailsRS("modelName")
		modelImg = brandModelDetailsRS("modelImg")
		brandID = brandModelDetailsRS("brandID")
		brandName = replace(brandName," Ericsson","")
		brandName = replace(brandName,"/Qualcomm","")
	end if
	brandModelDetailsRS = null
	
'	sql = "exec sp_getCatsByModel " & modelID & ",2"
	sql = "exec sp_categoriesByModel 2, " & modelID
	session("errorSQL") = sql
	set categoryListRS = oConn.execute(sql)
%>
<!--#include virtual="/template/topCO.asp"-->
<div class="curCategory">
	<div class="viewingTitle">CURRENTLY VIEWING:</div>
    <div class="curCat"><%=brandName & " " & modelName%></div>
    <div class="catImg" style="background:url(/modelpics/thumbs/<%=modelImg%>) 0px 0px no-repeat;"></div>
    <div class="changeCat"><a href="/b-<%=brandID%>-<%=formatSEO(brandName)%>-cell-phone-accessories.html" title="Select a new <%=brandName%> device" class="whiteLink">CHANGE DEVICE</a></div>
</div>
<div class="titleBar">
	<div class="centerContain">
		<div class="fl titleTxt">CHOOSE A <%=ucase(modelName)%> CATEGORY</div>
    	<div class="fl titleArrow"></div>
    </div>
</div>
<div class="categoryMenu">
	<div class="centerContain">
		<%
        do while not categoryListRS.EOF
            categoryID = categoryListRS("typeID")
			if categoryID <> 19 then
				categoryName = categoryListRS("typeName")
				categoryTitle = catNameSwap(categoryName)
				if categoryTitle = "Unlocked Cell Phones" then categoryTitle = "Unlocked<br>Cell Phones"
				if len(categoryTitle) > 19 then useClass = "menuTxt2" else useClass = "menuTxt"
        %>
        <div class="fl menuItem" onclick="window.location='/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=categoryID%>-<%=formatSEO(brandName & " " & modelName & " " &categoryName)%>.html'">
            <div class="menuPic"><img src="/images/mobile/categories/<%=categoryID%>.jpg" border="0" /></div>
            <div class="<%=useClass%>"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=categoryID%>-<%=formatSEO(brandName & " " & modelName & " " &categoryName)%>.html" class="whiteLink" title=""><%=categoryTitle%></a></div>
        </div>
        <%
			end if
            categoryListRS.movenext
        loop
        %>
    </div>
</div>
<script>
window.WEDATA.pageType = 'brandModel';
window.WEDATA.pageData = {
	brand: '<%= brandName %>',
	brandId: '<%= brandId %>',
	model: '<%= modelName %>',
	modelId: '<%= modelId %>'
}

</script>
<!--#include virtual="/template/bottom.asp"-->