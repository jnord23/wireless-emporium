<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "brands"
	
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	if brandID = 0 then response.Redirect("/")
	
	sql = "exec sp_brandDetailsByBrandID " & brandID
	session("errorSQL") = sql
	set brandDetailsRS = oConn.execute(sql)
	
	if brandDetailsRS.EOF then
		response.Redirect("/")
	else
		brandName = brandDetailsRS("brandName")
	end if
	brandDetailsRS = null
	
	sql = "exec sp_modelsByBrandTemp " & brandID & ",'" & sortBy & "','',null"
	session("errorSQL") = sql
	set modelsRS = oConn.execute(sql)
%>
<!--#include virtual="/template/topCO.asp"-->
<div class="curCategory">
	<div class="viewingTitle">CURRENTLY VIEWING:</div>
    <div class="catImg" style="background:url(/images/mobile/brands/<%=brandID%>.gif) no-repeat 0px -20px;"></div>
    <div class="changeCat" onclick="goHome()"><a href="/" title="m.CellularOutfitter.com" class="whiteLink">CHANGE BRAND</a></div>
</div>
<div class="titleBar">
	<div class="centerContain">
		<div class="fl titleTxt">CHOOSE YOUR <%=ucase(brandName)%> DEVICE</div>
    	<div class="fl titleArrow"></div>
    </div>
</div>
<div class="categoryMenu">
	<div class="centerContain">
		<%
        do while not modelsRS.EOF
            modelCnt = modelCnt + 1
            modelID = modelsRS("modelID")
            modelName = modelsRS("modelName")
            modelImg = modelsRS("modelImg")
            mobileReady = modelsRS("mobileReady")
            if isnull(mobileReady) then mobileReady = false
            if len(modelName) > 18 then useClass = "menuTxt2" else useClass = "menuTxt"
        %>
        <div id="singleDeviceBox_<%=modelCnt%>" class="fl menuItem"<% if modelCnt > 20 then %> style="display:none;"<% end if %> onclick="goHere('/m-<%=modelID%>-<%=formatSEO(brandID & " " & modelName)%>-cell-phone-accessories.html')">
            <div class="menuPic"><tempimg src="/modelpics/thumbs/<%=modelImg%>" border="0" /></div>
            <div class="<%=useClass%>"><a href="/m-<%=modelID%>-<%=formatSEO(brandID & " " & modelName)%>-cell-phone-accessories.html" class="whiteLink" title="View <%=modelName%> Accessories"><%=modelName%></a></div>
        </div>
        <%
            modelsRS.movenext
        loop
        %>
    </div>
</div>
<script>
    window.WEDATA.pageType = 'brand';
    window.WEDATA.pageData = {
        brand: '<%= brandName %>',
        brandId: '<%= brandId %>'
    }
</script>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript" src="/template/js/brand.js"></script>