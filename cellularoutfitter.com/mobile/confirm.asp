<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	dim pageName : pageName = "Confirm"
	dim useSessionID : useSessionID = prepInt(request.Cookies("mySession"))
	
	dim orderID : orderID = prepInt(request.QueryString("o"))
	dim accountID : accountID = prepInt(request.QueryString("a"))
	dim orderTotal : orderTotal = prepInt(request.QueryString("d"))
	dim usePostPurchase : usePostPurchase = request.querystring("pp")
	
	dim accountMasterId : accountMasterId = accountId
	' look for the parentId of the current account
	sql = "select parentId from we_accounts " &_
        "where email = '" & email & "' " &_
        "and accountId = " & accountid & " " &_
        "order by dateEntered " 'dateEntered just in case there are multiple records returned... which should NEVER happen
        'TODO: Check for more than 1 record returned and handle with an email to developers with debug information
        set rs = oConn.execute(sql)
		if (not rs.eof or rs.RecordCount > 0) and not isNull(rs("parentId")) then accountMasterId = rs("parentId")

	sql =	"select isnull(b.postPurchase,0) postPurchase, c.fname, c.lname, c.email, c.phone, c.bAddress1, c.bAddress2, c.bCity, c.bState, c.bZip, " &_
			"c.sAddress1, c.sAddress2, c.sCity, c.sState, c.sZip, d.itemDesc_CO, b.itemid, b.quantity, b.price, " &_
			"a.ordersubtotal, a.ordershippingfee, a.orderTax, a.ordergrandtotal, a.extOrderType, ot.orderDesc extOrderTypeDesc, isnull(e.promoCode, '') promoCode,isnull(e.promoPercent, 0) promoPercent, " &_
			"CASE " &_
			"	WHEN (b.cogs IS NULL OR b.cogs = 0) THEN d.cogs " &_
			" ELSE b.cogs " &_
			" END cogs " &_
			"from we_Orders a " &_
				"left join xOrderType ot on o.extOrderType = ot.typeId " &_
				"left join we_orderDetails b on a.orderID = b.orderID " &_
				"left join co_accounts c on a.accountID = c.accountID " &_
				"left join we_Items d on b.itemID = d.itemID " &_
				"left join co_coupons e on a.couponid = e.couponid " &_				
			"where a.orderID = " & orderID
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	dim fname : fname = prepStr(rs("fname"))
	dim lname : lname = prepStr(rs("lname"))
	dim email : email = prepStr(rs("email"))
	dim phone : phone = prepStr(rs("phone"))
	dim bAddress1 : bAddress1 = prepStr(rs("bAddress1"))
	dim bAddress2 : bAddress2 = prepStr(rs("bAddress2"))
	dim bCity : bCity = prepStr(rs("bCity"))
	dim bState : bState = prepStr(rs("bState"))
	dim bZip : bZip = prepStr(rs("bZip"))
	dim sAddress1 : sAddress1 = prepStr(rs("sAddress1"))
	dim sAddress2 : sAddress2 = prepStr(rs("sAddress2"))
	dim sCity : sCity = prepStr(rs("sCity"))
	dim sState : sState = prepStr(rs("sState"))
	dim sZip : sZip = prepStr(rs("sZip"))
	dim subTotal : subTotal = prepInt(rs("ordersubtotal"))
	dim shippingAmt : shippingAmt = prepInt(rs("ordershippingfee"))
	dim tax : tax = prepInt(rs("orderTax"))
	dim grandTotal : grandTotal = prepInt(rs("ordergrandtotal"))
	dim extOrderType : extOrderType = rs("extOrderType")
	dim extOrderTypeDesc : extOrderTypeDesc = rs("extOrderTypeDesc")
	dim promoCode : promoCode = rs("promoCode")
	dim promoPercent : promoPercent = rs("promoPercent")
	if extOrderTypeDesc = "" or extOrderTypeDesc = 0 or isNull(extOrderTypeDesc) then
	  extOrderTypeDesc = "CC"
	end if


	if bAddress2 <> "" then bAddress1 = bAddress1 & ", " & bAddress2
	if sAddress2 <> "" then sAddress1 = sAddress1 & ", " & sAddress2
%>
<!--#include virtual="/template/topCO.asp"-->
<%
	Response.Cookies("user")("id") = accountMasterId
	Response.Cookies("user")("email") = email
	Response.Cookies("user").Expires = DateAdd("yyyy",5,Now())

totalDiscount = 0

%>
<script>
window.WEDATA.pageType = 'orderConfirmation';
window.WEDATA.account.email = <%= jsStr(email) %>;
window.WEDATA.account.id = <%= jsStr(accountMasterId) %>;
window.WEDATA.orderData = {
    cartItems: [],
    orderNumber: <%= jsStr(orderId) %>,
    orderType: <%= jsStr(extOrderTypeDesc) %>,
    accountId: <%= jsStr(accountid) %>,
    taxAmount: <%= jsStr(tax) %>,
    grandTotal: <%= jsStr(grandtotal) %>,
    subTotal: <%= jsStr(subtotal) %>,
    shippingAmount: <%= jsStr(shippingAmt) %>,
    promoCode: <%= jsStr(promoCode) %>,
    shipping: {
        state: <%= jsStr(sState) %>,
        city: <%= jsStr(sCity) %>,
        zip: <%= jsStr(sZip) %>
    }
};
</script>
<link href="/template/css/basket.css" rel="stylesheet" type="text/css">
<div class="tb secureRow">
    <div class="fl secureText">Customer Information</div>
</div>
<div class="orderReviewContainer">
    <div class="orderReviewBox">
    	<div class="orderReviewRowMsg">
        	<div class="thankYouText">Thank you for your purchase at</div>
            <div class="thankYouTextBlue">CellularOutfitter.com</div>
            <div class="thankYouText">Here are the details of your order:</div>
        </div>
    	<div class="custInfoRow">
        	<div class="custInfoTitle">Name:</div>
            <div class="custInfoValue"><%=fname & " " & lname%></div>
        </div>
        <div class="custInfoRow">
        	<div class="custInfoTitle">Email:</div>
            <div class="custInfoValue"><%=email%></div>
        </div>
        <div class="custInfoRow">
        	<div class="custInfoTitle">Phone:</div>
            <div class="custInfoValue"><%=phone%></div>
        </div>
        <div class="custInfoRow">
        	<div class="custInfoTitle">Billing Address:</div>
            <div class="custInfoValue">
				<%=bAddress1%><br />
                <%=bCity & ", " & bState & " " & bZip%>
            </div>
        </div>
        <div class="custInfoRow">
        	<div class="custInfoTitle">Shipping Address:</div>
            <div class="custInfoValue">
				<%=sAddress1%><br />
                <%=sCity & ", " & sState & " " & sZip%>
            </div>
        </div>
    </div>
</div>
<div class="alertContainer">
    <div class="alertMsg">
    	<div class="alertValue">Please review your shipping address.</div>
        <div class="alertValue">If it is incorrect, contat us immediately at</div>
        <div class="alertValue"><a class="alertEmail" href="mailto:updates@cellularoutfitter.com">updates@cellularoutfitter.com</a></div>
    </div>
</div>
<div class="secureRow">
    <div class="secureText">Order Details</div>
</div>
<div class="orderReviewContainer">
    <div class="orderReviewBox">
        <div class="orderReviewRowMsg">
            <div class="custInfoTitle">OrderID:</div>
            <div class="custInfoValue"><%=orderID%></div>
            <div class="custInfoTitle" style="margin-top:10px;">Your Items:</div>
        </div>
		<%
		itemids = ""
        prodLap = 1
        do while not rs.EOF
            itemDesc = prepStr(rs("itemDesc_CO"))
            qty = prepInt(rs("quantity"))
            itemPrice = prepInt(rs("price"))
            itemCogs = prepInt(rs("cogs"))
            itemPostPurchase = rs("postPurchase")
            itemDiscount = (qty * itemPrice) * promoPercent
            totalDiscount = totalDiscount + itemDiscount

        %>
        <div class="orderReviewRowProd">
            <div class="itemDesc"><%=itemDesc%></div>
            <div class="qtyDetails">
                <div class="qtyText">Qty.</div>
                <div class="qtyValue"><%=qty%></div>
            </div>
            <% if qty = 1 then %>
            <div class="itemPrice">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <% else %>
            <div class="itemPriceSmall">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <div class="itemPrice">Item Total: <%=formatCurrency((itemPrice * qty),2)%></div>
            <% end if %>
        </div>
        <script>
            window.WEDATA.orderData.cartItems.push({
                itemId: <%= jsStr(rs("itemid")) %>,
                qty: <%= jsStr(qty) %>,
                price: <%= jsStr(itemPrice) %>,
                cogs: <%= jsStr(itemCogs) %>,
                discount: <%= jsStr(itemDiscount) %>,
                isPostPurchase: <%= jsStr(lcase(itemPostPurchase)) %>
            });
        </script>


        <%
			if itemids = "" then
				itemids = rs("itemid")
			else
				itemids = itemids & "," & rs("itemid")			
			end if
			
            rs.movenext
            prodLap = prodLap + 1
        loop
        %>
        <script> window.WEDATA.orderData.totalDiscount = <%= jsStr(totalDiscount) %>; </script>

    </div>
	<div class="orderReviewBox">
    	<div class="orderReviewRow">
        	<div class="fl orderReviewText">Subtotal:</div>
            <div class="fr orderReviewValue"><%=formatCurrency(subtotal,2)%></div>
        </div>
        <%if promoCode <> "" then%>
        <div class="orderReviewRow">
        	<div class="fl orderReviewText">Promo Code:</div>
            <div class="fr orderReviewValue"><%=promoCode%></div>
        </div>
        <%end if%>
        <%if (grandTotal-(subTotal+shippingAmt+tax)) < 0 then%>
        <div class="orderReviewRow">
        	<div class="fl orderReviewText">Discount:</div>
            <div class="fr orderReviewValue"><%=formatcurrency(grandTotal-(subTotal+shippingAmt+tax),2)%></div>
        </div>
        <%end if%>

        <div class="orderReviewRow">
        	<div class="fl orderReviewText">Tax:</div>
            <div class="fr orderReviewValue">
				<%=formatCurrency(tax,2)%>
            </div>
        </div>
        <div class="orderReviewRow">
        	<div class="fl orderReviewText">Shipping:</div>
            <div class="fr orderReviewValue">
				<%=shippingAmt%>
            </div>
        </div>
        <div class="orderReviewRow2">
        	<div class="fl orderReviewTextWhite">Grand Total:</div>
            <div class="fr orderReviewValueWhite">
				<%=formatCurrency(grandTotal,2)%>
            </div>
        </div>
    </div>
</div>
<div class="thanksText">
	We appreciate your business! You'll receive an Order Shipment Confirmation email upon shipment out of our warehouse.
    If you have any questions regarding your order, please email our customer service department at:
</div>
<div class="serviceEmailLink"><a href="mailto:service@cellularoutfitter.com" class="serviceLink">service@cellularoutfitter.com</a></div>
<script>
	<%if usePostPurchase = "Y" and isnull(extOrderType) then%>
	window.onload = function() { 
		pp_doPopup();
	}

	function doSubmit() {
		frm = document.frmPostPurchase;
		if (CurrencyFormatted(frm.addlGrandTotal.value) == "0.00") {
			alert("Plesae select the items");
			return false;
		}
		document.getElementById('pp_bottomBox').style.display = 'none';
		document.getElementById('pp_bottomBoxText').style.display = '';
		return true;
	}
	
	function pp_closePopup() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
		document.getElementById("popBox").innerHTML = "";
		document.getElementById("popBox").style.position = "fixed";
	}
	
	function pp_doPopup() {
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
		document.getElementById("popBox").style.position = "absolute";
		ajax('/ajax/postPurchase.asp?itemids=<%=itemids%>&orderid=<%=orderID%>','popBox');
	}
	
	function updatePPTotal() {
		frm = document.frmPostPurchase;
		nCount = frm.hidCount.value;
		taxTotal = frm.taxTotal.value;
		
		var addlTotal = parseFloat(0);
		var addlTax = parseFloat(0);
		var addlShipping = parseFloat(0);
		var itemTotal = parseFloat(0);
		var retailTotal = parseFloat(0);
		var itemSave = parseFloat(0);
		var qtyTotal = parseFloat(0);

		for (i=1; i<= nCount; i++) {
			itemChecked = document.getElementById('id_chkItem'+i).checked;
			price_co = document.getElementById('id_itemPrice'+i).value;
			price_retail = document.getElementById('id_itemRetail'+i).value;
			qty = document.getElementById('id_qty'+i).value;			
			
			if (itemChecked) {
				if ((!isNaN(qty))&&(qty != '')&&(qty != 0)) {		
					itemTotal = parseFloat(itemTotal) + parseFloat(price_co * qty);
					retailTotal = parseFloat(retailTotal) + parseFloat(price_retail * qty);
					itemSave = parseFloat(itemSave) + (parseFloat(retailTotal) - parseFloat(itemTotal));
					qtyTotal = parseInt(qtyTotal) + parseInt(qty);
				}
			}
		}
		
		if (parseFloat(taxTotal) > 0) {
			addlTax = parseFloat(itemTotal * <%=Application("taxMath")%>);
		}
		
		addlShipping = parseFloat(qtyTotal * 1.99);
		addlTotal = parseFloat(itemTotal) + parseFloat(addlShipping) + parseFloat(addlTax);

		document.getElementById('addlSave').innerHTML = CurrencyFormatted(itemSave);
		document.getElementById('addlTotal').innerHTML = CurrencyFormatted(itemTotal);
		document.getElementById('addlTaxTotal').innerHTML = CurrencyFormatted(addlTax);
		document.getElementById('addlGrandTotal').innerHTML = CurrencyFormatted(addlTotal);
		
		frm.addlGrandTotal.value = CurrencyFormatted(addlTotal);
	}
	
	function CurrencyFormatted(amount) {
		var i = parseFloat(amount);
		if(isNaN(i)) { i = 0.00; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		i = Math.round(i * 100) / 100;
		s = new String(i);
		if(s.indexOf('.') < 0) { s += '.00'; }
		if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
		s = minus + s;
		return s;
	}		
	<%end if%>
</script>
<!--#include virtual="/template/bottom.asp"-->