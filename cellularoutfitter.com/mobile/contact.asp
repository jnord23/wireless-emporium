<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
	pageName = "Contact Us"
	
	dim emailSent : emailSent = 0
	dim customerName : customerName = request.form("name")
	dim customerEmail : customerEmail = request.form("email")
	dim inquirySubject : inquirySubject = request.form("subject")
	dim customerPhone : customerPhone = request.form("phonenumber")
	dim customerOrderNumber : customerOrderNumber = request.form("orderno")
	dim customerQuestion : customerQuestion = request.form("question")
	
	if customerName <> "" then
		dim cdo_to, cdo_from, cdo_subject, cdo_body
		cdo_to = "support@CellularOutfitter.com"
		'cdo_to = "jon@wirelessemporium.com"
		cdo_from = customerEmail
		
		cdo_subject = "Contact Us"
		cdo_body = "<p>Name: " & customerName & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Phone: " & customerPhone & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Type of Inquiry: " & inquirySubject & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Order #: " & customerOrderNumber & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Question: " & replace(customerQuestion,vbcrlf,"<br>") & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Date/Time Sent: " & now & "</p>" & vbcrlf
		cdo_body = cdo_body & "<table width='500' border='0' cellspacing='3' cellpadding='3' align='center'>" & vbcrlf
		cdo_body = cdo_body & "<tr><td height='50' colspan='2'>&nbsp;</td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td width='500'><table width='500' border='0' cellspacing='0' cellpadding='0'>" & vbcrlf
		cdo_body = cdo_body & "<tr><td width='207' height='50' align='center'><img src='/images/CellularOutfitter.jpg'></td><td width='293'>&nbsp;</td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2'>&nbsp;</td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td><table width='400' cellspacing='0' cellpadding='0' align='center'>" & vbcrlf
		cdo_body = cdo_body & "<tr><td width='100' align='left'>Name </td><td width='10'> : </td><td> " & customerName & "</td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td width='100' align='left'>E-Mail : </td><td width='10'> : </td><td> " & customerEmail & "</td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td width='100' align='left'>Type of Inquiry  </td><td width='10'> : </td><td> " & inquirySubject & "</td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td width='100' align='left'>Phone : </td><td width='10'> : </td><td> " & customerPhone & "</td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td width='100' align='left'>Order #  </td><td width='10'> : </td><td> " & customerOrderNumber & "</td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td width='100' align='left'>Question : </td><td width='10'> : </td><td> " & customerQuestion & "</td>" & vbcrlf
		cdo_body = cdo_body & "</tr></table></td>" & vbcrlf
		cdo_body = cdo_body & "</tr></table></td>" & vbcrlf
		cdo_body = cdo_body & "</tr></table>" & vbcrlf
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		emailSent = 1
	end if
%>
<!--#include virtual="/template/topCO.asp"-->
<% if emailSent = 1 then %>
<p>
    <strong>
        <br><br>
        Thank you for contacting CellularOutfitter.com!
    </strong>
</p>
<p>
    <strong>
        A Representative from the appropriate department will contact you promptly.
        If your inquiry is sent between normal business hours (8am-5pm PST M-F)
        expect an answer in the next few hours or within one business day.
    </strong>
</p>
<p>
    <strong>
        <br><br>
        CellularOutfitter.com
    </strong>
</p>
<% else %>
<div class="cuTitle">You have a question? We surely have the answer.</div>
<div class="cuDesc">
	Have questions or comments regarding your order, products, usage, returns or anything else? 
    Our team of Customer Service Associates at CellularOutfitter.com are standing by to assist 
    in any way to ensure the highest level of service possible. Please contact us by providing 
    the pertinent information below and your inquiry will be addressed IMMEDIATELY*.
</div>
<form action="/contact.html" method="post" name="cuForm" onsubmit="return(checkForm())">
<div class="cuFormRow">
	<div class="fl cuFormTitle">Your Name:<span class="requiredField">*</span></div>
    <div class="fl cuFormValue"><input type="text" name="name" size="20" maxlength="100" value="" /></div>
</div>
<div class="cuFormRow">
	<div class="fl cuFormTitle">Your Email:<span class="requiredField">*</span></div>
    <div class="fl cuFormValue"><input type="text" name="email" size="20" maxlength="100" value="" /></div>
</div>
<div class="cuFormRow">
	<div class="fl cuFormTitle">Type of Inquiry:<span class="requiredField">*</span></div>
    <div class="fl cuFormValue">
    	<select name="subject" class="cuSelectBox">
            <option selected>Select here...</option>
            <option>Product Sales & Usage Inquiries</option>
            <option>Post-Order Customer Service Inquiries</option>
            <option>Request for RMA</option>
            <option>Billing Inquiries</option>
            <option>Marketing Inquiries</option>
            <option>General Inquiries</option>
        </select>
    </div>
</div>
<div class="cuFormRow">
	<div class="fl cuFormTitle">Phone Number:</div>
    <div class="fl cuFormValue"><input type="text" name="phonenumber" size="20" maxlength="20" value="" /></div>
</div>
<div class="cuFormRow">
	<div class="fl cuFormTitle">Order Number:</div>
    <div class="fl cuFormValue"><input type="text" name="orderno" size="8" maxlength="8" value="" /></div>
</div>
<div class="cuFormRow">
	<div class="fl cuFormTitle">Question:<span class="requiredField">*</span></div>
    <div class="fl cuFormValue"><textarea name="question" cols="25" rows="4"></textarea></div>
</div>
<div class="cuFormRow">
	<div class="fl subForm"><input type="image" src="/images/buttons/button-submit.png" border="0" /></div>
</div>
</form>
<% end if %>
<div class="extraContactInfo">
    To ensure delivery of our emails to your inbox, please add support@CellularOutfitter.com to your Address Book.
    <br>Thank You.
</div>
<div class="extraContactInfo">
    * Normal business hours are 8am-5pm PST M-F. If you contact us outside of normal business hours we will be sure 
    to address your inquiry within the next business day. <br />                                                                       
    If you would like to place an order by phone, <br /> please call 800-871-6926.
</div>
<!--#include virtual="/template/bottom.asp"-->