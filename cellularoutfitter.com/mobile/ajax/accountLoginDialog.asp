<!--#include virtual="/Framework/Data/Const.asp"-->
<!--#include virtual="/Framework/Utility/Static.asp"-->
<!--#include virtual="/includes/account/account_base.asp"-->
<div id="divLoginDialog">
    <div id="divLoginTop">
        <p>Login to CellularOutfitter</p>
        <div id="divLoginClose" onclick="closeFloatingBox();"></div>
    </div>
    <div id="divLoginMiddle">
        <form action="<%=urlRelLogin%>" name="LoginForm" method="post">
            <label for="inputEmail" title="Email">Email:</label><input type="text" id="inputEmail" name="inputEmail" value="Email Address" onfocus="removeGhost(this)" />
            <label for="inputPassword" title="Password">Password:</label><input type="text" id="inputPassword" name="inputPassword" value="Password" onfocus="replaceT(this)" />
            <input id="submitButton" name="submitButton" type="submit" value="Login" />
        </form>
        <p><a href="<%=urlRelForgotPassword%>">Forgot your password?(<%=urlRelLogin%>)</a></p>
        <p>Not a member yet? <a href="<%=urlRelRegister%>">Join us!</a></p>
    </div>
    <div id="divLoginBottom">
        <div id="divLoginFacebook" onclick="window.location.href='<%=urlRelFacebook%>'"></div>
        <div id="divLoginGoogle" onclick="window.location.href='<%=urlRelLoginGoogle%>'"></div>
    </div>
</div>
<%
call CloseConn(oConn)
%>