<%
	response.buffer = true
	response.expires = -1
	
	itemids = "146466"
	currentTotal = 0.0
	currentTax = 0.0
	if prepStr(request.querystring("itemids")) <> "" then itemids = prepStr(request.querystring("itemids"))
	if prepInt(request.querystring("currentTotal")) > 0 then currentTotal = prepInt(request.querystring("currentTotal"))	
	if prepInt(request.querystring("currentTax")) > 0 then currentTax = prepInt(request.querystring("currentTax"))
	
	orderid = prepInt(request.querystring("orderid"))
	auth_transactionid = prepStr(request.QueryString("transid"))
	extOrderType = prepInt(request.querystring("extordertype"))
	
	usePromoPercent = 0
	
	if instr(request.ServerVariables("SERVER_NAME"),"mdev.") > 0 then	
		if extOrderType = 0 then
			actionURL = "/framework/postProcessing.asp"
		elseif extOrderType = 1 then
			actionURL = "/framework/DoReferenceTransaction.asp"
		end if
	else
		if extOrderType = 0 then
			actionURL = "https://" & request.ServerVariables("SERVER_NAME") & "/framework/postProcessing.asp"
		elseif extOrderType = 1 then
			actionURL = "https://" & request.ServerVariables("SERVER_NAME") & "/framework/DoReferenceTransaction.asp"
		end if
	end if
	
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<div style="width:310px; text-align:left; margin-left:auto; margin-right:auto; margin-top:20px; position:relative; color:#444; font-family:Arial, Helvetica, sans-serif;">
	<form name="frmPostPurchase" action="<%=actionURL%>" method="post">
    <div style="position:absolute; top:-10px; right:-11px; cursor:pointer;" onClick="pp_closePopup()"><img src="/images/mobile/checkout/btn-close.png" border="0" /></div>
    <div style="float:left; width:100%;">
    	<div style="float:left; width:100%;">
	        <div style="float:left; width:100%; background:url(/images/mobile/checkout/top-corner-left.png) no-repeat; width:10px; height:40px;"></div>
		    <div style="float:left; width:290px; background:url(/images/mobile/checkout/bg-1-px.png) repeat-x; height:30px; padding-top:10px;">
                <div style="float:left; padding-left:20px;"><img src="/images/mobile/checkout/header-ico-checkmark.png" border="0" /></div>
                <div style="float:left; color:#fff; font-weight:bold; padding-left:10px; font-size:16px;">Thank you for your purchase!</div>
            </div>
	        <div style="float:left; width:100%; background:url(/images/mobile/checkout/top-corner-right.png) no-repeat; width:10px; height:40px;"></div>
        </div>
        <div style="float:left; padding:5px; font-size:12px; background-color:#fff;">
        	Your order is complete. However, we noticed you didn't get a chance to add some popular accessories for your phone.
            <%
			sql	=	"select	top 1 promoDate, promoPercent" & vbcrlf & _
					"from	postpurchasepromo" & vbcrlf & _
					"where	siteid = 2" & vbcrlf & _
					"order by 1 desc"
			set rs = oConn.execute(sql)
			if not rs.eof then usePromoPercent = rs("promoPercent")
			
			if usePromoPercent > 0 then
			%>
            <br /><br /><span style="color:#336699; font-weight:bold;">FOR ONE TIME ONLY</span>, just click <span style="color:#336699; font-weight:bold;">ADD TO MY ORDER</span> for an <span style="color:#336699; font-weight:bold; font-style:italic; text-decoration:underline;">additional <%=usePromoPercent%>% OFF</span> the related items below. <i>Please note:</i> <span style="color:#336699; font-weight:bold; text-decoration:underline;">You will never see this offer again</span> so order now!
            <%
			else
			%>
            <br /><br />Just click <span style="color:#336699; font-weight:bold;">ADD TO MY ORDER</span> and we'll instantly add your selected items to your cart.
            <%
			end if
			%>
        </div>
        <div style="float:left; width:300px; padding:5px; background-color:#fff; border-bottom-left-radius:5px; border-bottom-right-radius:5px;">
            <div style="float:left; width:100%; border-top:1px solid #ccc; height:0px; margin:5px 0px 5px 0px;"></div>
            <div style="float:left; width:100%;">
            <%
			sql = "sp_ppUpsell 2, '" & itemids & "', " & usePromoPercent & ", 60"
			set rs = oConn.execute(sql)
			lap = 0
			do until rs.eof
				lap = lap + 1
				itemdesc = rs("itemdesc")
'				if len(itemdesc) > 75 then itemdesc = left(itemdesc, 72) & "..."
				price_retail = rs("price_retail")
				price_co = rs("price")
				%>
            	<div style="float:left; width:100%; border-bottom:1px solid #ccc; padding:10px 0px 10px 0px;">
                	<div style="float:left; width:110px; text-align:center;">
                    	<div style="width:100%; text-align:left;"><img src="/productpics/thumb/<%=rs("itempic")%>" border="0" /></div>
                        <div class="qtySelect" style="margin-left:20px; text-align:center;">
                            <select id="id_qty<%=lap%>" name="chkQty<%=lap%>" class="invisibleSelect" onChange="updatePPTotal();" >
                                <% for i = 1 to 20 %>
                                <option value="<%=i%>"><%=i%></option>
                                <% next %>
                            </select>
                        </div>
					</div>
                	<div style="float:left; width:190px; text-align:left; font-size:12px;">
                        <div style="float:left; width:100%; background-color:#eaeff5; border-radius:5px; padding:3px 0px 3px 0px; margin-bottom:5px;">
                            <div style="float:left; padding-left:10px;"><input id="id_chkItem<%=lap%>" type="checkbox" name="chkItem<%=lap%>" value="<%=rs("itemid")%>" onClick="updatePPTotal();" /></div>
                            <div style="float:left; padding:2px 0px 0px 5px; color:#006599; font-weight:bold; font-size:11px;">Add this to my order!</div>
                        </div>
                        <div style="float:left; width:100%; padding-bottom:5px; color:#006599; text-decoration:underline;"><%=itemdesc%></div>
                        <div style="float:left; width:100%; padding-bottom:5px; color:#777;">Reg.: <span style="text-decoration:line-through;"><%=formatcurrency(price_retail)%></span></div>
                        <div style="float:left; width:100%; padding-bottom:5px;">
	                        <div style="float:left; color:#222; font-weight:bold;">Our Price:</div>
	                        <div style="float:left; color:#F00; font-weight:bold; padding-left:5px;"><%=formatcurrency(price_co)%></div>
	                        <div style="float:left; color:#F00; font-size:11px; padding-left:5px;">(Save <%=formatPercent((price_retail - price_co) / price_retail,0)%>)</div>
                        </div>
                        <div style="float:left; width:100%;">
                            <div style="float:left; padding:2px 3px 0px 0px;"><img src="/images/mobile/checkout/product-ico-checkmark.png" border="0" /></div>
                            <div style="float:left; color:#006599; font-weight:bold;">In Stock</div>
                            <div style="float:left; padding-left:5px;"><%=getRatingAvgStar(rs("avgReview"))%></div>
                        </div>
					</div>
                    <input id="id_itemPrice<%=lap%>" type="hidden" name="itemPrice<%=lap%>" value="<%=price_co%>" />
                    <input id="id_itemRetail<%=lap%>" type="hidden" name="itemRetail<%=lap%>" value="<%=price_retail%>" />
                </div>
            	<%
				rs.movenext
			loop
			

			%>
            </div>
            <div style="float:left; width:100%; margin:25px 0px 0px 0px; text-align:center;">
            	<div style="color:#333;">Please add the selected items to my order now for an additional: </div>
                <div style="padding-top:10px; color:#336699; font-weight:bold; font-size:16px;">$<span id="addlTotal">0.0</span>* (Save $<span id="addlSave">0.0</span>)</div>
            </div>
            <div id="pp_bottomBox" style="float:left; width:100%; margin-top:15px; padding:10px 0px 10px 0px;">
                <div style="float:left; padding-left:20px;">
                	<img src="/images/mobile/checkout/cta-no-thanks.png" border="0" onClick="pp_closePopup()" style="cursor:pointer;" />
				</div>
                <div style="float:left; padding-left:10px;">
                	<input type="image" src="/images/mobile/checkout/cta-add.png" onClick="return doSubmit();" />
				</div>
            </div>
            <div id="pp_bottomBoxText" style="display:none; float:left; width:100%; margin-top:15px; color:#333; font-size:16px; text-align:center; padding:10px 0px 10px 0px;">
				Please wait while we process your order.<br />
                <img src="/images/progress_bar.gif" border="0" />
            </div>
            <%
			curTax = 0
			curGrandTotal = 0
			sql = 	"select	isnull(ordersubtotal, 0) ordersubtotal, isnull(ordershippingfee, 0) ordershippingfee, isnull(orderTax, 0) orderTax, isnull(ordergrandtotal, 0) ordergrandtotal, accountid" & vbcrlf & _
					"from	we_orders" & vbcrlf & _
					"where	orderid = '" & orderid & "'"
			set rs = oConn.execute(sql)
			if not rs.eof then
				curSubTotal = prepInt(rs("ordersubtotal"))
				curShippingFee = prepInt(rs("ordershippingfee"))
				curTax = prepInt(rs("orderTax"))
				curGrandTotal = prepInt(rs("ordergrandtotal"))
				accountid = prepInt(rs("accountid"))
			end if
			%>
            <div style="float:left; width:100%; margin:15px 0px 0px 0px; font-size:12px; text-align:center;">
                *Items will be added to your current order. You will see a 2nd charge for $<span id="addlGrandTotal">0.00</span> on your credit card. 
                Total includes $<span id="addlTaxTotal">0.00</span> in sales tax for California residents. 
                This is a CellularOutfitter.com offer.
            </div>
        </div>
    </div>
    <div style="clear:both; height:15px;"></div>
    <input type="hidden" name="addlGrandTotal" value="" />
    <input type="hidden" name="grandTotal" value="<%=curGrandTotal%>" />
    <input type="hidden" name="taxTotal" value="<%=curTax%>" />
    <input type="hidden" name="hidCount" value="<%=lap%>" />
    <input type="hidden" name="orderid" value="<%=orderid%>" />
    <input type="hidden" name="transid" value="<%=auth_transactionid%>" />
	</form>
</div>
<%

call CloseConn(oConn)

function getRatingAvgStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarFull.gif"" border=""0"" width=""14"" height=""13"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarHalf.gif"" border=""0"" width=""8"" height=""13"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function
%>
