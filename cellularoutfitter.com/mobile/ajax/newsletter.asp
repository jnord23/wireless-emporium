<%
dim fso : set fso = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
if prepInt(mySession) = 0 then
	mySession = session.SessionID
	response.cookies("mySession") = mySession
	response.cookies("mySession").expires = now+30
end if
	
dim semail, brandID, modelID, typeID, itemID
semail = prepStr(request.QueryString("semail"))
brandID = prepInt(request.QueryString("brandID"))
modelID = prepInt(request.QueryString("modelID"))
typeID = prepInt(request.QueryString("typeID"))
itemID = prepInt(request.QueryString("itemID"))
dim newsletterMsg : newsletterMsg = ""

bSubscribed = false

if semail <> "" then
	if semail = "closewidget" then
		response.cookies("welcomeCouponUsed") = mySession
		response.cookies("welcomeCouponUsed").expires = now+30
		response.write "do not want"
	elseif instr(trim(semail), "@") > 0 and instr(trim(semail), " ") = 0 and instr(trim(semail), ".@") = 0 then
		if isValidEmail(semail) then
			response.Cookies("customerEmail") = semail
			response.Cookies("customerEmail").expires = now + 60
			
			sql = "exec sp_emailSub 2,'" & semail & "',''," & brandID & "," & modelID & "," & typeID & "," & itemID
			session("errorSQL") = sql
			set emailRS = oConn.execute(sql)
			
			if emailRS("returnValue") = "duplicate" then
				newsletterTitle = "Oops!"
				newsletterMsg = "Our records indicate that you are already registered making you ineligible for this offer.  We're sorry for any inconvenience - be sure to check your inbox for our latest deals. " &_
								"<br /><br />" &_
								"Sincerely,<br />" &_                                                    
								"CellularOutfitter.com"
			else
				dim cdo_to, cdo_from, cdo_subject, cdo_body
				set curHTML = fso.OpenTextFile(server.MapPath("/images/email/blast/template/co-welcome-email.htm"),1,false,0)
				cdo_body = curHTML.readall
				cdo_body = replace(cdo_body,"{{campaign_id}}","welcome1")
				cdo_body = replace(cdo_body,"{{promo_code}}","WELCOME25")
				cdo_body = replace(cdo_body,"{{expDate}}",date+7)
				
				if semail = "jnord@test.com" then semail = "jon@wirelessemporium.com"
				cdo_to = semail
				cdo_from = "Cellular Outfitter <support@CellularOutfitter.com>"
				cdo_subject = "Welcome and Prepare Yourself for Serious Savings"
				
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
				
				newsletterTitle = "Check Your Inbox!"
				newsletterMsg = "Thanks for joining! As promised, we've sent your 25% off coupon code to the email address you have provided." &_
                    			"<br /><br />" &_
                    			"And guess what? There are more deals to come!" &_
                    			"<br /><br />" &_
                    			"Now that you are a member, you'll be the first to know about exclusive sales, special discounts, and insider information before anyone else! But keep it a secret, because this is just for our fans. Be sure to check your inbox, because you never know when something special will show up!"
								
			end if
			response.cookies("welcomeCouponUsed") = mySession
			response.cookies("welcomeCouponUsed").expires = now+30			
			bSubscribed = true
		else
			newsletterMsg = "Please enter a valid email address and try again. (" & semail & ")"
		end if
	else
		newsletterMsg = "Please make sure you enter a valid email address and try again. (" & semail & ")"
	end if
else
	newsletterMsg = "Email error, please double check that you have entered a valid email. (" & semail & ")"
end if

call CloseConn(oConn)

%>
    <div onclick="closeEmailWidget('overlay2');<%if bSubscribed then%>showHide(null, 'icon25');showHide(null, 'floating-banner');<%end if%>" class="btn-close"></div>
    <div class="modal-title-container">
        <div class="modal-title-left"></div>
        <div class="modal-title-center">
            <div class="check"><img src="/images/mobile/emailpromo/checkmark.png" /></div>
            <div class="modal-title"><%=newsletterTitle%></div>
        </div>
        <div class="modal-title-right"></div>
    </div>
    <div class="modal-content">
        <p style="text-align:center;"><%=newsletterMsg%></p>
        <p style="text-align:center;">
            <input type="button" class="modal-button" id="modal-success" value="CONTINUE" onclick="closeEmailWidget('overlay2');<%if bSubscribed then%>showHide(null, 'icon25');showHide(null, 'floating-banner');<%end if%>" />
        </p>
    </div>
<%

Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address
	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function
%>