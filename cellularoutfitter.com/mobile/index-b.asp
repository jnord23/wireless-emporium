<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "Home"
	
	dim reset : reset = prepInt(request.QueryString("reset"))
	if reset = 1 then
		response.Cookies("mobileBrandID") = 0
		response.Cookies("mobileModelID") = 0
	end if
	
	sql = "exec sp_brands"
	session("errorSQL") = sql
	set brandListRS = oConn.execute(sql)
%>
<!--#include virtual="/template/topCO.asp"-->
<div class="Banner"></div>
<div class="titleBar">
	<div class="centerContain">
		<div class="fl titleTxt">SHOP BY PRODUCT CATEGORY</div>
    	<div class="fl titleArrow"></div>
    </div>
</div>
<div class="categoryMenu">
	<div class="centerContain">
		<%
        do while not brandListRS.EOF
            brandID = brandListRS("brandID")
            brandName = brandListRS("brandName")
            if len(brandName) > 20 then useClass = "menuTxt2" else useClass = "menuTxt"
        %>
        <div class="fl menuItem" onclick="window.location='/b-<%=brandID%>-<%=formatSEO(brandName)%>-cell-phone-accessories.html'">
            <div class="menuPic"><img src="/images/mobile/brands/<%=brandID%>.gif" border="0" /></div>
            <div class="<%=useClass%>"><a href="/c-<%=brandID%>-<%=formatSEO(brandName)%>-cell-phone-accessories.html" class="whiteLink" title=""><%=brandName%></a></div>
        </div>
        <%
            brandListRS.movenext
        loop
        %>
    </div>
</div>
<!--#include virtual="/template/bottom.asp"-->