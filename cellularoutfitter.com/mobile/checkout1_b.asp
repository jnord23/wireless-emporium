<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<%
	
	pageName = "Checkout"
	securePage = 1
	
	dim itemID : itemID = prepInt(request.Form("itemID"))
	dim qty : qty = prepInt(request.Form("qty"))
	dim mySession : mySession = prepStr(request.cookies("mySession"))
	dim cartTotal : cartTotal = 0
	dim totalQty : totalQty = 0
	
	if mySession = "" then
		mySession = session.SessionID
		response.cookies("mySession") = mySession
		response.cookies("mySession").expires = now+30
	end if
	
	sql = "exec sp_itemsInBasket 2, " & mySession
	session("errorSQL") = sql
	set cartItemsRS = oConn.execute(sql)
	
	do while not cartItemsRS.EOF
		qty = cartItemsRS("qty")
		price = cartItemsRS("price_CO")
		cartTotal = cartTotal + (price*qty)
		totalQty = totalQty + qty
		cartItemsRS.movenext
	loop
	cartItemsRS.movefirst
	
	shipcost0 = 4.00 + (1.99 * totalQty)
	shipcost2 = 8.00 + (1.99 * totalQty)
	shipcost3 = 21.00 + (1.99 * totalQty)
%>
<!--#include virtual="/template/topCO.asp"-->
<form method="post" action="/framework/processing.html" name="checkout1Form">
<div class="checkoutSteps">
	<div class="centerContain">
		<div class="fl lockImg"></div>
    	<div class="fl step1">1. Your Info &amp; Payment</div>
    	<div class="fl step2 inactiveCheckoutStep">2. Review</div>
    </div>
</div>
<div class="checkoutTitleBar" onclick="revealShippingOptions()">
	<div class="fl">Shipping Options</div>
    <div class="fr expandDiv">+</div>
</div>
<div id="shippingOptionDiv" class="shippingOptions hidden">
	<div class="centerContain">
        <div class="shippingOptionRow">
            <div class="fl shippingRadio">
                <input type="radio" name="shiptype" value="0" checked="checked" />
                <input type="hidden" name="shipcost0" value="<%=shipcost0%>" />
                <input type="hidden" name="shipcost0_show" value="<%=formatCurrency(shipcost0,2)%>" />
            </div>
            <div class="fl shippingTxt">USPS First Class (4-10 business days)</div>
        </div>
        <div class="shippingOptionRow">
            <div class="fl shippingRadio">
                <input type="radio" name="shiptype" value="2" />
                <input type="hidden" name="shipcost2" value="<%=shipcost2%>" />
                <input type="hidden" name="shipcost2_show" value="<%=formatCurrency(shipcost2,2)%>" />
            </div>
            <div class="fl shippingTxt">USPS Priority Mail (2-4 business days)</div>
        </div>
        <div class="shippingOptionRow">
            <div class="fl shippingRadio">
                <input type="radio" name="shiptype" value="3" />
                <input type="hidden" name="shipcost3" value="<%=shipcost3%>" />
                <input type="hidden" name="shipcost3_show" value="<%=formatCurrency(shipcost3,2)%>" />
            </div>
            <div class="fl shippingTxt">USPS Express Mail (1-2 business days)</div>
        </div>
        <div class="applyShipping" onclick="showShippingOnTotal()">Apply</div>
        <div class="mustApply">(MUST press Apply to update total)</div>
    </div>
</div>
<div class="checkoutTitleBar">Billing Address</div>
<div class="lightBg">
	<div class="centerContain">
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">First Name:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="fname" value="" /></div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Last Name:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="lname" value="" /></div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Address 1:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="bAddress1" value="" /></div>
        <div class="formTitle">
            <div class="fl requiredField"></div>
            <div class="fl">Address 2:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="bAddress2" value="" /></div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Zip/Postal Code:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="bZip" value="" onchange="getCityState(this.value,'bill')" /></div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">City:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="bCity" value="" /></div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">State/Province:</div>
        </div>
        <div class="formField">
            <select class="invisibleSelect" name="bState" onchange="checkTax(this.value)">
                <option value=""></option>
                <%getStates(bState)%>
            </select>
        </div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Phone:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="phone" value="" /></div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Email:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="email" value="" /></div>
    </div>
</div>
<div class="checkoutTitleBar">Shipping Address</div>
<div class="lightBgBordered">
	<div class="centerContain">
        <div class="moreOptions">
            <div class="fl checkbox"><input type="checkbox" name="shippingAddr" value="0" onchange="shipAddrOptions(this.checked)" /></div>
            <div class="fl diffShip">Different Than Billing Address</div>
        </div>
        <div id="shippingFields" class="shippingAddress">
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">Address 1:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="sAddress1" value="" /></div>
            <div class="formTitle">
                <div class="fl requiredField"></div>
                <div class="fl">Address 2:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="sAddress2" value="" /></div>
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">Zip/Postal Code:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="sZip" value="" onchange="getCityState(this.value,'ship')" /></div>
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">City:</div>
            </div>
            <div class="formField"><input class="invisibleText" type="text" name="sCity" value="" /></div>
            <div class="formTitle">
                <div class="fl requiredField">*</div>
                <div class="fl">State/Province:</div>
            </div>
            <div class="formField">
                <select class="invisibleSelect" name="sState">
                    <option value=""></option>
                    <%getStates(bState)%>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="checkoutTitleBar2">
	<div class="fl lockIcon"></div>
	<div class="fl graphicTitle">Secure Credit Card Payment</div>
</div>
<div class="lightBg ccForm">
	<div class="ccTitle">This is a secure 128-bit SSL<br />encrypted payment</div>
	<div class="centerContain ccFormElements">
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Cardholder Name:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="cc_cardOwner" value="" /></div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Credit Card #:</div>
        </div>
        <div class="formField"><input class="invisibleText" type="text" name="cardNum" value="" maxlength="16" /></div>
        <div class="creditCardExamples">
            <div class="tb fieldExamples">
            	<div class="fl">No Spaces/Dashes</div>
                <div class="fr creditCards"><img src="/images/mobile/icons/creditCardsSmall.gif" border="0" /></div>
            </div>
            <div class="fieldExamples">Example: 1234123412341234</div>
        </div>
        <div class="formTitle">
            <div class="fl requiredField">*</div>
            <div class="fl">Security Code:</div>
        </div>
        <div class="formFieldSmall"><input class="invisibleTextSmall" type="text" name="secCode" value="" maxlength="4" /></div>
        <div class="formTitle">
            <div class="fl requiredField"></div>
            <div class="fl">Expiration Date:</div>
        </div>
        <div>
            <div class="fl formFieldSmall">
                <select class="invisibleSelectSmall" name="cc_month">
                    <% for i = 1 to 12 %>
                    <option value="<%=i%>"><%=i%></option>
                    <% next %>
                </select>
            </div>
            <div class="fl expDateEntrySpacer">/</div>
            <div class="fl formFieldSmall">
                <select class="invisibleSelectSmall" name="cc_year">
                    <%
                    startingYear = year(now)
                    for i = startingYear to startingYear + 15 %>
                    <option value="<%=i%>"><%=i%></option>
                    <% next %>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="cartItem">
    <div class="cartSummaryTitle">CART SUMMARY</div>
    <div class="summaryRow">
        <div class="fl rowTitle">Cart Total:</div>
        <div class="fr rowValue"><%=formatCurrency(cartTotal,2)%></div>
    </div>
    <div class="summaryRow">
        <div class="fl rowTitle">Tax:</div>
        <div id="taxDiv" class="fr rowValue"><%=formatCurrency(0,2)%></div>
    </div>
    <div id="shipSummary" class="summaryRow hidden">
        <div class="fl rowTitle">Shipping:</div>
        <div id="shippingCostDiv" class="fr rowValue"><%=formatCurrency(shipcost0,2)%></div>
    </div>
    <div id="estSummary" class="summaryRowDark">
        <div class="fl rowTitle">Estimated Total:</div>
        <div id="estTotalDiv" class="fr rowValueGreen"><%=formatCurrency(cartTotal,2)%></div>
    </div>
    <div id="grandSummary" class="summaryRowDark hidden">
        <div class="fl rowTitle">Grand Total:</div>
        <div id="grandTotalDiv" class="fr rowValueGreen"><%=formatCurrency(cartTotal,2)%></div>
    </div>
</div>
<div class="proceedButton">
	<input type="image" name="Proceed to Checkout" src="/images/mobile/checkout/cta-btn.png" border="0" onclick="return(checkout1FormReview())" />
    <input type="hidden" name="subtotal" value="<%=cartTotal%>" />
    <input type="hidden" name="itemTotal" value="<%=cartTotal%>" />
    <input type="hidden" name="shippingTotal" value="<%=shipcost0%>" />
    <input type="hidden" name="taxAmt" value="0" />
    <input type="hidden" name="grandTotal" value="<%=cartTotal+shipcost0%>" />
    <input type="hidden" name="showShipping" value="0" />
    <input type="hidden" name="totalQty" value="<%=totalQty%>" />
    <%
	dim cartItemNum : cartItemNum = 0
	do while not cartItemsRS.EOF
		cartItemNum = cartItemNum + 1
	%>
    <input type="hidden" name="ssl_item_number_<%=cartItemNum%>" value="<%=cartItemsRS("itemID")%>">
	<input type="hidden" name="ssl_item_qty_<%=cartItemNum%>" value="<%=cartItemsRS("qty")%>">
	<input type="hidden" name="ssl_item_price_<%=cartItemNum%>" value="<%=cartItemsRS("price_CO")%>">
    <%
		cartItemsRS.movenext
	loop
	%>
    <input type="hidden" name="ssl_ProdIdCount" value="<%=totalQty%>" />
    <input type="hidden" name="mobile" value="1" />
</div>
</form>
<div id="customerInfo" style="display:none;"></div>
<script language="javascript">
	var taxValue = <%=Application("taxMath")%>
	var taxAmt = 0
</script>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript" type="text/javascript" src="/template/js/checkout.js"></script>