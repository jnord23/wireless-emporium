<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "brand-model-category"
	
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim sortBy : sortBy = prepStr(request.Form("sortBy"))
	if categoryID = 0 then response.Redirect("/")
	if brandID = 0 then response.Redirect("/")
	
	sql = "exec sp_coCategoryDetails " & categoryID
	session("errorSQL") = sql
	set categoryDetailsRS = oConn.execute(sql)
	if categoryDetailsRS.EOF then response.Redirect("/") else categoryName = categoryDetailsRS("categoryName")
	categoryDetailsRS = null
	
	sql = "exec sp_brandDetailsByBrandID " & brandID
	session("errorSQL") = sql
	set brandDetailsRS = oConn.execute(sql)
	if brandDetailsRS.EOF then response.Redirect("/") else brandName = brandDetailsRS("brandName")
	brandDetailsRS = null
	brandName = replace(brandName," Ericsson","")
	brandName = replace(brandName,"/Qualcomm","")
	
	sql = "exec sp_modelDetailsByID " & modelID
	session("errorSQL") = sql
	set modelDetailsRS = oConn.execute(sql)
	if modelDetailsRS.EOF then
		response.Redirect("/")
	else
		modelName = modelDetailsRS("modelName")
		modelImg = modelDetailsRS("modelImg")
	end if
	modelDetailsRS = null
	
	if sortBy = "" then sortBy = "pop"
	
	if categoryID = 5 then
		sql = "exec sp_bmcdProducts " & modelID & ", 5, '" & sortBy & "', 2, 0, 0, 5"
	elseif categoryID = 8 then
		sql = "exec sp_bmcdProducts " & modelID & ", 8, '" & sortBy & "', 2, 0, 0, 8"
	else
		sql = "exec sp_bmcdProducts	" & modelID & ", " & categoryID & ", '" & sortBy & "', 2, 0, " & brandID & ", 0"
	end if
	prodSQL = sql
	session("errorSQL") = sql
	set productsRS = oConn.execute(sql)
	
	function reviewScore(partNumber)
		sql = "select AVG(rating) as avgRating from co_Reviews where PartNumbers = '" & partNumber & "' and approved = 1"
		session("errorSQL") = sql
		set reviewRS = oConn.execute(sql)
		if not reviewRS.EOF then
			reviewScore = "<img src='/images/mobile/icons/" & cint(reviewRS("avgRating")) & "Star.gif' border='0' />"
		end if
	end function
%>
<!--#include virtual="/template/topCO.asp"-->
<div class="curCategory">
	<div class="viewingTitle">CURRENTLY VIEWING:</div>
    <div class="curCat"><%=brandName & " " & modelName & " " & categoryName%></div>
    <div class="catImg" style="background:url(/modelpics/thumbs/<%=modelImg%>) 0px 0px no-repeat;"></div>
    <div class="changeCat"><a href="/sc-<%=categoryID%>-sb-<%=brandID%>-cell-phone-<%=formatSEO(categoryName)%>-for-<%=formatSEO(brandName)%>.html" title="Change device" class="whiteLink">CHANGE DEVICE</a></div>
</div>
<div class="sortBar">
	<div class="sortBox">
    	<form name="sortForm" method="post">
            <select name="sortBy" class="sortObject" onchange="document.sortForm.submit()">
                <option value="">SORT BY</option>
                <option value="pop">Popular</option>
                <option value="new">Newest</option>
                <option value="low">Lowest Price</option>
                <option value="high">Highest Price</option>
            </select>
            <input type="hidden" name="brandID" value="<%=brandID%>" />
            <input type="hidden" name="modelID" value="<%=modelID%>" />
            <input type="hidden" name="categoryID" value="<%=categoryID%>" />
        </form>
    </div>
</div>
<div class="categoryMenu">
	<%
	dim productCnt : modelCnt = 0
	dim previousItemID : previousItemID = 0
	do while not productsRS.EOF
		itemID = productsRS("itemID")
		itemPartNumber = productsRS("partNumber")
		itemDesc = productsRS("itemDesc_CO")
		itemPic = productsRS("itemPic_CO")
		itemPrice = productsRS("price_co")
		itemRetail = productsRS("price_retail")
		reviewCnt = productsRS("reviews")
		if len(modelName) > 18 then useClass = "menuTxt2" else useClass = "menuTxt"
		
		if prepInt(itemPrice) > 0 and itemID <> previousItemID then
			productCnt = productCnt + 1
				if productCnt < 11 then
				useClass = "productRow"
				useImg = "img"
			else
				useClass = "productRow2"
				useImg = "tempimg"
			end if
			
			if fso.fileExists(server.MapPath("/productpics/thumb/" & itemPic)) then
	%>
    <div id="singleProdBox_<%=productCnt%>" class="<%=useClass%>" onclick="goHere('/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html')">
    	<div class="fl productLeft">
        	<div class="productPic"><<%=useImg%> src="/productpics/thumb/<%=itemPic%>" border="0" /></div>
            <% if reviewCnt > 0 then %>
            <div class="productReviews"><%=reviewScore(itemPartNumber)%> (<%=reviewCnt%>)</div>
            <% end if %>
        </div>
        <div class="fl productRight">
        	<div class="productDetails"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html" class="greyLink" title=""><%=itemDesc%></a></div>
            <div class="productPrice"><%=formatCurrency(itemPrice)%></div>
            <div class="productSavings">Save <%=formatPercent(1-(itemPrice/itemRetail),0)%></div>
            <div class="productRetail">Retail Price: <span class="retail"><%=formatCurrency(itemRetail)%></span></div>
        </div>
    </div>
    <%
			end if
		end if
		previousItemID = itemID
		productsRS.movenext
	loop
	%>
</div>
<script language="javascript">
	var pageName = "BMC"
	var maxProducts = <%=productCnt%>
</script>
<!--#include virtual="/template/bottom.asp"-->