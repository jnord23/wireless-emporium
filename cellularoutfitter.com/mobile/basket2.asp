<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	pageName = "Basket"
	
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	dim itemID : itemID = prepInt(request.Form("itemID"))
	dim qty : qty = prepInt(request.Form("qty"))
	dim wishList : wishList = prepInt(request.Form("wishList"))
	dim promo : promo = prepStr(request.Form("promo"))
	dim mySession : mySession = prepStr(request.cookies("mySession"))
	dim removePromo : removePromo = prepInt(request.QueryString("removePromo"))
	dim cartTotal : cartTotal = 0
	dim miniTop : miniTop = 1
	dim sPromoCode
	
	if mySession = "" then
		mySession = session.SessionID
		response.cookies("mySession") = mySession
		response.cookies("mySession").expires = now+30
	end if
	
	if removePromo = 1 then session("promocode") = ""
	
	if itemID > 0 then
		if wishList > 0 then
			if qty = 0 then
				sql = "delete from shoppingCartWishList where itemID = " & itemID & " and sessionID = " & mySession
			else
				sql = "exec sp_addToWishList 2," & mySession & "," & itemID & "," & wishList
			end if
			session("errorSQL") = sql
			oConn.execute(sql)
		else
			sql = "delete from shoppingCart where sessionID = " & mySession & " and purchasedOrderID is not null"
			session("errorSQL") = sql
			oConn.execute(sql)
			if qty = 0 then
				sql = "delete from shoppingCart where itemID = " & itemID & " and sessionID = " & mySession
			else
				sql =	"if (select count(*) from shoppingCart where store = 2 and itemID = " & itemID & " and sessionID = " & mySession & ") > 0 " &_
							"update shoppingCart " &_
							"set qty = " & qty & " " &_
							"where itemID = " & itemID & " and sessionID = " & mySession & " " &_
						"else " &_
							"insert into shoppingCart (" &_
								"store,sessionID,accountID,itemID,qty,mobileOrder" &_
							") values(" &_
								"2," & mySession & ",0," & itemID & "," & qty & ",1" &_
							")"
			end if
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		
		response.Redirect("/basket2.html")
	elseif promo <> "" then
		sql = "exec sp_promoDetails '" & promo & "'"
		session("errorSQL") = sql
		set promoDetailsRS = oConn.execute(sql)
		
		if not promoDetailsRS.EOF then
			session("promocode") = promo
		else
			promoError = "Promo Code Not Found"
		end if
	end if
	
	sPromoCode = session("promocode")
	
	if sPromoCode <> "" then
		'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
		dim couponid
		couponid = 0
		sql = "exec sp_promoDetails '" & sPromoCode & "'"
		session("errorSQL") = sql
		set RS = oConn.execute(sql)
		if not RS.eof then
			dim promoMin, promoPercent, typeID, excludeBluetooth, couponDesc, FreeItemPartNumber
			couponid = RS("couponid")
			promoMin = RS("promoMin")
			promoPercent = RS("promoPercent")
			typeID = RS("typeID")
			excludeBluetooth = RS("excludeBluetooth")
			couponDesc = RS("couponDesc")
			FreeItemPartNumber = RS("FreeItemPartNumber")
			setValue = RS("setValue")
			oneTime = RS("oneTime")
		else
			promoError = "Promo Code Not Found"
			sPromoCode = ""
		end if
	end if
	
	sql = "exec sp_itemsInBasket 2, " & mySession
	session("errorSQL") = sql
	set cartItemsRS = oConn.execute(sql)
	
	sql = "exec sp_itemsInWishList 2, " & mySession
	session("errorSQL") = sql
	set wishItemsRS = oConn.execute(sql)
	
	sql = "exec sp_alsoPurchased 2, " & mySession
	saveSQL = sql
	session("errorSQL") = sql
	set alsoPurchasedRS = oConn.execute(sql)
	
	shipCutOff = datediff("n",now,cdate(date & " 12:00:00PM"))
	if shipCutOff > 0 then
		sdsHours = 0
		sdsMinutes = shipCutOff
		do while sdsMinutes > 60
			sdsMinutes = sdsMinutes - 60
			sdsHours = sdsHours + 1
		loop
		if sdsHours > 1 then useHour = "hours" else useHour = "hour"
		if sdsMinutes > 1 then useMinute = "minutes" else useMinute = "minute"
	end if
%>
<!--#include virtual="/template/topCO.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<%
	if prepStr(miniTotalQuantity) = "0 Items" or prepStr(miniTotalQuantity) = "" then haveItems = false else haveItems = true
%>
<div class="tb basketValues">
	<div class="centerContain pt10">
        <div class="fl propBox1">
            <div class="blueProp">
            	<div class="fr italic">PRICES</div>
                <div class="fr bold pr5">LOWEST</div>
            </div>
            <div class="grayProp">110% Price Guarantee</div>
        </div>
        <div class="fl propThumb"></div>
        <div class="fl propBox2">
            <div class="blueProp">
                <div class="fl bold pr5">90-DAY</div>
                <div class="fl italic">RETURNS</div>
            </div>
            <div class="grayProp">Complete Satisfaction</div>
        </div>
    </div>
</div>
<% if prepStr(promoError) <> "" then %>
<div class="promoError"><%=promoError%></div>
<% end if %>
<% if not cartItemsRS.EOF then %>
<div class="proceedToCheckout" onclick="window.location='https://m.cellularoutfitter.com/checkout1.html'"></div>
<div class="centerContain tb">
	<div class="fl ready2ShipIcon"></div>
    <div class="fl ready2ShipText">
    	<% if shipCutOff > 0 then %>
    	All items in your cart are <strong class="greenText">IN STOCK</strong> and ship today if you order within the next 
        	<%
			if sdsHours > 0 then
			%>
		        <strong><%=sdsHours & " " & useHour%></strong> and <strong><%=sdsMinutes & " " & useMinute%></strong>.
            <% else %>
            	<strong><%=sdsMinutes & " " & useMinute%></strong>.
            <% end if %>
        <% else %>
        All items in your cart are <strong class="greenText">IN STOCK</strong> and ships within 24 hours.
        <% end if %>
    </div>
</div>
<div class="topSubTotal">
	<div class="centerContain pl20">
		Cart Subtotal (<%=miniTotalQuantity%>): <strong id="topSubTotalValue" class="blueText"></strong>
    </div>
</div>
<% end if %>
<% if miniTotalQuantity = "0 Items" then %>
<div class="noItemsInCart">YOU DO NOT HAVE ANY ITEMS IN YOUR SHOPPING CART</div>
<% else %>
<%
	itemDetails = ""
	do while not cartItemsRS.EOF
		itemID = cartItemsRS("itemID")
		qty = cartItemsRS("qty")
		itemDesc = cartItemsRS("itemDesc_CO")
		itemPic = cartItemsRS("itemPic_CO")
		price = cartItemsRS("price_CO")
		retail = cartItemsRS("price_retail")
		itemDetails = itemDetails & qty & "##" & itemID & "##" & itemDesc & "##" & price & "@@"
%>
<div class="basicCartItem">
	<div class="centerContain">
        <div class="fl itemPicIcon"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html"><img src="/productpics/icon/<%=itemPic%>" border="0" /></a></div>
        <div class="fl cartItemDetailsLarge">
        	<div class="tb">
	            <div class="fl itemDescBold"><%=itemDesc%></div>
                <div class="fl qtySelectFloat">
                    <form name="itemForm_<%=itemID%>" action="/basket2.html" method="post">
                    <select name="qty" class="invisibleSelect" onchange="document.itemForm_<%=itemID%>.submit();">
                        <% for i = 0 to 20 %>
                        <option value="<%=i%>"<% if qty = i then %> selected="selected"<% end if %>><%=i%></option>
                        <% next %>
                    </select>
                    <input type="hidden" name="itemID" value="<%=itemID%>" />
                    <input type="hidden" name="wishList" value="0" />
                    </form>
                </div>
            </div>
            <div class="basePriceSmall tb">
            	<div class="fl priceText">Item Price:</div>
				<div class="fl redText bold"><%=formatCurrency(price,2)%></div>
                <div class="fl retailText">Reg: <%=formatCurrency(retail,2)%></div>
            </div>
            <div class="basePriceSmall tb">
            	<div class="fl priceText">You Save:</div>
				<div class="fl redText bold"><%=formatCurrency(retail-price,2)%> (<%=formatPercent(((price/retail)-1)*-1,0)%>)</div>
            </div>
            <div class="basePriceSmall tb">
            	<div class="fl priceText">Item Total:</div>
				<div class="fl blueText bold"><%=formatCurrency(price*qty,2)%></div>
            </div>
            <div class="tb mtb15 ml10">
            	<div class="fl mr15" onclick="saveItem(<%=itemID%>)"><img src="/images/mobile/buttons/save4Later.gif" border="0" width="103" height="28" /></div>
                <div class="fl" onclick="removeItem(<%=itemID%>)"><img src="/images/mobile/buttons/deleteItem.gif" border="0" width="59" height="28" /></div>
            </div>
        </div>
    </div>
</div>
<%
		cartTotal = cartTotal + (price*qty)
		cartItemsRS.movenext
	loop
	cartItemsRS = null
end if

if not wishItemsRS.EOF then
	wishCnt = 0
	do while not wishItemsRS.EOF
		wishCnt = wishCnt + prepInt(wishItemsRS("qty"))
		wishItemsRS.movenext
	loop
	wishItemsRS.movefirst
%>
<div class="topSubTotal">
	<div class="centerContain pl20">
		Saved Items (<%=wishCnt%>)
    </div>
</div>
<%
	do while not wishItemsRS.EOF
		itemID = wishItemsRS("itemID")
		qty = wishItemsRS("qty")
		itemDesc = wishItemsRS("itemDesc_CO")
		itemPic = wishItemsRS("itemPic_CO")
		price = wishItemsRS("price_CO")
		retail = wishItemsRS("price_retail")
%>
<div class="wishCartItem">
	<div class="centerContain">
        <div class="fl itemPicIcon"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html"><img src="/productpics/icon/<%=itemPic%>" border="0" style="border:1px solid #CCC;" /></a></div>
        <div class="fl cartItemDetailsLarge">
        	<div class="tb">
	            <div class="fl itemDescBold"><%=itemDesc%></div>
                <div class="fl qtySelectFloat">
                    <form name="itemWishForm_<%=itemID%>" action="/basket2.html" method="post">
                    <select name="qty" class="invisibleSelect" onchange="document.itemForm_<%=itemID%>.submit();">
                        <% for i = 0 to 20 %>
                        <option value="<%=i%>"<% if qty = i then %> selected="selected"<% end if %>><%=i%></option>
                        <% next %>
                    </select>
                    <input type="hidden" name="itemID" value="<%=itemID%>" />
                    <input type="hidden" name="wishList" value="1" />
                    </form>
                </div>
            </div>
            <div class="basePriceSmall tb">
            	<div class="fl priceText">Item Price:</div>
				<div class="fl redText bold"><%=formatCurrency(price,2)%></div>
                <div class="fl retailText">Reg: <%=formatCurrency(retail,2)%></div>
            </div>
            <div class="basePriceSmall tb">
            	<div class="fl priceText">You Save:</div>
				<div class="fl redText bold"><%=formatCurrency(retail-price,2)%> (<%=formatPercent(((price/retail)-1)*-1,0)%>)</div>
            </div>
            <div class="basePriceSmall tb">
            	<div class="fl priceText">Item Total:</div>
				<div class="fl blueText bold"><%=formatCurrency(price*qty,2)%></div>
            </div>
            <div class="tb mtb15 ml10">
            	<div class="fl mr15" onclick="buyItem(<%=itemID%>)"><img src="/images/mobile/buttons/addBackToCart.gif" border="0" width="103" height="28" /></div>
                <div class="fl" onclick="removeWishItem(<%=itemID%>)"><img src="/images/mobile/buttons/deleteItem.gif" border="0" width="59" height="28" /></div>
            </div>
        </div>
    </div>
</div>
<%
		wishItemsRS.movenext
	loop
	wishItemsRS = null
end if

if itemDetails <> "" then
	itemDetailsArray = split(left(itemDetails,len(itemDetails)-2),"@@")
%>
<div class="totalBox">
	<%
	if prepStr(couponDesc) <> "" then
		if prepInt(promoMin) <= cartTotal then
			cartTotal = cartTotal - (cartTotal * promoPercent)
		end if
	%>
    <div class="promoApplied"><strong>Promo Applied: </strong><%=couponDesc%></div>
    <div class="removePromo"><a href="/basket2.html?removePromo=1" style="color:#F00;">Remove Promo</a></div>
    <% else %>
	<div class="tb promoBox" onclick="showPromo()" style="display:none;">
    	<div class="fl">Apply a Promotion Code</div>
        <div class="fl expandPromo"></div>
    </div>
    <form name="promoForm" method="post" action="/basket2.html">
    <div id="promoFormBox" class="hidePromoBox">
    	<div class="fl promoField"><input type="text" name="promo" value="" class="invisibleText" /></div>
        <div class="fl promoSubmit" onclick="submitPromoForm()">Apply</div>
    </div>
    </form>
    <% end if %>
    <div class="tb orderTotal">
    	<div class="fl">Estimated Order Total:</div>
        <div class="fr" id="bottomTotal"><%=formatCurrency(cartTotal,2)%></div>
    </div>
</div>
<%
end if
%>
<div class="centerContain">
    <div class="tb checkLine">
        <div class="fl greenCheck"></div>
        <div class="fl checkText">Over 2 Million Satisfied Customers</div>
    </div>
    <div class="tb checkLine">
        <div class="fl greenCheck"></div>
        <div class="fl checkText">All Orders Ship Within 24 Hours</div>
    </div>
    <div class="tb checkLine">
        <div class="fl greenCheck"></div>
        <div class="fl checkText">Unconditional 1-Year Warranty</div>
    </div>
    <div class="tb checkLine">
        <div class="fl greenCheck"></div>
        <div class="fl checkText">90-Day No-Hassle Return Policy</div>
    </div>
</div>
<% if haveItems then %>
<div class="proceedToCheckout" onclick="window.location='https://m.cellularoutfitter.com/checkout1.html'"></div>
<div class="paymentMethods" onclick="window.location='/checkout1.html'"></div>
<div class="payPal2" onclick="document.PaypalForm.submit()">
	<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.html" style="margin:0px;">
		<%
		if itemDetails <> "" then
			itemLap = 0
			for i = 0 to ubound(itemDetailsArray)
				singleItemArray = split(itemDetailsArray(i),"##")
				qty = singleItemArray(0)
				itemID = singleItemArray(1)
				itemDesc = singleItemArray(2)
				itemPrice = singleItemArray(3)
        %>
        <input type="hidden" name="L_NUMBER<%=itemLap%>" value="<%=itemID%>">
        <input type="hidden" name="L_NAME<%=itemLap%>" value="<%=itemDesc%>">
        <input type="hidden" name="L_QTY<%=itemLap%>" value="<%=qty%>">
        <input type="hidden" name="L_AMT<%=itemLap%>" value="<%=itemPrice%>">
        <%
            	itemLap = itemLap + 1
       		next
		end if
        %>
        <input type="hidden" name="numItems" value="<%=itemLap%>">
        <input type="hidden" name="paymentAmount" value="<%=cartTotal%>">
        <input type="hidden" name="ItemsAndWeight" value="<%=itemLap%>|<%=cartItems%>">
        <input type="hidden" name="mobileOrder" value="1">
    </form>
</div>
<% else %>
<div class="space15">&nbsp;</div>
<% end if %>
<div class="tb bottomLink">
	<div class="centerContain">
		<div class="fl cellphoneIcon"></div>
    	<div class="fl bottomLinkText"><a href="tel:1-888-589-9914" class="bottomHref" title="Call Us">Order By Phone</a></div>
    </div>
</div>
<div class="tb bottomLink">
	<div class="centerContain">
		<div class="fl returnsIcon"></div>
    	<div class="fl bottomLinkText"><a href="/returns.html" class="bottomHref" title="Returns">Returns Made Easy</a></div>
    </div>
</div>
<div class="tb bottomLink">
	<div class="centerContain">
		<div class="fl shopMoreIcon"></div>
    	<div class="fl bottomLinkText"><a href="/" class="bottomHref" title="Continue Shopping">Shop More Items</a></div>
    </div>
</div>
<% if haveItems then %>
<div class="otherItemsText">
	<div class="centerContain">Swipe to See What Customers Who Bought Items in Your Card Also Purchased:</div>
</div>
<div class="swipeBox">
	<div class="innerSwipeBox">
    	<table width="1000">
        	<tr>
		<%
		apLap = 0
		apImgLap = 0
		apItemList = ""
        do while not alsoPurchasedRS.EOF
			apLap = apLap + 1
			itemID = alsoPurchasedRS("itemID")
            itemPic = alsoPurchasedRS("itemPic_CO")
            itemDesc = alsoPurchasedRS("itemDesc_CO")
			itemPrice = prepInt(alsoPurchasedRS("price_co"))
			apItemList = apItemList & itemID & ","
			
			if fso.fileExists(server.MapPath("/productpics/icon/" & itemPic)) then
				apImgLap = apImgLap + 1
        %>
        <td width="100" class="alsoPurchasedItem">
        	<form name="ap_<%=itemID%>" action="/basket2.html" method="post">
                <div class="alsoPurchasedPic"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html"><img src="/productpics/icon/<%=itemPic%>" border="0" /></a></div>
                <div class="alsoPurchasedText"><%=itemDesc%></div>
                <div class="alsoPurchasedPrice"><%=formatCurrency(itemPrice,2)%></div>
            	<div class="addToCart_swipe" onclick="addToCart_ap(<%=itemID%>)">Add to Cart</div>
            	<input type="hidden" name="qty" value="1" />
	            <input type="hidden" name="itemID" value="<%=itemID%>" />
            </form>
        </td>
        <%
            end if
			alsoPurchasedRS.movenext
        loop
        %>
        	</tr>
        </table>
    </div>
</div>
<% end if %>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript" src="/template/js/basket.js"></script>
<!-- <%=saveSQL%> -->
<!-- <%=apLap%> -->
<!-- <%=apImgLap%> -->
<!-- <%=apItemList%> -->