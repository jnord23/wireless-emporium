	<div id="lowerBox">
        <div id="socializeText">Want to socialize with us?</div>
        <div id="socialIcons">
        	<div id="socialIcon"><a href="http://twitter.com/#!/wirelessemp/"><img src="/images/mobile/icons/twitter.gif" border="0" width="29" height="28" alt="Visit Our Twitter Page" /></a></div>
            <div id="socialIcon"><a href="http://www.facebook.com/WirelessEmporium"><img src="/images/mobile/icons/facebook.gif" border="0" width="29" height="28" alt="Visit Our Facebook Page" /></a></div>
            <div id="socialIcon"><a href="https://pinterest.com/wirelessemp/"><img src="/images/mobile/icons/pintrest.gif" border="0" width="29" height="28" alt="Visit Our Pintrest Page" /></a></div>
            <div id="socialIcon"><a href="https://plus.google.com/113217319312103488691?rel=author"><img src="/images/mobile/icons/googlePlus.gif" border="0" width="29" height="28" alt="Visit Our Google Page" /></a></div>
        </div>
        <div id="lowerSpacer"></div>
        <div id="footerText"><a href="http://www.wirelessemporium.com/?mobileAccess=full" id="freeText">View Full Site</a> | <a href="tel:<%=mobilePhoneNumber%>" id="freeText"><%=mobilePhoneNumber%></a></div>
        <div id="footerText"><a href="/privacy.htm" id="bottomLinks">Privacy Policy</a> | <a href="/terms.htm" id="bottomLinks">Terms and Conditions</a> | <a href="/contact.htm" id="bottomLinks">Contact Us</a></div>
        <div id="footerText">&copy; 2002-<%=year(date)%> Wireless Emporium, Inc.</div>
    </div>
</div>
</body>
</html>
<div id="dumpZone" style="display:none;"></div>
<div id="testZone"></div>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
  isMasterPage = 1
</script>
<script language="javascript" src="/Framework/UserInterface/Js/ajax.js"></script>
<script language="javascript">
	var pageName = "<%=pageName%>"
</script>
<script language="javascript" src="/template/js/base.js"></script>