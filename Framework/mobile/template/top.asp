<%
'	response.buffer = false
'	response.expires = -1
	
	'if prepInt(securePage) = 1 then
	'	if request.ServerVariables("HTTPS") = "off" then
	'		response.write(request.ServerVariables("ALL_HTTP"))
	'		response.end()
	'		response.redirect("https://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL"))
	'	end if
	'else
	'	if request.ServerVariables("HTTPS") = "on" then
	'		response.redirect("http://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL"))
	'	end if
	'end if
	
	mySession = prepInt(request.cookies("mySession"))
		
	sql = 	"select isnull(sum(a.qty), 0) totalQty " & vbcrlf & _
			"from shoppingcart a " & vbcrlf & _
			"where a.store = " & storeID & " and a.sessionid = '" & mySession & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)"
	session("errorSQL") = sql
	set objRsMiniCart = oConn.execute(sql)
	
	if objRsMiniCart.EOF then
		cartItems = 0
		miniTotalQuantity = 0 & " Items"
	else
		miniTotalQuantity = prepInt(objRsMiniCart("totalQty"))
		if miniTotalQuantity = 1 then
			cartItems = 1
			miniTotalQuantity = miniTotalQuantity & " Item"
		else
			cartItems = miniTotalQuantity
			miniTotalQuantity = miniTotalQuantity & " Items"
		end if
	end if
	
	if instr(request.ServerVariables("HTTP_X_REWRITE_URL"),".aspl") > 0 then
		response.Status = "301 Moved Permanently"
		response.AddHeader "Location", replace(request.ServerVariables("HTTP_X_REWRITE_URL"),".aspl",".html")
	end if
	
	useMetaPage = replace(request.ServerVariables("HTTP_X_REWRITE_URL"),".html",".asp")
	useMetaPage = replace(request.ServerVariables("HTTP_X_REWRITE_URL"),".htm",".asp")
	if pageName = "Basket" or pageName = "Basket2" then useMetaPage = "/cart/basket.asp"
	
'	sql = "select title, description, keywords from MetaTags where siteID = " & siteID & " and PhysicalAbsolutePath = '" & useMetaPage & "'"
'	session("errorSQL") = sql
'	set metaRS = oConn.execute(sql)
'	
'	if not metaRS.EOF then
'		metaTitle = metaRS("title")
'		metaDesc = metaRS("description")
'		metaKeywords = metaRS("keywords")
'	else
'		sql = "select title, description, keywords from MetaTags where siteID = " & siteID & " and brandID = " & prepInt(brandID) & " and modelID is null and typeID is null"
'		if prepInt(modelID) > 0 then sql = replace(sql,"modelID is null","modelID = " & modelID)
'		if prepInt(typeID) > 0 then sql = replace(sql,"typeID is null","typeID = " & typeID)
'		saveSQL = sql
'		session("errorSQL") = sql
'		set metaRS = oConn.execute(sql)
'		
'		if not metaRS.EOF then
'			metaTitle = metaRS("title")
'			metaDesc = metaRS("description")
'			metaKeywords = metaRS("keywords")
'		else
'			sql = "select seTitle as title, seDescription as description, seKeywords as keywords from metaTags_default where siteID = 0 and basePage = '" & basePageName & "'"
'			session("errorSQL") = sql
'			set metaRS = oConn.execute(sql)
'			
'			if not metaRS.EOF then
'				metaTitle = updateGenericMeta(metaRS("title"))
'				metaDesc = updateGenericMeta(metaRS("description"))
'				metaKeywords = updateGenericMeta(metaRS("keywords"))
'			end if
'		end if
'	end if
if instr(request.ServerVariables("SERVER_NAME"),"cellularoutfitter") > 0 then
	useServer = replace(request.ServerVariables("SERVER_NAME"),"m.cell","www.cell")
	useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	siteID = 2
else
	useServer = replace(request.ServerVariables("SERVER_NAME"),"m.wireless","www.wireless")
	useURL = replace(request.ServerVariables("HTTP_X_REWRITE_URL"),".html",".asp")
	useURL = replace(request.ServerVariables("HTTP_X_REWRITE_URL"),".htm",".asp")
	siteID = 0
end if

canonicalURL = "http://" & useServer & useURL
if prepVal(setCanonical) <> "" then
	tempServer = request.ServerVariables("SERVER_NAME")
	if left(tempServer,5) = "mdev." then tempServer = replace(tempServer,"mdev.","staging.")
	if left(tempServer,2) = "m." then tempServer = replace(tempServer,"m.","www.")
	canonicalURL = "http://" & tempServer & setCanonical
end if
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><%=SEtitle%></title>
    <meta name="Description" content="<%=SEdescription%>">
    <meta name="Keywords" content="<%=SEkeywords%>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1, user-scalable=0"/>
    <meta name="format-detection" content="telephone=no">
    <link id="baseCss" href="/template/css/base.css?v=20131223" rel="stylesheet" type="text/css">
    <link rel="canonical" href="<%=canonicalURL%>" />
    <script language="javascript" src="/framework/UserInterface/Js/ajax_noDynamicLoad.js"></script>
    <script language="javascript" src="/template/js/base.js?v=20140429"></script>
	<%=extraHeadFiles%>
    <%call printPixel(1)%>
</head>
<body onscroll="userScrolled()">
<% if instr(request.ServerVariables("SERVER_NAME"),"mdev.") > 0 then response.Write(request.ServerVariables("LOCAL_ADDR") & "<br>") %>
<%call printPixel(2)%>
<div id="wholeSite">