<%
dim disablePayPal : disablePayPal = 0
dim emphasizePayPal : emphasizePayPal = 1

function saveEmail()
	if prepStr(request.QueryString("utm_email")) <> "" then
		response.Cookies("customerEmail") = prepStr(request.QueryString("utm_email"))
		response.Cookies("customerEmail").expires = now + 60
	elseif prepStr(request.Cookies("customerEmail")) <> "" then
		response.Cookies("customerEmail") = prepStr(request.Cookies("customerEmail"))
		response.Cookies("customerEmail").expires = now + 60
	end if
end function

function jnord(msg)
	if Request.Cookies("username") = "jnord" then response.Write("<table border='0' width='100%' style='font-size:8pt;'><tr><td>" & msg & "</td></tr></table>")
	if request.cookies("myAccount") = "575054" then response.Write("<table border='0' width='100%' style='font-size:8pt;'><tr><td>" & msg & "</td></tr></table>")
end function

function isDropship(partNumber)
	if instr(partNumber,"MLD-") then
		isDropship = true
	elseif instr(partNumber,"HYP-") then
		isDropship = true
	elseif instr(partNumber,"DEC-SKN-") then
		isDropship = true
	elseif left(partNumber,3) = "MS-" then
		isDropship = true
	else
		isDropship = false
	end if
end function

function getPricingTM(dbPriceTM, dbPriceWE)
	tmPriceMin = cdbl(3.99)
	tmPriceDIff = cdbl(5)

	newPrice = tmPriceMin
	if not isnumeric(ds(dbPriceTM)) then dbPriceTM = 0
	if not isnumeric(ds(dbPriceWE)) then dbPriceWE = 0

	dbPriceTM = prepInt(dbPriceTM)
	dbPriceWE = prepInt(dbPriceWE)

	if dbPriceTM > 0 then
		newPrice = dbPriceTM
	else
		newPrice = dbPriceWE - tmPriceDiff
		if newPrice < tmPriceMin then
			newPrice = tmPriceMin
		end if
	end if

	getPricingTM = newPrice
end function

sub ImageHtmlDivWrapper(byval strImageTag, byval strUrl, byval strSaleImg, byval blnSale, byval altText, byval position)
	strDivWrapperHtmlStart	=	EMPTY_STRING
	strDivIconHtmlStart		=	EMPTY_STRING
	strDivHtmlEnd			=	EMPTY_STRING

	if blnSale then
		strDivWrapperHtmlStart	=	"<div style=""display:block; position: relative; z-index:0;"">" & vbcrlf
		strDivIconHtmlStart		=	"	<div style=""display:block; position: absolute; " & position & " z-index:1;"">" & vbcrlf & _
									"		<a href="""& strUrl &""" title=""" & altText & """>" & vbcrlf & _
												strSaleImg & vbcrlf & _
									"		</a>" & vbcrlf
		strDivHtmlEnd			=	"	</div>" & vbcrlf & _
									"</div>"
	end if

	response.write strDivWrapperHtmlStart
	response.write "<a href=""" & strUrl & """ title=""" & altText & """ >" & strImageTag & "</a>"
	response.write strDivIconHtmlStart
	response.write strDivHtmlEnd
end sub

'No longer being used on CA
sub ImageHtmlDivWrapper2(byval strImageTag, byval strUrl, byval strSaleImg, byval blnSale, byval altText, byval position, byval onSale) 'Overload of ImageHtmlDivWrapper(...)
	response.write "<div style=""display:block; position: relative; z-index:0;"">" & vbcrlf 'Begin relative container
	if blnSale then  '25% off badge
		response.write			"	<div style=""display:block; position: absolute; " & position & " z-index:1;"">" & vbcrlf & _
								"		<a href="""& strUrl &""" title=""" & altText & """>" & vbcrlf & _
											strSaleImg & vbcrlf & _
								"		</a>" & vbcrlf
		response.Write			"	</div>" & vbcrlf
	end if
	if onSale then 'Discounted by an arbitrary percent or amount
		response.write "<div class=""productOnSaleSmall onSale""></div>"
	end if
	response.write "<a href=""" & strUrl & """ title=""" & altText & """ >" & strImageTag & "</a>" & vbcrlf 'Product image, which displays no matter what
	response.write "</div>" & vbcrlf 'End relative container
end sub

function generateColorPickerText(byref arrColorRS, strColors, colorSelected, style)
	ret = ""
	if instr(strColors, ",") > 0 and isarray(arrColorRS) then
		arrTemp = split(strColors, ",")
		for i=0 to ubound(arrTemp)
			if arrTemp(i) <> "" then
				for j=0 to ubound(arrColorRS, 2)
					if cstr(arrTemp(i)) = cstr(arrColorRS(0,j)) then
						if cstr(colorSelected) = cstr(arrTemp(i)) then
							ret = ret & "<div style=""display:inline-block; " & style & """ id=""id_colorText_" & i & """>" & ucase(arrColorRS(1,j)) & "</div>"
						else
							ret = ret & "<div style=""display:none; " & style & """ id=""id_colorText_" & i & """>" & ucase(arrColorRS(1,j)) & "</div>"
						end if
					end if
				next
			end if
		next
	end if
	generateColorPickerText = ret
end function

function generateColorPicker(byref arrColorRS, pItemID, strColors, colorSelected, numColors, onSale)
	ret = ""
	if instr(strColors, ",") > 0 and isarray(arrColorRS) then
		arrTemp = split(strColors, ",")
		for i=0 to ubound(arrTemp)
			if arrTemp(i) <> "" then
				for j=0 to ubound(arrColorRS, 2)
					if cstr(arrTemp(i)) = cstr(arrColorRS(0,j)) then
						colorBoxID = "div_colorOuterBox_" & pItemID & "_" & i
						if cstr(colorSelected) = cstr(arrTemp(i)) then
							curClassName = "colorBoxSelected"
						else
							curClassName = "colorBoxUnselected"
						end if
						ret = ret & "<div class=""" & curClassName & """ id=""" & colorBoxID & """ onclick=""changeProduct('" & pItemID & "','" & arrTemp(i) & "','" & numColors & "','" & i & "','" & onSale & "');"" onmouseover=""preloading('" & pItemID & "','" & arrTemp(i) & "');"">"
						ret = ret & "<div style=""width:10px; height:10px; background-color:" & arrColorRS(2,j) & "; border:1px solid " & arrColorRS(3,j) & "; cursor:pointer;""></div> "
						ret = ret & "</div>"
					end if
				next
			end if
		next
	end if
	generateColorPicker = ret
end function

function insertDetails(val)
	if prepStr(val) <> "" then
		if brandName = "Other" then
			val = replace(val,"XXX ","")
		else
			val = replace(val,"XXX",prepStr(brandName))
		end if
		val = replace(val,"YYY",prepStr(modelName))
		val = replace(val,"ZZZ",prepStr(newItemDesc))
	end if
	insertDetails = val
end function

function insertDetailsAdv(val,useBrandName,useModelName)
	if prepStr(val) <> "" then
		session("errorSQL2") = "useBrandName:" & useBrandName & "<br>useModelName:" & useModelName
		val = replace(val,"XXX",prepStr(useBrandName))
		val = replace(val,"YYY",prepStr(useModelName))

		colorList = "Hot Pink##Lime Green##Orange##Gray##White##Black##Purple##Red##Blue##Green"
		colorArray = split(colorList,"##")
		colorCodeList = "14##27##24##02##11##10##21##20##17##16"
		colorCodeArray = split(colorCodeList,"##")

		for i = 0 to ubound(colorCodeArray)
			if right(partNumber,2) = colorCodeArray(i) then useColor = colorArray(i)
		next

		val = replace(val,"ZZZ",prepStr(useColor))
	end if
	insertDetailsAdv = val
end function

function formatSEO(val)
	if not isNull(val) and val <> "" then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o"),"!","-")
		formatSEO = replace(replace(replace(replace(formatSEO,"?","-"),"----","-"),"---","-"),"--","-")
	else
		formatSEO = ""
	end if
	select case Application("SiteId")
		case 1
			select case formatSEO
				case "sidekick" : formatSEO = "t-mobile-sidekick"
				case "at-t---cingular" : formatSEO = "att-cingular"
				case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
				case "sprint---nextel" : formatSEO = "sprint-nextel"
				case "u-s--cellular" : formatSEO = "us-cellular"
				case "antennas-parts" : formatSEO = "antennas"
				case "faceplates" : formatSEO = "covers-films-faceplates"
				case "bling-kits---charms" : formatSEO = "charms-bling-kits"
				case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
				case "hands-free" : formatSEO = "headsets-hands-free-kits"
				case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
				case "holsters-belt-clips" : formatSEO = "holsters-phone-holders"
				case "leather-cases" : formatSEO = "cases-pouches-skins"
				case "full-body-protectors" : formatSEO = "protective-films"
			end select
		case else
			select case formatSEO
				case "sidekick" : formatSEO = "t-mobile-sidekick"
				case "at-t---cingular" : formatSEO = "att-cingular"
				case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
				case "sprint---nextel" : formatSEO = "sprint-nextel"
				case "u-s--cellular" : formatSEO = "us-cellular"
				case "antennas-parts" : formatSEO = "antennas"
				case "faceplates" : formatSEO = "faceplates-screen-protectors"
				case "bling-kits---charms" : formatSEO = "charms-bling-kits"
				case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
				case "hands-free" : formatSEO = "headsets-hands-free-kits"
				case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
				case "holsters-belt-clips" : formatSEO = "holsters-holders"
				case "leather-cases" : formatSEO = "cases-pouches-skins"
			end select
	end select
end function

function brandSEO(val)
	val = cStr(val)
	select case val
		case "1" : brandSEO = "utstarcom-cell-phone-accessories.asp"
		case "2" : brandSEO = "sony-ericsson-cell-phone-accessories.asp"
		case "3" : brandSEO = "kyocera-cell-phone-accessories.asp"
		case "4" : brandSEO = "lg-cell-phone-accessories.asp"
		case "5" : brandSEO = "motorola-cell-phone-accessories.asp"
		case "6" : brandSEO = "nextel-cell-phone-accessories.asp"
		case "7" : brandSEO = "nokia-cell-phone-accessories.asp"
		case "8" : brandSEO = "panasonic-cell-phone-accessories.asp"
		case "9" : brandSEO = "samsung-cell-phone-accessories.asp"
		case "10" : brandSEO = "sanyo-cell-phone-accessories.asp"
		case "11" : brandSEO = "siemens-cell-phone-accessories.asp"
		case "12" : brandSEO = "universal-cell-phone-accessories.asp"
		case "13" : brandSEO = "nec-cell-phone-accessories.asp"
		case "14" : brandSEO = "blackberry-cell-phone-accessories.asp"
		case "15" : brandSEO = "other-cell-phone-accessories.asp"
		case "16" : brandSEO = "palm-cell-phone-accessories.asp"
		case "17" : brandSEO = "iphone-accessories.asp"
		case "18" : brandSEO = "pantech-cell-phone-accessories.asp"
		case "19" : brandSEO = "sidekick-cell-phone-accessories.asp"
		case "20" : brandSEO = "htc-cell-phone-accessories.asp"
		case "28" : brandSEO = "casio-cell-phone-accessories.asp"
		case "29" : brandSEO = "huawei-cell-phone-accessories.asp"
		case "30" : brandSEO = "zte-cell-phone-accessories.asp"
		case else : brandSEO = "brand.asp?brandid=" & val
	end select
end function

function nameSEO(val)
	select case val
		case "Antennas/Parts" : nameSEO = "Antennas and Parts"
		case "Hands-Free" : nameSEO = "Hands-Free and Headsets"
		case "Holsters/Belt Clips" : nameSEO = "Holsters/Holders"
		case "Key Pads/Flashing" : nameSEO = "Keypads and Flashing Keypads"
		case "Faceplates"
			select case Application("SiteId")
				case 1 : nameSEO = "Covers"
				case else : nameSEO = "Covers and Faceplates"
			end select
		case "Full Body Protectors" : nameSEO = "Body Guards/Skins"
		case "Data Cable & Memory" : nameSEO = "Data Cable & Memory Cards"
		case else : nameSEO = val
	end select
end function

function singularSEO(val)
	select case val
		case "Antennas/Parts" : singularSEO = "Antenna"
		case "Holsters/Belt Clips" : singularSEO = "Holster/Holder"
		case "Key Pads/Flashing" : singularSEO = "Keypad"
		case "Data Cable & Memory" : singularSEO = "Data Cable"
		case "Chargers" : singularSEO = "Charger"
		case "Faceplates" : singularSEO = "Faceplate/Screen Protector"
		case "Leather Cases" : singularSEO = "Case"
		case "Full Body Protectors" : singularSEO = "Body Guard/Skin"
		case else : singularSEO = val
	end select
end function

function categorySEO(val)
	val = cStr(val)
	select case val
		case "1": categorySEO = "cell-phone-batteries.asp"
		case "2": categorySEO = "cell-phone-chargers-cables.asp"
		case "3": categorySEO = "cell-phone-faceplates-covers.asp"
		case "5": categorySEO = "bluetooth-headsets-handsfree.asp"
		case "6": categorySEO = "cell-phone-holsters-holders-mounts.asp"
		case "7": categorySEO = "cell-phone-pouches-carrying-cases.asp"
		case "12": categorySEO = "cell-phone-signal-boosters.asp"
		case "13": categorySEO = "cell-phone-memory-cards-readers.asp"
		case "15": categorySEO = "cell-phones-tablets.asp"
		case "18": categorySEO = "cell-phone-screen-protectors-skins.asp"
		case "22": categorySEO = "cell-phone-bundle-packs.asp"
		case "1030": categorySEO = "cell-phone-design-faceplates-covers.asp"
		case "1040": categorySEO = "cell-phone-heavy-duty-hybrid-cases.asp"
		case "1050": categorySEO = "cell-phone-rhinestone-bling-cases.asp"
		case "1060": categorySEO = "cell-phone-rubberized-hard-covers.asp"
		case "1070": categorySEO = "cell-phone-silicone-cases.asp"
		case "1080": categorySEO = "cell-phone-tpu-crystal-candy-cases.asp"
		case "1090": categorySEO = "cell-phone-replacement-and-extended-batteries.asp"
		case "1100": categorySEO = "cell-phone-universal-batteries.asp"
		case "1110": categorySEO = "cell-phone-car-chargers.asp"
		case "1120": categorySEO = "cell-phone-desktop-cradles-docks.asp"
		case "1130": categorySEO = "cell-phone-home-chargers.asp"
		case "1140": categorySEO = "cell-phone-spare-battery-chargers.asp"
		case "1150": categorySEO = "cell-phone-universal-chargers.asp"
		case "1360": categorySEO = "cell-phone-data-cables.asp"
		case "1380": categorySEO = "battery-powered-cell-phone-chargers.asp"
		case "1160": categorySEO = "cell-phone-horizontal-cases-pouches.asp"
		case "1170": categorySEO = "cell-phone-vertical-cases-pouches.asp"
		case "1180": categorySEO = "sleeves-portfolios.asp"
		case "1190": categorySEO = "cell-phone-holsters.asp"
		case "1220": categorySEO = "cell-phone-bluetooth-car-kits.asp"
		case "1230": categorySEO = "cell-phone-bluetooth-headsets.asp"
		case "1240": categorySEO = "speakers.asp"
		case "1250": categorySEO = "cell-phone-wired-headsets.asp"
		case "1260": categorySEO = "decal-skin.asp"
		case "1270": categorySEO = "music-skins.asp"
		case "1280": categorySEO = "cell-phone-screen-surface-protection.asp"
		case "1290": categorySEO = "cell-phone-starter-kit.asp"
		case "1300": categorySEO = "cell-phone-power-pack.asp"
		case else : categorySEO = "category.asp?categoryid=" & val
	end select
end function

function optCarrierName(val)
	select case lcase(val)
		case "at&t / cingular"
			optCarrierName = "AT&T"
		case "sprint / nextel"
			optCarrierName = "Sprint"
		case "verizon wireless"
			optCarrierName = "Verizon"
		case "boost mobile / southern linc"
			optCarrierName = "Boost Mobile"
		case "cricket wireless"
			optCarrierName = "Cricket"
		case "metropcs"
			optCarrierName = "Metro PCS"
		case else
			optCarrierName = val
	end select
end function

function optBrandName(val)
	select case lcase(val)
		case "apple"
			optBrandName = "iPhone"
		case else
			optBrandName = val
	end select
end function

function getCssDateParam(pathToCss)
'	session("errorSQL") = "getCssDateParam(" & pathToCss & ")"
	dim dtTemp : dtTemp = year(date) & right("00" & month(date), 2) & right("00" & day(date), 2)
	if pathToCss <> "" then
		dtTemp = getFileLastModified(pathToCss)
	end if

	dtTemp = "?v=" & dtTemp
	getCssDateParam = dtTemp
end function

function getFileLastModified(s)
	dim dtTemp : dtTemp = ""
	dim fsTemp : set fsTemp = CreateObject("Scripting.FileSystemObject")
	fileAbsolutePath = server.mappath(s)

	if fsTemp.FileExists(fileAbsolutePath) then
		set oFile = fsTemp.GetFile(fileAbsolutePath)
		dtModified = oFile.DateLastModified
		dtTemp = year(dtModified)
		dtTemp = dtTemp & right("00" & month(dtModified), 2)
		dtTemp = dtTemp & right("00" & day(dtModified), 2)
		dtTemp = dtTemp & right("00" & hour(dtModified), 2)
		dtTemp = dtTemp & right("00" & minute(dtModified), 2)
		dtTemp = dtTemp & right("00" & second(dtModified), 2)
	end if

	getFileLastModified	= dtTemp
end function

function getDbRows(pSql)
	Dim tRet	:	tRet = NULL

	'CursorType : adOpenForwardOnly 0 , LockType adLockReadOnly 1
	set oRsTemp = Server.CreateObject("ADODB.Recordset")
	oRsTemp.Open pSql, oConn, 0, 1

	If oRsTemp.EOF Then
		tRet = NULL
	Else
		tRet = oRsTemp.GetRows()
	End If

	oRsTemp.Close : Set oRsTemp = Nothing

	getDbRows = tRet
end function


'filters two dimentional recordset array
function filter2DRS(pArr, pCol, pCompareValue)
	dim nIdx, nRows, nRowFound
	nRowFound = -1
	if not isnull(pArr) and isarray(pArr) then
		redim arrReturn(ubound(pArr,1), nRowFound)
		for nRows=0 to ubound(pArr, 2)
			strColumns = ""
			if lcase(cstr(pCompareValue)) = lcase(cstr(pArr(pCol, nRows))) then
				nRowFound = nRowFound + 1
				redim preserve arrReturn(ubound(pArr,1), nRowFound)
				for nCol=0 to ubound(pArr,1)
					arrReturn(nCol, nRowFound) = pArr(nCol, nRows)
				next
			end if
		next
		filter2DRS = arrReturn
		exit function
	end if
	filter2DRS = array(-1, -1)
end function

function sort2DRS(byref pArr, pCompareCol)
	if not isnull(pArr) and isarray(pArr) then
		arrReturn = pArr
		redim arrSwapTemp(ubound(arrReturn,1), 0)
		for i = 0 to ubound(arrReturn, 2)
			for j = i + 1 to ubound(arrReturn, 2)
				if cstr(lcase(arrReturn(pCompareCol, i))) > cstr(lcase(arrReturn(pCompareCol, j))) then
					for nCol=0 to ubound(arrReturn, 1)
						arrSwapTemp(nCol, 0) = arrReturn(nCol, i)
					next
					for nCol=0 to ubound(arrReturn, 1)
						arrReturn(nCol, i) = arrReturn(nCol, j)
					next
					for nCol=0 to ubound(arrReturn, 1)
						arrReturn(nCol, j) = arrSwapTemp(nCol, 0)
					next
				end if
			next
		next
		sort2DRS = arrReturn
	end if
end function

sub createStitchByArray(pImgWidth, pImgHeight, pImgPath1, pImgPath2, pStitchImgPath, byref pArr, idxImage)
	if not isnull(pArr) then
		set fso = CreateObject("Scripting.FileSystemObject")
		set oImg = Server.CreateObject("Persits.Jpeg")
		set oJpeg = Server.CreateObject("Persits.Jpeg")
		oJpeg.Quality = 60
		oJpeg.Interpolation = 1
		nRecordCount = ubound(pArr,2)+1
		imgCanvasWidth = pImgWidth * nRecordCount
		isFileDeleted = false

		if fso.FileExists(pStitchImgPath) then
			set imgFile = fso.getFile(pStitchImgPath)
			session("errorSQL2") = "pStitchImgPath:" & pStitchImgPath & "<br>size:" & imgFile.size
			on error resume next
				oImg.Open pStitchImgPath
				if err.number <> 0 then
					fso.DeleteFile(pStitchImgPath)
					isFileDeleted = true
				end if
			On Error Goto 0
			if not isFileDeleted then
				if isnumeric(imgCanvasWidth) and isnumeric(oImg.Width) then
					if cint(imgCanvasWidth) <> cint(oImg.Width) then		'if stitch image contains wrong number of item images
						fso.DeleteFile(pStitchImgPath)
						isFileDeleted = true
					end if
				end if
			end if
		end if
		if not isFileDeleted and fso.FileExists(pStitchImgPath) then
			set fsTemp = fso.GetFile(pStitchImgPath)
			if datediff("n", fsTemp.DateLastmodified, now) > 60 then		'if stitch image is too old
				fso.DeleteFile(pStitchImgPath)
			end if
		end if

		if pStitchImgPath <> "" and not fso.FileExists(pStitchImgPath) then
			oJpeg.New imgCanvasWidth, pImgHeight, &HFFFFFF
			imgX = 0
			imgY = 0
			for nRows=0 to ubound(pArr,2)
				imgFullPath1 = pImgPath1 & "\" & pArr(idxImage, nRows)
				imgFullPath2 = pImgPath2 & "\" & pArr(idxImage, nRows)
				if not fso.FileExists(imgFullPath1) then
					if fso.FileExists(imgFullPath2) then
						oImg.Open imgFullPath2
						oImg.Height = pImgHeight
						oImg.Width = pImgWidth
						oImg.Save imgFullPath1
					end if
				end if
				if len(pArr(idxImage, nRows)) > 0 then
					if fso.FileExists(imgFullPath1) then
						oImg.Open imgFullPath1
						oImg.Height = pImgHeight
						oImg.Width  = pImgWidth
'						oImg.Sharpen 1, 101
						oJpeg.Canvas.DrawImage X + (imgX)/2, Y + (imgY)/2, oImg
						imgX = imgX + (pImgWidth*2)
					end if
				end if
			next
			on error resume next
				oJpeg.Save pStitchImgPath
				errorLap = 0
				do while err.number <> 0
					errorLap = errorLap + 1
					err.Clear
					oJpeg.Save pStitchImgPath
					if errorLap = 200 then exit do
				loop
			On Error Goto 0
		end if
	end if
end sub

sub printPixel(tagPostion)
'	set objConn = GetNewConn()

	strBrandId = obj2Str(brandid)
	strModelId = obj2Str(modelid)
	strCategoryId = obj2Str(categoryid)
	strSubCategoryId = obj2Str(subcategoryid)
	strCarrierId = obj2Str(carrierid)
	strIsPhone = obj2Str(phoneOnly)
	strGenre = obj2Str(musicSkinGenre)
	strVendorOrder = Ck("vendororder")
	strPath = GetPhysicalAbsolutePath()
	strSiteID = Application("SiteId")

	makeNull = "null"

	if Request.ServerVariables("SERVER_PORT_SECURE")="1" then useHttp = "https" else useHttp = "http"
	if isId(strBrandId) then strBrandId = DbId( strBrandId) else strBrandId = makeNull
	if isId(strModelId) then strModelId = DbId( strModelId) else strModelId = makeNull
	if isId(strCategoryId) then strCategoryId = DbId( strCategoryId) else strCategoryId = makeNull
	if isId(strSubCategoryId) then strSubCategoryId = DbId( strSubCategoryId) else strSubCategoryId = makeNull
	if isId(strCarrierId) then strCarrierId = DbId( strCarrierId) else strCarrierId = makeNull
	if isId(strIsPhone) then strIsPhone = DbNum( strIsPhone) else strIsPhone = makeNull
	if strGenre <> "" then strGenre = DbStr( strGenre) else strGenre = makeNull
	if strVendorOrder <> "" then strVendorOrder = DbStr( strVendorOrder) else strVendorOrder = makeNull
	if not IsStaging() then strLive = "	and isLive = 1" & vbcrlf else strLive = ""

	sql = "exec sp_getPagePixels " & vbcrlf & _
			"@siteID = " & strSiteID & ", " & vbcrlf & _
			"@path = '%" & strPath & "%', " & vbcrlf & _
			"@tagPosition = " & tagPostion & ", " & vbcrlf & _
			"@brandID = " & strBrandId & ", " & vbcrlf & _
			"@modelID = " & strModelId & ", " & vbcrlf & _
			"@typeID = " & strCategoryId & ", " & vbcrlf & _
			"@subTypeID = " & strCategoryId & ", " & vbcrlf & _
			"@carrierID = " & strCarrierId & ", " & vbcrlf & _
			"@isPhone = " & strIsPhone & ", " & vbcrlf & _
			"@genre = " & strGenre & ", " & vbcrlf & _
			"@vendorCode  = " & strVendorOrder
'	session("printPixel") = sql
'	response.write vbNewLine & vbNewLine & "<pre>" & sql & "</pre>" & vbNewLine & vbNewLine
'	response.End()
'	set rsPixel = objConn.execute(sql)
	set rsPixel = oConn.execute(sql)
	do until rsPixel.eof
		eventText = rsPixel("eventText")
		eventText = replace(eventText, "{http}", useHttp)
		if not isnull(brandid) then eventText = replace(eventText, "{brandid}", brandid)
		if not isnull(categoryid) then eventText = replace(eventText, "{categoryid}", categoryid)
'		if not isnull(subcategoryid) then eventText = replace(eventText, "{subcategoryid}", subcategoryid)
		if not isnull(categoryid) then eventText = replace(eventText, "{subcategoryid}", categoryid)
		if not isnull(modelid) then eventText = replace(eventText, "{modelid}", modelid)
		if not isnull(nOrderId) then eventText = replace(eventText, "{orderid}", nOrderId)
		if not isnull(nOrderSubTotal) then eventText = replace(eventText, "{orderSubTotal}", nOrderSubTotal)
		if not isnull(nOrderGrandTotal) then eventText = replace(eventText, "{orderGrandTotal}", nOrderGrandTotal)
		if not isnull(nOrderTax) then eventText = replace(eventText, "{orderTax}", nOrderTax)

		if strSiteID = 3 or strSiteID = 1 then
			if not isnull(nOrdershippingfee) then eventText = replace(eventText, "{shippingFee}", nOrdershippingfee)
		else
			if not isnull(nShipFee) then eventText = replace(eventText, "{shippingFee}", nShipFee)
		end if

		if instr(strPath, "/product") > 0 then
			if not isnull(itemid) then eventText = replace(eventText, "{itemid}", itemid)
		else
			eventText = replace(eventText, "{itemid}", "")
		end if

		eventText = replace(eventText, "{ga_conv_items}", ga_conv_items)
		eventText = replace(eventText, "{ga_conv_img_items}", ga_conv_img_items)

		response.write eventText & vbNewLine & vbNewLine
		rsPixel.movenext
	loop
end sub




function dateOnly(dateVal)
	session("errorSQL2") = "dateOnly:" & dateVal
	if instr(dateVal," ") > 0 then
		dateVal = left(dateVal,instr(dateVal," ") - 1)
	end if
	if isdate(dateVal) then
		dateOnly = cdate(dateVal)
	else
		dateOnly = dateVal
	end if
end function

function ds(strValue)
	if len(strValue) > 0 then
		strContent = trim(replace(strValue,chr(10),""))
		strContent = replace(strContent,vbcrlf,"")
		strContent = replace(strContent,chr(13),"")
		if not isNull(strContent) and strContent <> "" then
'			strContent = replace(strContent,">","&gt;")
'			strContent = replace(strContent,"<","&lt;")
'			strContent = replace(strContent,"'","&apos;")
'			strContent = replace(strContent,"""","&#x22;")
'			strContent = replace(strContent,")","&#x29;")
'			strContent = replace(strContent,"(","&#x28;")

			strContent = replace(strContent,"%26%2343%3B1","+")
			strContent = replace(strContent,"&#43;","+")
			strContent = replace(strContent,"--","-")
			strContent = replace(strContent,"/*","")
			strContent = replace(strContent,"*/","")
			strContent = replace(strContent,"xp_","")
			strContent = replace(strContent,Chr(39),"''")
			strContent = replace(strContent,"<","&lt;")
			strContent = replace(strContent,">","&gt;")
		end if
	end if
	ds = strContent
end function

function prepStr(strVal)
	if isnull(strVal) or len(strVal) < 1 then strVal = ""
	prepStr = trim(ds(strVal))
end function

function prepVal(strVal)
	if isnull(strVal) or len(strVal) < 1 then strVal = ""
	prepVal = trim(strVal)
end function

function prepInt(intVal)
	intValAdjusted = trim(intVal)
	if isnull(intValAdjusted) or len(intValAdjusted) < 1 or not isnumeric(intValAdjusted) then intValAdjusted = 0
	prepInt = cdbl(ds(intValAdjusted))
end function









'safe check if value is not empty [knguyen/20110509]
function hasLen(objValue)
	hasLen=true
	if isnull(objValue) then
		hasLen=false
	else
		if len(trim(objValue))=0 then
			hasLen=false
		end if
	end if
end function

'safe check if value is empty [knguyen/20110509]
function hasNoLen(objValue)
	hasNoLen=not hasLen(objValue)
end function

'polish string values before use [knguyen/20110509]
function obj2Str(objValue)
	obj2Str=EMPTY_STRING

	if hasLen(objValue) then
		obj2Str=trim(cstr(objValue))
	end if
end function

'escape string value for eval
function EvalStr(objValue)
	EvalStr=""""""
	if hasLen(objValue) then
		EvalStr=""""&replace(trim(cstr(objValue)),"""","""""")&"""" 'handles the db quotes
	end if
end function

'escape string value for db
function DbStr(objValue)
	DbStr="''"
	if hasLen(objValue) then
		DbStr="'"&replace(trim(cstr(objValue)),"'","''")&"'" 'handles the db quotes
	end if
end function

function DbStrNil(objValue)
	DbStrNil = DbStr(objValue)
	if DbStrNil="''" then DbStrNil = "null"
end function

'validate Id value for db
function DbId( strValue)
	DbId=EMPTY_STRING
	if isId( strValue) then
		DbId=strValue
	end if
end function

function DbIdNil( strValue)
	DbIdNil = DbId( strValue)
	if HasNoLen( DbIdNil) then DbIdNil = "null"
end function

'validate Id value for db
function DbNum( strValue)
	DbNum=EMPTY_STRING
	if hasLen( strValue) then
		if isnumeric( strValue) then
			DbNum=strValue
		end if
	end if
end function

'validate Id value for db
function DbNumNil( strValue)

	DbNumNil = DbNum( strValue)
	if HasNoLen( DbNumNil) then DbNumNil = "null"
end function

'escape string value for javascript
function JsStr( objValue)
	JsStr="''"
	if hasLen(objValue) then
		JsStr="'"& replace(trim(cstr(objValue)),"'","\'") &"'" 'handles the js quotes
	end if
end function

'escape string value for javascript
function JsNum( objValue)
	JsNum = DbNum( objValue) 'may need to change this if we need to force to 0
end function

'safe check if value is an id[knguyen/20110509]
function isId( strValue)
	isId = false
	if hasLen( strValue) and isNumeric( strValue) then
		if clng( strValue)>=0 then isId = true
	end if
end function

'crash-proof case-insenstive compare
function IsEqual( byval strValueX, byval strValueY)
	if IsObject( strValueX) or IsObject( strValueY) then 'cannot compare objects
		IsEqual = false
	else
		IsEqual = ( lcase( Obj2Str( strValueX)) = lcase( Obj2Str( strValueY)))
	end if
end function

'safe check if value is an id[knguyen/20110509]
function StartsWith( byval strString, byval strStartStringToCheck)
	StartsWith = haslen( strStartStringToCheck) and ( instr(1, strString, strStartStringToCheck, vbTextCompare) = 1)
end function

sub SafeOverride( byref strVariable, byval strValue)
	if hasLen( strValue) then strVariable = strValue
end sub

function GetNewConn()
'	dim objConn: set objConn=Server.CreateObject("ADODB.Connection")
'	dim strConnStr: strConnStr = obj2Str(Session("ConnectionStringSqlServer"))
'	if not hasLen( strConnStr) then strConnStr=obj2Str(Session("ConnectionString"))
'	objConn.Open strConnStr
	dim objConn
	call getDBConn(objConn)
	set GetNewConn=objConn
end function

sub CloseConn( byref objConn)
	if IsObject( objConn) then
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if

	if IsObject(weUtil) then
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if
end sub

sub checkConnObject( byref objConn)
	response.write "<!--"
	response.write "IsObject(objConn):" & IsObject(objConn) & vbNewLine
	if IsObject(objConn) then
		if not objConn is nothing then
			response.write "objConn is not nothing" & vbNewLine
			response.write "objConn.State:" & objConn.State & vbNewLine
		else
			response.write "objConn is nothing" & vbNewLine
		end if
	end if

	response.write "IsObject(weUtil):" & IsObject(weUtil) & vbNewLine
	if IsObject(weUtil) then
		if not weUtil is nothing then
			response.write "weUtil is not nothing" & vbNewLine
		else
			response.write "weUtil is nothing" & vbNewLine
		end if
	end if

	response.write "-->"
end sub

sub responseRedirect(url)
	call CloseConn(oConn)
	response.redirect(url)
	response.end
end sub

sub responseEnd()
	call CloseConn(oConn)
	response.end
end sub

sub CloseRs( byref objRs)
	if IsObject( objRs) then
		if not objRs is nothing then
			objRs.Close()
			set objRs = nothing
		end if
	end if
end sub

function DbSingleValue( byref strSql)
	dim objConn: set objConn=GetNewConn()
	dim objRs: set objRs = objConn.execute( strSql)

	if not objRs.eof then
		DbSingleValue = obj2Str(objRs.Fields.Item(0).Value)
	else
		DbSingleValue = EMPTY_STRING
	end if

	objRs.close()
	set objRs=nothing

	call CloseConn( objConn)
end function

function DbList( byref strSql)
	dim objConn: set objConn=GetNewConn()
	dim objRs: set objRs = objConn.execute( strSql)

	dim lstReturnValue: set lstReturnValue = new DynamicArray

	while not objRs.eof
		lstReturnValue.Add( obj2Str( objRs.Fields.Item(0).Value))
		objRs.MoveNext()
	wend

	objRs.close()
	set objRs=nothing

	call CloseConn( objConn)

	set DbList = lstReturnValue
end function




function ApplyPageAttribute( byval strText, byref dicReplaceAttribute)
	ApplyPageAttribute=strText
	for each strKey in dicReplaceAttribute
		if instr(ApplyPageAttribute,"{"&strKey&"}") > 0 then
			'session("errorSQL") = "ApplyPageAttribute:" & ApplyPageAttribute
			ApplyPageAttribute = replace( ApplyPageAttribute, "{"&strKey&"}", dicReplaceAttribute( strKey))
		end if
	next
end function

sub LookupSeoAttributes()
	'DBGMODE=true
	'make the call and override existing meta values as needed

	dim dicSeo: set dicSeo = GetSeoAttributes( dicSeoAttributeInput)
	call SafeOverride( SEtitle, ApplyPageAttribute( dicSeo.Item("Title"), dicReplaceAttribute))
	call SafeOverride( SEdescription, ApplyPageAttribute( dicSeo.Item("Description"), dicReplaceAttribute))
	call SafeOverride( SEkeywords, ApplyPageAttribute( dicSeo.Item("Keywords"), dicReplaceAttribute))

	if topText = "" then call SafeOverride( topText, ApplyPageAttribute( dicSeo.Item("TopText"), dicReplaceAttribute))
	if bottomText = "" then call SafeOverride( bottomText, ApplyPageAttribute( dicSeo.Item("BottomText"), dicReplaceAttribute))
	'call SafeOverride( strH1, ApplyPageAttribute( dicSeo.Item("H1"), dicReplaceAttribute))
	'call SafeOverride( strH2, ApplyPageAttribute( dicSeo.Item("H2"), dicReplaceAttribute))
	'call SafeOverride( strBreadcrumb, ApplyPageAttribute( dicSeo.Item("BreadCrumbText"), dicReplaceAttribute))
	call SafeOverride( strAltText, ApplyPageAttribute( dicSeo.Item("AltText"), dicReplaceAttribute))
	'call DbgGlobal( "SEtitle")
	set dicSeo = nothing
	set dicReplaceAttribute =  nothing
	set dicSeoAttributeInput =  nothing
end sub

const SQL_SOFTMATCH_END = "null) is null"

function GetSeoAttributes( byref dicSeoAttributeInput)
	dim objConn: set objConn=GetNewConn()

	'response.end
	dim strBrandId: strBrandId = obj2Str( dicSeoAttributeInput( "BrandId"))
	dim strModelId: strModelId = obj2Str( dicSeoAttributeInput( "ModelId"))
	dim strCategoryId: strCategoryId = obj2Str( dicSeoAttributeInput( "CategoryId"))
	dim strCarrierId: strCarrierId = obj2Str( dicSeoAttributeInput( "CarrierId"))
	dim strIsPhone: strIsPhone = obj2Str( dicSeoAttributeInput( "IsPhone"))

	if isId( strBrandId) then strBrandId = DbId( strBrandId) &") = "& DbId( strBrandId) else strBrandId = SQL_SOFTMATCH_END
	if isId( strModelId) then strModelId = DbId( strModelId) &") = "& DbId( strModelId) else strModelId = SQL_SOFTMATCH_END
	if isId( strCategoryId) then strCategoryId = DbId( strCategoryId) &") = "& DbId( strCategoryId) else strCategoryId = SQL_SOFTMATCH_END
	if isId( strCarrierId) then strCarrierId = DbId( strCarrierId) &") = "& DbId( strCarrierId) else strCarrierId = SQL_SOFTMATCH_END
	if isId( strIsPhone) then strIsPhone = DbNum( strIsPhone) &") = "& DbNum( strIsPhone) else strIsPhone = SQL_SOFTMATCH_END

	dim dicReturnValue: set dicReturnValue = CreateObject("Scripting.Dictionary")

	'--db-driven seo-logic [knguyen/20110509]
	dim strSql: strSql = "select top 1 [Title], [Description], [Keywords], [H1], [H2], [TopText], [BottomText], [AltText], [BreadCrumbText]" &_
" from MetaTags with(nolock)" & _
" where [SiteId]="&DbId( obj2Str( Application("SiteId"))) &_
" and isnull( [PhysicalAbsolutePath], " & DbStr(GetPhysicalAbsolutePath())&") = "&DbStr(GetPhysicalAbsolutePath()) &_
" and isnull( [TypeId],"& strCategoryId &_
" and isnull( [BrandId],"& strBrandId &_
" and isnull( [ModelId],"& strModelId &_
" and isnull( [CarrierId],"& strCarrierId &_
" and isnull( [IsPhone],"& strIsPhone &_
" order by [PhysicalAbsolutePath] desc," &_
" case when [TypeId] is null then 0 else 1 end" &_
" + case when [BrandId] is null then 0 else 1 end" &_
" + case when [ModelId] is null then 0 else 1 end" &_
" + case when [CarrierId] is null then 0 else 1 end" &_
" + case when [IsPhone] is null then 0 else 1 end desc"
	if DBGMODE then response.write "<pre>" & strSql & "</pre>": response.end

	dim objRsMeta: set objRsMeta = Server.CreateObject("ADODB.Recordset")
	objRsMeta.open strSql, objConn, 0, 1 ' adOpenForwardOnly, adLockReadOnly 'assumes existing connection (pooled)
	if not objRsMeta.eof then

		dicReturnValue( "Title") = obj2Str(objRsMeta("Title").Value)
		dicReturnValue( "Description") = obj2Str(objRsMeta("Description").Value)
		dicReturnValue( "Keywords") = obj2Str(objRsMeta("Keywords").Value)
		dicReturnValue( "TopText") = obj2Str(objRsMeta("TopText").Value)
		dicReturnValue( "BottomText") = obj2Str(objRsMeta("BottomText").Value)
		dicReturnValue( "H1") = obj2Str(objRsMeta("H1").Value)
		dicReturnValue( "H2") = obj2Str(objRsMeta("H2").Value)
		dicReturnValue( "AltText") = obj2Str(objRsMeta("AltText").Value)
		dicReturnValue( "BreadCrumbText") = obj2Str(objRsMeta("BreadCrumbText").Value)
	else
		dicReturnValue( "Title") = EMPTY_STRING
		dicReturnValue( "Description") = EMPTY_STRING
		dicReturnValue( "Keywords") = EMPTY_STRING
		dicReturnValue( "TopText") = EMPTY_STRING
		dicReturnValue( "BottomText") = EMPTY_STRING
		dicReturnValue( "H1") = EMPTY_STRING
		dicReturnValue( "H2") = EMPTY_STRING
		dicReturnValue( "AltText") = EMPTY_STRING
		dicReturnValue( "BreadCrumbText") = EMPTY_STRING
	end if

	objRsMeta.close()
	set  objRsMeta=nothing

	set GetSeoAttributes = dicReturnValue
	call CloseConn( objConn)
end function


function GetContentEventText( byref dicPageAttributeInput)
	'response.end
	dim objConn: set objConn=GetNewConn()
	dim strBrandId: strBrandId = obj2Str( dicPageAttributeInput( "BrandId"))
	dim strModelId: strModelId = obj2Str( dicPageAttributeInput( "ModelId"))
	dim strCategoryId: strCategoryId = obj2Str( dicPageAttributeInput( "CategoryId"))
	dim strSubCategoryId: strSubCategoryId = obj2Str( dicPageAttributeInput( "SubCategoryId"))
	dim strCarrierId: strCarrierId = obj2Str( dicPageAttributeInput( "CarrierId"))
	dim strIsPhone: strIsPhone = obj2Str( dicPageAttributeInput( "IsPhone"))
	dim strGenre: strGenre = obj2Str( dicPageAttributeInput( "Genre"))
	dim strVendorOrder: strVendorOrder = Ck("vendororder")

	if isId( strBrandId) then strBrandId = DbId( strBrandId) &") = "& DbId( strBrandId) else strBrandId = SQL_SOFTMATCH_END
	if isId( strModelId) then strModelId = DbId( strModelId) &") = "& DbId( strModelId) else strModelId = SQL_SOFTMATCH_END
	if isId( strCategoryId) then strCategoryId = DbId( strCategoryId) &") = "& DbId( strCategoryId) else strCategoryId = SQL_SOFTMATCH_END
	if isId( strSubCategoryId) then strSubCategoryId = DbId( strSubCategoryId) &") = "& DbId( strSubCategoryId) else strSubCategoryId = SQL_SOFTMATCH_END
	if isId( strCarrierId) then strCarrierId = DbId( strCarrierId) &") = "& DbId( strCarrierId) else strCarrierId = SQL_SOFTMATCH_END
	if isId( strIsPhone) then strIsPhone = DbNum( strIsPhone) &") = "& DbNum( strIsPhone) else strIsPhone = SQL_SOFTMATCH_END
	if isId( strGenre) then strGenre = DbStr( strGenre) &") = "& DbStr( strGenre) else strGenre = SQL_SOFTMATCH_END
	if isId( strVendorOrder) then strVendorOrder = DbStr( strVendorOrder) &") = "& DbStr( strVendorOrder) else strVendorOrder = SQL_SOFTMATCH_END

	dim dicReturnValue: set dicReturnValue = CreateObject("Scripting.Dictionary")

	'--db-driven content event-logic [knguyen/20110520]
	strSql = 	" select [Name] as [Name], t.[Text] as [Value]" & vbcrlf & _
				" from ContentEvent e with(nolock)" & vbcrlf & _
				" inner join ContentEventText t with(nolock)" & vbcrlf & _
				" on t.[ContentEventTextId]=e.[ContentEventTextId] and t.[IsActive]=1 and e.[IsActive]=1" & vbcrlf & _
				" and getdate() between isnull( e.[StartDate], dateadd( dd, -1, getdate())) and isnull( e.[EndDate], dateadd( dd, 1, getdate()))" & vbcrlf & _
				" and [SiteId]="& DbId( obj2Str( Application("SiteId"))) & vbcrlf & _
				" and isnull( [PhysicalAbsolutePath], "& DbStr(GetPhysicalAbsolutePath()) &") = "& DbStr(GetPhysicalAbsolutePath()) & vbcrlf & _
				" and isnull( [TypeId],"& strCategoryId & vbcrlf & _
				" and isnull( [SubtypeId],"& strSubCategoryId & vbcrlf & _
				" and isnull( [BrandId],"& strBrandId & vbcrlf & _
				" and isnull( [ModelId],"& strModelId & vbcrlf & _
				" and isnull( [CarrierId],"& strCarrierId & vbcrlf & _
				" and isnull( [IsPhone],"& strIsPhone & vbcrlf & _
				" and isnull( [Genre],"& strGenre & vbcrlf & _
				" and isnull( [VendorOrder],"& strVendorOrder & vbcrlf & _
				" inner join ContentEventTextType c with(nolock)" & vbcrlf & _
				" on c.[ContentEventTextTypeId]=t.[ContentEventTextTypeId] and c.[IsActive]=1" & vbcrlf & _
				" order by [ContentEventId]"
	if DBGMODE then response.write "<pre>" & strSql & "</pre>"
'	response.write DbStr(GetPhysicalAbsolutePath()) & "<br>"
'	response.write "parentTypeID:" & parentTypeID & "<br>"
'	response.write "categoryid:" & categoryid & "<br>"

	dim objRsMeta: set objRsMeta = Server.CreateObject("ADODB.Recordset")
	objRsMeta.open strSql, objConn, 0, 1 ' adOpenForwardOnly, adLockReadOnly 'assumes existing connection (pooled)
	dim strName: strName = EMPTY_STRING
	dim strValue: strValue = EMPTY_STRING

	while not objRsMeta.eof ' stream all values into one string
		strName = obj2Str( objRsMeta("Name").Value)
		dicReturnValue( strName) = obj2Str( dicReturnValue( strName)) & obj2Str( objRsMeta("Value").Value)
		objRsMeta.MoveNext
	wend

	objRsMeta.close()
	set  objRsMeta=nothing
	call CloseConn( objConn)

	set GetContentEventText = dicReturnValue
end function

sub OutputPixel( byref dicContentEventText)
	if not isobject( dicContentEventText) then 'hack fallback logic to check for pixels
		dim dicPageAttributeInput: set dicPageAttributeInput = CreateObject("Scripting.Dictionary")
		set dicContentEventText = GetContentEventText( dicPageAttributeInput)
	end if
	'set known product attributes to replace out template placeholders
	dim dicTokenReplacement : set dicTokenReplacement = CreateObject("Scripting.Dictionary")
	dicTokenReplacement( "OrderId")=Qs("o") '{OrderId}
	dicTokenReplacement( "OrderGrandTotal")=Qs("d") '{OrderGrandTotal}
	dicTokenReplacement( "OrderSubTotal")=Qs("c") '{OrderSubTotal}

	if Request.ServerVariables("SERVER_PORT_SECURE")="1" then
		dicTokenReplacement( "Http")="https" '{Http}
	else
		dicTokenReplacement( "Http")="http"
	end if

	dim strPixel: strPixel = ApplyPageAttribute( obj2Str( dicContentEventText( "Pixel")), dicTokenReplacement)

	err.clear  '--clear false-positives
	on error resume next
	response.write( strPixel)
	if err.number<>0 and StartsWith( err.description, "type mismatch") then
		response.write( "<!-- CepNotInit -->")
		err.clear
	else
		response.write( "<!-- CepInit -->")
	end if
end sub

function IsStaging()
	IsStaging = instr( lcase( Request.ServerVariables("SERVER_NAME")) , "www.")=0
end function

sub PermanentRedirect( byval strTargetUrl) '--should move this somewhere global [knguyen/20110511]
	'Request.ServerVariables("SCRIPT_NAME")
	dim strEffTargetUrl: strEffTargetUrl = strTargetUrl

	if not IsStaging() then 'this is to not 301 redirect with the querystring whenver in www [knguyen/20110513]
		dim lstUrlPart: lstUrlPart = split( strTargetUrl, "?")
		dim strEffQs: strEffQs = ""
		strEffTargetUrl = lstUrlPart(0)

		if ubound( lstUrlPart)>0 then
			dim lstNvPair: lstNvPair = split( lstUrlPart(1), "&")
			for each strNvPair in lstNvPair
				if instr( strNvPair, "ec=")=0 and instr( strNvPair, "id=")=0 then
					if hasLen( strEffQs) then strEffQs=strEffQs&"&"
						strEffQs=strEffQs&strNvPair
				end if

			next
			if hasLen( strEffQs) then strEffTargetUrl = strEffTargetUrl & "?" & strEffQs
		end if
	end if

	call CloseConn(oConn)

	response.Status = "301 Moved Permanently"
	response.AddHeader "Location", strEffTargetUrl
	response.end
end sub

'create link with default title attribute
function GetLink( byval strUrl, byval strLabel)
	GetLink=GetLinkGeneral( strUrl, strLabel, false, false)
end function

function GetLinkPop( byval strUrl, byval strLabel)
	GetLinkPop=GetLinkGeneral( strUrl, strLabel, false, true)
end function


function GetXLink( byval strUrl, byval strLabel)
	GetXLink=GetLinkGeneral( strUrl, strLabel, true, true)
end function

function GetLinkGeneral( byval strUrl, byval strLabel, byval blnNoFollow, byval blnNewWindow) '--please do not use this function directly [knguyen/20110517]
	dim strNoFollow: strNoFollow="": if blnNoFollow then strNoFollow=" rel=""nofollow"""
	dim strNewWindow: strNewWindow="": if blnNewWindow then strNewWindow=" target=""_new"""
	GetLinkGeneral = "<a href="""&strUrl&""" title="""&HtmlEncode(TagStrip(strLabel))&""""&strNoFollow&strNewWindow&">"&strLabel&"</a>"
end function

function HtmlEncode( byval strValue)
	HtmlEncode=replace(replace(replace(replace(strValue, "<", "&lt;"), ">", "&gt;"), """", "&quot;"), "'", "&#39;")
end function

function strip_tags(strHTML)
    dim regEx
    Set regEx = New RegExp
    With regEx
        .Pattern = "<(.|\n)+?>"
        .IgnoreCase = true
        .Global = true
    End With
    strip_tags = regEx.replace(strHTML, "")
end function


function TagStrip( strHtml) 'targeted html tag strip
	dim lstTag: lstTag = Array( _
					"(<span[^>]*>)" _
					,"(</span>)" _
					,"(<br[^>]*>)" _
					,"(<br[^>]*/>)" _
					,"(<p[^>]*>)" _
					,"(</p>)" _
					,"(<em[^>]*>)" _
					,"(</em>)" _
					,"(<img[^>]*>)" _
					,"(<i[^>]*>)" _
					,"(</i>)" _
					,"(<b[^>]*>)" _
					,"(</b>)" _
					,"(<strong[^>]*>)" _
					,"(</strong>)" _
					,"(<ul[^>]*>)" _
					,"(</ul>)" _
					,"(<li[^>]*>)" _
					,"(</li>)" _
					,"(<font[^>]*>)" _
					,"(</font>)" _
					,"(<font[^>]*>)" _
					,"(</font>)" _
					,"(<table[^>]*>)" _
					,"(</table>)" _
					,"(<tbody[^>]*>)" _
					,"(</tbody>)" _
					,"(<tr[^>]*>)" _
					,"(</tr>)" _
					,"(<td[^>]*>)" _
					,"(</td>)" _
					,"(<div[^>]*>)" _
					,"(</div>)" _
					,"(<p[^>]*/>)" _
					,"(<a[^>]*>)" _
					,"(</a>)" _
					,"(<ol[^>]*>)" _
					,"(</ol>)" _
					,"(<sup[^>]*>)" _
					,"(</sup>)" _
					,"(<sub[^>]*>)" _
					,"(</sub>)")


	dim objRegExp, strOutput
	Set objRegExp = New Regexp

	objRegExp.IgnoreCase = True
	objRegExp.Global = True

	for each strTag in lstTag
		objRegExp.Pattern = strTag
		strHtml = objRegExp.Replace(strHtml, "")
	next

	set objRegExp = Nothing

	TagStrip = strHtml

end function

function GetAlphaNumeric( byval strText)
	GetAlphaNumeric=EMPTY_STRING
	if hasLen( strText) then
		dim intIndex, strNextChar
		for intIndex=1 to len(strText)
			strNextChar=mid(strText,intIndex,1)
			if ( lcase( strNextChar)>="a" and lcase( strNextChar)<="z") or _
				( lcase( strNextChar)>="0" and lcase( strNextChar)<="9") then
				GetAlphaNumeric = GetAlphaNumeric & strNextChar
			end if
		next
	end if
end function

dim dicCacheXQs: set dicCacheXQs = nothing '* optimization this way we do not reparse each time Qs is called

'querystring access with whitespace sanitization
function Qs( strKey)
	Qs=obj2Str( Request.QueryString( strKey))

	if HasNoLen( Qs) and IsUrlRewritePage() then
		if dicCacheXQs is nothing then
			dim strXQs: strXQs = EMPTY_STRING
			dim strHttpXRewriteUrl: strHttpXRewriteUrl = Request.ServerVariables("HTTP_X_REWRITE_URL")
			dim intPosition: intPosition = instr(1, strHttpXRewriteUrl, "?", VbTextCompare)
			if intPosition>1 and intPosition+1<=len( strHttpXRewriteUrl) then strXQs=mid( strHttpXRewriteUrl, intPosition+1, len( strHttpXRewriteUrl)-intPosition)
			dim objDelimitedList: set objDelimitedList = new DelimitedList
			objDelimitedList.Delimiter = "&"
			objDelimitedList.List = strXQs

			set dicCacheXQs = CreateObject("Scripting.Dictionary")
			dicCacheXQs.CompareMode = DIC_COMPARE_MODE_INSENSITIVE

			for each strKeyValue in objDelimitedList.AsArray
				dim lstKeyValue: lstKeyValue = split( strKeyValue, "=")
				if ubound( lstKeyValue)=1 then dicCacheXQs( lstKeyValue(0)) = lstKeyValue(1)
			next
		end if
		Qs=Obj2Str( dicCacheXQs( strKey))
	end if
end function

function Ck( byval strKey)
	Ck = EMPTY_STRING

	if not Request.Cookies is nothing then '--ensure cookie collection is valid
		if not Request.Cookies( strKey) is nothing then '--ensure cookie object is valid
			if not Request.Cookies( strKey).HasKeys then '--ensure cookie object is single-level
				Ck = Obj2Str( Request.Cookies( strKey))
			end if
		end if
	end if
end function

function CkSet( byval strKey, byval strValue)
	Response.Cookies( strKey) = strValue
end function

'form value access with whitespace sanitization
function Fv( strKey)
	Fv=obj2Str( Request.Form( strKey))
end function

'basic debug statement.. provide label and value
sub Dbg( byval strName, byval strValue)
	response.write( "<div style=""border: solid 1px #000000; background-color: #aaffff;"">"& strName & ": [" & strValue & "]</div>"&vbcrlf)
end sub

'quick debug statement (querystring version)
sub DbgQs( strName)
	call Dbg( strName, Request.QueryString( strName))
end sub

'quick debug statement (form post version)
sub DbgFs( strName)
	call Dbg( strName, Request.Form( strName))
end sub

'quick debug statement (global variable version)
sub DbgGlobal( byval strName)
	response.write( strName & ":[" & replace(eval( strName),"<","&lt;") & "]"&vbcrlf)
end sub

'gets the url path as developer sees from the code
function GetPhysicalAbsolutePath()
	GetPhysicalAbsolutePath=Request.ServerVariables("SCRIPT_NAME")
end function

'gets the url path as the visitor sees from the browser
function GetVirtualAbsolutePath()
	dim strHttpXRewriteUrl: strHttpXRewriteUrl = Request.ServerVariables("HTTP_X_REWRITE_URL")
	dim intPosition: intPosition = instr(1, strHttpXRewriteUrl, "?", VbTextCompare)
	GetVirtualAbsolutePath=strHttpXRewriteUrl

	if intPosition>1 then GetVirtualAbsolutePath=left( strHttpXRewriteUrl, intPosition-1)
end function

'gets domain host (without protocol segment, i.e. "http://"
function GetCurrentDomain()
	GetCurrentDomain = Request.ServerVariables("SERVER_NAME")
end function

'gets domain host with protocol segment, i.e. "http://" or "https://" depending on current security context
function GetCurrentDomainFull()
	dim strHttp: strHttp = EMPTY_STRING
	if Request.ServerVariables("SERVER_PORT_SECURE")="1" then
		strHttp="https"
	else
		strHttp="http"
	end if

	GetCurrentDomainFull = strHttp & "://" & GetCurrentDomain()
end function

if IsStaging() and Qs("_Test")="1" then
	call response.write( "<!--" & vbcrlf)

		'Request.ServerVariables("SCRIPT_NAME")
	call Dbg( "ServerVariables[SCRIPT_NAME]", GetPhysicalAbsolutePath())
	call DbgQs( "CategoryId")
	call DbgQs( "BrandId")
	call DbgQs( "ModelId")
	call DbgQs( "CarrierId")
	call DbgQs( "ItemId")
	call DbgQs( "PhoneOnly")
	call response.write( "-->")
end if

function GetPhysicalImagePath( byval strImageSubPath)
	dim strPhysicalImagePath: strPhysicalImagePath = Server.MapPath("/images/")

	if hasLen( strImageSubPath) then
		strImageSubPath = replace( strImageSubPath, "/", "\")
		if not StartsWith( strImageSubPath, "\") then strImageSubPath = "\" & strImageSubPath
	else
		strImageSubPath = EMPTY_STRING
	end if

	GetPhysicalImagePath = strPhysicalImagePath & strImageSubPath
end function

function IsUrlRewritePage()
	IsUrlRewritePage = ( not IsEqual( GetPhysicalAbsolutePath(), GetVirtualAbsolutePath()))
end function

function FormatDecimalInput( byval strValue)
	FormatDecimalInput = EMPTY_STRING
	if HasLen( strValue) then
		if IsNumeric( strValue) then
			FormatDecimalInput = FormatNumber( strValue, 2, -1, 0, 0)
		end if
	end if
end function

function FormatUsdOutput( byval strValue)
	if not HasLen( strValue) then strValue="0"
	if not IsNumeric( strValue) then strValue="0"
	FormatUsdOutput = FormatCurrency( strValue, 2, -1, 0, -1)
end function

function NewQueryString( byval strQueryString)
	set NewQueryString=new DelimitedList
	NewQueryString.Delimiter="&"
	while StartsWith( strQueryString, "?")
		strQueryString = mid( strQueryString, 2)
	wend
	NewQueryString.List = strQueryString
end function

function GetUtc()
	dim dtNow: dtNow = now()
	dim objShell: set objShell = CreateObject("WScript.Shell")
	dim intOffSetMin: intOffSetMin = objShell.RegRead( "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\TimeZoneInformation\ActiveTimeBias")
    GetUtc = dateadd( "n", intOffSetMin, dtNow)
end function

function FormatLongDateTime( dtValue)
	FormatLongDateTime = FormatDateTime( dtValue, VbLongDate) &" "& FormatDateTime( dtValue, VbLongTime)
end function

function OutputJsonHeader()
	call Response.AddHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT")
	call Response.AddHeader("Last-Modified", FormatLongDateTime( GetUtc()))
	call Response.AddHeader("Cache-Control", "no-cache, must-revalidate")
	call Response.AddHeader("Pragma", "no-cache")
	call Response.AddHeader("Content-Type", "application/json")
end function

function GetSiteId()
	GetSiteId = Obj2Str( Application("SiteId"))
end function

function weEmail(strTo,strFrom,strSubject,strBody)
	Dim objErrMail
	Set objErrMail = CreateObject("CDO.Message")
	With objErrMail
		.From = strFrom
		.To = strTo
		.Subject = strSubject
		.HTMLBody = strBody
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	End With
	Set objErrMail = nothing
end function

sub getCmsData(site_id, pBasepage, pRewriteURL, byref pMetaString)
'	response.write "pBasepage:" & pBasepage & "<br>"
'	response.write "pRewriteURL:" & pRewriteURL & "<br>"
	pMetaString = ""
	if inStr(pBasepage,"?") > 0 then pBasepage = left(pBasepage,inStr(pBasepage,"?")-1)
	if inStr(pRewriteURL,"?") > 0 then pRewriteURL = left(pRewriteURL,inStr(pRewriteURL,"?")-1)

	sql = "CMSGet '" & prepInt(site_id) & "', '" & prepStr(pBasepage) & "', '" & prepStr(pRewriteURL) & "', 1"
'	response.write sql
	set cmsRS = oConn.execute(sql)
	if not cmsRS.eof then
		pMetaString = updateGenericMeta(cmsRS("seTitle"))
		pMetaString = pMetaString & "!##!" & updateGenericMeta(cmsRS("seDescription"))
		pMetaString = pMetaString & "!##!" & updateGenericMeta(cmsRS("seKeywords"))
		pMetaString = pMetaString & "!##!" & updateGenericMeta(cmsRS("seTopText"))
		pMetaString = pMetaString & "!##!" & updateGenericMeta(cmsRS("seBottomText"))
		pMetaString = pMetaString & "!##!" & updateGenericMeta(cmsRS("seH1"))
	end if
end sub

function cmsData(curPage)
	if prepStr(pageTitle) = "" then pageTitle = pageName
	if instr(pageTitle,"brand-model-category-details_mp.asp") > 0 then pageTitle = "brand-model-category-details.asp"
	if curPage = "/index_mp.asp" then curPage = "/"
	if instr(curPage,"&strSort") > 0 then curPage = left(curPage,instr(curPage,"&strSort")-1)
	if instr(curPage,"?utm_source") > 0 then curPage = left(curPage,instr(curPage,"?utm_source")-1)
	sql = "select * from metaTags where SiteId = " & prepInt(siteID) & " and physicalAbsolutePath = '" & prepStr(curPage) & "'"
	set metaRS = oConn.execute(sql)

	if not metaRS.EOF then
		if metaRS("active") then
			sql = "select * from metaTags_default where SiteId = " & siteID & " and basePage = '" & SQLquote(pageTitle) & "'"
			set defaultMetaRS = oConn.execute(sql)

			if not defaultMetaRS.EOF then
				SEtitle = updateGenericMeta(defaultMetaRS("seTitle"))
				SEdescription = updateGenericMeta(defaultMetaRS("seDescription"))
				SEkeywords = updateGenericMeta(defaultMetaRS("seKeywords"))
				SeTopText = updateGenericMeta(defaultMetaRS("SeTopText"))
				SeBottomText = updateGenericMeta(defaultMetaRS("SeBottomText"))
				contentText = updateGenericMeta(defaultMetaRS("seContentText"))
				SeH1 = updateGenericMeta(defaultMetaRS("seH1"))
				if prepStr(SeTopText) = "" then SeTopText = contentText
			end if
		else
			if prepStr(metaRS("Title")) <> "" then SEtitle = metaRS("Title")
			if prepStr(metaRS("Description")) <> "" then SEdescription = metaRS("Description")
			if prepStr(metaRS("Keywords")) <> "" then SEkeywords = metaRS("Keywords")
			if prepStr(metaRS("topText")) <> "" then contentText = metaRS("topText")
			if prepStr(metaRS("bottomText")) <> "" then SeBottomText = metaRS("bottomText")
			if prepStr(metaRS("h1")) <> "" then SeH1 = metaRS("h1")
			if prepStr(SeTopText) = "" then SeTopText = contentText
		end if
	else
		if instr(request.ServerVariables("URL"),"masterPage.asp") > 0 then useURL = pageTitle else usePage = request.ServerVariables("URL")
		sql = "select * from metaTags_default where SiteId = " & siteID & " and basePage = '" & SQLquote(pageTitle) & "'"
		set defaultMetaRS = oConn.execute(sql)

		if not defaultMetaRS.EOF then
			SEtitle = updateGenericMeta(defaultMetaRS("seTitle"))
			SEdescription = updateGenericMeta(defaultMetaRS("seDescription"))
			SEkeywords = updateGenericMeta(defaultMetaRS("seKeywords"))
			contentText = updateGenericMeta(defaultMetaRS("seContentText"))
			SeTopText = updateGenericMeta(defaultMetaRS("SeTopText"))
			SeBottomText = updateGenericMeta(defaultMetaRS("SeBottomText"))
			SeH1 = updateGenericMeta(defaultMetaRS("seH1"))
			if prepStr(SeTopText) = "" then SeTopText = contentText
		end if
	end if

	sql = "select * from metaTags_default where SiteId = " & siteID & " and basePage = '" & SQLquote(pageTitle) & "'"
	set defaultMetaRS = oConn.execute(sql)

	if not defaultMetaRS.EOF then
		if prepStr(SEtitle) = "" then SEtitle = updateGenericMeta(defaultMetaRS("seTitle"))
		if prepStr(SEdescription) = "" then SEdescription = updateGenericMeta(defaultMetaRS("seDescription"))
		if prepStr(SEkeywords) = "" then SEkeywords = updateGenericMeta(defaultMetaRS("seKeywords"))
		if prepStr(contentText) = "" then contentText = updateGenericMeta(defaultMetaRS("seContentText"))
		if prepStr(SeTopText) = "" then SeTopText = updateGenericMeta(defaultMetaRS("SeTopText"))
		if prepStr(SeBottomText) = "" then SeBottomText = updateGenericMeta(defaultMetaRS("SeBottomText"))
		if prepStr(SeH1) = "" then SeH1 = updateGenericMeta(defaultMetaRS("seH1"))
		if prepStr(SeTopText) = "" then SeTopText = contentText
	end if

	if prepStr(SeTopText) <> "" then SeTopText = SeTopText
	if prepStr(SeBottomText) <> "" then SeBottomText = SeBottomText
	session("metaTags") = updateGenericMeta(SEtitle) & "##" & updateGenericMeta(SEdescription) & "##" & updateGenericMeta(SEkeywords) & "##" & updateGenericMeta(SeTopText) & "##" & updateGenericMeta(SeBottomText) & "##" & updateGenericMeta(SeH1)
end function

function updateGenericMeta(curVal)
	if prepStr(itemDesc_CA) <> "" then itemDesc = itemDesc_CA
	if prepStr(itemDesc_CO) <> "" then itemDesc = itemDesc_CO
	if prepStr(itemDesc_PS) <> "" then itemDesc = itemDesc_PS
	if prepStr(curVal) <> "" then
		if prepStr(BrandName) = "" then BrandName = "Universal"
		curVal = replace(curVal,"XXX",BrandName)
		if prepStr(modelName) = "" then modelName = "Universal"
		curVal = replace(curVal,"YYY",modelName)
		if prepStr(categoryName) = "" then categoryName = ""
		curVal = replace(curVal,"ZZZ",categoryName)
		curVal = replace(curVal,"CCC",carrierName)
		updateGenericMeta = replace(replace(curVal,"PPP",itemDesc),"Universal Universal","Universal")
	else
		updateGenericMeta = ""
	end if
end function

function cleanMP(curVal)
	curVal = replace(curVal,"??","-")
	curVal = replace(curVal,"°","&deg;")
	curVal = replace(curVal,"??","&trade;")
	curVal = replace(curVal,"®","&reg;")
	curVal = replace(curVal,chr(174),"&copy;")
	curVal = replace(curVal,chr(153),"&trade;")
	curVal = replace(curVal,"ö","o")
	curVal = replace(curVal,"ü","u")
	curVal = replace(curVal,"á","a")
	curVal = replace(curVal,chr(225),"a")
	curVal = replace(curVal,chr(246),"o")
	curVal = replace(curVal,chr(252),"u")
	curVal = replace(curVal,chr(147),"""")
	curVal = replace(curVal,chr(148),"""")
	curVal = replace(curVal,chr(150),"-")
	curVal = replace(curVal,chr(151),"-")
	cleanMP = curVal
end function

function SQLquote(strValue)
	if not isNull(strValue) and strValue <> "" then
		SQLquote = trim(replace(strValue,"'","''"))
		SQLquote = replace(SQLquote,chr(34),"''''")
		SQLquote = replace(SQLquote,vbcrlf," ")
		SQLquote = replace(SQLquote,vbtab," ")
	else
		SQLquote = strValue
	end if
end function

function isEmail(emailVal)
	if instr(emailVal,"@") < 1 then
		isEmail = false
	else
		testingVal = split(emailVal,"@")
		if instr(testingVal(1),".") < 1 then
			isEmail = false
		else
			if len(testingVal(0)) < 1 then
				isEmail = false
			else
				postSplit = split(testingVal(1),".")
				if len(postSplit(0)) > 0 and len(postSplit(1)) > 0 then
					isEmail = true
				else
					isEmail = false
				end if
			end if
		end if
	end if
end function

function chopStr(curVal,myLen)
	if len(curVal) > myLen then
		if mid(curVal,myLen+1,1) = " " then lastSpace = 1 else lastSpace = 0
		curVal = left(curVal,myLen)
		if lastSpace = 0 then
			if instrrev(curVal," ") > 0 then curVal = left(curVal,instrrev(curVal," "))
		end if
		chopStr = trim(curVal) & "&#8230;"
	else
		chopStr = curVal
	end if
end function

Function titleCase(byVal strValue)
       Dim intChat, intLastChar, blnUCase
       strValue = LCase(strValue)
       For intChar = 1 To Len(strValue)
              blnUCase = False
              If intChar = 1 Then
                     blnUCase = True
              Else
                     intLastChar = Asc(Mid(strValue, intChar - 1, 1))
                     If Not ((intLastChar >= 97 And intLastChar <= 122) Or (intLastChar >= 65 And intLastChar <= 90)) Then
                           blnUCase = True
                     End If
              End If
              If blnUCase Then
                     strValue = Left(strValue, intChar - 1) & UCase(Mid(strValue, intChar, 1)) & Mid(strValue, intChar + 1)
              End If
       Next
       TitleCase = strValue
End Function

function setMVT(siteID,testPage)
	sql =	"select a.id as projID, a.isLive, b.name as objName, (select COUNT(*) from mvt_variations where projectID = a.id and active = 1) as varCnt " &_
			"from mvt_projects a " &_
				"left join mvt_objects b on a.id = b.projectID " &_
			"where a.testPage = '" & testPage & "' and a.active = 1 and b.disabled = 0 and a.siteID = " & siteID
	'session("errorSQL") = sql
	set mvtRS = oConn.execute(sql)

	dim objList : objList = ""
	do while not mvtRS.EOF
		projID = prepInt(mvtRS("projID"))
		isLive = mvtRS("isLive")
		if objList = "" then
			objList = "mvt_" & mvtRS("objName")
		else
			objList = objList & "##mvt_" & mvtRS("objName")
		end if
		totalVars = prepInt(mvtRS("varCnt"))
		mvtRS.movenext
	loop
	if prepInt(request.Cookies("mvtBucket_" & projID)) = 0 or prepInt(request.Cookies("mvtBucket_" & projID)) > totalVars then
		randomize
		mvtID = int(rnd * totalVars) + 1
		response.Cookies("mvtBucket_" & projID) = mvtID
		response.cookies("mvtBucket_" & projID).expires = dateAdd("m", 1, now)
	else
		mvtID = prepInt(request.Cookies("mvtBucket_" & projID))
	end if
	if instr(request.ServerVariables("SERVER_NAME"),"staging") < 1 then
		if not isLive then mvtID = 1
	end if
	mvtCssList = objList
end function

function setAB(testID,curURL)
	session("newMpPage") = ""
	sql = "select active, isLive, combo, (select count(*) from mvt_variations where projectID = " & testID & " and active = 1) as variations from mvt_projects where id = " & testID
	'session("errorSQL") = sql
	set abRS = oConn.execute(sql)

	if abRS("combo") then
		maxCnt = prepInt(abRS("variations"))+1
		splitCnt = 1
	else
		maxCnt = 10
		splitCnt = 5
	end if

	if abRS("active") then
		if instr(request.ServerVariables("SERVER_NAME"),"staging.") > 0 or abRS("isLive") then
			mvtID = prepInt(request.Cookies("mvtID_" & testID))

			if mvtID = 0 then
				randomize
				mvtID = int(rnd * maxCnt) + 1
				response.Cookies("mvtID_" & testID) = mvtID
				response.Cookies("mvtID_" & testID).Expires = DateAdd("d", 30, now)
				if mvtID > splitCnt then
					newURL = replace(curURL,".","_new.")
					if instr(newURL,"_mp") > 0 then
						session("newMpPage") = newURL
					else
						response.Redirect(newURL)
					end if
				end if
			else
				if mvtID > splitCnt then
					newURL = replace(curURL,".","_new.")
					if instr(newURL,"_mp") > 0 then
						session("newMpPage") = newURL
					else
						response.Redirect(newURL)
					end if
				end if
			end if
		end if
	end if
end function

function autoDeleteCompPages(delType,delVal)
	set fso = CreateObject("Scripting.FileSystemObject")
	dim weBrand : weBrand = 0
	dim weBmc : weBmc = 0
	dim caBmc : caBmc = 0
	dim coBmc : coBmc = 0
	dim coProd : coProd = 0
	dim psProd : psProd = 0

	if delType = "updateItem" then
		prodDetailArray = split(delVal,"##")
		tempBrandID = prodDetailArray(0)
		tempModelID = prodDetailArray(1)
		tempTypeID = prodDetailArray(2)
		tempItemID = prodDetailArray(3)

		sql =	"select a.brandName, b.modelName " &_
				"from we_brands a " &_
					"left join we_models b on b.modelID = " & tempModelID & " " &_
				"where a.brandID = " & tempBrandID
'		session("errorSQL") = sql
		set delRS = oConn.execute(sql)

		if not delRS.eof then
			brandModel = formatSEO(delRS("brandName")) & "-" & formatSEO(delRS("modelName"))
		else
			brandModel = "Universal"
		end if

		weBmc = 1
		weBrand = 1
		weBrandModel = 1

		caBmc = 1

		coBmc = 1
		coProd = 1

		psProd = 1
	elseif delType = "model" then
		prodDetailArray = split(delVal,"##")
		tempBrandID = prodDetailArray(0)
		tempModelID = prodDetailArray(1)

		weBrand = 1
	elseif delType = "price" then
		prodDetailArray = split(delVal,"##")
		tempBrandID = prodDetailArray(0)
		tempModelID = prodDetailArray(1)
		tempTypeID = prodDetailArray(2)
		tempItemID = prodDetailArray(3)

		weBmc = 1
		caBmc = 1
		coBmc = 1
		coProd = 1
		psProd = 1
	elseif delType = "weHome" then
		weHome = 1
	elseif delType = "weBrands" then
		prodDetailArray = split(delVal,"##")
		tempBrandID = prodDetailArray(0)
		weBrand = 1
	elseif delType = "weAllBrands" then
		activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brand\*.htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	elseif delType = "weBrandModels" then
		prodDetailArray = split(delVal,"##")
		brandModel = prodDetailArray(0)
		weBrandModel = 1
	elseif delType = "weAllBrandModels" then
		activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brandModel\*.htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	elseif delType = "weBMCD" then
		prodDetailArray = split(delVal,"##")
		tempBrandID = prodDetailArray(0)
		tempModelID = prodDetailArray(1)
		tempTypeID = prodDetailArray(2)
		weBmc = 1
	elseif delType = "weAllBMCD" then
		activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brandModelCatDetails\*.htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	end if

	if weHome = 1 then
		activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\index.htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	end if

	if weBrand = 1 then
		activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brand\brand_" & tempBrandID & ".htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	end if

	if weBrandModel = 1 then
		activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brandModel\accessories-for-" & brandModel & ".htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	end if

	if weBmc = 1 then
		activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brandModelCatDetails\brand_" & tempBrandID & "_model_" & tempModelID & "_category_" & tempTypeID & "*.htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	end if

	if caBmc = 1 then
		activeFiles = "C:\inetpub\wwwroot\cellphoneaccents.com\www\compressedPages\bmc\bmc_" & tempBrandID & "_" & tempModelID & "_" & tempTypeID & "*.htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	end if

	if coBmc = 1 then
		activeFiles = "C:\inetpub\wwwroot\cellularoutfitter.com\www\compressedPages\bmc\bmc_" & tempBrandID & "_" & tempModelID & "_" & tempTypeID & "*.htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	end if

	if coProd = 1 then
		activeFiles = "C:\inetpub\wwwroot\cellularoutfitter.com\www\compressedPages\products\product_" & tempItemID & ".htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	end if

	if psProd = 1 then
		activeFiles = "C:\inetpub\wwwroot\phonesale.com\www\compressedPages\products\product_" & tempItemID & ".htm"
		'session("errorSQL") = activeFiles
		On Error Resume Next
		fso.deleteFile(activeFiles)
		On Error Goto 0
	end if
end function

function mobileDetect()
	userBrowser = request.ServerVariables("HTTP_USER_AGENT")
	mobileAccess = request.QueryString("mobileAccess")
	session("mobileAccess") = prepStr(session("mobileAccess"))
	mobileByPass = prepInt(request.QueryString("mobileByPass"))
	if isnull(mobileAccess) or len(mobileAccess) < 1 then mobileAccess = "" end if
	if (isnull(session("mobileAccess")) or len(session("mobileAccess")) < 1) and mobileByPass = 0 then
		'no mobile bypass session found
		if (instr(userBrowser,"Mobile") > 0 or instr(userBrowser,"BlackBerry") > 0 or instr(userBrowser,"Opera Mini") > 0) and instr(userBrowser,"iPad") < 1 then
			'when it is e-mail blast do not send them WE mobile site
'			if lcase(prepStr(request.querystring("utm_medium"))) <> "email" or prepStr(request.querystring("utm_campaign")) = "BlackFriday" then
			if true then
				'mobile browser detected
				if mobileAccess = "" and left(request.ServerVariables("SERVER_NAME"),2) <> "m." then
					'no mobile bypass in QS - move to mobile site with brand/model/category info
					if instr(request.ServerVariables("SERVER_NAME"),"www.") > 0 then useServerName = replace(request.ServerVariables("SERVER_NAME"),"www.","m.")
					if instr(request.ServerVariables("SERVER_NAME"),"staging.") > 0 then useServerName = replace(request.ServerVariables("SERVER_NAME"),"staging.","mdev.")
					if instr(lcase(request.ServerVariables("SERVER_NAME")),"cellularoutfitter") > 0 then
						useURL = replace(request.ServerVariables("HTTP_X_REWRITE_URL"),".asp",".html")
						if instr(useURL,"/cart/") > 0 then useURL = replace(useURL,"/cart","")
						response.Redirect("//" & useServerName & useURL)
					else
						response.Redirect("//" & useServerName & request.ServerVariables("HTTP_X_REWRITE_URL") & ".htm")
					end if
				else
					'mobile bypass found in QS - save session
					session("mobileAccess") = 1
				end if
			end if
		end if
	end if
end function

function emailList(emailName)
	sql = "select a.emailAddr, b.email from we_emailLists a left join we_adminUsers b on a.adminID = b.adminID where emailName = '" & emailName & "'"
	session("errorSQL") = sql
	set emailListRS = oConn.execute(sql)

	toList = ""
	do while not emailListRS.EOF
		email1 = emailListRS("emailAddr")
		email2 = emailListRS("email")

		if isnull(email1) then
			toList = toList & email2 & ","
		else
			toList = toList & email1 & ","
		end if
		emailListRS.movenext
	loop
	if toList <> "" then toList = left(toList,len(toList)-1)
	emailList = toList
end function

function getRS(sql)
	Dim oConnTemp
	Dim oRS

'	set oConnTemp = Server.CreateObject("ADODB.Connection")
'	oConnTemp.Open Session("ConnectionString")
	call getDBConn(oConnTemp)

	Set oRS = Server.CreateObject("ADODB.Recordset")
	oRS.CursorLocation = 3
	oRS.Open sql, oConnTemp, 0, 1
	Set oRS.ActiveConnection = Nothing

	Set getRS = oRS

	oConnTemp.Close
	Set oConnTemp = Nothing
end function

sub getDBConn(byref conn)
	'	set weUtil = server.CreateObject("weUtil.DBConn")
	'	if instr(request.ServerVariables("HTTP_HOST"),"staging") > 0 then
	'		set conn = weUtil.getStagingConn()
	'	else
	'		set conn = weUtil.getLiveConn()
	'	end if

	set conn = CreateObject("ADODB.Connection")
		if instr(request.ServerVariables("HTTP_HOST"),"staging") > 0 then
			conn.Open "DSN=devDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"
		else
			conn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"
	end if
end sub

function updateHTML(oldTxt)
	newTxt = replace(oldTxt,"&lt;","<")
	newTxt = replace(newTxt,"&gt;",">")
	newTxt = replace(newTxt,"&amp;lt;","<")
	newTxt = replace(newTxt,"&amp;gt;",">")
	updateHTML = newTxt
end function

function cancelTestOrder(orderID)
	SQL = "UPDATE we_Orders SET cancelled = -1 WHERE orderID = '" & orderID & "'"
	oConn.execute SQL
	SQL = "SELECT a.itemID, a.quantity, b.partNumber, b.itemKit_NEW, b.inv_qty FROM we_Orderdetails a join we_Items b on a.itemID = b.itemID and b.master = 1 WHERE orderID = '" & orderID & "'"
	session("errorSQL2") = SQL
	set RS = oConn.execute(SQL)
	do until RS.eof
		curItemID = RS("itemID")
		curQty = RS("quantity")
		curPartNumber = RS("partNumber")
		curKit = RS("itemKit_NEW")
		curInvQty = RS("inv_qty")
		if isnull(curKit) then
			SQL = "UPDATE we_Items SET inv_qty = inv_qty + " & curQty & ", NumberOfSales = NumberOfSales - " & curQty & " WHERE itemID = '" & curItemID & "'"
			oConn.execute SQL

			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & curItemID & "," & curInvQty & "," & curQty & "," & orderID & "," & prepInt(session("adminID")) & ",'Cancel Order','" & now & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		else
			SQL = "SELECT b.itemID,a.typeID,a.partNumber,b.inv_qty FROM we_items a left join we_items b on a.partNumber = b.partNumber and b.master = 1 WHERE a.itemID IN (" & curKit & ")"
			set RS3 = oConn.execute(SQL)
			if RS3.EOF then
				%>
				<h3>Inventory Update Error on itemID <%=curItemID%></h3>
				<%
			else
				SQL = "UPDATE we_Items SET NumberOfSales = NumberOfSales - " & curQty & " WHERE itemID = '" & curItemID & "'"
				oConn.execute SQL
				do while not RS3.EOF
					sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS3("itemID") & "," & RS3("inv_qty") & "," & curQty & "," & orderID & "," & prepInt(session("adminID")) & ",'Cancel Kit Order','" & now & "')"
					session("errorSQL") = sql
					oConn.execute(sql)

					SQL = "UPDATE we_Items SET inv_qty = inv_qty + " & curQty & " WHERE itemID = '" & RS3("itemID") & "'"
					oConn.execute SQL
					RS3.movenext
				loop
			end if
		end if
		RS.movenext
	loop
end function
%>