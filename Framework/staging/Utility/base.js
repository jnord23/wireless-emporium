	function ajax(newURL,rLoc) {
		if (newURL.indexOf("?") > 0) {
			newURL = newURL + '&ajaxID=' + Math.floor(Math.random()*999999)
		}
		else {
			newURL = newURL + '?ajaxID=' + Math.floor(Math.random()*999999)
		}
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						alert("No data available")					
					}
					else if (rVal == "refresh") {
						curLoc = window.location
						window.location = curLoc
					}
					else if (rVal == "wrong recaptcha") {
						Recaptcha.reload();
						document.getElementById('recaptcha_msg').innerHTML = '<span style="color:red;"><b>You have entered the wrong secret words.</b></span>';
					}
					else {
						correctAnswer = true;
						if (document.getElementById(rLoc) != undefined) {
							document.getElementById(rLoc).innerHTML = rVal;
						}
					}
					rVal = null;
				}
				else {
					//document.getElementById("testZone").innerHTML = newURL
					//alert("Error performing action");
				}
			}
		};
		httpRequest.send(null);
		httpRequest.close;
	}
	
	function showDefaultMetaTags() {
		if (document.getElementById('defaultMetaTags').style.display == '') {
			document.getElementById('defaultMetaTags').style.display = 'none'
		}
		else {
			document.getElementById('metaTagBox').style.display = 'none'
			document.getElementById('defaultMetaTags').style.display = ''
		}
	}
	
	function showMetaTags() {
		if (document.getElementById('metaTagBox').style.display == '') {
			document.getElementById('metaTagBox').style.display = 'none'
		}
		else {
			document.getElementById('defaultMetaTags').style.display = 'none'
			document.getElementById('metaTagBox').style.display = ''
		}
	}
	
	function ajaxPost(newURL, params, rLoc) {
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('POST', newURL, true);
		httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		httpRequest.setRequestHeader("Content-length", params.length);
		httpRequest.send(params);
				
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						alert("No data available")
					}
					else if (rVal == "refresh") {
						curLoc = window.location
						window.location = curLoc
					}
					else {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
//					document.getElementById("testZone").innerHTML = newURL
//					alert("Error performing action")
				}
			}
		};
	}	
	
	function validateEmail(email) 
	{
		var splitted = email.match("^(.+)@(.+)$");
		if (splitted == null) return false;
		if (splitted[1] != null) {
			var regexp_user=/^\"?[\w-_\.]*\"?$/;
			if (splitted[1].match(regexp_user) == null) return false;
		}
		if (splitted[2] != null) {
			var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
			if (splitted[2].match(regexp_domain) == null) {
				var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
				if (splitted[2].match(regexp_ip) == null) return false;
			}
			return true;
		}
		return false;
	}	
	
	function getSelectedRadioValue(buttonGroup) {
		var i = getSelectedRadio(buttonGroup);
	   if (i == -1) {
		  return "";
	   } else {
		  if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
			 return buttonGroup[i].value;
		  } else { // The button group is just the one button, and it is checked
			 return buttonGroup.value;
		  }
	   }
	}
	
	function getSelectedRadio(buttonGroup) {
	   if (buttonGroup[0]) {
		  for (var i=0; i<buttonGroup.length; i++) {
			 if (buttonGroup[i].checked) {
				return i
			 }
		  }
	   } else {
		  if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
	   }
	   return -1;
	}
	
	// bloomreach add/remove cart tracking
	function br_cartTracking(tType, itemid, color, prodName, price) {
		try
		{
			if (tType == 'add') {
				BrTrk.getTracker().logEvent('Cart', tType, {'prod_id': itemid, 'sku': itemid, 'prod_color': color, 'prod_name': prodName}, {'price' : price});
			} else {
				BrTrk.getTracker().logEvent('Cart', tType, {'prod_id': itemid, 'sku': itemid, 'prod_color': color, 'prod_name': prodName});
			}
			
		}
		catch(e) {}    
	}	
	
	function formatSEO(s){
		var output = s.replace(/[^a-zA-Z0-9]/g,' ').replace(/\s+/g,"-").toLowerCase();
		/* remove first dash */
		if(output.charAt(0) == '-') output = output.substring(1);
		/* remove last dash */
		var last = output.length-1;
		if(output.charAt(last) == '-') output = output.substring(0, last);
		
		return output;
	}
	
	function checkEmail(email) {
		email = email.trim();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
		if (!filter.test(email)) {
			return false;
		}
		return true;
	}
	
	function Open_Popup(theURL,winName,features) {
		window.open(theURL,winName,features);
	}

	function getQsVars(pUrl)
	{
		var vars = [], hash, hashes;
		if (pUrl != "") {
			hashes = pUrl.slice(pUrl.indexOf('?') + 1).split('&');
		} else {
			hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		}
		if (hashes != undefined) {
			for(var i = 0; i < hashes.length; i++)
			{
				hash = hashes[i].split('=');
				vars.push(hash[0]);
				vars[hash[0]] = hash[1];
			}
		}
		return vars;
	}