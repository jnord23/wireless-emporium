<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/Framework/account.asp"-->
<%
	on error resume next
		mySession = request.cookies("mySession")
		mySrToken = prepStr(session("sr_token"))
		if mySrToken = "" then mySrToken = prepStr(request.form("mySrToken"))
		if mySrToken <> "" then
			session("sr_token") = mySrToken
			Response.Cookies("srLogin") = mySrToken
			Response.Cookies("srLogin").Path = "/"
			Response.Cookies("srLogin").Expires = Date + 1		
		else
			session("sr_token") = mySrToken
			Response.Cookies("srLogin") = mySrTokenf
			Response.Cookies("srLogin").Path = "/"
			Response.Cookies("srLogin").Expires = now		
		end if
	on error goto 0
	HTTP_REFERER = request.servervariables("HTTP_REFERER")
	siteid = ""
	siteName = lcase(request.ServerVariables("SERVER_NAME"))
	apiLoginID = ""
	apiTransactionKey = ""
	mobileOrder = prepInt(request.Form("mobile"))
	if instr(siteName, "wirelessemporium.com") > 0 then
		siteid = 0
		siteURL = "wirelessemporium.com"
		strSiteEmail = "sales@wirelessemporium.com"
		imgLogo = "/images/WElogo.gif"
		sitePhone = "(800) 305-1106"		
		
		apiLoginID = CC_API_LOGIN_ID
		apiTransactionKey = CC_TRANSACTION_KEY
		tblAccount = "we_accounts"
		if mobileOrder = 1 then
			strInvoiceDescription = "m.wirelessemporium.com Online Post Purchase"
		else
			strInvoiceDescription = "WirelessEmporium.com Online Post Purchase"
		end if
	elseif instr(siteName, "cellphoneaccents.com") > 0 then
		siteid = 1
		siteURL = "cellphoneaccents.com"
		strSiteEmail = "sales@cellphoneaccents.com"
		imgLogo = "/images/full_logo.jpg"
		sitePhone = "(888) 244-7203"
		strInvoiceDescription = "CellphoneAccents.com Online Post Purchase"		

		apiLoginID = CA_CC_API_LOGIN_ID
		apiTransactionKey = CA_CC_TRANSACTION_KEY
		tblAccount = "ca_accounts"
	elseif instr(siteName, "cellularoutfitter.com") > 0 then
		siteid = 2
		siteURL = "cellularoutfitter.com"
		strSiteEmail = "sales@cellularoutfitter.com"
		imgLogo = "/images/CellularOutfitter.jpg"
		sitePhone = "(888) 871-6926"
		if mobileOrder = 1 then
			strInvoiceDescription = "m.CellularOutfitter.com Online Post Purchase"
		else
			strInvoiceDescription = "CellularOutfitter.com Online Post Purchase"
		end if
		
		if instr(siteName,"m.cellularoutfitter.com") > 0 then mobileSite = 1	
		
		apiLoginID = CO_CC_API_LOGIN_ID
		apiTransactionKey = CO_CC_TRANSACTION_KEY
		tblAccount = "co_accounts"
	elseif instr(siteName, "phonesale.com") > 0 then
		siteid = 3
		siteURL = "phonesale.com"
		strSiteEmail = "sales@phonesale.com"
		imgLogo = "/images/phonesale.jpg"
		sitePhone = "(800) 818-4575"
		strInvoiceDescription = "PhoneSale.com Online Post Purchase"
		
		apiLoginID = PS_CC_API_LOGIN_ID
		apiTransactionKey = PS_CC_TRANSACTION_KEY		
		tblAccount = "ps_accounts"
	elseif instr(siteName, "tabletmall.com") > 0 then
		siteid = 10
		siteURL = "tabletmall.com"
		strSiteEmail = "sales@tabletmall.com"
		imgLogo = "/images/logo.gif"
		sitePhone = "(888) 551-6570"
		strInvoiceDescription = "TabletMall.com Online Post Purchase"
		
		apiLoginID = TM_CC_API_LOGIN_ID
		apiTransactionKey = TM_CC_TRANSACTION_KEY
		tblAccount = "er_accounts"
	end if

	orderid = prepInt(request.form("orderid"))
	nOrderTotal = prepInt(request.form("addlGrandTotal"))	
	transid = prepInt(request.form("transid"))

	'prevent refresh page
	sql = "select top 1 orderdetailid from we_orderdetails where orderid = '" & orderid & "' and postpurchase = 1"
	set rs = oConn.execute(sql)
	if not rs.eof then
		call CloseConn(oConn)
		response.redirect("/?pp=101")
		response.end
	end if
	
	sql	=	"select	accountid, isnull(CIM_CustomerProfileID, 0) profileID, isnull(CIM_CustomerPaymentProfileID, 0) pProfileID, baddress1" & vbcrlf & _
			"from	v_accounts" & vbcrlf & _
			"where	accountid in (" & vbcrlf & _
			"		select	accountid" & vbcrlf & _
			"		from	we_orders" & vbcrlf & _
			"		where	orderid = '" & orderid & "' )" & vbcrlf & _
			"	and	site_id = '" & siteid & "'"
	set rs = oConn.execute(sql)
	if rs.eof then
		call CloseConn(oConn)
		response.redirect("/?pp=102")
		response.end
	end if

	bTestmode = false
	if rs("baddress1") = "4040 N. Palm St." then bTestmode = true
	
	accountid = rs("accountid")
	cimProfileID = rs("profileID")
	cimPaymentProfileID = rs("pProfileID")
	
'	response.write "cimProfileID:" & cimProfileID & "<br>"
'	response.write "cimPaymentProfileID:" & cimPaymentProfileID & "<br>"
	
	if not bTestmode then
		if clng(cimProfileID) = 0 or clng(cimPaymentProfileID) = 0 then
			call CloseConn(oConn)
			response.redirect("/?pp=103")
			response.end
		end if
	
		'====== makes post purchase reqeust
		xml	=	"<?xml version=""1.0"" encoding=""utf-8""?>" & _
				"<createCustomerProfileTransactionRequest xmlns=""AnetApi/xml/v1/schema/AnetApiSchema.xsd"">" & _
				"	<merchantAuthentication>" & _
				"		<name>" & apiLoginID & "</name>" & _
				"		<transactionKey>" & apiTransactionKey & "</transactionKey>" & _
				"	</merchantAuthentication>" & _
				"	<transaction>" & _
				"		<profileTransAuthOnly>" & _
				"			<amount>" & nOrderTotal & "</amount>" & _
				"			<customerProfileId>" & cimProfileID & "</customerProfileId>" & _
				"			<customerPaymentProfileId>" & cimPaymentProfileID & "</customerPaymentProfileId>" & _			
				"			<order>" & _
				"				<invoiceNumber>" & orderid & "</invoiceNumber>" & _
				"				<description>" & strInvoiceDescription & "</description>" & _
				"				<purchaseOrderNumber>" & orderid & "</purchaseOrderNumber>" & _
				"			</order>" & _
				"			<recurringBilling>false</recurringBilling>" & _
				"		</profileTransAuthOnly>" & _
				"	</transaction>" & _
				"</createCustomerProfileTransactionRequest>"
	
		set objResponse = SendApiRequest(CIM_APIURL, xml)
		if not IsApiResponseSuccess(objResponse) then
			cimErrorCode = objResponse.selectSingleNode("/*/api:messages/api:message/api:code").Text
			cimErrorText = objResponse.selectSingleNode("/*/api:messages/api:message/api:text").Text
	
			'======= delete customer profile id from db and from payment gateway
			sql = 	"update	" & tblAccount & vbcrlf & _
					"set	CIM_CustomerProfileID = null" & vbcrlf & _
					"	,	CIM_CustomerPaymentProfileID = null" & vbcrlf & _
					"where	accountid = '" & accountid & "'"
			oConn.execute(sql)
			
			xml	=	"<?xml version=""1.0"" encoding=""utf-8""?>" & vbcrlf & _
					"<deleteCustomerProfileRequest xmlns=""AnetApi/xml/v1/schema/AnetApiSchema.xsd"">" & vbcrlf & _
					"<merchantAuthentication>" & vbcrlf & _
					"	<name>" & apiLoginID & "</name>" & _
					"	<transactionKey>" & apiTransactionKey & "</transactionKey>" & _
					"</merchantAuthentication>" & vbcrlf & _
					"<customerProfileId>" & cimProfileID & "</customerProfileId>" & vbcrlf & _
					"</deleteCustomerProfileRequest>"
			set objResponse = SendApiRequest(CIM_APIURL, xml)	
	
			%>
			<h3>This order cannot be processed for the following reason from payment gateway</h3>
			<ul>
				<li>response status: <%=cimErrorCode%></li>
				<li>response: <%=cimErrorText%></li>
			</ul>
			<a href="/">Continue Shopping</a>
			<%
			call CloseConn(oConn)
			response.end
		else
			set objDirectResponse = objResponse.selectSingleNode("/*/api:directResponse")
			arrDirectResponseFields = Split(objDirectResponse.Text, ",")
			transactionID = Server.HTMLEncode(arrDirectResponseFields(6))
		end if
	end if
				
	curSubTotal = cdbl(0)
	curShippingFee = cdbl(0)
	curTax = cdbl(0)
	curGrandTotal = cdbl(0)
	curDiscount = cdbl(0)
	
	'====== get current order amounts
	sql = 	"select	isnull(ordersubtotal, 0) ordersubtotal, replace(isnull(ordershippingfee, 0), '$', '') ordershippingfee, isnull(orderTax, 0) orderTax, isnull(ordergrandtotal, 0) ordergrandtotal, accountid" & vbcrlf & _
			"from	we_orders" & vbcrlf & _
			"where	orderid = '" & orderid & "'"
	set rs = oConn.execute(sql)
	if not rs.eof then
		curSubTotal = prepInt(rs("ordersubtotal"))
		curShippingFee = prepInt(rs("ordershippingfee"))
		curTax = prepInt(rs("orderTax"))
		curGrandTotal = prepInt(rs("ordergrandtotal"))
		accountid = prepInt(rs("accountid"))
		if curGrandTotal < (curSubTotal+curShippingFee+curTax) then
			curDiscount = (curSubTotal+curShippingFee+curTax) - curGrandTotal
		end if
	end if

	'====== add additional items to the order detail			
	nLoopProduct = prepInt(request.form("hidCount"))
	nAddlTotalPrice = cdbl(0)
	nAddlShipping = 0
	nAddlTax = 0
	nTotalQty = 0
	for i=1 to nLoopProduct
		if prepInt(request.form("chkItem"&i)) > 0 then
			qty = prepInt(request.form("chkQty"&i))
			if qty > 0 then
				nItemCount = nItemCount + 1
				itemid = prepInt(request.form("chkItem"&i))
				price = round(prepInt(request.form("itemPrice"&i)), 2)
				nTotalQty = nTotalQty + qty
				nAddlTotalPrice = nAddlTotalPrice + (price * qty)
				sql = "sp_InsertOrderDetail3 '" & orderid & "', '" & itemid & "', '" & qty & "', '" & price & "', 1"
				oConn.execute(sql)
			end if
		end if
	next

	newSubTotal = curSubTotal + nAddlTotalPrice
	if siteid = 2 then nAddlShipping = nTotalQty * 1.99
	newShippingFee = curShippingFee + nAddlShipping
	
	if curTax > 0 then nAddlTax = nAddlTotalPrice * Application("taxMath")
	newTax = curTax + nAddlTax
	nAddlGrandTotal = nAddlTotalPrice + nAddlShipping + nAddlTax
	newGrandTotal = newSubTotal + newShippingFee + newTax - curDiscount

'	response.write "curSubTotal:" & curSubTotal & "<br>"
'	response.write "curShippingFee:" & curShippingFee & "<br>"
'	response.write "curTax:" & curTax & "<br>"
'	response.write "curGrandTotal:" & curGrandTotal & "<br>"
'	response.write "curDiscount:" & curDiscount & "<br><br>"
'
'	response.write "nAddlShipping:" & nAddlShipping & "<br>"
'	response.write "nAddlTotalPrice:" & nAddlTotalPrice & "<br>"	
'	response.write "nAddlTax:" & nAddlTax & "<br><br>"
'
'	response.write "newSubTotal:" & newSubTotal & "<br>"
'	response.write "newShippingFee:" & newShippingFee & "<br>"
'	response.write "newTax:" & newTax & "<br>"
'	response.write "newGrandTotal:" & newGrandTotal & "<br><br>"
'	response.end

	nAddlGrandTotal = formatnumber(nAddlGrandTotal, 2) 
	nAddlTotalPrice = formatnumber(nAddlTotalPrice, 2) 
	nAddlShipping = formatnumber(nAddlShipping, 2) 
	nAddlTax = formatnumber(nAddlTax, 2)
	
	'====== update order total amounts
	sql = "sp_updateExistingOrderValue '" & orderid & "', '" & formatnumber(newSubTotal, 2) & "', '" & formatnumber(newShippingFee, 2) & "', '" & formatnumber(newTax, 2) & "', '" & formatnumber(newGrandTotal, 2) & "'"
	oConn.execute(sql)
	
	'====== need to migrate the following sql stmt to SP later..
	sql = 	"update	we_orders" & vbcrlf & _
			"set	pp_orderGrandTotal = '" & nAddlGrandTotal & "'" & vbcrlf & _
			"	,	pp_transactionID = '" & transactionID & "'" & vbcrlf & _
			"where	orderid = '" & orderid & "'"			
	oConn.execute(sql)	

	call CloseConn(oConn)

	if siteid = 0 then
		if instr(siteName,"m.wire") > 0 or instr(siteName,"mdev.wire") > 0 then
			response.redirect("/cart/postcomplete.htm?a=" & accountid & "&o=" & orderid & "&d=" & nAddlGrandTotal & "&c=" & nAddlTotalPrice & "&s=" & nAddlShipping & "&t=" & nAddlTax & "&mobileOrder=1")
		else
			response.redirect("/cart/postcomplete?a=" & accountid & "&o=" & orderid & "&d=" & nAddlGrandTotal & "&c=" & nAddlTotalPrice & "&s=" & nAddlShipping & "&t=" & nAddlTax)
		end if
	else
		response.redirect("/cart/postcomplete.asp?a=" & accountid & "&o=" & orderid & "&d=" & nAddlGrandTotal & "&c=" & nAddlTotalPrice & "&s=" & nAddlShipping & "&t=" & nAddlTax)
	end if
	
	
%>
