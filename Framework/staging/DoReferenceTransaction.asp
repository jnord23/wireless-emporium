<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/Framework/account.asp"-->
<!--#include virtual="/cart/process/PaypalSOAP/Constants.asp"-->
<%
	HTTP_REFERER = request.servervariables("HTTP_REFERER")
	siteid = ""
	mobileOrder = 0
	siteName = lcase(request.ServerVariables("SERVER_NAME"))
	
	if instr(siteName, "wirelessemporium.com") > 0 then
		siteid = 0
		strOrderDescription = "WirelessEmporium.com Online Postpurchase Order"
	elseif instr(siteName, "cellphoneaccents.com") > 0 then
		siteid = 1
		strOrderDescription = "CellphoneAccents.com Online Postpurchase Order"
	elseif instr(siteName, "cellularoutfitter.com") > 0 then
		siteid = 2
		strOrderDescription = "CellularOutfitter.com Online Postpurchase Order"
	elseif instr(siteName, "phonesale.com") > 0 then
		siteid = 3
		strOrderDescription = "PhoneSale.com Online Postpurchase Order"
	elseif instr(siteName, "tabletmall.com") > 0 then
		siteid = 10
		strOrderDescription = "TabletMall.com Online Postpurchase Order"
	end if

	orderid = prepInt(request.form("orderid"))
	nOrderTotal = prepInt(request.form("addlGrandTotal"))	

	sql = "select top 1 orderdetailid from we_orderdetails with (nolock) where orderid = '" & orderid & "' and postpurchase = 1"
	set rs = oConn.execute(sql)
	if not rs.eof then
		call responseRedirect("/?ppErr=101")
	end if
	
	sql = "select extordernumber, extordertype from we_orders with (nolock) where orderid = '" & orderid & "' and approved = 1 and (cancelled is null or cancelled = 0)"
	set rs = oConn.execute(sql)
	if rs.eof then
		call responseRedirect("/?ppErr=102")
	else
		if rs("extordertype") <> 1 then
			call responseRedirect("/?ppErr=103")
		end if
	end if	
	
	sql	=	"select	accountid, baddress1, isnull(BillingAgreementID, 0) paypalBAID" & vbcrlf & _
			"from	v_accounts" & vbcrlf & _
			"where	accountid in (" & vbcrlf & _
			"		select	accountid" & vbcrlf & _
			"		from	we_orders" & vbcrlf & _
			"		where	orderid = '" & orderid & "' )" & vbcrlf & _
			"	and	site_id = '" & siteid & "'"
	set rs = oConn.execute(sql)
	if rs.eof then
		call responseRedirect("/?ppErr=104")
	end if

	bTestmode = false
	if rs("baddress1") = "4040 N. Palm St." then bTestmode = true
	
	accountid = rs("accountid")
	paypalBAID = rs("paypalBAID")
	
	curSubTotal = cdbl(0)
	curShippingFee = cdbl(0)
	curTax = cdbl(0)
	curGrandTotal = cdbl(0)
	curDiscount = cdbl(0)
	
	'====== get current order amounts
	sql = 	"select	isnull(ordersubtotal, 0) ordersubtotal, replace(isnull(ordershippingfee, 0), '$', '') ordershippingfee, isnull(orderTax, 0) orderTax, isnull(ordergrandtotal, 0) ordergrandtotal, accountid" & vbcrlf & _
			"from	we_orders" & vbcrlf & _
			"where	orderid = '" & orderid & "'"
	set rs = oConn.execute(sql)
	if not rs.eof then
		curSubTotal = prepInt(rs("ordersubtotal"))
		curShippingFee = prepInt(rs("ordershippingfee"))
		curTax = prepInt(rs("orderTax"))
		curGrandTotal = prepInt(rs("ordergrandtotal"))
		accountid = prepInt(rs("accountid"))
		if curGrandTotal < (curSubTotal+curShippingFee+curTax) then
			curDiscount = (curSubTotal+curShippingFee+curTax) - curGrandTotal
		end if
	else
		call responseRedirect("/?ppErr=105")
	end if

	'====== add additional items to the order detail			
	nLoopProduct = prepInt(request.form("hidCount"))
	nAddlTotalPrice = cdbl(0)
	nAddlShipping = 0
	nAddlTax = 0
	nTotalQty = 0
	for i=1 to nLoopProduct
		if prepInt(request.form("chkItem"&i)) > 0 then
			qty = prepInt(request.form("chkQty"&i))
			if qty > 0 then
				nItemCount = nItemCount + 1
				itemid = prepInt(request.form("chkItem"&i))
				price = round(prepInt(request.form("itemPrice"&i)), 2)
				nTotalQty = nTotalQty + qty
				nAddlTotalPrice = nAddlTotalPrice + (price * qty)
'				sql = "sp_InsertOrderDetail3 '" & orderid & "', '" & itemid & "', '" & qty & "', '" & price & "', 1"
				oConn.execute(sql)
			end if
		end if
	next

	newSubTotal = curSubTotal + nAddlTotalPrice
	if siteid = 2 then nAddlShipping = nTotalQty * 1.99
	newShippingFee = curShippingFee + nAddlShipping
	
	if curTax > 0 then nAddlTax = nAddlTotalPrice * Application("taxMath")
	newTax = curTax + nAddlTax
	nAddlGrandTotal = nAddlTotalPrice + nAddlShipping + nAddlTax
	newGrandTotal = newSubTotal + newShippingFee + newTax - curDiscount

'	response.write "curSubTotal:" & curSubTotal & "<br>"
'	response.write "curShippingFee:" & curShippingFee & "<br>"
'	response.write "curTax:" & curTax & "<br>"
'	response.write "curGrandTotal:" & curGrandTotal & "<br>"
'	response.write "curDiscount:" & curDiscount & "<br><br>"
'
'	response.write "nAddlShipping:" & nAddlShipping & "<br>"
'	response.write "nAddlTotalPrice:" & nAddlTotalPrice & "<br>"	
'	response.write "nAddlTax:" & nAddlTax & "<br><br>"
'
'	response.write "newSubTotal:" & newSubTotal & "<br>"
'	response.write "newShippingFee:" & newShippingFee & "<br>"
'	response.write "newTax:" & newTax & "<br>"
'	response.write "newGrandTotal:" & newGrandTotal & "<br><br>"
'	response.end

	nAddlGrandTotal = formatnumber(nAddlGrandTotal, 2) 
	nAddlTotalPrice = formatnumber(nAddlTotalPrice, 2) 
	nAddlShipping = formatnumber(nAddlShipping, 2) 
	nAddlTax = formatnumber(nAddlTax, 2)
	
	if not bTestmode then
		dim SoapStr
		SoapStr = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & vbcrlf
		SoapStr = SoapStr & "<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbcrlf
		SoapStr = SoapStr & "	<soap:Header>" & vbcrlf
		SoapStr = SoapStr & "		<RequesterCredentials xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
		SoapStr = SoapStr & "			<Credentials xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
		SoapStr = SoapStr & "				<Username>" & API_USERNAME & "</Username>" & vbcrlf
		SoapStr = SoapStr & "				<Password>" & API_PASSWORD & "</Password>" & vbcrlf
		SoapStr = SoapStr & "				<Signature>" & API_SIGNATURE & "</Signature>" & vbcrlf
		SoapStr = SoapStr & "			</Credentials>" & vbcrlf
		SoapStr = SoapStr & "		</RequesterCredentials>" & vbcrlf
		SoapStr = SoapStr & "	</soap:Header>" & vbcrlf
		SoapStr = SoapStr & "	<soap:Body>" & vbcrlf
		SoapStr = SoapStr & "		<DoReferenceTransactionReq xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
		SoapStr = SoapStr & "			<DoReferenceTransactionRequest>" & vbcrlf
		SoapStr = SoapStr & "				<Version xmlns=""urn:ebay:apis:eBLBaseComponents"">" & API_VERSION & "</Version>" & vbcrlf
		SoapStr = SoapStr & "				<DoReferenceTransactionRequestDetails xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
		SoapStr = SoapStr & "					<ReferenceID>" & paypalBAID & "</ReferenceID>" & vbcrlf
		SoapStr = SoapStr & "					<PaymentAction>Authorization</PaymentAction>" & vbcrlf
		SoapStr = SoapStr & "					<PaymentDetails>" & vbcrlf
		SoapStr = SoapStr & "						<OrderTotal currencyID=""USD"">" & nAddlGrandTotal & "</OrderTotal>" & vbcrlf
		SoapStr = SoapStr & "						<ItemTotal currencyID=""USD"">" & nAddlTotalPrice & "</ItemTotal>" & vbcrlf
		SoapStr = SoapStr & "						<TaxTotal currencyID=""USD"">" & nAddlTax & "</TaxTotal>" & vbcrlf
		SoapStr = SoapStr & "						<OrderDescription>" & strOrderDescription & "</OrderDescription>" & vbcrlf
		SoapStr = SoapStr & "						<ShippingTotal currencyID=""USD"">" & nAddlShipping & "</ShippingTotal>" & vbcrlf
		SoapStr = SoapStr & "					</PaymentDetails>" & vbcrlf
		SoapStr = SoapStr & "					<InvoiceID>" & orderid & "</InvoiceID>" & vbcrlf		
		SoapStr = SoapStr & "				</DoReferenceTransactionRequestDetails>" & vbcrlf
		SoapStr = SoapStr & "			</DoReferenceTransactionRequest>" & vbcrlf
		SoapStr = SoapStr & "		</DoReferenceTransactionReq>" & vbcrlf
		SoapStr = SoapStr & "	</soap:Body>" & vbcrlf
		SoapStr = SoapStr & "</soap:Envelope>" & vbcrlf
		
'		response.write "<pre>" & SoapStr & "</pre>"
'		response.end
		
		'CREATE OBJECTS
		dim objXMLDOC, objXMLDOM, oNode, strError
		set objXMLDOC = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
		set objXMLDOM = Server.CreateObject("Msxml2.DomDocument")
		set oNode = Server.CreateObject("Microsoft.XMLDOM")
		
		'MAKE THE CALL
		objXMLDOC.open "POST", API_ENDPOINT, False
		objXMLDOC.setRequestHeader "Content-Type", "text/xml"
		objXMLDOC.send(SoapStr)
		objXMLDOM.async = false
		objXMLDOM.LoadXML objXMLDOC.responseText
		
		strError = ""
		if objXMLDOM.parseError.errorCode <> 0 then
			strError = strError & "<h3>Parser error found.</h3>"
		else
			session("errorSQL") = objXMLDOC.responseText
			set oNode = objXMLDOM.getElementsByTagName("Ack")
			if (not oNode is nothing) then
				if oNode.item(0).text = "Failure" then
					set oNode = objXMLDOM.getElementsByTagName("ShortMessage")
					if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
					set oNode = objXMLDOM.getElementsByTagName("LongMessage")
					if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
					set oNode = objXMLDOM.getElementsByTagName("ErrorCode")
					if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
				end if
			else
				strError = strError & "<h3>No data received.</h3>"
			end if
		end if
		
		if strError <> "" then
			response.write strError
			response.write "<br><br><a href=""/"">Continue Shopping</a>"
			call responseEnd()
		end if

		set oNode = objXMLDOM.getElementsByTagName("CorrelationID")
		if (not oNode is nothing) then CorrelationID = oNode.item(0).text
	end if

	for i=1 to nLoopProduct
		if prepInt(request.form("chkItem"&i)) > 0 then
			qty = prepInt(request.form("chkQty"&i))
			if qty > 0 then
				itemid = prepInt(request.form("chkItem"&i))
				price = round(prepInt(request.form("itemPrice"&i)), 2)
				sql = "sp_InsertOrderDetail3 '" & orderid & "', '" & itemid & "', '" & qty & "', '" & price & "', 1"
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
		end if
	next

	'====== update order total amounts
	sql = "sp_updateExistingOrderValue '" & orderid & "', '" & formatnumber(newSubTotal, 2) & "', '" & formatnumber(newShippingFee, 2) & "', '" & formatnumber(newTax, 2) & "', '" & formatnumber(newGrandTotal, 2) & "'"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	'====== need to migrate the following sql stmt to SP later..
	sql = 	"update	we_orders" & vbcrlf & _
			"set	pp_orderGrandTotal = '" & nAddlGrandTotal & "'" & vbcrlf & _
			"	,	pp_transactionID = '" & CorrelationID & "'" & vbcrlf & _
			"where	orderid = '" & orderid & "'"			
	session("errorSQL") = sql
	oConn.execute(sql)	

	if siteid = 0 then
		if instr(siteName,"m.wire") > 0 or instr(siteName,"mdev.wire") > 0 then
			call responseRedirect("/cart/postcomplete.htm?a=" & accountid & "&o=" & orderid & "&d=" & nAddlGrandTotal & "&c=" & nAddlTotalPrice & "&s=" & nAddlShipping & "&t=" & nAddlTax & "&mobileOrder=1")
		else
			call responseRedirect("/cart/postcomplete?a=" & accountid & "&o=" & orderid & "&d=" & nAddlGrandTotal & "&c=" & nAddlTotalPrice & "&s=" & nAddlShipping & "&t=" & nAddlTax)
		end if
	else
		call responseRedirect("/cart/postcomplete.asp?a=" & accountid & "&o=" & orderid & "&d=" & nAddlGrandTotal & "&c=" & nAddlTotalPrice & "&s=" & nAddlShipping & "&t=" & nAddlTax)	
	end if
%>
