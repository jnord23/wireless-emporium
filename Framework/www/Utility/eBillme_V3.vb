<%

Class eBillme_V3
	Dim serviceURL
	Dim oXmlHTTP
	Dim username
	Dim password
	Dim payeeToken
	Dim fso
	Dim dataPath

	Sub InitWSDL()
		username = SEB_USERNAME
		password = SEB_PASSWORD
		payeeToken = SEB_PAYEE
'		serviceUrl = "https://test.modasolutions.com/axis/eBillmeServiceV3.jws?WSDL"
		serviceUrl = "https://www.modasolutions.com/axis/eBillmeServiceV3.jws?WSDL"
'		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		Set oXmlHTTP = CreateObject("Msxml2.ServerXMLHTTP.3.0")
		oXmlHTTP.Open "POST", serviceUrl, False
		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		oXmlHTTP.setRequestHeader "SOAPAction", serviceUrl
		
		Set fso = CreateObject("Scripting.FileSystemObject")
		dataPath = fso.BuildPath("C:\inetpub\wwwroot\wirelessemporium.com\www\xmlFiles", "WUPay")
	End Sub


	Function GetSoapRequestXml(methodXml, methodName)
		Dim soapRequestXml
		soapRequestXml = "<?xml version='1.0' encoding='utf-8'?>"
		soapRequestXml = soapRequestXml & " <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:def=""http://DefaultNamespace"">"
		soapRequestXml = soapRequestXml & " <soapenv:Body>"
		soapRequestXml = soapRequestXml & " <def:" & methodName & " soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">"
		soapRequestXml = soapRequestXml & " <" & methodName & "Xml xsi:type=""xsd:string""><![CDATA[" & methodXml & " ]]></" & methodName & "Xml>"
		soapRequestXml = soapRequestXml & " </def:" & methodName & ">"
		soapRequestXml = soapRequestXml & " </soapenv:Body>"
		soapRequestXml = soapRequestXml & " </soapenv:Envelope>"
		GetSoapRequestXml = soapRequestXml
	End Function


	Function GetSettlementDetails(startdate, enddate)
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode
		
		InitWSDL()
		
		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "getsettlementdetails_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getsettlementdetails_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getsettlementdetails_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getsettlementdetails_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getsettlementdetails_request/ns:startdate")
		objXmlNode.text = startdate

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getsettlementdetails_request/ns:enddate")
		objXmlNode.text = enddate
		
		methodXml = objXmlDoc.xml
		
		soapRequestXml = GetSoapRequestXml(methodXml, "getSettlementDetails")
		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		GetSettlementDetails = GetSoapResponseXml(soapResponseXml, "getSettlementDetails")
	End Function


	Function GetOrderInformation(orderid)
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode
		
		InitWSDL()

		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "getorderinformation_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderinformation_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderinformation_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderinformation_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderinformation_request/ns:orderrefid")
		objXmlNode.text = orderid

		methodXml = objXmlDoc.xml

		soapRequestXml = GetSoapRequestXml(methodXml, "getOrderInformation")
		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		GetOrderInformation = GetSoapResponseXml(soapResponseXml, "getOrderInformation")
	End Function


	Function GetOrderStatus()
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode

		InitWSDL()
		
		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "getorderstatus_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderstatus_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderstatus_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderstatus_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		methodXml = objXmlDoc.xml

		soapRequestXml = GetSoapRequestXml(methodXml, "getOrderStatus")

		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		GetOrderStatus = GetSoapResponseXml(soapResponseXml, "getOrderStatus")
	End Function
	

	Function UpdateOrder(orderid, expirydate)
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode

		InitWSDL()
		
		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "updateorderinformation_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:updateorderinformation_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:updateorderinformation_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:updateorderinformation_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:updateorderinformation_request/ns:order/ns:orderrefid")
		objXmlNode.text = orderid

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:updateorderinformation_request/ns:order/ns:expirydate")
		objXmlNode.text = expirydate
		
		methodXml = objXmlDoc.xml
		
		soapRequestXml = GetSoapRequestXml(methodXml, "updateOrderInformation")
		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		UpdateOrder = GetSoapResponseXml(soapResponseXml, "updateOrderInformation")
	End Function


	Function CancelOrder(orderid, reasonid)
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode

		InitWSDL()

		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "cancelorder_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:cancelorder_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:cancelorder_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:cancelorder_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:cancelorder_request/ns:orderrefid")
		objXmlNode.text = orderid

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:cancelorder_request/ns:reasonid")
		objXmlNode.text = reasonid
		
		methodXml = objXmlDoc.xml
		
		soapRequestXml = GetSoapRequestXml(methodXml, "cancelOrder")
		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		CancelOrder = GetSoapResponseXml(soapResponseXml, "cancelOrder")
	End Function
	

	Function SubmitRefund(orderid, refundamount, refundreason)
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode

		InitWSDL()

		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "submitrefund_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:orderrefid")
		objXmlNode.text = orderid

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:refundamount")
		objXmlNode.text = refundamount
		
		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:refundreason")
		objXmlNode.text = refundreason

		methodXml = objXmlDoc.xml
		
		soapRequestXml = GetSoapRequestXml(methodXml, "submitRefund")
		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		SubmitRefund = GetSoapResponseXml(soapResponseXml, "submitRefund")
	End Function
	
	
	Function GetSoapResponseXml(responseXml, methodName)
		Dim objXmlDoc, objXmlNode
		
		GetSoapResponseXml = ""
		
		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.LoadXml(responseXml)
		
		Set objXmlNode = objXmlDoc.selectSingleNode("//" & methodName & "Return")
		If Not (objXmlNode Is Nothing) Then
			GetSoapResponseXml = objXmlNode.Text
			Set objXmlNode = Nothing
		Else
			objXmlDoc.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'"
			Set objXmlNode = objXmlDoc.selectSingleNode("//soapenv:Fault")
			If Not (objXmlNode Is Nothing) Then
				GetSoapResponseXml = objXmlNode.Xml
				Set objXmlNode = Nothing
			End If			
		End If
		Set objXmlDoc = Nothing
	End Function

End Class

%>
