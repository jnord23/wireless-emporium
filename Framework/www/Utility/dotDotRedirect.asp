<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%'URLs ending with a period arrive at this page
'IIS catches these, while Helicon APE ISAPI Rewrite Filter (.htaccess) does not
'TODO: perform this redirection in Web.Config; Current attempts to change Rewrite value to Redirect cause entire rule to fail (2012-11-28)
dim currentRequestUrl : currentRequestUrl = request.ServerVariables("HTTP_X_ORIGINAL_URL") : output = currentRequestUrl

'Remove all the periods at the end of the URL and redirect
if right(currentRequestUrl,1) = "." then
	do while right(currentRequestUrl,1) = "."
		currentRequestUrl = left(currentRequestUrl, len(currentRequestUrl) - 1) : output = output & "<br />" & currentRequestUrl
	loop
	select case request.ServerVariables("HTTP_HOST")
		case "www.wirelessemporium.com"		
			suffix = ".asp"		
		case "www.cellularoutfitter.com"
			suffix = ".html"
		case "www.cellphoneaccents.com"
			suffix = "" '.htaccess already properly shows categories and pdp without filename extension
		case "www.phonesale.com"
			suffix = ".html"
		case else
			suffix = ""
	end select

	call PermanentRedirect(currentRequestUrl & suffix)
end if
%>
