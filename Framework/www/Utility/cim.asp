<%
function SendApiRequest(apiURL, reqString)
	dim objHTTP
	dim objResponse
	
	set objHTTP = Server.CreateObject("Msxml2.ServerXMLHTTP.6.0")
	objHTTP.open "post", apiURL, False
	objHTTP.setRequestHeader "Content-Type", "text/xml"
	objHTTP.send reqString
'	Response.Write "Raw response: " & Server.HTMLEncode(objHTTP.responseText) & "<br><br>" & vbCrLf
	
	set objResponse = Server.CreateObject("MSXML2.DOMDocument.3.0")
	objResponse.loadXML objHTTP.responseText
	objResponse.setProperty "SelectionNamespaces", "xmlns:api='AnetApi/xml/v1/schema/AnetApiSchema.xsd'"
	
	set SendApiRequest = objResponse
end function


function IsApiResponseSuccess(objResponse)
	if "Ok" = objResponse.selectSingleNode("/*/api:messages/api:resultCode").Text then
		IsApiResponseSuccess = true
	else
		IsApiResponseSuccess = false
	end if
end function	
%>