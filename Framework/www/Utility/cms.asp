<!-- METADATA TYPE="typelib" NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<%
	set fs = CreateObject("Scripting.FileSystemObject")
	
	if inStr(cms_basepage,"?") > 0 then cms_basepage = left(cms_basepage,inStr(cms_basepage,"?")-1) 
	if inStr(cms_rewriteURL,"?") > 0 then cms_rewriteURL = left(cms_rewriteURL,inStr(cms_rewriteURL,"?")-1) 
	
	if request.form("myAction") <> "" then
		cms_updateType = prepStr(request.form("updateType"))
		cms_SEtitle = prepStr(replace(request.form("SEtitle"), ",", "!##!"))
		cms_SEdescription = prepStr(replace(request.form("SEdescription"), ",", "!##!"))
		cms_SEkeywords = prepStr(replace(request.form("SEkeywords"), ",", "!##!"))
		cms_SeTopText = prepStr(replace(request.form("SeTopText"), ",", "!##!"))
		cms_SeBottomText = prepStr(replace(request.form("SeBottomText"), ",", "!##!"))
		cms_SeH1 = prepStr(replace(request.form("SeH1"), ",", "!##!"))
		cms_useGeneric = prepInt(request.form("useGeneric"))

		if cms_SEtitle = "" then cms_SEtitle = "[BLANK TEXT]"
		if cms_SEdescription = "" then cms_SEdescription = "[BLANK TEXT]"
		if cms_SEkeywords = "" then cms_SEkeywords = "[BLANK TEXT]"
		if cms_SeTopText = "" then cms_SeTopText = "[BLANK TEXT]"
		if cms_SeBottomText = "" then cms_SeBottomText = "[BLANK TEXT]"
		if cms_SeH1 = "" then cms_SeH1 = "[BLANK TEXT]"
		
		metaString = cms_SEtitle & "," & cms_SEdescription & "," & cms_SEkeywords & "," & cms_SeTopText & "," & cms_SeBottomText & "," & cms_SeH1
		response.write "<div style=""float:left; width:100%; margin:20px 0px 20px 0px; color:#000;"">"
		response.write "cms_siteid:" & cms_siteid & "<br>"
		response.write "cms_basepage:" & cms_basepage & "<br>"
		response.write "cms_rewriteURL:" & cms_rewriteURL & "<br>"
		response.write "metaString:" & metaString & "<br>"
		response.write "cms_updateType:" & cms_updateType & "<br>"
		response.write "cms_useGeneric:" & cms_useGeneric & "<br>"
		response.write "</div>"
'		response.end
		
		Dim cmd
		Set cmd = Server.CreateObject("ADODB.Command")
		Set cmd.ActiveConnection = oConn
		cmd.CommandText = "CMSUpdate"
		cmd.CommandType = adCmdStoredProc 
	
		cmd.Parameters.Append cmd.CreateParameter("ret", adInteger, adParamReturnValue)
		cmd.Parameters.Append cmd.CreateParameter("p_siteid", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_basepage", adVarChar, adParamInput, 1000)
		cmd.Parameters.Append cmd.CreateParameter("p_rewriteURL", adVarChar, adParamInput, 1000)
		cmd.Parameters.Append cmd.CreateParameter("p_metaString", adVarChar, adParamInput, 8000)
		cmd.Parameters.Append cmd.CreateParameter("p_updateType", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_useGeneric", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_adminid", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_ipaddress", adVarChar, adParamInput, 100)
		cmd.Parameters.Append cmd.CreateParameter("p_compressedPage", adVarChar, adParamOutput, 1000)
	
		cmd("p_siteid")		=	cms_siteid
		cmd("p_basepage")	=	cms_basepage
		cmd("p_rewriteURL")	=	cms_rewriteURL
		cmd("p_metaString")	=	metaString
		cmd("p_updateType")	=	cms_updateType
		cmd("p_useGeneric")	=	cms_useGeneric
		cmd("p_adminid")	=	session("adminID")
		cmd("p_ipaddress")	=	Request.ServerVariables("HTTP_X_FORWARDED_FOR")		
	
		cmd.Execute
	
		cmsSuccess			= 	cmd("ret")
		cms_compressedPage 	= 	cmd("p_compressedPage")
		
		if cmsSuccess <> 0 then
			response.write "<div style=""float:left; width:100%; margin:20px 0px 20px 0px; color:#000;"">Error occurred while updating.</div>"
		else
			if not isnull(cms_compressedPage) then
				cms_compressedPage = replace(replace(replace(cms_compressedPage, "[BRANDID]", brandID), "[MODELID]", modelID), "[CATEGORYID]", categoryID)
				cms_compressedPage = replace(replace(cms_compressedPage, "[MODELNAME]", brandModel), "[ITEMID]", itemID)
	
				cms_compPath = server.MapPath("\compressedPages" & cms_compressedPage)
				if fs.fileExists(cms_compPath) then 
					fs.deleteFile(cms_compPath)
'					response.write cms_compPath
				end if
			end if
			
			Randomize
			if inStr(cms_rewriteURL,"?") > 0 then 
				cms_rewriteURL = cms_rewriteURL & "&cmsid=" & (Int((2347899) * Rnd + 1))
			else
				cms_rewriteURL = cms_rewriteURL & "?cmsid=" & (Int((2347899) * Rnd + 1))
			end if
			response.write cms_rewriteURL
			response.redirect cms_rewriteURL
		end if
		response.end		
	end if
	
	'first recordset: specific, second recordset: generic
	sql = "CMSGet '" & prepInt(cms_siteid) & "', '" & prepStr(cms_basepage) & "', '" & prepStr(cms_rewriteURL) & "', 0"
	set cmsRS = oConn.execute(sql)
%>
	<div style="float:left; padding:5px 20px 0px 20px;"><a href="javascript:showMetaTags()" style="color:#FFF; font-weight:bold; font-size:12px;">Specific Meta Tags</a></div>
	<div style="float:left; padding:5px 20px 0px 20px;"><a href="javascript:showDefaultMetaTags()" style="color:#FFF; font-weight:bold; font-size:12px;">Generic Meta Tags</a></div>
    <%
	if cmsRS.state = 1 then
		dim arrTagName(5)
		arrTagName(0) = "SEtitle"
		arrTagName(1) = "SEdescription"
		arrTagName(2) = "SEkeywords"
		arrTagName(3) = "SeTopText"
		arrTagName(4) = "SeBottomText"
		arrTagName(5) = "SeH1"
	
		recordsetLoop = 0
		do until cmsRS is nothing
			recordsetLoop = recordsetLoop + 1
			if recordsetLoop = 1 then			'======specific
				myAction = "Save Meta Tags"
				metagBoxID = "metaTagBox"
			elseif recordsetLoop = 2 then		'======generic
				myAction = "Save Default Meta Tags"
				metagBoxID = "defaultMetaTags"
			end if
			
			%>
			<div id="<%=metagBoxID%>" style="float:left; position:absolute; top:28px; left:0px; width:550px; background-color:#FFF; border:1px solid #000; color:#000; padding-top:20px; z-index:999; display:none;">
            	<div style="float:left; width:100%;">
                <%
					response.write "cms_siteid :" & cms_siteid & "<br>"
					response.write "cms_basepage: " & cms_basepage & "<br>"
					response.write "cms_rewriteURL: " & cms_rewriteURL & "<br>"
				%>
                </div>
				<form action="<%=cms_post%>" method="post">
                <%
				for i=0 to 5
					tagName = arrTagName(i)
					tagDbValue = ""
					if not cmsRS.eof then
						tagDbValue = cmsRS(tagName)
						useGeneric = cmsRS("useGeneric")
					end if
				%>
                <div style="float:left; width:100%; margin-top:10px;">
                    <div style="float:left; width:100px; text-align:right;"><%=tagName%>:</div>
                    <div style="float:left; width:350px; text-align:right;"><textarea name="<%=tagName%>" cols="40" rows="4" onkeyup="document.getElementById('<%=tagName%>_length').innerHTML='Length: ' + document.metaForm.<%=tagName%>.value.length"><%=tagDbValue%></textarea></div>
                    <div style="float:left; width:50px; margin-left:5px;" id="<%=tagName%>_length">Length: <%=len(tagDbValue)%></div>
                </div>
                <%
				next
				
				if recordsetLoop = 1 then	'specific one
				%>
                <input type="hidden" name="updateType" value="0" />
                <div style="float:left; width:200px; margin:10px 0px 10px 10px;"><input type="checkbox" name="useGeneric" value="1"<% if useGeneric then %> checked="checked"<% end if %> /> Use Generic Meta Tags</div>
                <%
				else
				%>
				<input type="hidden" name="updateType" value="1" />
                <%
				end if
				%>
                <div style="float:right; width:200px; text-align:right; margin:10px 10px 10px 0px;">
                    <input type="submit" name="myAction" value="<%=myAction%>" />
                </div>
                <input type="hidden" name="adminBar" value="1" />
                <input type="hidden" name="brandModel" value="<%=brandModel%>" />
                <input type="hidden" name="itemID" value="<%=itemID%>" />
                <input type="hidden" name="brandID" value="<%=brandID%>" />
                <input type="hidden" name="modelID" value="<%=modelID%>" />
                <input type="hidden" name="categoryID" value="<%=categoryID%>" />
                <input type="hidden" name="cms_siteid" value="<%=cms_siteid%>" />
                <input type="hidden" name="cms_basepage" value="<%=cms_basepage%>" />
                <input type="hidden" name="cms_rewriteURL" value="<%=cms_rewriteURL%>" />
				</form>
            </div>
            <%
			set cmsRS = cmsRS.nextrecordset
		loop
	end if	'cms.state
%>