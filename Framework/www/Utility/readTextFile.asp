<%
	function readTextFile(fileName)
		Dim strContents	: strContents = ""
	
		if fileName <> "" then
			Const ForReading = 1, ForWriting = 2, ForAppending = 3
			Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
	
			Dim FSO, Filepath
			set FSO = createObject("Scripting.FileSystemObject")
			Filepath = fileName
	
			if FSO.FileExists(Filepath) Then
				Dim TextStream
				Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)
	
				' Read file in one hit
				strContents = TextStream.ReadAll
	
				TextStream.Close
				Set TextStream = nothing
			End If
	
			Set FSO = Nothing
		End If 
	
		readTextFile = strContents
	end function
%>