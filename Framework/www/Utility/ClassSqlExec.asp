<%
'	basic usage:
'		dim objSql: set objSql = new SqlExec
'		objSql.Sql = "select [TypeId], [TypeName] from we_types with(nolock)"
'		objSql.Execute()
'		while( objSql.Read())
'			call dbg( "TypeName", objSql.Item( "TypeName"))
'		wend
'		set objSql=nothing
const Const_SqlExec_OpenStatic=3
const Const_SqlExec_ConstLockReadOnly=1

class SqlExec
	public Sql
	public LegacyReadMode ' use this mode when handling text, ntext columns
	private m_objConn
	private m_objRs

	private m_blnFirstIteration

	private sub class_initialize() '--constructor (executes on new keyword)
		Sql=EMPTY_STRING
		LegacyReadMode = false
		set m_objConn = nothing
		set m_objRs = nothing

		m_blnFirstIteration=true
	end sub

	'************ Property Get **************
	public property get Item( byval strColumnName)
		Item = EMPTY_STRING

		if not m_objRs is nothing then Item = Obj2Str( m_objRs( strColumnName).Value)
	end property

	public property get HasRows
		HasRows = false
		if not m_objRs is nothing then
			HasRows = not m_objRs.EOF
		end if
	end property

	'****************************************

	sub Execute()
		if m_objConn is nothing then set m_objConn = GetNewConn() 'opens a new connection
		'call dbg( "sql", sql): response.end

		Session("errorSQL") = Sql
		if LegacyReadMode then
			set m_objRs = Server.CreateObject( "ADODB.ResultSet")
			call m_objRs.open( Sql, m_objConn, Const_SqlExec_OpenStatic, Const_SqlExec_ConstLockReadOnly)
		else
			set m_objRs = m_objConn.Execute( Sql)
		end if
	end sub

' sample code
'dim objSqlExecReview: set objSqlExecReview = GetReview( "15940", 3)
'dim strJson: strJson = objSqlExecReview.ExecuteJson()
'set objSqlExecReview = nothing *>
'<script>
'	var objJson = eval( '('+<*=JsStr( strJson) *>+')')
'	//or assdirectly ...
'	var objJson = <*=strJson*>;
'</script>
	public function ExecuteJson()
		Execute()

		dim strReturnValue: strReturnValue = STRING_EMPTY

		while( Read())
			dim strSubReturnValue: strSubReturnValue = STRING_EMPTY
			for each objColumn in m_objRs.Fields
				if HasLen( strSubReturnValue) then strSubReturnValue = strSubReturnValue &","
				strSubReturnValue = strSubReturnValue &DBQT& objColumn.Name &DBQT&":"& JsStr( Replace( Replace( Item( objColumn.Name), "'", "&#39;"), VbCrLf, "<br />"))
			next
			if HasLen( strReturnValue) then strReturnValue = strReturnValue &","
			strReturnValue = strReturnValue & "{"& strSubReturnValue &"}"
		wend

		Destroy()
		ExecuteJson = "{ " &DBQT& "data" &DBQT& ": ["& strReturnValue &"]}"
	end function

	function Read()
		if not m_blnFirstIteration then
			m_objRs.movenext
		else 
			m_blnFirstIteration = false
		end if

		Read = not m_objRs.EOF

		if not Read then Destroy()
	end function

	private sub class_terminate() '--destructor (executes when object set to nothing)
		call Destroy()
	end sub

	private sub Destroy()
		call CloseRs( m_objRs)
		call CloseConn( m_objConn)
	end sub

end class
%>