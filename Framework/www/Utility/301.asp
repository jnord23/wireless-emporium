<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
if instr(request.ServerVariables("HTTP_HOST"), "cellularoutfitter.com") > 0 then
	useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	if instr(useURL, "-music-skins") > 0 and instr(useURL, ".html") = 0 then
		if right(useURL, 7) = "-artist" then
			call PermanentRedirect(useURL & "-0-all.html")
		elseif right(useURL, 12) = "-music-skins" then
			call PermanentRedirect(useURL & "-genres.html")
		end if
	end if
elseif instr(request.ServerVariables("HTTP_HOST"), "wirelessemporium.com") > 0 then
	useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	if instr(useURL, "-music-skins") > 0 and instr(useURL, ".asp") = 0 then
		if right(useURL, 7) = "-artist" then
			call PermanentRedirect(useURL & "-0-all.asp")
		elseif right(useURL, 12) = "-music-skins" then
			call PermanentRedirect(useURL & "-genre.asp")
		end if
	end if
end if
%>
