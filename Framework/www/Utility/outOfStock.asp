<%
	function outOfStockEmail(nPartNumber,curQty,nProdQuantity)	
		strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\admin\scheduled_tasks\outOfStock.html")
		strTemplate = replace(strTemplate,"#nPartNumber#",nPartNumber)
		strTemplate = replace(strTemplate,"#curDate#",now)
		strTemplate = replace(strTemplate,"#curQty#",curQty)
		strTemplate = replace(strTemplate,"#orderQty#",nProdQuantity)
		
		sql = "select top 10 (select top 1 itemPic from we_Items where partNumber = '" & nPartNumber & "') as itemPic, vendorCode, vendorPN, orderAmt, receivedAmt, complete, processOrderDate, completeOrderDate from we_vendorOrder where partNumber = '" & nPartNumber & "' and process = 1 order by id desc"
		session("errorSQL") = sql
		set rsStock = oConn.execute(sql)
		
		if rsStock.EOF then
			sql = "select top 1 itemPic from we_Items where partNumber = '" & nPartNumber & "'"
			session("errorSQL") = sql
			set rsStock = oConn.execute(sql)
			
			strTemplate = replace(strTemplate,"#prodPic#",rsStock("itemPic"))
			rsStock.movenext
		else
			strTemplate = replace(strTemplate,"#prodPic#",rsStock("itemPic"))	
		end if
		
		purchDetails = "<table border=1 bordercolor=#000000 cellpadding=3 cellspacing=0><tr bgColor='#333333' style='color:#fff; font-weight:bold;'><td>Vendor</td><td>OrderAmt</td><td>ReceivedAmt</td><td>PurchaseDate</td></tr>"
		bgColor = "#ffffff"
		purchLap = 0
		do while not rsStock.EOF
			if not isnull(rsStock("vendorCode")) then
				purchLap = purchLap + 1
				vendorCode = rsStock("vendorCode")
				orderAmt = rsStock("orderAmt")
				receivedAmt = rsStock("receivedAmt")
				orderComplete = rsStock("complete")
				if not orderComplete then receivedAmt = "Pending"
				purchaseDate = rsStock("processOrderDate")
				purchDetails = purchDetails & "<tr bgColor='" & bgColor & "'><td>" & vendorCode & "</td><td align='right'>" & orderAmt & "</td><td align='right'>" & receivedAmt & "</td><td align='right'>" & purchaseDate & "</td></tr>"
				if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
			end if
			rsStock.movenext
		loop
		if purchLap = 0 then purchDetails = purchDetails & "<tr><td colspan='4' align='center' style='font-weight:bold;'>No Purchase Data Found</td></tr>"
		purchDetails = purchDetails & "</table>"
		strTemplate = replace(strTemplate,"#purchHistory#",purchDetails)
		
		sql = "select top 30 sum(quantity) as totalSales, CONVERT(varchar(11), a.entrydate, 101) as saleDate from we_orderDetails a left join we_Items b on a.itemid = b.itemID left join we_orders c on a.orderid = c.orderID where b.PartNumber = '" & nPartNumber & "' and c.approved = 1 and c.cancelled is null and c.parentOrderID is null and CONVERT(varchar(11), a.entrydate, 101) > '" & date-30 & "' group by CONVERT(varchar(11), a.entrydate, 101) order by CONVERT(varchar(11), a.entrydate, 101)"
		session("errorSQL") = sql
		set rsStock = oConn.execute(sql)
		
		salesDetails = "<table border=1 bordercolor=#000000 cellpadding=0 cellspacing=0><tr bgColor='#333333'><td align='center' style='font-weight:bold; color:#fff; padding:10px;'>Last 30 Days of Sales</td></tr><tr><td valign='bottom'><div style='float:left; vertical-align:bottom;'>"
		salesLap = 0
		curDate = date - 29
		do while not rsStock.EOF
			salesLap = salesLap + 1
			totalSales = rsStock("totalSales")
			saleDate = rsStock("saleDate")
			
			if cdate(saleDate) <> cdate(curDate) then
				salesDetails = salesDetails & "<div style='float:left; height:11px; width:15px; padding-left:5px;'><div style='width:15px; height:10px; text-align:center;'>0</div><div style='background-color:#00F; height:1px; width:15px;' title='" & curDate & "'></div></div>"
			else
				salesDetails = salesDetails & "<div style='float:left; height:" & (totalSales*10)+10 & "px; width:15px; padding-left:5px; vertical-align:bottom;'><div style='width:15px; height:10px; text-align:center;'>" & totalSales & "</div><div style='background-color:#00F; height:" & (totalSales*10) & "px; width:15px;' title='" & saleDate & "'></div></div>"
				rsStock.movenext
			end if
			curDate = curDate + 1
			if salesLap > 29 then exit do
		loop
		if salesLap = 0 then salesDetails = salesDetails & "<div style='font-weight:bold; width:200px; text-align:center;'>No Sales Data Found</div>"
		salesDetails = salesDetails & "</div></td></tr></table>"
		strTemplate = replace(strTemplate,"#salesHistory#",salesDetails)
		
		cdo_from = "warehouseAI@wirelessemporium.com"
		cdo_subject = nPartNumber & " is out of stock!"
		cdo_body = strTemplate
		cdo_to = emailList("Out Of Stock")
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
	end function
%>