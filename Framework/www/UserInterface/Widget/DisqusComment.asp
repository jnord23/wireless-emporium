<%
'disqus comment widget per hlee [knguyen/20110615]

function GetDisqusUniquePageId( byval strPhysicalAbsolutePath, byval strArticleId)
	GetDisqusUniquePageId=GetSiteID()&"_"&replace( replace( mid( strPhysicalAbsolutePath, 2, len( strPhysicalAbsolutePath)-1), ".asp", EMPTY_STRING), "/", ",")&"_a."&strArticleId
end function

'OutputDisqusWidget( GetVirtualAbsolutePath(), GetPhysicalAbsolutePath(), request("a"))
function OutputDisqusWidget( byval strVirtualAbsolutePath, byval strPhysicalAbsolutePath, byval strArticleId)
 %>
<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'cablog';

    // The following are highly recommended additional parameters. Remove the slashes in front to use.
    var disqus_identifier = '<%=GetDisqusUniquePageId( strPhysicalAbsolutePath, strArticleId)%>';
    var disqus_url = '<%=replace(GetCurrentDomainFull(), "dev.", "staging.")&strVirtualAbsolutePath%>';

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a><%
end function %>