function ajax(newURL,rLoc) {
	if (newURL.indexOf("?") > 0) {
		newURL = newURL + '&ajaxID=' + Math.floor(Math.random()*999999)
	}
	else {
		newURL = newURL + '?ajaxID=' + Math.floor(Math.random()*999999)
	}
	var httpRequest;
	if (window.XMLHttpRequest) { // Mozilla, Safari, ...
		httpRequest = new XMLHttpRequest();
		if (httpRequest.overrideMimeType) {
			httpRequest.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) { // IE
		httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}
	httpRequest.open('GET', newURL, true);
	httpRequest.onreadystatechange = function(){
		if (httpRequest.readyState == 4) {
			if (httpRequest.status == 200) {
				badRun = 0
				var rVal = httpRequest.responseText
				if (rVal == "") {
					//alert("No data available")
				}
				else if (rVal == "refresh") {
					curLoc = window.location
					window.location = curLoc
				}
				else if(document.getElementById(rLoc)) {
					document.getElementById(rLoc).innerHTML = rVal
				}
				rVal = null;
			}
			else {
				//document.getElementById("testZone").innerHTML = newURL
			}
		}
	};
	httpRequest.send(null);
}

if (typeof isMasterPage == "undefined") { isMasterPage = 0 }
if (isMasterPage == 0) {
	var freshCode = Math.floor(Math.random() * 999999999)
	ajax('/ajax/dynamicUpdate.asp?adminBar=1&curPage=' + curPage + '&basePageName=' + basePageName + '&codeID=' + freshCode + '&cms_basepage=' + cms_basepage,'adminBar')
}

function showMetaTags() {
	if (document.getElementById('metaTagBox').style.display == '') {
		document.getElementById('metaTagBox').style.display = 'none'
	}
	else {
		document.getElementById('defaultMetaTags').style.display = 'none'
		document.getElementById('metaTagBox').style.display = ''
	}
}

function showDefaultMetaTags() {
	if (document.getElementById('defaultMetaTags').style.display == '') {
		document.getElementById('defaultMetaTags').style.display = 'none'
	}
	else {
		document.getElementById('metaTagBox').style.display = 'none'
		document.getElementById('defaultMetaTags').style.display = ''
	}
}