<%

'html rendering helper functions

function GetHtmlDropdownOptionSet( byref plValueName, byval strSelectedValue)
	GetHtmlDropdownOptionSet = EMPTY_STRING
	for each strValueName in plValueName.AsArray
		dim lstValueName: lstValueName = split( strValueName, "=")
		dim strSelectedAttribute: strSelectedAttribute = EMPTY_STRING
		if IsEqual( lstValueName( 1), strSelectedValue) then strSelectedAttribute=" selected"
		GetHtmlDropdownOptionSet = GetHtmlDropdownOptionSet &"<option value="""& lstValueName(1) &""""& strSelectedAttribute &">"& lstValueName(0) &"</a>"
	next 
end function

'render the tag attributes to avoid typos [knguyen/20110602]
function GetHtmlIdName( byval strFieldName)
	GetHtmlIdName="id="""& strFieldName &""" name="""& strFieldName &""""
end function

%>