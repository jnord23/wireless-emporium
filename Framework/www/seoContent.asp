<%
' Application Settings
'dbug = false
if dbug = "" and instr(Request.ServerVariables("HTTP_HOST"), "staging.") > 0 then dbug = true
if dbug then response.write "{WhoIsThis:" & "/Framework/seoContent.asp" & "}"



' Common Variables
intTemplates = 3
if dbug then response.write "{intTemplates:" & intTemplates & "}"

intArr = intTemplates - 1
if dbug then response.write "{intArr:" & intArr & "}"

'dim Templates() 'Do not dim BECAUSE if this file is included more than once in the same script, then redefined error will occur
redim Templates(intArr)

intDefault = 0 'First item in array
if dbug then response.write "{intDefault:" & intDefault & "}"

intUseTemplate = 0 'First item in array
if dbug then response.write "{intUseTemplate:" & intUseTemplate & "}"



' TOKEN LEGEND
' * * * TODO * * *		Check for querystring values (X, Y, Z, P)
if XXX = "" then 'Brand
	if prepStr(request.QueryString("x")) <> "" then
		XXX = prepStr(request.QueryString("x"))
	else
		XXX = "XXX"
	end if 
end if
if dbug then response.write "{XXX:" & XXX & "}"
if YYY = "" then 'Model
	if prepStr(request.QueryString("y")) <> "" then
		YYY = prepStr(request.QueryString("y"))
	else
		YYY = "YYY"
	end if 
end if
if dbug then response.write "{YYY:" & YYY & "}"
if ZZZ = "" then 'Category
	if prepStr(request.QueryString("z")) <> "" then
		ZZZ = prepStr(request.QueryString("z"))
	else
		ZZZ = "ZZZ"
	end if 
end if
if dbug then response.write "{ZZZ:" & ZZZ & "}"
if PPP = "" then 'Product
	if prepStr(request.QueryString("p")) <> "" then
		PPP = prepStr(request.QueryString("p"))
	else
		PPP = "PPP"
	end if 
end if
if dbug then response.write "{PPP:" & PPP & "}"



'AJAX Development has been abandoned because random paragraphs are not necessary
'This entire include is to be replaced with the CMS Tool
if Request.ServerVariables("SCRIPT_NAME") = "/Framework/seoContent.asp" then 'AJAX
	if dbug then response.write "{AJAX}"
	currentSite = lcase(prepStr(request.QueryString("cs")))
	if dbug then response.write "{currentSite:" & currentSite & "}"
	currentPage = lcase(prepStr(request.QueryString("cp")))
	if dbug then response.write "{currentPage:" & currentPage & "}"	
	randomize : intUseTemplate = Int((Rnd * intArr) + 1) 'Return a random
	if dbug then response.write "{intUseTemplate:" & intUseTemplate & "}"	
else 'INCLUDE
	if dbug then response.write "{INCLUDE}"
	currentSite = Request.ServerVariables("HTTP_HOST")
	if dbug then response.write "{currentSite:" & currentSite & "}"
	currentPage = Request.ServerVariables("SCRIPT_NAME")
	if dbug then response.write "{currentPage:" & currentPage & "}"	
end if



' CONTENT Building Section
' * * * TODO * * *		MVT?
if instr(currentSite, "wirelessemporium.com") then 'WE
	if dbug then response.write "{WE}"
	
	if currentPage = "/index_mp.asp" then 'Home
		if dbug then response.write "{Home}"
		'No Content for this page
	end if
	
	if currentPage = "/category.asp" then 'Category
		if dbug then response.write "{Category}"
		Templates(0) = Tokenize("Optimize your mobile phone with our premium ZZZ. We offer the largest selection of quality ZZZ online.")
		Templates(1) = Tokenize("Maximize your cell phone's potential with a quality ZZZ. With the largest variety of cell phone accessories, we guarantee that we have just the right product for you.")
		Templates(2) = Tokenize("With our wide assortment of elegant ZZZ, you no longer have to compromise style for functionality.")
	end if
	
	if currentPage = "/brand_mp.asp" then 'Brand
		if dbug then response.write "{Brand}"
		Templates(0) = Tokenize("Stay connected with the largest selection of XXX accessories online.")
		Templates(1) = Tokenize("Shop from the leading source of premium XXX accessories.")
		Templates(2) = Tokenize("Looking to accessorize your XXX phone? Shop with confidence from the #1 trusted source of quality cell phone accessories online.")
	end if
	
	if currentPage = "/brand-model.asp" then 'Brand-Model
		if dbug then response.write "{Brand-Model}"
		Templates(0) = Tokenize("If you're looking to accessorize your XXX YYY, look no further than Wireless Emporium, the leading online retailer of cell phone accessories. Offering the largest variety of quality tech accessories, we ensure you quality products at the best prices.")
		Templates(1) = Tokenize("Supplement your XXX YYY with top-notch accessories. Shop with confidence and ease with Wireless Emporium , the go-to online destination for the finest cell phone accessories.")
		Templates(2) = Tokenize("Choose from the latest and greatest in technological innovation here at Wireless Emporium. We offer superior XXX YYY accessories at value prices.")
	end if
	
	if currentPage = "/brand-model-category.asp" then 'Brand-Model-Category
		if dbug then response.write "{Brand-Model-Category}"
		Templates(0) = Tokenize("Shopping for PPP for your XXX YYY? Here at Wireless Emporium, we offer the most extensive collection of PPP online; we guarantee it.")
		Templates(1) = Tokenize("If you're looking for just the right PPP for your XXX YYY, you've come to the right place. As the leading online retailer for cell phone accessories, we guarantee that we have the perfect PPP for you.")
		Templates(2) = Tokenize("Wireless Emporium offers the largest variety of cell phone accessories at the sweetest prices online. Browse our collection of PPP for your XXX YYY!")
	end if
end if

if instr(currentSite, "cellphoneaccents.com") then 'CA
	if dbug then response.write "{CA}"
	
	if currentPage = "/includes/template/bottomHTML.asp" then 'bottomHTML
		if dbug then response.write "{bottomHTML}"
		'There is no longer going to be content on this page
	end if
		
	if currentPage = "/category.asp" then 'Category
		if dbug then response.write "{Category}"
		Templates(0) = Tokenize("Keep stylish with these ZZZ. Cellphone Accents is the go-to source for quality cell phone accessories at the best prices.")
		Templates(1) = Tokenize("Here at Cellphone Accents, we believe in expressing your cell phone. Make just the right statement with our selection of ZZZ.")
		Templates(2) = Tokenize("Save big on quality cell phone accessories at Cellphone Accents. Protect your investment with our large variety of ZZZ.")
	end if

	if currentPage = "/brand.asp" then 'Brand
		if dbug then response.write "{Brand}"
		Templates(0) = Tokenize("Score the best accessories for your XXX phone here at CellphoneAccents. From car chargers to stylish faceplates, we offer premium tech products at the best prices online.")
		Templates(1) = Tokenize("Accessorize your XXX phone with premium cell phone accessories. Choose from our wide selection of products and shop with confidence.")
		Templates(2) = Tokenize("If you're hunting for the perfect accessories for your XXX phone, look no further than Cellphone Accents. We offer the largest selection of cell phone accessories online at the best prices!")
	end if
	
	' "Explore Our Selection"
	if currentPage = "/brand-model.asp" or currentPage = "/brand-model-category.asp" then 'Category
		if dbug then response.write "{BM|BMC}"
		Templates(0) = Tokenize("What makes Cellphone Accents standout from the rest?</p>" &_ 
						"&nbsp; &bull; A flat-rate shipping fee of $2.99: No hidden shipping fees, no matter how large the order.<br />" &_
						"&nbsp; &bull; An unrivaled 90-day guarantee: We want to make sure that you're completely satisfied with your purchase.<br />" &_ 
						"&nbsp; &bull; A 365-day warranty: We stand behind the quality of our products. If you encounter a technical issue with your product, we will happily issue a no-hassle exchange.<br />" & _
						"<p>With the largest selection of cell phone accessories online coupled with top-notch customer service, it's a no-brainer why Cellphone Accents is the leading retail destination for all your tech needs.")
		Templates(1) = Tokenize("Only at Cellphone Accents can you get quality cell phone accessories at premium factory-direct prices. To sweeten the deal, we stand by our products with a full one year warranty and a 90-day satisfaction guarantee.")
		Templates(2) = Tokenize("Choose from Cellphone Accent's variety of innovative products and pay below-retail prices. Shop for your XXX YYY with confidence?�we offer $2.99 flat-rate shipping and an unbeatable 90-day satisfaction guarantee.")
	end if

end if

if instr(currentSite, "cellularoutfitter.com") then 'CO
	if dbug then response.write "{CO}"
	
	if currentPage = "/index_mp.asp" then 'Home
		if dbug then response.write "{Home}"
		'No Content for this page
	end if
	
	if currentPage = "/category.asp" then 'Category
		if dbug then response.write "{Category}"
		Templates(0) = Tokenize("Who says you have to pay top dollar for premium products? Save big on ZZZ with Cellular Outfitter.")
		Templates(1) = Tokenize("Here at Cellular Outfitter, we offer the latest in tech products at rock-bottom prices. Shop our endless inventory for the right ZZZ for your mobile device.")
		Templates(2) = Tokenize("We've combined two of our favorite things: innovative tech accessories and warehouse prices. Shop Cellular Outfitter's ZZZ at the lowest prices guaranteed.")
	end if
	
	if currentPage = "/brand.asp" then 'Brand
		if dbug then response.write "{Brand}"
		'No content assigned here
	end if
	
	if currentPage = "/brand-model.asp" then 'Brand-Model
		if dbug then response.write "{Brand-Model}"
		Templates(0) = Tokenize("Find the top-rated accessories for your XXX YYY. We offer the widest variety of quality products at lowest prices online.")
		Templates(1) = Tokenize("Accessorize your XXX YYY with the very best in cell phone accessories. You name it, we have it!")
		Templates(2) = Tokenize("At Cellular Outfitter, we sell only the highest quality accessories for your XXX YYY at unbeatable prices.")
	end if
	
	if currentPage = "/brand-model-category.asp" or currentPage = "/brand-model-category-b.asp" then 'Brand-Model-Category
		if dbug then response.write "{Brand-Model-Category}"
		Templates(0) = Tokenize("Find the right ZZZ for your XXX YYY at Cellular Outfitter. With our 110% low price guarantee, you'll never find a better deal.")
		Templates(1) = Tokenize("Pick up the perfect ZZZ for your XXX YYY. We offer the largest inventory of cell phone accessories at the cheapest prices online.")
		Templates(2) = Tokenize("Looking for ZZZ for your XXX YYY? Shop our selection of premium products at our unprecedented discount prices!")
	end if
end if


if instr(currentSite, "phonesale.com") then 'PS
	if dbug then response.write "{PS}"
	
	if currentPage = "/index_mp.asp" then 'Home
		if dbug then response.write "{Home}"
		'No Content for this page
	end if
	
	if currentPage = "/brand.asp" then 'Category
		if dbug then response.write "{Category}"
		Templates(0) = Tokenize("Protect your investment with one of our top-notch ZZZ. At PhoneSale, we aim to provide the best shopping experience to our loyal customers.")
		Templates(1) = Tokenize("We guarantee that our broad inventory will meet all your tech needs. Need proof? Check out our extensive collection of ZZZ.")
		Templates(2) = Tokenize("Shop from our wide array of ZZZ and receive your products in no time with our fast and free shipping.")
	end if
	
	'No more on brands...
	
	if currentPage = "/brand-model.asp" then 'Brand-Model
		if dbug then response.write "{Brand-Model}"
		Templates(0) = Tokenize("Here at PhoneSale, we offer the very best accessories for your XXX YYY. Check out our large assortment of quality cell phone accessories.")
		Templates(1) = Tokenize("Looking to accessorize your XXX YYY? Shop with PhoneSale, the leading online retailer for all your cellular needs.")
		Templates(2) = Tokenize("Accessorize your XXX YYY with one of PhoneSale's premium tech products. We offer everything for your PPP at the best prices online.")
	end if
	
	if currentPage = "/brand-model-category.asp" then 'Brand-Model-Category
		if dbug then response.write "{Brand-Model-Category}"
		Templates(0) = Tokenize("At PhoneSale.com, we offer the most extensive collection of XXX YYY ZZZ accessories online.")
		Templates(1) = Tokenize("Choose from our huge selection of quality PPP for your XXX YYY.")
		Templates(2) = Tokenize("PhoneSale.com offers the most expansive collection of XXX YYY ZZZ at the best prices online. Shop now to snag our great deals today!")
	end if
end if




' INCLUDE Section
' When the file is included in an ASP page, this is what will be rendered to the search engine crawler

if Templates(intUseTemplate) <> "" then
%>
<%if not nop then response.write "<p>"%><%=Templates(intUseTemplate)%><%if not nop then response.write "</p>"%>
<%
end if






' FUNCTIONS
function Tokenize(raw)
	dim sb : sb = raw
	sb = replace(sb, "XXX", XXX)
	sb = replace(sb, "YYY", YYY)
	sb = replace(sb, "ZZZ", ZZZ)
	sb = replace(sb, "PPP", PPP)
	Tokenize = sb
end function
function prepStr(strVal)
	if isnull(strVal) or len(strVal) < 1 then strVal = ""
	prepStr = trim(ds(strVal))
end function
function ds(strValue)
	if len(strValue) > 0 then
		strContent = trim(replace(strValue,chr(10),""))
		strContent = replace(strContent,vbcrlf,"")
		strContent = replace(strContent,chr(13),"")
		if not isNull(strContent) and strContent <> "" then
'			strContent = replace(strContent,">","&gt;")
'			strContent = replace(strContent,"<","&lt;")
'			strContent = replace(strContent,"'","&apos;")
'			strContent = replace(strContent,"""","&#x22;")
'			strContent = replace(strContent,")","&#x29;")
'			strContent = replace(strContent,"(","&#x28;")
			
			strContent = replace(strContent,"%26%2343%3B1","+")
			strContent = replace(strContent,"&#43;","+")
			strContent = replace(strContent,"--","")
			strContent = replace(strContent,"/*","")
			strContent = replace(strContent,"*/","")
			strContent = replace(strContent,"xp_","")
			strContent = replace(strContent,Chr(39),"''")
		end if
	end if
	ds = strContent
end function
%>
