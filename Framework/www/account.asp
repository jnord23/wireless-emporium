<%
'set oConn = Server.CreateObject("ADODB.Connection")
'oConn.Open Session("ConnectionString")
'''''''''''''''
'''FUNCTIONS'''
'''''''''''''''
function isLoggedIn()
	isLoggedIn = false
	if session("accountEmail") <> "" and not isnull(session("accountEmail")) then
		isLoggedIn = true
	end if
end function
function accountLogOut()
'	if isLoggedIn then
		session("accountEmail") = ""
		response.redirect "/"
'	else
		'response.end 'Causing problems because IE sends request twice due to div with onclick event and nested a link to same request location
'	response.redirect "/"
'	end if
end function
function getAccountEmail()
	if isLoggedIn then
		getAccountEmail = session("accountEmail")
	else
		getAccountEmail = null
	end if
end function
function getAccountId()
	getAccountId = null
	if isLoggedIn then
		sql = "select accountid from " & getAccountsTable & " " &_
		"where email = '" & getAccountEmail & "' " &_
		"and isActive = 1 " &_
		"and isMaster = 1 " &_
		"order by dateEntered " 'dateEntered just in case there are multiple records returned... which should NEVER happen
		'TODO: Check for more than 1 record returned and handle with an email to developers with debug information
		set rs = oConn.execute(sql)
		if not rs.eof then getAccountId = rs("accountid")
	end if
end function
function getAccountIdByEmail(email)
	getAccountId = null
	sql = "select accountid from " & getAccountsTable & " " &_
	"where email = '" & email & "' " &_
	"and isActive = 1 " &_
	"and isMaster = 1 " &_
	"order by dateEntered " 'dateEntered just in case there are multiple records returned... which should NEVER happen
	'TODO: Check for more than 1 record returned and handle with an email to developers with debug information
	set rs = oConn.execute(sql)
	if not rs.eof then getAccountId = rs("accountid")
end function
function accountLoggedInRequired()
	if not isLoggedIn then
		response.redirect urlRelLogin
	end if
end function
function validateUser(email, pass)
	sql = "select top 1 * from " & getAccountsTable & " where isActive = 1 and isMaster = 1 and pword = '" & pass & "' and email = '" & email & "'"
	session("errorSQL") = sql
	'response.write sql
	'response.end
	ValidateUser = false
	set rs = oConn.execute(sql)
	if rs.eof then
		ValidateUser = false
	else
		accountLogIn email
		ValidateUser = true
	end if
	'rs.close
end function
' * private * - The following method should only be called internally - there are no checks made!
' And the proper usage is sub name SPACE argument
sub accountLogIn(e)
	session("accountEmail") = e
end sub
function accountEmailExists(email)
	accountEmailExists = false
	sql = "select COUNT(*) as numOfAccounts from " & getAccountsTable & " " &_
	"where email = '" & email & "' " &_
	"and isMaster = 1"
	set rs = oConn.Execute(sql)
	numOfAccounts = prepInt(rs("numOfAccounts"))
	if numOfAccounts => 1 then
		accountEmailExists = true
	end if
end function
'''''''''''''''
'''VARIABLES'''
'''''''''''''''
function getAccountsTable()
	strHost = lcase(request.ServerVariables("HTTP_HOST"))
	strHost = replace(strHost,"staging.","")
	strHost = replace(strHost,"www.","")
	select case strHost
		case "wirelessemporium.com"
			getAccountsTable = "we_accounts"
		case "cellularoutfitter.com"
			getAccountsTable = "CO_accounts"
	end select
end function
'''''''''''''''
'''''PATHS'''''
'''''''''''''''
function urlRelForgotPassword()
	urlRelForgotPassword = "/account/password_forgot.asp"
end function
function urlRelRegister()
	urlRelRegister = "/account/register.asp"
end function
function urlRelLogin()
	urlRelLogin = "/account/login.asp"
end function
function urlRelFacebook()
	urlRelFacebook = "/Account/FacebookAuthLogin"
end function
function urlRelLoginGoogle()
	urlRelLoginGoogle = "/Account/GoogleAuthLogin"
end function
function urlRelLogout()
	urlRelLogout = "/account/logout.asp"
end function
function urlRelMyAccount()
	'Previously "/index_account.asp", but masterPage.asp and urlRelAccountHomePage() now handle serving up the underlying page
	urlRelMyAccount = "/"
end function
function urlRelAccountHomePage()
	urlRelAccountHomePage = "/index_account.asp"
end function



%>