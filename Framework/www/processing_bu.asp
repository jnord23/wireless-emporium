<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/Framework/account.asp"-->
<%
	on error resume next
		mySession = request.cookies("mySession")
		mySrToken = prepStr(session("sr_token"))
		if mySrToken = "" then mySrToken = prepStr(request.form("mySrToken"))
		if mySrToken <> "" then
			session("sr_token") = mySrToken
			Response.Cookies("srLogin") = mySrToken
			Response.Cookies("srLogin").Path = "/"
			Response.Cookies("srLogin").Expires = Date + 1		
		else
			session("sr_token") = mySrToken
			Response.Cookies("srLogin") = mySrToken
			Response.Cookies("srLogin").Path = "/"
			Response.Cookies("srLogin").Expires = now		
		end if
	on error goto 0
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.Open Session("ConnectionString")

	gblCaTax = cdbl(Application("taxMath"))

	HTTP_REFERER = request.servervariables("HTTP_REFERER")
	zeroOrder = false
	siteid = ""
	siteName = lcase(request.ServerVariables("SERVER_NAME"))
	apiLoginID = ""
	apiTransactionKey = ""
	useCIM = false
	tblAccount = ""
	if instr(siteName, "wirelessemporium.com") > 0 then
		siteid = 0
		siteURL = "wirelessemporium.com"
		strSiteEmail = "sales@wirelessemporium.com"
		imgLogo = "/images/WElogo.gif"
		sitePhone = "(800) 305-1106"		
		strInvoiceDescription = "WirelessEmporium.com Online Order"
		
		apiLoginID = CC_API_LOGIN_ID
		apiTransactionKey = CC_TRANSACTION_KEY
		useCIM = true
		tblAccount = "we_accounts"
	elseif instr(siteName, "cellphoneaccents.com") > 0 then
		siteid = 1
		siteURL = "cellphoneaccents.com"
		strSiteEmail = "sales@mobileGiant.ca"
		imgLogo = "/images/logos/mobileGiant-Logo.gif"
		sitePhone = "(888) 244-7203"
		strInvoiceDescription = "mobileGiant.ca Online Order"		

		apiLoginID = CA_CC_API_LOGIN_ID
		apiTransactionKey = CA_CC_TRANSACTION_KEY
		tblAccount = "ca_accounts"
	elseif instr(siteName, "cellularoutfitter.com") > 0 then
		siteid = 2
		siteURL = "cellularoutfitter.com"
		strSiteEmail = "sales@cellularoutfitter.com"
		imgLogo = "/images/CellularOutfitter.jpg"
		sitePhone = "(888) 871-6926"
		strInvoiceDescription = "CellularOutfitter.com Online Order"		
		
		apiLoginID = CO_CC_API_LOGIN_ID
		apiTransactionKey = CO_CC_TRANSACTION_KEY
		useCIM = true
		tblAccount = "co_accounts"
	elseif instr(siteName, "phonesale.com") > 0 then
		siteid = 3
		siteURL = "phonesale.com"
		strSiteEmail = "sales@phonesale.com"
		imgLogo = "/images/phonesale.jpg"
		sitePhone = "(800) 818-4575"
		strInvoiceDescription = "PhoneSale.com Online Order"
		
		apiLoginID = PS_CC_API_LOGIN_ID
		apiTransactionKey = PS_CC_TRANSACTION_KEY
		tblAccount = "ps_accounts"
	elseif instr(siteName, "tabletmall.com") > 0 then
		siteid = 10
		siteURL = "tabletmall.com"
		strSiteEmail = "sales@tabletmall.com"
		imgLogo = "/images/logo.gif"
		sitePhone = "(888) 551-6570"
		strInvoiceDescription = "TabletMall.com Online Order"
		
		apiLoginID = TM_CC_API_LOGIN_ID
		apiTransactionKey = TM_CC_TRANSACTION_KEY
		tblAccount = "er_accounts"
	end if

	if siteid = "" then
		response.write "<h3>This page will only accept forms submitted from the secure website.</h3>"
		response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>"
		response.end
	end if	
	
	
	select case siteid
		case 0
			if request.form("trackingSessionID") <> "" then
				tsql = "update Tracking set visitedProcessing = 1 where sessionID = " & request.form("trackingSessionID")
				session("errorSQL") = tsql
				set	RS = oConn.execute(tsql)
			end if
			
			mobileOrder = request.Form("mobile")
			if isnull(mobileOrder) or len(mobileOrder) < 1 then
				mobileOrder = 0
			else
				mobileOrder = 1
				sql = "update shoppingCart set processing = 1 where sessionID = " & mySession
				session("errorSQL") = sql
				oConn.execute(sql)
				
				if request.querystring("utm_source") = "AdMob" then
					session("adTracking") = "AdMob"
					session("additionalQS") = "&utm_source=AdMob&utm_medium=" & request.querystring("utm_medium") & "&utm_campaign=" & request.querystring("utm_campaign")
				end if
			end if
			
'			if instr(HTTP_REFERER,"wirelessemporium.com") < 1 then
'				response.write "<h3>This page will only accept forms submitted from the wirelessemporium.com secure website.</h3>" & vbcrlf
'				response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>"
'				response.end
'			end if
			
			ssl_ProdIdCount = request.form("ssl_ProdIdCount")
			
			sSAddy1 = request.form("sAddress1")
			sSAddy2 = request.form("sAddress2")
			sSCity = request.form("sCity")
			sSState = request.form("sState")
			sSZip = request.form("sZip")
			
			if mobileOrder = 1 then
				sBAddy1 = request.form("bAddress1")
				sBAddy2 = request.form("bAddress2")
				sBCity = request.form("bCity")
				sBState = request.form("bState")
				sBZip = request.form("bZip")
			else
				sameaddress = prepStr(request.Form("sameaddress"))
				
				if sameaddress = "yes" or mobileOrder = 1 then
					sBAddy1 = request.form("bAddress1")
					sBAddy2 = request.form("bAddress2")
					sBCity = request.form("bCity")
					sBState = request.form("bState")
					sBZip = request.form("bZip")
				else
					sBAddy1 = request.form("sAddress1")
					sBAddy2 = request.form("sAddress2")
					sBCity = request.form("sCity")
					sBState = request.form("sState")
					sBZip = request.form("sZip")
				end if
			end if
			
			if instr(sSZip,"-") > 0 then
				zipArray = split(sSZip,"-")
				sSZip = zipArray(0)
			end if
			if instr(sBZip,"-") > 0 then
				zipArray = split(sBZip,"-")
				sBZip = zipArray(0)
			end if
			
			sPromoCode = request.form("sPromoCode")
			nPromo = request.form("nPromo") ' this is couponid
			nSubTotal = request.form("subTotal")
			discountTotal = prepInt(request.Form("discountTotal"))
			useQty = request.form("nTotalQuantity")
			nShipRate = prepInt(request.form("shippingTotal"))
			nShippingID = request.form("sID" & request.form("radioAddress"))
			sCardNum = request.form("cardNum")
			nCATax = prepInt(request.form("nCATax"))

			sEmailOpt = request.form("chkOptMail")
			nShipType = prepInt(request.form("shiptypeID"))
			sCardType = request.form("cc_cardType")
			sCVV2 = request.form("secCode")
			sCC_Month = request.form("cc_month")
			sCC_Year = request.form("cc_year")
			
			sEmail = request.form("email")
			sFname = request.form("fname")
			sLname = request.form("lname")
			sPhone = request.form("phoneNumber")
			
			nAccountId = prepInt(request.Form("accountid"))
			parentAcctID = prepInt(request.Form("parentAcctID"))

			if len(sBZip) = 5 and isNumeric(sBZip) then
				sBCountry = "US"
			else
				if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
					sBCountry = "CANADA"
				else
					sBCountry = ""
				end if
			end if


			session("errorSQL") = "nSubTotal: " & nSubTotal & "<br>Qty:" & useQty
			if isnull(useQty) or len(useQty) < 1 or not isnumeric(useQty) then useQty = 0
			nTotalQuantity = cDbl(useQty)

			session("errorSQL") = nSubTotal

			if (len(sSZip) = 5 and isNumeric(sSZip) ) or instr(US50STATES,sSState) > 0 then
				sSCountry = "US"
			else
				if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
					sSCountry = "CANADA"
				else
					sSCountry = ""
				end if
			end if
			
			if prepStr(session("sr_token")) <> "" then
				if instr(lcase(replace(sSAddy1,".","")),"po box") > 0 then
					response.Redirect("/cart/shopRunnerError")
				end if
				if sSCountry <> "US" or sSState = "AE" or sSState = "AA" or sSState = "AP" or sSState = "AS" or sSState = "FM" or sSState = "GU" or sSState = "MH" or sSState = "MP" or sSState = "PW" or sSState = "VI" then
					response.Redirect("/cart/shopRunnerError")
				end if
			end if
			
			select case nShipType
				case "0"
					if sSCountry <> "US" then
						strShiptype = "First Class Int'l"
						nShipRate = 0.00 + (0 * nTotalQuantity)
					else
						strShiptype = "First Class"
						nShipRate = 0.00 + (0 * nTotalQuantity)
					end if
				case "2"
					if now >= cdate("12/17/2012") and now <= cdate("12/19/2012 11:59:59 AM") and nSubTotal >= 30 then 
						if sSCountry <> "US" then
							strShiptype = "USPS Priority Int'l"
							nShipRate = 0 + (0 * nTotalQuantity)
						else
							strShiptype = "USPS Priority"
							nShipRate = 0 + (0 * nTotalQuantity)
						end if
					else
						if sSCountry <> "US" then
							strShiptype = "USPS Priority Int'l"
							nShipRate = 10.99 + (0 * nTotalQuantity)
						else
							strShiptype = "USPS Priority"
							nShipRate = 6.99 + (0 * nTotalQuantity)
						end if
					end if
				case "3"
					if freeship = 1 then
						strShiptype = "USPS Express"
						nShipRate = 0 + (0 * nTotalQuantity)
					else
						strShiptype = "USPS Express"
						nShipRate = 24.99 + (0 * nTotalQuantity)
					end if
				case "4" : strShiptype = "UPS Ground"
				case "5" : strShiptype = "UPS 3 Day Select"
				case "6" : strShiptype = "UPS 2nd Day Air"
			end select
			
			if prepStr(session("sr_token")) <> "" then
				if sSZip > 80000 and sSZip < 99355 then
					nShipType = 98
					strShiptype = "ShopRunner, 2-Day Shipping - FREE"
				else
					nShipType = 99
					strShiptype = "ShopRunner, 2-Day Shipping - FREE"
				end if
			end if
			
			if ssl_ProdIdCount = "0" or ssl_ProdIdCount = "" or not isNumeric(ssl_ProdIdCount) then
				'basket currently empty
				response.redirect("/sessionexpired?ec=101&pic=" & ssl_ProdIdCount)
				response.end
			else
				ssl_ProdIdCount = cDbl(ssl_ProdIdCount)
			end if			

			if nAccountId = 0 then
				'this is an insert new account (we_Accounts)
				SQL = "SET NOCOUNT ON; "
				if parentAcctID > 0 then
					SQL = SQL & "INSERT INTO we_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered,CustomerIP,parentID) VALUES ("
				else
					SQL = SQL & "INSERT INTO we_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered,CustomerIP) VALUES ("
				end if
				SQL = SQL & "'" & ds(sFname) & "', "
				SQL = SQL & "'" & ds(sLname) & "', "
				SQL = SQL & "'" & ds(sBAddy1) & "', "
				SQL = SQL & "'" & ds(sBAddy2) & "', "
				SQL = SQL & "'" & ds(sBCity) & "', "
				SQL = SQL & "'" & ds(sBState) & "', "
				SQL = SQL & "'" & ds(sBZip) & "', "
				SQL = SQL & "'" & ds(sBCountry) & "', "
				SQL = SQL & "'" & ds(sEmail) & "', "
				SQL = SQL & "'" & ds(sPhone) & "', "
				SQL = SQL & "'" & ds(sSAddy1) & "', "
				SQL = SQL & "'" & ds(sSAddy2) & "', "
				SQL = SQL & "'" & ds(sSCity) & "', "
				SQL = SQL & "'" & ds(sSState) & "', "
				SQL = SQL & "'" & ds(sSZip) & "', "
				SQL = SQL & "'" & ds(sSCountry) & "', "
				SQL = SQL & "'" & now & "', "
				if parentAcctID > 0 then
				SQL = SQL & "'" & Request.ServerVariables("REMOTE_ADDR") & "', "
				SQL = SQL & "'" & ds(parentAcctID) & "')"
				else
				SQL = SQL & "'" & Request.ServerVariables("REMOTE_ADDR") & "')"
				end if
				SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
				SQL = SQL & "SET NOCOUNT OFF;"
				session("errorSQL") = SQL
				dim nAccountId
				set RS = oConn.execute(SQL)
				if not RS.eof then nAccountId = RS("newAccountID")
			else
				sql =   "update we_accounts set fname = '" & ds(sFname) & "',lname = '" & ds(sLname) & "',bAddress1 = '" & ds(sBAddy1) & "',bAddress2 = '" & ds(sBAddy2) & "'," &_
						"bCity = '" & ds(sBCity) & "',bState = '" & ds(sBState) & "',bZip = '" & ds(sBZip) & "',bCountry = '" & ds(sBCountry) & "',email = '" & ds(sEmail) & "'," &_
						"phone = '" & ds(sPhone) & "',sAddress1 = '" & ds(sSAddy1) & "',sAddress2 = '" & ds(sSAddy2) & "',sCity = '" & ds(sSCity) & "'," &_
						"sState = '" & ds(sSState) & "',sZip = '" & ds(sSZip) & "',sCountry = '" & ds(sSCountry) & "',CustomerIP = '" & Request.ServerVariables("REMOTE_ADDR") &"' where accountID = " & nAccountId
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
			
			oConn.execute ("sp_ModifyEmailOpt '" & ds(sFname) & "','" & sEmail & "','" & sEmailOpt & "'")
			
			' Add new shoppingcart
			strItems = ""
			CellPhoneInOrder = 0
			for a = 1 to ssl_ProdIdCount
				session("errorSQL") = "SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "' (" & a & ")"
				SQL = 	"SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "'" &_
						" union " &_
						"SELECT '3' as typeID FROM we_items_musicSkins WHERE id = '" & request.form("ssl_item_number_" & a) & "'"
				set oRsCheckType = oConn.execute(SQL)
				if not oRsCheckType.eof then
					if oRsCheckType("typeID") = 16 then 
						CellPhoneInOrder = 1
						Freeship = 0 'check Free overnight shipping
					end if
					strItems = strItems & request.form("ssl_item_number_" & a) & ","
				end if
				oRsCheckType.close
				set oRsCheckType = nothing
			next
			
			strItems = left(strItems,len(strItems)-1)
			' end add new shoppingcart
			
			nSubTotal = formatNumber(nSubTotal,2)
			session("errorSQL") = nSubTotal & " + " & nCATax & " + " & nShipRate & " + " & discountTotal
			nOrderTotal = nSubTotal + nCATax + nShipRate - discountTotal
			nOrderTotal = formatNumber(nOrderTotal,2)
				
			'WRITE THE ORDER IN THE DATABASE
			nOrderID = fWriteBasketItemIntoDB(siteid,nAccountId,nSubTotal,nShipRate,nOrderTotal,strShiptype,nCATax,nShippingID,nPromo)

			if request.form("trackingSessionID") <> "" and cStr(nOrderID) <> "" then
				tsql = "update Tracking set assignedOrderID = " & cStr(nOrderID) & " where sessionID = " & request.form("trackingSessionID")
				session("errorSQL") = tsql
				set	RS = oConn.execute(tsql)
			end if

			sCardNum = replace(sCardNum, Chr(34), "")
			sCardNum = replace(sCardNum, Chr(39), "")
			
			sExp = right("00" & sCC_Month,2) & right(sCC_Year, 2)
			
			if nOrderTotal = 0 then
				nResponseStatus = 1
				zeroOrder = true
				session("zeroOrder") = 1
			end if
		case 1
			if instr(HTTP_REFERER,"mobilegiant.ca") < 1 then
				response.write "<h2>This page will only accept forms submitted from the MobileGiant secure website.</h2>" & vbcrlf
				response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>"
				response.end
			end if
			
			PaymentType = request.form("PaymentType")
			
			nSubTotal = request.form("subTotal")
			sPromoCode = request.form("sPromoCode")
			nPromo = request.form("nPromo")
			nSubTotal = request.form("subTotal")
			nShipRate = request.form("shippingTotal")
			nShippingID = request.form("sID" & request.form("radioAddress"))
			
			if request.form("sstate") = "CA" then
				nCATax = round(cDbl(nSubTotal) * gblCaTax, 2)
			else
				nCATax = 0
			end if
			nTotalQuantity = cDbl(request.form("nTotalQuantity"))
			
			sEmail = request.form("email")
			sPwd = ds(request.form("pword2"))
			sFname = request.form("fname")
			sLname = request.form("lname")
			sPhone = request.form("phone")
			
			sSAddy1 = request.form("saddress1")
			sSAddy2 = request.form("saddress2")
			sSCity = request.form("scity")
			sSState = request.form("sstate")
			sSZip = request.form("szip")
			
			if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
				sSCountry = "CANADA"
			else
				sSCountry = ""
			end if
			
			'nShipType = request.form("shiptype")
			nShipType = request.form("cbShip")
			select case nShipType
				case "2"
					if sSCountry = "CANADA" then
						strShiptype = "USPS Priority Int'l"
					else
						strShiptype = "USPS Priority"
					end if
				case "3"
					if sSCountry = "CANADA" then
						strShiptype = "USPS Priority Int'l"
					else
						strShiptype = "USPS Express"
					end if
				case "0"
					if sSCountry = "CANADA" then
						strShiptype = "First Class Int'l"
					else
						strShiptype = "First Class"
					end if
				case "4" : strShiptype = "UPS Ground"
				case "5" : strShiptype = "UPS 3 Day Select"
				case "6" : strShiptype = "UPS 2nd Day Air"
			end select
			
			nOrderTotal = nSubTotal + nCATax + nShipRate
			nOrderTotal = nOrderTotal
			sBAddy1 = request.form("baddress1")
			sBAddy2 = request.form("baddress2")
			sBCity = request.form("bcity")
			sBState = request.form("bstate")
			sBZip = request.form("bzip")
			sBCountry = request.form("bcountry")
			
			sEmailOpt = request.form("chkOptMail")
			
			'this is an insert new account
			SQL = "SET NOCOUNT ON; "
			SQL = SQL & "INSERT INTO CA_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered) VALUES ("
			SQL = SQL & "'" & ds(sFname) & "', "
			SQL = SQL & "'" & ds(sLname) & "', "
			SQL = SQL & "'" & ds(sBAddy1) & "', "
			SQL = SQL & "'" & ds(sBAddy2) & "', "
			SQL = SQL & "'" & ds(sBCity) & "', "
			SQL = SQL & "'" & ds(sBState) & "', "
			SQL = SQL & "'" & ds(sBZip) & "', "
			SQL = SQL & "'" & ds(sBCountry) & "', "
			SQL = SQL & "'" & ds(sEmail) & "', "
			SQL = SQL & "'" & ds(sPhone) & "', "
			SQL = SQL & "'" & ds(sSAddy1) & "', "
			SQL = SQL & "'" & ds(sSAddy2) & "', "
			SQL = SQL & "'" & ds(sSCity) & "', "
			SQL = SQL & "'" & ds(sSState) & "', "
			SQL = SQL & "'" & ds(sSZip) & "', "
			SQL = SQL & "'" & ds(sSCountry) & "', "
			SQL = SQL & "'" & now & "'); "
			SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
			SQL = SQL & "SET NOCOUNT OFF;"
			session("errorSQL") = SQL

			set RS = oConn.execute(SQL)
			if not RS.eof then nAccountId = RS("newAccountID")
			
			oConn.execute ("sp_ModifyEmailOpt_CA '" & ds(sFname) & "','" & ds(sEmail) & "','" & sEmailOpt & "'")

			ssl_ProdIdCount = request.form("ssl_ProdIdCount")
			if ssl_ProdIdCount = "0" or ssl_ProdIdCount = "" or not isNumeric(ssl_ProdIdCount) then
				response.redirect("/sessionexpired")
				response.end
			else
				ssl_ProdIdCount = cDbl(ssl_ProdIdCount)
			end if
			
			if PaymentType = "eBillMe" then
				%>
				<html><body>
				<form name="eBillmeForm" action="/cart/process/eBillMe/eBillMe.asp" method="post">
					<input type="hidden" name="amount" value="<%=nsubTotal%>">
					<input type="hidden" name="nAmount" value="<%=nOrderTotal%>">
					<input type="hidden" name="nAccountId" value="<%=nAccountId%>">
					<input type="hidden" name="shipping" value="<%=nShipRate%>">
					<input type="hidden" name="shiptype" value="<%=strShiptype%>">
					<input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
					<input type="hidden" name="nCouponid" value="<%=nPromo%>">
					<input type="hidden" name="tax" value="<%=nCATax%>">
					<input type="hidden" name="ShippingAddressID" value="<%=nShippingID%>">
					<%
					for a = 1 to ssl_ProdIdCount
						%>
						<input type="hidden" name="ssl_item_number_<%=a%>" value="<%=request.form("ssl_item_number_" & a)%>">
						<input type="hidden" name="ssl_item_qty_<%=a%>" value="<%=request.form("ssl_item_qty_" & a)%>">
						<%
					next
					%>
					<input type="hidden" name="myBasketXML" value="<%=request.form("myBasketXML")%>">
					<input type="hidden" name="ssl_ProdIdCount" value="<%=ssl_ProdIdCount%>">
				</form>
				<script language="javascript">
					document.eBillmeForm.submit();
				</script>
				</body></html>
				<%
				response.end
			end if
			
			'WRITE THE ORDER IN THE DATABASE

			nOrderID = fWriteBasketItemIntoDB(siteid,nAccountId,nSubTotal,nShipRate,nOrderTotal,strShiptype,nCATax,nShippingID,nPromo)
			
			' Add new shoppingcart
			strItems = ""
			CellPhoneInOrder = 0
			for a = 1 to ssl_ProdIdCount
				set oRsCheckType = oConn.execute("SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "'")
				if not oRsCheckType.eof then
					if oRsCheckType("typeID") = 16 then CellPhoneInOrder = 1
					strItems = strItems & request.form("ssl_item_number_" & a) & ","
				end if
				oRsCheckType.close
				set oRsCheckType = nothing
			next
			if strItems <> "" then strItems = left(strItems,len(strItems)-1)
			' end add new shoppingcart
			
			sCardType = request.form("cc_cardType")
			sCardNum = request.form("cardNum")
			sCardNum = replace(sCardNum, Chr(34), "")
			sCardNum = replace(sCardNum, Chr(39), "")
			sCVV2 = request.form("secCode")
			
			sCC_Month = request.form("cc_month")
			sCC_Year = request.form("cc_year")
			sExp = right("00" & sCC_Month,2) & right(sCC_Year, 2)

		case 2 ' CO
			'if instr(HTTP_REFERER,"https://www.cellularoutfitter.com") < 1 and instr(HTTP_REFERER,"http://staging.cellularoutfitter.com") < 1 and instr(HTTP_REFERER,"https://m.cellularoutfitter.com") < 1 and instr(HTTP_REFERER,"http://mdev.cellularoutfitter.com") < 1 then
				'response.write "<h3>This page will only accept forms submitted from the Cellular Outfitter secure website.</h3>" & vbcrlf
				'response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>"
				'response.Write("<!-- HTTP_REFERER: " & HTTP_REFERER & " -->")
				'response.end
			'end if
			
			mobileOrder = prepInt(request.Form("mobile"))
			PaymentType = request.form("PaymentType")
			sPromoCode = request.form("sPromoCode")
			nPromo = request.form("nPromo")
			nShippingID = request.form("sID" & request.form("radioAddress"))
			nSubTotal = request.form("subTotal")
			nShipRate = request.form("shippingTotal")
			nShipRate = cDbl(nShipRate)
			if request.form("sstate") = "CA" then
				nCATax = round(cDbl(nSubTotal) * gblCaTax, 2)
			else
				nCATax = 0
			end if
			nTotalQuantity = cDbl(request.form("nTotalQuantity"))
			
			sEmail = request.form("email")
			sPwd = ds(request.form("pword2"))
			sFname = request.form("fname")
			sLname = request.form("lname")
			sPhone = request.form("phone")
			
			sSAddy1 = request.form("saddress1")
			sSAddy2 = request.form("saddress2")
			sSCity = request.form("scity")
			sSState = request.form("sstate")
			sSZip = request.form("szip")
			
			accountAlreadyExists = false
			if trim(prepStr(request.form("pword"))) <> "" and trim(prepStr(request.form("chkActivateAccount"))) = "Y" then 'Determine values for account creation
				if accountEmailExists(ds(sEmail)) then
					accountAlreadyExists = true
					pword = prepStr(request.form("pword"))
				else
					pword = prepStr(request.form("pword"))
					isActive = 1
					isMaster = 1
				end if
			else
				pword = "NULL"
				isActive = 0
				isMaster = 0
			end if
			
			if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
				sSCountry = "CANADA"
			else
				sSCountry = ""
			end if
			nShipType = request.form("shiptype")
			
			select case nShipType
				case "2"
					if sSCountry = "CANADA" then
						strShiptype = "USPS Priority Int'l"
					else
						strShiptype = "USPS Priority"
					end if
				case "3"
					if sSCountry = "CANADA" then
						strShiptype = "USPS Priority Int'l"
					else
						strShiptype = "USPS Express"
					end if
				case "4"
					if sSCountry = "CANADA" then
						strShiptype = "First Class Int'l"
					else
						strShiptype = "First Class"
					end if
				case "5"
					if sSCountry = "CANADA" then
						strShiptype = "USPS Priority Int'l"
					else
						strShiptype = "USPS Priority"
					end if
				case "0"
					if sSCountry = "CANADA" then
						strShiptype = "First Class Int'l"
					else
						strShiptype = "First Class"
					end if
				case "7" : strShiptype = "UPS Ground"
				case "8" : strShiptype = "UPS 3 Day Select"
				case "9" : strShiptype = "UPS 2nd Day Air"
			end select
			
			'BuySafe values
			dim ShoppingCartId, WantsBondValue, buysafeamount
			ShoppingCartId = request.form("buysafecartID")
			WantsBondValue = request.form("WantsBondField")
			if request.form("buysafeamount") = "" then
				buysafeamount = 0
			else
				buysafeamount = cDbl(request.form("buysafeamount"))
			end if
			
			nSubTotal = formatNumber(nSubTotal,2)
			nOrderTotal = nSubTotal + nCATax + nShipRate + buysafeamount
			nOrderTotal = formatNumber(nOrderTotal,2)
			
			sBAddy1 = request.form("baddress1")
			sBAddy2 = request.form("baddress2")
			sBCity = request.form("bcity")
			sBState = request.form("bstate")
			sBZip = request.form("bzip")
			sBCountry = request.form("bcountry")
			
			sEmailOpt = request.form("chkOptMail")
			
			'The customer's order information will either be: 1) Recorded to an existing account or 2) Recorded in a newly created account
			if isLoggedIn then
			
				sql = 	"update CO_accounts set " &_
						"fname = '" & 		ds(sFname) & "', " &_
						"lname = '" & 		ds(sLname) & "', " &_
						"bAddress1 = '" & 	ds(sBAddy1) & "', " &_
						"bAddress2 = '" & 	ds(sBAddy2) & "', " &_
						"bCity = '" & 		ds(sBCity) & "', " &_
						"bState = '" & 		ds(sBState) & " ', " &_
						"bZip = '" & 		ds(sBZip) & "', " &_
						"bCountry = '" & 	ds(sBCountry) & "', " &_
						"phone = '" & 		ds(sPhone) & "', " &_
						"sAddress1 = '" & 	ds(sSAddy1) & "', " &_
						"sAddress2 = '" & 	ds(sSAddy2) & "', " &_
						"sCity = '" & 		ds(sSCity) & "', " &_
						"sState = '" & 		ds(sSState) & "', " &_
						"sZip = '" & 		ds(sSZip) & "', " &_
						"sCountry = '" & 	ds(sSCountry) & "' " &_
						"where email = '" & getAccountEmail & "' " &_
						"and isActive = 1 " &_
						"and isMaster = 1"
				session("errorSQL") = sql
				oConn.Execute(sql)
				'updateAccountResult = 
				'TODO : Return number of rows affected and email if it's not 1
				'if not updateAccountResult = 1 then
				'	'Send error email
				'	strFrom = "shaun@wirelessemporium.com"
				'	strSubject = "Error: Account Creation / Update"
				'	strBody = "/Framework/processing.asp" & vbcrlf & vbcrlf & "Number of rows was updated was different that expected!" & vbcrlf & vbcrlf & "Number of rows updated: " & updateAccountResult & vbcrlf & vbcrlf & sql & vbcrlf & vbcrlf
				'	strTo = "shaun@wirelessemporium.com"
				'	weEmail strTo,strFrom,strSubject,strBody
				'end if
				nAccountId = getAccountId
					
			elseif accountAlreadyExists then
				sql = 	"update CO_accounts set " &_
						"fname = '" & 		ds(sFname) & "', " &_
						"lname = '" & 		ds(sLname) & "', " &_
						"bAddress1 = '" & 	ds(sBAddy1) & "', " &_
						"bAddress2 = '" & 	ds(sBAddy2) & "', " &_
						"bCity = '" & 		ds(sBCity) & "', " &_
						"bState = '" & 		ds(sBState) & " ', " &_
						"bZip = '" & 		ds(sBZip) & "', " &_
						"bCountry = '" & 	ds(sBCountry) & "', " &_
						"phone = '" & 		ds(sPhone) & "', " &_
						"sAddress1 = '" & 	ds(sSAddy1) & "', " &_
						"sAddress2 = '" & 	ds(sSAddy2) & "', " &_
						"sCity = '" & 		ds(sSCity) & "', " &_
						"sState = '" & 		ds(sSState) & "', " &_
						"sZip = '" & 		ds(sSZip) & "', " &_
						"sCountry = '" & 	ds(sSCountry) & "', " &_
						"pword = '" & 		ds(pword) & "' " &_
						"where email = '" & ds(sEmail) & "' " &_
						"and isActive = 1 " &_
						"and isMaster = 1"
				session("errorSQL") = sql
				oConn.Execute(sql)
				nAccountId = getAccountIdByEmail(ds(sEmail))
			else
			
				'this is an insert new account
				SQL = "SET NOCOUNT ON; "
				SQL = SQL & "INSERT INTO CO_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,pword,isActive,isMaster,dateEntered) VALUES ("
				SQL = SQL & "'" & ds(sFname) & "', "
				SQL = SQL & "'" & ds(sLname) & "', "
				SQL = SQL & "'" & ds(sBAddy1) & "', "
				SQL = SQL & "'" & ds(sBAddy2) & "', "
				SQL = SQL & "'" & ds(sBCity) & "', "
				SQL = SQL & "'" & ds(sBState) & "', "
				SQL = SQL & "'" & ds(sBZip) & "', "
				SQL = SQL & "'" & ds(sBCountry) & "', "
				SQL = SQL & "'" & ds(sEmail) & "', "
				SQL = SQL & "'" & ds(sPhone) & "', "
				SQL = SQL & "'" & ds(sSAddy1) & "', "
				SQL = SQL & "'" & ds(sSAddy2) & "', "
				SQL = SQL & "'" & ds(sSCity) & "', "
				SQL = SQL & "'" & ds(sSState) & "', "
				SQL = SQL & "'" & ds(sSZip) & "', "
				SQL = SQL & "'" & ds(sSCountry) & "', "
				SQL = SQL & "'" & ds(pword) & "', "
				SQL = SQL & "'" & ds(isActive) & "', "
				SQL = SQL & "'" & ds(isMaster) & "', "
				SQL = SQL & "'" & now & "'); "
				SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
				SQL = SQL & "SET NOCOUNT OFF;"
				session("errorSQL") = SQL
				'response.write "<p>" & SQL & "</p>" & vbcrlf
	
				set RS = oConn.execute(SQL)
				if not RS.eof then nAccountId = RS("newAccountID")
			
			end if
			
			oConn.execute ("sp_ModifyEmailOpt_CO '" & ds(sFname) & "','" & ds(sEmail) & "','" & sEmailOpt & "'")
			

			ssl_ProdIdCount = request.form("ssl_ProdIdCount")
			if ssl_ProdIdCount = "0" or ssl_ProdIdCount = "" or not isNumeric(ssl_ProdIdCount) then
				'basket currently empty
				response.redirect("/sessionexpired.asp")
				response.end
			else
				ssl_ProdIdCount = cDbl(ssl_ProdIdCount)
			end if
			
			if PaymentType = "eBillMe" then
				%>
				<html><body>
				<form name="eBillmeForm" action="/cart/process/eBillMe/eBillMe.asp" method="post">
					<input type="hidden" name="amount" value="<%=nsubTotal%>">
					<input type="hidden" name="nAmount" value="<%=nOrderTotal%>">
					<input type="hidden" name="nAccountId" value="<%=nAccountId%>">
					<input type="hidden" name="shipping" value="<%=nShipRate%>">
					<input type="hidden" name="shiptype" value="<%=strShiptype%>">
					<input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
					<input type="hidden" name="nCouponid" value="<%=nPromo%>">
					<input type="hidden" name="tax" value="<%=nCATax%>">
					<input type="hidden" name="buysafeamount" value="<%=buysafeamount%>">
					<input type="hidden" name="ShippingAddressID" value="<%=nShippingID%>">
					<%
					for a = 1 to ssl_ProdIdCount
						%>
						<input type="hidden" name="ssl_item_number_<%=a%>" value="<%=request.form("ssl_item_number_" & a)%>">
						<input type="hidden" name="ssl_item_qty_<%=a%>" value="<%=request.form("ssl_item_qty_" & a)%>">
						<%
					next
					%>
					<input type="hidden" name="myBasketXML" value="<%=request.form("myBasketXML")%>">
					<input type="hidden" name="ssl_ProdIdCount" value="<%=ssl_ProdIdCount%>">
				</form>
				<script language="javascript">
					document.eBillmeForm.submit();
				</script>
				</body></html>
				<%
				response.end
			end if
			
			'WRITE THE ORDER IN THE DATABASE
			nOrderID = fWriteBasketItemIntoDB(siteid,nAccountId,nSubTotal,nShipRate,nOrderTotal,strShiptype,nCATax,nShippingID,nPromo)
			
			' Add new shoppingcart
			strItems = ""
			CellPhoneInOrder = 0
			for a = 1 to ssl_ProdIdCount
				set oRsCheckType = oConn.execute("SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "'")
				if not oRsCheckType.eof then
					if oRsCheckType("typeID") = 16 then CellPhoneInOrder = 1
					strItems = strItems & request.form("ssl_item_number_" & a) & ","
				else
					set oRsCheckType = oConn.execute("SELECT 3 as typeID FROM we_items_musicSkins WHERE id = " & request.form("ssl_item_number_" & a))
					if not oRsCheckType.eof then
						if oRsCheckType("typeID") = 16 then CellPhoneInOrder = 1
						strItems = strItems & request.form("ssl_item_number_" & a) & ","
					end if
				end if
				oRsCheckType.close
				set oRsCheckType = nothing
			next
			strItems = left(strItems,len(strItems)-1)
			' end add new shoppingcart

			sCardType = request.form("cc_cardType")
			sCardNum = request.form("cardNum")
			sCardNum = replace(sCardNum, Chr(34), "")
			sCardNum = replace(sCardNum, Chr(39), "")
			sCVV2 = request.form("secCode")

			sCC_Month = request.form("cc_month")
			sCC_Year = request.form("cc_year")
			sExp = right("00" & sCC_Month,2) & right(sCC_Year, 2)
		case 3
			if instr(HTTP_REFERER, ".phonesale.com/cart") <= 0 then
				if instr(HTTP_REFERER, "https://optimizely.appspot.com") <= 0 then
					response.write "<h3>This page will only accept forms submitted from the Phonesale.com secure website.</h3>"
					response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>"
					response.end
				end if			
			end if
			
			sSAddy1 = request.form("saddress1")
			sSAddy2 = request.form("saddress2")
			sSCity = request.form("scity")
			sSState = request.form("sstate")
			sSZip = request.form("szip")
			
			sBAddy1 = request.form("baddress1")
			sBAddy2 = request.form("baddress2")
			sBCity = request.form("bcity")
			sBState = request.form("bstate")
			sBZip = request.form("bzip")
				
			if len(sBZip) = 5 and isNumeric(sBZip) then
				sBCountry = "US"
			else
				session("countryerror") = "Sorry, we only accept credit cards issued from the US."
				response.redirect("/cart/basket.asp?ec=103")
				response.end
			end if
			
			sPromoCode = request.form("sPromoCode")
			nPromo = request.form("nPromo") ' this is couponid
			
			nSubTotal = request.form("subTotal")
			discountTotal = prepInt(request.Form("discountTotal"))
			session("errorSQL") = "nSubTotal: " & nSubTotal & "<br>Qty:" & request.form("nTotalQuantity")
			useQty = request.form("nTotalQuantity")
			if isnull(useQty) or len(useQty) < 1 or not isnumeric(useQty) then useQty = 0
			nTotalQuantity = cDbl(useQty)
			
			nShipRate = request.form("shippingTotal")
			nShippingID = request.form("sID" & request.form("radioAddress"))
			sCardNum = request.form("cardNum")
			
			session("errorSQL") = nSubTotal
			if request.form("sstate") = "CA" then
				nCATax = round(cDbl(nSubTotal) * gblCaTax, 2)
			else
				nCATax = 0
			end if
			
			sEmail = request.form("email")
			sFname = request.form("fname")
			sLname = request.form("lname")
			sPhone = request.form("phone")
			
			sEmailOpt = request.form("chkOptMail")
			
			if len(sSZip) = 5 and isNumeric(sSZip) then
				sSCountry = "US"
			else
				session("countryerror") = "Sorry, your zip code must be in standard 5-digit US format."
				response.redirect("http://www.phonesale.com/cart/basket.asp?ec=102")
				response.end
			end if
			
			nShipType = request.form("shiptypeID")
			
			select case nShipType
				case "0"
					strShiptype = "First Class"
					nShipRate = 0.00 + (0 * nTotalQuantity)
				case "2"
					strShiptype = "USPS Priority"
					nShipRate = 7.99 + (0 * nTotalQuantity)
				case "3"
					if freeship = 1 then
						strShiptype = "USPS Express"
						nShipRate = 0 + (0 * nTotalQuantity)
					else
						strShiptype = "USPS Express"
						nShipRate = 19.99 + (0 * nTotalQuantity)
					end if
				case "7" : strShiptype = "UPS Ground"
				case "8" : strShiptype = "UPS 3 Day Select"
				case "9" : strShiptype = "UPS 2nd Day Air"
			end select
			
			ssl_ProdIdCount = request.form("ssl_ProdIdCount")
			if ssl_ProdIdCount = "0" or ssl_ProdIdCount = "" or not isNumeric(ssl_ProdIdCount) then
				'basket currently empty
				response.redirect("http://www.phonesale.com/sessionexpired.asp?ec=101")
				response.end
			else
				ssl_ProdIdCount = cDbl(ssl_ProdIdCount)
			end if
			
			'this is an insert new account (PS_Accounts)
			SQL = "SET NOCOUNT ON; "
			SQL = SQL & "INSERT INTO PS_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered,CustomerIP) VALUES ("
			SQL = SQL & "'" & ds(sFname) & "', "
			SQL = SQL & "'" & ds(sLname) & "', "
			SQL = SQL & "'" & ds(sBAddy1) & "', "
			SQL = SQL & "'" & ds(sBAddy2) & "', "
			SQL = SQL & "'" & ds(sBCity) & "', "
			SQL = SQL & "'" & ds(sBState) & "', "
			SQL = SQL & "'" & ds(sBZip) & "', "
			SQL = SQL & "'" & ds(sBCountry) & "', "
			SQL = SQL & "'" & ds(sEmail) & "', "
			SQL = SQL & "'" & ds(sPhone) & "', "
			SQL = SQL & "'" & ds(sSAddy1) & "', "
			SQL = SQL & "'" & ds(sSAddy2) & "', "
			SQL = SQL & "'" & ds(sSCity) & "', "
			SQL = SQL & "'" & ds(sSState) & "', "
			SQL = SQL & "'" & ds(sSZip) & "', "
			SQL = SQL & "'" & ds(sSCountry) & "', "
			SQL = SQL & "'" & now & "', "
			SQL = SQL & "'" & Request.ServerVariables("REMOTE_ADDR") &"')"
			SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
			SQL = SQL & "SET NOCOUNT OFF;"
			session("errorSQL") = SQL

			set RS = oConn.execute(SQL)
			if not RS.eof then nAccountId = RS("newAccountID")
			
			oConn.execute ("sp_ModifyEmailOpt_PS '" & ds(sFname) & "','" & sEmail & "','" & sEmailOpt & "'")
			
			' Add new shoppingcart
			strItems = ""
			CellPhoneInOrder = 0
			for a = 1 to ssl_ProdIdCount
				session("errorSQL") = "SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "' (" & a & ")"
				set oRsCheckType = oConn.execute("SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "'")
				if not oRsCheckType.eof then
					if oRsCheckType("typeID") = 16 then 
						CellPhoneInOrder = 1
						Freeship = 0 'check Free overnight shipping
					end if
					strItems = strItems & request.form("ssl_item_number_" & a) & ","
				end if
				oRsCheckType.close
				set oRsCheckType = nothing
			next
			
			strItems = left(strItems,len(strItems)-1)
			' end add new shoppingcart
			
			nSubTotal = formatNumber(nSubTotal,2)
			nOrderTotal = nSubTotal + nCATax + nShipRate - discountTotal
			'nOrderTotal = request.Form("grandTotal")
			nOrderTotal = formatNumber(nOrderTotal,2)
				
			'WRITE THE ORDER IN THE DATABASE
			nOrderID = fWriteBasketItemIntoDB(siteid,nAccountId,nSubTotal,nShipRate,nOrderTotal,strShiptype,nCATax,nShippingID,nPromo)
			
			sCardType = request.form("cc_cardType")
			'sCardNum = request.form("cardNum")
			sCardNum = replace(sCardNum, Chr(34), "")
			sCardNum = replace(sCardNum, Chr(39), "")
			sCVV2 = request.form("secCode")
			
			sCC_Month = request.form("cc_month")
			sCC_Year = request.form("cc_year")
			sExp = right("00" & sCC_Month,2) & right(sCC_Year, 2)

		case 10
			if instr(HTTP_REFERER,"tabletmall.com") < 1 then
				response.write "<h3>This page will only accept forms submitted from the tabletMall.com secure website.</h3>" & vbcrlf
				response.end
			end if
			
			ssl_ProdIdCount = request.form("ssl_ProdIdCount")
			
			sSAddy1 = request.form("sAddress1")
			sSAddy2 = request.form("sAddress2")
			sSCity = request.form("sCity")
			sSState = request.form("sState")
			sSZip = request.form("sZip")
			
			sameaddress = prepStr(request.Form("sameaddress"))
			
			if sameaddress = "yes" then
				sBAddy1 = request.form("bAddress1")
				sBAddy2 = request.form("bAddress2")
				sBCity = request.form("bCity")
				sBState = request.form("bState")
				sBZip = request.form("bZip")
			else
				sBAddy1 = request.form("sAddress1")
				sBAddy2 = request.form("sAddress2")
				sBCity = request.form("sCity")
				sBState = request.form("sState")
				sBZip = request.form("sZip")
			end if
			
			if instr(sSZip,"-") > 0 then
				zipArray = split(sSZip,"-")
				sSZip = zipArray(0)
			end if
			if instr(sBZip,"-") > 0 then
				zipArray = split(sBZip,"-")
				sBZip = zipArray(0)
			end if
			
			sPromoCode = request.form("sPromoCode")
			nPromo = request.form("nPromo") ' this is couponid
			nSubTotal = request.form("subTotal")
			discountTotal = prepInt(request.Form("discountTotal"))
			useQty = request.form("nTotalQuantity")
			nShipRate = prepInt(request.form("shippingTotal"))
			nShippingID = request.form("sID" & request.form("radioAddress"))
			sCardNum = request.form("cardNum")
			if request.form("sstate") = "CA" then
				nCATax = round(cDbl(nSubTotal) * gblCaTax, 2)
			else
				nCATax = 0
			end if
			sEmailOpt = request.form("chkOptMail")
			nShipType = prepInt(request.form("shiptypeID"))
			ShoppingCartId = request.form("buysafecartID")
			WantsBondValue = request.form("WantsBondField")
			if request.form("buysafeamount") = "" then
				buysafeamount = 0
			else
				buysafeamount = cDbl(request.form("buysafeamount"))
			end if
			sCardType = request.form("cc_cardType")
			sCVV2 = request.form("secCode")
			sCC_Month = request.form("cc_month")
			sCC_Year = request.form("cc_year")
						
			sEmail = request.form("email")
			sFname = request.form("fname")
			sLname = request.form("lname")
			sPhone = request.form("phoneNumber")
			
			nAccountId = prepInt(request.Form("accountid"))
			parentAcctID = prepInt(request.Form("parentAcctID"))

	
		
			if len(sBZip) = 5 and isNumeric(sBZip) then
				sBCountry = "US"
			else
				if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
					sBCountry = "CANADA"
				else
					sBCountry = ""
				end if
			end if
			
			session("errorSQL") = "nSubTotal: " & nSubTotal & "<br>Qty:" & useQty
			if isnull(useQty) or len(useQty) < 1 or not isnumeric(useQty) then useQty = 0
			nTotalQuantity = cDbl(useQty)
			
			nAvsStreet = sBAddy1 &" "&sBAddy2
			nAvsStreet = replace(nAvsStreet, "&", "and")
			nAvsZip = sBZip
			
			session("errorSQL") = nSubTotal
			
			if len(sSZip) = 5 and isNumeric(sSZip) then
				sSCountry = "US"
			else
				if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
					sSCountry = "CANADA"
				else
					sSCountry = ""
				end if
			end if
			
			select case nShipType
				case "0"
					if sSCountry <> "US" then
						strShiptype = "First Class Int'l"
						nShipRate = 4.99 + (0 * nTotalQuantity)
					else
						strShiptype = "First Class"
						nShipRate = 4.99 + (0 * nTotalQuantity)
					end if
				case "2"
					if sSCountry <> "US" then
						strShiptype = "USPS Priority Int'l"
						nShipRate = 6.99 + (0 * nTotalQuantity)
					else
						strShiptype = "USPS Priority"
						nShipRate = 6.99 + (0 * nTotalQuantity)
					end if
				case "3"
					if freeship = 1 then
						strShiptype = "USPS Express"
						nShipRate = 0 + (0 * nTotalQuantity)
					else
						strShiptype = "USPS Express"
						nShipRate = 19.99 + (0 * nTotalQuantity)
					end if
				case "4" : strShiptype = "UPS Ground"
				case "5" : strShiptype = "UPS 3 Day Select"
				case "6" : strShiptype = "UPS 2nd Day Air"
			end select
			
			if ssl_ProdIdCount = "0" or ssl_ProdIdCount = "" or not isNumeric(ssl_ProdIdCount) then
				'basket currently empty
				response.redirect("/sessionexpired.asp?ec=101")
				response.end
			else
				ssl_ProdIdCount = cDbl(ssl_ProdIdCount)
			end if
			
			if nAccountId = 0 then
				'this is an insert new account (we_Accounts)
				SQL = "SET NOCOUNT ON; "
				if parentAcctID > 0 then
					SQL = SQL & "INSERT INTO " & accountTbl & " (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered,CustomerIP,parentID) VALUES ("
				else
					SQL = SQL & "INSERT INTO er_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered,CustomerIP) VALUES ("
				end if
				SQL = SQL & "'" & ds(sFname) & "', "
				SQL = SQL & "'" & ds(sLname) & "', "
				SQL = SQL & "'" & ds(sBAddy1) & "', "
				SQL = SQL & "'" & ds(sBAddy2) & "', "
				SQL = SQL & "'" & ds(sBCity) & "', "
				SQL = SQL & "'" & ds(sBState) & "', "
				SQL = SQL & "'" & ds(sBZip) & "', "
				SQL = SQL & "'" & ds(sBCountry) & "', "
				SQL = SQL & "'" & ds(sEmail) & "', "
				SQL = SQL & "'" & ds(sPhone) & "', "
				SQL = SQL & "'" & ds(sSAddy1) & "', "
				SQL = SQL & "'" & ds(sSAddy2) & "', "
				SQL = SQL & "'" & ds(sSCity) & "', "
				SQL = SQL & "'" & ds(sSState) & "', "
				SQL = SQL & "'" & ds(sSZip) & "', "
				SQL = SQL & "'" & ds(sSCountry) & "', "
				SQL = SQL & "'" & now & "', "
				SQL = SQL & "'" & Request.ServerVariables("REMOTE_ADDR") & "'"
				if parentAcctID > 0 then
					SQL = SQL & ",'" & ds(parentAcctID) & "') "
				else
					SQL = SQL & ") "
				end if
				SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
				SQL = SQL & "SET NOCOUNT OFF;"
				session("errorSQL") = SQL

				set RS = oConn.execute(SQL)
				if not RS.eof then nAccountId = RS("newAccountID")
			else
				sql =   "update er_accounts set fname = '" & ds(sFname) & "',lname = '" & ds(sLname) & "',bAddress1 = '" & ds(sBAddy1) & "',bAddress2 = '" & ds(sBAddy2) & "'," &_
						"bCity = '" & ds(sBCity) & "',bState = '" & ds(sBState) & "',bZip = '" & ds(sBZip) & "',bCountry = '" & ds(sBCountry) & "',email = '" & ds(sEmail) & "'," &_
						"phone = '" & ds(sPhone) & "',sAddress1 = '" & ds(sSAddy1) & "',sAddress2 = '" & ds(sSAddy2) & "',sCity = '" & ds(sSCity) & "'," &_
						"sState = '" & ds(sSState) & "',sZip = '" & ds(sSZip) & "',sCountry = '" & ds(sSCountry) & "',CustomerIP = '" & Request.ServerVariables("REMOTE_ADDR") &"' where accountID = " & nAccountId
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
			
			oConn.execute ("sp_ModifyEmailOpt_ER '" & ds(sFname) & "','" & sEmail & "','" & sEmailOpt & "'")
			
			strItems = ""
			CellPhoneInOrder = 0
			for a = 1 to ssl_ProdIdCount
				session("errorSQL") = "SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "' (" & a & ")"
				set oRsCheckType = oConn.execute("SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "'")
				if not oRsCheckType.eof then
					if oRsCheckType("typeID") = 16 then 
						CellPhoneInOrder = 1
						Freeship = 0 'check Free overnight shipping
					end if
					strItems = strItems & request.form("ssl_item_number_" & a) & ","
				end if
				oRsCheckType.close
				set oRsCheckType = nothing
			next
			
			strItems = left(strItems,len(strItems)-1)
			' end add new shoppingcart
			
			nSubTotal = formatNumber(nSubTotal,2)
			session("errorSQL") = nSubTotal & " + " & nCATax & " + " & nShipRate & " + " & buysafeamount & " + " & discountTotal
			nOrderTotal = nSubTotal + nCATax + nShipRate + buysafeamount - discountTotal
			nOrderTotal = formatNumber(nOrderTotal,2)
				
			'WRITE THE ORDER IN THE DATABASE
			nOrderID = fWriteBasketItemIntoDB(siteid,nAccountId,nSubTotal,nShipRate,nOrderTotal,strShiptype,nCATax,nShippingID,nPromo)
			
			sCardNum = replace(sCardNum, Chr(34), "")
			sCardNum = replace(sCardNum, Chr(39), "")
			
			sExp = right("00" & sCC_Month,2) & right(sCC_Year, 2)			
	end select


	bTestmode = false
	if sCardNum = "4000100011112224" then
		bTestmode = true		'yyy Test mode
	elseif sCardNum = "4000100011112222" then
		bTestmode = true		'NYZ Test mode
	elseif sCardNum = "5424180279791740" then
		bTestmode = true		'negative AVS Test mode
	elseif sCardNum = "371122223332241" then
		bTestmode = true		'Test mode
	elseif sCardNum = "4111111111111111" then
		bTestmode = true		'Test mode
	elseif sCardNum = "4000200011112226" then
		bTestmode = true		'N/A Test mode
	elseif sCardNum = "6011222233332273" then
		bTestmode = true		'Test mode
	elseif sCardNum = "4000600011112223" then
		bTestmode = true		'Test mode
	end if
	
	if bTestmode = true then
		if ds(sBAddy1) <> "4040 N. Palm St." then
			response.redirect("/?testError=1")
			response.end
		end if
	end if

	'====================================================================
	post_url = CC_TRANS_POST_URL
	Set post_values = CreateObject("Scripting.Dictionary")
	post_values.CompareMode = vbTextCompare

	post_values.Add "x_login", apiLoginID
	post_values.Add "x_tran_key", apiTransactionKey

	post_values.Add "x_version", "3.1"
	post_values.Add "x_type", "AUTH_ONLY"
	post_values.Add "x_method", "CC"
	post_values.Add "x_amount", nOrderTotal
	post_values.Add "x_card_num", ds(sCardNum)
	post_values.Add "x_exp_date", ds(sExp)
	post_values.Add "x_card_code", ds(sCVV2)
	post_values.Add "x_test_request", bTestmode
	
	post_values.Add "x_invoice_num", nOrderID
	post_values.Add "x_description", strInvoiceDescription
			
	'Billing Information
	post_values.Add "x_first_name", ds(sFname)
	post_values.Add "x_last_name", ds(sLname)
	post_values.Add "x_address", ds(sBAddy1) & " " & ds(sBAddy2)
	post_values.Add "x_city", ds(sBCity)
	post_values.Add "x_state", ds(sBState)
	post_values.Add "x_zip", ds(sBZip)
	post_values.Add "x_country", ds(sBCountry)
	post_values.Add "x_phone", ds(sPhone)
	post_values.Add "x_email", ds(sEmail)
	post_values.Add "x_customer_ip", Request.ServerVariables("REMOTE_ADDR")
	
	'Shipping Information
	post_values.Add "x_ship_to_first_name", ds(sFname)
	post_values.Add "x_ship_to_last_name", ds(sLname)
	post_values.Add "x_ship_to_address", ds(sSAddy1) & " " & ds(sSAddy2)
	post_values.Add "x_ship_to_city", ds(sSCity)
	post_values.Add "x_ship_to_state", ds(sSState)
	post_values.Add "x_ship_to_zip", ds(sSZip)
	post_values.Add "x_ship_to_country", ds(sSCountry)
	
	post_values.Add "x_delim_data", true
	post_values.Add "x_delim_char", "|"
	post_values.Add "x_recurring_billing", false
	post_values.Add "x_relay_response", false
	post_values.Add "x_duplicate_window", 5
	'====================================================================

	post_string = ""
	for each key in post_values
		post_string = post_string & key & "=" & Server.URLEncode(post_values(key)) & "&"
	next
	post_string = Left(post_string,Len(post_string)-1)
	
	Dim objRequest, post_response

	if not zeroOrder then
		Set objRequest = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
			objRequest.open "POST", post_url, false
			session("errorSQL") = "post_url:" & post_url
			objRequest.send post_string
			post_response = objRequest.responseText
		Set objRequest = nothing
	
		Dim response_array
		response_array = split(post_response, post_values("x_delim_char"), -1)
	
		nResponseStatus = response_array(0)
		responseText = response_array(3)
		authcode = response_array(4)
		avsStreet = response_array(5)
		transactionID = response_array(6)
		sCardCodeStatus = response_array(38)
		avsText = ""
		cvvText = ""
	end if

	if nResponseStatus = 1 then
		on error resume next
		if useCIM then
			sql = "select accountid, isnull(CIM_CustomerProfileID, 0) profileID from " & tblAccount & " where email = '" & ds(sEmail) & "'"
			set rsCIM = oConn.execute(sql)
			if not rsCIM.eof then
				do until rsCIM.eof
					if clng(rsCIM("profileID")) > 0 then	'delete customer profile if exists
						sql = 	"update	" & tblAccount & vbcrlf & _
								"set	CIM_CustomerProfileID = null" & vbcrlf & _
								"	,	CIM_CustomerPaymentProfileID = null" & vbcrlf & _
								"where	accountid = '" & rsCIM("accountid") & "'"
						oConn.execute(sql)
						
						xml	=	"<?xml version=""1.0"" encoding=""utf-8""?>" & vbcrlf & _
								"<deleteCustomerProfileRequest xmlns=""AnetApi/xml/v1/schema/AnetApiSchema.xsd"">" & vbcrlf & _
								"<merchantAuthentication>" & vbcrlf & _
								"	<name>" & apiLoginID & "</name>" & _
								"	<transactionKey>" & apiTransactionKey & "</transactionKey>" & _
								"</merchantAuthentication>" & vbcrlf & _
								"<customerProfileId>" & rsCIM("profileID") & "</customerProfileId>" & vbcrlf & _
								"</deleteCustomerProfileRequest>"
						set objResponse = SendApiRequest(CIM_APIURL, xml)
					end if
					rsCIM.movenext
				loop
			end if

'			cimMode = "liveMode"
'			if bTestmode then cimMode = "testMode"
			cimMode = "none"
			xml	=	"<?xml version=""1.0"" encoding=""utf-8""?>" & _
					"<createCustomerProfileRequest xmlns=""AnetApi/xml/v1/schema/AnetApiSchema.xsd"">" & _
					"	<merchantAuthentication>" & _
					"  		<name>" & apiLoginID & "</name>" & _
					"  		<transactionKey>" & apiTransactionKey & "</transactionKey>" & _
					"	</merchantAuthentication>" & _
					"	<profile>" & _
					"		<merchantCustomerId>" & nAccountID & "</merchantCustomerId>" & _
					"		<email>" & ds(sEmail) & "</email>" & _
					"		<paymentProfiles>" & _
					"  			<billTo>" & _
					"    			<firstName>" & ds(sFname) & "</firstName>" & _
					"    			<lastName>" & ds(sLname) & "</lastName>" & _
					"    			<company></company>" & _
					"    			<address>" & ds(sBAddy1) & " " & ds(sBAddy2) & "</address>" & _
					"    			<city>" & ds(sBCity) & "</city>" & _
					"    			<state>" & ds(sBState) & "</state>" & _
					"    			<zip>" & ds(sBZip)& "</zip>" & _
					"    			<phoneNumber>" & ds(sPhone) & "</phoneNumber>" & _
					"  			</billTo>" & _
					"  			<payment>" & _
					"    			<creditCard>" & _
					"      				<cardNumber>" & ds(sCardNum) & "</cardNumber>" & _
					"      				<expirationDate>20" & right(sExp, 2) & "-" & left(sExp, 2) & "</expirationDate>" & _
					"      				<cardCode>" & ds(sCVV2) & "</cardCode>" & _
					"    			</creditCard>" & _
					"  			</payment>" & _
					"		</paymentProfiles>" & _
					"	</profile>" & _
					"	<validationMode>" & cimMode & "</validationMode>" & _
					"</createCustomerProfileRequest>"
					
			set objResponse = SendApiRequest(CIM_APIURL, xml)
			if IsApiResponseSuccess(objResponse) then
				sql = 	"update " & tblAccount & vbcrlf & _
						"set 	CIM_CustomerProfileID = '" & objResponse.selectSingleNode("/*/api:customerProfileId").Text & "'" & vbcrlf & _
						"	, 	CIM_CustomerPaymentProfileID = '" & objResponse.selectSingleNode("/*/api:customerPaymentProfileIdList/api:numericString").Text & "'" & vbcrlf & _
						"where	accountid = '" & nAccountID & "'"
				session("errorSQL") = sql
				oConn.execute(sql)
			else
				'E00039: already exists.
				'DO NOTHING FOR NOW
'				cimErrorCode = objResponse.selectSingleNode("/*/api:messages/api:message/api:code").Text
'				cimErrorText = objResponse.selectSingleNode("/*/api:messages/api:message/api:text").Text
'				response.write "cimErrorCode:" & cimErrorCode & "<br>"
'				response.write "cimErrorText:" & cimErrorText & "<br>"
'				response.end
'				if cimErrorCode <> "E00039" then	
'					
'				end if
			end if			
		end if
		on error goto 0
		
		if siteid = 0 and request.form("trackingSessionID") <> "" then
			tsql = "update Tracking set processingApproved = 1 where sessionID = " & request.form("trackingSessionID")
			session("errorSQL") = tsql
			set	RS = oConn.execute(tsql)
		end if
		
		SQL = "UPDATE we_orders SET transactionID = '" & transactionID & "', approved = 1 WHERE orderid = '" & nOrderID & "'"
		set updateRS = oConn.execute(SQL)
		
		dim thisItemID, thisQuantity
		'update Number of sales
		SQL = "SELECT A.itemID, A.quantity, B.ItemKit_NEW FROM we_orderdetails A INNER JOIN we_items B ON A.itemID=B.itemID WHERE A.orderID = '" & nOrderID & "'"
		'session("errorSQL") = SQL
		set orderdetailRS = oConn.execute(SQL)
		do until orderdetailRS.eof
			thisItemID = orderdetailRS("itemID")
			thisQuantity = orderdetailRS("quantity")
			if isNull(orderdetailRS("ItemKit_NEW")) then
				SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & thisQuantity & " WHERE itemID = '" & thisItemID & "'"
			else
				SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & thisQuantity & " WHERE itemID IN (" & orderdetailRS("ItemKit_NEW") &","&thisItemID & ")"
			end if
			session("errorSQL") = request.servervariables("PATH_INFO") & ":<br>" & SQL
			oConn.execute SQL
			orderdetailRS.movenext
		loop
		' update ShoppingCart table
		SQL = "UPDATE ShoppingCart SET purchasedOrderID = '" & nOrderID & "' WHERE store = '" & siteid & "' AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
		SQL = SQL & " AND itemID IN (" & strItems & ")"
		session("errorSQL") = SQL
		oConn.execute SQL
		
		if mobileOrder = 1 then
			sql = "update we_orders set mobileSite = 1 where orderID = '" & nOrderID & "'"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if	
		
		select case siteid
			case 0
				if instr(siteName,"m.wire") > 0 or instr(siteName,"mdev.wire") > 0 then
					response.redirect("/cart/complete.htm?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal & "&mobileOrder=" & mobileOrder & "&pp=Y")
				else
					response.redirect("/cart/complete?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal & "&mobileOrder=" & mobileOrder & "&pp=Y")
				end if
			case 1
				response.redirect("/complete?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal)
			case 2
				if prepInt(mobileOrder) = 1 then
					response.redirect("/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal & "&pp=Y")
				else
					response.redirect("/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal & "&pp=Y")
				end if
			case 10
				response.redirect("/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal)
			case else
				response.redirect("/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal)
		end select
	else
		if siteid = 2 then
		%>
        <form name="declinedOrder" action="/cart/process/declined.asp" method="post">
        	<input type="hidden" name="declinedOrderID" value="<%=nOrderID%>" />
        </form>
        <script>
			window.onload = function () { document.declinedOrder.submit(); }
        </script>
        <%
			response.end
		end if
		
		if siteid = 0 and request.form("trackingSessionID") <> "" then
			tsql = "update Tracking set processingApproved = 0 where sessionID = " & request.form("trackingSessionID")
			session("errorSQL") = tsql
			set	RS = oConn.execute(tsql)
		end if

		select case nResponseStatus
			case 2 : strResponseStatus = "Declined"
			case 3 : strResponseStatus = "Error"
			case 4 : strResponseStatus = "Held For Review"
		end select
		select case ucase(avsStreet)
			case "A" : avsText = "Address (Street) matches, ZIP does not"
			case "B" : avsText = "Address information not provided for AVS check"
			case "E" : avsText = "AVS error"
			case "G" : avsText = "Non-U.S. Card Issuing Bank"
			case "N" : avsText = "No Match on Address (Street) or ZIP"
			case "P" : avsText = "Invalid Credit Card Number"		'Old 2012-10-18: "AVS not applicable for this transaction"
			case "R" : avsText = "Retry - system unavailable or timed out"
			case "S" : avsText = "Service not supported by issuer"
			case "U" : avsText = "Address information is unavailable"
			case "W" : avsText = "Nine digit ZIP matches, Address (Street) does not"
			case "X" : avsText = "Address (Street) and nine digit ZIP match"
			case "Y" : avsText = "Wrong Expiration Date or CVV"		'Old 2012-10-18: "Address (Street) and five digit ZIP match"
			case "Z" : avsText = "Five digit ZIP matches, Address (Street) does not"
			case else : avsText = "Error occured during process"
		end select
		select case ucase(cardCodeStatus)
			case "M" : cvvText = "Match"
			case "N" : cvvText = "No Match"
			case "P" : cvvText = "Not Processed"
			case "S" : cvvText = "Should have been present"
			case "U" : cvvText = "Issuer unable to process request"
		end select
		ErrorStr = strResponseStatus & ": " & avsText & ". " & cvvText
		
		if siteid = 0 and request.form("trackingSessionID") <> "" then
			tsql = "update Tracking set processingErrorMessage = '" & ds(ErrorStr) & "' where sessionID = " & request.form("trackingSessionID")
			session("errorSQL") = tsql
			set	RS = oConn.execute(tsql)
		end if

	end if

'	response.write "<br><br>"		
'	response.write "<ol>" & vbcrlf
'	for each val in response_array
'		response.write "<li>" & val & "</li>" & vbcrlf
'	next
'	response.write "</ol>" & vbcrlf
'	response.end
	response.write "<html><head><link rel=stylesheet href=""/includes/css/style.css"" type=""text/css""></head><body bgcolor=#ffffff text=#000000 leftmargin=0 topmargin=0 marginwidth='0' marginheight='0' link=#666666 vlink=#666666 alink=#ff6600>" & vbcrlf
	%>
	<br><br>
	<table cellpadding="3" cellspacing="1" width="75%" border="1" align="center" class="mc-text" style="border-collapse:collapse;">
		<tr>
			<td align=middle><a href="/"><img src="<%=imgLogo%>" border="0"></a></td>
		</tr>
		<tr>
			<td>
				<strong>
					<%
					if ErrorStr = "" then
						%>
						Due to security reasons, we are unable to process your order online at this time.
						<%
					else
						response.write ErrorStr
					end if
					%>
					<br><br>
				</strong>
			</td>
		</tr>
		<tr>
			<td>
				<br>Please resubmit your order for processing.
				<br>If you continue to experience difficulties, please contact us at <%=sitePhone%> and an Associate will be happy to process your order immediately over the phone.
				<br><br>
				Thank you for shopping with <%=siteURL%>!
				<br><br>
				<a href="mailto:<%=strSiteEmail%>"><%=strSiteEmail%></a>
                <br /><br />
                <div align="left" style="padding:10px 0px 20px 10px;">
                <%if siteid = 0 then%>
                    <!--start http://www.livehelpnow.net  -->
                    <!--This DIV object will show the live chat button, have it placed in the location on your website where you would like the live chat button to show-->
                    <div id="lhnContainer" style="float:left; text-align:left; display:inline-block; font-weight:bold; color:#222; padding-right:10px;">
                    	Chat with live agent:
                    </div>
                    <div id="lhnContainer" style="float:left; text-align:left; width:50%; padding-top:3px;">
                        <div id="lhnChatButton"></div>
                    </div>
                    <!--You may install the following code in an external JavaScript file if you like-->
                    <script type="text/javascript">
                        // Misc Options
                        var lhnAccountN = 3410; //LiveHelpNow account #
                        var lhnButtonN = 998; //Button #
                        var lhnVersion = 5.3; //LiveHelpNow version #
                        // Chat Options
                        var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://"); //to force secure chat replace this line with: var lhnJsHost = 'https://';
                        var lhnInviteEnabled = 1; //Invite visitor to chat automatically
                        var lhnInviteChime = 0; //1 = disable invite beep sound,0 = keep invite beep sound enabled
                        var lhnWindowN = 0; //Chat window #, leave 0 to open default window setup for your account
                        var lhnDepartmentN = 0; //Department #, leave 0 to not route by department
                        var lhnCustomInvitation = ''; //change to 1 to use custom invitation, see this article for customization instructions: http://help.livehelpnow.net/article.aspx?cid=1&aid=1739
                        var lhnCustom1 = document.URL.toString(); //Custom1 feed value please use encodeURIComponent() function to encode your values
                        var lhnCustom2 = ''; //Custom2 feed value please use encodeURIComponent() function to encode your values
                        var lhnCustom3 = ''; //Custom3 feed value please use encodeURIComponent() function to encode your values
                        var lhnTrackingEnabled = 't'; //change to 'f' to disable visitor tracking
                        // Options End
                        var lhnScriptSrc = lhnJsHost + 'www.livehelpnow.net/lhn/scripts/livehelpnow.aspx?lhnid=' + lhnAccountN + '&iv=' + lhnInviteEnabled + '&d=' + lhnDepartmentN + '&ver=' + lhnVersion + '&rnd=' + Math.random() + '&custom1=' + lhnCustom1;
                        var lhnScript = document.createElement("script"); lhnScript.type = "text/javascript";lhnScript.src = lhnScriptSrc;
                        if (window.addEventListener) {
                            window.addEventListener('load', function () { document.getElementById('lhnContainer').appendChild(lhnScript); }, false);
                        }
                        else if (window.attachEvent) {
                            window.attachEvent('onload', function () { document.getElementById('lhnContainer').appendChild(lhnScript); });
                        }
                    </script>
                    <!--end http://www.livehelpnow.net  -->                
                <%end if%>
                </div>
			</td>
		</tr>
		<tr><td height="5" bgcolor="#CCCCCC"><img src="/images/spacer.gif" width="1" height="5" border="0"></td></tr>
		<tr>
			<td>
				<p>We apologize for the issues that you are having with this transaction. Please verify that the following information is entered correctly:</p>
				<ul>
					<li>Billing Address (your billing address must be entered exactly as it is on your credit card statement)</li>
					<li>Credit Card Number</li>
					<li>Expiration Date</li>
					<li>CVV Code (you can find this 3 digit code on the back of the card, for Amex the code can also be 4 digits and be located on the front)</li>
				</ul>
				<p>If all of this information is entered correctly please fill out the form below and we will get back to you shortly.</p>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="mc-text">
					<form action="/cart/DeclineProcess.asp" method="post">
					<tr>
						<td valign="top">Customer Name on Order:</p></td>
						<td valign="top"><p><input type="text" name="CustomerName" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Email address associated with order:</p></td>
						<td valign="top"><p><input type="text" name="EmailAddress" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Contact phone number:</p></td>
						<td valign="top"><p><input type="text" name="ContactPhone" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Error code given:</p></td>
						<td valign="top"><p><input type="text" name="ErrorCode" size="80" value="<%=ErrorStr%>"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Additional comments/concerns:</p></td>
						<td valign="top"><p><textarea name="AdditionalComments" cols="65" rows="3"></textarea></p></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><p><input type="submit" name="submitted" value="Send Form"></p></td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
<%
function fWriteBasketItemIntoDB(nSiteID,nAccountId,nSubTotal,sShipRate,nOrderTotal,strShiptype,sTax,shippingid,nPromo)
	'this function will write the basket items into the database we_orders
	if ssl_ProdIdCount = 0 then
		'basket currently empty
		response.redirect("/sessionexpired.asp?ec=102")
		response.end
	else
		'insert into WE_Order table
		SQL = "sp_InsertOrder3 '" & nSiteID & "','" & nAccountId & "','" & FormatNumber(nSubTotal,2) & "','" & FormatNumber(sShipRate,2) & "'," & sTax & ",'" & FormatNumber(nOrderTotal,2) & "','" & replace(strShiptype, "'", "''") & "','" & shippingid & "','" & nPromo & "','" & Now & "'"
		session("errorSQL") = SQL
		nOrderId = oConn.execute(SQL).fields(0).value
		session("invoice") = nOrderId
		'initialize basket variables
		dim sSprocString
		sSprocString = ""
		for a = 1 to ssl_ProdIdCount
			nIdProd = request.form("ssl_item_number_" & a)
			nQtyProd = request.form("ssl_item_qty_" & a)
			nPrice = request.form("ssl_item_price_" & a)
			'get string for the insertorderdetail stored proc
			'To be integrated into a new stored procedure in the future 2012-10-12
			if nPrice <> "" then
				'updatePrice = "update we_orderdetails set price = '" & nPrice & "' where orderid = '" & nOrderId & "' and itemid = '" & nIdProd & "'"
				'response.write updatePrice & "<br />"
				'oConn.execute updatePrice
				sSprocString = "sp_InsertOrderDetailShaun '" & nOrderId & "','" & nIdProd & "','" & nQtyProd & "','" & nPrice & "'"		
			else
				sSprocString = "sp_InsertOrderDetail '" & nOrderId & "','" & nIdProd & "','" & nQtyProd & "'"
			end if
			'stored procedure that will write each orderitem detail within the orderdetail db
			oConn.execute(sSprocString)
		next
	end if
	
	sql = "update we_orderDetails set musicSkins = 1 where itemID >= 1000000 and (musicSkins is null or musicSkins = 0) and orderid ='" & nOrderId & "'"
	session("errorSQL") = sql
	oConn.execute(sql)	

	sql = "update we_orders set sessionID = '" & mySession & "' where orderID = '" & nOrderId & "'"
	session("errorSQL") = sql
	oConn.execute(sql)
		
	if nSiteID = 0 then
		sql = "update we_orders set shoprunnerID = '" & prepStr(session("sr_token")) & "', note = '" & session("specialOfferText") & "' where orderID = '" & nOrderId & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
	elseif nSiteID = 10 then
		sql = "update we_orders set note = '" & session("specialOfferText") & "' where orderID = '" & nOrderId & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	fWriteBasketItemIntoDB = nOrderId
end function
%>