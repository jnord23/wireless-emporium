<%

'follow class to be separated later as inc_DelimitedList
Class DelimitedList
	'************** Properties **************
	Public List
	Public Delimiter
	Public IgnoreEmptyValue
	'****************************************

	'*********** Event Handlers *************
	Private Sub Class_Initialize()
		List=EMPTY_STRING
		Delimiter="|"
		IgnoreEmptyValue=True
	End Sub
	'****************************************

	'************ Property Get **************
	Public Property Get Item( byval strName)
		Item = EMPTY_STRING

		dim strNameValue
		for each strNameValue in split( List, Delimiter)
			if instr( 1, strNameValue, strName&"=", VBTextCompare)=1 then Item = replace(( split( strNameValue, "="))(1),"%3D", "="): exit Property
		next		
	End Property

	Public Property Get Count
		Count=0
		if HasLen( List) then Count = UBound( split( List, Delimiter))+1		
	End Property

	Public Property Get AsArray
		AsArray = split( List, Delimiter)
	End Property

	Public Property Get AsQueryString
		AsQueryString = List
		if HasLen( AsQueryString) then AsQueryString = "?" & AsQueryString
	End Property	

	Public Property Get IsEmpty
		IsEmpty = HasNoLen( List)
	End Property

	'****************************************


	'************** Methods *****************

	Private Function IsInsertable( byval strValue)
	  IsInsertable = ( ( IgnoreEmptyValue and HasLen( strValue)) or not IgnoreEmptyValue)
	End Function

	Public Sub Add( byval strName, byval strValue)
	  if HasLen( strName) and IsInsertable( strValue) then
	    if HasLen( List) then List = List & Delimiter
	    List = List & strName &"="& replace( strValue, "=", "%3D")
	  end if
	End Sub

	Public Sub AddUnique( strName, strValue)
		call Remove( strName)
		call Add( strName, strValue)
	End Sub

	Public Sub AddElement( byval strValue)
	  if IsInsertable( strValue) then
		if HasLen( List) then List = List & Delimiter
		List = List & strValue
	  end if
	End Sub

	Public Sub AddElementUnique( strValue)
		call RemoveElement( strValue)
		call AddElement( strValue)
	End Sub

	Public Sub RemoveElement( byval strValue)
		dim strListNew: strListNew=EMPTY_STRING
		dim strElement
	
		for each strElement in split( List, Delimiter)
			if not IsEqual( strElement, strValue) then
				if HasLen( strListNew) then strListNew = strListNew & Delimiter
				strListNew = strListNew & strElement
			end if
		next

		List = strListNew
	End Sub

	Public Sub Remove( byval strName)
		dim strListNew: strListNew=EMPTY_STRING
		dim strNameValue 
	
		for each strNameValue in split( List, Delimiter)
			if not StartsWith( strNameValue, strName&"=") then
				if HasLen( strListNew) then strListNew = strListNew & Delimiter
				strListNew = strListNew & strNameValue
			end if
		next
		
		List = strListNew
	End Sub
	'****************************************
End Class

%>