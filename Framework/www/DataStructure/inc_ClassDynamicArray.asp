<%
' adopted from http://www.4guysfromrolla.com/webtech/032800-1.shtml [knguyen/20110603]

Class DynamicArray
	'************** Properties **************
	Private m_lstArray
	Private m_intCount
	'****************************************

	'*********** Event Handlers *************
	Private Sub Class_Initialize()
		Redim m_lstArray(0)
		m_intCount=0
	End Sub
	'****************************************

	'************ Property Get **************
	Public Property Get Item(intPos)
		'Make sure the end developer is not requesting an
		'"out of bounds" array element
		If intPos < LBound(m_lstArray) or intPos > UBound(m_lstArray) then
			Exit Property		'Invalid range
		End If

		Item = m_lstArray(intPos)
	End Property
	
	
	Public Property Get LowerBound
		LowerBound = LBound(m_lstArray)
	End Property
	
	Public Property Get UpperBound
		UpperBound = UBound(m_lstArray)
	End Property

	Public Property Get Length
		Length = m_intCount
	End Property

	Public Property Get AsArray
		AsArray = m_lstArray
	End Property
	'****************************************

	'************ Property Let **************
	Public Property Let Item(intPos, objValue)
		'Make sure intPos >= LBound(m_lstArray)
		If intPos < LBound(m_lstArray) Then Exit Property

		If intPos > UBound(m_lstArray) then
			'We need to resize the array
			Redim Preserve m_lstArray(intPos)
			m_lstArray(intPos) = objValue
		Else
			'We don't need to resize the array
			m_lstArray(intPos) = objValue
		End If
	End Property
	'****************************************


	'************** Methods *****************
	Public Sub Add(objValue)
		Redim Preserve m_lstArray(m_intCount)
		m_lstArray(UpperBound) = objValue
		m_intCount=m_intCount+1
	End Sub

	Public Sub RemoveAt(intPos)
		 'Make sure intPos is within acceptable ranges
		 If intPos < LBound(m_lstArray) or intPos > UBound(m_lstArray) then
			 Exit Sub		'Invalid range
		 End If

		 Dim intIndex
		 For intIndex = intPos to UBound(m_lstArray) - 1
			 m_lstArray(intIndex) = m_lstArray(intIndex + 1)
			 m_intCount=m_intCount-1
		 Next

		 Redim Preserve m_lstArray(UBound(m_lstArray) - 1)
	End Sub
	'****************************************

	Public Sub RemoveFirst()
		 call Remove( LowerBound)
	End Sub	

	Public Sub RemoveLast()
		 call Remove( UpperBound)
	End Sub	

End Class

%>