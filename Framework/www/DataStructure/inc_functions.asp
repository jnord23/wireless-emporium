<!--#include file="inc_ClassDynamicArray.asp"--><%

function stripHTML(strText)
	if not isnull(strText) and "" <> strText then
		lap = 0
		do while instr(strText,"<") > 0
			lap = lap + 1
			strText = left(strText,instr(strText,"<")-1) & mid(strText,instr(strText,">")+1)
		loop
	end if	
	stripHTML = strText
end function

'safe check if value is not empty [knguyen/20110509]
function hasLen(objValue)
	hasLen=true
	if isnull(objValue) then 
		hasLen=false
	else
		if len(trim(objValue))=0 then 
			hasLen=false
		end if
	end if
end function

'safe check if value is empty [knguyen/20110509]
function hasNoLen(objValue)
	hasNoLen=not hasLen(objValue)
end function

'polish string values before use [knguyen/20110509]
function obj2Str(objValue)
	obj2Str=""
	if hasLen(objValue) then 
		obj2Str=trim(cstr(objValue))	
	end if
end function

'escape string value for eval
function EvalStr(objValue)
	EvalStr=""""""
	if hasLen(objValue) then 
		EvalStr=""""&replace(trim(cstr(objValue)),"""","""""")&"""" 'handles the db quotes
	end if
end function

'escape string value for db
function DbStr(objValue)
	DbStr="''"
	if hasLen(objValue) then 
		DbStr="'"&replace(trim(cstr(objValue)),"'","''")&"'" 'handles the db quotes
	end if
end function

function DbStrNil(objValue)
	DbStrNil = DbStr(objValue)
	if DbStrNil="''" then DbStrNil = "null"
end function


'validate Id value for db
function DbId( strValue)
	DbId=EMPTY_STRING
	if isId( strValue) then 
		DbId=strValue
	end if
end function

function DbIdNil( strValue)
	DbIdNil = DbId( strValue)
	if HasNoLen( DbIdNil) then DbIdNil = "null"
end function

'validate Id value for db
function DbNum( strValue)
	DbNum=EMPTY_STRING
	if hasLen( strValue) then 
		if isnumeric( strValue) then 
			DbNum=strValue
		end if
	end if
end function

'validate Id value for db
function DbNumNil( strValue)
	DbNumNil = DbNum( strValue)
	if HasNoLen( DbNumNil) then DbNumNil = "null"
end function


'safe check if value is an id[knguyen/20110509]
function isId( strValue)
	isId = false
	if hasLen( strValue) and isNumeric( strValue) then
		if clng( strValue)>=0 then isId = true
	end if
end function

'crash-proof case-insenstive compare 
function IsEqual( byval strValueX, byval strValueY)
	if IsObject( strValueX) or IsObject( strValueY) then 'cannot compare objects
		IsEqual = false
	else
		IsEqual = ( lcase( Obj2Str( strValueX)) = lcase( Obj2Str( strValueY)))
	end if
end function

'safe check if value is an id[knguyen/20110509]
function StartsWith( byval strString, byval strStartStringToCheck)
	StartsWith = haslen( strStartStringToCheck) and ( instr(1, strString, strStartStringToCheck, vbTextCompare) = 1)
end function

sub SafeOverride( byref strVariable, byval strValue)
	if hasLen( strValue) then strVariable = strValue
end sub

function GetNewConn()
	dim objConn: set objConn=Server.CreateObject("ADODB.Connection")
	dim strConnStr: strConnStr = obj2Str(Session("ConnectionStringSqlServer"))
	if not hasLen( strConnStr) then strConnStr=obj2Str(Session("ConnectionString"))
	objConn.Open strConnStr
	set GetNewConn=objConn
end function

sub CloseConn( byref objConn)
	if isobject(objConn) then
		objConn.Close
		set objConn = nothing
	end if
end sub

function DbSingleValue( byref strSql)
	dim objConn: set objConn=GetNewConn()
	dim objRs: set objRs = objConn.execute( strSql)

	if not objRs.eof then
		DbSingleValue = obj2Str(objRs.Fields.Item(0).Value)
	else
		DbSingleValue = EMPTY_STRING
	end if
	
	objRs.close()
	set objRs=nothing

	call CloseConn( objConn)
end function




function DbList( byref strSql)
	dim objConn: set objConn=GetNewConn()
	dim objRs: set objRs = objConn.execute( strSql)

	dim lstReturnValue: set lstReturnValue = new DynamicArray

	while not objRs.eof
		lstReturnValue.Add( obj2Str( objRs.Fields.Item(0).Value))
		objRs.MoveNext()
	wend
	
	objRs.close()
	set objRs=nothing

	call CloseConn( objConn)

	set DbList = lstReturnValue
end function


function ApplyPageAttribute( byval strText, byref dicReplaceAttribute)
	ApplyPageAttribute=strText
	for each strKey in dicReplaceAttribute
		ApplyPageAttribute = replace( ApplyPageAttribute, "{"&strKey&"}", dicReplaceAttribute( strKey))
	next 
end function

sub LookupSeoAttributes()
	'DBGMODE=true
	'make the call and override existing meta values as needed

	dim dicSeo: set dicSeo = GetSeoAttributes( dicSeoAttributeInput)
	call SafeOverride( SEtitle, ApplyPageAttribute( dicSeo.Item("Title"), dicReplaceAttribute))
	call SafeOverride( SEdescription, ApplyPageAttribute( dicSeo.Item("Description"), dicReplaceAttribute))
	call SafeOverride( SEkeywords, ApplyPageAttribute( dicSeo.Item("Keywords"), dicReplaceAttribute))
	call SafeOverride( topText, ApplyPageAttribute( dicSeo.Item("TopText"), dicReplaceAttribute))
	call SafeOverride( bottomText, ApplyPageAttribute( dicSeo.Item("BottomText"), dicReplaceAttribute))
	call SafeOverride( strH1, ApplyPageAttribute( dicSeo.Item("H1"), dicReplaceAttribute))
	call SafeOverride( strH2, ApplyPageAttribute( dicSeo.Item("H2"), dicReplaceAttribute))
	call SafeOverride( strBreadcrumb, ApplyPageAttribute( dicSeo.Item("BreadCrumbText"), dicReplaceAttribute))
	call SafeOverride( strAltText, ApplyPageAttribute( dicSeo.Item("AltText"), dicReplaceAttribute))
	'call DbgGlobal( "SEtitle")
	set dicSeo = nothing
	set dicReplaceAttribute =  nothing
	set dicSeoAttributeInput =  nothing
end sub

const SQL_SOFTMATCH_END = "null) is null"

function GetSeoAttributes( byref dicSeoAttributeInput)
	dim objConn: set objConn=GetNewConn()

	'response.end
	dim strBrandId: strBrandId = obj2Str( dicSeoAttributeInput( "BrandId"))
	dim strModelId: strModelId = obj2Str( dicSeoAttributeInput( "ModelId"))
	dim strCategoryId: strCategoryId = obj2Str( dicSeoAttributeInput( "CategoryId"))
	dim strCarrierId: strCarrierId = obj2Str( dicSeoAttributeInput( "CarrierId"))
	dim strIsPhone: strIsPhone = obj2Str( dicSeoAttributeInput( "IsPhone"))

	if isId( strBrandId) then strBrandId = DbId( strBrandId) &") = "& DbId( strBrandId) else strBrandId = SQL_SOFTMATCH_END
	if isId( strModelId) then strModelId = DbId( strModelId) &") = "& DbId( strModelId) else strModelId = SQL_SOFTMATCH_END
	if isId( strCategoryId) then strCategoryId = DbId( strCategoryId) &") = "& DbId( strCategoryId) else strCategoryId = SQL_SOFTMATCH_END
	if isId( strCarrierId) then strCarrierId = DbId( strCarrierId) &") = "& DbId( strCarrierId) else strCarrierId = SQL_SOFTMATCH_END
	if isId( strIsPhone) then strIsPhone = DbNum( strIsPhone) &") = "& DbNum( strIsPhone) else strIsPhone = SQL_SOFTMATCH_END

	dim dicReturnValue: set dicReturnValue = CreateObject("Scripting.Dictionary")

	'--db-driven seo-logic [knguyen/20110509]
	dim strSql: strSql = "select top 1 [Title], [Description], [Keywords], [H1], [H2], [TopText], [BottomText], [AltText], [BreadCrumbText]" &_
" from MetaTags with(nolock)" & _ 
" where [SiteId]="&DbId( obj2Str( Application("SiteId"))) &_
" and isnull( [PhysicalAbsolutePath], " & DbStr(GetPhysicalAbsolutePath())&") = "&DbStr(GetPhysicalAbsolutePath()) &_
" and isnull( [TypeId],"& strCategoryId &_
" and isnull( [BrandId],"& strBrandId &_
" and isnull( [ModelId],"& strModelId &_
" and isnull( [CarrierId],"& strCarrierId &_
" and isnull( [IsPhone],"& strIsPhone &_
" order by [PhysicalAbsolutePath] desc," &_
" case when [TypeId] is null then 0 else 1 end" &_
" + case when [BrandId] is null then 0 else 1 end" &_
" + case when [ModelId] is null then 0 else 1 end" &_
" + case when [CarrierId] is null then 0 else 1 end" &_
" + case when [IsPhone] is null then 0 else 1 end desc"
	if DBGMODE then response.write "<pre>" & strSql & "</pre>": response.end

	dim objRsMeta: set objRsMeta = Server.CreateObject("ADODB.Recordset")
	objRsMeta.open strSql, objConn, 0, 1 ' adOpenForwardOnly, adLockReadOnly 'assumes existing connection (pooled)
	if not objRsMeta.eof then
		dicReturnValue( "Title") = obj2Str(objRsMeta("Title").Value)
		dicReturnValue( "Description") = obj2Str(objRsMeta("Description").Value)
		dicReturnValue( "Keywords") = obj2Str(objRsMeta("Keywords").Value)
		dicReturnValue( "TopText") = obj2Str(objRsMeta("TopText").Value)
		dicReturnValue( "BottomText") = obj2Str(objRsMeta("BottomText").Value)
		dicReturnValue( "H1") = obj2Str(objRsMeta("H1").Value)
		dicReturnValue( "H2") = obj2Str(objRsMeta("H2").Value)
		dicReturnValue( "AltText") = obj2Str(objRsMeta("AltText").Value)
		dicReturnValue( "BreadCrumbText") = obj2Str(objRsMeta("BreadCrumbText").Value)		
	else
		dicReturnValue( "Title") = EMPTY_STRING
		dicReturnValue( "Description") = EMPTY_STRING
		dicReturnValue( "Keywords") = EMPTY_STRING
		dicReturnValue( "TopText") = EMPTY_STRING
		dicReturnValue( "BottomText") = EMPTY_STRING
		dicReturnValue( "H1") = EMPTY_STRING
		dicReturnValue( "H2") = EMPTY_STRING
		dicReturnValue( "AltText") = EMPTY_STRING
		dicReturnValue( "BreadCrumbText") = EMPTY_STRING	
	end if
	
	objRsMeta.close()
	set  objRsMeta=nothing
	
	set GetSeoAttributes = dicReturnValue	
	call CloseConn( objConn)
end function


function GetContentEventText( byref dicPageAttributeInput)
	'response.end
	dim objConn: set objConn=GetNewConn()
	dim strBrandId: strBrandId = obj2Str( dicPageAttributeInput( "BrandId"))
	dim strModelId: strModelId = obj2Str( dicPageAttributeInput( "ModelId"))
	dim strCategoryId: strCategoryId = obj2Str( dicPageAttributeInput( "CategoryId"))
	dim strCarrierId: strCarrierId = obj2Str( dicPageAttributeInput( "CarrierId"))
	dim strIsPhone: strIsPhone = obj2Str( dicPageAttributeInput( "IsPhone"))
	dim strGenre: strGenre = obj2Str( dicPageAttributeInput( "Genre"))
	dim strVendorOrder: strVendorOrder = Ck("vendororder")

	if isId( strBrandId) then strBrandId = DbId( strBrandId) &") = "& DbId( strBrandId) else strBrandId = SQL_SOFTMATCH_END
	if isId( strModelId) then strModelId = DbId( strModelId) &") = "& DbId( strModelId) else strModelId = SQL_SOFTMATCH_END
	if isId( strCategoryId) then strCategoryId = DbId( strCategoryId) &") = "& DbId( strCategoryId) else strCategoryId = SQL_SOFTMATCH_END
	if isId( strCarrierId) then strCarrierId = DbId( strCarrierId) &") = "& DbId( strCarrierId) else strCarrierId = SQL_SOFTMATCH_END
	if isId( strIsPhone) then strIsPhone = DbNum( strIsPhone) &") = "& DbNum( strIsPhone) else strIsPhone = SQL_SOFTMATCH_END
	if isId( strGenre) then strGenre = DbStr( strGenre) &") = "& DbStr( strGenre) else strGenre = SQL_SOFTMATCH_END
	if isId( strVendorOrder) then strVendorOrder = DbStr( strVendorOrder) &") = "& DbStr( strVendorOrder) else strVendorOrder = SQL_SOFTMATCH_END

	dim dicReturnValue: set dicReturnValue = CreateObject("Scripting.Dictionary")

	'--db-driven content event-logic [knguyen/20110520]
	dim strSql: strSql = _
" select [Name] as [Name], t.[Text] as [Value]" &_ 
" from ContentEvent e with(nolock)" &_
" inner join ContentEventText t with(nolock)" &_
" on t.[ContentEventTextId]=e.[ContentEventTextId] and t.[IsActive]=1 and e.[IsActive]=1" &_
" and getdate() between isnull( e.[StartDate], dateadd( dd, -1, getdate())) and isnull( e.[EndDate], dateadd( dd, 1, getdate()))" &_
" and [SiteId]="& DbId( obj2Str( Application("SiteId"))) &_
" and isnull( [PhysicalAbsolutePath], "& DbStr(GetPhysicalAbsolutePath()) &") = "& DbStr(GetPhysicalAbsolutePath()) &_
" and isnull( [TypeId],"& strCategoryId &_
" and isnull( [BrandId],"& strBrandId &_
" and isnull( [ModelId],"& strModelId &_
" and isnull( [CarrierId],"& strCarrierId &_
" and isnull( [IsPhone],"& strIsPhone &_
" and isnull( [Genre],"& strGenre &_
" and isnull( [VendorOrder],"& strVendorOrder &_
" inner join ContentEventTextType c with(nolock)" &_
" on c.[ContentEventTextTypeId]=t.[ContentEventTextTypeId] and c.[IsActive]=1" &_
" order by [ContentEventId]"
	if DBGMODE then response.write "<pre>" & strSql & "</pre>": response.end

	dim objRsMeta: set objRsMeta = Server.CreateObject("ADODB.Recordset")
	objRsMeta.open strSql, objConn, 0, 1 ' adOpenForwardOnly, adLockReadOnly 'assumes existing connection (pooled)
	dim strName: strName = EMPTY_STRING
	dim strValue: strValue = EMPTY_STRING

	while not objRsMeta.eof ' stream all values into one string
		strName = obj2Str( objRsMeta("Name").Value)
		dicReturnValue( strName) = obj2Str( dicReturnValue( strName)) & obj2Str( objRsMeta("Value").Value)
		objRsMeta.MoveNext
	wend

	objRsMeta.close()
	set  objRsMeta=nothing
	call CloseConn( objConn)

	set GetContentEventText = dicReturnValue
end function

sub OutputPixel( byref dicContentEventText)
	if not isobject( dicContentEventText) then 'hack fallback logic to check for pixels
		dim dicPageAttributeInput: set dicPageAttributeInput = CreateObject("Scripting.Dictionary")
		set dicContentEventText = GetContentEventText( dicPageAttributeInput)
	end if
	'set known product attributes to replace out template placeholders
	dim dicTokenReplacement : set dicTokenReplacement = CreateObject("Scripting.Dictionary")
	dicTokenReplacement( "OrderId")=Qs("o") '{OrderId}
	dicTokenReplacement( "OrderGrandTotal")=Qs("d") '{OrderGrandTotal}
	dicTokenReplacement( "OrderSubTotal")=Qs("c") '{OrderSubTotal}

	if Request.ServerVariables("SERVER_PORT_SECURE")="1" then
		dicTokenReplacement( "Http")="https" '{Http}
	else
		dicTokenReplacement( "Http")="http"
	end if

	dim strPixel: strPixel = ApplyPageAttribute( obj2Str( dicContentEventText( "Pixel")), dicTokenReplacement)

	err.clear  '--clear false-positives
	on error resume next
	response.write( strPixel)
	if err.number<>0 and StartsWith( err.description, "type mismatch") then 
		response.write( "<!-- CepNotInit -->") 
		err.clear
	else
		response.write( "<!-- CepInit -->")	
	end if
end sub

function IsStaging()
	IsStaging = instr( lcase( Request.ServerVariables("SERVER_NAME")) , "www.")=0
end function

sub PermanentRedirect( byval strTargetUrl) '--should move this somewhere global [knguyen/20110511]
	'Request.ServerVariables("SCRIPT_NAME")
	dim strEffTargetUrl: strEffTargetUrl = strTargetUrl

	if not IsStaging() then 'this is to not 301 redirect with the querystring whenver in www [knguyen/20110513]
		dim lstUrlPart: lstUrlPart = split( strTargetUrl, "?")
		dim strEffQs: strEffQs = ""
		strEffTargetUrl = lstUrlPart(0)
	
		if ubound( lstUrlPart)>0 then
			dim lstNvPair: lstNvPair = split( lstUrlPart(1), "&")
			for each strNvPair in lstNvPair
				if instr( strNvPair, "ec=")=0 and instr( strNvPair, "id=")=0 then
					if hasLen( strEffQs) then strEffQs=strEffQs&"&"
						strEffQs=strEffQs&strNvPair 
				end if
				
			next
			if hasLen( strEffQs) then strEffTargetUrl = strEffTargetUrl & "?" & strEffQs
		end if
	end if

	response.Status = "301 Moved Permanently"
	response.AddHeader "Location", strEffTargetUrl
	response.end
end sub

'create link with default title attribute
function GetLink( byval strUrl, byval strLabel)
	GetLink=GetLinkGeneral( strUrl, strLabel, false, false)
end function

function GetLinkPop( byval strUrl, byval strLabel)
	GetLinkPop=GetLinkGeneral( strUrl, strLabel, false, true)
end function


function GetXLink( byval strUrl, byval strLabel)
	GetXLink=GetLinkGeneral( strUrl, strLabel, true, true)
end function

function GetLinkGeneral( byval strUrl, byval strLabel, byval blnNoFollow, byval blnNewWindow) '--please do not use this function directly [knguyen/20110517]
	dim strNoFollow: strNoFollow="": if blnNoFollow then strNoFollow=" rel=""nofollow"""
	dim strNewWindow: strNewWindow="": if blnNewWindow then strNewWindow=" target=""_new"""
	GetLinkGeneral = "<a href="""&strUrl&""" title="""&HtmlEncode(TagStrip(strLabel))&""""&strNoFollow&strNewWindow&">"&strLabel&"</a>" 
end function

function HtmlEncode( byval strValue)
	HtmlEncode=replace(replace(replace(replace(strValue, "<", "&lt;"), ">", "&gt;"), """", "&quot;"), "'", "&#39;")
end function


function TagStrip( strHtml) 'targeted html tag strip
	dim lstTag: lstTag = Array( _
					"(<span[^>]*>)" _
					,"(</span>)" _
					,"(<br[^>]*>)" _
					,"(<br[^>]*/>)" _
					,"(<p[^>]*>)" _
					,"(</p>)" _
					,"(<em[^>]*>)" _
					,"(</em>)" _			
					,"(<img[^>]*>)" _
					,"(<i[^>]*>)" _
					,"(</i>)" _
					,"(<b[^>]*>)" _
					,"(</b>)" _
					,"(<strong[^>]*>)" _
					,"(</strong>)" _
					,"(<ul[^>]*>)" _
					,"(</ul>)" _
					,"(<li[^>]*>)" _
					,"(</li>)" _
					,"(<font[^>]*>)" _ 
					,"(</font>)" _
					,"(<font[^>]*>)" _
					,"(</font>)" _
					,"(<table[^>]*>)" _
					,"(</table>)" _
					,"(<tbody[^>]*>)" _
					,"(</tbody>)" _
					,"(<tr[^>]*>)" _
					,"(</tr>)" _
					,"(<td[^>]*>)" _
					,"(</td>)" _
					,"(<div[^>]*>)" _
					,"(</div>)" _
					,"(<p[^>]*/>)" _
					,"(<a[^>]*>)" _
					,"(</a>)" _
					,"(<ol[^>]*>)" _
					,"(</ol>)" _
					,"(<sup[^>]*>)" _
					,"(</sup>)" _
					,"(<sub[^>]*>)" _
					,"(</sub>)")
	
	
	dim objRegExp, strOutput
	Set objRegExp = New Regexp
	
	objRegExp.IgnoreCase = True
	objRegExp.Global = True
	
	for each strTag in lstTag
		objRegExp.Pattern = strTag
		strHtml = objRegExp.Replace(strHtml, "")
	next
	
	set objRegExp = Nothing
	
	TagStrip = strHtml

end function

function GetAlphaNumeric( byval strText)
	GetAlphaNumeric=EMPTY_STRING
	if hasLen( strText) then
		dim intIndex, strNextChar
		for intIndex=1 to len(strText)
			strNextChar=mid(strText,intIndex,1)
			if ( lcase( strNextChar)>="a" and lcase( strNextChar)<="z") or _
				( lcase( strNextChar)>="0" and lcase( strNextChar)<="9") then
				GetAlphaNumeric = GetAlphaNumeric & strNextChar
			end if
		next
	end if
end function

const TABLE_START = "<table border=""0"" cellspacing=""0"" cellpadding=""0"">"
const TABLE_END = "</table>"
const EMPTY_STRING = ""
const TOKEN_BRANDNAME = "{BrandName}"
const TOKEN_MODELNAME = "{ModelName}"
const TOKEN_PRODUCTNAME = "{ProductName}" ' corresponds to ItemDesc
const TOKEN_CATEGORYNAME = "{CategoryName}"
const TOKEN_CARRIERNAME = "{CarrierName}"

const WE_ID = "0"
const CA_ID = "1"
const CO_ID = "2"
const PS_ID = "3"
const ER_ID = "10"
const FG_ID = "5"

'querystring access with whitespace sanitization
function Qs( strKey)
	Qs=obj2Str( Request.QueryString( strKey))
end function

function Ck( byval strKey)
	Ck = EMPTY_STRING
	
	if not Request.Cookies is nothing then '--ensure cookie collection is valid
		if not Request.Cookies( strKey) is nothing then '--ensure cookie object is valid
			if not Request.Cookies( strKey).HasKeys then '--ensure cookie object is single-level
				Ck = Obj2Str( Request.Cookies( strKey))
			end if
		end if
	end if
end function

function CkSet( byval strKey, byval strValue)
	Response.Cookies( strKey) = strValue
end function

'form value access with whitespace sanitization
function Fv( strKey)
	Fv=obj2Str( Request.Form( strKey))
end function

'basic debug statement.. provide label and value
sub Dbg( byval strName, byval strValue)
	response.write( strName & ":[" & strValue & "]"&vbcrlf)
end sub

'quick debug statement (querystring version)
sub DbgQs( strName)
	call Dbg( strName, Request.QueryString( strName))
end sub

'quick debug statement (form post version)
sub DbgFs( strName)
	call Dbg( strName, Request.Form( strName))
end sub

'quick debug statement (global variable version)
sub DbgGlobal( byval strName)
	response.write( strName & ":[" & replace(eval( strName),"<","&lt;") & "]"&vbcrlf)
end sub

'gets the url path as developer sees from the code
function GetPhysicalAbsolutePath()
	GetPhysicalAbsolutePath=Request.ServerVariables("SCRIPT_NAME")
end function

'gets the url path as the visitor sees from the browser
function GetVirtualAbsolutePath()
	dim strHttpXRewriteUrl: strHttpXRewriteUrl = Request.ServerVariables("HTTP_X_REWRITE_URL")
	dim intPosition: intPosition = instr(1, strHttpXRewriteUrl, "?", VbTextCompare)
	GetVirtualAbsolutePath=strHttpXRewriteUrl

	if intPosition>1 then GetVirtualAbsolutePath=left( strHttpXRewriteUrl, instr(1, strHttpXRewriteUrl, "?", VbTextCompare)-1)
end function

'gets domain host (without protocol segment, i.e. "http://"
function GetCurrentDomain()
	GetCurrentDomain = Request.ServerVariables("SERVER_NAME")
end function

'gets domain host with protocol segment, i.e. "http://" or "https://" depending on current security context
function GetCurrentDomainFull()
	dim strHttp: strHttp = EMPTY_STRING
	if Request.ServerVariables("SERVER_PORT_SECURE")="1" then
		strHttp="https"
	else
		strHttp="http"
	end if

	GetCurrentDomainFull = strHttp & "://" & GetCurrentDomain()
end function

if IsStaging() and Qs("_Test")="1" then
	call response.write( "<!--" & vbcrlf)
	
		'Request.ServerVariables("SCRIPT_NAME")
	call Dbg( "ServerVariables[SCRIPT_NAME]", GetPhysicalAbsolutePath())
	call DbgQs( "CategoryId")
	call DbgQs( "BrandId")
	call DbgQs( "ModelId")
	call DbgQs( "CarrierId")
	call DbgQs( "ItemId")
	call DbgQs( "PhoneOnly")
	call response.write( "-->")
end if

function GetPhysicalImagePath( byval strImageSubPath)
	dim strPhysicalImagePath: strPhysicalImagePath = Server.MapPath("/images/")

	if hasLen( strImageSubPath) then
		strImageSubPath = replace( strImageSubPath, "/", "\")
		if not StartsWith( strImageSubPath, "\") then strImageSubPath = "\" & strImageSubPath
	else
		strImageSubPath = EMPTY_STRING
	end if

	GetPhysicalImagePath = strPhysicalImagePath & strImageSubPath
end function

function FormatDecimalInput( byval strValue)
	FormatDecimalInput = EMPTY_STRING
	if HasLen( strValue) then 
		if IsNumeric( strValue) then
			FormatDecimalInput = FormatNumber( strValue, 2, -1, 0, 0)
		end if
	end if
end function

function FormatUsdOutput( byval strValue)
	if not HasLen( strValue) then strValue="0"
	if not IsNumeric( strValue) then strValue="0"
	FormatUsdOutput = FormatCurrency( strValue, 2, -1, 0, -1)
end function

function NewQueryString( byval strQueryString)
	set NewQueryString=new DelimitedList
	NewQueryString.Delimiter="&"
	while StartsWith( strQueryString, "?")
		strQueryString = mid( strQueryString, 2)
	wend
	NewQueryString.List = strQueryString
end function


'follow class to be separated later as inc_DelimitedList
Class DelimitedList
	'************** Properties **************
	Public List
	Public Delimiter
	'****************************************

	'*********** Event Handlers *************
	Private Sub Class_Initialize()
		List=EMPTY_STRING
		Delimiter="|"
	End Sub
	'****************************************

	'************ Property Get **************
	Public Property Get Item( byval strName)
		Item = EMPTY_STRING

		dim strNameValue
		for each strNameValue in split( List, Delimiter)
			if instr( 1, strNameValue, strName&"=", VBTextCompare)=1 then Item = replace(( split( strNameValue, "="))(1),"%3D", "="): exit Property
		next		
	End Property

	Public Property Get Count
		Count=0
		if HasLen( List) then Count = UBound( split( List, Delimiter))+1		
	End Property

	Public Property Get AsArray
		AsArray = split( List, Delimiter)
	End Property
	
	Public Property Get AsQueryString
		AsQueryString = List
		if HasLen( AsQueryString) then AsQueryString = "?" & AsQueryString
	End Property	
	'****************************************


	'************** Methods *****************

	Public Sub Add( byval strName, byval strValue)
	  if HasLen( strName) and HasLen( strValue) then
	    if HasLen( List) then List = List & Delimiter
	    List = List & strName &"="& replace( strValue, "=", "%3D")
	  end if
	End Sub

	Public Sub AddUnique( strName, strValue)
		call Remove( strName)
		call Add( strName, strValue)
	End Sub

	Public Sub Remove( byval strName)
		dim strListNew: strListNew=EMPTY_STRING
		dim strNameValue 
	
		for each strNameValue in split( List, Delimiter)
			if not StartsWith( strNameValue, strName&"=") then
				if HasLen( strListNew) then strListNew = strListNew & Delimiter
				strListNew = strListNew & strNameValue
			end if
		next
		
		List = strListNew
	End Sub
	'****************************************
End Class %>