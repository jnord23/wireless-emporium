<%

' global constants
const TABLE_START = "<table border=""0"" cellspacing=""0"" cellpadding=""0"">"
const TABLE_END = "</table>"
const EMPTY_STRING = ""
const DBQT = """"
const TOKEN_BRANDNAME = "{BrandName}"
const TOKEN_MODELNAME = "{ModelName}"
const TOKEN_PRODUCTNAME = "{ProductName}" ' corresponds to ItemDesc
const TOKEN_CATEGORYNAME = "{CategoryName}"
const TOKEN_CARRIERNAME = "{CarrierName}"

' site ids
const WE_ID = "0"
const CA_ID = "1"
const CO_ID = "2"
const PS_ID = "3"
const ER_ID = "10"
const FG_ID = "5"

'500 friends
const WE_500_ACCID = "6duz39nbC69gBUT"
const WE_500_SECKEY = "9wHottvv42jZPNDlGtwqALdwbr4uzq7P"




'WUPay
const WE_WUPAY_MERCHANT_TOKEN = "bwbUBbhCrG8KIXSZcdtemw=="
const WE_WUPAY_SKEY = "T1234567890abcde"

const CA_WUPAY_MERCHANT_TOKEN = "hsSppW5qWmW3kKSxWrK5vA=="
const CA_WUPAY_SKEY = "T1234567890abcde"

const CO_WUPAY_MERCHANT_TOKEN = "OEdu5qSfs9syu3ulGbK/PA=="
const CO_WUPAY_SKEY = "T1234567890abcde"



'Payment Gateway (Authorize.net)
CIM_APITESTURL = "https://apitest.authorize.net/xml/v1/request.api"
CIM_APIURL = "https://api.authorize.net/xml/v1/request.api"

'==========WE
const CC_API_LOGIN_ID = "53gJrLkDFk6x"
const CC_TRANSACTION_KEY = "54rN2d9vMB8hC4z6"
const CC_TRANS_POST_URL = "https://secure.authorize.net/gateway/transact.dll"

'==========CO
const CO_CC_API_LOGIN_ID = "327tsFNsB7"
const CO_CC_TRANSACTION_KEY = "5ySL343Wq3v492nC"
const CO_CC_TRANS_POST_URL = "https://secure.authorize.net/gateway/transact.dll"

'==========CA
const CA_CC_API_LOGIN_ID = "8zm79tMw2W9"
const CA_CC_TRANSACTION_KEY = "5rfgT7D45GT3d5ZT"
const CA_CC_TRANS_POST_URL = "https://secure.authorize.net/gateway/transact.dll"

'==========PS
const PS_CC_API_LOGIN_ID = "47EjQx2h"
const PS_CC_TRANSACTION_KEY = "26Cvh83R86fK4Tyb"
const PS_CC_TRANS_POST_URL = "https://secure.authorize.net/gateway/transact.dll"

'==========TM
const TM_CC_API_LOGIN_ID = "86Ry7xP69"
const TM_CC_TRANSACTION_KEY = "2A4VzhX9KYe886py"
const TM_CC_TRANS_POST_URL = "https://secure.authorize.net/gateway/transact.dll"



const US50STATES = "AL|AK|AZ|AR|CA|CO|CT|DE|FL|GA|HI|ID|IL|IN|IA|KS|KY|LA|ME|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VT|VA|WA|WV|WI|WY"

' special integer symantecs
const ENUM_INVALID_ID = -1
const ENUM_ALL = -2
const ENUM_NONE = -3

const DIC_COMPARE_MODE_INSENSITIVE = 1

' database table site-independent abstraction
dim dicTableReview: set dicTableReview = CreateObject("Scripting.Dictionary")
dicTableReview( WE_ID) = "we_Reviews"
dicTableReview( CA_ID) = "ca_Reviews"
dicTableReview( CO_ID) = "co_Reviews"
dicTableReview( PS_ID) = "ps_Reviews"
%>