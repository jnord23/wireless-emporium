<%

'dropdown option values

dim plReviewOrderDate: set plReviewOrderDate = new DelimitedList
plReviewOrderDate.IgnoreEmptyValue = False
call plReviewOrderDate.Add( "Date: Any Order", EMPTY_STRING)
call plReviewOrderDate.Add( "Date: Newest", "Newest")
call plReviewOrderDate.Add( "Date: Oldest", "Oldest")

dim plReviewOrderRating: set plReviewOrderRating = new DelimitedList
plReviewOrderRating.IgnoreEmptyValue = False
call plReviewOrderRating.Add( "Rating: Any Order", EMPTY_STRING)
call plReviewOrderRating.Add( "Rating: Highest", "Highest")
call plReviewOrderRating.Add( "Rating: Lowest", "Lowest")

'queries used

function GetRatingLevel( byval dblRating)
    if dblRating = 5 then
        GetRatingLevel = "Excellent"
    elseif dblRating => 4.5 then
        GetRatingLevel = "Very Good - Excellent"
    elseif dblRating => 4 then
        GetRatingLevel = "Very Good"
    elseif dblRating => 3.5 then
        GetRatingLevel = "Good - Very Good"
    elseif dblRating => 3 then
        GetRatingLevel = "Good"
    elseif dblRating => 2.5 then
        GetRatingLevel = "Fair - Good"
    elseif dblRating => 2 then
        GetRatingLevel = "Fair"
    elseif dblRating => 1.5 then
        GetRatingLevel = "Poor - Fair"
    else
        GetRatingLevel = "Poor"
    end if
end function

function ReviewCondition( byval strItemId)
	ReviewCondition = _
		" where " &VbCrLf&_
		" 	( " &VbCrLf&_
		" 		PartNumbers in ( " &VbCrLf&_
		" 			select PartNumber " &VbCrLf&_
		" 			from we_Items with(nolock) " &VbCrLf&_
		" 			where ItemId="& DbId( strItemId) &VbCrLf&_
		" 		)			 " &VbCrLf&_
		" 		or " &VbCrLf&_
		" 		ItemId="& DbId( strItemId) &VbCrLf&_
		" 	)" &VbCrLf&_
		" 	and Approved = 1"
end function

function GetReview( byval strItemId, byval intTopN, byval strOrderByDate, byval strOrderByRating)
	dim plSortOrder: set plSortOrder = new DelimitedList
	plSortOrder.Delimiter = ","
	if IsEqual( strOrderByDate, "newest") then call plSortOrder.AddElement( "DateTimeEntd desc")
	if IsEqual( strOrderByDate, "oldest") then call plSortOrder.AddElement( "DateTimeEntd")
	if IsEqual( strOrderByRating, "highest") then call plSortOrder.AddElement( "Rating desc")
	if IsEqual( strOrderByRating, "lowest") then call plSortOrder.AddElement( "Rating")

	dim strSortOrderClause: strSortOrderClause = EMPTY_STRING
	if not plSortOrder.IsEmpty then strSortOrderClause = " order by "& plSortOrder.List
	set plSortOrder = nothing

	dim objSqlExec: set objSqlExec = new SqlExec
	dim strSqlTopN: strSqlTopN = EMPTY_STRING
	if intTopN>0 then strSqlTopN = " top "& DbNum( intTopN)

	objSqlExec.Sql = _
		" select"& strSqlTopN &VbCrLf&_
		" 	Nickname, " &VbCrLf&_
		" 	ReviewTitle, " &VbCrLf&_
		" 	Review, " &VbCrLf&_
		" 	Rating," &VbCrLf&_
		" 	DateTimeEntd" &VbCrLf&_
		" from "& dicTableReview( GetSiteId()) &" with(nolock)" &VbCrLf&_
		ReviewCondition( strItemId) &VbCrLf&_
		strSortOrderClause

	objSqlExec.Execute()
	set GetReview = objSqlExec
end function

function GetUserRatingStat( byval strItemId)
	dim dicReturnValue : set dicReturnValue = CreateObject("Scripting.Dictionary")
	dicReturnValue( "AvgRating")="0.0"
	dicReturnValue( "RatingCount")="0"

	dim objSqlExec: set objSqlExec = new SqlExec
	objSqlExec.Sql =  _
		" select " &VbCrLf&_
		" 	isnull( avg( Rating), 0.0) as AvgRating " &VbCrLf&_
		"	, count( Rating) as RatingCount " &VbCrLf&_
		" from "& dicTableReview( GetSiteId()) &" with(nolock) " &VbCrLf&_
		ReviewCondition( strItemId) 
	objSqlExec.Execute()

	if objSqlExec.Read() then
		dicReturnValue( "AvgRating")=objSqlExec.Item( "AvgRating")
		dicReturnValue( "RatingCount")=objSqlExec.Item( "RatingCount")
	end if

	set objSqlExec = nothing

	set GetUserRatingStat = dicReturnValue
end function


%>